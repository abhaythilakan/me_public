import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { ModalsService } from '../services/modals.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-retire-asset-popup',
  templateUrl: './retire-asset-popup.component.html',
  styleUrls: ['./retire-asset-popup.component.scss']
})
export class RetireAssetPopupComponent implements OnInit {
  totalAmountArr: any[];

  public Asset_RetirementDetailsModel_Primary: Asset_RetirementDetailsModel_Primary[] = [];
  public Asset_RetirementDetailsModel_Spouse: Asset_RetirementDetailsModel_Spouse[] = [];

  otherRetAssetTypeDDL_Primary = [];
  //RetAssetTaxVsDefferedDDL = [];

  constructor(
    private modalService: NgbModal,
    private storeService: StoreServicesService,
    public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService,
    public modal: ModalsService,
    private router: Router
  ) { }

  ngOnInit() {
    //this.getRetAssetTaxVsDefferedDDL();
    this.getRetirementAssetDetails_Primary();
    this.getotherRetAssetTypeDDL_Primary();
  }
  ngDoCheck() {
    if (this.Asset_RetirementDetailsModel_Primary == undefined || !(this.Asset_RetirementDetailsModel_Primary.length > 0)) {
      //alert("this.Asset_RetirementDetailsModel_Primary.length"+this.Asset_RetirementDetailsModel_Primary);''
      //this.cashAccountAssetModel.AssetList = this.nonRetirementAssetsModel4;
      //if (!(this.Asset_RetirementDetailsModel_Primary.length > 0)) {
      let mdl = new Asset_RetirementDetailsModel_Primary();
      mdl.retID = 0;
      mdl.assetID_Retirement = this.storeService.getObject('AssetID');
      mdl.isActive = "true";
      mdl.isOtherRetirement = false;
      mdl.profileID = this.storeService.getObject('profileID');
      this.Asset_RetirementDetailsModel_Primary = [];
      this.Asset_RetirementDetailsModel_Primary.push(mdl);
      //this.totalCashAccProp = 0;
      //}
    }
  }
  dismissModal() {
    this.modal.dismissModal();
  }
  closeModal() {
    this.modal.closeModal();
    //this.storeService.putObject("confirmDelete",false);
  }
  confirmModal() {
    //this.storeService.putObject("confirmDelete",true);
    this.modal.closeModal();
  }

  
  // getRetAssetTaxVsDefferedDDL() {
  //   this.ApiServicesService.headerAppendedGet('api/Common/getTaxVsDeffered').then(res => {
  //     if (res && res._body && !res._body.target) {
  //       let parseBody = JSON.parse(res._body);
  //       this.RetAssetTaxVsDefferedDDL = parseBody.Result;
  //     }
  //   })
  // }

  getotherRetAssetTypeDDL_Primary() {
    this.ApiServicesService.headerAppendedGet('api/Common/AssetTypesOtherRetirement').then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.otherRetAssetTypeDDL_Primary = parseBody.Result;
      }
    })
  }

  getRetirementAssetDetails_Primary() {
    this.ApiServicesService.headerAppendedPost('api/Asset/getRetirementAssetDetails', this.storeService.getObject('AssetID')).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.Asset_RetirementDetailsModel_Primary = parseBody.Result;
      }
    })
  }


  // onChangeOtherRetiretaxVsDefferedTypeDDL_Primary(val: any, i) {
  //   this.Asset_RetirementDetailsModel_Primary[i].taxVsDefferedID = val;
  // }
  onChangeOtherRetireTypeDDL_Primary(val: any, i) {
    this.Asset_RetirementDetailsModel_Primary[i].assetTypeID = val;
  }

  addOtherRetire_Primary() {
    let mdl = new Asset_RetirementDetailsModel_Primary();
    mdl.retID = 0;
    mdl.assetID_Retirement = this.storeService.getObject('AssetID');
    mdl.isActive = "true";
    mdl.isOtherRetirement = false;
    mdl.profileID = this.storeService.getObject('profileID');
    this.Asset_RetirementDetailsModel_Primary.push(mdl);
  }

  removeOtherRetRow_Primary(index) {
    if (this.Asset_RetirementDetailsModel_Primary[index].retID > 0) {
      this.Asset_RetirementDetailsModel_Primary[index].isActive = "false";
    }
    else {
      this.Asset_RetirementDetailsModel_Primary.splice(index);
    }
  }

  addDeletedOtherRetRow_Primary(index) {
    this.Asset_RetirementDetailsModel_Primary[index].isActive = "true";
  }

  onParimarySave() {
    this.ApiServicesService.headerAppendedPost('api/Asset/saveRetirementDetails', this.Asset_RetirementDetailsModel_Primary).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        if (parseBody.apiStatus == 0) {
          this.notificationService.success("Retirement Asset Saved Successfully");
          this.Asset_RetirementDetailsModel_Primary = parseBody.Result;
          this.closeModal();
        }
        else if (parseBody.apiStatus == 1) {
          this.notificationService.error(parseBody.ValidationErrors[0].FieldErrorMessage);
        }
        else if (parseBody.apiStatus == 2) {
          this.notificationService.error(parseBody.Result.Message);
        }
      }
    }).catch(err => {
      if (err && err._body && !err._body.target) {
        let parsedBody = JSON.parse(err._body);
        if ((parsedBody.error && parsedBody.message) || (parsedBody.message && parsedBody.apiStatus == 1))
          this.notificationService.error(parsedBody.message);
        else
          this.notificationService.error('Error while saving Retirement Asset Collection');
      } else {
        this.notificationService.error('Error while saving Retirement Asset Collection');
      }
    });
  }

}

export class Asset_RetirementDetailsModel_Primary {
  public retID: number
  public assetID_Retirement: number
  public profileID: number
  public assetTypeID: number
  public taxVsDefferedID: number
  public company: string
  public amount: number
  public valueAsOf: Date = new Date()
  public monthlyContribution: number
  public empContribution: number
  public notes: string
  public isOtherRetirement: Boolean = false
  public isActive: string = ""
  public createdDate: Date = new Date()
  public updatedDate: Date = new Date()
  public updatedBy: number
}

export class Asset_RetirementDetailsModel_Spouse {
  public retID: number
  public assetID_Retirement: number
  public profileID: number
  public assetTypeID: number
  public taxVsDefferedID: number
  public company: string
  public amount: number
  public valueAsOf: Date = new Date()
  public monthlyContribution: number
  public empContribution: number
  public notes: string
  public isOtherRetirement: Boolean = false
  public isActive: string = ""
  public createdDate: Date = new Date()
  public updatedDate: Date = new Date()
  public updatedBy: number
}



