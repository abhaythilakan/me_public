import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetireAssetPopupComponent } from './retire-asset-popup.component';

describe('RetireAssetPopupComponent', () => {
  let component: RetireAssetPopupComponent;
  let fixture: ComponentFixture<RetireAssetPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetireAssetPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetireAssetPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
