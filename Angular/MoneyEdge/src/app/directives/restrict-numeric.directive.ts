import { Directive, ElementRef, Renderer, Input, HostListener, NgModule } from '@angular/core';
import * as $ from "jquery";

@Directive({
  selector: '[restrictNumeric]',
})
export class InputRestricterDirective {
  @Input() restrictNumeric: number;
  constructor(private el: ElementRef, private renderer: Renderer) {

  }

  @HostListener('keydown', ['$event']) keydown(event) {
    let e = < KeyboardEvent > event;
    if(e.ctrlKey)
      e.preventDefault();
  }

  @HostListener('keypress', ['$event']) keypress(event) {
    let el = < HTMLSelectElement > event.target;
    let e = < KeyboardEvent > event;
    let previous: any = el.previousElementSibling;
    if (e.altKey || this.checkIfNotNumber(e))
      e.preventDefault();
    else if (e.which == 8) {
      if (!el.value && previous && e.which == 8)
        previous.focus()
    }
  }

  private checkIfNotNumber(e): boolean {
    return (e.shiftKey || (e.which < 48 || e.which > 57)) &&
      e.which != < number > 8 &&
      e.which != < number > 9 &&
      e.which != < number > 13 &&
      e.which != < number > 39 &&
      e.which != < number > 37
  }
}

