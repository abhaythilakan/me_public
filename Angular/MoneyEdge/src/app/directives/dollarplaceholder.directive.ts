import { Directive, HostListener, ElementRef, OnInit, SimpleChanges } from "@angular/core";
import { CurrencypipePipe } from "../pipe/currencypipe.pipe";

@Directive({
  selector: '[appDollarplaceholder]'
})
export class DollarplaceholderDirective {
  private el: HTMLInputElement;
  private onLoadCheck: boolean = false;

  constructor(
    private elementRef: ElementRef,
    private currencyPipe: CurrencypipePipe
  ) {
    this.el = this.elementRef.nativeElement;
  } 


  ngOnInit() {
    this.el.blur();
    // if ((this.el.value !== null) && (this.el.value.trim() !== '')) {
    //   this.el.value = this.currencyPipe.transform(this.el.value);
    // } else {
    //   this.el.value = "0.00";
    // }
  }

  @HostListener("focus", ["$event.target.value"])
  onFocus(value) {
    console.log("Focus");
    this.el.value = this.currencyPipe.parse(value); // opossite of transform
  }

  @HostListener("blur", ["$event.target.value"])
  onBlur(value) {
    console.log("Blur");
    this.el.value = this.currencyPipe.transform(value);
  }
}



