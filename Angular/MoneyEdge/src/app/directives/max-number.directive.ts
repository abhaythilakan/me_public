import { Directive, ElementRef, Renderer, Input, HostListener, NgModule } from '@angular/core';
import * as $ from "jquery";

@Directive({
    selector: '[restrictmaxvalue]'
})
export class MaxNumberDirective {
    @Input('restrictmaxvalue') maxValue: number;

    constructor(private el: ElementRef, private renderer: Renderer) { }
    @HostListener('keyup', ['$event']) onKeyUp(event) {
        let oldVal = null;
        let el = <HTMLSelectElement>event.target;

        if (Number($(el).val()) > Number(this.maxValue) &&
            event.keyCode != 46 // delete
            &&
            event.keyCode != 8 // backspace
        ) {
            event.preventDefault();
            $(el).val(oldVal);
        } else {
            oldVal = Number($(el).val());
        }

    }

}


