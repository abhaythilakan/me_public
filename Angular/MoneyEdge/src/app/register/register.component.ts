import { Component, OnInit, NgZone } from '@angular/core';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { StoreServicesService } from '../services/store-services.service';
import { Router, NavigationExtras } from '@angular/router';
import { Data } from '../providers/data/data.service';

@Component({
  selector: 'app-register',

  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  public ResultOfSignUp={
    "tokeninfo":{
      "access_token": "",
      "token_type": "",
      "expires_in": "",
      "authority": ""
    }
  }
  emailIdExist: boolean;
  fullDataparsed: any;
  fullData: any;
  errored_msg_status: boolean;
  errored_msg: any;
  userNameExist: boolean;
  name1: any;
  _timeout: any;
  public signUpModel: any = {
    fname: "",
    lname: "",
    mobile: "",
    email: "",
    password: "",
    refCode: ""
  };
  constructor(public ApiServicesService: ApiServicesService, private notificationService: NotificationServicesService,
    private storeService: StoreServicesService, private router: Router, public lc: NgZone, private data: Data) {
    this.userNameExist = false;

  }

  ngOnInit() {
    this.storeService.putObject("hasClients", false);
  }
  public register(): void {
    if (!this.userNameExist) {
      let req =
        {
          "advisorInfo": {
            "firstName": this.signUpModel.firstname,
            "lastName": this.signUpModel.lastname
          },
          "UserInfo": {
            "emailPrimary": this.signUpModel.email,
            "userName": this.signUpModel.fname,
            "password": this.signUpModel.password
          }
        }


      let header = {
        "Content-Type": "application/json"
      }




      this.
        ApiServicesService.post('api/Advisor/AdvisorRegister', req, { headers: header })
        .then(res => {
          let parsedBody = JSON.parse(res._body);
          let result = JSON.stringify(parsedBody);
          this.ResultOfSignUp=parsedBody.Result;
          console.log("token"+this.ResultOfSignUp.tokeninfo.access_token);
          this.storeService.putObject('token',this.ResultOfSignUp.tokeninfo.access_token);
          if (parsedBody.apiStatus == 0) {
            // alert("suceess");
            console.log("advisor dta" + result);
            let object = {
              "name": parsedBody
            }
            this.ApiServicesService.headerAppendedPost('api/Login/GetSessionContext', '').then(res => {
              let parsedBody = JSON.parse(res._body);
              this.storeService.putObject("advisorId", parsedBody.Result.advisorID);
              this.storeService.putObject("Role", parsedBody.Result.Role);
              this.storeService.putObject("UserID", parsedBody.Result.UserID);
              this.storeService.putObject("hasClients", parsedBody.Result.hasClients);

            })
              .catch(error => {
                this.notificationService.error(error);
              })

            let advisorDataObject = {
              "userinfo": parsedBody.Result.UserInfo,
              "advisorInfo": parsedBody.Result.advisorInfo,
              "advglobal": parsedBody.Result.advglobal,
              "advRetirement": parsedBody.Result.advRetirement,
              "advIncome": parsedBody.Result.advIncome,
              "advDebt": parsedBody.Result.advDebt,
              "advEmergency": parsedBody.Result.advEmergency,
              "advInsurance": parsedBody.Result.advInsurance,
              "advEducation": parsedBody.Result.advEducation,
              "advBranch": parsedBody.Result.advBranch,
              // "advContact":parsedBody.Result.advContact,
              "adminadvancedReportDefaults": parsedBody.Result.adminadvancedReportDefaults,
              "adminExpressReportDefaults": parsedBody.Result.adminExpressReportDefaults,
              "defaultColleges": parsedBody.Result.defaultColleges,
              "AdvancedReportDefaults": parsedBody.Result.AccesSettings.AdvancedReportDefaults,
              "ExpressReportDefaults": parsedBody.Result.AccesSettings.ExpressReportDefaults

            }
            this.storeService.putObject("advContact", parsedBody.Result.advContact);
            this.storeService.putObject("userinfo", parsedBody.Result.UserInfo);
            this.storeService.putObject("advisorInfo", parsedBody.Result.advisorInfo);
            this.storeService.putObject("advisormodel", advisorDataObject);
            this.storeService.putObject("token", parsedBody.Result.tokeninfo.access_token);
            this.storeService.putObject("AdvancedReportDefaults", parsedBody.Result.AccesSettings.AdvancedReportDefaults);
            this.storeService.putObject("ExpressReportDefaults", parsedBody.Result.AccesSettings.ExpressReportDefaults);
            this.storeService.putObject("advisorId", parsedBody.Result.advisorInfo.advisorId);


            // this.data.storage = advisorDataObject;
            this.router.navigate(["advisor-settings"]);



          }
          else if (parsedBody.apiStatus == 1) {
            this.errored_msg_status = true;
            this.notificationService.error(parsedBody.ValidationErrors[0].FieldErrorMessage);
            this.errored_msg = parsedBody.ValidationErrors[0].FieldErrorMessage;

          }
          else if (parsedBody.apiStatus == 2) {
            this.notificationService.error(parsedBody.Message);
          }
        })
        .catch(err => {
          if (err && err._body && !err._body.target) {
            let parsedBody = JSON.parse(err._body);
            if (parsedBody.error && parsedBody.message)
              this.notificationService.error(parsedBody.message);
            else
              this.notificationService.error('Could not register');
          } else {
            this.notificationService.error('Could not register');
          }
        });
    }
  }


  displayName() {
    this._timeout = null;
    if (this._timeout) { //if there is already a timeout in process cancel it
      window.clearTimeout(this._timeout);
    }
    this._timeout = window.setTimeout(() => {
      let req =
        {
          "userName": this.signUpModel.fname,
          "userId": 0
        }

      // this.memberShipService.register(req, this.goBack.bind(this));

      let header = {
        "Content-Type": "application/json"
      }
      this._timeout = null;
      this.lc.run(() =>
        // this.name1 = this.signUpModel.fname
        this.
          ApiServicesService.post('api/Login/IsUsernameExist', req, { headers: header })
          .then(res => {
            let parsedBody = JSON.parse(res._body);
            if (parsedBody.Result.isExists) {
              console.log("user name" + parsedBody.Result.Message);
              this.userNameExist = true;
            }
            else {
              console.log("user name" + parsedBody.Result.Message);
              this.userNameExist = false;
            }


          })
          .catch(err => {
            if (err && err._body && !err._body.target) {
              let parsedBody = JSON.parse(err._body);
              if (parsedBody.error && parsedBody.message)
                this.notificationService.error(parsedBody.message);
              else
                this.notificationService.error('Please check the name');
            } else {
              this.notificationService.error('Please check the name');
            }
          })
      );

    }, 1000);
  }
  checkEmail() {
    this._timeout = null;
    if (this._timeout) { //if there is already a timeout in process cancel it
      window.clearTimeout(this._timeout);
    }
    this._timeout = window.setTimeout(() => {
      let req =
        {
          "email": this.signUpModel.email,
          "userId": 0
        }

      // this.memberShipService.register(req, this.goBack.bind(this));

      let header = {
        "Content-Type": "application/json"
      }
      this._timeout = null;
      this.lc.run(() =>
        // this.name1 = this.signUpModel.fname
        this.
          ApiServicesService.post('api/Login/IsEmailExist', req, { headers: header })
          .then(res => {
            let parsedBody = JSON.parse(res._body);
            if (parsedBody.Result.isExists) {
              console.log("email id" + parsedBody.Result.Message);
              this.emailIdExist = true;
            }
            else {
              console.log("email id" + parsedBody.Result.Message);
              this.emailIdExist = false;
            }


          })
          .catch(err => {
            if (err && err._body && !err._body.target) {
              let parsedBody = JSON.parse(err._body);
              if (parsedBody.error && parsedBody.message)
                this.notificationService.error(parsedBody.message);
              else
                this.notificationService.error('Please check the email id');
            } else {
              this.notificationService.error('Please check the  email id');
            }
          })
      );

    }, 1000);
  }
}
