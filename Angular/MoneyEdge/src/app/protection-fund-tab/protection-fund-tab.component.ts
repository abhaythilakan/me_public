import { Component, OnInit } from '@angular/core';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { StoreServicesService } from '../services/store-services.service';

@Component({
  selector: 'app-protection-fund-tab',
  templateUrl: './protection-fund-tab.component.html',
  styleUrls: ['./protection-fund-tab.component.scss']
})
export class ProtectionFundTabComponent implements OnInit {
  DetailShow = [];

  public protectionNeedResult = {
    "profileID": null,
    "inflationRate": null,
    "returnRate": null,
    "protectionNeedModel": [{
      "personID": null,
      "personName": null,
      "isPrimary": null,
      "totalDebtAndDeath": null,
      "debtIsInclude": null,
      "incomeReplacement": null,
      "totalMortgage": null,
      "mortgageIsInclude": null,
      "totalEducationCost": null,
      "eduIsInclude": null,
      "DebtAndDeath": {
        "finalExpenseName": null,
        "finalExpenseValue": null,
        "DebtDetails": [{}],
      },
      "Mortgage": [{}],
      "EducationNeed": [{
        "name": null,
        "EducationNeedDetailsModel": [{}]
      }],
      "totalProtectionNeed": "",
    }],
    "protectionPlan": [
      {
        "personName": null,
        "totalGroup": null,
        "Group": null,
        "totalIndividual": null,
        "Individual": null,
        "totalAssets": null,
        "Assets": null,
        "totalProtectionPlan": null,
        "Shortfall_Surplus": null,
      }],
    "Statement": null,
    "astModel": [{
      "totalSum": null,
      "AssetList": null
    }],
  };
  showProtectionNeed: boolean;
  MortgageDetailShow: boolean;
  EducationDetailShow: boolean;
  profileID: any;
  personCount: number;
  constructor(public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService,
    private storeService: StoreServicesService) { }

  ngOnInit() {

    this.profileID = this.storeService.getObject('profileID');
    this.showProtectionNeed = false;
    this.DetailShow = [{
      "Debt": false,
      "Mortgage": false,
      "Education": false,
    }];
    this.MortgageDetailShow = false;
    this.EducationDetailShow = false;
    this.getDefaultValues();
  }
  getDefaultValues() {
    this.ApiServicesService.headerAppendedPost('api/protection/protectionInfo', this.profileID).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.protectionNeedResult = parsedBody.Result;
        if (this.protectionNeedResult.protectionNeedModel) {
          this.personCount = this.protectionNeedResult.protectionNeedModel.length;
          this.showProtectionNeed = true;
          for (let i = 0; i < this.protectionNeedResult.protectionNeedModel.length; i++) {
            var valueToPush = {}; // or "var valueToPush = new Object();" which is the same
            valueToPush["Debt"] = false;
            valueToPush["Mortgage"] = false;
            valueToPush["Education"] = false;
            this.DetailShow.push(valueToPush);
          }
        }

      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i < parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[i].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Message);

      }
    })
      .catch(error => {
        this.notificationService.error("error while getting education details" + error);
      })
  }

  getResult() {
    let req = {
      "profileID": this.protectionNeedResult.profileID,
      "inflationRate": this.protectionNeedResult.inflationRate,
      "returnRate": this.protectionNeedResult.returnRate,
      "protectionNeedModel": this.protectionNeedResult.protectionNeedModel,
      "protectionPlan":  this.protectionNeedResult.protectionPlan,
      "Statement": this.protectionNeedResult.Statement,
    };

    this.ApiServicesService.headerAppendedPost('api/protection/goProtectionInfo', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        console.log(parsedBody.Result);
        if (parsedBody.Result) {
          this.protectionNeedResult = parsedBody.Result;
        }
      }
      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);
      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);

      }
    })
  }

}
