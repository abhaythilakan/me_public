import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProtectionFundTabComponent } from './protection-fund-tab.component';

describe('ProtectionFundTabComponent', () => {
  let component: ProtectionFundTabComponent;
  let fixture: ComponentFixture<ProtectionFundTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProtectionFundTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProtectionFundTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
