import { Component, OnInit } from '@angular/core';
import { TabsComponent } from './tabs/tabs.component';
import { Location } from '@angular/common';

import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { StoreServicesService } from './services/store-services.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',

  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  route: string;
  token: any;
  email: any;
  sub: any;
  public options = {
    position: ["top", "right"],
    timeOut: 5000,
    lastOnBottom: true
  }
  title = 'MONEY EDGE';
  constructor(private _Activatedroute: ActivatedRoute, location: Location,
    private _router: Router, public storeService: StoreServicesService) {
    _router.events.subscribe((val) => {
      if (location.path() != '') {
        this.route = location.path();
        // alert("_router"+this.route);
       if(this.route!='/support'){
        this.storeService.putObject('hide_login',false);
      }
      } else {
        // this.route = 'Home'
      }
    });

  }
  ngOnInit() {
    this.checkTokenAvailability();
    
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  checkTokenAvailability() {

    window.addEventListener('storage', function(event){
      if (event.key == 'logout-event') { 
         window.location.reload();
      }
  });

    // this.sub = Observable.interval(1000)
    // .subscribe((val) => {
    //   if (!this.storeService.getObject("token")) {
    //     // window.location.reload();
    //   }
    // })
  }

}

