import { Component, OnInit, NgZone, Renderer } from '@angular/core';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { Router, ActivatedRoute } from '@angular/router';
import { parse } from 'url';
import { Location } from '@angular/common';
import * as enLocale from 'date-fns/locale/en';
import * as $ from 'jquery';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.scss']
})
export class AddClientComponent implements OnInit {
  emailIdExist: boolean;
  isRequired: any;
  options = {
    locale: enLocale,
    minYear: 1970,
    maxYear: 2030,
    displayFormat: 'MM-DD-YYYY',
    barTitleFormat: 'MMMM YYYY',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    // minDate: new Date(Date.now()), // Minimal selectable date
    // maxDate: new Date(Date.now())
  };
  Male: string;
  Female: string;
  clientId: number;
  hiddenPwd: boolean;
  clientID: number;
  sub: any;
  userNameExist: boolean;
  _timeout: any;
  reportType: any;
  reports: any;
  notYetSaved: boolean;
  userImage: any;
  image: any;
  DateOfBirth: any;
  public addClientModel = {
    "clientId": null,
    "emailPrimary": "",
    "firstName": "",
    "lastName": "",
    "advisorId": "",
    "imageString": null,
    "imagePath": "",
    "password": "",
    "confirmPassword": "",
    "userName": "",
    "report": {
      "VALUE": "",
      "TEXT": ""
    },
    "clientDOB": null,
    "gender": "",
    "dob": {
      "day": "",
      "month": "",
      "year": ""
    },
    "clientGender": ""

  }
  report = {
    "TEXT": "",
    "VALUE": ""
  }
  constructor(private storeService: StoreServicesService, public ApiServicesService: ApiServicesService, private notificationService: NotificationServicesService,
    private router: Router, private _location: Location, public lc: NgZone, private route: ActivatedRoute, private renderer: Renderer
  ) {
    this.hiddenPwd = true;
  }

  ngOnInit() {
    this.addClientModel.clientGender = "--Select--";
    // let fieldElement=<HTMLFormElement>document.getElementsByClassName('ngx-datepicker-input');
    // alert("fieldElement"+fieldElement);
    // fieldElement.removeAttribute('readonly');
    $('.ngx-datepicker-input').removeAttr('readonly');
    $('.ngx-datepicker-input').attr('readonly', 'false');

    this.Female = "Female";
    this.addClientModel.clientDOB = new Date();
    this.Male = "Male";
    this.hiddenPwd = true;
    this.addClientModel.imagePath = "assets/img/user.png";
    this.sub = this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.clientId = +params['page'] || 0;
        if (this.clientId != null || this.clientId) {

          this.addClientModel.clientId = this.clientId;
          this.hiddenPwd = false;
        }
        else {
          this.hiddenPwd = true;
          this.addClientModel.clientId = 0;
        }
        this.storeService.putObject("clientId", this.clientId);
        if (this.clientId) {
          this.hiddenPwd = true;
        }
        console.log(("client id in page" + this.clientId));
      });
    if (this.storeService.getObject("hasClients") && this.storeService.getObject("clientId")) {
      this.ApiServicesService.headerAppendedGet('api/Client/GetClientOnlyDetails?clientID=' + this.storeService.getObject("clientId")).then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.addClientModel = parsedBody.Result;
          if (this.addClientModel.clientId == null || this.addClientModel.clientId == "" || !this.addClientModel.clientId) {
            this.addClientModel.clientId = 0
          }
          else {
            this.addClientModel.clientId = this.addClientModel.clientId;
          }
          if(this.addClientModel.clientGender == null || this.addClientModel.clientGender == "" || !this.addClientModel.clientGender){
            this.addClientModel.clientGender="--Select--";
          }
          else{
            this.addClientModel.clientGender=this.addClientModel.clientGender;
          }
        }
      });
    }
    this.notYetSaved = true;
    /**Get Report Type */
    let header = {
      "Content-Type": "application/json"
    }
    this.ApiServicesService.get('api/Client/getReportTypeDDL', { headers: header }).then(res => {
      let parsedBody = JSON.parse(res._body);
      console.log("parsed body" + res);
      if (parsedBody.apiStatus == 0) {
        this.reports = parsedBody.Result;
        console.log("parsed body" + this.reports);
      }

    })
  }
  displayName() {
    this._timeout = null;
    if (this._timeout) { //if there is already a timeout in process cancel it
      window.clearTimeout(this._timeout);
    }
    this._timeout = window.setTimeout(() => {
      let req =
        {
          "userName": this.addClientModel.userName,
          "userId": 0
        }
      let header = {
        "Content-Type": "application/json"
      }
      this._timeout = null;
      this.lc.run(() =>
        // this.name1 = this.signUpModel.fname
        this.
          ApiServicesService.post('api/Login/IsUsernameExist', req, { headers: header })
          .then(res => {
            let parsedBody = JSON.parse(res._body);
            if (parsedBody.Result.isExists) {
              console.log("user name" + parsedBody.Result.Message);
              this.userNameExist = true;
            }
            else {
              console.log("user name" + parsedBody.Result.Message);
              this.userNameExist = false;
            }


          })
          .catch(err => {
            if (err && err._body && !err._body.target) {
              let parsedBody = JSON.parse(err._body);
              if (parsedBody.error && parsedBody.message)
                this.notificationService.error(parsedBody.message);
              else
                this.notificationService.error('Please check the name');
            } else {
              this.notificationService.error('Please check the name');
            }
          })
      );

    }, 1000);
  }

  changeListener($event): void {
    this.readThis($event.target);

  }

  readThis(inputValue: any): void {
    var file: File = inputValue.files[0];
    var myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.image = myReader.result;
      let header = {
        "Content-Type": "application/json"
      }
      let req = {
        "id": this.storeService.getObject("clientID"),
        "imageString": this.image
      }
      console.log("image value" + this.image)
      this.
        ApiServicesService.post('api/Client/imageUpload', req, { headers: header })
        .then(res => {
          let parsedBody = JSON.parse(res._body);
          if (parsedBody.apiStatus == 0) {
            this.userImage = parsedBody.Result;
            this.addClientModel.imagePath = parsedBody.Result;

          }
          else if (parsedBody.apiStatus == 2) {
            this.notificationService.error("Not a valid format,Accepts Only .jpg,.png formats");
          }

        })
        .catch(err => {
          if (err && err._body && !err._body.target) {
            let parsedBody = JSON.parse(err._body);
            if (parsedBody.error && parsedBody.message)
              this.notificationService.error(parsedBody.message);
            else
              this.notificationService.error('Image Not Uploaded Properly');
          } else {
            this.notificationService.error('Image Upload Failed');
          }
        });

    }
    myReader.readAsDataURL(file);
  }
  addClient() {
    if (!this.emailIdExist) {
      // this.DateOfBirth = this.addClientModel.clientDOB.month + "/" + this.addClientModel.clientDOB.day + "/" + this.addClientModel.clientDOB.year;

      let body = {
        "emailPrimary": this.addClientModel.emailPrimary,
        "firstName": this.addClientModel.firstName,
        "lastName": this.addClientModel.lastName,
        "advisorId": this.storeService.getObject("advisorId"),
        "imagePath": this.addClientModel.imagePath,
        "password": this.addClientModel.password,
        "userName": this.addClientModel.userName,
        "reportType": this.reportType,
        "clientDOBString": this.addClientModel.clientDOB,
        "gender": this.addClientModel.clientGender,
        "clientId": this.addClientModel.clientId
      }
      this.ApiServicesService.headerAppendedPost('api/Client/saveClient', body).then(res => {
        let parsedBody = JSON.parse(res._body);

        if (parsedBody.apiStatus == 0) {
          this.notYetSaved = false;
          this.router.navigate(["app-global-settings"]);
          let parsedBody = JSON.parse(res._body);
          console.log("Result of reg" + JSON.stringify(parsedBody.Result));
          this.storeService.putObject("clientId", parsedBody.Result.clientId)
        }
        else if (parsedBody.apiStatus == 1) {
          for (let i = 0; i < parsedBody.ValidationErrors.length; i++) {
            this.notificationService.error(parsedBody.ValidationErrors[0].FieldErrorMessage);
          }
        }
      })
        .catch(err => {
          if (err && err._body && !err._body.target) {
            let parsedBody = JSON.parse(err._body);
            if (parsedBody.error && parsedBody.message)
              this.notificationService.error(parsedBody.message);
            else
              this.notificationService.error('Error Occured');
          } else {
            this.notificationService.error('Error Occured');

          }
        });
    }
  }

  globalSettings() {
    this.router.navigate(["app-client-list-new"]);

  }
  ddlChange(newObj) {
    console.log(newObj);
    this.reportType = newObj;
  }
  backClicked() {
    this._location.back();
  }
  checkEmail(e) {
    if (this.addClientModel.emailPrimary == this.storeService.getObject("email_from_login")) {
      console.log("same");
    }
    else {
      console.log("not same");
      this._timeout = null;
      if (this._timeout) { //if there is already a timeout in process cancel it
        window.clearTimeout(this._timeout);
      }
      this._timeout = window.setTimeout(() => {
        let req =
          {
            "email": this.addClientModel.emailPrimary,
            "userId": this.storeService.getObject('UserID')
          }

        let header = {
          "Content-Type": "application/json"
        }
        this._timeout = null;
        this.lc.run(() =>
          // this.name1 = this.signUpModel.fname
          this.
            ApiServicesService.post('api/Login/IsEmailExist', req, { headers: header })
            .then(res => {
              let parsedBody = JSON.parse(res._body);
              if (parsedBody.Result.isExists) {
                console.log("email id" + parsedBody.Result.Message);
                this.emailIdExist = true;
              }
              else {
                console.log("email id" + parsedBody.Result.Message);
                this.emailIdExist = false;
              }


            })
            .catch(err => {
              if (err && err._body && !err._body.target) {
                let parsedBody = JSON.parse(err._body);
                if (parsedBody.error && parsedBody.message)
                  this.notificationService.error(parsedBody.message);
                else
                  this.notificationService.error('Please check the email id');
              } else {
                this.notificationService.error('Please check the  email id');
              }
            })
        );

      }, 1000);
    }

  }
}
