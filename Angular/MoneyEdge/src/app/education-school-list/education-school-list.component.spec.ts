import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EducationSchoolListComponent } from './education-school-list.component';

describe('EducationSchoolListComponent', () => {
  let component: EducationSchoolListComponent;
  let fixture: ComponentFixture<EducationSchoolListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EducationSchoolListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EducationSchoolListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
