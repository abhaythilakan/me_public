import { Component, OnInit } from '@angular/core';
import { ModalsService } from '../services/modals.service';
import { StoreServicesService } from '../services/store-services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-education-school-list',
  templateUrl: './education-school-list.component.html',
  styleUrls: ['./education-school-list.component.scss']
})
export class EducationSchoolListComponent implements OnInit {
  collegeId: any;
  itemsTotal: any;
  collegesNotfound: boolean;
  totalCollegeSerachedUsingName: any;
  totalCollegeSerachedUsingState: any;
  nameBasedsearch: boolean;
  fullsearch: boolean;
  collegeNameByState: any;
  stateBasedsearch: boolean;
  statesForsearch: any;
  states: any;
  tableShow: boolean;
  loader: boolean;
  colleges: any;
  closeResult: string;
  stateValue: any;
  selectedCollege: any;
  schoollist: any;
  page: number;
  constructor(private route: ActivatedRoute,
    private router: Router, private storeService: StoreServicesService, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modalService: NgbModal,
    public modal: ModalsService) { }

  ngOnInit() {
    this.page = 0;
    this.loader = false;
    this.getSchoollist();
    this.getStates();
  }
  /**get states api */
  getStates() {
    this.ApiServicesService.get('api/Common/GetState').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.states = parsedBody.Result;
        this.statesForsearch = parsedBody.Result;

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }

  /**Open Modals */
  open(content) {
    this.modalService.open(content, {
      size: 'lg'
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }
  getDismissReason(reason) {

  }
  closeModal() {
    this.modal.closeModal();

  }
  dismiss() {
    this.modal.dismissModal();
  }
  confirmModal() {
    this.modal.closeModal();

  }
  getSchoollist() {
    this.
      ApiServicesService.headerAppendedPost('api/College/FavCollegeList', this.storeService.getObject('profileID'))
      .then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.schoollist = parsedBody.Result;
        }
        else if (parsedBody.apiStatus == 1) {
          for (let i = 0; i < parsedBody.Result.ValidationErrors.length; i++) {
            this.notificationService.error(parsedBody.Result.ValidationErrors[i].FieldErrorMessage);
          }
        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Message);

        }
      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error('Unable To Get School List');
        } else {
          this.notificationService.error('Unable To Get School List');
        }
      });
  }
  onSelectionChangeInstate(instate, CollegeName, CollegeId) {
    this.stateValue = instate;
    this.selectedCollege = CollegeName;
    this.collegeId = CollegeId;

  }
  onSelectionChangeOutstate(outstate, CollegeName, CollegeId) {
    this.stateValue = outstate;
    this.selectedCollege = CollegeName;
    this.collegeId = CollegeId;
  }
  clickStateSearch(e) {
    this.page = e;
    // this.searchCollege(this.collegename,e);
    this.collegeByState(this.collegeNameByState, e);
    // this.getAllColleges(e)
  }
  submit() {
    this.closeModal();
    this.storeService.putObject('stateValue', this.stateValue);
    this.storeService.putObject('selectedCollege', this.selectedCollege);
    this.storeService.putObject('collegeId', this.collegeId);
  }
  public searchCollege(collegename, pageno) {
    this.loader = true;
    let req =
      {
        "searchValue": collegename,
        "PageNo": pageno,
        "PageSize": 20
      }

    let header = {
      "Content-Type": "application/json"

    }



    this.
      ApiServicesService.post('api/College/collegeSearchName', req, { headers: header })
      .then(res => {
        let parsedBody = JSON.parse(res._body);


        if (parsedBody.apiStatus == 0) {
          this.loader = false;
          this.tableShow = true;
          this.colleges = parsedBody.Result;
          this.stateBasedsearch = false;
          this.totalCollegeSerachedUsingName = parsedBody.Result[0].TotalCount;

        }



      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error('No College Found');
        } else {
          this.notificationService.error('No College Found');
        }
      });
  }
  collegeByState(collegeNameByState, pageno) {
    this.collegeNameByState = collegeNameByState;

    console.log("value" + collegeNameByState);
    let req =
      {

        "searchValue": collegeNameByState,
        "PageNo": pageno,
        "PageSize": 20

      }

    let header = {
      "Content-Type": "application/json"
    }

    console.log("headfer" + JSON.stringify(header));


    this.
      ApiServicesService.post('api/College/collegeSearchState', req, { headers: header })
      .then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.tableShow = true;
          this.colleges = parsedBody.Result;
          this.stateBasedsearch = true;
          this.fullsearch = false;
          this.nameBasedsearch = false;

          // this.itemsTotal = parsedBody.Result[0].TotalCount;
          this.totalCollegeSerachedUsingState = parsedBody.Result[0].TotalCount;

        }


      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error('Could not register');
        } else {
          this.notificationService.error('Could not register');
        }
      });
  }
  click(e) {
    this.page = e;
    this.getAllColleges(e)
  }
  getAllColleges(pageno) {
    this.loader = true;
    let req =
      {

        "PageNo": pageno,
        "PageSize": 20
      }

    let header = {
      "Content-Type": "application/json"

    }
    this.
      ApiServicesService.post('api/College/collegeSearchAll', req, { headers: header })
      .then(res => {
        let parsedBody = JSON.parse(res._body);


        if (parsedBody.apiStatus == 0) {
          this.loader = false;
          this.tableShow = true;
          this.collegesNotfound = false;
          this.colleges = parsedBody.Result;
          this.fullsearch = true;
          this.stateBasedsearch = false;
          this.nameBasedsearch = false;

          this.itemsTotal = parsedBody.Result[0].TotalCount / 20;
        }
      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error('Could not Find College');
        } else {
          this.notificationService.error('Could not Find College');
        }
      });
  }
}
