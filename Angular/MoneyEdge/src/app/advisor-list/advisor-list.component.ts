import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StoreServicesService } from '../services/store-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { ModalsService } from '../services/modals.service';
import { Router } from '@angular/router';
import { ApiServicesService } from '../services/api-services.service';

@Component({
  selector: 'app-advisor-list',
  templateUrl: './advisor-list.component.html',
  styleUrls: ['./advisor-list.component.scss']
})
export class AdvisorListComponent implements OnInit {
  advisorSearchText: any;
  txtBx: boolean;
  advisorsListData: any;
  page: number;
  SortColumnDateValue: any;
  text: any;
  loader: boolean;
  ascending: boolean;
  SortOrder: number;
  tableShow: boolean;
  sizeOfTable: any;
  constructor(private modalService: NgbModal, private storeService: StoreServicesService, private ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modal: ModalsService, private router: Router) {
    this.page = 1;
    this.SortColumnDateValue = 0;
  }
  ngOnInit() {
    this.advisorSearchText;
    this.loader = false;
    this.tableShow = false;
    this.txtBx = false;
    this.text = "";
    this.ascending = true;
    this.getAdvisorsList(this.page, this.text, this.SortColumnDateValue);
  }
  getAdvisorsList(pageno, text, SortColumnDateValue) {
    this.loader = true;
    this.text = text;
    this.page = pageno;
    this.SortColumnDateValue = SortColumnDateValue;
    if (this.ascending) {
      this.SortOrder = 0
    }
    else {
      this.SortOrder = 1;
    }
    let req =
      {
        "Collection": {
          "MangerID": this.storeService.getObject("advisorId"),
          "PageNo": this.page,
          "PageSize": 5,
          "SortColumn": this.SortColumnDateValue,
          "SortOrder": this.SortOrder,
          "SearchText": this.text
        }
      }
    this.ApiServicesService.headerAppendedPost('api/Advisor/GetAdvisorList', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {

        if (parsedBody.Result.Collection) {
          this.sizeOfTable = parsedBody.Result.Collection.TotalCount;
          if (this.sizeOfTable > 0)
          {
            this.tableShow = true;
          }
        }
        this.advisorsListData = parsedBody.Result.advisors;
        this.loader = false;
      }
    })
  }
  showSearchTxt() {
    this.txtBx = true;
  }
  searchClient(text) {
    this.getAdvisorsList(this.page, text, this.SortColumnDateValue);
  }
  sortedBasedSearch(SortColumnDateValue) {
    this.ascending = !this.ascending;
    this.getAdvisorsList(this.page, this.text, SortColumnDateValue)
  }
  editAdvisor(advisorId) {
    // window.event.stopPropagation();
    this.router.navigate(['/app-add-advisor'], { queryParams: { id: advisorId }});
  }
  searchadvisor(text) {
    this.getAdvisorsList(this.page,text, this.SortColumnDateValue);
  }
  paginatedSearch(e) {
    this.getAdvisorsList(e, this.text, this.SortColumnDateValue);
  }
}
