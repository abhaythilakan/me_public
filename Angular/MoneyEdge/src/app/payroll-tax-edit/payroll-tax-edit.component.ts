import { Component, OnInit } from '@angular/core';
import { ModalsService } from '../services/modals.service';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';

@Component({
  selector: 'app-payroll-tax-edit',
  templateUrl: './payroll-tax-edit.component.html',
  styleUrls: ['./payroll-tax-edit.component.scss']
})
export class PayrollTaxEditComponent implements OnInit {
  changedPayperiod: any;
  PayPeriods: any;
  taxTypeID: any;
  lengthOfchildmodel: any;
  payTaxmodal: any;
  // payTaxmodal: any;
  n: number;

  changedPayrollTax: any;
  selectedTax: { "VALUE": number; "TEXT": string; };
  PayrollTaxes: any;

  constructor(public modal: ModalsService, private storeService: StoreServicesService, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService) { }
  public payTax = {
    "amount": "",
    "notes": "",
    "isMouseHover": ""
  }
  ngOnInit() {
    this.changedPayrollTax = "";
    this.getTaxType();
    // this.selectedTax={"VALUE":1,"TEXT":"Combined Payroll"}
    this.getTaxDetails();
    this.getPayperiod();
  }
  payPeriodChanged(val: any) {
    console.log("state changed" + val);
    this.changedPayperiod = val;
  }
  getPayperiod() {
    this.ApiServicesService.headerAppendedGet('api/Common/GetPayperiod').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.PayPeriods = parsedBody.Result;

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }
  closeModal() {
    this.modal.closeModal();
    this.storeService.putObject("saveTax", false);
  }
  confirmModal(item) {
    this.storeService.putObject("saveTax", true);
    this.modal.closeModal();

  }
  /**get tax type api */
  getTaxType() {
    this.ApiServicesService.headerAppendedGet('api/Common/GetFutureTaxes').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.PayrollTaxes = parsedBody.Result;

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }
  getTaxDetails() {
    this.ApiServicesService.headerAppendedPost('api/IncomeDetails/GetTaxes', this.storeService.getObject("incomeID")).then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.payTaxmodal = parsedBody.Result;
        console.log("payTaxmodal" + JSON.stringify(this.payTaxmodal));

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }

  taxChanged(val: any) {
    console.log("state changed" + val);
    this.changedPayrollTax = val;
  }
  saveTaxDetails(payTax) {
console.log("paytax data"+JSON.stringify(payTax));
    if (this.changedPayrollTax) {
      this.taxTypeID = this.changedPayrollTax;


    }
    else {
      this.taxTypeID = payTax.taxTypeID

    }
    let req = [{
      "taxDetailID": payTax.taxDetailID,
      "incomeID": this.storeService.getObject("incomeID"),
      "taxTypeID": this.taxTypeID,
      "payingTax": payTax.payingTax,
      "note": payTax.note,
      "isMouseHover": payTax.isMouseHover,
      "isActive": payTax.isActive,
      "isOther": payTax.isOther,
      "profileID": this.storeService.getObject("profileID"),
      "paytype":payTax.paytype
    }]
    console.log("paytax" + JSON.stringify(req));

    // this.ApiServicesService.headerAppendedPost('api/IncomeDetails/saveTaxeDetails', req).then(res => {
      this.ApiServicesService.headerAppendedPost('api/IncomeDetails/savepayrollTaxeDetails', req).then(res => {

      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.notificationService.success("Tax Details Saved Successfully");
          this.storeService.putObject("saveTax", true);
          this.modal.closeModal();
        }
        else if (parsedBody.apiStatus == 1) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
        else {
          this.notificationService.error("Error occured while saving taxes");
        }

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }

  addMore() {
    console.log("childID" + JSON.stringify(this.payTaxmodal));
    // this.childDateOfBirth=child.dob.month+"/"+child.dob.day+"/"+child.dob.year;
    // this.childDateOfBirth=this.childModel.dob;
    this.lengthOfchildmodel = this.payTaxmodal.length;
    if (this.payTaxmodal) {
      this.n = this.lengthOfchildmodel - 1;
    }
    else {
      this.n = 0;
    }
    this.payTaxmodal.push(
      {
        "taxDetailID": "",
        "incomeID": "",
        "taxTypeID": "",
        "amount": "",
        "note": "",
        "isMouseHover": "",
        "isActive": "",
        "isOther": "",
        "createdDate": "",
        "updatedDate": "",
        "updatedBy": "",
        "profileID": ""
      }
    )
  }
  remove(payTax) {
    console.log("remove" + JSON.stringify(payTax));
    this.ApiServicesService.headerAppendedPost('api/IncomeDetails/DeleteTaxes', payTax.taxDetailID).then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.notificationService.success("Tax Removed Successfully");
          this.getTaxDetails();
          this.modal.closeModal();
        }
        else {
          this.notificationService.error("Error occured while saving taxes");
        }

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }
}
