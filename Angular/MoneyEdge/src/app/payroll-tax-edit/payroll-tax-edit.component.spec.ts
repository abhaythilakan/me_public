import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollTaxEditComponent } from './payroll-tax-edit.component';

describe('PayrollTaxEditComponent', () => {
  let component: PayrollTaxEditComponent;
  let fixture: ComponentFixture<PayrollTaxEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollTaxEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollTaxEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
