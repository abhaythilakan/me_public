import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActionPointsPageComponent } from './action-points-page.component';

describe('ActionPointsPageComponent', () => {
  let component: ActionPointsPageComponent;
  let fixture: ComponentFixture<ActionPointsPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActionPointsPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActionPointsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
