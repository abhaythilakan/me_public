import { Component, OnInit, ViewChild } from '@angular/core';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalsService } from '../services/modals.service';
import { DatepickerOptions } from 'ng2-datepicker';
import * as enLocale from 'date-fns/locale/en';

@Component({
  selector: 'app-action-points-page',
  templateUrl: './action-points-page.component.html',
  styleUrls: ['./action-points-page.component.scss']
})
export class ActionPointsPageComponent implements OnInit {
  completedactionPlanBased: any;
  @ViewChild('acc') acc;
  upcomingactionPlanBased: any;
  pastactionPlanBased: any;
  actionPlanBased: any;
  choosedPeriod: any = "--Select--";
  notificationTypes: any;
  assignTypes: any;
  options: DatepickerOptions = {
    locale: enLocale,
    displayFormat: 'MM-DD-YYYY',
    barTitleFormat: 'MMMM YYYY',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    // minDate: new Date(Date.now()), // Minimal selectable date
    // maxDate: new Date(Date.now()),  // Maximal selectable date
    //barTitleIfEmpty: 'Click to select a date'
  };
  showStatus = [];
  public actionPoints = [
    { "actionID": null, "actionName": null }
  ]
  public favoriteSeason = { "actionID": null, "actionName": null }
  public actionPlanModal =
    {
      "profileID": this.storeService.getObject('profileID'),
      "actionID": null,
      "actionNameID": null,
      "actionName": null,
      "dueDate": null,
      "assignedTo": null,
      "notificationTypeID": null,
      "comment": null,
      "actionStatus": null,
      "otherActions":
        {
          "otherID": null,
          "otherPersonName": null,
          "otherEmailID": null
        }
    }

  actionLinkShow: boolean;
  constructor(private storeService: StoreServicesService, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService) { }

  ngOnInit() {
    this.actionPlanModal.dueDate = new Date(Date.now());
    this.actionLinkShow = false;
    this.choosedPeriod;
    this.listActionPoints();
    this.getAssignTypes();
    this.getNotification();
    this.getAllActions();
    this.pastDueActions();
    this.upComingActions();
  }
  actionLinks() {
    this.actionLinkShow = !this.actionLinkShow;
  }
  listActionPoints() {
    this.ApiServicesService.headerAppendedGet('api/actionPlan/predefineActions').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.actionPoints = parsedBody.Result;
      }
    })
  }
  getAssignTypes() {
    this.ApiServicesService.headerAppendedPost('api/actionPlan/assignedToDDL', this.storeService.getObject('profileID')).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.assignTypes = parsedBody.Result;
      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
    })
  }
  getNotification() {
    this.ApiServicesService.headerAppendedGet('api/Common/ActionNotifyType').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.notificationTypes = parsedBody.Result;
      }
    })
  }
  saveActionPlan() {
    this.actionPlanModal.actionNameID = this.favoriteSeason.actionID;
    this.actionPlanModal.actionName = this.favoriteSeason.actionName;
    if (this.actionPlanModal.assignedTo == 0) {
      this.actionPlanModal.otherActions.otherID = this.actionPlanModal.assignedTo;
    }
    console.log("actionPlanModal" + JSON.stringify(this.actionPlanModal));
    this.ApiServicesService.headerAppendedPost('api/actionPlan/saveAction', this.actionPlanModal).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        // this.assignTypes = parsedBody.Result;
        this.notificationService.success("Action Plan Saved Successfully,Email Sent");
        this.actionPlanModal = {
          "profileID": this.storeService.getObject('profileID'),
          "actionID": null,
          "actionNameID": null,
          "actionName": null,
          "dueDate": null,
          "assignedTo": null,
          "notificationTypeID": null,
          "comment": null,
          "actionStatus": null,
          "otherActions":
            {
              "otherID": null,
              "otherPersonName": null,
              "otherEmailID": null
            }
        }
        this.favoriteSeason.actionName = null;
        this.actionPlanModal.dueDate = new Date(Date.now());

        this.listActionPoints();
      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
    })
  }
  chooseOption(val: any) {
    if (val == 0) {
      this.ApiServicesService.headerAppendedPost('api/actionPlan/all_Today', this.storeService.getObject('profileID')).then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.actionPlanBased = parsedBody.Result;
        }
        else if (parsedBody.apiStatus == 1) {
          for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
            this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
          }
        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Result.Message);

        }
      })
    }
    else if (val == 1) {
      this.ApiServicesService.headerAppendedPost('api/actionPlan/all_ThisMonth', this.storeService.getObject('profileID')).then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.actionPlanBased = parsedBody.Result;
        }
        else if (parsedBody.apiStatus == 1) {
          for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
            this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
          }
        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Result.Message);

        }
      })
    }
    else if (val == 2) {
      this.ApiServicesService.headerAppendedPost('api/actionPlan/GetAllActions', this.storeService.getObject('profileID')).then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.actionPlanBased = parsedBody.Result;
        }
        else if (parsedBody.apiStatus == 1) {
          for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
            this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
          }
        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Result.Message);

        }
      })
    }
  }
  /**For Getting the all the action points */
  getAllActions() {
    this.ApiServicesService.headerAppendedPost('api/actionPlan/GetAllActions', this.storeService.getObject('profileID')).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.actionPlanBased = parsedBody.Result;
        this.choosedPeriod = 2;
      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
    })
  }
  /**For Getting the all the action points that are still not completed after due date*/

  pastDueActions() {
    this.ApiServicesService.headerAppendedPost('api/actionPlan/pasttDueActions', this.storeService.getObject('profileID')).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.pastactionPlanBased = parsedBody.Result;
      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
    })
  }
  /**For Getting  all the action points can occur in future*/

  upComingActions() {
    this.ApiServicesService.headerAppendedPost('api/actionPlan/upcomingActions', this.storeService.getObject('profileID')).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.upcomingactionPlanBased = parsedBody.Result;
      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
    })
  }
  completedActions() {
    this.ApiServicesService.headerAppendedPost('api/actionPlan/completedActions', this.storeService.getObject('profileID')).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.completedactionPlanBased = parsedBody.Result;
      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
    })
  }
  actionPlanCompleted(actionID) {
    this.ApiServicesService.headerAppendedPost('api/actionPlan/markCompleted', actionID).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.getAllActions();
        this.pastDueActions();
        this.upComingActions();
        this.notificationService.success("Action Plan Completed Successfully");

      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
    })
  }
  actionPlanProgress(actionID) {
    this.ApiServicesService.headerAppendedPost('api/actionPlan/markInProgress', actionID).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.getAllActions();
        this.pastDueActions();
        this.upComingActions();
        this.notificationService.success("Action Plan Progressing");
      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
    })
  }
  actionPlanReschedule(actionID) {
    this.ApiServicesService.headerAppendedPost('api/actionPlan/getOnReshedule', actionID).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.getAllActions();
        this.pastDueActions();
        this.upComingActions();
        this.actionPlanModal = parsedBody.Result;
        this.favoriteSeason.actionName = this.actionPlanModal.actionName;
        this.notificationService.success("Reschdule Action Plan");
      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
    })
  }
}
