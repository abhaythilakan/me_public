import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherTaxEditComponent } from './other-tax-edit.component';

describe('OtherTaxEditComponent', () => {
  let component: OtherTaxEditComponent;
  let fixture: ComponentFixture<OtherTaxEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherTaxEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherTaxEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
