import { Component, OnInit } from '@angular/core';
import { ModalsService } from '../services/modals.service';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';

@Component({
  selector: 'app-other-tax-edit',
  templateUrl: './other-tax-edit.component.html',
  styleUrls: ['./other-tax-edit.component.scss']
})
export class OtherTaxEditComponent implements OnInit {
  changedPayperiod: any;
  PayPeriods: any;
  OtherTaxTypes: any;
  otherTaxModal: any;


  constructor(public modal: ModalsService, private storeService: StoreServicesService, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService) { }

  ngOnInit() {
    this.otherTaxModal=this.storeService.getObject('otherTax');
    console.log("otherTaxModal"+JSON.stringify(this.otherTaxModal));
    this.getOtherTaxTypes();
    this.getPayperiod();
  }
  payPeriodChanged(val: any) {
    console.log("state changed" + val);
    this.changedPayperiod = val;
  }
  getPayperiod() {
    this.ApiServicesService.headerAppendedGet('api/Common/PayTaxTpeDDL').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.PayPeriods = parsedBody.Result;

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }
  closeModal() {
    this.modal.closeModal();
  }
  confirmModal(item) {
    this.modal.closeModal();

  }
    /**Get Other Taxes Types  */
    getOtherTaxTypes() {
      this.ApiServicesService.headerAppendedGet('api/Common/GetOtherTaxes').then(res => {
        if (res && res._body && !res._body.target) {
          let parsedBody = JSON.parse(res._body);
          this.OtherTaxTypes = parsedBody.Result;
  
        }
      })
        .catch(err => {
          this.notificationService.error(err);
  
        })
    }
    /**Update Other Tax Values */
    UpdateOtherTax(){
      // if(this.otherTaxModal.taxTypeID==""){
      //   this.otherTaxModal.taxTypeID= this.OtherTaxTypes[0].VALUE;
      // }{
      //   this.otherTaxModal.taxTypeID=this.otherTaxModal.taxTypeID;
      // }
      let req = [{
        "taxDetailID":this.otherTaxModal.taxDetailID,
        "profileID": this.storeService.getObject('profileID'),
        "taxTypeID": this.otherTaxModal.taxTypeID,
        "payingTax": this.otherTaxModal.payingTax,
        "note": this.otherTaxModal.note,
        "isMouseHover": this.otherTaxModal.isMouseHover,
        "paytype":this.otherTaxModal.paytype
      }]
      this.ApiServicesService.headerAppendedPost('api/IncomeDetails/saveOtherTaxeDetails',req).then(res => {
        if (res && res._body && !res._body.target) {
          let parsedBody = JSON.parse(res._body);
          if (parsedBody.apiStatus == 0) {
            this.notificationService.success("Other Tax Details Saved Successfully");
            this.closeModal();
           
          }
          else if (parsedBody.apiStatus == 2) {
            this.notificationService.error(parsedBody.Message);
          }
        }
      })
        .catch(err => {
          this.notificationService.error(err);
    
        })
    }
    otherTaxChangeType(val:any){

    }
}
