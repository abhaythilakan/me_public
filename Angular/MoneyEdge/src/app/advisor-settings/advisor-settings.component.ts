import { Component, OnInit, Input, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { StoreServicesService } from '../services/store-services.service';
import { Data } from '../providers/data/data.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { NgbModal, NgbModalRef, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import * as _ from "lodash";
// import { Http,Headers,Response } from '@angular/http';
import { Http, Headers, Request, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ModalsService } from '../services/modals.service';
import { ConfirmationComponent } from '../confirmation/confirmation.component';
import { error } from 'util';
import * as $ from "jquery";
@Component({
  selector: 'app-advisor-settings',

  templateUrl: './advisor-settings.component.html',
  styleUrls: ['./advisor-settings.component.scss']
})
export class AdvisorSettingsComponent implements OnInit {
  changedDisclaimerval: any;
  disclaimer: any;
  settingslabel: string;
  chart: any;
  charts = [
    'Pie chart',
    'Bar Graph'
  ];
  income: any;
  incomeperiod = [
    '$ xxx - 3Month household income',
    '$ xxx - 6Month household income'
  ];
  disabled: boolean;
  checked: boolean;
  pagination: boolean;
  noClg: boolean;
  emailIdExist: boolean;
  _timeout: any;
  admin: boolean;
  client: boolean;
  advisor: boolean;
  zipLength: number;
  changedDropdownglobalstate: any;
  chnagedglobalstate: any;
  contactState: any;
  globalstate: any;
  publicAccessExpress: any;
  publicAccessSettingsnw: any;
  imgPath: any;
  arr = [];
  arrTwo = [];
  arrIndexTwo = {};
  arrIndex = {};
  branchState: any;
  changedBranchState: any;
  discalimer: any;
  changedDisclaimer: any;
  lifeExpectencies: any;
  specificnoofyearsBox: boolean;
  changedDropdownyearDeath: any;
  timeZone: any;
  currency: any;
  yearAfterDeath: string;
  changedTimezone: any;
  changedState: any;
  changedDropdowncurrency: any;
  isThreeMontha: boolean;
  isThreeMonth: boolean;
  advContact: any;
  advisorInfo: any;
  userinfo: any;
  chnagedState: any;
  ExpressReportDefaults: any;
  AdvancedReportDefaults: any;
  nameBasedsearch: boolean;
  fullsearch: boolean;
  stateBasedsearch: boolean;
  totalCollegeSerachedUsingState: any;
  totalCollegeSerachedUsingName: any;
  modalRef: any;
  graphvalue: number;
  defaultstate: any;
  currenyValue: any;
  timezoneId: any;
  disclaimerId: any;
  userImage: any;
  finalReports: any;
  expressReports: any;
  advancedReports: any;
  reports: { "TEXT": string; "ID": string; }[];
  selectedDeviceObjTwo: any;
  image: any;
  status: any;
  collegeNameByState: any;


  stateSearch = { "VALUE": null, "TEXT": null }

  favoriteColleges: any;
  favouriteCollege: boolean;
  MyToken: any;
  handleError(arg0: any): any {
    throw new Error("Method not implemented.");
  }
  errorMessage: any;
  statusMessage: any;
  page: number;

  collegename: any;
  totalItems: any;

  itemsTotal: any;
  sortOrder: any;
  collegesNotfound: boolean;
  loader: boolean;
  disclaimerDatavalue: any;
  selectedDeviceObj: any;
  publicDeals: any[] = [];
  publicAccessSettings: any[] = [];
  publicAccessSettingsExpress: any[] = [];
  collegeFavouriteList: any[];
  statesForsearch: any;
  tableShow: boolean;
  colleges: any;
  token: any;
  selectedElement: any;
  monthsArray: { "id": number; "name": string; }[];
  months: string[];
  closeResult: string;
  currencies: any;
  timezones: any;
  disclaimers: any;
  zip: string;
  states: any;
  // advisorModel: any;
  next: boolean;
  advisorData: any;
  defaultSelectedstate = { "VALUE": "0", "TEXT": "--Select--" }
  private advisorModel: any =
    {
      "advisorInfo": {
        "advisorId": "",
        "firstName": "",
        "lastName": "",
        "titleA": "",
        "titleB": "",
        "managerId": "",
        "numPlanner": "",
        "disclaimer": "",
        "couponCode": "",
        "capitalChoiceCode": "",
        "setUpFee": "",
        "initMonthlyFee": "",
        "monthlyFee": "",
        "finraReg": "",
        "secReg": "",
        "brokerName": "",
        "riaName": "",
        "createdDate": "",
        "designation": "",
        "imgPath": ""
      },
      "advglobal": {
        "defultID": "",
        "profileID": "",
        "clientID": "",
        "advisorID": "",
        "timeZone": "",
        "currency": "",
        "defaultstate": ""
      },
      "advRetirement": {
        "RetirementDefaultID": "",
        "profileID": "",
        "clientID": "",
        "advisorID": "",
        "retireAge": "",
        "lifeExpectency": "",
        "preRetReturnTax": "",
        "retTax": "",
        "retReturnTax": "",
        "monthIncome": "",
        "inflation": ""
      },
      "advIncome": {
        "defaultID": "",
        "profileID": "",
        "clientID": "",
        "advisorID": "",
        "primaryInflation": "",
        "spouseInflation": "",
        "inflation": ""
      },
      "advDebt": {
        "defaultID": "",
        "profileID": "",
        "clientID": "",
        "advisorID": "",
        "minPay": "",
        "unpaidPercentage": "",
        "inflation": ""
      },
      "advEmergency": {
        "isBarChart": "",
        "isSixMonth": "",
        "defaultID": "",
        "profileID": "",
        "clientID": "",
        "advisorID": "",
        "emergencyFundValue": "",
        "rateA": "",
        "rateB": "",
        "rateC": "",
        "isPieChart": "",
        "isThreeMonth": "",
        "inflation": ""
      },
      "advInsurance": {
        "defaultID": "",
        "profileID": "",
        "clientID": "",
        "advisorID": "",
        "cost": "",
        "monthlyIncome": "",
        "yearAfterDeath": "",
        "specificnoofyears": "",
        "retAsset": "",
        "otherRetAsset": "",
        "cashAccounts": "",
        "spouseRetAsset": "",
        "nonRetAsset": "",
        "inflation": ""
      },
      "advEducation": {
        "defaultID": "",
        "profileID": "",
        "clientID": "",
        "advisorID": "",
        "collegeStartMonth": "",
        "tutionFeeInflation": "",
        "collegeAge": "",
        "collageYears": "",
        "parentPay": "",
        "rateA": "",
        "rateB": "",
        "rateC": ""
      },
      "advBranch": {
        "bId": "",
        "advisorId": "",
        "bFirstName": "",
        "bLastName": "",
        "bAddress": "",
        "bCity": "",
        "bState": "",
        "bZip": "",
        "bPhone": "",
        "bCompany": ""
      },
      "advContact": {
        "contactId": "",
        "advisorId": "",
        "address": "",
        "city": "",
        "state": "",
        "zip": "",
        "telephone": "",
        "cellphone": ""
      },
      "UserInfo": {
        "userId": "",
        "userName": "",
        "password": "",
        "emailPrimary": "",
        "emailPrimaryValid": "",
        "emailSecondary": "",
        "emailSecondaryValid": "",
        "advisorId": "",
        "clientId": "",
        "isActive": "",
        "trialEndDate": "",
        "expDate": "",
        "createdDate": "",
        "isAdmin": "",
        "isManager": "",
        "isSuperUser": "",
        "isLicensed": "",
        "hashCode": "",
        "requestedForgot": "",
        "isTrial": "",
        "isNonTrial": "",
        "profileId": ""
      },
      "AccesSettings": {
        "AdvancedReportDefaults": [],
        "ExpressReportDefaults": [],
        "apiStatus": ""
      },
      "specificnoofyears": ""
    }
  constructor(private route: ActivatedRoute,
    private router: Router, private storeService: StoreServicesService, private data: Data, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modalService: NgbModal, public http: Http,
    public modal: ModalsService, public lc: NgZone
  ) {


    this.loader = false;
    this.MyToken = this.storeService.getObject("token");
    /**Get Advisor Details */

    this.ApiServicesService.headerAppendedGet('api/Advisor/AdvisorDetails').then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.advisorModel = parsedBody.Result;
        console.log("bb"+JSON.stringify( this.advisorModel));
        if (this.advisorModel.advInsurance.yearAfterDeath == 3) {
          this.specificnoofyearsBox = true;
        }
        if (this.advisorModel.advEmergency.isPieChart == true) {
          this.chart = 'Pie chart';
        }
        else {
          this.chart = 'Bar Graph';
        }
        if (this.advisorModel.advEmergency.isThreeMonth == true) {
          this.income = "$ xxx - 3Month household income";
        }
        else {
          this.income = "$ xxx - 6Month household income";
        }
        this.storeService.putObject("email_from_login", this.advisorModel.UserInfo.emailPrimary);

        if (this.advisorModel.advisorInfo.imgPath == "") {
          this.advisorModel.advisorInfo.imgPath = "assets/img/user.png";
        }
        else {
          this.advisorModel.advisorInfo.imgPath = this.advisorModel.advisorInfo.imgPath;
        }
        if (this.advisorModel.advisorInfo.disclaimer) {
          this.advisorModel.advisorInfo.disclaimer = +this.advisorModel.advisorInfo.disclaimer;
          this.disclaimerMethod(this.advisorModel.advisorInfo.disclaimer);

        }
      }
    })
      .catch(error => {
        this.notificationService.error("error while getting advisor details" + error);
      })



  }

  ngOnInit() {
    this.settingslabel = "Advisor Settings"
    this.stateSearch = { "VALUE": "0", "TEXT": "--Select--" }
    this.checked = true;
    this.disabled = true;
    this.page = 0;
    this.noClg = false;
    this.pagination = false;
    this.nameBasedsearch = false;
    this.fullsearch = false;
    this.stateBasedsearch = false;
    this.totalCollegeSerachedUsingName = 0;
    this.itemsTotal = 0;
    this.totalCollegeSerachedUsingState = 0;
    this.next = false;
    this.tableShow = false;
    if (this.storeService.getObject('Role') == "Advisor") {
      console.log("advisor");
      this.advisor = true;
      this.client = false;
      this.admin = false;
    }
    else if (this.storeService.getObject('Role') == "Client") {
      this.client = true;
      this.advisor = false;
      this.admin = false;
    }
    else if (this.storeService.getObject('Role') == "Admin") {
      this.admin = true;
      this.client = false;
      this.advisor = false;

    }
    this.zipLength = 5;
    this.changedDropdownyearDeath = "";
    this.changedDropdowncurrency = "";
    this.changedDropdownglobalstate = "";
    this.changedState = "";
    this.changedTimezone = "";
    this.changedDisclaimer = "";
    this.changedBranchState = "";
    this.reports = [
      {
        "TEXT": "Advanced Report",
        "ID": "1"
      },
      {
        "TEXT": "Express Report",
        "ID": "2"
      }
    ]
    this.favouriteCollege = false;
    this.showFavouriteColleges();

    /**get states api */
    /**get states api */
    this.ApiServicesService.get('api/Common/GetState').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.states = parsedBody.Result;
        this.statesForsearch = parsedBody.Result;

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
    /**get life expectancy */
    this.ApiServicesService.get('api/Common/GetIncomeAfterDeath').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.lifeExpectencies = parsedBody.Result;
      }
    })
    /** GetDisclaimer api */

    this.ApiServicesService.get('api/Common/GetDisclaimer').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.disclaimers = parsedBody.Result;

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
    /**get life expectancy */
    this.ApiServicesService.get('api/Common/GetIncomeAfterDeath').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.lifeExpectencies = parsedBody.Result;
      }
    })

    /** get timezone api */

    this.ApiServicesService.get('api/Common/GetTimeZone').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.timezones = parsedBody.Result;

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
    /** get timezone api */
    this.ApiServicesService.get('api/Common/GetCurrencies').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.currencies = parsedBody.Result;

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })


    /*setting months */

    this.monthsArray = [
      {
        "id": 1,
        "name": "Janaury"
      },
      {
        "id": 2,
        "name": "February"
      }, {
        "id": 3,
        "name": "March"
      }, {
        "id": 4,
        "name": "April"
      }, {
        "id": 5,
        "name": "May"
      }, {
        "id": 6,
        "name": "June"
      }, {
        "id": 7,
        "name": "July"
      }, {
        "id": 8,
        "name": "August"
      }, {
        "id": 9,
        "name": "September"
      }, {
        "id": 10,
        "name": "October"
      }, {
        "id": 11,
        "name": "November"
      }, {
        "id": 12,
        "name": "December"
      },
    ]

    for (let i = 0; i <= this.monthsArray.length; i++) {
      this.selectedElement = {
        "name": "September",
        "id": 9
      };
    }
    console.log("selc" + this.selectedElement.name);

  }
  nextStep() {
    this.next = true;
  }
  backToStep() {
    this.next = false;
  }

  open(content) {
    this.modalRef = this.modalService.open(content, {
      size: 'lg'
    });
    this.modalRef.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
    // this.modalRef = this.modalService.open(content, {
    //   size: 'lg'
    // }).result.then((result) => {
    //   this.closeResult = `Closed with: ${result}`;
    // }, (reason) => {
    //   this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    // });
  }
  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  dismiss() {
    this.colleges = [];
    this.next = false;
    this.tableShow = false;
    this.pagination = false;
    this.modalRef.dismiss();
  }
  public searchCollege(collegename, pageno) {
    this.loader = true;
    if (pageno) {
      this.page = pageno;
    }

    let req =
      {
        "searchValue": collegename,
        "PageNo": this.page,
        "PageSize": 20
      }

    let header = {
      "Content-Type": "application/json"

    }
    this.
      ApiServicesService.post('api/College/collegeSearchName', req, { headers: header })
      .then(res => {
        let parsedBody = JSON.parse(res._body);


        if (parsedBody.apiStatus == 0 && parsedBody.Result) {
          if (parsedBody.Result.length > 0) {
            this.noClg = false;
            this.loader = false;
            this.tableShow = true;
            this.collegesNotfound = false;
            this.colleges = parsedBody.Result;
            this.nameBasedsearch = true;
            this.pagination = true;
            this.fullsearch = false;
            this.stateBasedsearch = false;
            //  this.itemsTotal = parsedBody.Result[0].TotalCount;
            this.totalCollegeSerachedUsingName = parsedBody.Result[0].TotalCount;
          }
          else if (parsedBody.Result.length <= 0) {
            this.noClg = true;
          }

        }
        else if (parsedBody.apiStatus == 1) {
          for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
            if (parsedBody.Result.ValidationErrors[0].FieldErrorMessage) {
              this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
            }
            else {
              this.notificationService.error("Error Occured");
            }
          }
        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Result.Message);

        }
      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error("Error Occured");
        } else {
          this.notificationService.error("Error Occured");
        }
      });
  }

  getAllColleges(pageno) {
    this.loader = true;
    if (pageno) {
      this.page = pageno;
    }
    let req =
      {

        "PageNo": this.page,
        "PageSize": 20
      }

    let header = {
      "Content-Type": "application/json"

    }
    this.
      ApiServicesService.post('api/College/collegeSearchAll', req, { headers: header })
      .then(res => {
        let parsedBody = JSON.parse(res._body);


        if (parsedBody.apiStatus == 0 && parsedBody.Result) {
          if (parsedBody.Result.length > 0) {
            this.pagination = true;
            this.noClg = false;
            this.loader = false;
            this.tableShow = true;
            this.collegesNotfound = false;
            this.colleges = parsedBody.Result;
            this.fullsearch = true;
            this.stateBasedsearch = false;
            this.nameBasedsearch = false;

            this.itemsTotal = parsedBody.Result[0].TotalCount;
          }
          else if (parsedBody.Result.length <= 0) {
            this.noClg = true;
          }
        }
        else if (parsedBody.apiStatus == 1) {
          for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
            if (parsedBody.Result.ValidationErrors[0].FieldErrorMessage) {
              this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
            }
            else {
              this.notificationService.error("Error Occured");
            }
          }
        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Result.Message);
        }
      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error("Error Occured");
        } else {
          this.notificationService.error("Error Occured");
        }
      });
  }
  collegeByState(collegeNameByState, pageno) {
    this.collegeNameByState = collegeNameByState;
    if (pageno) {
      this.page = pageno;
    }
    let req =
      {
        "searchValue": collegeNameByState,
        "PageNo": this.page,
        "PageSize": 20
      }

    let header = {
      "Content-Type": "application/json"
    }
    this.
      ApiServicesService.post('api/College/collegeSearchState', req, { headers: header })
      .then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0 && parsedBody.Result) {
          if (parsedBody.Result.length > 0) {
            this.noClg = false;
            this.pagination = true;
            this.tableShow = true;
            this.colleges = parsedBody.Result;
            this.stateBasedsearch = true;
            this.fullsearch = false;
            this.nameBasedsearch = false;
            this.totalCollegeSerachedUsingState = parsedBody.Result[0].TotalCount;
          }
          else if (parsedBody.Result.length <= 0) {
            this.noClg = true;
          }
        }

        else if (parsedBody.apiStatus == 1) {
          for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
            if (parsedBody.Result.ValidationErrors[0].FieldErrorMessage) {
              this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
            }
            else {
              this.notificationService.error("Error Occured");
            }
          }
        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Result.Message);

        }

      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error("Error Occured");
        } else {
          this.notificationService.error("Error Occured");
        }
      });
  }

  addprop(e, college) {

    if (e.target.checked) {

      let collegeFavouriteList = []
      this.publicDeals.push(college.collegeID)         // this.collegeFavouriteList.push(college);
    }

  }


  addToBranch(e, college) {

    if (e.target.checked) {
      this.advisorModel.advBranch.address = this.advisorModel.advContact.address;
      this.advisorModel.advBranch.bCity = this.advisorModel.advContact.city;
      this.advisorModel.advBranch.bZip = this.advisorModel.advContact.zip;
      if (this.changedState != "") {
        this.advisorModel.advBranch.bState = this.changedState
      }
      else {
        this.advisorModel.advBranch.bState = this.advisorModel.advContact.state;
      }
    }
    else {
      this.advisorModel.advBranch.address = null;
      this.advisorModel.advBranch.bCity = null;
      this.advisorModel.advBranch.bState = null;
      this.advisorModel.advBranch.bZip = null;
    }
  }
  addToFavColleges() {
    var body = {
      "advisorId": this.storeService.getObject("advisorId"),
      "CollegeID": this.publicDeals

    };
    this.
      ApiServicesService.headerAppendedPost('api/College/saveFaveColleges', body)
      .then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.showFavouriteColleges();
          this.dismiss();

        }
      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error('Could not added to  the favourite college list');
        } else {
          this.notificationService.error('Could not added to  the favourite college list');

        }
      });
  }
  onChangeObj(newObj) {

    this.selectedDeviceObj = newObj;

    let headerType = {
      "Content-Type": "application/json"
      // "Authorization":"bearer"+" "+this.storeService.getObject("token")
    }
    this.
      ApiServicesService.post('api/Advisor/getDisclaimerDetails', this.selectedDeviceObj, { headers: headerType })
      .then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.disclaimerDatavalue = parsedBody.Result;

        }


      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error("Error Occured");
        } else {
          this.notificationService.error("Error Occured");
        }
      });
  }
  click(e) {
    this.page = e;
    this.getAllColleges(e)
  }
  clickNameBasedSeach(e) {
    this.page = e;
    this.searchCollege(this.collegename, e);
  }
  clickStateSearch(e) {
    this.page = e;
    this.collegeByState(this.collegeNameByState, e);
  }
  showFavouriteColleges() {
    /**Load Favourite college list */

    this.ApiServicesService.headerAppendedPost('api/College/FavCollegeList', '').then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.favouriteCollege = true;
        console.log("body of favo list" + JSON.stringify(parsedBody.Result));
        this.favoriteColleges = parsedBody.Result;
        this.storeService.putObject("confirmDelete", false);
      }
    })

  }

  changeListener($event): void {
    this.readThis($event.target);
  }

  readThis(inputValue: any): void {
    var file: File = inputValue.files[0];
    var myReader: FileReader = new FileReader();

    myReader.onloadend = (e) => {
      this.image = myReader.result;
      let header = {
        "Content-Type": "application/json"
      }
      let req = {
        "id": this.storeService.getObject("advisorId"),
        "imageString": this.image
      }
      console.log("image value" + this.image)
      this.
        ApiServicesService.post('api/Advisor/imageUpload', req, { headers: header })
        .then(res => {
          let parsedBody = JSON.parse(res._body);
          if (parsedBody.apiStatus == 0) {
            // this.userImage = parsedBody.Result;
            this.advisorModel.advisorInfo.imgPath = parsedBody.Result;
          }
          if (parsedBody.apiStatus == 2) {
            this.notificationService.error("Not a valid format,Accepts Only .jpg,.png formats");
          }
        })
        .catch(err => {
          if (err && err._body && !err._body.target) {
            let parsedBody = JSON.parse(err._body);
            if (parsedBody.error && parsedBody.message)
              this.notificationService.error(parsedBody.message);
            else
              this.notificationService.error("Error Occured");
          } else {
            this.notificationService.error("Error Occured");
          }
        });
    }
    myReader.readAsDataURL(file);
  }
  /**Submiting the whole advisor settings */
  public continue() {
    if (this.chart == 'Pie chart') {
      this.advisorModel.advEmergency.isPieChart = true;
    }
    else {
      this.advisorModel.advEmergency.isPieChart = false;
    }
    if (this.income == "$ xxx - 3Month household income") {
      this.advisorModel.advEmergency.isThreeMonth = true;
    }
    else {
      this.advisorModel.advEmergency.isThreeMonth = false;
    }
    if (!this.emailIdExist) {
      if (this.changedDropdownglobalstate != "") {
        this.globalstate = this.changedDropdownglobalstate;
        // this.advisorModel.advglobal.currency=this.changedDropdowncurrency;
      }
      else if (this.changedDropdownglobalstate == "") {
        this.globalstate = this.advisorModel.advglobal.defaultstate;
      }
      if (this.changedState != "") {
        this.contactState = this.changedState;
      }
      else if (this.changedState == "") {
        this.contactState = this.advisorModel.advContact.state;
      }


      if (this.changedDropdownyearDeath != "") {
        this.yearAfterDeath = this.changedDropdownyearDeath;
      }
      else if (this.changedDropdownyearDeath == "") {
        this.yearAfterDeath = this.advisorModel.advInsurance.yearAfterDeath;
      }
      if (this.changedDropdowncurrency != "") {
        this.currency = this.changedDropdowncurrency;
        // this.advisorModel.advglobal.currency=this.changedDropdowncurrency;
      }
      else if (this.changedDropdowncurrency == "") {
        this.currency = this.advisorModel.advglobal.currency;
      }
      if (this.changedState != "") {
        this.defaultstate = this.changedState;
      }
      else if (this.changedState == "") {
        this.defaultstate = this.advisorModel.advglobal.defaultstate;
      }
      if (this.changedTimezone != "") {
        this.timeZone = this.changedTimezone;
      }
      else if (this.changedTimezone == "") {
        this.timeZone = this.advisorModel.advglobal.timeZone;
      }
    
      if (this.changedDisclaimer != "") {
        this.disclaimer = this.changedDisclaimer;
      }
      else if (this.changedDisclaimer == "") {
        
        this.disclaimer = this.advisorModel.advisorInfo.disclaimer
        this.advisorModel.advisorInfo.disclaimer=this.advisorModel.advisorInfo.disclaimer;
      }
      if (this.changedBranchState != "") {
        this.branchState = this.changedBranchState;
      }
      else if (this.changedBranchState == "") {
        this.branchState = this.advisorModel.advBranch.bState
      }

      if (this.arr.length == 0) {
        this.publicAccessSettingsnw = this.advisorModel.AccesSettings.AdvancedReportDefaults;
      }
      else {
        this.publicAccessSettingsnw = this.arr;
      }
      if (this.arrTwo.length == 0) {
        this.publicAccessExpress = this.advisorModel.AccesSettings.AdvancedReportDefaults;
      }
      else {
        this.publicAccessExpress = this.arrTwo;
      }
      let header = {
        "Content-Type": "application/json"
      }
      let req = {
        "advisorInfo": {
          "advisorId": this.advisorModel.advisorInfo.advisorId,
          "firstName": this.advisorModel.advisorInfo.firstName,
          "lastName": this.advisorModel.advisorInfo.lastName,
          "titleA": this.advisorModel.advisorInfo.titleA,
          "titleB": this.advisorModel.advisorInfo.designation,
          "managerId": this.advisorModel.advisorInfo.managerId,
          "numPlanner": "",
          "disclaimer": this.disclaimer,
          "couponCode": "",
          "capitalChoiceCode": "",
          "setUpFee": "",
          "initMonthlyFee": "",
          "monthlyFee": "",
          "finraReg": this.advisorModel.advisorInfo.finraReg,
          "secReg": this.advisorModel.advisorInfo.secReg,
          "brokerName": this.advisorModel.advisorInfo.brokerName,
          "riaName": this.advisorModel.advisorInfo.riaName,
          "createdDate": "",
          "designation": this.advisorModel.advisorInfo.designation,
          "imgPath": this.advisorModel.advisorInfo.imgPath
        },
        "advglobal": {
          "defultID": this.advisorModel.advglobal.defultID,
          "profileID": this.advisorModel.advglobal.profileID,
          "clientID": this.advisorModel.advglobal.clientID,
          "advisorID": this.advisorModel.advglobal.advisorID,
          "timeZone": this.timeZone,
          "currency": this.currency,
          "defaultstate": this.globalstate
        },
        "advRetirement": {
          "RetirementDefaultID": this.advisorModel.advRetirement.RetirementDefaultID,
          "profileID": this.advisorModel.advRetirement.profileID,
          "clientID": this.advisorModel.advRetirement.clientID,
          "advisorID": this.advisorModel.advRetirement.advisorID,
          "retireAge": this.advisorModel.advRetirement.retireAge,
          "lifeExpectency": this.advisorModel.advRetirement.lifeExpectency,
          "preRetReturnTax": this.advisorModel.advRetirement.preRetReturnTax,
          "retTax": this.advisorModel.advRetirement.retTax,
          "retReturnTax": this.advisorModel.advRetirement.retReturnTax,
          "monthIncome": this.advisorModel.advRetirement.monthIncome,
          "inflation": this.advisorModel.advRetirement.inflation
        },
        "advIncome": {
          "defaultID": this.advisorModel.advIncome.defaultID,
          "profileID": this.advisorModel.advIncome.profileID,
          "clientID": this.advisorModel.advIncome.clientID,
          "advisorID": this.advisorModel.advIncome.advisorID,
          "primaryInflation": this.advisorModel.advIncome.primaryInflation,
          "spouseInflation": this.advisorModel.advIncome.spouseInflation,
          "inflation": this.advisorModel.advIncome.inflation
        },
        "advDebt": {
          "defaultID": this.advisorModel.advDebt.defaultID,
          "profileID": this.advisorModel.advDebt.profileID,
          "clientID": this.advisorModel.advDebt.clientID,
          "advisorID": this.advisorModel.advDebt.advisorID,
          "minPay": this.advisorModel.advDebt.minPay,
          "unpaidPercentage": this.advisorModel.advDebt.unpaidPercentage,
          "inflation": this.advisorModel.advIncome.inflation
        },
        "advEmergency": {
          "isBarChart": this.advisorModel.advEmergency.graphIDBar,
          "isSixMonth": this.isThreeMonth,
          "defaultID": this.advisorModel.advEmergency.defaultID,
          "profileID": this.advisorModel.advEmergency.profileID,
          "clientID": this.advisorModel.advEmergency.clientID,
          "advisorID": this.advisorModel.advEmergency.advisorID,
          "emergencyFundValue": this.advisorModel.advEmergency.emergencyFundValue,
          "rateA": this.advisorModel.advEmergency.rateA,
          "rateB": this.advisorModel.advEmergency.rateB,
          "rateC": this.advisorModel.advEmergency.rateC,
          "isPieChart": this.advisorModel.advEmergency.isPieChart,
          "isThreeMonth": this.advisorModel.advEmergency.isThreeMonth,
          "inflation": this.advisorModel.advEmergency.inflation
        },
        "advInsurance": {
          "defaultID": this.advisorModel.advInsurance.defaultID,
          "profileID": this.advisorModel.advInsurance.profileID,
          "clientID": this.advisorModel.advInsurance.clientID,
          "advisorID": this.advisorModel.advInsurance.advisorID,
          "cost": this.advisorModel.advInsurance.cost,
          "monthlyIncome": this.advisorModel.advInsurance.monthlyIncome,
          "yearAfterDeath": this.changedDropdownyearDeath,
          "specificnoofyears": this.advisorModel.advInsurance.specificnoofyears,
          "retAsset": this.advisorModel.advInsurance.retAsset,
          "otherRetAsset": this.advisorModel.advInsurance.otherRetAsset,
          "cashAccounts": this.advisorModel.advInsurance.cashAccounts,
          "spouseRetAsset": this.advisorModel.advInsurance.spouseRetAsset,
          "nonRetAsset": this.advisorModel.advInsurance.nonRetAsset,
          "inflation": this.advisorModel.advInsurance.inflation
        },
        "advEducation": {
          "defaultID": this.advisorModel.advEducation.defaultID,
          "profileID": this.advisorModel.advEducation.profileID,
          "clientID": this.advisorModel.advEducation.clientID,
          "advisorID": this.storeService.getObject("advisorId"),
          "collegeStartMonth": this.advisorModel.advEducation.collegeStartMonth,
          "tutionFeeInflation": this.advisorModel.advEducation.tutionFeeInflation,
          "collegeAge": this.advisorModel.advEducation.collegeAge,
          "collageYears": this.advisorModel.advEducation.collageYears,
          "parentPay": this.advisorModel.advEducation.parentPay,
          "rateA": this.advisorModel.advEducation.rateA,
          "rateB": this.advisorModel.advEducation.rateB,
          "rateC": this.advisorModel.advEducation.rateC
        },
        "advBranch": {
          "bId": this.advisorModel.advBranch.bId,
          "advisorId": this.storeService.getObject("advisorId"),
          "bFirstName": "",
          "bLastName": "",
          "bAddress": this.advisorModel.advBranch.bAddress,
          "bCity": this.advisorModel.advBranch.bCity,
          "bState": this.branchState,
          "bZip": this.advisorModel.advBranch.bZip,
          "bPhone": "",
          "bCompany": ""
        },
        "advContact": {
          "contactId": this.advisorModel.advContact.contactId,
          "advisorId": this.storeService.getObject("advisorId"),
          "address": this.advisorModel.advContact.address,
          "city": this.advisorModel.advContact.city,
          "state": this.contactState,
          "zip": this.advisorModel.advContact.zip,
          "telephone": this.advisorModel.advContact.telephone,
          "cellphone": this.advisorModel.advContact.cellphone
        },
        "UserInfo": {
          "userId": this.advisorModel.UserInfo.userId,
          "userName": this.advisorModel.UserInfo.userName,

          "emailPrimary": this.advisorModel.UserInfo.emailPrimary,
          "emailPrimaryValid": "",
          "emailSecondary": this.advisorModel.UserInfo.emailSecondary,
          "emailSecondaryValid": "",
          "advisorId": this.storeService.getObject("advisorId"),
          "clientId": "",
          "isActive": "",
          "trialEndDate": "",
          "expDate": "",
          "createdDate": "",
          "isAdmin": "",
          "isManager": "",
          "isSuperUser": "",
          "isLicensed": "",
          "hashCode": "",
          "requestedForgot": "",
          "isTrial": "",
          "isNonTrial": "",
          "profileId": this.advisorModel.UserInfo.profileId
        },
        "AccesSettings": {
          "AdvancedReportDefaults": this.publicAccessSettingsnw,
          "ExpressReportDefaults": this.publicAccessExpress
        }
      }
      console.log("this is posted data" + JSON.stringify(req));
      this.
        ApiServicesService.post('api/Advisor/AdvisorRegister', req, { headers: header })
        .then(response => {
          let parsedBody = JSON.parse(response._body);


          if (parsedBody.apiStatus == 0) {

            this.router.navigate(["app-client-list-new"]);

          }
          else if (parsedBody.apiStatus == 1) {
            for (let i = 0; i < parsedBody.Result.ValidationErrors.length; i++) {
              this.notificationService.error(parsedBody.Result.ValidationErrors[i].FieldErrorMessage);
            }
          }
          else if (parsedBody.apiStatus == 2) {
            this.notificationService.error(parsedBody.Message);
          }
        })
        .catch(err => {
          if (err && err._body && !err._body.target) {
            let parsedBody = JSON.parse(err._body);
            if (parsedBody.error && parsedBody.message)
              this.notificationService.error(parsedBody.message);
            else
              this.notificationService.error("Could Not Register");
          } else {
            this.notificationService.error("Could Not Register");
          }
        });
    }

  }

  onChangeObjThree(newObj) {
    this.timezoneId = newObj;
  }
  onChangeObjFour(newObj) {
    this.currenyValue = newObj;
  }

  globalStateChanged(val: any) {
    this.changedDropdownglobalstate = val;
    this.advisorModel.advglobal.defaultstate = val;
  }
  onChangeState(newObj) {
    this.chnagedState = newObj;
  }
  currencyChanged(val: any) {
    this.changedDropdowncurrency = val;
    this.advisorModel.advglobal.currency = val;
  }
  stateChanged(val: any) {
    this.changedState = val;
  }
  branchstateChanged(val: any) {
    this.changedBranchState = val;
  }
  timezoneChanged(val: any) {
    this.changedTimezone = val;
  }
  disclaimerChanged(val: any) {
    this.changedDisclaimer = val;
    this.selectedDeviceObj = val;
    this.disclaimerMethod(val);
  }
  disclaimerMethod(val) {
    let headerType = {
      "Content-Type": "application/json"
      // "Authorization":"bearer"+" "+this.storeService.getObject("token")
    }
    this.
      ApiServicesService.post('api/Advisor/getDisclaimerDetails', val, { headers: headerType })
      .then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.disclaimerDatavalue = parsedBody.Result;
        }
      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error("Error Occured");
        } else {
          this.notificationService.error("Error Occured");
        }
      });
  }
  yearAfterDeathChanged(val: any) {
    console.log(val);
    this.changedDropdownyearDeath = val;
    if (this.changedDropdownyearDeath == 3) {
      this.specificnoofyearsBox = true;
      this.advisorModel.advInsurance.specificnoofyears = this.advisorModel.advInsurance.specificnoofyears;
    }
    else {
      this.specificnoofyearsBox = false;
      this.advisorModel.advInsurance.specificnoofyears = "0";
    }
  }

  /** Remove colleges From favList */
  remove(item) {
    this.modal.openModal(ConfirmationComponent, item, '');
    this.storeService.putObject("collegeIDArray", item);
    this.modal.resultFunction().then(res => {

      let req = {
        //  "advisorId":item.advisorID,
        "CollegeID": [item.collegeID]
      }
      let header = {
        "Content-Type": "application/json"
      }

      this.
        ApiServicesService.headerAppendedPost('api/College/RemoveFavCollege', req)
        .then(res => {
          let parsedBody = JSON.parse(res._body);
          if (parsedBody.apiStatus == 0) {
            this.showFavouriteColleges();
          }
        })
        .catch(err => {
          if (err && err._body && !err._body.target) {
            let parsedBody = JSON.parse(err._body);
            if (parsedBody.error && parsedBody.message)
              this.notificationService.error(parsedBody.message);
            else
              this.notificationService.error("Error Occured");
          } else {
            this.notificationService.error("Error Occured");
          }
        });
    })
  }
  addAdvancedReport(e, personal) {
    var index = this.publicAccessSettings.indexOf(personal.pageId);
    if (e.target.checked) {
      if (index === -1) {
        this.publicAccessSettings.push(personal);
        this.addOrReplace(personal);

      }
    } else {
      this.publicAccessSettings.splice(index, 1);

    }
    console.log("id of access report" + JSON.stringify(this.arr));
  }
  addExpressReport(e, personal) {
    var index = this.publicAccessSettings.indexOf(personal.pageId);
    if (e.target.checked) {
      if (index === -1) {
        this.publicAccessSettings.push(personal);
        this.addOrReplaceTwo(personal);

      }
    } else {
      this.publicAccessSettings.splice(index, 1);

    }
    console.log("id of access report" + JSON.stringify(this.arrTwo));
  }
  addOrReplace(object) {
    var index = this.arrIndex[object.pageId];
    if (index === undefined) {
      index = this.arr.length;
      this.arrIndex[object.pageId] = index;
    }
    this.arr[index] = object;
  }
  addOrReplaceTwo(object) {
    var index = this.arrIndexTwo[object.pageId];
    if (index === undefined) {
      index = this.arrTwo.length;
      this.arrIndexTwo[object.pageId] = index;
    }
    this.arrTwo[index] = object;
  }
  advisorsettings() {
    this.router.navigate(["advisor-settings"]);

  }
  clientsettings() {

  }
  checkEmail() {
    if (this.advisorModel.UserInfo.emailPrimary == this.storeService.getObject("email_from_login")) {
      console.log("same");
    }
    else {
      console.log("not same");
      this._timeout = null;
      if (this._timeout) { //if there is already a timeout in process cancel it
        window.clearTimeout(this._timeout);
      }
      this._timeout = window.setTimeout(() => {
        let req =
          {
            "email": this.advisorModel.UserInfo.emailPrimary,
            "userId": this.storeService.getObject('UserID')
          }

        let header = {
          "Content-Type": "application/json"
        }
        this._timeout = null;
        this.lc.run(() =>
          // this.name1 = this.signUpModel.fname
          this.
            ApiServicesService.post('api/Login/IsEmailExist', req, { headers: header })
            .then(res => {
              let parsedBody = JSON.parse(res._body);
              if (parsedBody.Result.isExists) {
                console.log("email id" + parsedBody.Result.Message);
                this.emailIdExist = true;
              }
              else {
                console.log("email id" + parsedBody.Result.Message);
                this.emailIdExist = false;
              }


            })
            .catch(err => {
              if (err && err._body && !err._body.target) {
                let parsedBody = JSON.parse(err._body);
                if (parsedBody.error && parsedBody.message)
                  this.notificationService.error(parsedBody.message);
                else
                  this.notificationService.error('Please check the email id');
              } else {
                this.notificationService.error('Please check the  email id');
              }
            })
        );

      }, 1000);
    }

  }
  onPaste(e) {
    e.preventDefault();
    return false;
  }
  changedTab(e) {

    if (e.activeId === 'ngb-tab-0') {
      this.settingslabel = "Global Settings"
    }
    else if (e.activeId === 'ngb-tab-1') {
      this.settingslabel = "Advisor Settings"
    }
  }
}