import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvisorSettingsComponent } from './advisor-settings.component';

describe('AdvisorSettingsComponent', () => {
  let component: AdvisorSettingsComponent;
  let fixture: ComponentFixture<AdvisorSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvisorSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvisorSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
