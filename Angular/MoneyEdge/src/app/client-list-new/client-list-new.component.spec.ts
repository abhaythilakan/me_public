import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientListNewComponent } from './client-list-new.component';

describe('ClientListNewComponent', () => {
  let component: ClientListNewComponent;
  let fixture: ComponentFixture<ClientListNewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientListNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientListNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
