import { Component, OnInit } from '@angular/core';
import { ApiServicesService } from '../services/api-services.service';
import { ActivatedRoute, Router } from '@angular/router';
import { StoreServicesService } from '../services/store-services.service';
import { Data } from '../providers/data/data.service';

@Component({
  selector: 'app-client-list-new',
  templateUrl: './client-list-new.component.html',
  styleUrls: ['./client-list-new.component.scss']
})
export class ClientListNewComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private router: Router,private storeService:StoreServicesService,private data: Data,public ApiServicesService:ApiServicesService) { }

  ngOnInit() {
  }
  addClient(){
    this.router.navigate(["app-add-client"]);
    }
}
