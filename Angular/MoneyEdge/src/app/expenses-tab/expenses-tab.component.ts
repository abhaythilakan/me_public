import { Component, OnInit } from '@angular/core';
import { StoreServicesService } from '../services/store-services.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationServicesService } from '../services/notification-services.service';
import { ModalsService } from '../services/modals.service';
import { ApiServicesService } from '../services/api-services.service';
import { Router } from '@angular/router';
import { ConfirmationComponent } from '../confirmation/confirmation.component';
import { isEmpty } from 'rxjs/operator/isEmpty';
import * as enLocale from 'date-fns/locale/en';
import * as frLocale from 'date-fns/locale/fr';
import { Data } from '../providers/data/data.service';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { Alert } from 'selenium-webdriver';
import { Console } from '@angular/core/src/console';
import * as Highcharts from 'highcharts';


@Component({
  selector: 'app-expenses-tab',
  templateUrl: './expenses-tab.component.html',
  styleUrls: ['./expenses-tab.component.scss']
})

export class ExpensesTabComponent implements OnInit {
  ExpenseType: any[] = [];
  ExpenseSubCategory: any[] = [];
  disableType: boolean[] = [];
  MonthlyDiff: number = 0;
  TotalExp: number = 0;
  disableSubCategory: boolean[] = [];
  LivingExpense: any = {
    "totalExpense": "0",
    "homeExpense": "0",
    "nonHomeExpense": "0",
    "expenseList": [
      {
        "ExpenseId": "0",
        "profileID": this.storeService.getObject('profileID'),
        "expenseCategoryId": "0",
        "expSubCategoryId": "0",
        "typeId": "0",
        "company": "",
        "amount": "",
        "notes": "",
        "showInMouseOver": false
      }
    ]
  }
  InsuranceExpList: any = {
    "faceValueSum": "0",
    "insuranceList": [
      {
        "InsuranceCategory": "",
        "insuranceType": "",
        "company": "",
        "name": "",
        "faceValue": "",
        "currentCashValue": ""
      }
    ]
  }
  DeptExpList: any = {
    "totalPay": "0",
    "deptList": [
      {
        "deptCatType": "",
        "deptType": "",
        "creditorName": "",
        "balance": "",
        "balanceAsOf": "",
        "interestRate": "",
        "principleInterestRate": ""
      }
    ]
  }
  IncomeExpList: any = {
    "total": "0",
    "incomeOutput": [
      {
        "name": "",
        "totalGrossAmount": "",
        "incomeList": [
          {
            "incomeName": "",
            "grossAmount": ""
          }
        ]
      }
    ]
  }
  TaxExpList: any = {
    "total": "0",
    "expenseOutput": [
      {
        "name": "",
        "totalTax": "",
        "incomeList": [
          {
            "taxName": "",
            "taxAmount": ""
          }
        ]
      }
    ]
  }
  SavExpList: any = {
    "total": "0",
    "savingOutput": [
      {
        "name": "",
        "totalSaving": "",
        "savingList": [
          {
            "SavingName": "",
            "SavingAmount": ""
          }
        ]
      }
    ]
  }
  ExpenseCategory: any;
  options = {
    locale: enLocale,
    minYear: 1900,
    displayFormat: 'MM-DD-YYYY',
    barTitleFormat: 'MMMM YYYY',
    firstCalendarDay: 0
  };
  constructor(private modalService: NgbModal, private storeService: StoreServicesService, private ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modal: ModalsService, private router: Router) { }


  ngOnInit() {
    this.getExpenseTypes();
    this.getLivingExpense();
    this.getAllExpense();
  }

  LoadChart(){
    Highcharts.chart('container', {
      chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: 'pie'
      },
      title: {
          text: ''
      },
      tooltip: {
          pointFormat: '<b>{point.percentage:.1f}%</b>',
          backgroundColor: 'rgba(219,219,216,0.8)'
      },
      plotOptions: {
          pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              dataLabels: {
                  enabled: true,
                  format: '<b>{point.name}</b>: {point.y:.1f}',
                  style: { fontFamily: '\'Lato\', sans-serif', fontSize: '11px' }
              }
          },
      },
      series: [{
          data: [{
              name: 'Income',
              y:this.IncomeExpList.total,
              color: '#7AC5CD'
          }, {
              name: 'Expense',
              y: this.TotalExp,
              color: '#ffb2b2'
          }]
      }]
  });
  }
  addRow(index): any {
    this.LivingExpense.expenseList.push({
      "ExpenseId": "0",
      "profileID": this.storeService.getObject('profileID'),
      "expenseCategoryId": "0",
      "expSubCategoryId": "0",
      "typeId": "0",
      "company": "",
      "amount": "",
      "notes": "",
      "showInMouseOver": false
    })
    this.disableSubCategory[this.LivingExpense.expenseList.length - 1] = true;
    this.disableType[this.LivingExpense.expenseList.length - 1] = true;
    this.getExpenseSubType(0, this.LivingExpense.expenseList.length - 1);
    this.getType(0, this.LivingExpense.expenseList.length - 1);
  }
  expenseCategoryChange(val, index) {
    this.getExpenseSubType(val, index);
  }
  expenseSubCategoryChange(val, index) {
    this.getType(val, index);
  }

  getExpenseTypes(): any {
    this.ApiServicesService.headerAppendedGet('api/Expense/getExpenseCategory').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.ExpenseCategory = parsedBody.Result;
      }
    })
  }
  getExpenseSubType(id, index, flag?) {
    this.ApiServicesService.headerAppendedGet('api/Expense/getExpenseSubCategory?id=' + id).then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.ExpenseSubCategory[index] = parsedBody.Result;
        if (this.ExpenseSubCategory[index].length > 1) {
          this.disableSubCategory[index] = false;
          if (flag !== "first") {
            this.disableType[index] = true;
            this.LivingExpense.expenseList[index].typeId = 0;
            this.LivingExpense.expenseList[index].expSubCategoryId = 0;
          }
        }
        else {
          this.disableSubCategory[index] = true;
          this.LivingExpense.expenseList[index].expSubCategoryId = 0;
          this.disableType[index] = true;
          this.LivingExpense.expenseList[index].typeId = 0;
        }
      }
    })
  }
  getType(id, index, flag?) {
    if (id == null)
      id = 0;
    this.ApiServicesService.headerAppendedGet('api/Expense/getExpenseType?id=' + id).then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.ExpenseType[index] = parsedBody.Result;
        if (this.ExpenseType[index].length > 1) {
          this.disableType[index] = false;
        }
        else {
          this.disableType[index] = true;
          this.LivingExpense.expenseList[index].typeId = 0;
        }
      }
    })
  }
  getAllExpense() {
    this.ApiServicesService.headerAppendedPost('api/Expense/getAllExpense', this.storeService.getObject('profileID'))
      .then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.DeptExpList = parsedBody.Result.deptExpList;
          this.InsuranceExpList = parsedBody.Result.insuranceExpList;
          this.TaxExpList = parsedBody.Result.taxExpList;
          this.SavExpList = parsedBody.Result.savExpList;
          this.IncomeExpList = parsedBody.Result.incomeExpList;
          this.calculateExpense();
        }
      })
  }
  getLivingExpense() {
    this.ApiServicesService.headerAppendedPost('api/Expense/getLivingExpense', this.storeService.getObject('profileID'))
      .then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          if (parsedBody.Result.expenseList != null) {
            this.LivingExpense = parsedBody.Result;
            for (let i in this.LivingExpense.expenseList) {
              this.getExpenseSubType(this.LivingExpense.expenseList[i].expenseCategoryId, i, "first");
              this.getType(this.LivingExpense.expenseList[i].expSubCategoryId, i, "first");
            }
            this.calculateExpense();
          }
        }
      })
  }
  calculateExpense() {
    this.TotalExp =
      (this.DeptExpList.totalPay +
        this.InsuranceExpList.faceValueSum +
        this.TaxExpList.total +
        this.SavExpList.total +
        this.LivingExpense.totalExpense);
       this.MonthlyDiff= this.IncomeExpList.total-this.TotalExp;
        if(this.MonthlyDiff<0)
        {
          this.MonthlyDiff = Math.abs(this.MonthlyDiff);
        }
        this.LoadChart();
  }
  saveLivingExpense() {
    let flag = false;
    for (let i = this.LivingExpense.expenseList.length - 1; i > 0; --i) {
      if (this.LivingExpense.expenseList[i].expenseCategoryId == "0" &&
        this.LivingExpense.expenseList[i].expSubCategoryId == "0" &&
        this.LivingExpense.expenseList[i].company == "" &&
        this.LivingExpense.expenseList[i].amount == "") {
        this.LivingExpense.expenseList.splice(i, 1);
      }
    }
    for (let i in this.LivingExpense.expenseList) {
      if (!flag)
        if (this.LivingExpense.expenseList[i].expSubCategoryId > 0 && this.LivingExpense.expenseList[i].typeId == 0 && this.ExpenseType[i].length > 1) {
          flag = true;
        }
    }
    if (flag) {
      this.notificationService.error("Type required");
    }
    else {
      let req = this.LivingExpense.expenseList;
      this.ApiServicesService.headerAppendedPost('api/Expense/saveLivingExpense', req).then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.LivingExpense = parsedBody.Result;
          this.calculateExpense();
          this.notificationService.success("Living expense saved successfully");
        }
        if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Result.Message);
        }
        if (parsedBody.apiStatus == 1) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      })
    }
  }
  removeLivingExpense(expense, i) {
    this.modal.openModal(ConfirmationComponent, '', '');
    this.modal.resultFunction().then(res => {
      if (expense.ExpenseId == 0) {
        this.LivingExpense.expenseList.splice(i, i);
      }
      else {
        let req = {
          "profileId": expense.profileID,
          "expenseId": expense.ExpenseId
        }
        let header = {
          "Content-Type": "application/json"
        }
        this.
          ApiServicesService.headerAppendedPost('api/Expense/deleteLivingExpense', req)
          .then(res => {
            let parsedBody = JSON.parse(res._body);
            if (parsedBody.apiStatus == 0) {
              this.LivingExpense = parsedBody.Result;
              this.calculateExpense();
              if (this.LivingExpense.expenseList.length > 0 && this.LivingExpense.expenseList[0].ExpenseId !== 0) {
                for (let i in this.LivingExpense.expenseList) {
                  this.getExpenseSubType(this.LivingExpense.expenseList[i].expenseCategoryId, i, "first");
                  this.getType(this.LivingExpense.expenseList[i].expSubCategoryId, i, "first");
                }
              }
              if (this.LivingExpense.expenseList.length == 1 && this.LivingExpense.expenseList[0].ExpenseId == 0) {
                this.disableSubCategory[i] = true;
                this.LivingExpense.expenseList[i].expSubCategoryId = 0;
                this.disableType[i] = true;
                this.LivingExpense.expenseList[i].typeId = 0;
              }
            }
          })
          .catch(err => {
            if (err && err._body && !err._body.target) {
              let parsedBody = JSON.parse(err._body);
              if (parsedBody.error && parsedBody.message)
                this.notificationService.error(parsedBody.message);
              else
                this.notificationService.error('Could not delete');
            } else {
              this.notificationService.error('Could not delete');
            }
          });
      }
    })
  }
}
