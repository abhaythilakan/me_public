import { Component, OnInit } from '@angular/core';
import { StoreServicesService } from '../services/store-services.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  admin: boolean;
  client: boolean;
  advisor: boolean;

  constructor(public storeService:StoreServicesService,private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    if(this.storeService.getObject('Role')=="Advisor"){
      console.log("advisor");
      this.advisor=true;
      this.client=false;
      this.admin=false;
    }
    else if(this.storeService.getObject('Role')=="Client"){
      this.client=true;
      this.advisor=false;
      this.admin=false;
    }
    else if(this.storeService.getObject('Role')=="Admin"){
      this.admin=true;
      this.client=false;
      this.advisor=false;
      
    }
  }
  logOut(){
    localStorage.setItem('logout-event', 'logout' + Math.random());

    this.storeService.putObject("token_cleared",false);
    this.storeService.remove('token');
    // this.storeService.clearAll();
    // window.location.reload();
    this.router.navigate(["app-login"]);
  }
  changePassword(){
    this.router.navigate(["app-change-password"]);
  }
  advisorsettings(){
    this.router.navigate(["advisor-settings"]);

  }
  clientsettings(){
    
  }
}
