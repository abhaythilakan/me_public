import { Component, OnInit } from '@angular/core';
import { ApiServicesService } from '../../services/api-services.service';
import { NotificationServicesService } from '../../services/notification-services.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalsService } from '../../services/modals.service';

@Component({
  selector: 'app-cal-debt-vehicle-loan-payment',
  templateUrl: './cal-debt-vehicle-loan-payment.component.html',
  styleUrls: ['./cal-debt-vehicle-loan-payment.component.scss']
})
export class CalDebtVehicleLoanPaymentComponent implements OnInit {
  monthsArray: { "id": number; "name": string; }[];


  constructor(public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modalService: NgbModal,
    public modal: ModalsService) { }

  public loanModal = {
    "cName": null,
    "purchasePrice": null,
    "cashDown": null,
    "drpCashDown": null,
    "tradeAllowance": null,
    "salesTaxRate": null,
    "intRate": null,
    "noOfInstl": null,
    "monName": null,
    "years": null,
    "totSalesTax": null,
    "totBalanceFinanced": null,
    "monthlyPayment": null,
    "currentBalance": null,
    "currentDate": null
  }

  ngOnInit() {
    this.loanModal.drpCashDown = 10;
    this.getVehicleLoan();
    this.monthsArray = [
      {
        "id": 1,
        "name": "Janaury"
      },
      {
        "id": 2,
        "name": "February"
      }, {
        "id": 3,
        "name": "March"
      }, {
        "id": 4,
        "name": "April"
      }, {
        "id": 5,
        "name": "May"
      }, {
        "id": 6,
        "name": "June"
      }, {
        "id": 7,
        "name": "July"
      }, {
        "id": 8,
        "name": "August"
      }, {
        "id": 9,
        "name": "September"
      }, {
        "id": 10,
        "name": "October"
      }, {
        "id": 11,
        "name": "November"
      }, {
        "id": 12,
        "name": "December"
      },
    ]
    this.loanModal.currentDate='Feb, 2018'
  }
  dismiss() {
    this.modal.dismissModal();

  }
  getVehicleLoan() {
    this.ApiServicesService.headerAppendedGet('api/Calculators/GetVehicleLoanCalculator').then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.loanModal = parseBody.Result;
        if(!this.loanModal.currentDate){
          var ts = new Date();


          this.loanModal.currentDate=ts.toDateString();
        }
      }
    })
  }
  onChangeSlider(e) {
    this.loanModal.drpCashDown = e;
    console.log("this.someValue" + this.loanModal.drpCashDown);
  }
  termChanger(e) {
    this.loanModal.noOfInstl= e;

  }
  Calculate(){
    this.ApiServicesService.headerAppendedPost('api/Calculators/VehicleLoanCalculator', this.loanModal).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        // this.ccGoModal=true;
        this.loanModal = parsedBody.Result;
      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
    })
    .catch(error => {
      this.notificationService.error("Error Occured" + error);
    })
  }
}
