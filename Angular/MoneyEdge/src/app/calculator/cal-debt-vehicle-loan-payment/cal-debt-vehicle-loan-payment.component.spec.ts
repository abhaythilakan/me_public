import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalDebtVehicleLoanPaymentComponent } from './cal-debt-vehicle-loan-payment.component';

describe('CalDebtVehicleLoanPaymentComponent', () => {
  let component: CalDebtVehicleLoanPaymentComponent;
  let fixture: ComponentFixture<CalDebtVehicleLoanPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalDebtVehicleLoanPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalDebtVehicleLoanPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
