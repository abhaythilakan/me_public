import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalsService } from '../../services/modals.service';
import { ApiServicesService } from '../../services/api-services.service';
import { NotificationServicesService } from '../../services/notification-services.service';

@Component({
  selector: 'app-cal-goals-dreams',
  templateUrl: './cal-goals-dreams.component.html',
  styleUrls: ['./cal-goals-dreams.component.scss']
})
export class CalGoalsDreamsComponent implements OnInit {

  public savingsGoals: any = {
    "name": "",
    "inflationRate": "",
    "returnRate1": "",
    "returnRate2": "",
    "returnRate3": "",
    "savingName":"",
    "years":"",
    "totalCurrentCostToday":"",
    "totalcurrentSavingsToday": "",
    "savingsDetail":[],
  };
  tableShow:boolean;
  public goalModel = [{
    "savingName": "",
    "currentCost": this.savingsGoals.totalCurrentCostToday,
    "currentSavings": this.savingsGoals.totalcurrentSavingsToday,
    "years": "",
  }]
  public goal = {
    "savingName": "",
    "currentCost": "",
    "currentSavings": "",
    "years": "",
  }

  constructor(private modalService: NgbModal,
    public modal: ModalsService,
    public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService) { }

  ngOnInit() {
    this.tableShow=false;
    this.getDefaultValues();
  }

  getDefaultValues() {
    this.ApiServicesService.headerAppendedGet('api/Calculators/getGoals').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.savingsGoals = parsedBody.Result;
        }
      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }

  getResult(){
    let req={
      "name": this.savingsGoals.name,
      "inflationRate":  this.savingsGoals.inflationRate,
      "returnRate1":  this.savingsGoals.returnRate1,
      "returnRate2": this.savingsGoals.returnRate2,
      "returnRate3":  this.savingsGoals.returnRate3,
      "goalsInput":this.goalModel,
    };


    this.ApiServicesService.headerAppendedPost('api/Calculators/GoalsAtGo', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        if (parsedBody.Result.goalsOutput.length > 0) {
          this.tableShow = true;
          this.savingsGoals.savingsDetail = parsedBody.Result.goalsOutput;
        }
        else if (parsedBody.Result.nationalColleges.length <= 0) {
          this.tableShow = false;
        }
      }
      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);
      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);

      }
    })
  }

  addGoal(val) {
    this.goalModel = this.goalModel || [];
    this.goalModel.push({
      "savingName": "",
      "currentCost": this.savingsGoals.totalCurrentCostToday,
      "currentSavings": this.savingsGoals.totalcurrentSavingsToday,
      "years": "",
    })
  }
  removeGoal(child, index) {
      this.goalModel.splice(index, index);
  }
  dismiss() {
    this.modal.dismissModal();
  }
}
