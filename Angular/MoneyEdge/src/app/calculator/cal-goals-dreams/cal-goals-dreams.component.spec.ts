import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalGoalsDreamsComponent } from './cal-goals-dreams.component';

describe('CalGoalsDreamsComponent', () => {
  let component: CalGoalsDreamsComponent;
  let fixture: ComponentFixture<CalGoalsDreamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalGoalsDreamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalGoalsDreamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
