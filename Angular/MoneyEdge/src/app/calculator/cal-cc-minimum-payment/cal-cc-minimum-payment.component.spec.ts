import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalCcMinimumPaymentComponent } from './cal-cc-minimum-payment.component';

describe('CalCcMinimumPaymentComponent', () => {
  let component: CalCcMinimumPaymentComponent;
  let fixture: ComponentFixture<CalCcMinimumPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalCcMinimumPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalCcMinimumPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
