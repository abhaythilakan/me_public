import { Component, OnInit } from '@angular/core';
import { ApiServicesService } from '../../services/api-services.service';
import { NotificationServicesService } from '../../services/notification-services.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalsService } from '../../services/modals.service';
import { CalAmortizationScheduleComponent } from '../cal-amortization-schedule/cal-amortization-schedule.component';

@Component({
  selector: 'app-cal-cc-minimum-payment',
  templateUrl: './cal-cc-minimum-payment.component.html',
  styleUrls: ['./cal-cc-minimum-payment.component.scss']
})
export class CalCcMinimumPaymentComponent implements OnInit {
  ccGoModal: boolean;
  monthsArray: { "id": number; "name": string; }[];
  percentPayments: any[];
  constructor(public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modalService: NgbModal, 
    public modal: ModalsService) { }
    public someValue=null;
    public ccModal={
      "cName": null,
      "loanAmount": null,
      "intRate": null,
      "cardMinPay": null,
      "percentPay": null,
      "monName": null,
      "years": null,
      "totPayments": null,
      "payOffYears": null,
      "payoff": null,
      "interestPaid": null,
      "currentBalance": null
    }
 
  ngOnInit() {
    this.ccGoModal=false;
    this.ccModal.percentPay=2.00;
    this.ccModal.cardMinPay=15.00;
    this.getCredtiCardMinimumPayment();
    this.monthsArray = [
      {
        "id": 1,
        "name": "Janaury"
      },
      {
        "id": 2,
        "name": "February"
      }, {
        "id": 3,
        "name": "March"
      }, {
        "id": 4,
        "name": "April"
      }, {
        "id": 5,
        "name": "May"
      }, {
        "id": 6,
        "name": "June"
      }, {
        "id": 7,
        "name": "July"
      }, {
        "id": 8,
        "name": "August"
      }, {
        "id": 9,
        "name": "September"
      }, {
        "id": 10,
        "name": "October"
      }, {
        "id": 11,
        "name": "November"
      }, {
        "id": 12,
        "name": "December"
      },
    ]
  }
  dismiss() {
    this.modal.dismissModal();
  }
  fullWidthAmortization_scheduleModal(){
    this.modal.openModal(CalAmortizationScheduleComponent, 'lg', 'modal-full-width');
    this.modal.resultFunction().then(res => {
    })
  }
  getCredtiCardMinimumPayment(){
    this.ApiServicesService.headerAppendedGet('api/Calculators/getcreditCardCalculator').then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.ccModal = parseBody.Result;
        }
    })
  }
  onChangeSlider(e){
    this.ccModal.percentPay=e;
    console.log("this.someValue"+this.ccModal.percentPay);
  }
  onChangeSliderDollar(e){
    this.ccModal.cardMinPay=e;
    console.log("this.someValue"+this.ccModal.cardMinPay);
  }
  Calculate(){
    this.ApiServicesService.headerAppendedPost('api/Calculators/creditCardCalculator', this.ccModal).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.ccGoModal=true;
        this.ccModal = parsedBody.Result;
      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
    })
    .catch(error => {
      this.notificationService.error("Error Occured" + error);
    })
  }
  Reset(){
    this.ccGoModal=false;
    this.getCredtiCardMinimumPayment();
  }
}
