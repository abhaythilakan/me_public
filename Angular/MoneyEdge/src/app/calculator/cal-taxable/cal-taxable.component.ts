import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { NotificationServicesService } from '../../services/notification-services.service';
import { StoreServicesService } from '../../services/store-services.service';
import { ModalsService } from '../../services/modals.service';
import { ApiServicesService } from '../../services/api-services.service';

@Component({
  selector: 'app-cal-taxable',
  templateUrl: './cal-taxable.component.html',
  styleUrls: ['./cal-taxable.component.scss']
})
export class CalTaxableComponent implements OnInit {
  currentDate = new Date();
  public TaxInvestment: any = {
    "name":"",
    "cAge": "",
    "taxRate": "",
    "defTaxRate": "",
    "taxFreeRate": "",
    "fTaxRateA": "1",
    "fTaxRateB": "1",
    "sTaxRateA": "1",
    "sTaxRateB": "1",
    "olTaxRateA": "1",
    "olTaxRateB": "1",
    "amount": "",
    "years": "",
    "annualInvest": "",
    "TotalTaxable": "",
    "TotalTaxDeferred": "",
    "TotalTaxFree": "",
    "TotTxAfter": "",
    "TotDfTxAfter": "",
    "TotTfTxAfter": "",
    "Details": []
  }
  constructor(private modalService: NgbModal, private storeService: StoreServicesService, private ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modal: ModalsService, private router: Router) { }


  ngOnInit() {
    this.getDefaultValues();
  }
  onChangeRateSlider1(e) {
    this.TaxInvestment.fTaxRateA = e;
  }
  onChangeRateSlider2(e) {
    this.TaxInvestment.fTaxRateB = e;
  }
  onChangetaxableSlider(e) {
    this.TaxInvestment.taxRate = e;
  }
  onChangedeferredSlider(e) {
    this.TaxInvestment.defTaxRate = e;
  }
  onChangeFreeSlider(e) {
    this.TaxInvestment.taxFreeRate = e;
  }
  getDefaultValues() {
    this.ApiServicesService.headerAppendedGet('api/Calculators/getTaxableVsTaxDeffered').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.TaxInvestment = parsedBody.Result;
        }
      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }
  getResult() {
    let req = {
      "annualInvest":this.TaxInvestment.annualInvest,
      "taxRate": this.TaxInvestment.taxRate,
      "defTaxRate": this.TaxInvestment.defTaxRate,
      "taxFreeRate": this.TaxInvestment.taxFreeRate,
      "fTaxRateA": this.TaxInvestment.fTaxRateA,
      "fTaxRateB": this.TaxInvestment.fTaxRateB,
      "amount": this.TaxInvestment.amount,
      "cAge": this.TaxInvestment.cAge,
      "sTaxRateA": this.TaxInvestment.sTaxRateA,
      "sTaxRateB": this.TaxInvestment.sTaxRateB,
      "olTaxRateA": this.TaxInvestment.olTaxRateA,
      "olTaxRateB": this.TaxInvestment.olTaxRateB,
      "years": this.TaxInvestment.years
    };
    console.log(JSON.stringify(req));
    this.ApiServicesService.headerAppendedPost('api/Calculators/TaxableVsTaxDeffered', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.TaxInvestment = parsedBody.Result;
      }

      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);

      }
    })
  }
  dismiss() {
    this.modal.dismissModal();
  }

}
