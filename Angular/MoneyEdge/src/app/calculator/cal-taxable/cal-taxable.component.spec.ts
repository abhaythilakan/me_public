import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalTaxableComponent } from './cal-taxable.component';

describe('CalTaxableComponent', () => {
  let component: CalTaxableComponent;
  let fixture: ComponentFixture<CalTaxableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalTaxableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalTaxableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
