import { Component, OnInit } from '@angular/core';
import { ApiServicesService } from '../../services/api-services.service';
import { NotificationServicesService } from '../../services/notification-services.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalsService } from '../../services/modals.service';

@Component({
  selector: 'app-cal-amortization-schedule',
  templateUrl: './cal-amortization-schedule.component.html',
  styleUrls: ['./cal-amortization-schedule.component.scss']
})
export class CalAmortizationScheduleComponent implements OnInit {

  constructor(public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modalService: NgbModal, 
    public modal: ModalsService) { }

  ngOnInit() {
  }
  dismiss() {
    this.modal.dismissModal();
  }
}
