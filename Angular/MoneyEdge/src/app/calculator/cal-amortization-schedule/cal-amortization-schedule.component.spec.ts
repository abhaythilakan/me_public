import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalAmortizationScheduleComponent } from './cal-amortization-schedule.component';

describe('CalAmortizationScheduleComponent', () => {
  let component: CalAmortizationScheduleComponent;
  let fixture: ComponentFixture<CalAmortizationScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalAmortizationScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalAmortizationScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
