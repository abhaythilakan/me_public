import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalCurrentCollegeCostComponent } from './cal-current-college-cost.component';

describe('CalCurrentCollegeCostComponent', () => {
  let component: CalCurrentCollegeCostComponent;
  let fixture: ComponentFixture<CalCurrentCollegeCostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalCurrentCollegeCostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalCurrentCollegeCostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
