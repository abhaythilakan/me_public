import { Component, OnInit } from '@angular/core';
import { ModalsService } from '../../services/modals.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiServicesService } from '../../services/api-services.service';
import { NotificationServicesService } from '../../services/notification-services.service';

@Component({
  selector: 'app-cal-current-college-cost',
  templateUrl: './cal-current-college-cost.component.html',
  styleUrls: ['./cal-current-college-cost.component.scss']
})
export class CalCurrentCollegeCostComponent implements OnInit {
  stateSearch = { "VALUE": null, "TEXT": null };
  statesForsearch: any;
  states: any;
  page: number;
  noNationalClg: boolean;
  noOtherClg: boolean;
  loader: boolean;
  tableShowNational: boolean;
  tableShowOther: boolean;
  collegesNotfound: boolean;
  nationalColleges: any;
  otherColleges: any;
  shortsearch: boolean;
  fullsearch: boolean;
  totalCollegeSerached: any;
  pagination: boolean;
  collegename: any;
  collegeNameByState: any;
  itemsTotal: any;
  columnShowInState: boolean;
  columnShowOutState: boolean;
  showEntireTable: boolean;
  residencyStatusList: any;
  residencyStatus = { "value": null, "text": null };

  constructor(private modalService: NgbModal, private modal: ModalsService, public ApiServicesService: ApiServicesService, private notificationService: NotificationServicesService) {
    this.loader = false;
  }

  ngOnInit() {
    this.stateSearch = { "VALUE": "0", "TEXT": "--Select--" };
    this.page = 0;
    this.noNationalClg = false;
    this.noOtherClg = false;
    this.tableShowNational = false;
    this.tableShowOther = false;
    this.shortsearch = false;
    this.fullsearch = false;
    this.totalCollegeSerached = 0;
    this.pagination = false;
    this.itemsTotal = 0;
    this.columnShowInState = true;
    this.columnShowOutState = true;
    this.showEntireTable = false;
    this.residencyStatusList = [{
      "value": 1,
      "text": "In-state student"
    }, {
      "value": 2,
      "text": "Out-of-state student"
    }, {
      "value": 3,
      "text": "Both"
    }];
    this.residencyStatus={ "value": "3", "text": "Both" };

    /**get states api */
    this.ApiServicesService.get('api/Common/GetState').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.states = parsedBody.Result;
        this.statesForsearch = parsedBody.Result;

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
    /** get natioanl colleges*/
    this.ApiServicesService.headerAppendedGet('api/Calculators/GetCollegeCostCalculator')
      .then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0 && parsedBody.Result) {

          if (parsedBody.Result.nationalColleges.length > 0) {
            this.tableShowNational = true;
            this.noNationalClg = false;
            this.nationalColleges = parsedBody.Result.nationalColleges;
          }
          else if (parsedBody.Result.nationalColleges.length <= 0) {
            this.noNationalClg = true;
          }
        }
      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error("Error Occured");
        } else {
          this.notificationService.error("Error Occured");
        }
      });

  }
  dismissModal() {
    this.modal.closeModal();
  }
  click(e) {
    this.page = e;
    this.getAllColleges(e)
  }
  clickSeach(e) {
    this.page = e;
    this.searchCollege(this.collegename, this.stateSearch.VALUE, e);
  }


  public searchCollege(collegename, state, pageno) {
    this.loader = true;
    if (state == 0) {
      state = null;
    }
    if (pageno) {
      this.page = pageno;
    }

    let req =
      {
        "state": state,
        "name": collegename,
        "PageNo": this.page,
        "PageSize": 20
      }

    let header = {
      "Content-Type": "application/json"

    }
    this.ApiServicesService.headerAppendedPost('api/Calculators/CollegeCostCalculator', req)
      .then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0 && parsedBody.Result) {

          if (parsedBody.Result.nationalColleges.length > 0) {
            this.tableShowNational = true;
            this.noNationalClg = false;
            this.nationalColleges = parsedBody.Result.nationalColleges;
          }
          else if (parsedBody.Result.nationalColleges.length <= 0) {
            this.noNationalClg = true;
          }

          if (parsedBody.Result.otherColleges.length > 0) {
            this.noOtherClg = false;
            this.loader = false;
            this.tableShowOther = true;
            this.collegesNotfound = false;
            this.otherColleges = parsedBody.Result.otherColleges;
            this.shortsearch = true;
            this.pagination = true;
            this.fullsearch = false;
            this.showEntireTable = true;
            //  this.itemsTotal = parsedBody.Result[0].TotalCount;
            this.totalCollegeSerached = parsedBody.Result.otherColleges[0].TotalCount;
          }
          else if (parsedBody.Result.otherColleges.length <= 0) {
            this.noOtherClg = true;
            this.tableShowOther = false;
            this.loader = false;
            this.pagination = false;
            this.showEntireTable = false;
          }

        }
        else if (parsedBody.apiStatus == 1) {
          for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
            if (parsedBody.Result.ValidationErrors[0].FieldErrorMessage) {
              this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
            }
            else {
              this.notificationService.error("Error Occured");
            }
          }
        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Result.Message);

        }
      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error("Error Occured");
        } else {
          this.notificationService.error("Error Occured");
        }
      });
  }

  getAllColleges(pageno) {
    this.loader = true;
    if (pageno) {
      this.page = pageno;
    }
    let req =
      {

        "PageNo": this.page,
        "PageSize": 20
      }

    let header = {
      "Content-Type": "application/json"

    }
    this.ApiServicesService.headerAppendedPost('api/Calculators/CollegeCostCalculator', req)
      .then(res => {
        let parsedBody = JSON.parse(res._body);

        if (parsedBody.apiStatus == 0 && parsedBody.Result) {
          console.log(parsedBody.Result.otherColleges);
          if (parsedBody.Result.otherColleges.length > 0) {
            this.noOtherClg = false;
            this.pagination = true;
            this.loader = false;
            this.tableShowOther = true;
            this.collegesNotfound = false;
            this.otherColleges = parsedBody.Result.otherColleges;
            this.fullsearch = true;
            this.shortsearch = false;
            this.itemsTotal = parsedBody.Result.otherColleges[0].TotalCount;
            this.showEntireTable = false;
          }
          else if (parsedBody.Result.otherColleges.length <= 0) {
            this.noOtherClg = true;
            this.tableShowOther = false;
            this.loader = false;
            this.pagination = false;
          }
        }
        else if (parsedBody.apiStatus == 1) {
          for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
            if (parsedBody.Result.ValidationErrors[0].FieldErrorMessage) {
              this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
            }
            else {
              this.notificationService.error("Error Occured");
            }
          }
        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Result.Message);
        }
      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error("Error Occured");
        } else {
          this.notificationService.error("Error Occured");
        }
      });
  }

  onSelectionResidencyStatus(status) {
    if (status == 1) {
      this.columnShowInState = true;
      this.columnShowOutState = false;
    } else if (status == 2) {
      this.columnShowInState = false;
      this.columnShowOutState = true;
    } else {
      this.columnShowInState = true;
      this.columnShowOutState = true;
    }
  }
}
