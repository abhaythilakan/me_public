import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentInterestCalculatorComponent } from './component-interest-calculator.component';

describe('ComponentInterestCalculatorComponent', () => {
  let component: ComponentInterestCalculatorComponent;
  let fixture: ComponentFixture<ComponentInterestCalculatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentInterestCalculatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentInterestCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
