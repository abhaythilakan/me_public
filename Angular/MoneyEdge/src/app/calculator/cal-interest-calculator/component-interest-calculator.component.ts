import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { NotificationServicesService } from '../../services/notification-services.service';
import { StoreServicesService } from '../../services/store-services.service';
import { ModalsService } from '../../services/modals.service';
import { ApiServicesService } from '../../services/api-services.service';


@Component({
  selector: 'app-component-interest-calculator',
  templateUrl: './component-interest-calculator.component.html',
  styleUrls: ['./component-interest-calculator.component.scss']
})
export class ComponentInterestCalculatorComponent implements OnInit {
  currentDate=new Date();
public ComponentInterest:any= {
  "name": "",
  "age": "",
  "initialStartInvestment": "",
  "monthlyInvestment": "",
  "interestRate1": "",
  "interestRate2": "",
  "interestRate3": "",
  "no_of_years": "",
  "total_anualInvestment": "",
  "total_totalInvestment": "",
  "total_futurevalueA": "",
  "total_futurevalueB": "",
  "total_futurevalueC": "",
  "cInterestDetails": []
}
intrst1:number;
intrst2:number;
intrst3:number;


  constructor(private modalService: NgbModal, private storeService: StoreServicesService, private ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modal: ModalsService, private router: Router) { }


  ngOnInit() {
    this.getDefaultValues();
  }
  getDefaultValues() {
    this.ApiServicesService.headerAppendedGet('api/Calculators/getCompoundInterest').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.ComponentInterest = parsedBody.Result;
        }
      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }
  getResult(){
    let req={
      "name": this.ComponentInterest.name,
      "age": this.ComponentInterest.age,
      "initialStartInvestment": this.ComponentInterest.initialStartInvestment,
      "monthlyInvestment": this.ComponentInterest.monthlyInvestment,
      "interestRate1": this.ComponentInterest.interestRate1,
      "interestRate2": this.ComponentInterest.interestRate2,
      "interestRate3": this.ComponentInterest.interestRate3,
      "no_of_years": this.ComponentInterest.no_of_years,
      "total_anualInvestment": this.ComponentInterest.total_anualInvestment,
      "total_totalInvestment": this.ComponentInterest.total_totalInvestment,
      "total_futurevalueA": this.ComponentInterest.total_futurevalueA,
      "total_futurevalueB": this.ComponentInterest.total_futurevalueB,
      "total_futurevalueC": this.ComponentInterest.total_futurevalueC,
    };
    this.ApiServicesService.headerAppendedPost('api/Calculators/FutureValue', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.ComponentInterest = parsedBody.Result;
        this.intrst1=this.ComponentInterest.interestRate1;
        this.intrst2=this.ComponentInterest.interestRate2;
        this.intrst3=this.ComponentInterest.interestRate3;
        
      }

      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);

      }
    })
  }
  closeModal() {
    this.modal.closeModal();
  }
}
