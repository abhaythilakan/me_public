import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalsService } from '../../services/modals.service';
import { ApiServicesService } from '../../services/api-services.service';
import { NotificationServicesService } from '../../services/notification-services.service';

@Component({
  selector: 'app-cal-mortage-principal-and-interest',
  templateUrl: './cal-mortage-principal-and-interest.component.html',
  styleUrls: ['./cal-mortage-principal-and-interest.component.scss']
})
export class CalMortagePrincipalAndInterestComponent implements OnInit {

  monthsArray: { "id": number; "name": string; }[];
  selectedElement: any;
  tableShow:boolean;
  currentBalanceShow:boolean;
  public mortageIntrest: any = {
    "cName": "",
    "loanAmount": "",
    "intRate": "",
    "no_of_terms": "",
    "monName": "",
    "years":"",
    "prinicipal_Interest_Pay":"",
    "payoff":"",
    "currentBalance": "",
  };

  constructor(private modalService: NgbModal, 
    public modal: ModalsService,
    public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService) { }

  ngOnInit() {

    this.monthsArray = [
      {
        "id": 1,
        "name": "Janaury"
      },
      {
        "id": 2,
        "name": "February"
      }, {
        "id": 3,
        "name": "March"
      }, {
        "id": 4,
        "name": "April"
      }, {
        "id": 5,
        "name": "May"
      }, {
        "id": 6,
        "name": "June"
      }, {
        "id": 7,
        "name": "July"
      }, {
        "id": 8,
        "name": "August"
      }, {
        "id": 9,
        "name": "September"
      }, {
        "id": 10,
        "name": "October"
      }, {
        "id": 11,
        "name": "November"
      }, {
        "id": 12,
        "name": "December"
      },
    ];
    this.tableShow=false;
    this.currentBalanceShow=false;
    this.getDefaultValues();
  }

  getDefaultValues() {
    this.ApiServicesService.headerAppendedGet('api/Calculators/getmortgageInterest').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.mortageIntrest = parsedBody.Result;
        }
      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }
  getResult(){
    let req={
      "cName": this.mortageIntrest.cName,
      "loanAmount": this.mortageIntrest.loanAmount,
      "intRate": this.mortageIntrest.intRate,
      "no_of_terms": this.mortageIntrest.no_of_terms,
      "monName": this.mortageIntrest.monName,
      "years":this.mortageIntrest.years,
      "prinicipal_Interest_Pay":this.mortageIntrest.prinicipal_Interest_Pay,
    };


    this.ApiServicesService.headerAppendedPost('api/Calculators/mortgageInterest', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        console.log(parsedBody.Result);
        if (parsedBody.Result) {
  
          this.currentBalanceShow=true;
          this.mortageIntrest = parsedBody.Result;
        }
        else {
          this.currentBalanceShow=false;
        }
      }
      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);
      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);

      }
    })
  }


  dismiss() {
    this.modal.dismissModal();
  }
  fullWidthModal(e){

  }
}
