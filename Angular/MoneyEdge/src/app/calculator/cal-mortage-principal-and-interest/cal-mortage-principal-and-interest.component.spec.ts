import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalMortagePrincipalAndInterestComponent } from './cal-mortage-principal-and-interest.component';

describe('CalMortagePrincipalAndInterestComponent', () => {
  let component: CalMortagePrincipalAndInterestComponent;
  let fixture: ComponentFixture<CalMortagePrincipalAndInterestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalMortagePrincipalAndInterestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalMortagePrincipalAndInterestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
