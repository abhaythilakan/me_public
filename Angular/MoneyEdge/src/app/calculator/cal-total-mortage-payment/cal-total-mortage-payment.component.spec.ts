import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalTotalMortagePaymentComponent } from './cal-total-mortage-payment.component';

describe('CalTotalMortagePaymentComponent', () => {
  let component: CalTotalMortagePaymentComponent;
  let fixture: ComponentFixture<CalTotalMortagePaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalTotalMortagePaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalTotalMortagePaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
