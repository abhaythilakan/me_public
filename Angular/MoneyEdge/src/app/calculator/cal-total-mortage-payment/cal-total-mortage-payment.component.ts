import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalsService } from '../../services/modals.service';
import { ApiServicesService } from '../../services/api-services.service';
import { NotificationServicesService } from '../../services/notification-services.service';

@Component({
  selector: 'app-cal-total-mortage-payment',
  templateUrl: './cal-total-mortage-payment.component.html',
  styleUrls: ['./cal-total-mortage-payment.component.scss']
})
export class CalTotalMortagePaymentComponent implements OnInit {

  monthsArray: { "id": number; "name": string; }[];
  tableShow:boolean;
  resultShow:boolean;
  public mortagePayment: any = {
    "cName": "",
    "loanAmount": "",
    "intRate": "",
    "no_of_terms": "",
    "propertyTax": "",
    "homeInsurance":"",
    "monthlyPMI":"",
    "monName":"",
    "years": "",
    "prinicipal_Interest_Pay": "",
    "monthly_Tax_Insurance_PMIPay": "",
    "total_Monthly_MortagePay": "",
  };

  constructor(private modalService: NgbModal, 
    public modal: ModalsService,
    public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService) { }

  ngOnInit() {
    this.monthsArray = [
      {
        "id": 1,
        "name": "Janaury"
      },
      {
        "id": 2,
        "name": "February"
      }, {
        "id": 3,
        "name": "March"
      }, {
        "id": 4,
        "name": "April"
      }, {
        "id": 5,
        "name": "May"
      }, {
        "id": 6,
        "name": "June"
      }, {
        "id": 7,
        "name": "July"
      }, {
        "id": 8,
        "name": "August"
      }, {
        "id": 9,
        "name": "September"
      }, {
        "id": 10,
        "name": "October"
      }, {
        "id": 11,
        "name": "November"
      }, {
        "id": 12,
        "name": "December"
      },
    ];
    this.tableShow=false;
    this.resultShow=false;
    this.getDefaultValues();
  }
  getDefaultValues() {
    this.ApiServicesService.headerAppendedGet('api/Calculators/getmortgagePayment').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.mortagePayment = parsedBody.Result;
        }
      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }
  getResult(){
    let req={
      "cName": this.mortagePayment.cName,
      "loanAmount": this.mortagePayment.loanAmount,
      "intRate": this.mortagePayment.intRate,
      "no_of_terms": this.mortagePayment.no_of_terms,
      "propertyTax": this.mortagePayment.propertyTax,
      "homeInsurance":this.mortagePayment.homeInsurance,
      "monthlyPMI":this.mortagePayment.monthlyPMI,
      "monName":this.mortagePayment.monName,
      "years": this.mortagePayment.years,
    };


    this.ApiServicesService.headerAppendedPost('api/Calculators/mortgagePayment', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        console.log(parsedBody.Result);
        if (parsedBody.Result) {
  
          this.resultShow=true;
          this.mortagePayment = parsedBody.Result;
        }
        else {
          this.resultShow=false;
        }
      }
      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);
      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);

      }
    })
  }
  dismiss() {
    this.modal.dismissModal();
  }
  fullWidthModal(e){

  }
}
