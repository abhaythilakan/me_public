import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalMortgageMissingValueComponent } from './cal-mortgage-missing-value.component';

describe('CalMortgageMissingValueComponent', () => {
  let component: CalMortgageMissingValueComponent;
  let fixture: ComponentFixture<CalMortgageMissingValueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalMortgageMissingValueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalMortgageMissingValueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
