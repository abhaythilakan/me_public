import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalsService } from '../../services/modals.service';
import { ApiServicesService } from '../../services/api-services.service';
import { NotificationServicesService } from '../../services/notification-services.service';

@Component({
  selector: 'app-cal-mortgage-missing-value',
  templateUrl: './cal-mortgage-missing-value.component.html',
  styleUrls: ['./cal-mortgage-missing-value.component.scss']
})
export class CalMortgageMissingValueComponent implements OnInit {

  monthsArray: { "id": number; "name": string; }[];
  tableShow: boolean;
  resultShow: boolean;
  public mortageValues: any = {
    "cName": "",
    "loanAmount": "",
    "intRate": "",
    "no_of_terms": "",
    "monName": "",
    "years": "",
    "monthlyPayment": "",
    "currentDate": "",
    "currentBalance": "",
    "loanAmountB": "",
    "intRateB": "",
    "no_of_termsB": "",
    "monthlyPaymentB": "",
  };
  calculatorType: { "class": string;"isDisabled":boolean }[];

  constructor(private modalService: NgbModal,
    public modal: ModalsService,
    public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService) { }

  ngOnInit() {
    this.monthsArray = [
      {
        "id": 1,
        "name": "Janaury"
      },
      {
        "id": 2,
        "name": "February"
      }, {
        "id": 3,
        "name": "March"
      }, {
        "id": 4,
        "name": "April"
      }, {
        "id": 5,
        "name": "May"
      }, {
        "id": 6,
        "name": "June"
      }, {
        "id": 7,
        "name": "July"
      }, {
        "id": 8,
        "name": "August"
      }, {
        "id": 9,
        "name": "September"
      }, {
        "id": 10,
        "name": "October"
      }, {
        "id": 11,
        "name": "November"
      }, {
        "id": 12,
        "name": "December"
      },
    ];

    this.calculatorType = [
      {
        "class": "bg-success",
        "isDisabled":false
      },
      {
        "class": "bg-success",
        "isDisabled":false
      },
      {
        "class": "bg-success",
        "isDisabled":false
      },
      {
        "class": "bg-dark",
        "isDisabled":true
      },
    ];
console.log(this.calculatorType);
    this.tableShow = false;
    this.resultShow = false;
    this.getDefaultValues();
  }

  getDefaultValues() {
    this.ApiServicesService.headerAppendedGet('api/Calculators/getMortgageMissingValue').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.mortageValues = parsedBody.Result;
        }
      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }

  calculatorTypeChange(type) {
    this.mortageValues.loanAmountB = true;
    this.mortageValues.intRateB = true;
    this.mortageValues.no_of_termsB = true;
    this.mortageValues.monthlyPaymentB = true;
    for(let i=0;i<this.calculatorType.length;i++){
      this.calculatorType[i].class="bg-success";
      this.calculatorType[i].isDisabled=false;
    }
    if (type == 1) {
      this.mortageValues.loanAmountB = false;
      this.calculatorType[0].class="bg-dark";
      this.calculatorType[0].isDisabled=true;
    } else if (type == 2) {
      this.mortageValues.intRateB = false;
      this.calculatorType[1].class="bg-dark";
      this.calculatorType[1].isDisabled=true;
    } else if (type == 3) {
      this.mortageValues.no_of_termsB = false;
      this.calculatorType[2].class="bg-dark";
      this.calculatorType[2].isDisabled=true;
    } else if (type == 4) {
      this.mortageValues.monthlyPaymentB = false;
      this.calculatorType[3].class="bg-dark";
      this.calculatorType[3].isDisabled=true;
    }
  }

  getResult() {
    let req = {
      "cName": this.mortageValues.cName,
      "loanAmount": this.mortageValues.loanAmount,
      "intRate": this.mortageValues.intRate,
      "no_of_terms": this.mortageValues.no_of_terms,
      "monName": this.mortageValues.monName,
      "years": this.mortageValues.years,
      "monthlyPayment": this.mortageValues.monthlyPayment,
      "currentBalance": this.mortageValues.currentBalance,
      "loanAmountB": this.mortageValues.loanAmountB,
      "intRateB": this.mortageValues.intRateB,
      "no_of_termsB": this.mortageValues.no_of_termsB,
      "monthlyPaymentB": this.mortageValues.monthlyPaymentB,
    };

    this.ApiServicesService.headerAppendedPost('api/Calculators/mortgageMissingValueCalculate', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        console.log(parsedBody.Result);
        if (parsedBody.Result) {
          this.resultShow = true;
          this.mortageValues = parsedBody.Result;
        }
        else {
          this.resultShow = false;
        }
      }
      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);
      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);

      }
    })
  }
  dismiss() {
    this.modal.dismissModal();
  }
}
