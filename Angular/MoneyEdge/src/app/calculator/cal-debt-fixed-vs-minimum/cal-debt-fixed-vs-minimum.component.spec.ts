import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalDebtFixedVsMinimumComponent } from './cal-debt-fixed-vs-minimum.component';

describe('CalDebtFixedVsMinimumComponent', () => {
  let component: CalDebtFixedVsMinimumComponent;
  let fixture: ComponentFixture<CalDebtFixedVsMinimumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalDebtFixedVsMinimumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalDebtFixedVsMinimumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
