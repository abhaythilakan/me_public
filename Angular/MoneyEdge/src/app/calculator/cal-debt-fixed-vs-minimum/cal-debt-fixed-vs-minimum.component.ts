import { Component, OnInit } from '@angular/core';
import { ApiServicesService } from '../../services/api-services.service';
import { NotificationServicesService } from '../../services/notification-services.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalsService } from '../../services/modals.service';

@Component({
  selector: 'app-cal-debt-fixed-vs-minimum',
  templateUrl: './cal-debt-fixed-vs-minimum.component.html',
  styleUrls: ['./cal-debt-fixed-vs-minimum.component.scss']
})
export class CalDebtFixedVsMinimumComponent implements OnInit {
  monthsArray: { "id": number; "name": string; }[];
  public paymentModal = {
    "cName": null,
    "loanAmount": null,
    "intRate": null,
    "percentPay": null,
    "cardMinPay": null,
    "monName": null,
    "years": null,
    "minPay": null,
    "output": {
      "currentBalance": null,
      "paidDate": null,
      "fixedPaidDate": null,
      "payOffYears": null,
      "fixedPayOffYears": null,
      "totalPayment":null,
      "fixedTotalPayment": null,
      "interestPaid":null,
      "fixedInterestPaid": null,
      "yearSavings":null,
      "interestSavings": null,
      "paymentSavings": null
    }
  }

  constructor(public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modalService: NgbModal,
    public modal: ModalsService) { }

  ngOnInit() {
    this.getDebtfixedMinimumPayment();
    this.paymentModal.percentPay = 2.00;
    this.paymentModal.cardMinPay = 15.00;
    this.monthsArray = [
      {
        "id": 1,
        "name": "Janaury"
      },
      {
        "id": 2,
        "name": "February"
      }, {
        "id": 3,
        "name": "March"
      }, {
        "id": 4,
        "name": "April"
      }, {
        "id": 5,
        "name": "May"
      }, {
        "id": 6,
        "name": "June"
      }, {
        "id": 7,
        "name": "July"
      }, {
        "id": 8,
        "name": "August"
      }, {
        "id": 9,
        "name": "September"
      }, {
        "id": 10,
        "name": "October"
      }, {
        "id": 11,
        "name": "November"
      }, {
        "id": 12,
        "name": "December"
      },
    ]
  }
  dismiss() {
    this.modal.dismissModal();

  }
  onChangeSlider(e) {
    this.paymentModal.percentPay = e;
    console.log("this.someValue" + this.paymentModal.percentPay);
  }
  onChangeSliderDollar(e) {
    this.paymentModal.cardMinPay = e;
    console.log("this.someValue" + this.paymentModal.cardMinPay);
  }
  getDebtfixedMinimumPayment() {
    this.ApiServicesService.headerAppendedGet('api/Calculators/GetfixedvsMinimumCalculator').then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.paymentModal = parseBody.Result;
      }
    })
  }
  calculate(){
    this.ApiServicesService.headerAppendedPost('api/Calculators/FixedvsMinimumCalculator', this.paymentModal).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        // this.ccGoModal=true;
        this.paymentModal = parsedBody.Result;
      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
    })
    .catch(error => {
      this.notificationService.error("Error Occured" + error);
    })
  }
  reset(){
    this.getDebtfixedMinimumPayment();
  }
}
