import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { StoreServicesService } from '../services/store-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { ModalsService } from '../services/modals.service';
import { Router } from '@angular/router';
import { ApiServicesService } from '../services/api-services.service';
import { ComponentInterestCalculatorComponent } from './cal-interest-calculator/component-interest-calculator.component';
import { CalCurrentCollegeCostComponent } from './cal-current-college-cost/cal-current-college-cost.component';
import { CalEarlyInvestorComponent } from './cal-early-investor/cal-early-investor.component';
import { CalLineedsDimeComponent } from './cal-lineeds-dime/cal-lineeds-dime.component';
import { CalCcMinimumPaymentComponent } from './cal-cc-minimum-payment/cal-cc-minimum-payment.component';
import { CalGoalsDreamsComponent } from './cal-goals-dreams/cal-goals-dreams.component';
import { CalDebtFixedVsMinimumComponent } from './cal-debt-fixed-vs-minimum/cal-debt-fixed-vs-minimum.component';
import { CalMortagePrincipalAndInterestComponent } from './cal-mortage-principal-and-interest/cal-mortage-principal-and-interest.component';
import { CalTotalMortagePaymentComponent } from './cal-total-mortage-payment/cal-total-mortage-payment.component';
import { CalMortgageMissingValueComponent } from './cal-mortgage-missing-value/cal-mortgage-missing-value.component';
import { CalTaxableComponent } from './cal-taxable/cal-taxable.component';
import { CalDebtVehicleLoanPaymentComponent } from './cal-debt-vehicle-loan-payment/cal-debt-vehicle-loan-payment.component';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {
 
  closeResult: string;

  constructor(private modalService: NgbModal, private storeService: StoreServicesService, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, public modal: ModalsService, private router: Router) { }


//modal one
  open(content) {
    this.modalService.open(content, { 
    	size: 'lg' 
    }).result.then((result) => {
      this.closeResult = 'Closed with: ${result}';
    }, (reason) => {
      this.closeResult = 'Dismissed ${this.getDismissReason(reason)}';
    });  
  }
  openEarly_investor() {
    this.modal.openModal(CalEarlyInvestorComponent, 'lg', 'modal-xl');
    this.modal.resultFunction().then(res => {
    })
  }
  openComponentCalc() {
    this.modal.openModal(ComponentInterestCalculatorComponent, 'lg', 'modal-xl');
    this.modal.resultFunction().then(res => {
    })
  }
  openCurrent_college_costs(){
    this.modal.openModal(CalCurrentCollegeCostComponent, 'lg', 'modal-xl');
    this.modal.resultFunction().then(res => {
    })
  }
//modal xl
  xlModal(content) {
    this.modalService.open(content, { 
       windowClass: 'modal-xl'  
    }).result.then((result) => {
      this.closeResult = 'Closed with: ${result}';
    }, (reason) => {
      this.closeResult = 'Dismissed ${this.getDismissReason(reason)}';
    });  
  }
  xlModallife_insurance_needs(){
    this.modal.openModal(CalLineedsDimeComponent, 'lg', 'modal-xl');
    this.modal.resultFunction().then(res => {
    })

  }
  xlmortgage_principal_interestModal(){
    this.modal.openModal(CalMortagePrincipalAndInterestComponent, 'lg', 'modal-xl');
    this.modal.resultFunction().then(res => {
    })
  }
  xltotal_mortgage_paymentModal(){
    this.modal.openModal(CalTotalMortagePaymentComponent, 'lg', 'modal-xl');
    this.modal.resultFunction().then(res => {
    })
  
  }
  xlmortgage_missing_valueModal(){
    this.modal.openModal(CalMortgageMissingValueComponent, 'lg', 'modal-xl');
    this.modal.resultFunction().then(res => {
    })
  
  }
  xltaxableModal(){
    this.modal.openModal(CalTaxableComponent, 'lg', 'modal-xl');
    this.modal.resultFunction().then(res => {
    })
  }
  xlvehicle_loan_paymentModal(){
    this.modal.openModal(CalDebtVehicleLoanPaymentComponent, 'lg', 'modal-xl');
    this.modal.resultFunction().then(res => {
    })
  }
  xlgoals_dreams_calculatorModal(){
    this.modal.openModal(CalGoalsDreamsComponent, 'lg', 'modal-xl');
    this.modal.resultFunction().then(res => {
    })
  }
  xlfixed_minimum_paymentModal(){
    this.modal.openModal(CalDebtFixedVsMinimumComponent, 'lg', 'modal-xl');
    this.modal.resultFunction().then(res => {
    })
  }
  credit_card_minimumxlModal(){
    this.modal.openModal(CalCcMinimumPaymentComponent, 'lg', 'modal-xl');
    this.modal.resultFunction().then(res => {
    })
  }
  //modal full-width  
 fullWidthModal(content) {
    this.modalService.open(content, { 
      windowClass: 'modal-full-width' 
    }).result.then((result) => {
      this.closeResult = 'Closed with: ${result}';
    }, (reason) => {
      this.closeResult = 'Dismissed ${this.getDismissReason(reason)}';
    });  
  }



////////////////////////////////

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  'with: ${reason}';
    }
  }

  ngOnInit() {
  }

}
