import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StoreServicesService } from '../../services/store-services.service';
import { ApiServicesService } from '../../services/api-services.service';
import { NotificationServicesService } from '../../services/notification-services.service';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ModalsService } from '../../services/modals.service';

@Component({
  selector: 'app-cal-lineeds-dime',
  templateUrl: './cal-lineeds-dime.component.html',
  styleUrls: ['./cal-lineeds-dime.component.scss']
})
export class CalLineedsDimeComponent implements OnInit {
  showSpouse: boolean;
  sp: string;
  closeResult: string;
  modalRef: NgbModalRef;

  constructor(
    private router: Router, private storeService: StoreServicesService, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modalService: NgbModal, 
    public modal: ModalsService) { }
    spouselist = [
      'Spouse',
      'No Spouse'
    ];
    children=[
      1,2,3,4,5,6,7,8,9,10
    ]
    public dimModal={
    "pName": null,
    "prAge": null,
    "sName": null,
    "sAge": null,
    "pMonthlyNeed": null,
    "pYears": null,
    "sYears": null,
    "isPrimaryIncluded": null,
    "sMonthlyNeed": null,
    "isSpouseIncluded": null,
    "deathExpenses": null,
    "isDeathExpensesIncluded": null,
    "debtAmount": null,
    "isDebtAmountIncluded": null,
    "mortgageBalance": null,
    "ismortgageBalanceIncluded": null,
    "educationNeeds": null,
    "isEducationNeedsIncluded": null,
    "inflationRate": null,
    "returnRate": null,
    "interestRate": null,

    "isSpouseShow": null,
    "Children": null,
    "pTotalNeed": null,
    "sTotalNeed": null,
    "primaryProtectionNeeds": null,
    "spouseProtectionNeeds": null
    }
  ngOnInit() {
    this.dimModal.Children=1;
    this.sp='Spouse';
    this.showSpouse=true;
    this.getDIME();
  }

  dismiss() {
    this.modal.dismissModal();
  }
  getDIME(){
    this.ApiServicesService.headerAppendedGet('api/Calculators/getDIME').then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.dimModal = parseBody.Result;
        
        if (this.dimModal.Children== ""||!this.dimModal.Children||this.dimModal.Children==null) {
          this.dimModal.Children = 1;
        }
        else {
          this.dimModal.Children = this.dimModal.Children;
        }
      }
    })
  }
  go(){

    this.ApiServicesService.headerAppendedPost('api/Calculators/DIMECalculator', this.dimModal).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.dimModal = parsedBody.Result;
      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
    })
    .catch(error => {
      this.notificationService.error("Error Occured" + error);
    })
  }
  reset(){
    this.getDIME();
  }
  optionChange(e){
   if(e=='No Spouse'){
     this.showSpouse=false;
   }
   else{
    this.showSpouse=true;
   }
  }
}
