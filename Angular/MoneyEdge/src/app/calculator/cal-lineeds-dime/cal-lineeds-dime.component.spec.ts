import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalLineedsDimeComponent } from './cal-lineeds-dime.component';

describe('CalLineedsDimeComponent', () => {
  let component: CalLineedsDimeComponent;
  let fixture: ComponentFixture<CalLineedsDimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalLineedsDimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalLineedsDimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
