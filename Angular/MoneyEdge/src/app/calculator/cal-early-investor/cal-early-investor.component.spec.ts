import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalEarlyInvestorComponent } from './cal-early-investor.component';

describe('CalEarlyInvestorComponent', () => {
  let component: CalEarlyInvestorComponent;
  let fixture: ComponentFixture<CalEarlyInvestorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalEarlyInvestorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalEarlyInvestorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
