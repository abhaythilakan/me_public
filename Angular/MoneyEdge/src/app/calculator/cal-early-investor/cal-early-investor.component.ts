import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { NotificationServicesService } from '../../services/notification-services.service';
import { StoreServicesService } from '../../services/store-services.service';
import { ModalsService } from '../../services/modals.service';
import { ApiServicesService } from '../../services/api-services.service';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-cal-early-investor',
  templateUrl: './cal-early-investor.component.html',
  styleUrls: ['./cal-early-investor.component.scss']
})

export class CalEarlyInvestorComponent implements OnInit {
  showChart: boolean;
  serieName: any;
  investedChart: any;
  accumulatedChart: any;
  currentDate = new Date();
  displayGraph: any;
  public EarlyInvestor: any = {
    "name":"",
    "currentAge": "",
    "endYear": "",
    "anualInvestment": "",
    "rateOfreturn": "",
    "earlySpender": "",
    "totalAnualInvestmentA": "",
    "totalAnualInvestmentB": "",
    "Details": [
      {
        "year": "",
        "age": "",
        "anualInvestmentA": "",
        "accumalationA": "",
        "anualInvestmentB": "",
        "accumalationB": "",
        "earlyInvestor": ""
      }]
  }
  
  constructor(private modalService: NgbModal, private storeService: StoreServicesService, private ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modal: ModalsService, private router: Router) { }

  ngOnInit() {
    this.displayGraph = "false";
    this.showChart = false;
    this.getDefaultValues();
  }
  loadCharts(): any {
    this.GraphDisplayChange();
    let annA = [];
    let annB = [];
    let accA = [];
    let accB = [];
    let age = [];
    this.EarlyInvestor.Details.forEach(item => annA.push(item.anualInvestmentA));
    this.EarlyInvestor.Details.forEach(item => annB.push(item.anualInvestmentB));;
    this.EarlyInvestor.Details.forEach(item => accA.push(item.accumalationA));
    this.EarlyInvestor.Details.forEach(item => accB.push(item.accumalationB));;
    if (this.EarlyInvestor.Details[0].age !== null) {
      this.EarlyInvestor.Details.forEach(item => age.push(item.age));;
    }
    else {
      let i = 1;
      this.EarlyInvestor.Details.forEach(item => age.push(i++));;
    }
    Highcharts.setOptions({
      lang: {
        thousandsSep: ','
      }
    })
    this.investedChart = {
      title: { text: 'Total Dollars Invested' },
      chart: { zoomType: 'x' },
      xAxis: {
        categories: age,
        title: {
          text: "Age"
        },
      },
      yAxis: {
        title: {
          text: "Invested Amount"
        }
      },
      series: [
        { name: 'Early Investor', data: annA },
        { name: 'Early Spendor', data: annB }
      ]
    };
    this.accumulatedChart = {
      title: { text: 'Accumulation Value' },
      chart: { zoomType: 'x' },
      xAxis: {
        categories: age,
        title: {
          text: "Age"
        },
      },
      yAxis: {
        title: {
          text: "Accumulated Value"
        }
      },
      
      series: [
        { name: 'Early Investor', data: accA },
        { name: 'Early Spendor', data: accB }
      ]
    };

  }
  GraphDisplayChange() {
    if (this.displayGraph == "true" && this.EarlyInvestor.Details !== null) {
      this.showChart = true;
    }
    else {
      this.showChart = false;
    }
  }
  getDefaultValues() {
    this.showChart = false;
    this.ApiServicesService.headerAppendedGet('api/Calculators/getEarlyInvestor').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.EarlyInvestor = parsedBody.Result;
        }
      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }
  getResult() {
    let req = {
      "currentAge": this.EarlyInvestor.currentAge,
      "endYear": this.EarlyInvestor.endYear,
      "rateOfreturn": this.EarlyInvestor.rateOfreturn,
      "anualInvestment": this.EarlyInvestor.anualInvestment,
    };
    this.ApiServicesService.headerAppendedPost('api/Calculators/EarlyInvestor', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.EarlyInvestor = parsedBody.Result;
        this.loadCharts();
      }

      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);

      }
    })
  }
  closeModal() {
    this.modal.closeModal();
  }
}
