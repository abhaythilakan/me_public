import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsuranceSpouseModelComponent } from './insurance-spouse-model.component';

describe('InsuranceSpouseModelComponent', () => {
  let component: InsuranceSpouseModelComponent;
  let fixture: ComponentFixture<InsuranceSpouseModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsuranceSpouseModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsuranceSpouseModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
