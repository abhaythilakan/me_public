import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StoreServicesService } from '../services/store-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { ModalsService } from '../services/modals.service';
import { Router } from '@angular/router';
import { ApiServicesService } from '../services/api-services.service';

@Component({
  selector: 'app-insurance-spouse-model',
  templateUrl: './insurance-spouse-model.component.html',
  styleUrls: ['./insurance-spouse-model.component.scss']
})
export class InsuranceSpouseModelComponent implements OnInit {
  spousePeriodShow: boolean;
  LifeInsTypes: any;
  public spouseDetails: any = {
    "InsuranceId": 0,
    "profileId": this.storeService.getObject('profileID'),
    "personId": "",
    "payDept": false,
    "payMortage": false,
    "payCollege": false,
    "financialExpence": "",
    "expenceafterdeath": "",
    "insurancePeriod": "",
    "insuranceType": "0",
    "retAsset": false,
    "otherRetAsset": false,
    "cashAccounts": false,
    "spouseRetAsset": false,
    "nonRetAsset": false,
    "selectAll": false,
    "faceValue": "",
    "type": "spouse"
  }
  constructor(private modalService: NgbModal, private storeService: StoreServicesService, private ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modal: ModalsService, private router: Router) { }


  ngOnInit() {
    this.getInsuranceTypes();
    this.spouseDetails=this.storeService.getObject('spouseDetails');
    this.changeSpouseInsType(this.spouseDetails.insuranceType);
  } 
  selectAllSpouseChange(val: any) {

    this.spouseDetails.retAsset = this.spouseDetails.selectAll;
    this.spouseDetails.otherRetAsset = this.spouseDetails.selectAll;
    this.spouseDetails.cashAccounts = this.spouseDetails.selectAll;
    this.spouseDetails.spouseRetAsset = this.spouseDetails.selectAll;
    this.spouseDetails.nonRetAsset = this.spouseDetails.selectAll;
  }
  changeSpouseInsType(val: any) {
    if (val == 3)
      this.spousePeriodShow = true;
    else
      this.spousePeriodShow = false;
  }
  getInsuranceTypes() {
    this.ApiServicesService.headerAppendedGet('api/Insurance/getLifeInsTypes').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.LifeInsTypes = parsedBody.Result;
      }
    })
  }
  spouseInsDetails() {
    if (this.spouseDetails.insuranceType != 3) {
      this.spouseDetails.insurancePeriod = "";
    }
    if(this.spouseDetails.insuranceType == 3 &&(this.spouseDetails.insurancePeriod =="" ||this.spouseDetails.insurancePeriod ==null) )
    {
      this.notificationService.error("Please specify number of years");
    }
    else{
      this.spouseDetails.type = "spouse";
      this.ApiServicesService.headerAppendedPost('api/Insurance/saveInsuranceDetails', this.spouseDetails).then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.spouseDetails = parsedBody.Result;
          this.storeService.putObject('spouseDetails',this.spouseDetails);
          this.modal.closeModal();
        }
        if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Result.Message);
  
        }
        if (parsedBody.apiStatus == 1) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      })
    }
  }
  closeModal() {
    this.modal.closeModal();
  }
}
