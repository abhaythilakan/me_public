import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetirementFundTabComponent } from './retirement-fund-tab.component';

describe('RetirementFundTabComponent', () => {
  let component: RetirementFundTabComponent;
  let fixture: ComponentFixture<RetirementFundTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetirementFundTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetirementFundTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
