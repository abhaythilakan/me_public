import { Component, OnInit } from '@angular/core';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';

@Component({
  selector: 'app-retirement-fund-tab',
  templateUrl: './retirement-fund-tab.component.html',
  styleUrls: ['./retirement-fund-tab.component.scss']
})
export class RetirementFundTabComponent implements OnInit {

  retirementLedgerModal={
    "currentNeed": null,
    "retTaxRate": null,
    "retReturnTax": null,
    "inflation": null,
    "preRetTax": null,
    "reportDate": null,
    "PersonInfoDetails": [
      {
        "age": null,
        "untilRetirement": null,
        "retAge": null,
        "inRetirement": null,
        "personID": null,
        "dob": null,
        "lifeExpectancy": null,
        "name": null,

        "isprimary": null
      }
    ],
    "rateA": null,
    "rateB": null,
    "rateC": null,
    "currentretSummary": [
      {
        "name":null,
        "personID": null,
        "totalplanbalance": null,
        "totalpersonalContribution": null,
        "totalempContribution": null,
        "currentRetSummaryDetails": []
      }
    ],
    "RetirementGoal": {
      "monthlyNeedFuture": null,
      "annualNeedFuture": null,
      "FIN": null,
      "RetirementGrowDetails": [
        {
          "rate": null,
          "amount": null,
          "shortfallsurplus": null
        }
      ],
      "AdditionalNeedDetails": [
        {
          "rate": null,
          "lumpsum": null,
          "lumpsumstring": null,
          "monthly": null,
          "monthlyString": null
        }
      ]
    },
    "whatif": [
      {
        "whatifplanbalance":null,
        "whatifpersonalContribution": null,
        "whatifmonthlyContribution": null,
        "isTaxed": null
      }
    ],
    "currentretSummaryTotal": {
      "totalplanbalance": null,
      "totalpersonalContribution": null,
      "totalempContribution": null
    },
    "otherValuesModel": {
      "startdate": null,
      "enddate": null,
      "count": null,
      "pName":null,
      "sName": null,
      "pCurrentBalance": null,
      "pCurrentContribution": null,
      "pCurrentBalnceNotInterest": null,
      "pCurrentContributionNotInterest": null,
      "sCurrentBalance": null,
      "sCurrentContribution": null,
      "sCurrentBalnceNotInterest": null,
      "sCurrentContributionNotInterest": null,
      "oCurrentBalance": null,
      "oCurrentContribution": null,
      "oCurrentBalnceNotInterest": null,
      "oCurrentContributionNotInterest": null,
      "wCurrentBalance": null,
      "wCurrentContribution": null,
      "wCurrentBalnceNotInterest": null,
      "wCurrentContributionNotInterest": null
    },
    "ledger": [
      {
        "year": null,
        "pAge": null,
        "sAge": null,
        "balance": null,
        "growth": null,
        "annualContribution": null,
        "annualRetNeed": null,
        "otherIncomeafterRet": null,
        "taxesonOtherIncomeafterRet": null,
        "withdrawal": null,
        "taxesOnWithdrawal": null,
        "endingBalance": null
      }
    ],
    "profileID": null
  }
  constructor(private storeService: StoreServicesService, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService) { }

  ngOnInit() {
    this.getRetirement();
  }
  getRetirement(){
    this.ApiServicesService.headerAppendedPost('api/RetirementGoal/GetRetirementDetails', this.storeService.getObject('profileID')).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.retirementLedgerModal=parsedBody.Result;
      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
    }) 
  }
}
