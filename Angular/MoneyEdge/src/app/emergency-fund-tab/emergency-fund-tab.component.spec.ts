import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmergencyFundTabComponent } from './emergency-fund-tab.component';

describe('EmergencyFundTabComponent', () => {
  let component: EmergencyFundTabComponent;
  let fixture: ComponentFixture<EmergencyFundTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmergencyFundTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmergencyFundTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
