import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalsService } from '../services/modals.service';

@Component({
  selector: 'app-emergency-fund-tab',
  templateUrl: './emergency-fund-tab.component.html',
  styleUrls: ['./emergency-fund-tab.component.scss']
})
export class EmergencyFundTabComponent implements OnInit {
  hideAmtBlk: boolean;
  showHide: boolean;
  public emergencyModal = {
    "targetAmount": null,
    "currentFund": null,
    "details": [
      {
        "assetCatg": null,
        "assetType": [
          {
            "name":null,
            "value": null,
          }
         
        ]
      }
    ],
    "status": null,
    "statusValue": null,
    "returnRate": null,
    "years": null,
    "monthlySavings": null,
  }



  constructor(private route: ActivatedRoute,
    private router: Router, private storeService: StoreServicesService, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modalService: NgbModal,
    public modal: ModalsService) { }

  ngOnInit() {
    this.getEmergencyFund();
    this.hideAmtBlk=true;
    this.showHide=false;
  }
  getEmergencyFund() {
    this.ApiServicesService.headerAppendedPost('api/EmergencyFund/GetFund', this.storeService.getObject('profileID')).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.emergencyModal = parsedBody.Result;
        if(this.emergencyModal.currentFund==""||this.emergencyModal.currentFund==0||this.emergencyModal.details==null||
      this.emergencyModal.details==[]){
          this.hideAmtBlk=false;
        }
        else if(this.emergencyModal.currentFund==""||this.emergencyModal.currentFund==0||this.emergencyModal.details==null||
        this.emergencyModal.details==[]){
          this.hideAmtBlk=true;
        }

      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i < parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[i].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Message);

      }
    })
      .catch(error => {
        this.notificationService.error("error while getting education details" + error);
      })
  }
  go() {
    let req = {
      "profileID": this.storeService.getObject('profileID'),
      "targetAmount": this.emergencyModal.targetAmount,
      "returnRate": this.emergencyModal.returnRate,
      "years": this.emergencyModal.years,
      "currentFund":this.emergencyModal.currentFund
    }
    this.ApiServicesService.headerAppendedPost('api/EmergencyFund/GoFund',req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.emergencyModal = parsedBody.Result;
        if(this.emergencyModal.currentFund==""||this.emergencyModal.currentFund==0||this.emergencyModal.details==null||
        this.emergencyModal.details==[]){
          this.hideAmtBlk=false;
        }
        else{
          this.hideAmtBlk=true;
        }

      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i < parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[i].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Message);

      }
    })
  }
}
