import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DebtTabComponent } from './debt-tab.component';

describe('DebtTabComponent', () => {
  let component: DebtTabComponent;
  let fixture: ComponentFixture<DebtTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DebtTabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DebtTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
