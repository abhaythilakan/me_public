import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap/modal/modal';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { ModalsService } from '../services/modals.service';
import { Router } from '@angular/router';
import { isNumber } from '@ng-bootstrap/ng-bootstrap/util/util';
import { _MatSlideToggleMixinBase } from '@angular/material';
import { forEach } from '@angular/router/src/utils/collection';
import * as enLocale from 'date-fns/locale/en';
@Component({
  selector: 'app-debt-tab',
  templateUrl: './debt-tab.component.html',
  styleUrls: ['./debt-tab.component.scss']
})



export class DebtTabComponent implements OnInit {
  options = {
    locale: enLocale,
    minYear: 1970,
    maxYear: 2030,
    displayFormat: 'MM-DD-YYYY',
    barTitleFormat: 'MMMM YYYY',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    // minDate: new Date(Date.now()), // Minimal selectable date
  };
  extraPayOutput: number;
  //mortageTypeDDL: any;
  totalInterestPaid: number;
  debtFreeDateString: string;

  mortageDDL = [];
  fixedDDL = [];
  revolveDDL = [];
  sum: number;
  constructor(
    private modalService: NgbModal,
    private storeService: StoreServicesService,
    public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService,
    public modal: ModalsService,
    private router: Router
  ) { }
  private mortageModel: Model[] = [];
  private fixedModel: Model[] = [];
  private revolveModel: Model[] = [];
  private disappearModel: DebtDisappearModel[] = [];
  ngOnInit() {
    this.getMortage();
    this.getFixed();
    this.getRevolve();
    this.getMortageDDL();
    this.getFixedDDL();
    this.getRevolveDDL();
    this.getExtraPayFromDebts();
    this.getDisappearListOutput();
  }

  ngOnChanges() {
    this.getExtraPayFromDebts();
    this.getDisappearListOutput();
  }

  ngDoCheck() {
    if (!(this.mortageModel.length > 0)) {
      let mortageM = new Model();
      mortageM.debtID = 0;
      mortageM.profileID = this.storeService.getObject('profileID');
      this.mortageModel.push(mortageM);
    }
    if (!(this.fixedModel.length > 0)) {
      let fixedM = new Model();
      fixedM.debtID = 0;
      fixedM.profileID = this.storeService.getObject('profileID');
      this.fixedModel.push(fixedM);
    }
    if (!(this.revolveModel.length > 0)) {
      let revolveM = new Model();
      revolveM.debtID = 0;
      revolveM.profileID = this.storeService.getObject('profileID');
      this.revolveModel.push(revolveM);
    }
    //this.getExtraPayFromDebts();
    //this.getDisappearListOutput();
  }

  getExtraPayFromDebts() {
    this.ApiServicesService.headerAppendedPost('api/Debt/getTotalExtraPayment', this.storeService.getObject('profileID')).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        let mdl = new DebtExtraPayModel();
        mdl = parseBody.Result;
        if (mdl && mdl.extraPay > 0)
          this.extraPayOutput = mdl.extraPay;
        else
          this.extraPayOutput = 0;
      }
    })
  }

  getDisappearListOutput() {
    let mdlx = new DebtExtraPayModel();
    mdlx.profileID = this.storeService.getObject('profileID');
    mdlx.extraPay = this.extraPayOutput;
    this.ApiServicesService.headerAppendedPost('api/Debt/getDebtDisappearList', mdlx).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.disappearModel = parseBody.Result;
        if (this.disappearModel != null && this.disappearModel.length > 0) {
          this.totalInterestPaid = 0;
          //this.disappearModel.forEach(x => this.totalInterestPaid += parseFloat(x.projInterestPaid == undefined ? "0" : x.projInterestPaid.toString());
          for (var itm in this.disappearModel) {
            this.totalInterestPaid += parseFloat(this.disappearModel[itm].projInterestPaid === undefined ? "0" : this.disappearModel[itm].projInterestPaid.toString());
          }
          this.debtFreeDateString = this.disappearModel[this.disappearModel.length-1].projPayOffString;
        }
      }
    })
  }

  getMortageDDL() {
    this.ApiServicesService.headerAppendedGet('api/Common/getMortageDDL').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.mortageDDL = parsedBody.Result;
      }
    })
  }
  getFixedDDL() {
    this.ApiServicesService.headerAppendedGet('api/Common/getFixedDDL').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.fixedDDL = parsedBody.Result;
      }
    })
  }
  getRevolveDDL() {
    this.ApiServicesService.headerAppendedGet('api/Common/getRevolveDDL').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.revolveDDL = parsedBody.Result;
      }
    })
  }

  changeMortageDLL(val: any, index) {
    this.mortageModel[index].debtTypeID = val;
  }
  changeFixedDLL(val: any, index) {
    this.fixedModel[index].debtTypeID = val;
  }
  changeRevolveDLL(val: any, index) {
    this.revolveModel[index].debtTypeID = val;
  }

  onChangeMortageMinPay(val) {
    if (this.mortageModel[val].minPay !== undefined && this.mortageModel[val].extraPay !== undefined && this.mortageModel[val].minPay > 0 && this.mortageModel[val].extraPay > 0) {
      this.mortageModel[val].actualPay = parseFloat(this.mortageModel[val].minPay.toString()) + parseFloat(this.mortageModel[val].extraPay.toString());
      if (isNaN(this.mortageModel[val].actualPay)) {
        this.mortageModel[val].actualPay = 0;
      }
    }
    else if (this.mortageModel[val].minPay !== undefined && this.mortageModel[val].minPay > 0) {
      this.mortageModel[val].actualPay = this.mortageModel[val].minPay;
    }
    else if (this.mortageModel[val].extraPay !== undefined && this.mortageModel[val].extraPay > 0) {
      this.mortageModel[val].actualPay = this.mortageModel[val].extraPay;
    }
    else {
      this.mortageModel[val].actualPay = 0;
    }
  }
  onChangeMortageExtraPay(val) {
    if (this.mortageModel[val].minPay !== undefined && this.mortageModel[val].extraPay !== undefined && this.mortageModel[val].minPay > 0 && this.mortageModel[val].extraPay > 0) {
      this.mortageModel[val].actualPay = parseFloat(this.mortageModel[val].minPay.toString()) + parseFloat(this.mortageModel[val].extraPay.toString());
      if (isNaN(this.mortageModel[val].actualPay)) {
        this.mortageModel[val].actualPay = 0;
      }
    }
    else if (this.mortageModel[val].minPay !== undefined && this.mortageModel[val].minPay > 0) {
      this.mortageModel[val].actualPay = this.mortageModel[val].minPay;
    }
    else if (this.mortageModel[val].extraPay !== undefined && this.mortageModel[val].extraPay > 0) {
      this.mortageModel[val].actualPay = this.mortageModel[val].extraPay;
    }
    else {
      this.mortageModel[val].actualPay = 0;
    }
  }
  onChangeFixedMinPay(val) {
    if (this.fixedModel[val].minPay !== undefined && this.fixedModel[val].extraPay !== undefined && this.fixedModel[val].minPay > 0 && this.fixedModel[val].extraPay > 0) {
      this.fixedModel[val].actualPay = parseFloat(this.fixedModel[val].minPay.toString()) + parseFloat(this.fixedModel[val].extraPay.toString());
      if (isNaN(this.fixedModel[val].actualPay)) {
        this.fixedModel[val].actualPay = 0;
      }
    }
    else if (this.fixedModel[val].minPay !== undefined && this.fixedModel[val].minPay > 0) {
      this.fixedModel[val].actualPay = this.fixedModel[val].minPay;
    }
    else if (this.fixedModel[val].extraPay !== undefined && this.fixedModel[val].extraPay > 0) {
      this.fixedModel[val].actualPay = this.fixedModel[val].extraPay;
    }
    else {
      this.fixedModel[val].actualPay = 0;
    }
  }
  onChangeFixedExtraPay(val) {
    if (this.fixedModel[val].minPay !== undefined && this.fixedModel[val].extraPay !== undefined && this.fixedModel[val].minPay > 0 && this.fixedModel[val].extraPay > 0) {
      this.fixedModel[val].actualPay = parseFloat(this.fixedModel[val].minPay.toString()) + parseFloat(this.fixedModel[val].extraPay.toString());
      if (isNaN(this.fixedModel[val].actualPay)) {
        this.fixedModel[val].actualPay = 0;
      }
    }
    else if (this.fixedModel[val].minPay !== undefined && this.fixedModel[val].minPay > 0) {
      this.fixedModel[val].actualPay = this.fixedModel[val].minPay;
    }
    else if (this.fixedModel[val].extraPay !== undefined && this.fixedModel[val].extraPay > 0) {
      this.fixedModel[val].actualPay = this.fixedModel[val].extraPay;
    }
    else {
      this.fixedModel[val].actualPay = 0;
    }
  }
  onChangeRevolveAmount(index) {
    let Amount = this.revolveModel[index].amount == undefined ? 0 : parseFloat(this.revolveModel[index].amount.toString());
    let unPaidBalance = this.revolveModel[index].unPaidBalance == undefined ? 0 : parseFloat(this.revolveModel[index].unPaidBalance.toString());
    let cardMinPay = this.revolveModel[index].cardMinPay == undefined ? 0 : parseFloat(this.revolveModel[index].cardMinPay.toString());
    let totPayAmt = 0
    if (Amount > 0 && unPaidBalance > 0) {
      totPayAmt = Amount * (unPaidBalance / 100);
    }
    if (Amount > 0) {
      if (totPayAmt > cardMinPay) {
        this.revolveModel[index].minPay = totPayAmt == undefined ? 0 : totPayAmt;
        this.revolveModel[index].actualPay = totPayAmt == undefined ? 0 : totPayAmt;
      }
      else {
        this.revolveModel[index].minPay = cardMinPay == undefined ? 0 : cardMinPay;
        this.revolveModel[index].actualPay = cardMinPay == undefined ? 0 : cardMinPay;
      }
    }
    else {
      if (cardMinPay > 0) {
        this.revolveModel[index].minPay = cardMinPay == undefined ? 0 : cardMinPay;
        this.revolveModel[index].actualPay = cardMinPay == undefined ? 0 : cardMinPay;
      }
      else {
        this.revolveModel[index].minPay = 0;
        this.revolveModel[index].actualPay = 0;
      }
    }
  }
  onChangeRevolveUnpaidBalance(index) {
    let Amount = this.revolveModel[index].amount == undefined ? 0 : parseFloat(this.revolveModel[index].amount.toString());
    let unPaidBalance = this.revolveModel[index].unPaidBalance == undefined ? 0 : parseFloat(this.revolveModel[index].unPaidBalance.toString());
    let cardMinPay = this.revolveModel[index].cardMinPay == undefined ? 0 : parseFloat(this.revolveModel[index].cardMinPay.toString());
    let totPayAmt = 0
    if (Amount > 0 && unPaidBalance > 0) {
      totPayAmt = Amount * (unPaidBalance / 100);
    }
    if (Amount > 0) {
      if (totPayAmt > cardMinPay) {
        this.revolveModel[index].minPay = totPayAmt == undefined ? 0 : totPayAmt;
        this.revolveModel[index].actualPay = totPayAmt == undefined ? 0 : totPayAmt;
      }
      else {
        this.revolveModel[index].minPay = cardMinPay == undefined ? 0 : cardMinPay;
        this.revolveModel[index].actualPay = cardMinPay == undefined ? 0 : cardMinPay;
      }
    }
    else {
      if (cardMinPay > 0) {
        this.revolveModel[index].minPay = cardMinPay == undefined ? 0 : cardMinPay;
        this.revolveModel[index].actualPay = cardMinPay == undefined ? 0 : cardMinPay;
      }
      else {
        this.revolveModel[index].minPay = 0;
        this.revolveModel[index].actualPay = 0;
      }
    }
  }
  onChangeRevolveCardPay(index) {
    let Amount = this.revolveModel[index].amount == undefined ? 0 : parseFloat(this.revolveModel[index].amount.toString());
    let unPaidBalance = this.revolveModel[index].unPaidBalance == undefined ? 0 : parseFloat(this.revolveModel[index].unPaidBalance.toString());
    let cardMinPay = this.revolveModel[index].cardMinPay == undefined ? 0 : parseFloat(this.revolveModel[index].cardMinPay.toString());
    let totPayAmt = 0
    if (Amount > 0 && unPaidBalance > 0) {
      totPayAmt = Amount * (unPaidBalance / 100);
    }
    if (Amount > 0) {
      if (totPayAmt > cardMinPay) {
        this.revolveModel[index].minPay = totPayAmt == undefined ? 0 : totPayAmt;
        this.revolveModel[index].actualPay = totPayAmt == undefined ? 0 : totPayAmt;
      }
      else {
        this.revolveModel[index].minPay = cardMinPay == undefined ? 0 : cardMinPay;
        this.revolveModel[index].actualPay = cardMinPay == undefined ? 0 : cardMinPay;
      }
    }
    else {
      if (cardMinPay > 0) {
        this.revolveModel[index].minPay = cardMinPay == undefined ? 0 : cardMinPay;
        this.revolveModel[index].actualPay = cardMinPay == undefined ? 0 : cardMinPay;
      }
      else {
        this.revolveModel[index].minPay = 0;
        this.revolveModel[index].actualPay = 0;
      }
    }
  }


  addMortage() {
    let mortageM = new Model();
    mortageM.debtID = 0;
    mortageM.profileID = this.storeService.getObject('profileID');
    this.mortageModel.push(mortageM);
  }
  addFixed() {
    let fixedM = new Model();
    fixedM.debtID = 0;
    fixedM.profileID = this.storeService.getObject('profileID');
    this.fixedModel.push(fixedM);
  }
  addRevolve() {
    let revolveM = new Model();
    revolveM.debtID = 0;
    revolveM.profileID = this.storeService.getObject('profileID');
    this.revolveModel.push(revolveM);
  }

  getMortage() {
    this.ApiServicesService.headerAppendedPost("api/Debt/getMortages", this.storeService.getObject('profileID')).then(res => {
      let parseBody = JSON.parse(res._body);
      if (parseBody.apiStatus == 0) {
        this.mortageModel = parseBody.Result;
      }
      else if (parseBody.apiStatus == 1) {
        this.notificationService.error(parseBody.ValidationErrors[0].FieldErrorMessage);
      }
      else if (parseBody.apiStatus == 2) {
        this.notificationService.error(parseBody.Result.Message);
      }
    }).catch(err => {
      if (err && err._body && !err._body.target) {
        let parsedBody = JSON.parse(err._body);
        if ((parsedBody.error && parsedBody.message) || (parsedBody.message && parsedBody.apiStatus == 1))
          this.notificationService.error(parsedBody.message);
        else
          this.notificationService.error('Could not found Mortage Collection');
      } else {
        this.notificationService.error('Could not Mortage Collection');
      }
    });
  }
  getFixed() {
    this.ApiServicesService.headerAppendedPost("api/Debt/getFixeds", this.storeService.getObject('profileID')).then(res => {
      let parseBody = JSON.parse(res._body);
      if (parseBody.apiStatus == 0) {
        this.fixedModel = parseBody.Result;
      }
      else if (parseBody.apiStatus == 1) {
        this.notificationService.error(parseBody.ValidationErrors[0].FieldErrorMessage);
      }
      else if (parseBody.apiStatus == 2) {
        this.notificationService.error(parseBody.Result.Message);
      }
    }).catch(err => {
      if (err && err._body && !err._body.target) {
        let parsedBody = JSON.parse(err._body);
        if ((parsedBody.error && parsedBody.message) || (parsedBody.message && parsedBody.apiStatus == 1))
          this.notificationService.error(parsedBody.message);
        else
          this.notificationService.error('Could not found Fixed Collection');
      } else {
        this.notificationService.error('Could not Fixed Collection');
      }
    });
  }
  getRevolve() {
    this.ApiServicesService.headerAppendedPost("api/Debt/getRevolvings", this.storeService.getObject('profileID')).then(res => {
      let parseBody = JSON.parse(res._body);
      if (parseBody.apiStatus == 0) {
        this.revolveModel = parseBody.Result;
      }
      else if (parseBody.apiStatus == 1) {
        this.notificationService.error(parseBody.ValidationErrors[0].FieldErrorMessage);
      }
      else if (parseBody.apiStatus == 2) {
        this.notificationService.error(parseBody.Result.Message);
      }
    }).catch(err => {
      if (err && err._body && !err._body.target) {
        let parsedBody = JSON.parse(err._body);
        if ((parsedBody.error && parsedBody.message) || (parsedBody.message && parsedBody.apiStatus == 1))
          this.notificationService.error(parsedBody.message);
        else
          this.notificationService.error('Could not found Revolving Collection');
      } else {
        this.notificationService.error('Could not Revolving Collection');
      }
    });
  }

  saveWholeMortageDebt() {
    this.ApiServicesService.headerAppendedPost('api/Debt/saveDebtDetailsMortage', this.mortageModel).then(res => {
      let parseBody = JSON.parse(res._body);
      if (parseBody.apiStatus == 0) {
        this.notificationService.success("Mortage Debt Saved Successfully");
        this.mortageModel = parseBody.Result;
        this.getExtraPayFromDebts();
        this.getDisappearListOutput();
      }
      else if (parseBody.apiStatus == 1) {
        this.notificationService.error(parseBody.ValidationErrors[0].FieldErrorMessage);
      }
      else if (parseBody.apiStatus == 2) {
        this.notificationService.error(parseBody.Result.Message);
      }
    }).catch(err => {
      if (err && err._body && !err._body.target) {
        let parsedBody = JSON.parse(err._body);
        if ((parsedBody.error && parsedBody.message) || (parsedBody.message && parsedBody.apiStatus == 1))
          this.notificationService.error(parsedBody.message);
        else
          this.notificationService.error('Error while saving Mortage Collection');
      } else {
        this.notificationService.error('Error while saving Mortage Collection');
      }
    });
  }
  saveWholeFixedDebt() {
    this.ApiServicesService.headerAppendedPost('api/Debt/saveDebtDetailsFixed', this.fixedModel).then(res => {
      let parseBody = JSON.parse(res._body);
      if (parseBody.apiStatus == 0) {
        this.notificationService.success("Fixed Debt Saved Successfully");
        this.fixedModel = parseBody.Result;
        this.getExtraPayFromDebts();
        this.getDisappearListOutput();
      }
      else if (parseBody.apiStatus == 1) {
        this.notificationService.error(parseBody.ValidationErrors[0].FieldErrorMessage);
      }
      else if (parseBody.apiStatus == 2) {
        this.notificationService.error(parseBody.Result.Message);
      }
    }).catch(err => {
      if (err && err._body && !err._body.target) {
        let parsedBody = JSON.parse(err._body);
        if ((parsedBody.error && parsedBody.message) || (parsedBody.message && parsedBody.apiStatus == 1))
          this.notificationService.error(parsedBody.message);
        else
          this.notificationService.error('Error while saving Fixed Collection');
      } else {
        this.notificationService.error('Error while saving Fixed Collection');
      }
    });
  }
  saveWholeRevolveDebt() {
    this.ApiServicesService.headerAppendedPost('api/Debt/saveDebtDetailsRevolving', this.revolveModel).then(res => {
      let parseBody = JSON.parse(res._body);
      if (parseBody.apiStatus == 0) {
        this.notificationService.success("Revolving Debt Saved Successfully");
        this.revolveModel = parseBody.Result;
        this.getExtraPayFromDebts();
        this.getDisappearListOutput();
      }
      else if (parseBody.apiStatus == 1) {
        this.notificationService.error(parseBody.ValidationErrors[0].FieldErrorMessage);
      }
      else if (parseBody.apiStatus == 2) {
        this.notificationService.error(parseBody.Result.Message);
      }
    }).catch(err => {
      if (err && err._body && !err._body.target) {
        let parsedBody = JSON.parse(err._body);
        if ((parsedBody.error && parsedBody.message) || (parsedBody.message && parsedBody.apiStatus == 1))
          this.notificationService.error(parsedBody.message);
        else
          this.notificationService.error('Error while saving Revolving Collection');
      } else {
        this.notificationService.error('Error while saving Revolving Collection');
      }
    });
  }

  removeMortageRow(index) {
    if (this.mortageModel[index].debtID > 0) {
      this.mortageModel[index].isActive = "false";
    }
    else {
      this.mortageModel.splice(index);
    }
  }
  removeFixedRow(index) {
    if (this.fixedModel[index].debtID > 0) {
      this.fixedModel[index].isActive = "false";
    }
    else {
      this.fixedModel.splice(index);
    }
  }
  removeRevolveRow(index) {
    if (this.revolveModel[index].debtID > 0) {
      this.revolveModel[index].isActive = "false";
    }
    else {
      this.revolveModel.splice(index);
    }
  }

  addDeletedMortageRow(index) {
    this.mortageModel[index].isActive = "true";
  }
  addDeletedFixedRow(index) {
    this.fixedModel[index].isActive = "true";
  }
  addDeletedRevolveRow(index) {
    this.revolveModel[index].isActive = "true";
  }



}

export class DebtExtraPayModel {
  public profileID: number
  public extraPay: number
}

export class Model {
  public debtID: number
  public profileID: number
  public debtCatId: number
  public creditor: string
  public amount: number
  public minPay: number
  public unPaidBalance: number
  public actualPay: number
  public extraPay: number
  public rate: number
  public isFixed: string = ""
  public notes: number
  public percentPay: number
  public introRate: number
  public introDate: Date = new Date()
  public isPlan: string = ""
  public debtTypeID: number
  public inclElmPlan: string = ""
  public cardMinPay: number
  public Balanceasof: Date = new Date()
  public PrimaryResidence: string = ""
  public NotesShow: number
  public debtCreatedDate: Date = new Date()
  public debtModifiedDate: Date = new Date()
  public debtModifiedBy: number
  public projectedPayOffDate: Date = new Date()
  public isActive: string = ""
}

export class DebtDisappearModel {
  public debtID: number
  public debtCreditor: string
  public debtCatId: number
  public debtCatName: string
  public accelDate: Date
  public accelPayment: number
  public accelDateString: string
  public initialMinimumPayment: number
  public newPayment: number
  public projPayOff: Date
  public projPayOffString: string
  public projInterestPaid: number
}