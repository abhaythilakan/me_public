import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalsService } from '../services/modals.service';
import * as enLocale from 'date-fns/locale/en';
import * as frLocale from 'date-fns/locale/fr';
import { DatepickerOptions } from 'ng2-datepicker';

@Component({
  selector: 'app-education-savings-modal',
  templateUrl: './education-savings-modal.component.html',
  styleUrls: ['./education-savings-modal.component.scss']
})
export class EducationSavingsModalComponent implements OnInit {
  public CurretSavingsDetails = [{
    "detailID": null,
    "educationID": null,
    "educationTypeID": null,
    "company": null,
    "amount": null,
    "cID": null,
    "amountValueAsOf": new Date(Date.now()).toISOString(),
    "monthly": null,
    "notes": null,
    "isActive": null,
    "detailsCreatedDate": null,
    "detailsUpdatedDate": null,
    "detailsModifiedBy": null
  }];

  // options = {
  //   locale: enLocale,
  //   minYear: 1970,
  //   maxYear: 2030,
  //   displayFormat: 'MM-DD-YYYY',
  //   barTitleFormat: 'MMMM YYYY',
  //   firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
  //   // minDate: new Date(Date.now()), // Minimal selectable date
  //   maxDate: new Date(Date.now())
  // };
  options: DatepickerOptions = {
    locale: enLocale,

    displayFormat: 'MM-DD-YYYY',
    barTitleFormat: 'MMMM YYYY',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    // minDate: new Date(Date.now()), // Minimal selectable date
    // maxDate: new Date(Date.now()),  // Maximal selectable date
    //barTitleIfEmpty: 'Click to select a date'
  };
  educationTypeID: any;
  educationTypeDDL: any;

  constructor(private route: ActivatedRoute,
    private router: Router, private storeService: StoreServicesService, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modalService: NgbModal,
    public modal: ModalsService) { }

  ngOnInit() {
    this.getEducationTypeDDL();
    this.getCurrentSavings();
  }
  getEducationTypeDDL() {

    this.ApiServicesService.headerAppendedGet('api/IncomeDetails/getEducationTypeDDL').then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.educationTypeDDL = parsedBody.Result;
      }
    })
  }
  closeModal() {
    this.modal.closeModal();

  }
  dismiss() {
    this.modal.dismissModal();
  }
  educationTypeIDChanged(val: any, i) {
    console.log("timezone changed" + val);
    this.educationTypeID = val;
    // this.CurretSavingsDetails[i].educationTypeID=val;
  }
  getCurrentSavings() {
    this.
      ApiServicesService.headerAppendedPost('api/Education/getEducationCurretSavingsDetails', this.storeService.getObject('educationID'))
      .then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.CurretSavingsDetails = parsedBody.Result;
          // for(let i=1;i<=this.CurretSavingsDetails.length;i++){
          //   if(!this.CurretSavingsDetails[i].amountValueAsOf){
          //     this.CurretSavingsDetails[i].amountValueAsOf=new Date(Date.now()).toISOString();
          //   }
            
          // }
         
          // console.log("get Date"+this.CurretSavingsDetails[0].amountValueAsOf)
        }
        else if (parsedBody.apiStatus == 1) {
          for (let i = 0; i < parsedBody.Result.ValidationErrors.length; i++) {
            this.notificationService.error(parsedBody.Result.ValidationErrors[i].FieldErrorMessage);
          }
        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Message);

        }
      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error('Unable To Get Current Savings');
        } else {
          this.notificationService.error('Unable To Get Current Savings');
        }
      });
  }

  /**currentSavings save */
  currentSavings(data) {
    let req = [];
    req.push(data);
    this.
      ApiServicesService.headerAppendedPost('api/Education/SaveEducationCurretSavingsDetails', req)
      .then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.notificationService.success('Current Savings Saved Successfully');
          this.getCurrentSavings();
          this.closeModal();
        }
        else if (parsedBody.apiStatus == 1) {
          for (let i = 0; i < parsedBody.Result.ValidationErrors.length; i++) {
            this.notificationService.error(parsedBody.Result.ValidationErrors[i].FieldErrorMessage);
          }
        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Message);

        }
      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error('Unable To Save Current Savings');
        } else {
          this.notificationService.error('Unable To Save Current Savings');
        }
      });
  }

  addMore() {
    this.CurretSavingsDetails = this.CurretSavingsDetails || [];
    this.CurretSavingsDetails.push({
      "detailID": 0,
      "educationID": this.storeService.getObject('educationID'),
      "educationTypeID": null,
      "company": null,
      "amount": null,
      "cID": null,
      "amountValueAsOf": new Date(Date.now()).toISOString(),
      "monthly": null,
      "notes": null,
      "isActive": null,
      "detailsCreatedDate": null,
      "detailsUpdatedDate": null,
      "detailsModifiedBy": null
    })
  }


}
