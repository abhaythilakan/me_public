import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EducationSavingsModalComponent } from './education-savings-modal.component';

describe('EducationSavingsModalComponent', () => {
  let component: EducationSavingsModalComponent;
  let fixture: ComponentFixture<EducationSavingsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EducationSavingsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EducationSavingsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
