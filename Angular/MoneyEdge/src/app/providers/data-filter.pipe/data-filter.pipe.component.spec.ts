import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataFilterPipe } from './data-filter.pipe.component';

describe('DataFilter.PipeComponent', () => {
  let component: DataFilterPipe;
  let fixture: ComponentFixture<DataFilterPipe>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataFilterPipe ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataFilterPipe);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
