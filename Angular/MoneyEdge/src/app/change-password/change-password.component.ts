import { Component, OnInit } from '@angular/core';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  public changePwdModel: any = {
    oldpassword: "",
    password: ""
  };
  constructor(public ApiServicesService: ApiServicesService, private notificationService: NotificationServicesService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

  }
  public changePassword() {
    let body = {
      "oldpassword": this.changePwdModel.oldpassword,
      "newPwd": this.changePwdModel.password
    }
    this.
      ApiServicesService.headerAppendedPost('api/Common/Changepassword', body)
      .then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.router.navigate(["app-change-password"]);


        }
        if (parsedBody.apiStatus == 1) {
          for (let i = 0; i < parsedBody.ValidationErrors.length; i++) {
            this.notificationService.error(parsedBody.ValidationErrors[i].FieldErrorMessage);
          }
        }
      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error('Error occured');
        } else {
          this.notificationService.error('Error occured');

        }
      });
  }
}
