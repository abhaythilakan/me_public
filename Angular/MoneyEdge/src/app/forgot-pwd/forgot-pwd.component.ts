import { Component, OnInit } from '@angular/core';
import { NotificationServicesService } from '../services/notification-services.service';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-forgot-pwd',
  templateUrl: './forgot-pwd.component.html',
  styleUrls: ['./forgot-pwd.component.scss']
})
export class ForgotPwdComponent implements OnInit {
  showMsg: any;
  showErr: boolean;
  userID: any;
  token: any;
  email: any;
  sub: any;
  public confirmPwdModel = {
    "password": "",

  }
  constructor(private storeService: StoreServicesService, private router: Router, private route: ActivatedRoute, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private _Activatedroute: ActivatedRoute,
    private _router: Router) { }

  ngOnInit() {

    this.showErr = false;
    this.sub = this._Activatedroute.queryParams
      .subscribe(params => {
        this.email = params.email;
        this.token = params.token;
        console.log('Query params ', this.email + this.token)
      });
    /**Evaluating whether the email link is expired  */
    this.linkExpired();
  }
  /**Evaluating whether the email link is expired  */
  linkExpired() {
    let req = {
      "email": this.email,
      "token": this.token
    }
    this.ApiServicesService.headerAppendedPost('api/Login/resetPasswordConfirm', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.userID = parsedBody.Result.userID;
        this.showErr = false;
      }
      else if (parsedBody.apiStatus == 1) {
        this.showErr = true;
        this.showMsg = parsedBody.Result.Message;
      }
    })
      .catch(error => {
        this.notificationService.error("error while email expiry evaluation" + error);
      })
  }
  /**Reset Password */
  resetPwd() {
    let req = {
      "password": this.confirmPwdModel.password,
      "token": this.token,
      "userID": this.userID
    }
    this.ApiServicesService.headerAppendedPost('api/Login/resetPassword', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.notificationService.success("Your Password Changed Successfully");
        this.router.navigate(["app-login"]);
      }
      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);

      }
    })
      .catch(error => {
        this.notificationService.error("error while email expiry evaluation" + error);
      })
  }
}
