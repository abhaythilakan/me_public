import { Component, OnInit } from '@angular/core';
import { StoreServicesService } from '../services/store-services.service';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {
  retirementCls: boolean;
  protectionfundCls: boolean;
  emergencyFund: boolean;
  expenseCls: boolean;
  insuranceCls: boolean;
  debtCls: boolean;
  educationCls: boolean;
  activateCls: boolean;
  incomeCls: boolean;
  personalCls: boolean;

  constructor(private storeService:StoreServicesService) { }

  ngOnInit() {
    this.personalCls=true;
    this.storeService.putObject('personal_tab',true);
    this.storeService.putObject('income_tab',false);
    this.storeService.putObject('asset_tab',false);
    this.storeService.putObject('education_tab',false);
    this.storeService.putObject('debt_tab',false);
    this.storeService.putObject('insurance_tab',false);
    this.storeService.putObject('expense_tab',false);
    this.storeService.putObject('emergency_tab',false);
    this.storeService.putObject('protection_tab',false);
    this.storeService.putObject('protection_tab',false);
    this.storeService.putObject('retirement_tab',false);
    
  }
  activatePersonalInfo(){
    this.personalCls=true;
    this.incomeCls=false;
    this.activateCls=false;
    this.educationCls=false;
    this.debtCls=false;
    this.insuranceCls=false;
    this.expenseCls=false;
    this.emergencyFund=false;
    this.protectionfundCls=false;
    this.retirementCls=false;
    
    this.storeService.putObject('personal_tab',true);
    this.storeService.putObject('income_tab',false);
    this.storeService.putObject('asset_tab',false);
    this.storeService.putObject('education_tab',false);
    this.storeService.putObject('debt_tab',false);
    this.storeService.putObject('insurance_tab',false);
    this.storeService.putObject('expense_tab',false);
    this.storeService.putObject('emergency_tab',false);
    this.storeService.putObject('protection_tab',false);
    this.storeService.putObject('protection_tab',false);
    this.storeService.putObject('retirement_tab',false);
    
  }
  activateIncome(){
    this.personalCls=false;
    this.incomeCls=true;
    this.activateCls=false;
    this.educationCls=false;
    this.debtCls=false;
    this.insuranceCls=false;
    this.expenseCls=false;
    this.emergencyFund=false;
    this.protectionfundCls=false;
    this.retirementCls=false;
    this.storeService.putObject('income_tab',true);
    this.storeService.putObject('personal_tab',false);
    this.storeService.putObject('income_tab',false);
    this.storeService.putObject('asset_tab',false);
    this.storeService.putObject('education_tab',false);
    this.storeService.putObject('debt_tab',false);
    this.storeService.putObject('insurance_tab',false);
    this.storeService.putObject('expense_tab',false);
    this.storeService.putObject('emergency_tab',false);
    this.storeService.putObject('protection_tab',false);
    this.storeService.putObject('protection_tab',false);
    
  }
  activateAsset(){
    this.personalCls=false;
    this.incomeCls=false;
    this.activateCls=true;
    this.educationCls=false;
    this.debtCls=false;
    this.insuranceCls=false;
    this.expenseCls=false;
    this.emergencyFund=false;
    this.protectionfundCls=false;
    this.retirementCls=false;
    this.storeService.putObject('asset_tab',true);
    this.storeService.putObject('personal_tab',false);
    this.storeService.putObject('income_tab',false);
    this.storeService.putObject('education_tab',false);
    this.storeService.putObject('debt_tab',false);
    this.storeService.putObject('insurance_tab',false);
    this.storeService.putObject('expense_tab',false);
    this.storeService.putObject('emergency_tab',false);
    this.storeService.putObject('protection_tab',false);
    this.storeService.putObject('protection_tab',false);
    this.storeService.putObject('retirement_tab',false);
    
  }
  activateEducation(){
    this.personalCls=false;
    this.incomeCls=false;
    this.activateCls=false;
    this.educationCls=true;
    this.debtCls=false;
    this.insuranceCls=false;
    this.expenseCls=false;
    this.emergencyFund=false;
    this.protectionfundCls=false;
    this.retirementCls=false;
    this.storeService.putObject('education_tab',true);
    this.storeService.putObject('personal_tab',false);
    this.storeService.putObject('income_tab',false);
    this.storeService.putObject('asset_tab',false);
    this.storeService.putObject('debt_tab',false);
    this.storeService.putObject('insurance_tab',false);
    this.storeService.putObject('expense_tab',false);
    this.storeService.putObject('emergency_tab',false);
    this.storeService.putObject('protection_tab',false);
    this.storeService.putObject('protection_tab',false);
    this.storeService.putObject('retirement_tab',false);
    
  }
  activateDebt(){
    this.personalCls=false;
    this.incomeCls=false;
    this.activateCls=false;
    this.educationCls=false;
    this.debtCls=true;
    this.insuranceCls=false;
    this.expenseCls=false;
    this.emergencyFund=false;
    this.protectionfundCls=false;
    this.retirementCls=false;
    this.storeService.putObject('debt_tab',true);
    this.storeService.putObject('personal_tab',false);
    this.storeService.putObject('income_tab',false);
    this.storeService.putObject('asset_tab',false);
    this.storeService.putObject('education_tab',false);
    this.storeService.putObject('insurance_tab',false);
    this.storeService.putObject('expense_tab',false);
    this.storeService.putObject('emergency_tab',false);
    this.storeService.putObject('protection_tab',false);
    this.storeService.putObject('protection_tab',false); 
    this.storeService.putObject('retirement_tab',false);
    
   }
  activateInsurance(){
    this.personalCls=false;
    this.incomeCls=false;
    this.activateCls=false;
    this.educationCls=false;
    this.debtCls=false;
    this.insuranceCls=true;
    this.expenseCls=false;
    this.emergencyFund=false;
    this.protectionfundCls=false;
    this.retirementCls=false;
    this.storeService.putObject('insurance_tab',true);
    this.storeService.putObject('personal_tab',false);
    this.storeService.putObject('income_tab',false);
    this.storeService.putObject('asset_tab',false);
    this.storeService.putObject('education_tab',false);
    this.storeService.putObject('debt_tab',false);
    this.storeService.putObject('expense_tab',false);
    this.storeService.putObject('emergency_tab',false);
    this.storeService.putObject('protection_tab',false);
    this.storeService.putObject('protection_tab',false); 
    this.storeService.putObject('retirement_tab',false);
    
   }
  activateExpense(){
    this.personalCls=false;
    this.incomeCls=false;
    this.activateCls=false;
    this.educationCls=false;
    this.debtCls=false;
    this.insuranceCls=false;
    this.expenseCls=true;
    this.emergencyFund=false;
    this.protectionfundCls=false;
    this.retirementCls=false;
    this.storeService.putObject('expense_tab',true);
    this.storeService.putObject('personal_tab',false);
    this.storeService.putObject('income_tab',false);
    this.storeService.putObject('asset_tab',false);
    this.storeService.putObject('education_tab',false);
    this.storeService.putObject('debt_tab',false);
    this.storeService.putObject('insurance_tab',false);
    this.storeService.putObject('emergency_tab',false);
    this.storeService.putObject('protection_tab',false);
    this.storeService.putObject('protection_tab',false);
    this.storeService.putObject('retirement_tab',false);
    
  }
  activateEmergencyFund(){
    this.personalCls=false;
    this.incomeCls=false;
    this.activateCls=false;
    this.educationCls=false;
    this.debtCls=false;
    this.insuranceCls=false;
    this.expenseCls=false;
    this.emergencyFund=true;
    this.protectionfundCls=false;
    this.retirementCls=false;
    this.storeService.putObject('emergency_tab',true);
    this.storeService.putObject('personal_tab',false);
    this.storeService.putObject('income_tab',false);
    this.storeService.putObject('asset_tab',false);
    this.storeService.putObject('education_tab',false);
    this.storeService.putObject('debt_tab',false);
    this.storeService.putObject('insurance_tab',false);
    this.storeService.putObject('expense_tab',false);
    this.storeService.putObject('protection_tab',false);
    this.storeService.putObject('protection_tab',false);
    this.storeService.putObject('retirement_tab',false);
    
  }
  activateProtectionFund(){
    this.personalCls=false;
    this.incomeCls=false;
    this.activateCls=false;
    this.educationCls=false;
    this.debtCls=false;
    this.insuranceCls=false;
    this.expenseCls=false;
    this.emergencyFund=false;
    this.protectionfundCls=true;
    this.retirementCls=false;
    this.storeService.putObject('protection_tab',true);
    this.storeService.putObject('personal_tab',false);
    this.storeService.putObject('income_tab',false);
    this.storeService.putObject('asset_tab',false);
    this.storeService.putObject('education_tab',false);
    this.storeService.putObject('debt_tab',false);
    this.storeService.putObject('insurance_tab',false);
    this.storeService.putObject('expense_tab',false);
    this.storeService.putObject('emergency_tab',false);
    this.storeService.putObject('protection_tab',false);
    this.storeService.putObject('retirement_tab',false);
    
  }
  activateRetirement(){
    this.personalCls=false;
    this.incomeCls=false;
    this.activateCls=false;
    this.educationCls=false;
    this.debtCls=false;
    this.insuranceCls=false;
    this.expenseCls=false;
    this.emergencyFund=false;
    this.protectionfundCls=false;
    this.retirementCls=true;
    this.storeService.putObject('retirement_tab',true);
    this.storeService.putObject('personal_tab',false);
    this.storeService.putObject('income_tab',false);
    this.storeService.putObject('asset_tab',false);
    this.storeService.putObject('education_tab',false);
    this.storeService.putObject('debt_tab',false);
    this.storeService.putObject('insurance_tab',false);
    this.storeService.putObject('expense_tab',false);
    this.storeService.putObject('emergency_tab',false);
    this.storeService.putObject('protection_tab',false);
  }
}
