import { Component, OnInit } from '@angular/core';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { StoreServicesService } from '../services/store-services.service';

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss']
})
export class SupportComponent implements OnInit {
  contactUsData: any;
  find: any;
  state: any;
  findUsData: any;
  states: any;
  mobileNumberLength: number;
  public contactModel={
    "name":"",
    "email":"",
    "phone":"",
    "state":null,
    "company":"",
    "findUs":null,
    "subject":"",
    "message":""
  }
  public selectedState={"VALUE":"0","TEXT":"--Select--"};
  public hearedSelected={"VALUE":0,"TEXT":"--Select--"};
  constructor(public ApiServicesService:ApiServicesService,private notificationService: NotificationServicesService,
  private storeService: StoreServicesService,) { }

  ngOnInit() {
    this.mobileNumberLength=10;
    this.storeService.putObject('hide_login',true);
    this.getStates();
    this.findUs();
    this.contactModel.state=this.selectedState.VALUE;
    this.contactModel.findUs=this.hearedSelected.VALUE;
  }

    getStates(){
     /**get states api */
    this.ApiServicesService.get('api/Common/GetState').then(res =>{
    if (res && res._body && !res._body.target) {
      let parsedBody = JSON.parse(res._body);
      this.states = parsedBody.Result;
   
    }
    }) 
    .catch(err => {
    this.notificationService.error(err);
 
    })
  }
  findUs(){
    this.ApiServicesService.headerAppendedGet('api/Common/FindUs').then(res=>{
      let parsedBody=JSON.parse(res._body);
      if(parsedBody.apiStatus==0){
        this.findUsData=parsedBody.Result;
      }
    })
  }
  StateChanged(newObj){
    console.log("globalStateChanged"+newObj);
    this.state = newObj;
  }
  statefindUs(newObj){
    console.log("globalStateChanged"+newObj);
    this.find = newObj;
  }
  submit(){
   let req={
        "name":this.contactModel.name,
        "email": this.contactModel.email,
        "phone": this.contactModel.phone,
        "stateCode":this.state,
        "company":this.contactModel.company,
        "findUs" :this.find,
        "subject" :this.contactModel.subject,
        "comments":this.contactModel.message
    }
    this.ApiServicesService.headerAppendedPost('api/Contact/postContactUs',req).then(res=>{
      let parsedBody=JSON.parse(res._body);
      if(parsedBody.apiStatus==0){
        this.contactUsData=parsedBody.Result;
        this.notificationService.success("Thank You For Contacting Us")
        this.contactModel={
          "name":"",
          "email":"",
          "phone":"",
          "state":"",
          "company":"",
          "findUs":"",
          "subject":"",
          "message":""
        }
  
      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i < parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[i].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Message);
      }
    })
  }
  onPaste(e) {
    e.preventDefault();
    return false;
  }
}
