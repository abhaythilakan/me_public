import { Component, OnInit } from '@angular/core';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { StoreServicesService } from '../services/store-services.service';
import { Router } from '@angular/router';
import { error } from 'selenium-webdriver';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  profileID: any;
  checkboxValue: boolean;
  remeberMe: boolean;
  public signInModel: any = {
    email: "",
    password: ""
  };
  constructor(public ApiServicesService: ApiServicesService, private notificationService: NotificationServicesService,
    private storeService: StoreServicesService, private router: Router) { }

  ngOnInit() {
    {
      if (this.storeService.getObject('userAuth')) {
        this.signInModel = this.storeService.getObject('userAuth');
      }
      else {
      }
    }
  }
  public login(): void {
    let req = {
      "username": this.signInModel.email,
      "password": this.signInModel.password,
    }
    let header = {
      "Content-Type": "application/json"
    }
    let parameter = {
      "username": req.username,
      "password": req.password,
      "grant_type": "password"
    }
    this.
      ApiServicesService.post('api/Login/SignIn', parameter, { headers: header })
      .then(res => {
        let parsedBody = JSON.parse(res._body);


        if (parsedBody.apiStatus == 0) {
          console.log("login response" + JSON.stringify(parsedBody));
          this.storeService.putObject("token_cleared", false);
          this.storeService.putObject("token", parsedBody.Result.access_token);
          this.ApiServicesService.headerAppendedPost('api/Login/GetSessionContext', '').then(res => {
            let parsedBody = JSON.parse(res._body);
            this.storeService.putObject("advisorId", parsedBody.Result.advisorID);
            this.storeService.putObject("Role", parsedBody.Result.Role);
            this.storeService.putObject("UserID", parsedBody.Result.UserID);
            this.storeService.putObject('clientId', parsedBody.Result.ClientID);
            this.storeService.putObject("hasClients", parsedBody.Result.hasClients);
            if (parsedBody.Result.Role == "Advisor") {
              this.router.navigateByUrl('/home');
            }
            else if (parsedBody.Result.Role == "Client") {


              /**Getting Profile Id For The Client  */
              let req = {
                "clientId": this.storeService.getObject('clientId')
              }
              this.ApiServicesService.headerAppendedPost('api/Client/showClientProfile', req).then(res => {
                let parsedBody = JSON.parse(res._body);
                if (parsedBody.apiStatus == 0) {
                  this.profileID = parsedBody.Result.profile.profileID;
                  this.storeService.putObject("profileID", this.profileID);
                  this.router.navigate(['client-details-info-board'], { queryParams: { page: this.storeService.getObject('clientId') } });
                }
              })
              // if(parsedBody.Result.hasClients){
              // this.storeService.putObject("hasClient",true);
              // this.router.navigateByUrl('/app-client-list');
              // }
              // else{
              // this.storeService.putObject("hasClient",false);
              // // this.router.navigateByUrl('/client-list-new');
              // this.router.navigateByUrl('/app-client-list');
              // }
            }
          })
            .catch(error => {
              this.notificationService.error(error);
            })

        }
        else if (parsedBody.apiStatus == 1) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Message);
        }
      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body)
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error('Password or username might be wrong');
        }
        else {
          this.notificationService.error('Password or username might be wrong');
        }
      });
  }


  addprop(e) {

    if (e.target.checked) {
      this.storeService.putObject('userAuth', this.signInModel);
    }
    else {
      this.storeService.putObject('userAuth', "");

    }
  }
  onBlurMethod(val: any) {

    return this.signInModel.email = val.split(' ').join('');
  }
  onBlurpwdMethod(val: any) {
    return this.signInModel.password = val.split(' ').join('');
  }
}
