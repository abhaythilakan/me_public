import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { ModalsService } from '../services/modals.service';
import { Router } from '@angular/router';
import { PayrollTaxEditComponent } from '../payroll-tax-edit/payroll-tax-edit.component';
import { window } from 'rxjs/operator/window';
import { OtherIncomeModalComponent } from '../other-income-modal/other-income-modal.component';
import { OtherTaxModalComponent } from '../other-tax-modal/other-tax-modal.component';
import { Window } from 'selenium-webdriver';
import { OtherTaxEditComponent } from '../other-tax-edit/other-tax-edit.component';
import { DeleteTaxModalComponent } from '../delete-tax-modal/delete-tax-modal.component';
import * as enLocale from 'date-fns/locale/en';
import * as frLocale from 'date-fns/locale/fr';
import { ConfirmationComponent } from '../confirmation/confirmation.component';
import { BaseChartDirective } from 'ng2-charts';
import * as Highcharts from 'highcharts';


@Component({
  selector: 'app-income-tab',
  templateUrl: './income-tab.component.html',
  styleUrls: ['./income-tab.component.scss']
})
export class IncomeTabComponent implements OnInit {
  investedChartforOther: { title: { text: string; }; chart: { zoomType: string; }; xAxis: { categories: any[]; title: { text: string; }; }; yAxis: { title: { text: string; }; }; series: { name: string; data: any[]; }[]; };
  investedChartforSpouse: { title: { text: string; }; chart: { zoomType: string; }; xAxis: { categories: any[]; title: { text: string; }; }; yAxis: { title: { text: string; }; }; series: { name: string; data: any[]; }[]; };
  investedChart: { title: { text: string; }; chart: { zoomType: string; }; xAxis: { categories: any[]; title: { text: string; }; }; yAxis: { title: { text: string; }; }; series: { name: string; data: any[]; }[]; };
  public IncomeProjectionDetails = [{
    "year": null,
    "age": null,
    "amount": null,
    "Tax": null
  }]
  public incomeProjectionData = {
    "primary": {
      "IncomeProjectionDetails": this.IncomeProjectionDetails
    },
    "spouse": {
      "IncomeProjectionDetails": this.IncomeProjectionDetails
    },
    "other": {
      "IncomeProjectionDetails": this.IncomeProjectionDetails
    },
  }
  spouseName: any;
  spouseRetirementAge: any;
  spouseGrossincome: any;

  endAge: boolean;
  startAge: boolean;
  showOther: boolean;
  showSpouse: boolean;
  showPrimary: boolean;
  showGraphSection: boolean;
  visibleStartDate: boolean;
  changedEndDate: any;
  changedStartDate: any;
  futureIncomeendTypeDDL: any;
  futureIncomeStartDateType: any;
  otherRetirementTotal: any;
  public projection: any = {
    "projection_percentage": "",
    "projection_percentagetwo": ""
  };
  public primaryData = [{
    "year": "",
    "age": "",
    "amount": ""
  }];
  public spouseData = [{
    "year": "",
    "age": "",
    "amount": ""
  }];
  public otherData = [{
    "year": "",
    "age": "",
    "amount": ""
  }];
  TotalIncome: any;
  otherTotal: any;
  spouseTotal: any;
  primaryTotal: any;
  // lineChartData: { data: number[]; label: string; }[];

  totalSumPimary: any;

  public primaryYearLabels: Array<any> = [];
  public spouseYearLabels: Array<any> = [];

  public otherYearLabels: Array<any> = [];

  public primaryAmountData: Array<any> = [];
  public primaryAmountDataUpdated: Array<any> = [];
  public spouseAmountDataUpdated: Array<any> = [];
  public otherAmountDataUpdated: Array<any> = [];

  public spouseAmountData: Array<any> = [];
  public otherAmountData: Array<any> = [];
  public TotalIncomeArray: Array<any> = [];

  // public incomeProjectionData = [{
  //   "name": "",
  //   "role": "",
  //   "IncomeProjectionDetails": [{
  //     "year": "",
  //     "age": "",
  //     "amount": ""
  //   }],
  //   "totalSum": "",
  //   "otherTotalIncomeBeforeRet": ""
  // }];
  options = {
    locale: enLocale,
    minYear: 1970,
    maxYear: 2030,
    displayFormat: 'MM-DD-YYYY',
    barTitleFormat: 'MMMM YYYY',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    // minDate: new Date(Date.now()), // Minimal selectable date
    maxDate: new Date(Date.now())
  };
  startDateArr: any[];
  OtherTaxTypes: any;
  getOtherTaxModal: any;
  hideTable: boolean;
  incomeField: any;
  endDate: string;
  arrayFinal: any[];
  arrIndex = {};
  Day: any;
  startDate: any;
  finalFutureIncome: any = {};
  fields: Object = {};
  payIncome: Object = {};
  changedPayperiod: any;
  PayPeriods: any;
  monthlyIncomeData: any;
  sizeOfTable: any;
  tableShow: boolean;
  page: number;
  closeResult: string;

  public income = {
    "payIncome": ""
  }
  arr = [];
  public futureIncome = [
    {
      "incomeID": "",
      "profileID": "",
      "personID": "",
      "name": "",
      "grossMonthlyIncome": "",
      "payType": "",
      "payIncome": "",
      "payRollTaxes": "",
      "homePay": "",
      "allowances": "",
      "notes": "",
      "isActive": "",
      "cola": "",
      "startType": null,
      "startDate": new Date(),
      "endType": null,
      "endDate": new Date(),
      "isFutureIncome": "",
      "incomeCreatedDate": "",
      "incomeUpdateDate": "",
      "incomeModifiedBy": "",
      "startAge": null,
      "endAge": null

    }
  ]
  constructor(private modalService: NgbModal, private storeService: StoreServicesService, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, public modal: ModalsService, private router: Router) { }

  //modal one
  open(content) {
    this.modalService.open(content, {
      size: 'lg'
    }).result.then((result) => {
      this.closeResult = 'Closed with: ${result}';
    }, (reason) => {
      this.closeResult = 'Dismissed ${this.getDismissReason(reason)}';
    });
  }

  ngOnInit() {

    this.startAge = false;
    this.endAge = false;
    this.page = 0;
    this.showPrimary = false;
    this.showSpouse = false;
    this.showOther = false;
    this.hideTable = false;
    this.visibleStartDate = false;
    this.showGraphSection = true;
    this.listMonthlyIncome(this.page);
    this.getPayperiod();
    this.getFutureIncome();
    this.GetOthertax();
    this.getOtherTaxTypes();
    this.getIncomeProjection();
    this.projection;
    this.startDatedropdown();
    this.endDateDropDown();
    this.primaryTotal = 0;
    this.spouseTotal = 0;
    this.otherRetirementTotal = 0;

  }

  payPeriodChanged(val: any) {
    this.changedPayperiod = val;
  }
  startdateDDChanged(val: any, i) {
    this.changedStartDate = val;
    this.futureIncome[i].startType = val;
    if (this.futureIncome[i].startType != 0) {
      this.startAge = true;
    }
    else if (this.futureIncome[i].startType == 0) {
      this.startAge = false;
    }
  }
  enddateDDChanged(val: any, i) {
    this.changedEndDate = val;
    this.futureIncome[i].endType = val;
    if (this.futureIncome[i].endType != 0) {
      this.endAge = true;
    }
    else if (this.futureIncome[i].endType == 0) {
      this.endAge = false;
    }
  }

  startDatedropdown() {


    this.ApiServicesService.headerAppendedPost('api/IncomeDetails/getfutureIncomeStartDateType', this.storeService.getObject("profileID")).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.futureIncomeStartDateType = parsedBody.Result;
      }
    })
  }
  endDateDropDown() {
    this.ApiServicesService.headerAppendedPost('api/IncomeDetails/getfutureIncomeendTypeDDL', this.storeService.getObject("profileID")).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.futureIncomeendTypeDDL = parsedBody.Result;
      }
    })
  }
  getPayperiod() {
    this.ApiServicesService.headerAppendedGet('api/Common/GetPayperiod').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.PayPeriods = parsedBody.Result;

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }
  listMonthlyIncome(pageno) {
    this.page = pageno;
    this.ApiServicesService.headerAppendedPost('api/IncomeDetails/GetMonthlyIncome', this.storeService.getObject("profileID")).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.tableShow = true;
        // this.sizeOfTable=parsedBody.Result.Collection.TotalCount;
        this.monthlyIncomeData = parsedBody.Result;
      }
    })
  }
  editPayrollTax(incomeID) {
    this.modal.openModal(PayrollTaxEditComponent, 'lg', 'modal-xl');
    this.storeService.putObject("incomeID", incomeID);
    this.modal.resultFunction().then(res => {
      this.listMonthlyIncome(this.page);
      this.getFutureIncome();
    })
  }
  saveMonthlyIncome() {
    let req = this.monthlyIncomeData;
    this.ApiServicesService.headerAppendedPost('api/IncomeDetails/saveMonthlyIncome', req).then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.listMonthlyIncome(this.page);
          this.notificationService.success('Monthly Income Saved Successfully');
          this.getIncomeProjection();

        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Message);
        }
        else if (parsedBody.apiStatus == 1) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }
  /**Get Future Income */
  getFutureIncome() {
    this.ApiServicesService.headerAppendedPost('api/IncomeDetails/getFutureIncomeDetails', this.storeService.getObject("profileID")).then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {

          this.futureIncome = parsedBody.Result;
          this.startDateArr = [];

          this.incomeField = parsedBody.Result[0].incomeID;
          console.log("income" + this.incomeField);
          if (this.incomeField == "" || this.incomeField == null) {
            this.hideTable = false;
          }
          else {
            this.hideTable = true;
          }
        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Message);
        }
      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }

  addOrReplace(object) {
    var index = this.arrIndex[object.pageId];
    if (index === undefined) {
      index = this.arr.length;
      this.arrIndex[object.pageId] = index;
    }
    this.arr[index] = object;
  }

  openOtherIncomemodal(lg) {

    this.modal.openModal(OtherIncomeModalComponent, 'lg', '');
    this.modal.resultFunction().then(res => {
      this.getFutureIncome();

    })
  }
  /**Income Get Other Tax */
  GetOthertax() {

    this.ApiServicesService.headerAppendedPost('api/IncomeDetails/getotherTaxDetails', this.storeService.getObject('profileID')).then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {

          this.getOtherTaxModal = parsedBody.Result.otherTaxDetails;
        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Message);
        }
      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }
  /**Get Other Taxes Types  */
  getOtherTaxTypes() {
    this.ApiServicesService.headerAppendedGet('api/Common/GetOtherTaxes').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.OtherTaxTypes = parsedBody.Result;

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }
  addOtherTax() {
    this.modal.openModal(OtherTaxModalComponent, 'lg', '');
    this.modal.resultFunction().then(res => {

      this.GetOthertax();
    })

  }
  /**Edit Other Tax */
  editOtherTax(otherTax) {
    console.log("otherTax" + JSON.stringify(otherTax));
    this.modal.openModal(OtherTaxEditComponent, 'lg', '');
    this.storeService.putObject('otherTax', otherTax);
    this.modal.resultFunction().then(res => {

      this.GetOthertax();
    })
  }
  /**Delete Other Tax */
  deleteOtherTax(otherTax) {
    this.modal.openModal(DeleteTaxModalComponent, '', '');
    // this.storeService.putObject("otherTaxToBeDeleted", otherTax);
    this.modal.resultFunction().then(res => {


      let header = {
        "Content-Type": "application/json"
      }

      this.
        ApiServicesService.headerAppendedPost('api/IncomeDetails/DeleteTaxes', otherTax.taxDetailID)
        .then(res => {
          let parsedBody = JSON.parse(res._body);
          if (parsedBody.apiStatus == 0) {
            this.GetOthertax();
          }
        })
        .catch(err => {
          if (err && err._body && !err._body.target) {
            let parsedBody = JSON.parse(err._body);
            if (parsedBody.error && parsedBody.message)
              this.notificationService.error(parsedBody.message);
            else
              this.notificationService.error('Could not Be Deleted');
          } else {
            this.notificationService.error('Could not Be Deleted');
          }
        });

    })
  }
  removeOtherTax(tax, index) {
    this.modal.openModal(ConfirmationComponent, '', '');

    this.modal.resultFunction().then(res => {
      if (tax.incomeID == 0) {
        this.futureIncome.splice(index, index);
      }
      else {
        let req = tax.incomeID;
        let header = {
          "Content-Type": "application/json"
        }

        this.
          ApiServicesService.headerAppendedPost('api/IncomeDetails/DeleteFutureIncome', req)
          .then(res => {
            let parsedBody = JSON.parse(res._body);
            if (parsedBody.apiStatus == 0) {
              this.getFutureIncome();
              this.getIncomeProjection();
            }
          })
          .catch(err => {
            if (err && err._body && !err._body.target) {
              let parsedBody = JSON.parse(err._body);
              if (parsedBody.error && parsedBody.message)
                this.notificationService.error(parsedBody.message);
              else
                this.notificationService.error('Could not register');
            } else {
              this.notificationService.error('Could not register');
            }
          });
      }
    })
  }

  saveWholeOtherIncome() {
    for (let i = this.futureIncome.length - 1; i >= 0; --i) {
      if (this.futureIncome[i].grossMonthlyIncome == "") {
        this.futureIncome.splice(i, 1);
      }
    }
    console.log("Future Data going to saved" + JSON.stringify(this.futureIncome));
    this.ApiServicesService.headerAppendedPost('api/IncomeDetails/saveFutureIncome', this.futureIncome).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.notificationService.success("Income Saved Successfully");
        this.getFutureIncome();
        this.getIncomeProjection();
      }
      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);
      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
      }
    })
  }

  addOtherIncome() {
    this.futureIncome = this.futureIncome || [];
    this.futureIncome.push(
      {
        "incomeID": "0",
        "profileID": this.storeService.getObject('profileID'),
        "personID": "",
        "name": "",
        "grossMonthlyIncome": "",
        "payType": "",
        "payIncome": "",
        "payRollTaxes": "",
        "homePay": "",
        "allowances": "",
        "notes": "",
        "isActive": "",
        "cola": "",
        "startType": null,
        "startDate": new Date(),
        "endType": null,
        "endDate": new Date(),
        "isFutureIncome": "",
        "incomeCreatedDate": "",
        "incomeUpdateDate": "",
        "incomeModifiedBy": "",
        "startAge": null,
        "endAge": null

      })
  }

  /**Income Projection Tab */
  public getIncomeProjection() {

    this.ApiServicesService.headerAppendedPost('api/IncomeDetails/IncomeProjection', this.storeService.getObject("profileID")).then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);

        if (parsedBody.apiStatus == 0 && parsedBody.Result) {
          this.showGraphSection = true;
          this.incomeProjectionData = parsedBody.Result;
          this.incomeProjectionData.primary = this.incomeProjectionData.primary;
          this.incomeProjectionData.spouse = this.incomeProjectionData.spouse;
          this.incomeProjectionData.other = this.incomeProjectionData.other;
          if (this.incomeProjectionData) {
            this.loadCharts();
          }

        }
        else if (!parsedBody.Result || parsedBody.Result.length == 0 || parsedBody.Result == " ") {
          this.showGraphSection = false;
        }
        else if (parsedBody.apiStatus == 1) {
          this.showGraphSection = false;
          this.notificationService.error(parsedBody.Result.ValidationErrors.FieldErrorMessage);
        }

        else if (parsedBody.apiStatus == 2) {
          this.showGraphSection = false;
          this.notificationService.error(parsedBody.Message);
        }

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })

  }

  loadCharts(): any {
    // this.GraphDisplayChange();
    let annA = [];
    let annB = [];
    let accA = [];

    let age = [];
    let spouseAge = [];
    let year = [];
    this.incomeProjectionData.primary.IncomeProjectionDetails.forEach(item => annA.push(item.amount));
    this.incomeProjectionData.spouse.IncomeProjectionDetails.forEach(item => annB.push(item.amount));
    this.incomeProjectionData.other.IncomeProjectionDetails.forEach(item => accA.push(item.amount));


    if (this.incomeProjectionData.primary.IncomeProjectionDetails[0].age !== null) {
      this.incomeProjectionData.primary.IncomeProjectionDetails.forEach(item => age.push(item.age));
    }
    else {
      let i = 1;
      this.incomeProjectionData.primary.IncomeProjectionDetails.forEach(item => age.push(i++));;
    }
    if (this.incomeProjectionData.spouse.IncomeProjectionDetails[0].age !== null) {

      this.incomeProjectionData.spouse.IncomeProjectionDetails.forEach(item => spouseAge.push(item.age));
    }
    else {
      let i = 1;
      this.incomeProjectionData.spouse.IncomeProjectionDetails.forEach(item => spouseAge.push(i++));;
    }
    if (this.incomeProjectionData.other.IncomeProjectionDetails[0].year !== null) {
      this.incomeProjectionData.other.IncomeProjectionDetails.forEach(item => year.push(item.year));
    }
    else {
      let i = 1;
      this.incomeProjectionData.other.IncomeProjectionDetails.forEach(item => year.push(i++));;
    }
    Highcharts.setOptions({
      lang: {
        thousandsSep: ','
      }
    })
    this.investedChart = {
      title: { text: 'Primary' },
      chart: { zoomType: 'x' },
      xAxis: {
        categories: age,
        title: {
          text: "Age"
        },
      },
      yAxis: {
        title: {
          text: "Amount"
        }
      },
      series: [
        { name: 'Primary', data: annA },
        // { name: 'Early Spendor', data: annB }
      ]
    };
    this.investedChartforSpouse = {
      title: { text: '' },
      chart: { zoomType: 'x' },
      xAxis: {
        categories: spouseAge,
        title: {
          text: "Age"
        },
      },
      yAxis: {
        title: {
          text: "Amount"
        }
      },

      series: [
        { name: 'Spouse', data: annB },

      ]
    };
    this.investedChartforOther = {
      title: { text: '' },
      chart: { zoomType: 'x' },
      xAxis: {
        categories: year,
        title: {
          text: "Year"
        },
      },
      yAxis: {
        title: {
          text: "Amount"  
        }
      },

      series: [
        { name: 'Other Income', data: accA },

      ]
    };
  }



  public randomize(rate): void {

    let req =
      {

        "profileID": this.storeService.getObject("profileID"),
        "goRate": rate

      }
    this.ApiServicesService.headerAppendedPost('api/IncomeDetails/IncomeProjectionGo', req).then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.incomeProjectionData = parsedBody.Result;
          this.incomeProjectionData.primary = this.incomeProjectionData.primary;
          this.incomeProjectionData.spouse = this.incomeProjectionData.spouse;
          this.incomeProjectionData.other = this.incomeProjectionData.other;
          if (this.incomeProjectionData) {
            this.loadCharts();
          }

        }
        else if (parsedBody.apiStatus == 1) {
          this.notificationService.error(parsedBody.Result.ValidationErrors.FieldErrorMessage);
        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Message);
        }

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })

  }

}
