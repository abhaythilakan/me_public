import { Component, OnInit } from '@angular/core';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { StoreServicesService } from '../services/store-services.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  public forgotPwdModel: any = {
       email: ""
     };
  constructor(public ApiServicesService:ApiServicesService, private notificationService: NotificationServicesService,
    private storeService:StoreServicesService,private router: Router) { }

  ngOnInit() {
  }
  public forgot(): void {

    let req=
    {
      "email":this.forgotPwdModel.email
  }
    let header={
      "Content-Type":"application/json"
     }
   
    this.
    ApiServicesService.post('api/ForgotPassword', req,{ headers: header})
      .then(res => {
        let parsedBody=JSON.parse(res._body);
        if(parsedBody.apiStatus==0){
          this.notificationService.success(parsedBody.Result.Message);
          this.router.navigateByUrl('/app-login');
        }
      else if(parsedBody.apiStatus==1){
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
      } 
      else if(parsedBody.apiStatus==2){
        this.notificationService.error(parsedBody.Result.Message);
      }
      })
          .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error('Unable to send mail');
        } else {
          this.notificationService.error('Please check the email');
        }
      });

  }
    
}
