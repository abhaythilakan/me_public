import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { error } from 'util';
import { ModalsService } from '../services/modals.service';
import { ConfirmationComponent } from '../confirmation/confirmation.component';
import { Http } from '@angular/http';
@Component({
  selector: 'app-global-settings',
  templateUrl: './global-settings.component.html',
  styleUrls: ['./global-settings.component.scss']
})
export class GlobalSettingsComponent implements OnInit {
  publicAccessSettingsnw: { "perId": string; "advisorId": string; "clientId": string; "profileId": string; "isRead": string; "isEdit": string; "pageId": string; "apiStatus": string; }[];
  isThreeMontha: boolean;
  arr = [];
  arrIndex = {};
  isThreeMonth: boolean;
  publicAccessSettings: any[] = [];
  publicAccessSettingsTwo: any[] = [];
  itemsTotal: any;
  totalCollegeSerachedUsingState: any;
  collegeNameByState: any;
  statesForsearch: any;
  next: boolean;
  favoriteColleges: any;
  favouriteCollege: boolean;
  collegename: any;
  page: any;
  totalCollegeSerachedUsingName: any;
  stateBasedsearch: boolean;
  fullsearch: boolean;
  nameBasedsearch: boolean;
  colleges: any;
  collegesNotfound: boolean;
  tableShow: boolean;
  loader: boolean;
  specificnoofyears: string;
  specificnoofyearsBox: boolean;
  timeZone: any;
  changedTimezone: any;
  defaultstate: any;
  changedState: any;
  currency: any;
  changedDropdowncurrency: any;
  yearAfterDeath: any;
  changedDropdownyearDeath: any;
  yearAfterdeath: any;
  lifeExpectencies: any;
  selectedElement: any;
  selectedTimezone: any;
  selectedState: any;
  selectedLifeExpectency: any;
  states: any;
  currencies: any;
  timezones: any;
  monthsArray: { "id": number; "name": string; }[];
  publicDeals: any[] = [];
  closeResult: string;


  public clientModal = {

    "usrCat": null,
    "client": {
      "clientId": "",
      "firstName": "",
      "lastName": "",
      "advisorId": "",
      "createdDate": ""
      , "imageString": "",
      "imagePath": "",
      "password": "",
      "userId": "",
      "userName": "",
      "reportType": "",
      "isActive": "",
      "advisorFirstName": "",
      "advisorLastName": "",
      "emailPrimary": "",
      "clientDOB": "",
      "clientDOBString": "",
      "clientGender": "",
      "advisorDDL": "",
      "reportTypeDDL": "",
      "stateDDL": "",
      "currencyDDL": "",
      "apiStatus": ""
    },
    "profile": {
      "profileID": "",
      "profileDate": "",
      "notes": "",
      "profileName": "",
      "clientID": "",
      "isClientAccess": "",
      "isActive": "",
      "designMode": "",
      "formattedActionPlan": "",
      "reportType": "",
      "isReportDateChanged": "",
      "finYear": "",
      "createdDate": "",
      "apiStatus": ""
    },
    "global": {
      "defultID": "",
      "profileID": "",
      "clientID": "",
      "advisorID": "",
      "timeZone": "",
      "currency": "",

      "defaultstate": ""
    },
    "ret": {
      "RetirementDefaultID": "",
      "profileID": "",
      "clientID": "",
      "advisorID": "",
      "retireAge": "",
      "lifeExpectency": "",
      "preRetReturnTax": "",
      "retTax": "",
      "retReturnTax": "",
      "monthIncome": "",
      "inflation": ""
    },
    "Income": {
      "defaultID": "",
      "profileID": "",
      "clientID": "",
      "advisorID": "",
      "primaryInflation": "",
      "spouseInflation": "",
      "inflation": ""
    },
    "debt": {
      "defaultID": "",
      "profileID": "",
      "clientID": "",
      "advisorID": "",
      "minPay": "",
      "unpaidPercentage": '',
      "inflation": ""
    },
    "emerg": {
      "defaultID": "",
      "profileID": "",
      "clientID": "",
      "advisorID": "",
      "emergencyFundValue": "",
      "rateA": "",
      "rateB": "",
      "rateC": "",
      "isPieChart": "",
      "isThreeMonth": "",
      "inflation": "",
      "isBarChart": "",
      "isSixMonth": ""
    },
    "insu": {
      "defaultID": "",
      "profileID": "",
      "clientID": "",
      "advisorID": "",
      "cost": "",
      "monthlyIncome": "",
      "yearAfterDeath": "",
      "specificnoofyears": "",
      "retAsset": "",
      "otherRetAsset": "",
      "cashAccounts": "",
      "spouseRetAsset": "",
      "nonRetAsset": "",
      "inflation": ""
    },
    "edu": {
      "defaultID": "",
      "profileID": "",
      "clientID": "",
      "advisorID": "",
      "collegeStartMonth": "",
      "tutionFeeInflation": "",
      "collegeAge": "",
      "collageYears": "",
      "parentPay": "",
      "rateA": "",
      "rateB": "6",
      "rateC": ""
    },
    "permissionList": [
      {
        "perId": "",
        "advisorId": "",
        "clientId": "",
        "profileId": "",
        "isRead": "",
        "isEdit": "",
        "pageId": "",
        "apiStatus": ""
      },
      {
        "perId": "",
        "advisorId": "",
        "clientId": "",
        "profileId": "",
        "isRead": "",
        "isEdit": "",
        "pageId": "",
        "apiStatus": "0"
      },
      {
        "perId": "",
        "advisorId": "",
        "clientId": "",
        "profileId": "",
        "isRead": "",
        "isEdit": "",
        "pageId": "",
        "apiStatus": ""
      },
      {
        "perId": "",
        "advisorId": "",
        "clientId": "",
        "profileId": "",
        "isRead": "",
        "isEdit": "",
        "pageId": "",
        "apiStatus": ""
      },
      {
        "perId": "",
        "advisorId": "",
        "clientId": "",
        "profileId": "",
        "isRead": "",
        "isEdit": "",
        "pageId": "",
        "apiStatus": ""
      },
      {
        "perId": "",
        "advisorId": "",
        "clientId": "",
        "profileId": "",
        "isRead": "",
        "isEdit": "",
        "pageId": "",
        "apiStatus": ""
      },
      {
        "perId": "",
        "advisorId": "",
        "clientId": "",
        "profileId": "",
        "isRead": "",
        "isEdit": "",
        "pageId": "",
        "apiStatus": ""
      }],
    "selectedElement": {
      "ID": "",
      "TEXT": ""
    },
    "selectedTimezone": {
      "ID": "",
      "TEXT": ""
    },
    "selectedState": {
      "ID": "",
      "TEXT": ""
    },
    "selectedLifeExpectency": {
      "VALUE": 3,
      "TEXT": "Specific number of years"
    },
    "specificnoofyears": ""
  }
  constructor(private modalService: NgbModal, private storeService: StoreServicesService, public ApiServicesService: ApiServicesService, private notificationService: NotificationServicesService,
    private router: Router, private _location: Location, public http: Http, public modal: ModalsService) { }

  open(content) {
    this.modalService.open(content, {
      size: 'lg'
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit() {
    /** get timezone api */
    this.changedDropdownyearDeath = "";
    this.changedDropdowncurrency = "";
    this.changedState = "";
    this.changedTimezone = "";
    this.specificnoofyearsBox = false;
    this.favouriteCollege = false;
    this.showFavouriteColleges();
    this.next = false;
    this.tableShow = false;
    this.monthsArray = [
      {
        "id": 1,
        "name": "Janaury"
      },
      {
        "id": 2,
        "name": "February"
      }, {
        "id": 3,
        "name": "March"
      }, {
        "id": 4,
        "name": "April"
      }, {
        "id": 5,
        "name": "May"
      }, {
        "id": 6,
        "name": "June"
      }, {
        "id": 7,
        "name": "July"
      }, {
        "id": 8,
        "name": "August"
      }, {
        "id": 9,
        "name": "September"
      }, {
        "id": 10,
        "name": "October"
      }, {
        "id": 11,
        "name": "November"
      }, {
        "id": 12,
        "name": "December"
      },
    ]

    for (let i = 0; i <= this.monthsArray.length; i++) {
      this.selectedElement = {
        "name": "September",
        "id": 9
      };
    }
    this.ApiServicesService.get('api/Common/GetTimeZone').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.timezones = parsedBody.Result;

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
    /** get currency api */
    this.ApiServicesService.get('api/Common/GetCurrencies').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.currencies = parsedBody.Result;

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })

    /**get states api */
    this.ApiServicesService.get('api/Common/GetState').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.states = parsedBody.Result;
        this.statesForsearch = parsedBody.Result;

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
    /**get life expectancy */
    this.ApiServicesService.get('api/Common/GetIncomeAfterDeath').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.lifeExpectencies = parsedBody.Result;
      }
    })

    /**Get Global Settings Details */
    this.ApiServicesService.headerAppendedPost('api/Client/GetClientDetails', this.storeService.getObject("clientId")).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.clientModal = parsedBody.Result;
        console.log("client Modal" + JSON.stringify(this.clientModal));
        this.clientModal.selectedElement = {
          "ID": this.clientModal.global.currency,
          "TEXT": ""
        }
        this.clientModal.selectedTimezone = {
          "ID": this.clientModal.global.timeZone,
          "TEXT": ""
        }
        this.clientModal.selectedState = {
          "ID": this.clientModal.global.defaultstate,
          "TEXT": ""
        }
        if (this.clientModal.insu.specificnoofyears) {
          this.specificnoofyearsBox = true;
          this.clientModal.specificnoofyears = this.clientModal.insu.specificnoofyears;
        }
        else {
          this.specificnoofyearsBox = false;
          this.clientModal.specificnoofyears = "0";
        }
      }
    })
      .catch(error => {
        this.notificationService.error("error occured" + error);
      })
  }
  backClicked() {
    this._location.back();
  }
  clientList() {
    this.router.navigate(["app-client-list"]);

  }
  saveSettings() {
    if (this.changedDropdownyearDeath != "") {
      this.yearAfterDeath = this.changedDropdownyearDeath;

    }
    else if (this.changedDropdownyearDeath == "") {
      this.yearAfterDeath = this.clientModal.insu.yearAfterDeath;
    }
    if (this.changedDropdowncurrency != "") {
      this.currency = this.changedDropdowncurrency;
    }
    else if (this.changedDropdowncurrency == "") {
      this.currency = this.clientModal.global.currency;
    }
    if (this.changedState != "") {
      this.defaultstate = this.changedState;
    }
    else if (this.changedState == "") {
      this.defaultstate = this.clientModal.global.defaultstate;
    }
    if (this.changedTimezone != "") {
      this.timeZone = this.changedTimezone;
    }
    else if (this.changedTimezone == "") {
      this.timeZone = this.clientModal.global.timeZone;
    }


    /**api/Client/saveClientDetails */
    if (this.arr.length == 0) {
      this.publicAccessSettingsnw = this.clientModal.permissionList;
    }
    else {
      this.publicAccessSettingsnw = this.arr;
    }
    let req = {


      "client":
        {
          "clientId": this.storeService.getObject("clientId")
        },
      "global":
        {
          "defultID": this.clientModal.global.defultID,
          "profileID": this.clientModal.global.profileID,
          "clientID": this.storeService.getObject("clientId"),
          "advisorID": this.storeService.getObject("advisorId"),
          "timeZone": this.changedTimezone,
          "currency": this.changedDropdowncurrency,
          "defaultstate": this.changedState
        },
      "ret":
        {
          "RetirementDefaultID": this.clientModal.global.defultID,
          "profileID": this.clientModal.global.profileID,
          "clientID": this.clientModal.global.clientID,
          "advisorID": this.storeService.getObject("advisorId"),
          "retireAge": this.clientModal.ret.retireAge,
          "lifeExpectency": this.clientModal.ret.lifeExpectency,
          "preRetReturnTax": this.clientModal.ret.preRetReturnTax,
          "retTax": this.clientModal.ret.retTax,
          "retReturnTax": this.clientModal.ret.retReturnTax,
          "monthIncome": this.clientModal.ret.monthIncome,
          "inflation": this.clientModal.ret.inflation,
        },
      "Income":
        {
          "defaultID": this.clientModal.Income.defaultID,
          "profileID": this.clientModal.Income.profileID,
          "clientID": this.clientModal.Income.clientID,
          "advisorID": this.storeService.getObject("advisorId"),
          "primaryInflation": this.clientModal.Income.primaryInflation,
          "spouseInflation": this.clientModal.Income.spouseInflation,
          "inflation": this.clientModal.Income.inflation

        },
      "debt":
        {
          "defaultID": this.clientModal.debt.defaultID,
          "profileID": this.clientModal.debt.profileID,
          "clientID": this.clientModal.debt.clientID,
          "advisorID": this.storeService.getObject("advisorId"),
          "minPay": this.clientModal.debt.minPay,
          "unpaidPercentage": this.clientModal.debt.unpaidPercentage,
          "inflation": this.clientModal.debt.inflation

        },
      "emerg":
        {
          "isBarChart": this.clientModal.emerg.isBarChart,
          "isSixMonth": this.clientModal.emerg.isSixMonth,
          "defaultID": this.clientModal.emerg.defaultID,
          "profileID": this.clientModal.emerg.profileID,
          "clientID": this.clientModal.emerg.clientID,
          "advisorID": this.storeService.getObject("advisorId"),
          "emergencyFundValue": this.clientModal.emerg.emergencyFundValue,
          "rateA": this.clientModal.emerg.rateA,
          "rateB": this.clientModal.emerg.rateB,
          "rateC": this.clientModal.emerg.rateC,
          "isPieChart": this.clientModal.emerg.isPieChart,
          "isThreeMonth": this.clientModal.emerg.isThreeMonth,
          "inflation": this.clientModal.emerg.inflation
        },
      "insu":
        {
          "defaultID": this.clientModal.insu.defaultID,
          "profileID": this.clientModal.insu.profileID,
          "clientID": this.clientModal.insu.clientID,
          "advisorID": this.storeService.getObject("advisorId"),
          "cost": this.clientModal.insu.cost,
          "monthlyIncome": this.clientModal.insu.monthlyIncome,
          "yearAfterDeath": this.yearAfterDeath,
          "specificnoofyears": this.clientModal.specificnoofyears,
          "retAsset": this.clientModal.insu.retAsset,
          "otherRetAsset": this.clientModal.insu.otherRetAsset,
          "cashAccounts": this.clientModal.insu.cashAccounts,
          "spouseRetAsset": this.clientModal.insu.spouseRetAsset,
          "nonRetAsset": this.clientModal.insu.nonRetAsset,
          "inflation": this.clientModal.insu.inflation

        },
      "edu":
        {
          "defaultID": this.clientModal.edu.defaultID,
          "profileID": this.clientModal.edu.profileID,
          "clientID": this.clientModal.edu.clientID,
          "advisorID": this.storeService.getObject("advisorId"),
          // "collegeStartMonth":"",
          // "tutionFeeInflation":"",
          // "collegeAge":"",
          // "collageYears":"",
          // "parentPay":"",
          // "rateA":"",
          // "rateB":"",
          // "rateC":""
          "collegeStartMonth": this.clientModal.edu.collegeStartMonth,
          "tutionFeeInflation": this.clientModal.edu.tutionFeeInflation,
          "collegeAge": this.clientModal.edu.collegeAge,
          "collageYears": this.clientModal.edu.collageYears,
          "parentPay": this.clientModal.edu.parentPay,
          "rateA": this.clientModal.edu.rateA,
          "rateB": this.clientModal.edu.rateB,
          "rateC": this.clientModal.edu.rateC,
          "inflation": ""
        },
      "permissionList": this.publicAccessSettingsnw

    }

    console.log("body" + JSON.stringify(req));

    this.ApiServicesService.headerAppendedPost('api/Client/saveClientDetails', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.notificationService.success('client settings saved successfully');
        this.router.navigate(["app-client-list"]);
      }
    })
  }
  yearAfterDeathChanged(val: any) {
    console.log(val);
    this.changedDropdownyearDeath = val;
    if (this.changedDropdownyearDeath == 3) {
      this.specificnoofyearsBox = true;
      this.clientModal.specificnoofyears = this.clientModal.insu.specificnoofyears;
    }
    else {
      this.specificnoofyearsBox = false;
      this.clientModal.specificnoofyears = "0";
    }
  }
  currencyChanged(val: any) {
    console.log(val);
    this.changedDropdowncurrency = val;
  }
  stateChanged(val: any) {
    console.log("state changed" + val);
    this.changedState = val;
  }
  timezoneChanged(val: any) {
    console.log("timezone changed" + val);
    this.changedTimezone = val;
  }


  public searchCollege(collegename, pageno) {
    this.loader = true;
    let req =
      {
        "searchValue": collegename,
        "PageNo": pageno,
        "PageSize": 20
      }

    let header = {
      "Content-Type": "application/json"

    }



    this.
      ApiServicesService.post('api/College/collegeSearchName', req, { headers: header })
      .then(res => {
        let parsedBody = JSON.parse(res._body);


        if (parsedBody.apiStatus == 0) {
          this.loader = false;
          this.tableShow = true;
          this.collegesNotfound = false;
          this.colleges = parsedBody.Result;
          this.nameBasedsearch = true;
          this.fullsearch = false;
          this.stateBasedsearch = false;
          //  this.itemsTotal = parsedBody.Result[0].TotalCount;
          this.totalCollegeSerachedUsingName = parsedBody.Result[0].TotalCount;

        }



      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error('Could not register');
        } else {
          this.notificationService.error('Could not register');
        }
      });
  }
  clickNameBasedSeach(e) {
    this.page = e;
    this.searchCollege(this.collegename, e);

  }
  addprop(e, college) {

    if (e.target.checked) {

      let collegeFavouriteList = []
      this.publicDeals.push(college.collegeID)         // this.collegeFavouriteList.push(college);
    }
    else {
      // alert("not checked");

    }
  }
  addToFavColleges() {
    var body = {
      "advisorId": this.storeService.getObject("advisorId"),
      "CollegeID": this.publicDeals

    };
    this.
      ApiServicesService.headerAppendedPost('api/College/saveFaveColleges', body)
      .then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.showFavouriteColleges();

        }
      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error('Could not added to  the favourite college list');
        } else {
          this.notificationService.error('Could not added to  the favourite college list');

        }
      });
  }
  showFavouriteColleges() {
    /**Load Favourite college list */

    this.ApiServicesService.headerAppendedPost('api/College/FavCollegeList', '').then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.favouriteCollege = true;
        console.log("body of favo list" + JSON.stringify(parsedBody.Result));
        this.favoriteColleges = parsedBody.Result;

      }


    })

  }

  collegeByState(collegeNameByState, pageno) {
    this.collegeNameByState = collegeNameByState;

    console.log("value" + collegeNameByState);
    let req =
      // {
      // "name" : collegeNameByState
      // }
      {

        "searchValue": collegeNameByState,
        "PageNo": pageno,
        "PageSize": 20

      }

    let header = {
      "Content-Type": "application/json"
    }

    console.log("headfer" + JSON.stringify(header));


    this.
      ApiServicesService.post('api/College/collegeSearchState', req, { headers: header })
      .then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.tableShow = true;
          this.colleges = parsedBody.Result;
          this.stateBasedsearch = true;
          this.fullsearch = false;
          this.nameBasedsearch = false;

          // this.itemsTotal = parsedBody.Result[0].TotalCount;
          this.totalCollegeSerachedUsingState = parsedBody.Result[0].TotalCount;

        }


      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error('Could not register');
        } else {
          this.notificationService.error('Could not register');
        }
      });
  }
  clickStateSearch(e) {
    this.page = e;
    // this.searchCollege(this.collegename,e);
    this.collegeByState(this.collegeNameByState, e);
    // this.getAllColleges(e)
  }
  click(e) {
    this.page = e;
    // this.searchCollege(this.collegename,e);
    // this.collegeByState(this.collegeNameByState,e);
    this.getAllColleges(e)
  }
  getAllColleges(pageno) {
    // "PageNo":1,
    // "PageSize":20
    this.loader = true;
    let req =
      {

        "PageNo": pageno,
        "PageSize": 20
      }

    let header = {
      "Content-Type": "application/json"

    }



    this.
      ApiServicesService.post('api/College/collegeSearchAll', req, { headers: header })
      .then(res => {
        let parsedBody = JSON.parse(res._body);


        if (parsedBody.apiStatus == 0) {
          this.loader = false;
          this.tableShow = true;
          this.collegesNotfound = false;
          this.colleges = parsedBody.Result;
          this.fullsearch = true;
          this.stateBasedsearch = false;
          this.nameBasedsearch = false;

          this.itemsTotal = parsedBody.Result[0].TotalCount;


        }



      })
      .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error('Could not register');
        } else {
          this.notificationService.error('Could not register');
        }
      });
  }
  addAdvancedReport(e, personal) {
    var index = this.publicAccessSettings.indexOf(personal.pageId);
    if (e.target.checked) {
      if (index === -1) {
        this.publicAccessSettings.push(personal);
        this.addOrReplace(personal);

      }
    } else {
      this.publicAccessSettings.splice(index, 1);

    }
    console.log("id of access report" + JSON.stringify(this.arr));
  }
  addOrReplace(object) {
    var index = this.arrIndex[object.pageId];
    if (index === undefined) {
      index = this.arr.length;
      this.arrIndex[object.pageId] = index;
    }
    this.arr[index] = object;
  }

  remove(item) {
    this.modal.openModal(ConfirmationComponent, item, '');
    this.storeService.putObject("collegeIDArray", item);
    this.modal.resultFunction().then(res => {

      let req = {
        //  "advisorId":item.advisorID,
        "CollegeID": [item.collegeID]
      }
      let header = {
        "Content-Type": "application/json"
      }

      this.
        ApiServicesService.headerAppendedPost('api/College/RemoveFavCollege', req)
        .then(res => {
          let parsedBody = JSON.parse(res._body);
          if (parsedBody.apiStatus == 0) {
            this.showFavouriteColleges();
          }
        })
        .catch(err => {
          if (err && err._body && !err._body.target) {
            let parsedBody = JSON.parse(err._body);
            if (parsedBody.error && parsedBody.message)
              this.notificationService.error(parsedBody.message);
            else
              this.notificationService.error('Could not register');
          } else {
            this.notificationService.error('Could not register');
          }
        });

    })


  }



}