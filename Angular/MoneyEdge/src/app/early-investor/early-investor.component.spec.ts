import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EarlyInvestorComponent } from './early-investor.component';

describe('EarlyInvestorComponent', () => {
  let component: EarlyInvestorComponent;
  let fixture: ComponentFixture<EarlyInvestorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EarlyInvestorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EarlyInvestorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
