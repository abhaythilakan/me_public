import { Component, OnInit } from '@angular/core';
import { ModalsService } from '../services/modals.service';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';

@Component({
  selector: 'app-other-income-modal',
  templateUrl: './other-income-modal.component.html',
  styleUrls: ['./other-income-modal.component.scss']
})
export class OtherIncomeModalComponent implements OnInit {
  endDate: string;
  startDate: string;
  public otherincomeModal = {
    "name": "",
    "grossMonthlyIncome": "",
    "payType": "",
    "payIncome": "",
    "payRollTaxes": "",
    "homePay": "",
    "allowances": "",
    "notes": "",
    "cola": "",
    "startDate": {
      "year": "",
      "month": "",
      "day": ""
    },
    "endDate": {
      "year": "",
      "month": "",
      "day": ""
    },
  }
  constructor(public modal: ModalsService, private storeService: StoreServicesService, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService) { }

  ngOnInit() {
  }
  closeModal() {
    this.modal.closeModal();
    this.storeService.putObject("saveTax", false);
  }
  confirmModal(item) {
    this.storeService.putObject("saveTax", true);
    this.modal.closeModal();

  }
  addOtherIncome() {
    this.startDate = this.otherincomeModal.startDate.month + "/" + this.otherincomeModal.startDate.day + "/" + this.otherincomeModal.startDate.year;
    this.endDate = this.otherincomeModal.endDate.month + "/" + this.otherincomeModal.endDate.day + "/" + this.otherincomeModal.endDate.year;

    let req = [
      {
        "profileID": this.storeService.getObject('profileID'),
        "grossMonthlyIncome": this.otherincomeModal.grossMonthlyIncome,
        "payRollTaxes": this.otherincomeModal.payRollTaxes,
        "homePay": this.otherincomeModal.homePay,
        "allowances": this.otherincomeModal.allowances,
        "notes": this.otherincomeModal.notes,
        "cola": this.otherincomeModal.cola,
        "startDate": this.startDate,
        "endDate": this.endDate
      }
    ]
    this.ApiServicesService.headerAppendedPost('api/IncomeDetails/saveFutureIncome', req).then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {

          this.notificationService.success("Future Income Saved Successfully");

          this.modal.closeModal();
        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Message);
        }

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }
}
