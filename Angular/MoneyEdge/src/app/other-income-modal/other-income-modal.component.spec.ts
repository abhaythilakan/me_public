import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherIncomeModalComponent } from './other-income-modal.component';

describe('OtherIncomeModalComponent', () => {
  let component: OtherIncomeModalComponent;
  let fixture: ComponentFixture<OtherIncomeModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherIncomeModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherIncomeModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
