import { Component, OnInit } from '@angular/core';

import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { Router } from '@angular/router';
import { ModalsService } from '../services/modals.service';
import { Http } from '@angular/http';
import { ReportHistoryListingComponent } from '../report-history-listing/report-history-listing.component';
import { ClientDetailsBoardLayoutComponent } from '../client-details-board-layout/client-details-board-layout.component';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.scss']
})
export class ClientListComponent implements OnInit {
  loader: boolean;
  clientSerachText: any;
  noClients: boolean;
  profileID: any;
  clientStatus: any;
  SortOrder: number;
  ascending: boolean;
  text: any;
  txtBx: boolean;
  tableShow: boolean;
  clientListData: any;
  sizeOfTable: any;
  page: number;
  SortColumnDateValue: any;
  closeResult: string;

  constructor(private modalService: NgbModal, private storeService: StoreServicesService, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, public http: Http, public modal: ModalsService, private router: Router) {
    this.page =1;

    this.SortColumnDateValue = 0;
  }

  open(content) {
    window.event.stopPropagation();

    this.modalService.open(content).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  ngOnInit() {
    this.clientSerachText;
    this.loader = false;
    if (this.storeService.getObject('hasClients')) {
      this.noClients = false;
    }
    else {
      this.noClients = true;
    }
   
    this.tableShow = false;
    this.txtBx = false;
    this.text = "";
    this.ascending = true;
    this.showStatus();
    this.listClient(this.page, this.text, this.SortColumnDateValue);

  }
  newClient() {
    this.router.navigate(["app-add-client"]);
  }
  paginatedSearch(e) {
    this.listClient(e, this.text, this.SortColumnDateValue);
  }
  listClient(pageno, text, SortColumnDateValue) {
    this.loader = true;
    this.text = text;
    this.page = pageno;
    this.SortColumnDateValue = SortColumnDateValue;
    if (this.ascending) {
      this.SortOrder = 0
    }
    else {
      this.SortOrder = 1;
    }
    let req =
      {
        "advisorId": this.storeService.getObject("advisorId"),
        "PageNo": this.page,
        "PageSize": 5,
        "SortColumn": this.SortColumnDateValue,
        "SortOrder": this.SortOrder,
        "SearchText": this.text

      }
    this.ApiServicesService.headerAppendedPost('api/Client/GetClientList', req).then(res => {
      // this.notificationService.success("success");
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.tableShow = true;
        if(parsedBody.Result.Collection){
          this.sizeOfTable = parsedBody.Result.Collection.TotalCount;
        }
       
        this.clientListData = parsedBody.Result.clients;
        this.loader = false;
      }


    })
  }
  searchClient(text) {
    console.log("search client text" + text);
    this.listClient(this.page, text, this.SortColumnDateValue);
  }
  sortedBasedSearch(SortColumnDateValue) {

    this.ascending = !this.ascending;
    this.listClient(this.page, this.text, SortColumnDateValue)
  }
  editClient(clientId) {
    window.event.stopPropagation();
    this.router.navigate(['/app-add-client'], { queryParams: { page: clientId } });
  }
  historyModal(clientId) {
    window.event.stopPropagation();
    this.modal.openModal(ReportHistoryListingComponent, clientId, '');
    this.storeService.putObject("clientId", clientId);
// return false;
  }
  showSearchTxt() {
    this.txtBx = true;
  }
  clientBoard(clientId, firstname, lastname) {

    console.log("clientid from list" + clientId);
    this.storeService.putObject('clientId', clientId);
    this.storeService.putObject('clientName', firstname + " " + lastname);
    /**Getting Profile Id For The Client  */
    let req = {
      "clientId": clientId
    }
    this.ApiServicesService.headerAppendedPost('api/Client/showClientProfile', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.profileID = parsedBody.Result.profile.profileID;
        this.storeService.putObject("profileID", this.profileID);
        this.router.navigate(['client-details-info-board'], { queryParams: { page: clientId } });
      }
    })

  }
  showStatus() {
    this.ApiServicesService.headerAppendedGet('api/Common/GetStatusDDL').then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.clientStatus = parsedBody.Result;
      }
    })
  }
  statusChanged(val: any) {

  }
  
  
}
