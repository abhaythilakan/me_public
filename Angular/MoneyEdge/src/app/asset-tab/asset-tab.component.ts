import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { ModalsService } from '../services/modals.service';
import { Router } from '@angular/router';
import { RetireAssetPopupComponent } from '../retire-asset-popup/retire-asset-popup.component';
import * as enLocale from 'date-fns/locale/en';

@Component({
  selector: 'app-asset-tab',
  templateUrl: './asset-tab.component.html',
  styleUrls: ['./asset-tab.component.scss']
})
export class AssetTabComponent implements OnInit {
  householdIncome: any;
  emergencyFund: any;
  private totalPersonalProp: number = 0;
  private totalRealEstateProp: number = 0;
  private totalNonRetireProp: number = 0;
  private totalCashAccProp: number = 0;

  private Asset_EmergencyFundModel = new Asset_EmergencyFundModel();

  private personnalAssetModel = new AssetModel1();
  private nonRetireAssetModel = new AssetModel2();
  private realEstateAssetModel = new AssetModel3();
  private cashAccountAssetModel = new AssetModel4();
  //private otherRetireAssetModel = new Asset_RetirementDetailsModel();

  private nonRetirementAssetsModel1 = new Array<NonRetirementAssetsModel1>();
  private nonRetirementAssetsModel2 = new Array<NonRetirementAssetsModel2>();
  private nonRetirementAssetsModel3 = new Array<NonRetirementAssetsModel3>();
  private nonRetirementAssetsModel4 = new Array<NonRetirementAssetsModel4>();

  private Asset_RetirementModel: Asset_RetirementModel[] = [];
  // private Asset_RetirementDetailsModel_Primary: Asset_RetirementDetailsModel_Primary[] = [];
  // private Asset_RetirementDetailsModel_Spouse: Asset_RetirementDetailsModel_Spouse[] = [];
  private Asset_RetirementDetailsModel_Other: Asset_RetirementDetailsModel_Other[] = [];
  //Asset_RetirementModel
  //Asset_RetirementDetailsModel

  personalPropertyTypeDDL = [];
  realEstateTypeDDL = [];
  nonRetInvestTypeDDL = [];
  cashAccountTypeDDL = [];
  retAssetTypeDDL = [];
  otherRetAssetTypeDDL = [];
  //assetTaxVsDefferedDDL = [];
  options = {
    locale: enLocale,
    minYear: 1970,
    maxYear: 2030,
    displayFormat: 'MM-DD-YYYY',
    barTitleFormat: 'MMMM YYYY',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    // minDate: new Date(Date.now()), // Minimal selectable date
  };
  constructor(
    private modalService: NgbModal,
    private storeService: StoreServicesService,
    public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService,
    public modal: ModalsService,
    private router: Router
  ) { }

  ngOnInit() {
    // this.emergencyFund=0;
    // this.householdIncome=0;
    // this.Asset_EmergencyFundModel.emergencyFund=0;
    // this.Asset_EmergencyFundModel.householdIncome=0;
    this.getPersonalPropertyDetails();
    this.getRealEstateAssetDetails();
    this.getNonRetirementAssetDetails();
    this.getCashaccountAssetDetails();
    this.getEmergencyFundandHouseholdIncome();
    this.getRetirementAsset();
    this.getOtherRetirementAsset();

    //this.fillNewObject();
    this.getpersonalPropertyTypeDDL();
    this.getrealEstateTypeDDL();
    this.getNonRetInvestTypeDDL();
    this.getCashAccountTypeDDL();
    this.getRetAssetTypeDDL()
    this.getotherRetAssetTypeDDL()
    //this.getassetTaxVsDefferedDDL()
  }
  ngDoCheck() {
    this.fillNewObject();
    if (this.personnalAssetModel.AssetList == undefined || !(this.personnalAssetModel.AssetList.length > 0)) {
      this.personnalAssetModel.AssetList = this.nonRetirementAssetsModel1;
      if (!(this.personnalAssetModel.AssetList.length > 0)) {
        let mdl = new NonRetirementAssetsModel1();
        mdl.assetID = 0;
        mdl.profileID = this.storeService.getObject('profileID');
        this.personnalAssetModel.AssetList.push(mdl);
        this.totalPersonalProp = 0;
      }
    }
    if (this.nonRetireAssetModel.AssetList == undefined || !(this.nonRetireAssetModel.AssetList.length > 0)) {
      this.nonRetireAssetModel.AssetList = this.nonRetirementAssetsModel2;
      if (!(this.nonRetireAssetModel.AssetList.length > 0)) {
        let mdl = new NonRetirementAssetsModel2();
        mdl.assetID = 0;
        mdl.profileID = this.storeService.getObject('profileID');
        this.nonRetireAssetModel.AssetList.push(mdl);
        this.totalNonRetireProp = 0;
      }
    }
    if (this.realEstateAssetModel.AssetList == undefined || !(this.realEstateAssetModel.AssetList.length > 0)) {
      this.realEstateAssetModel.AssetList = this.nonRetirementAssetsModel3;
      if (!(this.realEstateAssetModel.AssetList.length > 0)) {
        let mdl = new NonRetirementAssetsModel3();
        mdl.assetID = 0;
        mdl.profileID = this.storeService.getObject('profileID');
        this.realEstateAssetModel.AssetList.push(mdl);
        this.totalRealEstateProp = 0;
      }
    }
    if (this.cashAccountAssetModel.AssetList == undefined || !(this.cashAccountAssetModel.AssetList.length > 0)) {
      this.cashAccountAssetModel.AssetList = this.nonRetirementAssetsModel4;
      if (!(this.cashAccountAssetModel.AssetList.length > 0)) {
        let mdl = new NonRetirementAssetsModel4();
        mdl.assetID = 0;
        mdl.profileID = this.storeService.getObject('profileID');
        this.cashAccountAssetModel.AssetList.push(mdl);
        this.totalCashAccProp = 0;
      }
    }
    if (this.Asset_RetirementDetailsModel_Other == undefined || !(this.Asset_RetirementDetailsModel_Other.length > 0)) {
      //this.cashAccountAssetModel.AssetList = this.nonRetirementAssetsModel4;
      if (!(this.Asset_RetirementDetailsModel_Other.length > 0)) {
        let mdl = new Asset_RetirementDetailsModel_Other();
        mdl.retID = 0;
        mdl.assetID_Retirement = 0;
        mdl.isActive = "true";
        mdl.profileID = this.storeService.getObject('profileID');
        this.Asset_RetirementDetailsModel_Other.push(mdl);
        //this.totalCashAccProp = 0;
      }
    }
    // if (!(this.otherRetireAssetModel != null)) {
    //   let mdl = new Asset_RetirementDetailsModel();
    //   mdl.retID = 0;
    //   mdl.profileID = this.storeService.getObject('profileID');
    //   this.otherRetireAssetModel = mdl;
    // }
  }

  
  // getassetTaxVsDefferedDDL() {
  //   this.ApiServicesService.headerAppendedGet('api/Common/getTaxVsDeffered').then(res => {
  //     if (res && res._body && !res._body.target) {
  //       let parseBody = JSON.parse(res._body);
  //       this.assetTaxVsDefferedDDL = parseBody.Result;
  //     }
  //   })
  // }

  getpersonalPropertyTypeDDL() {
    this.ApiServicesService.headerAppendedGet('api/Common/AssetTypesPersonalProperty').then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.personalPropertyTypeDDL = parseBody.Result;
      }
    })
  }
  getrealEstateTypeDDL() {
    this.ApiServicesService.headerAppendedGet('api/Common/AssetTypesRealEstate').then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.realEstateTypeDDL = parseBody.Result;
      }
    })
  }
  getNonRetInvestTypeDDL() {
    this.ApiServicesService.headerAppendedGet('api/Common/AssetTypesNonRetirement').then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.nonRetInvestTypeDDL = parseBody.Result;
      }
    })
  }
  getCashAccountTypeDDL() {
    this.ApiServicesService.headerAppendedGet('api/Common/AssetTypesCashaccount').then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.cashAccountTypeDDL = parseBody.Result;
      }
    })
  }
  getRetAssetTypeDDL() {
    this.ApiServicesService.headerAppendedGet('api/Common/AssetTypesRetirementAsset').then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.retAssetTypeDDL = parseBody.Result;
      }
    })
  }
  getotherRetAssetTypeDDL() {
    this.ApiServicesService.headerAppendedGet('api/Common/AssetTypesOtherRetirement').then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.otherRetAssetTypeDDL = parseBody.Result;
      }
    })
  }

  fillNewObject() {
    if (this.personnalAssetModel == null) {
      this.personnalAssetModel = new AssetModel1();
    }
    if (this.nonRetireAssetModel == null) {
      this.nonRetireAssetModel = new AssetModel2();
    }
    if (this.realEstateAssetModel == null) {
      this.realEstateAssetModel = new AssetModel3();
    }
    if (this.cashAccountAssetModel == null) {
      this.cashAccountAssetModel = new AssetModel4();
    }
    // if (this.otherRetireAssetModel == null) {
    //   let mdl = new Asset_RetirementDetailsModel();
    //   mdl.retID = 0;
    //   mdl.profileID = this.storeService.getObject('profileID');
    //   this.otherRetireAssetModel = mdl;
    // }
  }

  getRetirementAsset() {
    this.ApiServicesService.headerAppendedPost('api/Asset/getRetirementAsset', this.storeService.getObject('profileID')).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.Asset_RetirementModel = parseBody.Result;
      }
    })
  }

  getOtherRetirementAsset() {
    this.ApiServicesService.headerAppendedPost('api/Asset/getOtherRetirementAsset', this.storeService.getObject('profileID')).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.Asset_RetirementDetailsModel_Other = parseBody.Result;
      }
    })
  }


  getEmergencyFundandHouseholdIncome() {
    this.ApiServicesService.headerAppendedPost('api/Asset/getEmergencyFundandHouseholdIncome', this.storeService.getObject('profileID')).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        //this.Asset_EmergencyFundHouseHoldingModel = parseBody.Result;
        this.Asset_EmergencyFundModel = parseBody.Result;
        this.emergencyFund = this.Asset_EmergencyFundModel.emergencyFund;
        this.householdIncome = this.Asset_EmergencyFundModel.householdIncome;
      }
    })
  }

  getPersonalPropertyDetails() {
    this.ApiServicesService.headerAppendedPost('api/Asset/getPersonalPropertyDetails', this.storeService.getObject('profileID')).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.personnalAssetModel = parseBody.Result;
        this.getTotalPersonnalProperty();
      }
    })
  }
  getRealEstateAssetDetails() {
    this.ApiServicesService.headerAppendedPost('api/Asset/getRealEstateAssetDetails', this.storeService.getObject('profileID')).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.realEstateAssetModel = parseBody.Result;
        this.getTotalReatEstateProperty();
      }
    })
  }
  getNonRetirementAssetDetails() {
    this.ApiServicesService.headerAppendedPost('api/Asset/getNonRetirementAssetDetails', this.storeService.getObject('profileID')).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.nonRetireAssetModel = parseBody.Result;
        this.getTotalNonRetireProperty();
      }
    })
  }
  getCashaccountAssetDetails() {
    this.ApiServicesService.headerAppendedPost('api/Asset/getCashaccountAssetDetails', this.storeService.getObject('profileID')).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        this.cashAccountAssetModel = parseBody.Result;
        this.getTotalCashAccProperty();
      }
    })
  }

  // onChangePersonalTaxDefferedTypeDDL(val: any, i) {
  //   this.personnalAssetModel.AssetList[i].taxVsDefferedID = val;
  // }
  onChangePersonalPropertyTypeDDL(val: any, i) {
    this.personnalAssetModel.AssetList[i].assetTypeID = val;
  }
  // onChangeRealEstateTaxDefferedTypeDDL(val: any, i) {
  //   this.realEstateAssetModel.AssetList[i].taxVsDefferedID = val;
  // }
  onChangeRealEstatePropertyTypeDDL(val: any, i) {
    this.realEstateAssetModel.AssetList[i].assetTypeID = val;
  }
  // onChangeNonRetirementTaxDefferedTypeDDL(val: any, i) {
  //   this.nonRetireAssetModel.AssetList[i].taxVsDefferedID = val;
  // }
  onChangeNonRetirementInvestmentPropertyTypeDDL(val: any, i) {
    this.nonRetireAssetModel.AssetList[i].assetTypeID = val;
  }
  // onChangeCashAccountTaxDefferedTypeDDL(val: any, i) {
  //   this.cashAccountAssetModel.AssetList[i].taxVsDefferedID = val;
  // }
  onChangeCashAccountPropertyTypeDDL(val: any, i) {
    this.cashAccountAssetModel.AssetList[i].assetTypeID = val;
  }
  // onChangeOtherRetireTaxDefferedTypeDDL(val: any, i) {
  //   this.Asset_RetirementDetailsModel_Other[i].taxVsDefferedID = val;
  // }
  onChangeOtherRetireTypeDDL(val: any, i) {
    this.Asset_RetirementDetailsModel_Other[i].assetTypeID = val;
  }


  savePersonalProperty() {
    this.ApiServicesService.headerAppendedPost('api/Asset/savePersonalProperty', this.personnalAssetModel.AssetList).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        if (parseBody.apiStatus == 0) {
          this.notificationService.success("Personal Property Saved Successfully");
          this.personnalAssetModel.AssetList = parseBody.Result;
          this.getTotalPersonnalProperty();
        }
        else if (parseBody.apiStatus == 1) {
          this.notificationService.error(parseBody.ValidationErrors[0].FieldErrorMessage);
        }
        else if (parseBody.apiStatus == 2) {
          this.notificationService.error(parseBody.Result.Message);
        }
      }
    }).catch(err => {
      if (err && err._body && !err._body.target) {
        let parsedBody = JSON.parse(err._body);
        if ((parsedBody.error && parsedBody.message) || (parsedBody.message && parsedBody.apiStatus == 1))
          this.notificationService.error(parsedBody.message);
        else
          this.notificationService.error('Error while saving Personal Property Collection');
      } else {
        this.notificationService.error('Error while saving Personal Property Collection');
      }
    });
  }
  saveNonRetirementInvestment() {
    this.ApiServicesService.headerAppendedPost('api/Asset/saveNonRetirementInvestment', this.nonRetireAssetModel.AssetList).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        if (parseBody.apiStatus == 0) {
          this.notificationService.success("Non-Retirement Investment Saved Successfully");
          this.nonRetireAssetModel.AssetList = parseBody.Result;
          this.getTotalNonRetireProperty();
        }
        else if (parseBody.apiStatus == 1) {
          this.notificationService.error(parseBody.ValidationErrors[0].FieldErrorMessage);
        }
        else if (parseBody.apiStatus == 2) {
          this.notificationService.error(parseBody.Result.Message);
        }
      }
    }).catch(err => {
      if (err && err._body && !err._body.target) {
        let parsedBody = JSON.parse(err._body);
        if ((parsedBody.error && parsedBody.message) || (parsedBody.message && parsedBody.apiStatus == 1))
          this.notificationService.error(parsedBody.message);
        else
          this.notificationService.error('Error while saving Non-Retirement Investment Collection');
      } else {
        this.notificationService.error('Error while saving Non-Retirement Investment Collection');
      }
    });
  }
  saveCashAccount() {
    this.ApiServicesService.headerAppendedPost('api/Asset/saveCashAccount', this.cashAccountAssetModel.AssetList).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        if (parseBody.apiStatus == 0) {
          this.notificationService.success("Cash Account Saved Successfully");
          this.cashAccountAssetModel.AssetList = parseBody.Result;
          this.getTotalCashAccProperty();
        }
        else if (parseBody.apiStatus == 1) {
          this.notificationService.error(parseBody.ValidationErrors[0].FieldErrorMessage);
        }
        else if (parseBody.apiStatus == 2) {
          this.notificationService.error(parseBody.Result.Message);
        }
      }
    }).catch(err => {
      if (err && err._body && !err._body.target) {
        let parsedBody = JSON.parse(err._body);
        if ((parsedBody.error && parsedBody.message) || (parsedBody.message && parsedBody.apiStatus == 1))
          this.notificationService.error(parsedBody.message);
        else
          this.notificationService.error('Error while saving Cash Account Collection');
      } else {
        this.notificationService.error('Error while saving Cash Account Collection');
      }
    });
  }
  saveRealEstate() {
    this.ApiServicesService.headerAppendedPost('api/Asset/saveRealEstate', this.realEstateAssetModel.AssetList).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        if (parseBody.apiStatus == 0) {
          this.notificationService.success("Cash Account Saved Successfully");
          this.realEstateAssetModel.AssetList = parseBody.Result;
          this.getTotalReatEstateProperty();
        }
        else if (parseBody.apiStatus == 1) {
          this.notificationService.error(parseBody.ValidationErrors[0].FieldErrorMessage);
        }
        else if (parseBody.apiStatus == 2) {
          this.notificationService.error(parseBody.Result.Message);
        }
      }
    }).catch(err => {
      if (err && err._body && !err._body.target) {
        let parsedBody = JSON.parse(err._body);
        if ((parsedBody.error && parsedBody.message) || (parsedBody.message && parsedBody.apiStatus == 1))
          this.notificationService.error(parsedBody.message);
        else
          this.notificationService.error('Error while saving Cash Account Collection');
      } else {
        this.notificationService.error('Error while saving Cash Account Collection');
      }
    });
  }

  getTotalPersonnalProperty() {
    this.totalPersonalProp = 0;
    for (var itm in this.personnalAssetModel.AssetList) {
      let num1 = parseFloat(this.totalPersonalProp == undefined ? "0" : this.totalPersonalProp.toString());
      let num2 = parseFloat(this.personnalAssetModel.AssetList[itm].amount == undefined ? "0" : this.personnalAssetModel.AssetList[itm].amount.toString());
      if (Number.isNaN(num1))
        num1 = 0;
      if (Number.isNaN(num2))
        num2 = 0;
      this.totalPersonalProp = num1 + num2;
    }
    if (Number.isNaN(this.totalPersonalProp)) {
      this.totalPersonalProp = 0;
    }
  }
  getTotalReatEstateProperty() {
    this.totalRealEstateProp = 0;
    for (var itm in this.realEstateAssetModel.AssetList) {
      let num1 = parseFloat(this.totalRealEstateProp == undefined ? "0" : this.totalRealEstateProp.toString());
      let num2 = parseFloat(this.realEstateAssetModel.AssetList[itm].amount == undefined ? "0" : this.realEstateAssetModel.AssetList[itm].amount.toString());
      if (Number.isNaN(num1))
        num1 = 0;
      if (Number.isNaN(num2))
        num2 = 0;
      this.totalRealEstateProp = num1 + num2;
    }
    if (Number.isNaN(this.totalRealEstateProp)) {
      this.totalRealEstateProp = 0;
    }
  }
  getTotalNonRetireProperty() {
    this.totalNonRetireProp = 0;
    for (var itm in this.nonRetireAssetModel.AssetList) {
      let num1 = parseFloat(this.totalNonRetireProp == undefined ? "0" : this.totalNonRetireProp.toString());
      let num2 = parseFloat(this.nonRetireAssetModel.AssetList[itm].amount == undefined ? "0" : this.nonRetireAssetModel.AssetList[itm].amount.toString());
      if (Number.isNaN(num1))
        num1 = 0;
      if (Number.isNaN(num2))
        num2 = 0;
      this.totalNonRetireProp = num1 + num2;
    }
    if (Number.isNaN(this.totalNonRetireProp)) {
      this.totalNonRetireProp = 0;
    }
  }
  getTotalCashAccProperty() {
    this.totalCashAccProp = 0;
    for (var itm in this.cashAccountAssetModel.AssetList) {
      let num1 = parseFloat(this.totalCashAccProp == undefined ? "0" : this.totalCashAccProp.toString());
      let num2 = parseFloat(this.cashAccountAssetModel.AssetList[itm].amount == undefined ? "0" : this.cashAccountAssetModel.AssetList[itm].amount.toString());
      if (Number.isNaN(num1))
        num1 = 0;
      if (Number.isNaN(num2))
        num2 = 0;
      this.totalCashAccProp = num1 + num2;
    }
    if (Number.isNaN(this.totalCashAccProp)) {
      this.totalCashAccProp = 0;
    }
  }


  addOtherRetire() {
    let mdl = new Asset_RetirementDetailsModel_Other();
    mdl.retID = 0;
    mdl.assetID_Retirement = 0;
    mdl.isActive = "true";
    mdl.profileID = this.storeService.getObject('profileID');
    this.Asset_RetirementDetailsModel_Other.push(mdl);
  }
  addPersonnalProp() {
    let mdl = new NonRetirementAssetsModel1();
    mdl.assetID = 0;
    mdl.profileID = this.storeService.getObject('profileID');
    this.personnalAssetModel.AssetList.push(mdl);
  }
  addNonRetire() {
    let mdl = new NonRetirementAssetsModel2();
    mdl.assetID = 0;
    mdl.profileID = this.storeService.getObject('profileID');
    mdl.isIncludeEmergencyFund = false;
    this.nonRetireAssetModel.AssetList.push(mdl);
  }
  addRealEstate() {
    let mdl = new NonRetirementAssetsModel3();
    mdl.assetID = 0;
    mdl.profileID = this.storeService.getObject('profileID');
    this.realEstateAssetModel.AssetList.push(mdl);
  }
  addCashAccount() {
    let mdl = new NonRetirementAssetsModel4();
    mdl.assetID = 0;
    mdl.isIncludeEmergencyFund = false;
    mdl.profileID = this.storeService.getObject('profileID');
    this.cashAccountAssetModel.AssetList.push(mdl);
  }


  removeOtherRetRow(index) {
    if (this.Asset_RetirementDetailsModel_Other[index].retID > 0) {
      this.Asset_RetirementDetailsModel_Other[index].isActive = "false";
    }
    else {
      this.Asset_RetirementDetailsModel_Other.splice(index);
    }
  }
  removePersonnalProp(index) {
    if (this.personnalAssetModel.AssetList[index].assetID > 0) {
      this.personnalAssetModel.AssetList[index].isActive = "false";
    }
    else {
      this.personnalAssetModel.AssetList.splice(index);
      this.getTotalPersonnalProperty();
    }
  }
  removeNonRetire(index) {
    if (this.nonRetireAssetModel.AssetList[index].assetID > 0) {
      this.nonRetireAssetModel.AssetList[index].isActive = "false";
    }
    else {
      this.nonRetireAssetModel.AssetList.splice(index);
      this.getTotalNonRetireProperty();
    }
  }
  removeRealEstate(index) {
    if (this.realEstateAssetModel.AssetList[index].assetID > 0) {
      this.realEstateAssetModel.AssetList[index].isActive = "false";
    }
    else {
      this.realEstateAssetModel.AssetList.splice(index);
      this.getTotalReatEstateProperty();
    }
  }
  removeCashAccount(index) {
    if (this.cashAccountAssetModel.AssetList[index].assetID > 0) {
      this.cashAccountAssetModel.AssetList[index].isActive = "false";
    }
    else {
      this.cashAccountAssetModel.AssetList.splice(index);
      this.getTotalCashAccProperty();
    }
  }


  addDeletedOtherRetRow(index) {
    this.Asset_RetirementDetailsModel_Other[index].isActive = "true";
  }
  addDeletedPersonnalPropRow(index) {
    this.personnalAssetModel.AssetList[index].isActive = "true";
  }
  addDeletedNonRetireRow(index) {
    this.nonRetireAssetModel.AssetList[index].isActive = "true";
  }
  addDeletedRealEstateRow(index) {
    this.realEstateAssetModel.AssetList[index].isActive = "true";
  }
  addDeletedCashAccountRow(index) {
    this.cashAccountAssetModel.AssetList[index].isActive = "true";
  }




  otherRetireSave() {
    this.ApiServicesService.headerAppendedPost('api/Asset/saveOtherRetirementDetails', this.Asset_RetirementDetailsModel_Other).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        if (parseBody.apiStatus == 0) {
          this.notificationService.success("Other Retirement Saved Successfully");
          this.Asset_RetirementDetailsModel_Other = parseBody.Result;
          //this.getTotalReatEstateProperty();
        }
        else if (parseBody.apiStatus == 1) {
          this.notificationService.error(parseBody.ValidationErrors[0].FieldErrorMessage);
        }
        else if (parseBody.apiStatus == 2) {
          this.notificationService.error(parseBody.Result.Message);
        }
      }
    }).catch(err => {
      if (err && err._body && !err._body.target) {
        let parsedBody = JSON.parse(err._body);
        if ((parsedBody.error && parsedBody.message) || (parsedBody.message && parsedBody.apiStatus == 1))
          this.notificationService.error(parsedBody.message);
        else
          this.notificationService.error('Error while saving Other Retirement Collection');
      } else {
        this.notificationService.error('Error while saving Other Retirement Collection');
      }
    });
  }
  emergencyFundSave() {
    this.Asset_EmergencyFundModel.emergencyFund = this.emergencyFund;
    this.Asset_EmergencyFundModel.profileID = this.storeService.getObject('profileID');
    this.ApiServicesService.headerAppendedPost('api/Asset/saveAsset_EmergencyFundDeatails', this.Asset_EmergencyFundModel).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        if (parseBody.apiStatus == 0) {
          this.notificationService.success("Emergency Fund Saved Successfully");
          this.Asset_EmergencyFundModel = parseBody.Result;
          this.getTotalReatEstateProperty();
        }
        else if (parseBody.apiStatus == 1) {
          this.notificationService.error(parseBody.ValidationErrors[0].FieldErrorMessage);
        }
        else if (parseBody.apiStatus == 2) {
          this.notificationService.error(parseBody.Result.Message);
        }
      }
    }).catch(err => {
      if (err && err._body && !err._body.target) {
        let parsedBody = JSON.parse(err._body);
        if ((parsedBody.error && parsedBody.message) || (parsedBody.message && parsedBody.apiStatus == 1))
          this.notificationService.error(parsedBody.message);
        else
          this.notificationService.error('Error while saving Emergency Fund Collection');
      } else {
        this.notificationService.error('Error while saving Emergency Fund Collection');
      }
    });
  }
  emergencyFundHouseholdSave() {
    this.Asset_EmergencyFundModel.householdIncome = this.householdIncome;
    this.Asset_EmergencyFundModel.profileID = this.storeService.getObject('profileID');
    this.ApiServicesService.headerAppendedPost('api/Asset/saveAssetHouseHoldAmount', this.Asset_EmergencyFundModel).then(res => {
      if (res && res._body && !res._body.target) {
        let parseBody = JSON.parse(res._body);
        if (parseBody.apiStatus == 0) {
          this.notificationService.success("Household Income Saved Successfully");
          this.Asset_EmergencyFundModel = parseBody.Result;
          this.getTotalReatEstateProperty();
        }
        else if (parseBody.apiStatus == 1) {
          this.notificationService.error(parseBody.ValidationErrors[0].FieldErrorMessage);
        }
        else if (parseBody.apiStatus == 2) {
          this.notificationService.error(parseBody.Result.Message);
        }
      }
    }).catch(err => {
      if (err && err._body && !err._body.target) {
        let parsedBody = JSON.parse(err._body);
        if ((parsedBody.error && parsedBody.message) || (parsedBody.message && parsedBody.apiStatus == 1))
          this.notificationService.error(parsedBody.message);
        else
          this.notificationService.error('Error while saving Household Income Collection');
      } else {
        this.notificationService.error('Error while saving Household Income Collection');
      }
    });
  }
  openRetirementAssets(assetID) {
    this.modal.openModal(RetireAssetPopupComponent, 'lg', '');
    this.storeService.putObject('AssetID', assetID);
    this.modal.resultFunction().then(res => {
      this.getRetirementAsset();
    })
  }

}

export class AssetModel1 {
  public totalSum: number;
  public AssetList: Array<NonRetirementAssetsModel1>;
}
export class AssetModel2 {
  public totalSum: number;
  public AssetList: Array<NonRetirementAssetsModel2>;
}
export class AssetModel3 {
  public totalSum: number;
  public AssetList: Array<NonRetirementAssetsModel3>;
}
export class AssetModel4 {
  public totalSum: number;
  public AssetList: Array<NonRetirementAssetsModel4>;
}


export class NonRetirementAssetsModel1 {
  public assetID: number
  public profileID: number
  public assetCatID: number
  public assetTypeID: number
  public taxVsDefferedID: number
  public company_location: string
  public amount: number
  public valueAsOf: Date = new Date()
  public monthlyContribution: number
  public description: string
  public isIncludeEmergencyFund: Boolean
  public isActive: string = ""
  public assetCreatedDate: Date = new Date()
  public assetUpdatedDate: Date = new Date()
  public assetUpdatedBy: number
}
export class NonRetirementAssetsModel2 {
  public assetID: number
  public profileID: number
  public assetCatID: number
  public assetTypeID: number
  public taxVsDefferedID: number
  public company_location: string
  public amount: number
  public valueAsOf: Date = new Date()
  public monthlyContribution: number
  public description: string
  public isIncludeEmergencyFund: Boolean
  public isActive: string = ""
  public assetCreatedDate: Date = new Date()
  public assetUpdatedDate: Date = new Date()
  public assetUpdatedBy: number
}
export class NonRetirementAssetsModel3 {
  public assetID: number
  public profileID: number
  public assetCatID: number
  public assetTypeID: number
  public taxVsDefferedID: number
  public company_location: string
  public amount: number
  public valueAsOf: Date = new Date()
  public monthlyContribution: number
  public description: string
  public isIncludeEmergencyFund: Boolean
  public isActive: string = ""
  public assetCreatedDate: Date = new Date()
  public assetUpdatedDate: Date = new Date()
  public assetUpdatedBy: number
}
export class NonRetirementAssetsModel4 {
  public assetID: number
  public profileID: number
  public assetCatID: number
  public assetTypeID: number
  public taxVsDefferedID: number
  public company_location: string
  public amount: number
  public valueAsOf: Date = new Date()
  public monthlyContribution: number
  public description: string
  public isIncludeEmergencyFund: Boolean
  public isActive: string = ""
  public assetCreatedDate: Date = new Date()
  public assetUpdatedDate: Date = new Date()
  public assetUpdatedBy: number
}

export class Asset_EmergencyFundModel {
  public fundID: number
  public profileID: number
  public householdIncome: number
  public emergencyFund: number
  public createdDate: Date
  public updatedDate: Date
  public updatedBy: number
}

export class Asset_RetirementModel {
  public assetID: number
  public profileID: number
  public personID: number
  public name: string;
  public totalAmount: number
  public lifeEcpectancy: number
  public retAge: number
  public createdDate: Date
  public updatedDate: Date
  public updatedBy: number
  public isActive: boolean
}

export class Asset_RetirementDetailsModel_Primary {
  public retID: number
  public assetID_Retirement: number
  public profileID: number
  public assetTypeID: number
  public taxVsDefferedID: number
  public company: string
  public amount: number
  public valueAsOf: Date = new Date()
  public monthlyContribution: number
  public empContribution: number
  public notes: string
  public isOtherRetirement: string = ""
  public isActive: string = ""
  public createdDate: Date = new Date()
  public updatedDate: Date = new Date()
  public updatedBy: number
}

export class Asset_RetirementDetailsModel_Spouse {
  public retID: number
  public assetID_Retirement: number
  public profileID: number
  public assetTypeID: number
  public taxVsDefferedID: number
  public company: string
  public amount: number
  public valueAsOf: Date = new Date()
  public monthlyContribution: number
  public empContribution: number
  public notes: string
  public isOtherRetirement: string = ""
  public isActive: string = ""
  public createdDate: Date = new Date()
  public updatedDate: Date = new Date()
  public updatedBy: number
}

export class Asset_RetirementDetailsModel_Other {
  public retID: number
  public assetID_Retirement: number = 0
  public profileID: number
  public assetTypeID: number
  public taxVsDefferedID: number
  public company: string
  public amount: number
  public valueAsOf: Date = new Date()
  public monthlyContribution: number
  public empContribution: number
  public notes: string
  public isOtherRetirement: Boolean
  public isActive: string = ""
  public createdDate: Date = new Date()
  public updatedDate: Date = new Date()
  public updatedBy: number
}
