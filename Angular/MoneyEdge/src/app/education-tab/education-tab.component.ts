import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute, Router } from '@angular/router';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { ModalsService } from '../services/modals.service';
import { EducationSchoolListComponent } from '../education-school-list/education-school-list.component';
import { EducationSavingsModalComponent } from '../education-savings-modal/education-savings-modal.component';
import { DatepickerOptions } from 'ng2-datepicker';

import * as enLocale from 'date-fns/locale/en';
@Component({
  selector: 'app-education-tab',
  templateUrl: './education-tab.component.html',
  styleUrls: ['./education-tab.component.scss']
})
export class EducationTabComponent implements OnInit {


  subEducationRow: boolean;
  showTables: boolean;
  rateValue: any;
  schoolID: string;
  goDataModel: any;
  dob: any;
  age: any;
  csstartDate: any;
  EducationInfoModelArray: any[];
  reportDate: any;
  collegename: any;
  choosedAge: boolean;
  choosedDate: boolean;
  preSelected: string;
  selectPlaceholder = '--select--';

  options: DatepickerOptions = {
    locale: enLocale,

    displayFormat: 'MM-DD-YYYY',
    barTitleFormat: 'MMMM YYYY',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    // minDate: new Date(Date.now()), // Minimal selectable date
    // maxDate: new Date(Date.now()),  // Maximal selectable date
    //barTitleIfEmpty: 'Click to select a date'
  };
  dateOrAgemodel: any;
  EducationDetails = [{
    "educationID": null,
    "startDateType": null,
    "startDate": null,
    "yearAtSchool": null,
    "collegename": null,
    "schoolID": null,
    "totalAnualCost": null,
    "totalCurrentSavings": null,
    "parentPayRate": null,
    "monthlySavings": null,
    "isActive": null,
    "collegeStartAge": null,
    "totalMonthlyContribution": null,
    "CurrentSavingsDetails": [
      {
        "detailID": null,
        "educationID": null,
        "educationTypeID": null,
        "company": null,
        "amount": null,
        "cID": null,
        "amountValueAsOf": null,
        "monthly": null,
        "notes": null,
        "isActive": null,
        "detailsCreatedDate": null,
        "detailsUpdatedDate": null,
        "detailsModifiedBy": null
      }
    ],
    "isSavingsRequired": true,
    "educationCreatedDate": "2018-01-24T14:05:00"
  }]
  savingsRequiredDetails = [
    {
      "collegename": null,
      "totalCostToday": null,
      "totalCostFuture": null,
      "timeToStartYear": null,
      "timeToStartMonth": null,
      "returnRate": null,
      "currentSavings": null,
      "lumpSum": null,
      "additionalMonthlySavings": null
    }
  ]
  eduCationDetailsModal: any;
  closeResult: string;

  public EducationInfoModel =
    [{
      "profileID": 30047,
      "childID": 0,
      "personID": 10012,
      "fName": "Athira DFDFFS",
      "IsParent": true,
      "dob": "1991-02-12T18:30:00",
      "age": 27,
      "dateOrAge": null,
      "EducationDetails": this.EducationDetails,
    }]

  public savingsRequiredModel = [
    {
      "name": "",
      "savingsRequiredDetails": this.savingsRequiredDetails,
    },
  ]
  public savingsRequiredGrandTotalModal = [{
    "returnRate": "",
    "lumpSum": "",
    "additionalMonthlySavings": ""
  }]
  constructor(private route: ActivatedRoute,
    private router: Router, private storeService: StoreServicesService, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modalService: NgbModal,
    public modal: ModalsService) {
    this.EducationDetails = [];

  }

  /**Open Modals */
  open(content) {
    this.modalService.open(content, {
      size: 'lg'
    }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }
  /**closed Modals */
  ngOnInit() {

    this.showTables = true;
    this.preSelected = "1";
    this.getEducationDetails();
    this.dateOrAgemodelFunction();
    this.rateValue = 0;
    this.subEducationRow = false;

  }
  chooseOption(selection, i, ind) {
    this.EducationInfoModel[i].dateOrAge = "";


    console.log("updateServer" + selection);
    console.log("EducationDetails" + JSON.stringify(this.EducationDetails))
    if (selection == "1") {
      this.EducationInfoModel[i].EducationDetails[ind].startDateType = "1";
      // this.EducationDetails[i].startDateType = "1";
    }
    else if (selection == "2") {
      this.EducationInfoModel[i].EducationDetails[ind].startDateType = "2";

      // this.EducationDetails[i].startDateType = "2";

    }
  }
  /*Get Education Details */
  getEducationDetails() {
    this.ApiServicesService.headerAppendedPost('api/Education/GetEducationInfo', this.storeService.getObject('profileID')).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.eduCationDetailsModal = parsedBody.Result;
        this.EducationDetails = [{
          "educationID": null,
          "startDateType": null,
          "startDate": null,
          "yearAtSchool": null,
          "collegename": null,
          "schoolID": null,
          "totalAnualCost": null,
          "totalCurrentSavings": null,
          "parentPayRate": null,
          "monthlySavings": null,
          "isActive": null,
          "collegeStartAge": null,
          "totalMonthlyContribution": null,
          "CurrentSavingsDetails": [
            {
              "detailID": null,
              "educationID": null,
              "educationTypeID": null,
              "company": null,
              "amount": null,
              "cID": null,
              "amountValueAsOf": null,
              "monthly": null,
              "notes": null,
              "isActive": null,
              "detailsCreatedDate": null,
              "detailsUpdatedDate": null,
              "detailsModifiedBy": null
            }
          ],
          "isSavingsRequired": true,
          "educationCreatedDate": "2018-01-24T14:05:00"
        }]

        this.reportDate = parsedBody.Result.reportDate;
        this.EducationInfoModel = parsedBody.Result.EducationInfoModel;
        for (let i = this.EducationInfoModel.length - 1; i >= 0; --i) {
          for (let j = this.EducationInfoModel[i].EducationDetails.length - 1; j >= 0; --j) {
            this.EducationInfoModel[i].EducationDetails[j].startDateType = this.EducationInfoModel[i].EducationDetails[j].startDateType;
            this.EducationInfoModel[i].EducationDetails[j].startDate = new Date(Date.now());
          }

          this.EducationInfoModel[i].EducationDetails = this.EducationInfoModel[i].EducationDetails;
          this.EducationDetails = this.EducationInfoModel[i].EducationDetails;
          console.log("EducationDetails" + this.EducationDetails);
        }
        if (!this.EducationInfoModel) {
          this.showTables = false;
        }
        else if (this.EducationInfoModel) {

          this.showTables = true;
        }
        this.savingsRequiredModel = parsedBody.Result.savingsRequired;
        this.savingsRequiredGrandTotalModal = parsedBody.Result.savingsRequiredGrandTotal;
      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i < parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[i].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Message);

      }
    })
      .catch(error => {
        this.notificationService.error("error while getting education details" + error);
      })
  }
  dateOrAgemodelFunction() {
    this.dateOrAgemodel = [
      {
        "name": "Age",
        "id": 1
      },
      {
        "name": "Date",
        "id": 2
      },
    ]
  }

  getSchoollist(i, ind) {
    this.modal.openModal(EducationSchoolListComponent, 'lg', '');
    this.modal.resultFunction().then(res => {
      console.log("This is the closed result" + res);
      this.EducationInfoModel[i].EducationDetails[ind].collegename = this.storeService.getObject("selectedCollege");
      // this.EducationDetails[index].collegename = this.storeService.getObject("selectedCollege");
      this.EducationInfoModel[i].EducationDetails[ind].totalAnualCost = this.storeService.getObject("stateValue");
      this.EducationInfoModel[i].EducationDetails[ind].schoolID = this.storeService.getObject('collegeId');
    })
  }

  editCurrentSavings(educationID) {
    this.modal.openModal(EducationSavingsModalComponent, 'lg', 'modal-xl');
    this.storeService.putObject("educationID", educationID);
    this.modal.resultFunction().then(res => {
      this.getEducationDetails();
    });
  }

  UpdateEducation() {
    this.EducationInfoModelArray = [];
    console.log("this.EducationInfoModel" + JSON.stringify(this.EducationInfoModel));
    for (let i = this.EducationInfoModel.length - 1; i >= 0; --i) {
      if (this.EducationInfoModel[i].dateOrAge = "") {
        this.EducationInfoModel.splice(i, 1);
      }
      for (let j = this.EducationInfoModel[i].EducationDetails.length - 1; j >= 0; --j) {

        if (this.EducationInfoModel[i].EducationDetails[j].schoolID) {
          this.schoolID = this.EducationInfoModel[i].EducationDetails[j].schoolID;
        }
        else {
          this.schoolID = this.storeService.getObject('collegeId');
        }
      }

    }
    let req = {
      "reportDate": this.reportDate,
      "userID": this.storeService.getObject('UserID'),
      "profileID": this.storeService.getObject('profileID'),
      "EducationInfoModel": this.EducationInfoModel
    };
    this.ApiServicesService.headerAppendedPost('api/Education/SaveEducationInfo', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        // this.goDataModel=parsedBody.Result;
        this.getEducationDetails();
        this.notificationService.success("Educational Detail Saved Successfully");
      }

      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);

      }
    })
  }
  Go(rateValue) {
    this.rateValue = rateValue;
    this.EducationInfoModelArray = [];
    for (let i = this.EducationInfoModel.length - 1; i >= 0; --i) {
      if (this.EducationInfoModel[i].dateOrAge = "") {
        this.EducationInfoModel.splice(i, 1);
      }
      if (this.EducationInfoModel[i].dateOrAge == "1") {
        this.csstartDate = this.EducationDetails[i].startDate;
      }
      else if (this.EducationInfoModel[i].dateOrAge == "2") {
        this.csstartDate = this.EducationDetails[i].startDate;
      }

    }
    let req = {
      "reportDate": this.reportDate,
      "userID": this.storeService.getObject('UserID'),
      "profileID": this.storeService.getObject('profileID'),
      "EducationInfoModel": this.EducationInfoModel,
      "goRate": this.rateValue,
      "savingsRequired": this.savingsRequiredModel,
      "savingsRequiredGrandTotal": this.savingsRequiredGrandTotalModal
    };

    console.log("Go object" + JSON.stringify(req))
    this.ApiServicesService.headerAppendedPost('api/Education/Gocalculation', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        for (let i = this.EducationInfoModel.length - 1; i >= 0; --i) {

          for (let j = this.EducationInfoModel[i].EducationDetails.length - 1; j >= 0; --j) {
            this.EducationInfoModel[i].EducationDetails[j].startDateType = this.EducationInfoModel[i].EducationDetails[j].startDateType;
          }
        }
        this.reportDate = parsedBody.Result.reportDate;
        this.EducationInfoModel = parsedBody.Result.EducationInfoModel;
        this.savingsRequiredModel = parsedBody.Result.savingsRequired;
        this.savingsRequiredGrandTotalModal = parsedBody.Result.savingsRequiredGrandTotal;
        this.notificationService.success("");
      }

      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);

      }
    })
  }

}
