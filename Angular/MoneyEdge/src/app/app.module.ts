import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { ImageUploadModule } from "angular2-image-upload";
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { NgbModule, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
// import { DataTableModule } from "angular2-datatable";
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EqualValidator } from './directives/equal-validator.directive';
import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './login/login.component';
import { routing } from './app.routes';
import * as $ from 'jquery';
import { NgDatepickerModule } from 'ng2-datepicker';
import { DpDatePickerModule } from 'ng2-date-picker';
import { NouisliderModule } from 'ng2-nouislider';
import { ApiServicesService } from './services/api-services.service';
import { NotificationServicesService } from './services/notification-services.service';
import { StoreServicesService } from './services/store-services.service';
import { ClientListComponent } from './client-list/client-list.component';
import { ClientListNewComponent } from './client-list-new/client-list-new.component';
import { RegisterComponent } from './register/register.component';
import { AdvisorSettingsComponent } from './advisor-settings/advisor-settings.component';
import { TabsComponent } from './tabs/tabs.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { AddClientComponent } from './add-client/add-client.component';
import { GlobalSettingsComponent } from './global-settings/global-settings.component';
import { BillingComponent } from './billing/billing.component';
import { Data } from './providers/data/data.service';
import { AddAdvisorComponent } from './add-advisor/add-advisor.component';
import { HeaderComponent } from './header/header.component';
import { AdvisorListComponent } from './advisor-list/advisor-list.component';
import { HomeComponent } from './home/home.component';
import { SupportComponent } from './support/support.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { DataFilterPipe } from './providers/data-filter.pipe/data-filter.pipe.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { ModalsService } from './services/modals.service';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ReportHistoryListingComponent } from './report-history-listing/report-history-listing.component';
import { ClientDetailsBoardLayoutComponent } from './client-details-board-layout/client-details-board-layout.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { LandingComponent } from './landing/landing.component';
import { InputRestricterDirective } from './directives/restrict-numeric.directive';
import { ActionPointsPageComponent } from './action-points-page/action-points-page.component';
import { PersonalInformationComponent } from './personal-information/personal-information.component';
import { IncomeTabComponent } from './income-tab/income-tab.component';
import { AssetTabComponent } from './asset-tab/asset-tab.component';
import { EducationTabComponent } from './education-tab/education-tab.component';
import { DebtTabComponent } from './debt-tab/debt-tab.component';
import { InsuranceTabComponent } from './insurance-tab/insurance-tab.component';
import { ExpensesTabComponent } from './expenses-tab/expenses-tab.component';
import { EmergencyFundTabComponent } from './emergency-fund-tab/emergency-fund-tab.component';
import { ProtectionFundTabComponent } from './protection-fund-tab/protection-fund-tab.component';
import { RetirementFundTabComponent } from './retirement-fund-tab/retirement-fund-tab.component';
import { PayrollTaxEditComponent } from './payroll-tax-edit/payroll-tax-edit.component';
import { ChartsModule } from 'ng2-charts';
import { OtherIncomeModalComponent } from './other-income-modal/other-income-modal.component';
import { OtherTaxModalComponent } from './other-tax-modal/other-tax-modal.component';
import { OtherTaxEditComponent } from './other-tax-edit/other-tax-edit.component';
import { DeleteTaxModalComponent } from './delete-tax-modal/delete-tax-modal.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import {
  MatAutocompleteModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatStepperModule,

} from '@angular/material';
import { MaxNumberDirective } from './directives/max-number.directive';
import { ForgotPwdComponent } from './forgot-pwd/forgot-pwd.component';
import { EducationSchoolListComponent } from './education-school-list/education-school-list.component';
import { ArchivedReportsComponent } from './archived-reports/archived-reports.component';
import { EducationSavingsModalComponent } from './education-savings-modal/education-savings-modal.component';
import { RestrictInputDirective } from './directives/inputmask-directive.directive';
import { RetireAssetPopupComponent } from './retire-asset-popup/retire-asset-popup.component';
import { MERoutePermissionService } from './services/meroute-permission-service.service';
import { InsurancePrimaryModelComponent } from './insurance-primary-model/insurance-primary-model.component';
import { InsuranceSpouseModelComponent } from './insurance-spouse-model/insurance-spouse-model.component';
import { DateStringService } from './services/date-string.service';
import { EarlyInvestorComponent } from './early-investor/early-investor.component';

import { CalCurrentCollegeCostComponent } from './calculator/cal-current-college-cost/cal-current-college-cost.component';
import { CalTaxableComponent } from './calculator/cal-taxable/cal-taxable.component';
import { ComponentInterestCalculatorComponent } from './calculator/cal-interest-calculator/component-interest-calculator.component';
import { CalLineedsDimeComponent } from './calculator/cal-lineeds-dime/cal-lineeds-dime.component';
import { CalCcMinimumPaymentComponent } from './calculator/cal-cc-minimum-payment/cal-cc-minimum-payment.component';
import { CalEarlyInvestorComponent } from './calculator/cal-early-investor/cal-early-investor.component';
import { ChartModule } from 'angular2-highcharts';
import { HighchartsStatic } from 'angular2-highcharts/dist/HighchartsService';
import { CalGoalsDreamsComponent } from './calculator/cal-goals-dreams/cal-goals-dreams.component';
import { CalDebtFixedVsMinimumComponent } from './calculator/cal-debt-fixed-vs-minimum/cal-debt-fixed-vs-minimum.component';
import { CalMortagePrincipalAndInterestComponent } from './calculator/cal-mortage-principal-and-interest/cal-mortage-principal-and-interest.component';
import { CalTotalMortagePaymentComponent } from './calculator/cal-total-mortage-payment/cal-total-mortage-payment.component';
import { CalMortgageMissingValueComponent } from './calculator/cal-mortgage-missing-value/cal-mortgage-missing-value.component';
import { CalAmortizationScheduleComponent } from './calculator/cal-amortization-schedule/cal-amortization-schedule.component';
import { CalDebtVehicleLoanPaymentComponent } from './calculator/cal-debt-vehicle-loan-payment/cal-debt-vehicle-loan-payment.component';
import { CurrencypipePipe } from './pipe/currencypipe.pipe';
import { DollarplaceholderDirective } from './directives/dollarplaceholder.directive';
import { DataTableModule } from './data-table';
declare var require: any;

export function highchartsFactory() {
      const hc = require('highcharts');
      const dd = require('highcharts/modules/drilldown');
      dd(hc);
      return hc;
}
@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    LoginComponent,
    ClientListComponent,
    ClientListNewComponent,
    RegisterComponent,
    EqualValidator,
    AdvisorSettingsComponent,
    TabsComponent,
    ForgotPasswordComponent,
    AddClientComponent,
    GlobalSettingsComponent,
    BillingComponent,
    AddAdvisorComponent,
    HeaderComponent,
    AdvisorListComponent,
    HomeComponent,
    SupportComponent,
    CalculatorComponent,
    DataFilterPipe,
    ConfirmationComponent,
    ChangePasswordComponent,
    ReportHistoryListingComponent,
    ClientDetailsBoardLayoutComponent,
    SideMenuComponent,
    LandingComponent,
    InputRestricterDirective,
    ActionPointsPageComponent,
    PersonalInformationComponent,
    IncomeTabComponent,
    AssetTabComponent,
    EducationTabComponent,
    DebtTabComponent,
    InsuranceTabComponent,
    ExpensesTabComponent,
    EmergencyFundTabComponent,
    ProtectionFundTabComponent,
    RetirementFundTabComponent,
    PayrollTaxEditComponent,
    OtherIncomeModalComponent,
    OtherTaxModalComponent,
    OtherTaxEditComponent,
    DeleteTaxModalComponent,
    DashboardComponent,
    MaxNumberDirective,
    ForgotPwdComponent,
    EducationSchoolListComponent,
    ArchivedReportsComponent,
    EducationSavingsModalComponent,
    RestrictInputDirective,
    RetireAssetPopupComponent,
    InsurancePrimaryModelComponent,
    InsuranceSpouseModelComponent,
    ComponentInterestCalculatorComponent,
    EarlyInvestorComponent,
    CalTaxableComponent,
    CalCurrentCollegeCostComponent,
    CalLineedsDimeComponent,
    CalCcMinimumPaymentComponent,
    CalEarlyInvestorComponent,
    CalAmortizationScheduleComponent,
    CalGoalsDreamsComponent,
    CalDebtFixedVsMinimumComponent,
    CalMortagePrincipalAndInterestComponent,
    CalTotalMortagePaymentComponent,
    CalMortgageMissingValueComponent,
    CalDebtVehicleLoanPaymentComponent,
    DollarplaceholderDirective,
    CurrencypipePipe

  ],
  entryComponents: [ConfirmationComponent, ReportHistoryListingComponent, PayrollTaxEditComponent, OtherIncomeModalComponent, OtherTaxModalComponent, OtherTaxEditComponent,
    DeleteTaxModalComponent, EducationSchoolListComponent, EducationSavingsModalComponent, RetireAssetPopupComponent, InsurancePrimaryModelComponent, InsuranceSpouseModelComponent, ComponentInterestCalculatorComponent,
    CalCurrentCollegeCostComponent, CalEarlyInvestorComponent, CalLineedsDimeComponent, CalCcMinimumPaymentComponent,
    CalAmortizationScheduleComponent, CalGoalsDreamsComponent, CalDebtFixedVsMinimumComponent, CalMortagePrincipalAndInterestComponent,
    CalTotalMortagePaymentComponent, CalMortgageMissingValueComponent, CalTaxableComponent, CalDebtVehicleLoanPaymentComponent,
    CalDebtVehicleLoanPaymentComponent],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatIconModule,
    JsonpModule,
    routing,
    SimpleNotificationsModule.forRoot(),
    SlimLoadingBarModule.forRoot(),
    BrowserAnimationsModule,
    ImageUploadModule.forRoot(),
    ChartsModule,
    NouisliderModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatStepperModule,
    NgDatepickerModule,
    DpDatePickerModule,

    NgxDatatableModule,
    
    ChartModule
  ],
  exports: [DpDatePickerModule
  ],
  providers: [ApiServicesService, MERoutePermissionService, NotificationServicesService, StoreServicesService, Data, ModalsService,
    DateStringService, CurrencypipePipe,HighchartsStatic,{
      provide: HighchartsStatic,
      useFactory: highchartsFactory
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }

