import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { StoreServicesService } from '../services/store-services.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalsService } from '../services/modals.service';
import { ConfirmationComponent } from '../confirmation/confirmation.component';
// import {DatepickerOptions} from '../ng-datepicker/ng-datepicker.component';
import * as enLocale from 'date-fns/locale/en';
import { DateStringService } from '../services/date-string.service';
@Component({
  selector: 'app-personal-information',
  templateUrl: './personal-information.component.html',
  styleUrls: ['./personal-information.component.scss']
})
export class PersonalInformationComponent implements OnInit {
  age: number;
  count_of_child: number;
  options = {
    locale: enLocale,
    minYear: 1970,
    maxYear: 2030,
    displayFormat: 'MM-DD-YYYY',
    barTitleFormat: 'MMMM YYYY',
    firstCalendarDay: 0, // 0 - Sunday, 1 - Monday
    // minDate: new Date(Date.now()), // Minimal selectable date
    // maxDate: new Date(Date.now())
  };


  startDate = new Date();
  maxNum = 50;
  maxDate = new Date();
  textvalue: number;
  birthDate: any;
  dob: { "year": number; "month": number; "day": number; };
  chidlModelediteed: any;
  n: number;
  lengthOfchildmodel: any;
  childDateOfBirth: string;
  finalChild: [{}];
  spouseemployerState: any;
  SpouseDateOfBirth: string;
  zipCodeLength: number;
  employerState: any;
  globalstate: any;
  public states = [{ "VALUE": null, "TEXT": null }];
  mobileNumberLength: number;
  DateOfBirth: string;
  public primaryDetailsModel = {
    "personID": "",
    "firstName": "",
    "midName": "",
    "lastName": "",
    "isMale": "",
    "isPrimary": "",
    "birthDate": null,
    "retirementAge": "",
    "lifeExpectancy": "",
    "Age": null,
    "emailID": "",
    "inclEducPage": "",
    "personCreatedDate": "",
    "personUpdatedDate": "",
    "personModifiedBy": "",

  }
  public primaryContactsModel = {
    "personInfoID": "",
    "profileID": "",
    "homePhone": null,
    "mobilePhone": null,
    "addressL1": null,
    "addressL2": null,
    "city": null,
    "state": null,
    "zip": null,
  }
  public primaryEmployeeModel = {
    "empID": "",
    "profileID": null,
    "ocuupation": null,
    "employer": null,
    "yearInocuupation": null,
    "yearAtEmployer": null,
    "addressL1": null,
    "addressL2": null,
    "city": null,
    "state": null,
    "zip": null,
    "isPrimary": null,
    "employerCreatedDate": null,
    "employerUpdatedDate": null,
    "employerModifiedBy": null,
  }
  public spouseDetailsModel = {
    "personID": "",
    "profileID": 30046,
    "firstName": null,
    "midName": null,
    "lastName": null,
    "isMale": null,
    "isPrimary": null,
    "birthDate": null,
    "retirementAge": 60,
    "lifeExpectancy": 90,
    "Age": null,
    "emailID": null,
    "inclEducPage": null,
    "personCreatedDate": null,
    "personUpdatedDate": null,
    "personModifiedBy": null,
    "apiStatus": 0
  }
  public spouseEmployeDetailsModel = {
    "empID": "",
    "profileID": "",
    "ocuupation": "",
    "employer": "",
    "yearInocuupation": "",
    "yearAtEmployer": "",
    "addressL1": "",
    "addressL2": "",
    "city": "",
    "state": "",
    "zip": "",
    "isPrimary": "",
    "employerCreatedDate": "",
    "employerUpdatedDate": "",
    "employerModifiedBy": "",
    "apiStatus": 0
  }
  public child = {
    "childID": "",
    "profileID": "",
    "fName": "",
    "midName": "",
    "Lname": "",
    "dob": "",
    "age": "",
    "ssn": "",
    "isMale": "",
    "inHouseHold": "",
    "includeEduPage": "",
    "childCreatedDate": "",
    "childUpdatedDate": "",
    "childModifiedBy": "",
    "apiStatus": ""
  }
  public childModel = [{
    "childID": 0,
    "profileID": "",
    "fName": "",
    "midName": "",
    "Lname": "",
    "dob": null,
    "age": null,
    "ssn": "",
    "isMale": "",
    "inHouseHold": "",
    "includeEduPage": "",
  }]

  constructor(private modalService: NgbModal, private storeService: StoreServicesService, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, public modal: ModalsService, private router: Router,
  public dateString:DateStringService) {

  }

  ngOnInit() {
    // this.count_of_child=1;
    this.mobileNumberLength = 10;
    this.zipCodeLength = 5;
    this.spouseemployerState = "";
    /**get states api */
    this.ApiServicesService.get('api/Common/GetState').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.states = parsedBody.Result;
      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
    /**For Getting Personal Information */
    this.getPrimaryDetails();
    /**For Getting Prrimary Contact Details */
    this.getPrimaryConatct();
    /**For Getting Prrimary Employee Details */
    this.getEmployeeDetails();
    this.getspouseDetails();
    this.getSpouseEmployeeDetails();
    this.getChildrenDetails();


  }
  getPrimaryDetails() {
    this.ApiServicesService.headerAppendedPost('api/PersonalInformation/GetPrimaryDetails', this.storeService.getObject('profileID')).then(result => {
      let parsedBody = JSON.parse(result._body);
      if (parsedBody.apiStatus == 0) {
        this.primaryDetailsModel = parsedBody.Result;
      }
    })
  }
  getspouseDetails() {
    this.ApiServicesService.headerAppendedPost('api/PersonalInformation/getSpouseDetails', this.storeService.getObject('profileID')).then(result => {
      let parsedBody = JSON.parse(result._body);
      if (parsedBody.apiStatus == 0) {
        this.spouseDetailsModel = parsedBody.Result;
      }
    })
  }
  getSpouseEmployeeDetails() {
    this.ApiServicesService.headerAppendedPost('api/PersonalInformation/getEmployerInfoSpouse', this.storeService.getObject('profileID')).then(result => {
      let parsedBody = JSON.parse(result._body);
      if (parsedBody.apiStatus == 0) {
        this.spouseEmployeDetailsModel = parsedBody.Result;
        if (!this.spouseEmployeDetailsModel.state) {
          this.spouseEmployeDetailsModel.state = this.states[0].VALUE;
        }
      }
    })
  }
  primaryDetails() {
    // this.DateOfBirth=this.primaryDetailsModel.birthDate.month+"/"+this.primaryDetailsModel.birthDate.day+"/"+this.primaryDetailsModel.birthDate.year;
    this.birthDate = this.primaryDetailsModel.birthDate;
    console.log("primaryDetailsModel.birthDate" + this.birthDate);

    let primaryDetailsModelReq = {
      "personID": this.primaryDetailsModel.personID,
      "profileID": this.storeService.getObject('profileID'),
      "firstName": this.primaryDetailsModel.firstName,
      "midName": this.primaryDetailsModel.midName,
      "lastName": this.primaryDetailsModel.lastName,
      "isMale": this.primaryDetailsModel.isMale,
      "isPrimary": "",
      "birthDate": new Date(this.birthDate),
      "retirementAge": this.primaryDetailsModel.retirementAge,
      "lifeExpectancy": this.primaryDetailsModel.lifeExpectancy,
      "Age": this.primaryDetailsModel.Age,
      "emailID": this.primaryDetailsModel.emailID,
      "inclEducPage": this.primaryDetailsModel.inclEducPage,
      "personCreatedDate": "",
      "personUpdatedDate": "",
      "personModifiedBy": "",

    }
    console.log("primaryDetailsModel.birthDate" + JSON.stringify(primaryDetailsModelReq));

    this.ApiServicesService.headerAppendedPost('api/PersonalInformation/primaryInfo', primaryDetailsModelReq).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.primaryDetailsModel = parsedBody.Result;
        this.getPrimaryDetails();
        this.notificationService.success("Personal Details Saved Successfully");
      }
      else if (parsedBody.apiStatus == 1) {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      }
      else if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
    })

  }
  /**For Getting Prrimary Contact Details */
  getPrimaryConatct() {
    this.ApiServicesService.headerAppendedPost('api/PersonalInformation/getPrimaryContact', this.storeService.getObject('profileID')).then(result => {
      let parsedBody = JSON.parse(result._body);
      if (parsedBody.apiStatus == 0) {
        this.primaryContactsModel = parsedBody.Result;
        if (!this.primaryContactsModel.state) {
          this.primaryContactsModel.state = this.states[0].VALUE;
        }
      }
    })
  }
  getEmployeeDetails() {
    this.ApiServicesService.headerAppendedPost('api/PersonalInformation/getEmployerInfo', this.storeService.getObject('profileID')).then(result => {
      let parsedBody = JSON.parse(result._body);
      if (parsedBody.apiStatus == 0) {
        this.primaryEmployeeModel = parsedBody.Result;
        if (!this.primaryEmployeeModel.state) {
          this.primaryEmployeeModel.state = this.states[0].VALUE;
        }
      }
    })
  }
  getChildrenDetails() {
    this.ApiServicesService.headerAppendedPost('api/PersonalInformation/getChildDetails', this.storeService.getObject('profileID')).then(result => {
      let parsedBody = JSON.parse(result._body);
      if (parsedBody.apiStatus == 0) {
        this.childModel = parsedBody.Result;
        if (!this.childModel) {
          for (let i = 0; i < 3; i++) {
            this.childModel = this.childModel || [];
            this.childModel.push({
              "childID": 0,
              "profileID": this.storeService.getObject('profileID'),
              "fName": "",
              "midName": "",
              "Lname": this.primaryDetailsModel.firstName,
              "dob": new Date(),
              "age": "",
              "ssn": "",
              "isMale": "",
              "inHouseHold": "",
              "includeEduPage": "",
            })
          }
        }
      }
    })
  }

  globalStateChanged(newObj) {
    console.log("globalStateChanged" + newObj);
    this.globalstate = newObj;
  }
  StateChanged(newObj) {
    console.log("globalStateChanged" + newObj);
    this.employerState = newObj;
  }
  StateChangedforSpouseEmployee(newObj) {
    console.log("globalStateChanged" + newObj);
    this.spouseemployerState = newObj;

  }
  primaryContacts() {
    if (this.globalstate != "") {
      this.primaryContactsModel.state = this.globalstate
    }
    else {
      this.primaryContactsModel.state = this.primaryContactsModel.state
    }
    let req = {
      "personInfoID": this.primaryContactsModel.personInfoID,
      "profileID": this.storeService.getObject('profileID'),
      "homePhone": this.primaryContactsModel.homePhone,
      "mobilePhone": this.primaryContactsModel.mobilePhone,
      "addressL1": this.primaryContactsModel.addressL1,
      "addressL2": this.primaryContactsModel.addressL2,
      "city": this.primaryContactsModel.city,
      "state": this.primaryContactsModel.state,
      "zip": this.primaryContactsModel.zip,

    }
    this.ApiServicesService.headerAppendedPost('api/PersonalInformation/primaryContact', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.primaryContactsModel = parsedBody.Result;
        this.getPrimaryConatct();
        this.notificationService.success("Contact Details Saved Successfully");
      }
      else {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[i].FieldErrorMessage);
        }
      }
    })
    console.log("Req need to send" + JSON.stringify(req));
  }
  primaryEmploymentDetails() {
    if (this.employerState != "") {
      this.primaryEmployeeModel.state = this.employerState;
    }
    else {
      this.primaryEmployeeModel.state = this.primaryEmployeeModel.state;
    }
    let req = {
      "empID": this.primaryEmployeeModel.empID,
      "profileID": this.storeService.getObject('profileID'),
      "ocuupation": this.primaryEmployeeModel.ocuupation,
      "employer": this.primaryEmployeeModel.employer,
      "yearInocuupation": this.primaryEmployeeModel.yearInocuupation,
      "yearAtEmployer": this.primaryEmployeeModel.yearAtEmployer,
      "addressL1": this.primaryEmployeeModel.addressL1,
      "addressL2": this.primaryEmployeeModel.addressL2,
      "city": this.primaryEmployeeModel.city,
      "state": this.primaryEmployeeModel.state,
      "zip": this.primaryEmployeeModel.zip,

    }
    this.ApiServicesService.headerAppendedPost('api/PersonalInformation/primaryEmployer', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.primaryEmployeeModel = parsedBody.Result;
        this.getEmployeeDetails();
        this.notificationService.success("Employee Details Saved Successfully");
      }
      else {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[i].FieldErrorMessage);
        }
      }
    })
    console.log("Req need to send" + JSON.stringify(req));

  }

  spouseDetails() {
    this.SpouseDateOfBirth = this.spouseDetailsModel.birthDate.month + "/" + this.spouseDetailsModel.birthDate.day + "/" + this.spouseDetailsModel.birthDate.year;

    let req = {
      "personID": this.spouseDetailsModel.personID,
      "profileID": this.storeService.getObject('profileID'),
      "firstName": this.spouseDetailsModel.firstName,
      "midName": this.spouseDetailsModel.midName,
      "lastName": this.spouseDetailsModel.lastName,
      "isMale": this.spouseDetailsModel.isMale,
      "isPrimary": "",
      "birthDate": this.spouseDetailsModel.birthDate,
      "retirementAge": this.spouseDetailsModel.retirementAge,
      "lifeExpectancy": this.spouseDetailsModel.lifeExpectancy,
      "Age": this.spouseDetailsModel.Age,
      "emailID": this.spouseDetailsModel.emailID,
      "inclEducPage": this.spouseDetailsModel.inclEducPage,
      "personCreatedDate": "",
      "personUpdatedDate": "",
      "personModifiedBy": "",

    }
    this.ApiServicesService.headerAppendedPost('api/PersonalInformation/SpouseInfo', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.spouseDetailsModel = parsedBody.Result;
        this.getspouseDetails();
        this.notificationService.success("Spouse Details Saved Successfully");
      }
      else {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[i].FieldErrorMessage);
        }
      }
      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
    })
    console.log("Req need to send" + JSON.stringify(req));

  }
  spouseEmployeDetails() {
    if (this.spouseemployerState != "") {
      this.spouseEmployeDetailsModel.state = this.spouseemployerState;
    }
    else {
      this.spouseEmployeDetailsModel.state = this.spouseEmployeDetailsModel.state;
    }
    let req = {
      "empID": this.spouseEmployeDetailsModel.empID,
      "profileID": this.storeService.getObject('profileID'),
      "ocuupation": this.spouseEmployeDetailsModel.ocuupation,
      "employer": this.spouseEmployeDetailsModel.employer,
      "yearInocuupation": this.spouseEmployeDetailsModel.yearInocuupation,
      "yearAtEmployer": this.spouseEmployeDetailsModel.yearAtEmployer,
      "addressL1": this.spouseEmployeDetailsModel.addressL1,
      "addressL2": this.spouseEmployeDetailsModel.addressL2,
      "city": this.spouseEmployeDetailsModel.city,
      "state": this.spouseEmployeDetailsModel.state,
      "zip": this.spouseEmployeDetailsModel.zip
    }
    console.log("DATA to be send" + JSON.stringify(req));
    this.ApiServicesService.headerAppendedPost('api/PersonalInformation/SpouseEmployer', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.spouseEmployeDetailsModel = parsedBody.Result;
        this.getSpouseEmployeeDetails();
        this.notificationService.success("Spouse Employee Details Saved Successfully");
      }
      else {
        for (let i = 0; i <= parsedBody.Result.ValidationErrors.length; i++) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[i].FieldErrorMessage);
        }
      }
      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
    })
  }
  addChild() {

    this.childModel = this.childModel || [];
    this.childModel.push({
      "childID": 0,
      "profileID": this.storeService.getObject('profileID'),
      "fName": "",
      "midName": "",
      "Lname": this.primaryDetailsModel.firstName,
      "dob": new Date(),
      "age": "",
      "ssn": "",
      "isMale": "",
      "inHouseHold": "",
      "includeEduPage": "",
    })
    

  }

  childDetailsSave(child) {
    this.finalChild = [{
      "childID": child.childID,
      "profileID": this.storeService.getObject('profileID'),
      "fName": child.fName,
      "midName": child.midName,
      "Lname": child.Lname,
      "dob": child.dob,
      "age": child.age,
      "ssn": child.ssn,
      "isMale": child.isMale,
      "inHouseHold": child.inHouseHold,
      "includeEduPage": child.includeEduPage,

    }];

    this.ApiServicesService.headerAppendedPost('api/PersonalInformation/childSave', this.finalChild).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        // this.childModel=parsedBody.Result;
        this.getChildrenDetails();
        this.notificationService.success("Children Saved Successfully");
      }

      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Message);

      }
    })
    console.log("child" + JSON.stringify(child));
  }
  mychange(val) {
  }
  saveWholeChildren() {
   
    for (let i = this.childModel.length - 1; i >= 0; --i) {
      this.childModel[i].dob=this.dateString.convert(this.childModel[i].dob);
      if (this.childModel[i].fName == "") {
        this.childModel.splice(i, 1);
      }
    }
    this.ApiServicesService.headerAppendedPost('api/PersonalInformation/childSave', this.childModel).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        // this.childModel=parsedBody.Result;
        this.getChildrenDetails();
        this.notificationService.success("Children Saved Successfully");
      }

      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);

      }
    })
  }
  removeChild(child, index) {
    console.log("Child Nedd to Remove" + JSON.stringify(child));
    console.log(" this.childModel length" + this.childModel.length + "index" + index);
    this.modal.openModal(ConfirmationComponent, '', '');
    this.modal.resultFunction().then((res) => { 
      if (child.childID == 0) {
        this.childModel.splice(index, index);
      }
      else {
        let req = {
          "profileID": child.profileID,
          "childID": child.childID
        }
        let header = {
          "Content-Type": "application/json"
        }

        this.
          ApiServicesService.headerAppendedPost('api/PersonalInformation/DeleteChild', req)
          .then(res => {
            let parsedBody = JSON.parse(res._body);
            if (parsedBody.apiStatus == 0) {
              this.getChildrenDetails();
            }
          })
          .catch(err => {
            if (err && err._body && !err._body.target) {
              let parsedBody = JSON.parse(err._body);
              if (parsedBody.error && parsedBody.message)
                this.notificationService.error(parsedBody.message);
              else
                this.notificationService.error('Could not register');
            } else {
              this.notificationService.error('Could not register');
            }
          });
      }
     
     }, () => console.log('Rejected!'));
  }
  // onBlurCount(){
  //   if(this.count_of_child>5){
  //     this.notificationService.error("Please choose the value 5 or below 5 ");
  //   }
  // }

  CalculateAge() {

    if (this.primaryDetailsModel.birthDate) {
      var timeDiff = Math.abs(Date.now() - this.primaryDetailsModel.birthDate);
      //Used Math.floor instead of Math.ceil
      //so 26 years and 140 days would be considered as 26, not 27.
      this.primaryDetailsModel.Age = Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);

    }
  }
  CalculateSpouseAge(){
    
    if (this.spouseDetailsModel.birthDate) {
     
      var timeDiff = Math.abs(Date.now() - this.spouseDetailsModel.birthDate);
      //Used Math.floor instead of Math.ceil
      //so 26 years and 140 days would be considered as 26, not 27.
      this.spouseDetailsModel.Age = Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
    }
  }
  CalculateChildAge(val,index){
    
    if (this.childModel[index].dob) {
     
      var timeDiff = Math.abs(Date.now() - this.childModel[index].dob);
      //Used Math.floor instead of Math.ceil
      //so 26 years and 140 days would be considered as 26, not 27.
      this.childModel[index].age = Math.floor((timeDiff / (1000 * 3600 * 24)) / 365);
    }
  }
}

