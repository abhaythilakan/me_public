
import { Component, Injectable, Directive, TemplateRef } from '@angular/core';
import { NgbModal, NgbModalRef, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
@Injectable()
export class ModalsService {
  obj: {};
  closeResult: string;
  modalRef: any;

  constructor(private modalService: NgbModal) { }
  public openModal(name, size, className) {
    this.modalRef = this.modalService.open(name, { "size": size, "windowClass": className });
  }
  public closeModal() {
    this.modalRef.close();
  }
  public dismissModal() {
    this.modalRef.dismiss();
  }
  public resultFunction() {
    return this.modalRef.result
      .then(res => this.handleSuccess(res))
      .catch(err => this.handleError(err));
  }
  // public rejectFunction() {
  //   return this.modalRef._reject
  //     .then(res => this.handleSuccess(res));
  //     .catch(err => this.handleError(err));
  // }

  private handleSuccess(res): Promise<any> {
    return Promise.resolve(res);
  }
  private handleError(error: any): Promise<any> {
    // console.error('An error occurred');
    // return Promise.reject(error);
    return 
  }
  
}
