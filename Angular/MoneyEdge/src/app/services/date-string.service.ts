import { Injectable } from '@angular/core';

@Injectable()
export class DateStringService {

  constructor() { }
  public convert(str) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [mnth, day, date.getFullYear()].join("-");
  }
}
