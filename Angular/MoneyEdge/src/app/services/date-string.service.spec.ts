import { TestBed, inject } from '@angular/core/testing';

import { DateStringService } from './date-string.service';

describe('DateStringService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DateStringService]
    });
  });

  it('should be created', inject([DateStringService], (service: DateStringService) => {
    expect(service).toBeTruthy();
  }));
});
