import { Injectable } from '@angular/core';

@Injectable()
export class StoreServicesService {

  constructor() { }

  public put(attr: string, value: any): void {
    this.setCookie(attr, value, 30);
  }

  public putObject(attr: string, value: any): void {
    this.setCookie(attr, JSON.stringify(value), 30);
  }

  public get(attr: string): any {
    if (this.getCookie(attr))
      return this.getCookie(attr);
  }

  public getObject(attr: string): any {
    if (this.getCookie(attr))
      return JSON.parse(this.getCookie(attr))
  }

  public remove(attr: string): void {
    this.removeCookie(attr);
  }

  public clearAll() {
    let decodedCookie = document.cookie;
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      var cookie = ca[i];
      var eqPos = cookie.indexOf("=");
      var name:string = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
      if(name!="userAuth"){
        this.removeCookie(name);
      }
     
    }
  }

  private setCookie(cname: string, cvalue: string, expireDays: number) {
    let d: any = new Date();
    d.setTime(d.getTime() + (expireDays * 24 * 60 * 60 * 1000));
    let expires: any = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  private getCookie(cname: string) {
    let name = cname + "=";
    let decodedCookie = document.cookie;
    let ca = decodedCookie.split(';');
    for (let i = 0; i < ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
  }

  private removeCookie(cname: string) {
    this.setCookie(cname, '', -1);
  }
  // public getAllCookie(){
  //   let decodedCookie=document.cookie;
  //   let ca=decodedCookie.split(';');
  //   console.log("final cookie"+ca);
    
  //   for (let i = 0; i < ca.length; i++) {
  //     var cookie = ca[i];
  //     let indexOne = cookie.indexOf("userAuth");
  //     // let indexOne = ca.indexOf("userAuth");
  //     ca.splice(indexOne, 1);
      
    
  //   }
  //   console.log("final cookie ****************"+ca);
  // return ca;
  // } 
}
