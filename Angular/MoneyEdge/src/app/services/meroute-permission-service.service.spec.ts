import { TestBed, inject } from '@angular/core/testing';

import { MERoutePermissionService } from './meroute-permission-service.service';

describe('MeroutePermissionServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MERoutePermissionService]
    });
  });

  it('should be created', inject([MERoutePermissionService], (service: MERoutePermissionService) => {
    expect(service).toBeTruthy();
  }));
});
