import { Injectable } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';


@Injectable()

export class NotificationServicesService {
    constructor(private notificationService: NotificationsService) {}

    public success(message: string): void {
        this.notificationService.remove();
        this.notificationService.success(
            'Success',
            message, {
                showProgressBar: false,
                pauseOnHover: false,
                clickToClose: false,
            }
        )
    }

    public error(message: string): void {
        this.notificationService.remove();
        this.notificationService.error(
            'Error',
            message, {
                showProgressBar: false,
                pauseOnHover: false,
                clickToClose: false,
            }
        )
    }

    public warning(message: string): void {
        this.notificationService.remove();
        this.notificationService.warn(
            'Warning',
            message, {
                showProgressBar: false,
                pauseOnHover: false,
                clickToClose: false,
            }
        )
    }
}

