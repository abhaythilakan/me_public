import { Injectable } from '@angular/core';
// import { Headers, Http } from '@angular/http';
import {Router } from '@angular/router';

import 'rxjs/add/operator/toPromise';
import {SlimLoadingBarService} from 'ng2-slim-loading-bar';
import * as $ from "jquery";
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { NotificationServicesService } from './notification-services.service';
import { StoreServicesService } from './store-services.service';
import { Type } from '@angular/core/src/type';
import {Http, Headers, Request, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
@Injectable()
export class ApiServicesService {
  errorMessage: any;
  statusMessage: any;
 //private parentUrl: string = 'http://52.25.167.251/';
 private parentUrl: string = 'http://192.168.0.15/';

  private headers: any;
  constructor(private http: Http, private slimLoadingBarService: SlimLoadingBarService, private notificationService: NotificationServicesService,
    private storeService:StoreServicesService,private router: Router) { }
  private getHeaderForLogin(headerVal: any): any {
    let headers: any = {
      'Content-Type': 'application/x-www-form-urlencoded'
     
    };
    let header: any;
    if (headerVal) {
      if (headerVal['Content-Type']){
        headers['Content-Type'] = headerVal['Content-Type'];
      }
      header = { ...headerVal, ...headers };
    } else {
      header = headers;
    }
   
    return new Headers(header);
  }
  public url(){
    return this.parentUrl;
  }
  private getHeader(headerVal: any): any {
    let headers: any = {
      'Content-Type': 'application/json'
     
    };
    let header: any;
    if (headerVal) {
      if (headerVal['Content-Type']){
        headers['Content-Type'] = headerVal['Content-Type'];
      }
      header = { ...headerVal, ...headers };
    } else {
      header = headers;
    }
   
    return new Headers(header);
  }

  public headerAppendedPost(url: string,req: any ){
    let serverUrl = this.parentUrl + url;
    // this.startLoading();
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');

    var t = this.storeService.getObject("token");
    headers.append("Authorization", "Bearer " + t); 
    return this.http.post(serverUrl, req, { headers: headers })
    .toPromise()
    .then(res => this.handleSuccess(res))
    .catch(err => this.handleError(err));
    // .map((response: Response) => {
    //   var result = response.json();
    //   this.handleSuccess(response);
    //   console.log("result"+JSON.stringify(result));
    
    // })
    // .catch(this.handleError)
    // .subscribe(
    // status => this.statusMessage = status,
    // error =>  this.handleError(error),
    // () => console.log("success")
    // );
  }
public headerAppendedGet(url: string){
  let serverUrl = this.parentUrl + url;
  // this.startLoading();
  
  var headers = new Headers();
  headers.append('Content-Type', 'application/json');

  var t = this.storeService.getObject("token");
  headers.append("Authorization", "Bearer " + t); 
  return this.http.get( serverUrl , { headers: headers })
  .toPromise()
  .then(res => this.handleSuccess(res))
  .catch(err => this.handleError(err));
 
}
  public post(url: string, req: any, headerVal ? : any, sendCookies: boolean = false): Promise < any > {
    let serverUrl = this.parentUrl + url;
    this.startLoading();
    return this.http
      .post(serverUrl, req, { headers: this.getHeader(headerVal), withCredentials: sendCookies })
      .toPromise()
      .then(res => this.handleSuccess(res))
      .catch(err => this.handleError(err));
  }

  public delete(url: string, headerVal ? : any): Promise < any > {
    let serverUrl = this.parentUrl + url;
    this.startLoading();
    return this.http
      .delete(serverUrl, { headers: this.getHeader(headerVal) })
      .toPromise()
      .then(res => this.handleSuccess(res))
      .catch(err => this.handleError(err));
  }

  public get(url: string, headerVal ? : any): Promise < any > {
    let serverUrl = this.parentUrl + url;
    this.startLoading();
    return this.http
      .get(serverUrl, { headers: this.getHeader(headerVal) })
      .toPromise()
      .then(res => this.handleSuccess(res))
      .catch(err => this.handleError(err));
  }
  private handleSuccess(res): Promise < any > {
    this.completeLoading();
    return Promise.resolve(res);
  }
  private handleError(error: any): Promise < any > {
    console.error('An error occurred');
    this.completeLoading();
    return Promise.reject(error);
  }
  private startLoading(): void {
  
    $('.APP-ROOT').append("<div class='main_loader_page'><div class='main_loader_spinner'></div></div>");
    this.slimLoadingBarService.start(() => {
      console.log('Loading complete');
    });
  }
  private completeLoading(): void {
    $('.main_loader_page').remove();
    this.slimLoadingBarService.complete();
  }


 

}


