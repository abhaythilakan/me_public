import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { StoreServicesService } from './store-services.service';
import { ApiServicesService } from './api-services.service';

@Injectable()
export class MERoutePermissionService {

  constructor( private storeService: StoreServicesService,
    private ApiServicesService: ApiServicesService, private _router: Router,) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    if(this.storeService.getObject("token")){
      
      this.storeService.putObject("token_cleared",false);
      return true;
    }
    if (!this.storeService.getObject("token") &&route.url[0].path == 'client-list'||route.url[0].path == 'client-list-new'
    ||route.url[0].path == 'advisor-settings'||
    route.url[0].path == 'add-client'||route.url[0].path == 'global-settings'||route.url[0].path == 'billing'||
    route.url[0].path == 'add-advisor'||
    route.url[0].path == 'advisor-list'||route.url[0].path == 'home'||route.url[0].path == 'calculator'
    ||route.url[0].path == 'app-client-list-new'
    ||route.url[0].path == 'app-change-password'||route.url[0].path == 'app-add-client'||route.url[0].path == 'app-global-settings'
    ||route.url[0].path == 'client-details-info-board'||route.url[0].path == 'dashboard'||route.url[0].path == 'archived-reports'||
    route.url[0].path == 'app-client-list'){
    // this.storeService.putObject("token_cleared",true);
    this._router.navigate(["app-login"]);
    return false;
    }
    
    else if(this.storeService.getObject("token") &&route.url[0].path == 'client-list'||route.url[0].path == 'app-login'||route.url[0].path == 'client-list-new'
    ||route.url[0].path == 'advisor-settings'||
    route.url[0].path == 'add-client'||route.url[0].path == 'global-settings'||route.url[0].path == 'billing'||
    route.url[0].path == 'add-advisor'||
    route.url[0].path == 'advisor-list'||route.url[0].path == 'home'||route.url[0].path == 'calculator'
    ||route.url[0].path == 'app-client-list-new'
    ||route.url[0].path == 'app-change-password'||route.url[0].path == 'app-add-client'||route.url[0].path == 'app-global-settings'
    ||route.url[0].path == 'client-details-info-board'||route.url[0].path == 'dashboard'||route.url[0].path == 'archived-reports'||
    route.url[0].path == 'app-client-list'){
     
      return true;
    }

  }
}

