import { Component, OnInit } from '@angular/core';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { ModalsService } from '../services/modals.service';

@Component({
  selector: 'app-report-history-listing',
  templateUrl: './report-history-listing.component.html',
  styleUrls: ['./report-history-listing.component.scss']
})
export class ReportHistoryListingComponent implements OnInit {
  totalCollegeSerachedUsingName: number;
  reportList: any;
  itemsTotal: any;
  tableShow: boolean;
  loader: boolean;
  page: number;
  
  constructor(private storeService:StoreServicesService,public ApiServicesService:ApiServicesService,
    private notificationService: NotificationServicesService,public modal:ModalsService) { }

  ngOnInit() {
    this.totalCollegeSerachedUsingName=50;
    this.loader=false;
    this.tableShow=false;
    this.page=0;
    this.listReport(this.page)
  
  }
  pageCount(e){
    this.page=e;
    this.listReport(this.page);
  }
  closeModal(){
    this.modal.closeModal();
  }
    /**List Reports */
  listReport(pageNo){
   
    let body={
      "clientId":this.storeService.getObject("clientId"),
      "advisorId":this.storeService.getObject("advisorId"),
      "PageNo":pageNo,
      "PageSize":5,
      "SortColumn":0,
      "SortOrder":0
      }
    this.ApiServicesService.headerAppendedPost('api/Profile/getProfileList',body).then(res => {
      let parsedBody=JSON.parse(res._body);
      this.loader=true;
           if (parsedBody.apiStatus == 0) {
            this.loader=false;
            this.tableShow=true;
             this.reportList=parsedBody.Result;
             this.itemsTotal = parsedBody.Result[0].TotalCount;
           
           }
      })
          .catch(err => {
        if (err && err._body && !err._body.target) {
          let parsedBody = JSON.parse(err._body);
          if (parsedBody.error && parsedBody.message)
            this.notificationService.error(parsedBody.message);
          else
            this.notificationService.error('Could not List');
        } else {
          this.notificationService.error('No Reports History Found');
        }
      });
    }
 
}
