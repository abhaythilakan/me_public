import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportHistoryListingComponent } from './report-history-listing.component';

describe('ReportHistoryListingComponent', () => {
  let component: ReportHistoryListingComponent;
  let fixture: ComponentFixture<ReportHistoryListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportHistoryListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportHistoryListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
