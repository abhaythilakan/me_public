import { Component, OnInit } from '@angular/core';
import { ModalsService } from '../services/modals.service';
import { StoreServicesService } from '../services/store-services.service';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';

@Component({
  selector: 'app-other-tax-modal',
  templateUrl: './other-tax-modal.component.html',
  styleUrls: ['./other-tax-modal.component.scss']
})
export class OtherTaxModalComponent implements OnInit {
  PayPeriods: any;
  changedPayperiod: any;
  taxType: any;
  OtherTaxTypes: any;
  public otherTaxModal = {
    // "incomeID": 30048,
    "taxTypeID": "",
    "payingTax": "",
    "note": "",
    "isMouseHover": false,
    "paytype":null
  }
  constructor(public modal: ModalsService, private storeService: StoreServicesService, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService) { }

  ngOnInit() {
    this.getOtherTaxTypes();
    this.getPayperiod();  
  }
  getPayperiod() {
    this.ApiServicesService.headerAppendedGet('api/Common/PayTaxTpeDDL').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.PayPeriods = parsedBody.Result;

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }
  closeModal() {
    this.modal.closeModal();
  }
  confirmModal(item) {
    this.modal.closeModal();

  }
  payPeriodChanged(val: any) {
    console.log("state changed" + val);
    this.changedPayperiod = val;
  }
  /**Get Other Taxes Types  */
  getOtherTaxTypes() {
    this.ApiServicesService.headerAppendedGet('api/Common/GetOtherTaxes').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.OtherTaxTypes = parsedBody.Result;

      }
    })
      .catch(err => {
        this.notificationService.error(err);

      })
  }
  addOtherTax() {
    
    if(this.otherTaxModal.taxTypeID==""){
      this.otherTaxModal.taxTypeID= this.OtherTaxTypes[0].VALUE;
    }{
      this.otherTaxModal.taxTypeID=this.otherTaxModal.taxTypeID;
    }
    let req = [{
      "profileID": this.storeService.getObject('profileID'),
      "taxTypeID": this.otherTaxModal.taxTypeID,
      "payingTax": this.otherTaxModal.payingTax,
      "note": this.otherTaxModal.note,
      "isMouseHover": this.otherTaxModal.isMouseHover,
      "paytype":this.otherTaxModal.paytype
    }]
    this.ApiServicesService.headerAppendedPost('api/IncomeDetails/saveOtherTaxeDetails',req).then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.notificationService.success("Other Tax Details Saved Successfully");
          this.closeModal();
         
        }
        else if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Message);
        }
      }
    })
      .catch(err => {
        this.notificationService.error(err);
  
      })
  }
  otherTaxChangeType(val:any){
    this.taxType=val;
  }
}
