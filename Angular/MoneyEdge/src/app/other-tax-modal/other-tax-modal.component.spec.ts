import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OtherTaxModalComponent } from './other-tax-modal.component';

describe('OtherTaxModalComponent', () => {
  let component: OtherTaxModalComponent;
  let fixture: ComponentFixture<OtherTaxModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OtherTaxModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OtherTaxModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
