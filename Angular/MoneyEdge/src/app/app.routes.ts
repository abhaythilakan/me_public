// ====== ./app/app.routes.ts ======

// Imports
// Deprecated import
// import { provideRouter, RouterConfig } from '@angular/router';
import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ClientListComponent } from './client-list/client-list.component';
import { ClientListNewComponent } from './client-list-new/client-list-new.component';
import { RegisterComponent } from './register/register.component';
import { AdvisorSettingsComponent } from './advisor-settings/advisor-settings.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { AddClientComponent } from './add-client/add-client.component';
import { GlobalSettingsComponent } from './global-settings/global-settings.component';
import { BillingComponent } from './billing/billing.component';
import { AddAdvisorComponent } from './add-advisor/add-advisor.component';
import { AdvisorListComponent } from './advisor-list/advisor-list.component';
import { HomeComponent } from './home/home.component';
import { SupportComponent } from './support/support.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ClientDetailsBoardLayoutComponent } from './client-details-board-layout/client-details-board-layout.component';
import { LandingComponent } from './landing/landing.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ForgotPwdComponent } from './forgot-pwd/forgot-pwd.component';
import { ArchivedReportsComponent } from './archived-reports/archived-reports.component';
import { MERoutePermissionService } from './services/meroute-permission-service.service';



// import { DogListComponent } from './dogs/dog-list.component';

// Route Configuration
export const routes: Routes = [
    {
      path: '',
      redirectTo: '/landing',
       pathMatch: 'full'       
      // component: LoginComponent // Remember to import the Home Component
    },
    { 
      path: 'app-login', 
      component: LoginComponent
     
    },
    {
      path:'client-list',
      component:ClientListComponent,
      canActivate: [MERoutePermissionService]
    },
    {
      path:'client-list-new',
      component:ClientListNewComponent,
      canActivate: [MERoutePermissionService]
    },
    {
      path:'register',
      component:RegisterComponent
     
    },
    {
      path:'advisor-settings',
      component:AdvisorSettingsComponent,
      canActivate: [MERoutePermissionService]
    },
    {
      path:'app-forgot-password',
      component:ForgotPasswordComponent
      
    },
    {
      path:'add-client',
      component:AddClientComponent,
      canActivate: [MERoutePermissionService]
    },
    {
      path:'global-settings',
      component:GlobalSettingsComponent,
      canActivate: [MERoutePermissionService]
    },
    {
      path:'billing',
      component:BillingComponent,
      canActivate: [MERoutePermissionService]
    },
    {
      path:'add-advisor',
      component:AddAdvisorComponent,
      canActivate: [MERoutePermissionService]
    },
    {
      path:'advisor-list',
      component:AdvisorListComponent,
      canActivate: [MERoutePermissionService]
    },
    {
      path:'home',
      component:HomeComponent,
      canActivate: [MERoutePermissionService]
    },
    {
      path:'support',
      component:SupportComponent
     
    },
    {
      path:'calculator',
      component:CalculatorComponent,
      canActivate: [MERoutePermissionService]
    },
    {
      path:'app-client-list-new',
      component:ClientListNewComponent,
      canActivate: [MERoutePermissionService]
    },
    {
      path:'app-change-password',
      component:ChangePasswordComponent,
      canActivate: [MERoutePermissionService]
    },
    {
      path:'app-add-client',
      component:AddClientComponent,
      canActivate: [MERoutePermissionService]
    },
    {
      path:'app-add-advisor',
      component:AddAdvisorComponent,
      canActivate: [MERoutePermissionService]
    },
    {
      path:'app-global-settings',
      component:GlobalSettingsComponent,
      canActivate: [MERoutePermissionService]
    },
    {
      path:'app-client-list',
      component:ClientListComponent,
      canActivate: [MERoutePermissionService]
    },
    {

      path:'landing',
      component:LandingComponent
      
    },
    {
      path:'client-details-info-board',
      component:ClientDetailsBoardLayoutComponent,
      canActivate: [MERoutePermissionService]
    },
    {
      path:'dashboard',
      component:DashboardComponent,
      canActivate: [MERoutePermissionService]
    },
    {
      path:'forgotpwd-confirm',
      component:ForgotPwdComponent,
      
    },
    {
      path:'archived-reports',
      component:ArchivedReportsComponent,
      canActivate: [MERoutePermissionService]
    }
    ,
    {
      path:'app-advisor-list',
      component:AdvisorListComponent,
      canActivate: [MERoutePermissionService]
    }
   
  ];

// Deprecated provide
// export const APP_ROUTER_PROVIDERS = [
//   provideRouter(routes)
// ];
export const routingProviders = [
  {provide: "boardGuard", useValue: boardGuard}
];
export function boardGuard(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
  return true;
}
export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
