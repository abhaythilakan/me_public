import { Component, OnInit } from '@angular/core';
import { ModalsService } from '../services/modals.service';
import { StoreServicesService } from '../services/store-services.service';

@Component({
  selector: 'app-delete-tax-modal',
  templateUrl: './delete-tax-modal.component.html',
  styleUrls: ['./delete-tax-modal.component.scss']
})
export class DeleteTaxModalComponent implements OnInit {

  constructor(public modal:ModalsService,private storeService:StoreServicesService) { }

  ngOnInit() {
  }
  closeModal(){
    this.modal.closeModal();
    this.storeService.putObject("confirmDelete",false);
  }
  confirmModal(){
    this.storeService.putObject("confirmDelete",true);
    this.modal.closeModal();
    
  }
}
