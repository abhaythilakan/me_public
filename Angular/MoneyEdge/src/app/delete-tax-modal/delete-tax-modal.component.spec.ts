import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteTaxModalComponent } from './delete-tax-modal.component';

describe('DeleteTaxModalComponent', () => {
  let component: DeleteTaxModalComponent;
  let fixture: ComponentFixture<DeleteTaxModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteTaxModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteTaxModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
