import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { StoreServicesService } from '../services/store-services.service';
import { NotificationServicesService } from '../services/notification-services.service';
import { ModalsService } from '../services/modals.service';
import { Router } from '@angular/router';
import { ApiServicesService } from '../services/api-services.service';

@Component({
  selector: 'app-insurance-primary-model',
  templateUrl: './insurance-primary-model.component.html',
  styleUrls: ['./insurance-primary-model.component.scss']
})
export class InsurancePrimaryModelComponent implements OnInit {
  textPeriodShow: boolean;
  LifeInsTypes: any;

  constructor(private modalService: NgbModal, private storeService: StoreServicesService, private ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modal: ModalsService, private router: Router) { }

  public primaryDetails: any = {
    "InsuranceId": 0,
    "profileId": this.storeService.getObject('profileID'),
    "personId": "",
    "payDept": false,
    "payMortage": false,
    "payCollege": false,
    "financialExpence": "",
    "expenceafterdeath": "",
    "insurancePeriod": "",
    "insuranceType": "0",
    "retAsset": false,
    "otherRetAsset": false,
    "cashAccounts": false,
    "spouseRetAsset": false,
    "nonRetAsset": false,
    "selectAll": false,
    "faceValue": "",
    "type": "primary"
  }
  ngOnInit() {
    this.getInsuranceTypes();
    this.primaryDetails=this.storeService.getObject('primaryDetails');
    this.changeInsType(this.primaryDetails.insuranceType);
  }  
  changeInsType(val: any) {
    if (val == 3)
      this.textPeriodShow = true;
    else
      this.textPeriodShow = false;
  }
  selectAllChange(val: any) {
    this.primaryDetails.retAsset = this.primaryDetails.selectAll;
    this.primaryDetails.otherRetAsset = this.primaryDetails.selectAll;
    this.primaryDetails.cashAccounts = this.primaryDetails.selectAll;
    this.primaryDetails.spouseRetAsset = this.primaryDetails.selectAll;
    this.primaryDetails.nonRetAsset = this.primaryDetails.selectAll;
  }
  getInsuranceTypes() {
    this.ApiServicesService.headerAppendedGet('api/Insurance/getLifeInsTypes').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.LifeInsTypes = parsedBody.Result;
      }
    })
  }
  primaryInsDetails() {
    if (this.primaryDetails.insuranceType != 3) {
      this.primaryDetails.insurancePeriod = "";
    }
    if(this.primaryDetails.insuranceType == 3 &&(this.primaryDetails.insurancePeriod =="" ||this.primaryDetails.insurancePeriod ==null) )
    {
      this.notificationService.error("Please specify number of years");
    }
    else{
      this.primaryDetails.type = "primary";
      this.ApiServicesService.headerAppendedPost('api/Insurance/saveInsuranceDetails', this.primaryDetails).then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {
          this.primaryDetails = parsedBody.Result;
          this.storeService.putObject('primaryDetails',this.primaryDetails);
          this.modal.closeModal();
        }  
        if (parsedBody.apiStatus == 2) {
          this.notificationService.error(parsedBody.Result.Message);
  
        }
        if (parsedBody.apiStatus == 1) {
          this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);
        }
      })
    }

  }
  closeModal() {
    this.modal.closeModal();
  }
}
