import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsurancePrimaryModelComponent } from './insurance-primary-model.component';

describe('InsurancePrimaryModelComponent', () => {
  let component: InsurancePrimaryModelComponent;
  let fixture: ComponentFixture<InsurancePrimaryModelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsurancePrimaryModelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsurancePrimaryModelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
