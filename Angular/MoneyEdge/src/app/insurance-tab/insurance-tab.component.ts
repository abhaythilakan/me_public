import { Component, OnInit } from '@angular/core';
import { StoreServicesService } from '../services/store-services.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NotificationServicesService } from '../services/notification-services.service';
import { ModalsService } from '../services/modals.service';
import { ApiServicesService } from '../services/api-services.service';
import { Router } from '@angular/router';
import { ConfirmationComponent } from '../confirmation/confirmation.component';
import { isEmpty } from 'rxjs/operator/isEmpty';
import * as enLocale from 'date-fns/locale/en';
import * as frLocale from 'date-fns/locale/fr';
import { Data } from '../providers/data/data.service';
import { NgForm } from '@angular/forms/src/directives/ng_form';
import { Alert } from 'selenium-webdriver';
import { InsurancePrimaryModelComponent } from '../insurance-primary-model/insurance-primary-model.component';
import { InsuranceSpouseModelComponent } from '../insurance-spouse-model/insurance-spouse-model.component';
import { DateStringService } from '../services/date-string.service';

@Component({
  selector: 'app-insurance-tab',
  templateUrl: './insurance-tab.component.html',
  styleUrls: ['./insurance-tab.component.scss']
})
export class InsuranceTabComponent implements OnInit {
  Math: Math;
  monthlyInsuranceOut: boolean;
  monthlyInsurance: boolean;
  PremiumTypes: any;
  childrenVisibility: boolean;
  primaryVisibility: boolean;
  spouseVisibility: boolean;
  spouseDetails: any;
  LifeInsTypes: any;
  options = {
    locale: enLocale,
    minYear: 1900,
    displayFormat: 'MM-DD-YYYY',
    barTitleFormat: 'MMMM YYYY',
    firstCalendarDay: 0// 0 - Sunday, 1 - Monday
    // minDate: new Date(Date.now()), // Minimal selectable date
  };
  primaryDetails: any;
  spouseInsuranceList: any;
  primaryInsuranceList: any;
  otherInsuranceList: any;
  OtherType: any;
  InsuranceTypes: any;
  OtherTypes: any;
  textPeriodShow: boolean;
  spousePeriodShow: boolean;
  //hideTable: boolean;
  public OtherInsurance: any = {
    "monthlyPremium": "",
    "InsuranceDetails": [
      {
        "lifeID": "0",
        "profileID": this.storeService.getObject('profileID'),
        "lifeTypeID": "0",
        "company": "",
        "premiumType": "0",
        "premiumAmount": "",
        "notes": "",
        "valueAsOf": new Date(),
        "NotesShow": "",
        "showInMouseOver": false
      }
    ]
  }
  public PrimaryInsurance: any =
    {
      "personInfo":
        {
          "personID": "",
          "profileID": "",
          "firstName": "",
          "midName": "",
          "lastName": "",
          "isMale": "",
          "isPrimary": "",
          "faceValue": "",
          "InsuranceDetails": [
            {
              "lifeID": "0",
              "profileID": this.storeService.getObject('profileID'),
              "faceValue": "",
              "personID": "",
              "childID": "",
              "lifeTypeID": "0",
              "lifeTypeName": "",
              "company": "",
              "amount": "",
              "premiumType": "0",
              "premiumAmount": "",
              "notes": "",
              "valueAsOf": new Date(),
              "NotesShow": "",
              "Type": "primary",
              "showInMouseOver": false
            }]
        },
      "details": {
        "InsuranceId": "0",
        "profileId": this.storeService.getObject('profileID'),
        "personId": "",
        "payDept": false,
        "payMortage": false,
        "payCollege": false,
        "financialExpence": "",
        "expenceafterdeath": "",
        "insurancePeriod": "",
        "insuranceType": "0",
        "retAsset": false,
        "otherRetAsset": false,
        "cashAccounts": false,
        "spouseRetAsset": false,
        "nonRetAsset": false,
        "selectAll": false,
        "faceValue": "",
        "type": "primary"
      }
    }
  public SpouseInsurance: any =
    {
      "personInfo":
        {
          "personID": "",
          "profileID": "",
          "firstName": "",
          "midName": "",
          "lastName": "",
          "isMale": "",
          "isPrimary": "",
          "faceValue": "",
          "InsuranceDetails": [
            {
              "lifeID": "0",
              "profileID": this.storeService.getObject('profileID'),
              "faceValue": "",
              "personID": "",
              "childID": "",
              "lifeTypeID": "0",
              "lifeTypeName": "",
              "company": "",
              "amount": "",
              "premiumType": "0",
              "premiumAmount": "",
              "notes": "",
              "valueAsOf": new Date(),
              "NotesShow": "",
              "Type": "spouse",
              "showInMouseOver": false
            }]
        },
      "details": {
        "InsuranceId": 0,
        "profileId": this.storeService.getObject('profileID'),
        "personId": "",
        "payDept": false,
        "payMortage": false,
        "payCollege": false,
        "financialExpence": "",
        "expenceafterdeath": "",
        "insurancePeriod": "",
        "insuranceType": "0",
        "retAsset": false,
        "otherRetAsset": false,
        "cashAccounts": false,
        "spouseRetAsset": false,
        "nonRetAsset": false,
        "selectAll": false,
        "faceValue": "",
        "type": "spouse"
      }
    }
  public ChildInsurance: any = {
    "childInfo": [
      {
        "childID": "",
        "profileID": this.storeService.getObject('profileID'),
        "fName": "",
        "midName": "",
        "Lname": "",
        "isMale": "",
        "faceValue": "",
        "InsuranceDetails": [
          {
            "lifeID": 0,
            "profileID": this.storeService.getObject('profileID'),
            "personID": "",
            "childID": "",
            "lifeTypeID": 0,
            "company": "",
            "faceValue": "",
            "amount": "",
            "premiumType": "0",
            "premiumAmount": "",
            "notes": "",
            "valueAsOf": new Date(),
            "NotesShow": "",
            "Type": "child"
          }
        ]
      }
    ]
  }

  constructor(private modalService: NgbModal, private storeService: StoreServicesService, private ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService, private modal: ModalsService, public router: Router, private dateString: DateStringService) { }
  ngOnInit() {
    this.Math = Math;
    this.monthlyInsurance = true;
    this.spouseVisibility = false;
    this.primaryVisibility = false;
    this.childrenVisibility = false;
    this.textPeriodShow = false;
    this.spousePeriodShow = false;
    this.otherInsuranceList = this.OtherInsurance.InsuranceDetails;
    this.primaryInsuranceList = this.PrimaryInsurance.personInfo.InsuranceDetails;
    this.spouseInsuranceList = this.SpouseInsurance.personInfo.InsuranceDetails;
    this.primaryDetails = this.PrimaryInsurance.details;
    this.spouseDetails = this.SpouseInsurance.details;
    this.getInsuranceTypes();
    this.getAllInsurance();

  }
  addRow(index) {

    this.otherInsuranceList.push({
      "lifeID": "0",
      "profileID": this.storeService.getObject('profileID'),
      "lifeTypeID": "0",
      "company": "",
      "premiumType": "0",
      "premiumAmount": "",
      "notes": "",
      "valueAsOf": new Date(),
      "NotesShow": "",
      "showInMouseOver": false
    });
  }
  addPrimaryRow(index) {
    this.primaryInsuranceList.push({
      "lifeID": "0",
      "profileID": this.storeService.getObject('profileID'),
      "faceValue": "",
      "personID": this.PrimaryInsurance.personInfo.personID,
      "childID": "",
      "lifeTypeID": "0",
      "lifeTypeName": "",
      "company": "",
      "amount": "",
      "premiumType": "0",
      "premiumAmount": "",
      "notes": "",
      "valueAsOf": new Date(),
      "NotesShow": "",
      "Type": "primary",
      "showInMouseOver": false
    });
  }
  addSpouseRow(index) {
    this.spouseInsuranceList.push({
      "lifeID": "0",
      "profileID": this.storeService.getObject('profileID'),
      "faceValue": "",
      "personID": this.SpouseInsurance.personInfo.personID,
      "childID": "",
      "lifeTypeID": "0",
      "lifeTypeName": "",
      "company": "",
      "amount": "",
      "premiumType": "0",
      "premiumAmount": "",
      "notes": "",
      "valueAsOf": new Date(),
      "NotesShow": "",
      "Type": "spouse",
      "showInMouseOver": false
    });
  }
  addChildrenRow(childDetails, index) {
    this.ChildInsurance.childInfo[index].InsuranceDetails.push({
      "lifeID": "0",
      "profileID": this.storeService.getObject('profileID'),
      "faceValue": "",
      "personID": "",
      "childID": childDetails.childID,
      "lifeTypeID": "0",
      "lifeTypeName": "",
      "company": "",
      "amount": "",
      "premiumType": "0",
      "premiumAmount": "",
      "notes": "",
      "valueAsOf": new Date(),
      "NotesShow": "",
      "Type": "child",
      "showInMouseOver": false
    })
  }

  otherChangeType(val: any) {
    this.OtherType = val;
  }

  addprimarydetails() {
    this.storeService.putObject('primaryDetails', this.primaryDetails);
    this.modal.openModal(InsurancePrimaryModelComponent, 'lg', '');
    this.modal.resultFunction().then(res => {
      this.primaryDetails = this.storeService.getObject('primaryDetails');
    })
  }
  addSpousedetails() {
    this.storeService.putObject('spouseDetails', this.spouseDetails);
    this.modal.openModal(InsuranceSpouseModelComponent, 'lg', '');
    this.modal.resultFunction().then(res => {
      this.spouseDetails = this.storeService.getObject('spouseDetails');
    })
  }
  saveOtherInsurance() {
    for (let i = this.otherInsuranceList.length - 1; i > 0; --i) {
      if (this.otherInsuranceList[i].lifeTypeID == "0" &&
        this.otherInsuranceList[i].company == "" &&
        this.otherInsuranceList[i].premiumType == "0" &&
        this.otherInsuranceList[i].premiumAmount == "" &&
        this.otherInsuranceList[i].valueAsOf == "") {
        this.otherInsuranceList.splice(i, 1);
      }
    }
    for (let item of this.otherInsuranceList) {
      item.valueAsOf = this.dateString.convert(item.valueAsOf);
    }
    let req = this.otherInsuranceList;
    this.ApiServicesService.headerAppendedPost('api/Insurance/saveOtherInsurance', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.OtherInsurance = parsedBody.Result;

        this.otherInsuranceList = this.OtherInsurance.InsuranceDetails;
        this.notificationService.success("Other insurance saved successfully");
      }

      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);

      }
    })
  }
  saveInsurancePrimary() {
    for (let i = this.primaryInsuranceList.length - 1; i > 0; --i) {
      console.log("fdsafsa" + this.primaryInsuranceList[i].valueAsOf);
      if (this.primaryInsuranceList[i].lifeTypeID == "0" &&
        this.primaryInsuranceList[i].faceValue == "" &&
        this.primaryInsuranceList[i].company == "" &&
        this.primaryInsuranceList[i].amount == "" &&
        this.primaryInsuranceList[i].premiumType == "0" &&
        this.primaryInsuranceList[i].premiumAmount == "" &&
        this.primaryInsuranceList[i].valueAsOf == "") {
        this.primaryInsuranceList.splice(i, 1);
      }
    }
    for (let item of this.primaryInsuranceList) {
      item.valueAsOf = this.dateString.convert(item.valueAsOf);
    }
    this.primaryInsuranceList[0].Type = "primary";
    let req = this.primaryInsuranceList;
    this.ApiServicesService.headerAppendedPost('api/Insurance/saveInsurance', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.PrimaryInsurance = parsedBody.Result;
        this.primaryInsuranceList = this.PrimaryInsurance.personInfo.InsuranceDetails;
        this.notificationService.success(this.PrimaryInsurance.personInfo.firstName + "'s insurance saved successfully");
      }

      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);

      }
    })
  }
  updatedPrimaryTotal() {
    this.PrimaryInsurance.personInfo.faceValue = this.primaryInsuranceList
      .map(c => c.faceValue > 0 ? parseFloat(c.faceValue) : 0)
      .reduce((sum, current) => sum + current);
  }
  updatedSpouseTotal() {
    this.SpouseInsurance.personInfo.faceValue = this.spouseInsuranceList
      .map(c => c.faceValue > 0 ? parseFloat(c.faceValue) : 0)
      .reduce((sum, current) => sum + current);
  }
  updatedChildrenTotal(index) {
    this.ChildInsurance.childInfo[index].InsuranceDetails
    this.ChildInsurance.childInfo[index].faceValue = this.ChildInsurance.childInfo[index].InsuranceDetails
      .map(c => c.faceValue > 0 ? parseFloat(c.faceValue) : 0)
      .reduce((sum, current) => sum + current);
  }
  saveInsuranceSpouse() {
    for (let i = this.spouseInsuranceList.length - 1; i > 0; --i) {
      if (this.spouseInsuranceList[i].lifeTypeID == "0" &&
        this.spouseInsuranceList[i].faceValue == "" &&
        this.spouseInsuranceList[i].company == "" &&
        this.spouseInsuranceList[i].amount == "" &&
        this.spouseInsuranceList[i].premiumType == "0" &&
        this.spouseInsuranceList[i].premiumAmount == "" &&
        this.spouseInsuranceList[i].valueAsOf == "") {
        this.spouseInsuranceList.splice(i, 1);
      }
    }
    for (let item of this.spouseInsuranceList) {
      item.valueAsOf = this.dateString.convert(item.valueAsOf);
    }
    this.spouseInsuranceList[0].Type = "spouse";
    let req = this.spouseInsuranceList;
    this.ApiServicesService.headerAppendedPost('api/Insurance/saveInsurance', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.SpouseInsurance = parsedBody.Result;
        this.spouseInsuranceList = this.SpouseInsurance.personInfo.InsuranceDetails;
        this.notificationService.success(this.SpouseInsurance.personInfo.firstName + "'s insurance saved successfully");
      }

      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);

      }
    })
  }
  saveInsuranceChildren(child, index) {
    let childInsuranceList = child.InsuranceDetails;
    for (let i = childInsuranceList.length - 1; i > 0; --i) {
      if (childInsuranceList.lifeTypeID == "0" &&
        childInsuranceList.faceValue == "" &&
        childInsuranceList.company == "" &&
        childInsuranceList.amount == "" &&
        childInsuranceList.premiumType == "0" &&
        childInsuranceList.premiumAmount == "" &&
        childInsuranceList.valueAsOf == "") {
        childInsuranceList.splice(i, 1);
      }
    }
    for (let item of childInsuranceList) {
      item.valueAsOf = this.dateString.convert(item.valueAsOf);
    }
    let req = childInsuranceList;
    this.ApiServicesService.headerAppendedPost('api/Insurance/saveChildInsurance', req).then(res => {
      let parsedBody = JSON.parse(res._body);
      if (parsedBody.apiStatus == 0) {
        this.ChildInsurance.childInfo[index].InsuranceDetails = parsedBody.Result.childInfo[0].InsuranceDetails;
        this.ChildInsurance.childInfo[index].faceValue = parsedBody.Result.childInfo[0].faceValue;
        this.notificationService.success(child.fName + "'s insurance saved successfully");
      }

      if (parsedBody.apiStatus == 2) {
        this.notificationService.error(parsedBody.Result.Message);

      }
      if (parsedBody.apiStatus == 1) {
        this.notificationService.error(parsedBody.Result.ValidationErrors[0].FieldErrorMessage);

      }
    })
  }
  getInsuranceTypes() {
    this.ApiServicesService.headerAppendedGet('api/Insurance/getOtherInsuranceTypes').then(res => {

      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.OtherTypes = parsedBody.Result;
      }
    })

    this.ApiServicesService.headerAppendedGet('api/Insurance/getInsuranceTypes').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.InsuranceTypes = parsedBody.Result;
      }
    })

    this.ApiServicesService.headerAppendedGet('api/Insurance/getLifeInsTypes').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.LifeInsTypes = parsedBody.Result;
      }
    })
    this.ApiServicesService.headerAppendedGet('api/Insurance/getPremiumTypes').then(res => {
      if (res && res._body && !res._body.target) {
        let parsedBody = JSON.parse(res._body);
        this.PremiumTypes = parsedBody.Result;
      }
    })
  }
  getAllInsurance() {
    this.ApiServicesService.headerAppendedPost('api/Insurance/getAllInsurance', this.storeService.getObject('profileID'))
      .then(res => {
        let parsedBody = JSON.parse(res._body);
        if (parsedBody.apiStatus == 0) {

          if (parsedBody.Result.OtherInsurance != null) {
            this.OtherInsurance = parsedBody.Result.OtherInsurance;
            this.otherInsuranceList = this.OtherInsurance.InsuranceDetails;
          }
          if (parsedBody.Result.PrimaryInsurance.personInfo != null) {
            this.primaryVisibility = true;
            this.PrimaryInsurance = parsedBody.Result.PrimaryInsurance;
            this.primaryInsuranceList = this.PrimaryInsurance.personInfo.InsuranceDetails;

            this.primaryDetails = this.PrimaryInsurance.details;
            //this.changeInsType(this.primaryDetails.insuranceType);
          }
          if (parsedBody.Result.SpouseInsurance.personInfo != null) {
            this.spouseVisibility = true;
            this.SpouseInsurance = parsedBody.Result.SpouseInsurance;
            this.spouseInsuranceList = this.SpouseInsurance.personInfo.InsuranceDetails;
            this.spouseDetails = this.SpouseInsurance.details;
            //this.changeSpouseInsType(this.spouseDetails.insuranceType);
          }
          if (parsedBody.Result.ChildInsurance.childInfo != null && parsedBody.Result.ChildInsurance.childInfo.length > 0) {
            this.childrenVisibility = true;
            this.ChildInsurance = parsedBody.Result.ChildInsurance;
          }
        }
      })
  }

  removeOtherInsurance(otherIns, i) {
    this.modal.openModal(ConfirmationComponent, '', '');
    this.modal.resultFunction().then(res => {
      if (otherIns.lifeID == 0) {
        this.otherInsuranceList.splice(i, i);
      }
      else {
        let req = {
          "profileId": otherIns.profileID,
          "lifeId": otherIns.lifeID
        }
        let header = {
          "Content-Type": "application/json"
        }
        this.
          ApiServicesService.headerAppendedPost('api/Insurance/deleteOtherInsurance', req)
          .then(res => {
            let parsedBody = JSON.parse(res._body);
            if (parsedBody.apiStatus == 0) {
              this.OtherInsurance = parsedBody.Result;
              this.otherInsuranceList = this.OtherInsurance.InsuranceDetails;
            }
          })
          .catch(err => {
            if (err && err._body && !err._body.target) {
              let parsedBody = JSON.parse(err._body);
              if (parsedBody.error && parsedBody.message)
                this.notificationService.error(parsedBody.message);
              else
                this.notificationService.error('Could not delete');
            } else {
              this.notificationService.error('Could not delete');
            }
          });
      }
    })
  }
  removePrimaryInsurance(primaryIns, i) {
    this.modal.openModal(ConfirmationComponent, '', '');
    this.modal.resultFunction().then(res => {
      if (primaryIns.lifeID == 0) {
        this.primaryInsuranceList.splice(i, i);
        this.updatedPrimaryTotal();
      }
      else {
        let req = {
          "profileId": primaryIns.profileID,
          "lifeId": primaryIns.lifeID,
          "type": "primary"
        }
        let header = {
          "Content-Type": "application/json"
        }
        this.
          ApiServicesService.headerAppendedPost('api/Insurance/deleteInsurance', req)
          .then(res => {
            let parsedBody = JSON.parse(res._body);
            if (parsedBody.apiStatus == 0) {
              this.PrimaryInsurance = parsedBody.Result;
              this.primaryInsuranceList = this.PrimaryInsurance.personInfo.InsuranceDetails;
            }
          })
          .catch(err => {
            if (err && err._body && !err._body.target) {
              let parsedBody = JSON.parse(err._body);
              if (parsedBody.error && parsedBody.message)
                this.notificationService.error(parsedBody.message);
              else
                this.notificationService.error('Could not delete');
            } else {
              this.notificationService.error('Could not delete');
            }
          });
      }
    })
  }
  removeSpouseInsurance(spouseIns, i) {
    this.modal.openModal(ConfirmationComponent, '', '');
    this.modal.resultFunction().then(res => {
      if (spouseIns.lifeID == 0) {
        this.spouseInsuranceList.splice(i, i);
        this.updatedSpouseTotal();
      }
      else {
        let req = {
          "profileId": spouseIns.profileID,
          "lifeId": spouseIns.lifeID,
          "type": "spouse"
        }
        let header = {
          "Content-Type": "application/json"
        }
        this.
          ApiServicesService.headerAppendedPost('api/Insurance/deleteInsurance', req)
          .then(res => {
            let parsedBody = JSON.parse(res._body);
            if (parsedBody.apiStatus == 0) {
              this.SpouseInsurance = parsedBody.Result;
              this.spouseInsuranceList = this.SpouseInsurance.personInfo.InsuranceDetails;
            }
          })
          .catch(err => {
            if (err && err._body && !err._body.target) {
              let parsedBody = JSON.parse(err._body);
              if (parsedBody.error && parsedBody.message)
                this.notificationService.error(parsedBody.message);
              else
                this.notificationService.error('Could not delete');
            } else {
              this.notificationService.error('Could not delete');
            }
          });
      }
    })
  }
  removeChildInsurance(insurance, p, i) {
    this.modal.openModal(ConfirmationComponent, '', '');
    this.modal.resultFunction().then(res => {
      if (insurance.lifeID == 0) {
        this.ChildInsurance.childInfo[p].InsuranceDetails.splice(i, i);
        this.updatedChildrenTotal(p);
      }
      else {
        let req = {
          "profileId": insurance.profileID,
          "lifeId": insurance.lifeID,
          "childId": insurance.childID
        }
        let header = {
          "Content-Type": "application/json"
        }
        this.
          ApiServicesService.headerAppendedPost('api/Insurance/deleteChildInsurance', req)
          .then(res => {
            let parsedBody = JSON.parse(res._body);
            if (parsedBody.apiStatus == 0) {
              this.ChildInsurance.childInfo[p].InsuranceDetails = parsedBody.Result.childInfo[0].InsuranceDetails;
              this.ChildInsurance.childInfo[p].faceValue = parsedBody.Result.childInfo[0].faceValue;
            }
          })
          .catch(err => {
            if (err && err._body && !err._body.target) {
              let parsedBody = JSON.parse(err._body);
              if (parsedBody.error && parsedBody.message)
                this.notificationService.error(parsedBody.message);
              else
                this.notificationService.error('Could not delete');
            } else {
              this.notificationService.error('Could not delete');
            }
          });
      }
    })
  }
  toggle() {
    setTimeout(() => { this.monthlyInsuranceOut = !this.monthlyInsuranceOut }, 0);
    setTimeout(() => { this.monthlyInsurance = !this.monthlyInsurance }, 500);
  }
}
