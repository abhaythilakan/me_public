import { Component, OnInit, NgZone } from '@angular/core';
import { StoreServicesService } from '../services/store-services.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiServicesService } from '../services/api-services.service';
import { NotificationServicesService } from '../services/notification-services.service';

@Component({
  selector: 'app-client-details-board-layout',
  templateUrl: './client-details-board-layout.component.html',
  styleUrls: ['./client-details-board-layout.component.scss']
})
export class ClientDetailsBoardLayoutComponent implements OnInit {
  dashboardCls: boolean;
  clientName: any;
  retirementCls: boolean;
  protectionfundCls: boolean;
  emergencyFund: boolean;
  expenseCls: boolean;
  insuranceCls: boolean;
  debtCls: boolean;
  educationCls: boolean;
  activateCls: boolean;
  incomeCls: boolean;
  personalCls: boolean;
  profileID: any;
  clientId: number;
  personal: boolean;
  sub: any;
  constructor(private storeService: StoreServicesService, private router: Router, public lc: NgZone, private route: ActivatedRoute, public ApiServicesService: ApiServicesService,
    private notificationService: NotificationServicesService) {

  }

  ngOnInit() {
    this.sub = this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.clientId = +params['page'] || 0;
        this.storeService.putObject("clientId", this.clientId);

        console.log(("client id in page" + this.clientId));
      });

    this.personalCls = true;

    if (this.storeService.getObject('clientName')) {

      this.clientName = this.storeService.getObject('clientName');
    }
    else {

      this.clientName = "Steven Smith";
    }
  }
  activateClientDashboard() {
    this.dashboardCls=true;
    this.personalCls = false;
    this.incomeCls = false;
    this.activateCls = false;
    this.educationCls = false;
    this.debtCls = false;
    this.insuranceCls = false;
    this.expenseCls = false;
    this.emergencyFund = false;
    this.protectionfundCls = false;
    this.retirementCls = false;

  }
  activatePersonalInfo() {

    this.personalCls = true;
    this.incomeCls = false;
    this.activateCls = false;
    this.educationCls = false;
    this.debtCls = false;
    this.insuranceCls = false;
    this.expenseCls = false;
    this.emergencyFund = false;
    this.protectionfundCls = false;
    this.retirementCls = false;
    this.dashboardCls=false;
  }
  activateIncome() {
    this.personalCls = false;
    this.incomeCls = true;
    this.activateCls = false;
    this.educationCls = false;
    this.debtCls = false;
    this.insuranceCls = false;
    this.expenseCls = false;
    this.emergencyFund = false;
    this.protectionfundCls = false;
    this.retirementCls = false;
    this.dashboardCls=false;

  }
  activateAsset() {
    this.personalCls = false;
    this.incomeCls = false;
    this.activateCls = true;
    this.educationCls = false;
    this.debtCls = false;
    this.insuranceCls = false;
    this.expenseCls = false;
    this.emergencyFund = false;
    this.protectionfundCls = false;
    this.retirementCls = false;
    this.dashboardCls=false;

  }
  activateEducation() {
    this.personalCls = false;
    this.incomeCls = false;
    this.activateCls = false;
    this.educationCls = true;
    this.debtCls = false;
    this.insuranceCls = false;
    this.expenseCls = false;
    this.emergencyFund = false;
    this.protectionfundCls = false;
    this.retirementCls = false;
    this.dashboardCls=false;

  }
  activateDebt() {
    this.personalCls = false;
    this.incomeCls = false;
    this.activateCls = false;
    this.educationCls = false;
    this.debtCls = true;
    this.insuranceCls = false;
    this.expenseCls = false;
    this.emergencyFund = false;
    this.protectionfundCls = false;
    this.retirementCls = false;
    this.dashboardCls=false;

  }
  activateInsurance() {
    this.personalCls = false;
    this.incomeCls = false;
    this.activateCls = false;
    this.educationCls = false;
    this.debtCls = false;
    this.insuranceCls = true;
    this.expenseCls = false;
    this.emergencyFund = false;
    this.protectionfundCls = false;
    this.retirementCls = false;
    this.dashboardCls=false;

  }
  activateExpense() {
    this.personalCls = false;
    this.incomeCls = false;
    this.activateCls = false;
    this.educationCls = false;
    this.debtCls = false;
    this.insuranceCls = false;
    this.expenseCls = true;
    this.emergencyFund = false;
    this.protectionfundCls = false;
    this.retirementCls = false;
    this.dashboardCls=false;

  }
  activateEmergencyFund() {
    this.personalCls = false;
    this.incomeCls = false;
    this.activateCls = false;
    this.educationCls = false;
    this.debtCls = false;
    this.insuranceCls = false;
    this.expenseCls = false;
    this.emergencyFund = true;
    this.protectionfundCls = false;
    this.retirementCls = false;
    this.dashboardCls=false;

  }
  activateProtectionFund() {
    this.personalCls = false;
    this.incomeCls = false;
    this.activateCls = false;
    this.educationCls = false;
    this.debtCls = false;
    this.insuranceCls = false;
    this.expenseCls = false;
    this.emergencyFund = false;
    this.protectionfundCls = true;
    this.retirementCls = false;
    this.dashboardCls=false;

  }
  activateRetirement() {
    this.personalCls = false;
    this.incomeCls = false;
    this.activateCls = false;
    this.educationCls = false;
    this.debtCls = false;
    this.insuranceCls = false;
    this.expenseCls = false;
    this.emergencyFund = false;
    this.protectionfundCls = false;
    this.retirementCls = true;
    this.dashboardCls=false;
  }

}
