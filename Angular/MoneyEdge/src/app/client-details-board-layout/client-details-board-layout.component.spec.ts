import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientDetailsBoardLayoutComponent } from './client-details-board-layout.component';

describe('ClientDetailsBoardLayoutComponent', () => {
  let component: ClientDetailsBoardLayoutComponent;
  let fixture: ComponentFixture<ClientDetailsBoardLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientDetailsBoardLayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientDetailsBoardLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
