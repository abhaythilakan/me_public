import { Component, OnInit ,Input } from '@angular/core';

import {NgbModal, NgbActiveModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import { ViewContainerRef } from '@angular/core';
import { ModalsService } from '../services/modals.service';
import { StoreServicesService } from '../services/store-services.service';
@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.scss']
})
export class ConfirmationComponent implements OnInit {

  
  constructor(public modal:ModalsService,private storeService:StoreServicesService) { 
 
   }

  ngOnInit() {

  }
  closeModal(){
    this.modal.closeModal();
    this.storeService.putObject("confirmDelete",false);
  }
  confirmModal(){
    this.storeService.putObject("confirmDelete",true);
    this.modal.closeModal();
    
  }
  dismissModalpopup(){
    this.modal.dismissModal();
  }
}
