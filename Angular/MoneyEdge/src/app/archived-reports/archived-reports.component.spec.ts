import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchivedReportsComponent } from './archived-reports.component';

describe('ArchivedReportsComponent', () => {
  let component: ArchivedReportsComponent;
  let fixture: ComponentFixture<ArchivedReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchivedReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivedReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
