﻿using CLayer;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLayer
{
    public class ProfileReg
    {
        public static ProfileModel saveProfile(ProfileModel data)
        {
            DLayer.ProfileReg cl = new DLayer.ProfileReg();
            return cl.saveProfile(data);
        }
        public static ProfileCollection saveProfileFull(ProfileCollection data)
        {
            DLayer.ProfileReg cl = new DLayer.ProfileReg();
            return cl.saveProfileFull(data);
        }
        public static ProfileCollection getProfileDetails(long ProfileID)
        {
            DLayer.ProfileReg cl = new DLayer.ProfileReg();
            return cl.getProfileDetails(ProfileID);
        }
        public static List<ProfileModel> getProfileList(ProfileListArgModel data)
        {
            DLayer.ProfileReg cl = new DLayer.ProfileReg();
            return cl.getProfileList(data);
        }
        public static bool deleteProfile(long profileID)
        {
            DLayer.ProfileReg cl = new DLayer.ProfileReg();
            return cl.deleteProfile(profileID);
        }
    }
}
