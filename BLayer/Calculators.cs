﻿using CLayer;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLayer
{
    public class Calculators
    {
        public static  EarlyInvestorModel getEarlyInvestor(EarlyInvestorModel inobj)
        {
            DLayer.Calculators dl = new DLayer.Calculators();
            return dl.getEarlyInvestor(inobj);
        }
        public static TaxableVsTaxDefferedModel TaxableVsTaxDeffered(TaxableVsTaxDefferedModel inobj)
        {
            DLayer.Calculators dl = new DLayer.Calculators();
            return dl.TaxableVsTaxDeffered(inobj);
        }
        public static DIMEModel DIME(DIMEModel inobj)
        {
            DLayer.Calculators dl = new DLayer.Calculators();
            return dl.DIME(inobj);
        }
        public static CompoundInterestModel compoundInterestAtGo(CompoundInterestModel modelObj)
        {
            DLayer.Calculators dl = new DLayer.Calculators();
            return dl.compoundInterestAtGo(modelObj);
        }
        public static GoalsAndDreamsModel GoalsAndDreamsAtGo(GoalsAndDreamsModel modelObj)
        {
            DLayer.Calculators dl = new DLayer.Calculators();
            return dl.GoalsAndDreamsAtGo(modelObj);
        }
        public static CreditCardModel creditCardCalculator(CreditCardModel inobj)
        {
            DLayer.Calculators dl = new DLayer.Calculators();
            return dl.creditCardCalculators(inobj);
        }
        public static CreditCardModel creditCardAmmortCal(CreditCardModel inobj)
        {
            DLayer.Calculators dl = new DLayer.Calculators();
            return dl.creditCardAmmortCal(inobj);
        }

        public static VehicleLoanModel vehicleloancalculator(VehicleLoanModel inobj,int status=0)
        {
            DLayer.Calculators dl = new DLayer.Calculators();
            return dl.vehicleloancalculator(inobj, status);
        }
        public static FixedvsMinimumCalcModel FixedvsMinimumCalculator(FixedvsMinimumCalcModel inobj)
        {
            DLayer.Calculators dl = new DLayer.Calculators();
            return dl.FixedvsMinimumCalculator(inobj);
        }
        public static FixedvsMinimumCalcModel FixedvsMinimumAmort(FixedvsMinimumCalcModel inobj)
        {
            DLayer.Calculators dl = new DLayer.Calculators();
            return dl.FixedvsMinimumAmort(inobj);
        }
        public static Mortgage_PI_Model mortgageInterestCalculator(Mortgage_PI_Model modelObj)
        {
            DLayer.Calculators dl = new DLayer.Calculators();
            return dl.mortgageInterestCalculator(modelObj);
        }
        public static MortgagePaymentModel mortgagePaymentCalculator(MortgagePaymentModel modelObj)
        {
            DLayer.Calculators dl = new DLayer.Calculators();
            return dl.mortgagePaymentCalculator(modelObj);
        }
        public static CollegecostListArg collegecostList(CollegecostListArg inobj)
        {
            DLayer.Calculators dl = new DLayer.Calculators();
            return dl.collegecostList(inobj);
        }
        public static MortgageMissingValueModel missingValueCalculate(MortgageMissingValueModel modelObj)
        {
            DLayer.Calculators dl = new DLayer.Calculators();
            return dl.mortgageMissingValue(modelObj);
        }
        public static List<CollegeCostResultModel> getcollegeCost()
        {
            DLayer.Calculators dl = new DLayer.Calculators();
            return dl.getcollegeCost();
        }
        public static mortgageAmortizationCalculator mortChart(Mortgage_PI_Model modelobj)
        {
            DLayer.Calculators dl = new DLayer.Calculators();
            return dl.mortgageAmortization(modelobj);
        }
    }
}
