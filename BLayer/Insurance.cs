﻿using CLayer;
using CLayer.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLayer
{
    public static class Insurance
    {
        public static FullInsurance GetFullInsurance(long id)
        {
            DLayer.Insurance dl = new DLayer.Insurance();
            return dl.GetFullInsurance(id);
        }
        public static OtherLifeInsurance SaveOtherInsurance(List<OtherInsuranceModel> inputList,long userId)
        {
            DLayer.Insurance dl = new DLayer.Insurance();
            return dl.SaveOtherInsurance(inputList, userId);
        }

        public static FullLifeInsurance SaveInsurance(List<InsuranceModel> inputList,long userId)
        {
            DLayer.Insurance dl = new DLayer.Insurance();
            return dl.SaveInsurance(inputList, userId);
        }

        public static ChildInsurance SaveChildInsurance(List<InsuranceModel> inputList,long userId)
        {
            DLayer.Insurance dl = new DLayer.Insurance();
            return dl.SaveChildInsurance(inputList,userId);
        }
        public static InsuranceDetailsModel SaveInsuranceDetails(InsuranceDetailsModel input,long userId)
        {
            DLayer.Insurance dl = new DLayer.Insurance();
            return dl.SaveInsuranceDetails(input, userId);
        }

        public static OtherLifeInsurance DeleteOtherInsurance(OtherInsuranceInputModel input,long userId)
        {
            DLayer.Insurance dl = new DLayer.Insurance();
            return dl.DeleteOtherInsurance(input, userId);
        }

        public static FullLifeInsurance DeleteInsurance(InsuranceInputModel input, long userId)
        {
            DLayer.Insurance dl = new DLayer.Insurance();
            return dl.DeleteInsurance(input, userId);
        }
        public static ChildInsurance DeleteChildInsurance(InsuranceChildInput input, long userId)
        {
            DLayer.Insurance dl = new DLayer.Insurance();
            return dl.DeleteChildInsurance(input, userId);
        }
    }
}
