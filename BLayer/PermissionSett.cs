﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLayer
{
    public class PermissionSett
    {
        public static List<PermissionModel> getClientList(long userID, CustomEnum.userCatEnum usrCat, ME_v2Entities db)
        {
            DLayer.PermissionSett pr = new DLayer.PermissionSett();
            return pr.getPermissionList(userID, usrCat);
        }
        public static List<PermissionModel> savePermission(List<PermissionModel> data, long id, CustomEnum.userCatEnum userType, ME_v2Entities db)
        {
            DLayer.PermissionSett pr = new DLayer.PermissionSett();
            return pr.savePermission(data, id, userType, db);
        }
        public static AccesSettingsModel savePermission(AccesSettingsModel data, long id,  ME_v2Entities db)
        {
            DLayer.PermissionSett pr = new DLayer.PermissionSett();
            return pr.saveAdvisorPermission(data, id,  db);
        }
    }
}
