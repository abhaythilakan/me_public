﻿using CLayer;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLayer
{
    public class AdvisorReg
    {

        public static List<AdvisorEmailListModel> getAdvListBasedOnCat(CLayer.common.CustomEnum.EmailAdvisorCategory enm, string couponcode)
        {
            DLayer.AdvisorReg dl = new DLayer.AdvisorReg();
            return dl.getAdvListBasedOnCat(enm, couponcode);
        }


        //    public static AdvisorModel.AdvisorDetails advisorReg(string fname, string lname, string username, string password)//string username = "danharriman", string password = "edge2014"
        public static advisorDetails advisorReg(CLayer.advisorDetails AdvisorDetails)//string username = "danharriman", string password = "edge2014"
        {
            DLayer.AdvisorReg dl = new DLayer.AdvisorReg();
            return dl.advisorReg(AdvisorDetails);
        }
        public static advisorDetails addnewadvisor(CLayer.advisorDetails AdvisorDetails, long managerID)
        {
            DLayer.AdvisorReg dl = new DLayer.AdvisorReg();
            return dl.addnewadvisor(AdvisorDetails, managerID);
        }


        public static bool advImageUpload(string path, long id)
        {
            DLayer.AdvisorReg dl = new DLayer.AdvisorReg();
            return dl.advImgUpload(path, id);

        }

        public static string getdisclaimerbody(long id)
        {
            DLayer.AdvisorReg dl = new DLayer.AdvisorReg();
            return dl.getDisclaimer(id);

        }

        public static advisorDetails getAdvisorDetails(long advisorID, long userID)
        {

            DLayer.AdvisorReg dl = new DLayer.AdvisorReg();
            return dl.GetAdvisorDetails(advisorID, userID);
        }


        public static advisorDetails getAddNewAdvisorDetails(long advisorID)
        {

            DLayer.AdvisorReg dl = new DLayer.AdvisorReg();
            return dl.GetAddnewAdvisorDetails(advisorID);
        }

        public static AdvisorListModel getAdvisorList(AdvisorListModel inObj)
        {
            DLayer.AdvisorReg dl = new DLayer.AdvisorReg();
            return dl.getAdvisorList(inObj);
        }

        public static bool advisordelete(long id)
        {
            DLayer.AdvisorReg dl = new DLayer.AdvisorReg();
            return dl.advisordelete(id);
        }
    }
}
