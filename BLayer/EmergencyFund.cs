﻿using CLayer;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BLayer
{
    public static class EmergencyFund
    {
        public static EmergencyFundTypes getEmergencyTypes()
        {
            DLayer.EmergencyFund dl = new DLayer.EmergencyFund();
            return dl.getEmergencyTypes();
        }
        public static EmergencyFundModel getEmergencyFund(long profileID)
        {
            DLayer.EmergencyFund dl = new DLayer.EmergencyFund();
            return dl.getEmergencyFund(profileID);
        }
       
        public static EmergencyFundModel getMonthlySavings(EmergencyFundModel ef, long profileID)
        {
            DLayer.EmergencyFund dl = new DLayer.EmergencyFund();
            return dl.getMonthlySavings(ef,profileID);
        }


    }
}
