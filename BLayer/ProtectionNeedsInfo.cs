﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BLayer
{
   public class ProtectionNeedsInfo
    {
        public static ProtectionNeeds getProtectionNeeds(long profileID)
        {
            DLayer.ProtectionNeedDetail pd = new DLayer.ProtectionNeedDetail();
            return pd.getProtectioninfo(profileID);
        }
        public static ProtectionNeeds goProtectionNeeds(ProtectionNeeds pm)
        {
            DLayer.ProtectionNeedDetail pd = new DLayer.ProtectionNeedDetail();
            return pd.goProtectioninfo(pm);
        }
    }
}
