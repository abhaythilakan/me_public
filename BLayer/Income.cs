﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLayer
{
    public class Income
    {
       
        public static List<IncomeModel> saveFutureIncome(List<IncomeModel> inobj, long userID)
        {
            DLayer.IncomeDetails dl = new DLayer.IncomeDetails();
            return dl.saveFutureIncome(inobj, userID);

        }
        public static List<IncomeTaxDetailModel> saveTaxeDetails(List<IncomeTaxDetailModel> inObj, long usrID)
        {
            DLayer.IncomeDetails dl = new DLayer.IncomeDetails();
            return dl.saveTaxeDetails(inObj, usrID);

        }
        public static List<IncomeTaxDetailModel> savepayrollTaxeDetails(List<IncomeTaxDetailModel> inObj, long usrID)
        {
            DLayer.IncomeDetails dl = new DLayer.IncomeDetails();
            return dl.savepayrollTaxeDetails(inObj, usrID);

        }
        public static List<IncomeTaxDetailModel> saveOtherTaxeDetails(List<IncomeTaxDetailModel> inObj, long usrID)
        {
            DLayer.IncomeDetails dl = new DLayer.IncomeDetails();
            return dl.saveOtherTaxeDetails(inObj, usrID);

        }
        public static bool deleteTax(long deleteID, long usrID)
        {
            DLayer.IncomeDetails dl = new DLayer.IncomeDetails();
            return dl.deleteTax( deleteID, usrID);
        }
        public static bool deleteFutureincome(long deleteID, long usrID)
        {
            DLayer.IncomeDetails dl = new DLayer.IncomeDetails();
            return dl.dedeleteFutureincome(deleteID, usrID);
        }
        public static IncomeProjectionObjectsModel incomeProjection(long profileID,decimal r1=0,decimal r2=0)
        {
            DLayer.IncomeDetails dl = new DLayer.IncomeDetails();
            IncomeProjectionObjectsModel outobj = new IncomeProjectionObjectsModel();
            List<IncomeProjectionModel> inobj = new List<IncomeProjectionModel>();
            inobj = dl.incomeProjection(profileID, r1, r2); ;

            foreach (var a in inobj)
            {
                if (a.role == "Primary")
                {
                    outobj.primary = new IncomeProjectionModel();
                    outobj.primary = a;
                }
                else if (a.role == "Spouse")
                {
                    outobj.spouse = new IncomeProjectionModel();
                    outobj.spouse = a;
                }
                else if (a.role == "Other")
                {
                    outobj.other = new IncomeProjectionModel();
                    outobj.other = a;
                }

            }

            return outobj;

        }
        public static List<IncomeModel> GetIncomeDetails(long usrId, CustomEnum.IncomeTypeEnum type)
        {
            DLayer.IncomeDetails dl = new DLayer.IncomeDetails();
            return dl.getIncomes(usrId, type);
        }
        public static List<IncomeTaxDetailModel> getTaxDetails(long id)
        {
            DLayer.IncomeDetails dl = new DLayer.IncomeDetails();
            return dl.getTaxes(id);
        }
        public static IncomeOtherTaxModel getotherTaxDetails(long id)
        {
            DLayer.IncomeDetails dl = new DLayer.IncomeDetails();
            return dl.getOtherTaxes(id);
        }
        public static List<IncomeModel> saveMonthlyIncome(List<IncomeModel> inobj, long userID)
        {
            DLayer.IncomeDetails dl = new DLayer.IncomeDetails();
            return dl.saveMonthlyIncome(inobj, userID);

        }

        
    }
}
