﻿using CLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLayer
{
    public static class Expense
    {
        public static LivingExpenseModel GetLivingExpense(long id)
        {
            DLayer.Expense dl = new DLayer.Expense();
            return dl.GetLivingExpense(id);
        }

        public static LivingExpenseModel SaveLivingExpense(List<ExpenseModel> inputList,long userId)
        {
            DLayer.Expense dl = new DLayer.Expense();
            return dl.SaveLivingExpense(inputList, userId);
        }

        public static LivingExpenseModel DeleteLivingExpense(ExpenseInputModel input,long userId)
        {
            DLayer.Expense dl = new DLayer.Expense();
            return dl.DeleteLivingExpense(input, userId);
        }
        public static ExpenseListModel GetAllExpenseList(long profileId)
        {
            DLayer.Expense dl = new DLayer.Expense();
            return dl.GetAllExpenseList(profileId);
        }
    }
}
