﻿using CLayer;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLayer
{
    public class RetirementGoal
    {
        public static RetirementModel GetRetirementDetails(long ID)
        {
            DLayer.RetirementGoal cl = new DLayer.RetirementGoal();
            return cl.GetRetirementDetails(ID);
        }

        public static RetirementModel GetRetirementDetailsGo(RetirementModel inobj)
        {
            DLayer.RetirementGoal cl = new DLayer.RetirementGoal();
            return cl.FINCalculation(inobj);
        }
    }
}
