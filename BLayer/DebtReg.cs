﻿using CLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLayer
{
    public class DebtReg
    {
        public static List<DebtModel> getMortageList(long profileId)
        {
            DLayer.DebtReg db = new DLayer.DebtReg();
            return db.getMortageList(profileId);
        }
        public static List<DebtModel> getFixedList(long profileId)
        {
            DLayer.DebtReg db = new DLayer.DebtReg();
            return db.getFixedList(profileId);
        }
        public static List<DebtModel> getRevolvingList(long profileId)
        {
            DLayer.DebtReg db = new DLayer.DebtReg();
            return db.getRevolvingList(profileId);
        }
        public static DebtModel getDebtDetails(long debtId)
        {
            DLayer.DebtReg db = new DLayer.DebtReg();
            return db.getDebtDetails(debtId);
        }
        public static DebtModel saveDebtDetails(DebtModel model)
        {
            DLayer.DebtReg db = new DLayer.DebtReg();
            return db.saveDebtDetails(model);
        }
        public static List<DebtModel> deleteDebt(DebtModel data)
        {
            DLayer.DebtReg db = new DLayer.DebtReg();
            return db.deleteDebt(data);
        }
        public static DebtAmortizationModel getDebtInputPrediction(DebtProjectionInput model)
        {
            DLayer.DebtReg db = new DLayer.DebtReg();
            return db.getDebtInputPrediction_Individual(model);
        }

        public static List<DebtDisappearYearlyD2> getDebtEliminationSystemAutoEstimation(long profileID, decimal extraPay)
        {
            DLayer.DemoDebt db = new DLayer.DemoDebt();
            return db.getDebtEliminationSystemAutoEstimation(profileID, extraPay);
        }

        public static List<DebtDisappearList> getDisappearList(long profileID, decimal extraPay)
        {
            DLayer.DemoDebt db = new DLayer.DemoDebt();
            return db.getDisappearList(profileID, extraPay);
        }

        public static decimal getTotalExtraPayment(long profileID)
        {
            DLayer.DebtReg db = new DLayer.DebtReg();
            return db.getTotalExtraPayment(profileID);
        }
    }
}
