﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLayer
{
    public class PersonalInformation
    {

        public static PersonModel saveSpouseInfo(PersonModel obj, long usrId)
        {
            DLayer.PersonalInformation dl = new DLayer.PersonalInformation();
            return dl.savePersonalInfo(obj, (int)CustomEnum.userTypeEnum.spouse, usrId);
        }

        public static Person_EmployerInfoModel saveSpouseEmployer(Person_EmployerInfoModel obj, long userID)
        {
            DLayer.PersonalInformation dl = new DLayer.PersonalInformation();
            return dl.saveemploymentInfo(obj, (int)CustomEnum.userTypeEnum.spouse, userID);
        }

        public static List<ChildrenInfoModel> saveChildInfo(List<ChildrenInfoModel> obj, long userID)
        {
          

            DLayer.PersonalInformation dl = new DLayer.PersonalInformation();
            return dl.saveChildInfo(obj,  userID);
        }

        public static PersonalInformationModel getallinfo(long profileID)
        {
            DLayer.PersonalInformation dl = new DLayer.PersonalInformation();
            return dl.GetPersonalInfoall(profileID);
        }

        public static PersonModel getPersonalDetails(long profileID)
        {
            DLayer.PersonalInformation dl = new DLayer.PersonalInformation();
            return dl.getPersonalInfo(profileID, (int)CustomEnum.userTypeEnum.primary);
        }
        public static Person_ContactInfoModel getPrimaryContact(long profileID)
        {
            DLayer.PersonalInformation dl = new DLayer.PersonalInformation();
            return dl.getPrimaryContact(profileID);
        }
        public static Person_EmployerInfoModel getEmployerInfo(long profileID)
        {
            DLayer.PersonalInformation dl = new DLayer.PersonalInformation();
            return dl.getEmployerInfo(profileID,(int)CustomEnum.userTypeEnum.primary);
        }
        public static PersonModel getSpouseDetails(long profileID)
        {
            DLayer.PersonalInformation dl = new DLayer.PersonalInformation();
            return dl.getPersonalInfo(profileID, (int)CustomEnum.userTypeEnum.spouse);
        }
        public static Person_EmployerInfoModel getEmployerInfoSpouse(long profileID)
        {
            DLayer.PersonalInformation dl = new DLayer.PersonalInformation();
            return dl.getEmployerInfo(profileID, (int)CustomEnum.userTypeEnum.spouse);
        }

        public static List<ChildrenInfoModel> getChildDetails(long profileID)
        {
            DLayer.PersonalInformation dl = new DLayer.PersonalInformation();
            return dl.getChildinfo(profileID);
        }

        public static bool deleteChild(childDelete childDelete, long userID)
        {
            DLayer.PersonalInformation dl = new DLayer.PersonalInformation();
            return dl.deleteChild(childDelete,userID);
        }
        public static PersonModel saveprimaryPerson(CLayer.PersonModel obj, long usrId)
        {
            DLayer.PersonalInformation dl = new DLayer.PersonalInformation();
            return dl.savePersonalInfo(obj, (int)CustomEnum.userTypeEnum.primary, usrId);
        }

        public static Person_ContactInfoModel saveprimaryContact(Person_ContactInfoModel obj, long usrId)
        {
            DLayer.PersonalInformation dl = new DLayer.PersonalInformation();
            return dl.savePrimaryContactInfo(obj, usrId);
        }

        public static Person_EmployerInfoModel saveprimaryEmployer(Person_EmployerInfoModel obj, long userID)
        {
            DLayer.PersonalInformation dl = new DLayer.PersonalInformation();
            return dl.saveemploymentInfo(obj, (int)CustomEnum.userTypeEnum.primary, userID);
        }
    }
}
