﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace BLayer
{
    public class ClientDBInfo
    {
        public static Income_ExpenseModel getIncomeVsExpense(long profileID)
        {
            DLayer.ClientDashBoard cd = new DLayer.ClientDashBoard();
            return cd.getIncomeVsExpense(profileID);
        }
        
        public static RetirementModel DBRetirement(long profileID)
        {
            DLayer.RetirementGoal cl = new DLayer.RetirementGoal();
            return cl.GetRetirementDetails(profileID);
        }

        public static InvestmentTaxableModel DashBoardInvestment(long profileID)
        {
            DLayer.ClientDashBoard cl = new DLayer.ClientDashBoard();
            return cl.DashBoardInvestment(profileID);
        }

        public static Education_Model educationDashBoard(long profileID)
        {
            DLayer.ClientDashBoard cd = new DLayer.ClientDashBoard();
            return cd.educationDashBoard(profileID);
        }

        public static List<LiabilitiesModel> DashBoardLiabilities(long profileID)
        {
            DLayer.ClientDashBoard cd = new DLayer.ClientDashBoard();
            return cd.DashBoardLiabilities(profileID);
        }
      
    }
}
