﻿using CLayer;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLayer
{
  public static  class Education
    {
        public static bool deleteEducationDetails(long id, long usrID)
        {
            DLayer.EducationInformation dl = new DLayer.EducationInformation();
            return dl.deleteCurrentSavings(id, usrID);
        }

        public static List<Education_DetailsModel> GetCurrentSavings(long id)
        {
            DLayer.EducationInformation dl = new DLayer.EducationInformation();
            return dl.getCurrentSavingDetails(id);
        }

        public static EducationModel gocalculation(EducationModel em)
        {
            DLayer.EducationInformation dl = new DLayer.EducationInformation();
            return dl.educationCalculation(em, null);
        }
        public static EducationModel EducationDetails(long id)
        {
            EducationModel em = new EducationModel();
            DLayer.EducationInformation dl = new DLayer.EducationInformation();
            em= dl.GetEducationInformation(id);
            em.favColleges = BLayer.College.ProfileFavListList(id);
            return em;
        }
        public static EducationModel saveEducation(EducationModel em,long usrID)
        {
            DLayer.EducationInformation dl = new DLayer.EducationInformation();
            return dl.saveEducationInfo(em, usrID);
        }
        public static List<Education_DetailsModel> saveEducationDetails(List<Education_DetailsModel> obj, long usrID)
        {
            DLayer.EducationInformation dl = new DLayer.EducationInformation();
            return dl.saveCurrentSavings(obj, usrID);
        }
        public static bool deleteEducation(long id, long usrID)
        {
            DLayer.EducationInformation dl = new DLayer.EducationInformation();
            return dl.deleteEducation(id, usrID);
        }
    }
}
