﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLayer
{
   public class College
    {
        public static List<AllCollegeModel>  collegeSearchName(CollegeListArgs col)
        {
            DLayer.CollegeCRUD dl = new DLayer.CollegeCRUD();
            return dl.getColleges(col, CustomEnum.ColgSearchType.name);
        }
        public static List<AllCollegeModel> collegeSearchState(CollegeListArgs col)
        {
            DLayer.CollegeCRUD dl = new DLayer.CollegeCRUD();
            return dl.getColleges(col, CustomEnum.ColgSearchType.state);
        }
        public static List<AllCollegeModel> collegeSearchAll(CollegeListArgs col)
        {
            DLayer.CollegeCRUD dl = new DLayer.CollegeCRUD();
            return dl.getColleges(col, CustomEnum.ColgSearchType.empty);
        }
        public static bool  saveFaveCollege(AdvisorFavList obj,long usrId)
        {
            DLayer.CollegeCRUD dl = new DLayer.CollegeCRUD();
            return dl.saveCollegesToFav(obj, usrId);
        }
        public static List<DefaultCollege> advisorFavListList(long id)
        {
            DLayer.CollegeCRUD dl = new DLayer.CollegeCRUD();
            return dl.getFavCollegeList(id,(int)CustomEnum.userCatEnum.Advisor);
        }
        public static List<DefaultCollege> ClientFavListList(long id)
        {
            DLayer.CollegeCRUD dl = new DLayer.CollegeCRUD();
            return dl.getFavCollegeList(id, (int)CustomEnum.userCatEnum.Advisor);
        }
        public static List<DefaultCollege> ProfileFavListList(long id)
        {
            DLayer.CollegeCRUD dl = new DLayer.CollegeCRUD();
            return dl.getFavCollegeList(id, (int)CustomEnum.userCatEnum.Profile);
        }



        public static bool RemoveFavCollege(AdvisorFavList favList, long userID)
        {
            DLayer.CollegeCRUD dl = new DLayer.CollegeCRUD();
            return dl.RemoveFavCollege(favList, userID);
        }

        public static List<AllCollegeModel> FavDefaultCollegeList()
        {
            DLayer.CollegeCRUD dl = new DLayer.CollegeCRUD();
            return dl.FavDefaultCollegeList();
        }
    }
}
