﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLayer
{
    public class DropDownList
    {
        public static List<DropDownList_String> DDL_List(enumDropDownListCode Type)
        {
            return DDLReg.getDropDownList(Type);
        }
        public static List<DropDownList_Int> DDL_List_int(enumDropDownList Type)
        {
            return DDLReg.getDropDownList_Int(Type);
        }
        public static List<DropDownList_Int> DDL_List_int_withID(enumDropDownList Type,long id)
        {
            return DDLReg.getDropDownList_Int(Type, id);
        }

    }
}
