﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace BLayer
{
   public  class ActionPlanInfo
    {
        public static actionPointModel saveActionPoint(actionPointModel ap,long userID)
        {
            DLayer.ActionPlanDetails ad = new DLayer.ActionPlanDetails();
            return ad.saveActionPlan(ap,userID);
        }
        public static List<actionList> GetAllActions(long profileID)
        {
            DLayer.ActionPlanDetails ad = new DLayer.ActionPlanDetails();
            return ad.GetActions(profileID,(int)CLayer.common.CustomEnum.actionTypeEnum.All);
        }
        public static List<actionList> all_Today(long profileID)
        {
            DLayer.ActionPlanDetails ad = new DLayer.ActionPlanDetails();
            return ad.GetActions(profileID, (int)CLayer.common.CustomEnum.actionTypeEnum.AllToday);
        }
        public static List<actionList> all_ThisMonth(long profileID)
        {
            DLayer.ActionPlanDetails ad = new DLayer.ActionPlanDetails();
            return ad.GetActions(profileID, (int)CLayer.common.CustomEnum.actionTypeEnum.AllThisMonth);
        }

        public static List<actionList> pastDueActions(long profileID)
        {
            DLayer.ActionPlanDetails ad = new DLayer.ActionPlanDetails();
            return ad.GetActions(profileID, (int)CLayer.common.CustomEnum.actionTypeEnum.pastDue);
        }
        public static List<actionList> upcomingActions(long profileID)
        {
            DLayer.ActionPlanDetails ad = new DLayer.ActionPlanDetails();
            return ad.GetActions(profileID, (int)CLayer.common.CustomEnum.actionTypeEnum.upcoming);
        }
        public static List<actionList> progressingActions(long profileID)
        {
            DLayer.ActionPlanDetails ad = new DLayer.ActionPlanDetails();
            return ad.GetActions(profileID, (int)CLayer.common.CustomEnum.actionTypeEnum.Progressing);
        }
        public static List<actionList> completedActions(long profileID)
        {
            DLayer.ActionPlanDetails ad = new DLayer.ActionPlanDetails();
            return ad.GetActions(profileID, (int)CLayer.common.CustomEnum.actionTypeEnum.Completed);
        }
        public static List<preDefinedActionModel> getPredefinedActions()
        {
            DLayer.ActionPlanDetails ad = new DLayer.ActionPlanDetails();
            return ad.getPredefinedActions();
        }
        public static List<DropDownList_Int> ddlAssignedTO(long profileID)
        {
            DLayer.ActionPlanDetails ad = new DLayer.ActionPlanDetails();
            return ad.getDDL_AssignedTo(profileID);
        }
        public static bool markCompleted(long actionID,long userID)
        {
            DLayer.ActionPlanDetails ad = new DLayer.ActionPlanDetails();
            return ad.changeStatus(actionID,userID,Convert.ToInt32(CustomEnum.actionStatus.Completed));
        }
        public static bool markInProgress(long actionID, long userID)
        {
            DLayer.ActionPlanDetails ad = new DLayer.ActionPlanDetails();
            return ad.changeStatus(actionID, userID, Convert.ToInt32(CustomEnum.actionStatus.Progressing));
        }
        public static actionPointModel getOnReshedule(long actionID)
        {
            DLayer.ActionPlanDetails ad = new DLayer.ActionPlanDetails();
            return ad.getOnReshedule(actionID);
        }

    }
}
