﻿using CLayer;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLayer
{
    public class ClientReg
    {
        public static void saveClientProfile(long clientID, string imagePath)
        {
            DLayer.ClientReg cl = new DLayer.ClientReg();
            cl.saveClientImage(clientID, imagePath);
        }
            public static ClientModel saveClient(ClientModel data, long UserID)
        {
            DLayer.ClientReg cl = new DLayer.ClientReg();
            return cl.saveClient(data, UserID);
        }
        public static ClientCollection1 saveClientFull(ClientCollection1 data)
        {
            DLayer.ClientReg cl = new DLayer.ClientReg();
            return cl.saveClientFull(data);
        }
        public static ClientCollection getClientDetails(long clientID, bool isClientOnly)
        {
            DLayer.ClientReg cl = new DLayer.ClientReg();
            return cl.getClientDetails(clientID, isClientOnly);
        }
        public static ClientListPaginated getClientList(ClientListArgModel data)
        {
            DLayer.ClientReg cl = new DLayer.ClientReg();
            return cl.getClientList(data);
        }
        public static bool deleteClient(long clientID, long UserID)
        {
            DLayer.ClientReg cl = new DLayer.ClientReg();
            return cl.deleteClient(clientID, UserID);
        }
    }
}
