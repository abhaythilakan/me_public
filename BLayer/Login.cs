﻿using CLayer;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLayer
{
    public class Login
    {
        public static bool isUserNameExists(string userName, long userId)
        {
            DLayer.Login dl = new DLayer.Login();
            return dl.isUserNameExists(userName, userId);
        }
        public static bool isEmailExists(string email, long userId)
        {
            DLayer.Login dl = new DLayer.Login();
            return dl.isEmailExists(email, userId);
        }

        public static UserModels isValidUser(string username, string password)//string username = "danharriman", string password = "edge2014"
        {
            DLayer.Login dl = new DLayer.Login();
            return dl.isValidUser(username, password);
        }

        public static bool saveActiveToken(string username, string password, string activeToken)
        {
            DLayer.Login dl = new DLayer.Login();
            return dl.saveActiveToken(username, password, activeToken);
        }

        public static bool isActiveToken(long userID, string activeToken)
        {
            DLayer.Login dl = new DLayer.Login();
            return dl.isActiveToken(userID, activeToken);
        }

        public static bool logOut(long userID)
        {
            DLayer.Login dl = new DLayer.Login();
            return dl.logOut(userID);
        }

            //public static LoginAdvisor isValidUser_AdminAdvisor(string username, string password)//string username = "danharriman", string password = "edge2014"
            //{
            //    DLayer.Login dl = new DLayer.Login();
            //    return dl.isValidUser_AdminAdvisor(username, password);
            //}
            //public static LoginClient isValidUser_Client(string username, string password)//string username = "test", string password = "client"
            //{
            //    DLayer.Login dl = new DLayer.Login();
            //    return dl.isValidUser_Client(username, password);
            //}

            public static User hasValidAccount(string email, string hashCode)
        {
            DLayer.Login dl = new DLayer.Login();
            return dl.hasValidAccount(email, hashCode);
        }

        public static long validForgorPasswordToken(string email, string hashCode)
        {
            DLayer.Login dl = new DLayer.Login();
            return dl.validForgorPasswordToken(email, hashCode);
        }

        public static bool resetPassword(ForgotPasswodModel fp)
        {
            DLayer.Login dl = new DLayer.Login();
            return dl.resetPassword(fp);
        }

        public static bool setLoginedUserToken(long ID, CLayer.common.CustomEnum.userRoles roles)
        {
            DLayer.Login dl = new DLayer.Login();
            return dl.setLoginedUserToken(ID, roles);
        }
    }
}
