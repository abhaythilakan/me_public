﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLayer
{
    public class Asset
    {
        public static AssetModel getNonRetAssetDetails(long profileID, int assetType)
        {
            DLayer.AssetDetails dl = new DLayer.AssetDetails();
            return dl.getNonRetAssetDetails(profileID, assetType);

        }
        public static List<Asset_RetirementModel> getRetirementAsset(long profileID)
        {
            DLayer.AssetDetails dl = new DLayer.AssetDetails();
            return dl.getRetirementAsset(profileID);
        }
        public static List<Asset_RetirementDetailsModel> getRetirementAssetDetails(long id,int assetType)
        {
            DLayer.AssetDetails dl = new DLayer.AssetDetails();
            return dl.getRetirementAssetDetails(id,assetType);
        }
        public static Asset_EmergencyFundModel getEmergencyFundandhouseholdIncome(long profileID)
        {
            DLayer.AssetDetails dl = new DLayer.AssetDetails();
            return dl.getEmergencyFundandhouseholdIncome(profileID);
        }

        public static NonRetirementAssetsModel saveNonRetirementAsset(NonRetirementAssetsModel inObj, int type, long usrID)
        {
            DLayer.AssetDetails dl = new DLayer.AssetDetails();
            return dl.saveNonRetirementAsset( inObj, type, usrID);
        }
        public static Asset_RetirementDetailsModel saveAsset_RetirementDetails(Asset_RetirementDetailsModel inObj, int type, long usrID)
        {
            DLayer.AssetDetails dl = new DLayer.AssetDetails();
            return dl.saveAsset_RetirementDetails(inObj, type, usrID);
        }
        public static Asset_EmergencyFundModel saveAsset_EmergencyFundDeatails(Asset_EmergencyFundModel inObj, long usrID)
        {
            DLayer.AssetDetails dl = new DLayer.AssetDetails();
            return dl.saveAsset_EmergencyFundDeatails(inObj, usrID);
        }


        public static bool deleteNonRetDetails(DeleteAssetDetailsModel inObj, long userID)
        {
            DLayer.AssetDetails dl = new DLayer.AssetDetails();
            return dl.deleteNonRetDetails(inObj, userID);
        }
        public static bool deleteRetDetails(DeleteAssetDetailsModel inObj, long userID)
        {
            DLayer.AssetDetails dl = new DLayer.AssetDetails();
            return dl.deleteRetDetails(inObj, userID);
        }

    }
}
