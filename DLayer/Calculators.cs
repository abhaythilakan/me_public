﻿using CLayer;
using System;
using CLayer.common;
using CLayer.DBModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Globalization;
using System.Data.SqlClient;
using DLayer.Common;

namespace DLayer
{
    public class Calculators
    {
        public EarlyInvestorModel getEarlyInvestor(EarlyInvestorModel inObj)
        {
            int a = 0;
            try
            {
                if (inObj.currentAge == null)
                {
                    a = 1;
                }

                int? currentAge = inObj.currentAge ?? 1;
                int? endYear = inObj.endYear;
                decimal? anualInvestment = inObj.anualInvestment;
                decimal? rateOfreturn = inObj.rateOfreturn;
                int earlySpender = inObj.earlySpender ?? 10;

                decimal? anualInvestment1 = 0;
                decimal? anualInvestment2 = 0;
                decimal? Amount = anualInvestment;
                decimal? total = 0;
                decimal? earlyInvestment = 0;
                decimal? Amount2 = anualInvestment;
                decimal? accumaltedAmount = 0;
                decimal? totalAnualInvestment1 = 0;
                decimal? totalAnualInvestment2 = 0;
                inObj.Details = new List<EarlyInvestorModelDetails>();
                for (int i = 0; i < endYear; i++)
                {
                    inObj.Details.Add(new EarlyInvestorModelDetails() { age = currentAge.Value });
                    if (i < 10)
                    {
                        anualInvestment1 = anualInvestment;

                        if (i == 0)
                        {
                            Amount = anualInvestment + (anualInvestment) * (rateOfreturn / 100);
                            earlyInvestment = Amount;
                            //Console.WriteLine((i + 1) + "   " + currentAge + "    " + anualInvestment1 + "   " + Math.Round(Convert.ToDecimal(Amount)) + "   " + anualInvestment2 + "   " + accumaltedAmount + "   " + Math.Round(earlyInvestment));
                            inObj.Details[i].year = i + 1;
                            inObj.Details[i].anualInvestmentA = anualInvestment1.Value;
                            inObj.Details[i].accumalationA = Math.Round(Convert.ToDecimal(Amount));
                            inObj.Details[i].anualInvestmentB = Math.Round(anualInvestment2.Value);
                            inObj.Details[i].accumalationB = Math.Round(accumaltedAmount.Value);
                            inObj.Details[i].earlyInvestor = Math.Round(Convert.ToDecimal(earlyInvestment));

                        }
                        else
                        {
                            total = Amount + anualInvestment1;
                            Amount = total + (total) * (rateOfreturn / 100);
                            earlyInvestment = Amount;
                            // Console.WriteLine((i + 1) + "   " + currentAge + "    " + anualInvestment1 + "    " + Math.Round(Convert.ToDecimal(Amount)) + "   " + anualInvestment2 + "   " + accumaltedAmount + "   " + Math.Round(earlyInvestment));
                            inObj.Details[i].year = i + 1;
                            inObj.Details[i].anualInvestmentA = anualInvestment1.Value;
                            inObj.Details[i].accumalationA = Math.Round(Convert.ToDecimal(Amount));
                            inObj.Details[i].anualInvestmentB = Math.Round(anualInvestment2.Value);
                            inObj.Details[i].accumalationB = Math.Round(accumaltedAmount.Value);
                            inObj.Details[i].earlyInvestor = Math.Round(Convert.ToDecimal(earlyInvestment));
                        }
                    }
                    else
                    {
                        anualInvestment2 = anualInvestment;
                        anualInvestment1 = 0;
                        total = Amount + anualInvestment1;
                        Amount = total + (total) * (rateOfreturn / 100);

                        accumaltedAmount = (accumaltedAmount + anualInvestment2) + ((accumaltedAmount + anualInvestment2) * (rateOfreturn / 100));
                        earlyInvestment = Amount - accumaltedAmount;

                        inObj.Details[i].year = i + 1;
                        inObj.Details[i].anualInvestmentA = anualInvestment1.Value;
                        inObj.Details[i].accumalationA = Math.Round(Convert.ToDecimal(Amount));
                        inObj.Details[i].anualInvestmentB = Math.Round(anualInvestment2.Value);
                        inObj.Details[i].accumalationB = Math.Round(accumaltedAmount.Value);
                        inObj.Details[i].earlyInvestor = Math.Round(Convert.ToDecimal(earlyInvestment));

                    }

                    totalAnualInvestment1 = totalAnualInvestment1 + anualInvestment1;
                    totalAnualInvestment2 = totalAnualInvestment2 + anualInvestment2;
                    currentAge++;


                    if (a == 1)
                    {
                        inObj.Details[i].age = null;
                    }
                }

                inObj.totalAnualInvestmentA = totalAnualInvestment1;
                inObj.totalAnualInvestmentB = totalAnualInvestment2;
            }
            catch (Exception ex)
            {
                throw;
            }
            return inObj;
        }

        public TaxableVsTaxDefferedModel TaxableVsTaxDeffered(TaxableVsTaxDefferedModel inobj)
        {
            try
            {
                int cAge = 0;
                decimal taxRate = inobj.taxRate.Value;
                decimal defTaxRate = inobj.defTaxRate.Value;
                decimal taxFreeRate = inobj.taxFreeRate.Value;
                decimal fTaxRate1 = inobj.fTaxRateA.Value;
                decimal sTaxRate1 = inobj.sTaxRateA ?? 0;
                decimal olTaxRate1 = inobj.olTaxRateA ?? 0;
                decimal fTaxRate2 = inobj.fTaxRateB.Value;
                decimal sTaxRate2 = inobj.sTaxRateB ?? 0;
                decimal olTaxRate2 = inobj.olTaxRateB ?? 0;
                decimal amount = inobj.amount.Value;
                decimal annualInvest = inobj.annualInvest ?? 0;
                int years = inobj.years.Value;

                decimal futureVal1 = 0;
                decimal futureVal2 = 0;
                decimal futureVal3 = 0;
                decimal taxReturnRate = 0;
                decimal defTaxReturnRate = 0;


                decimal gain = 0;
                inobj.Details = new List<TaxableVsTaxDefferedDetailsModel>();
                for (int i = 0; i <= years - 1; i++)
                {
                    inobj.Details.Add(new TaxableVsTaxDefferedDetailsModel() { year = i + 1 });
                    if ((i == 0))
                    {
                        futureVal1 = amount;
                        futureVal2 = futureVal1;
                        futureVal3 = futureVal1;
                    }
                    gain = futureVal1 * (taxRate / 100);
                    futureVal1 = ((futureVal1 * (1
                                + ((taxRate) / 100)))
                                + ((annualInvest)
                                - (gain
                                * ((fTaxRate1)
                                + ((sTaxRate1) + (olTaxRate1)))
                                / 100)));
                    futureVal2 = ((futureVal2 * (1
                                + ((defTaxRate) / 100)))
                                + (annualInvest));
                    futureVal3 = ((futureVal3 * (1
                                + ((taxFreeRate) / 100)))
                                + (annualInvest));
                    // Console.WriteLine(i + 1 + "   " + Math.Round(futureVal1) + "  " + Math.Round(futureVal2) + "  " + Math.Round(futureVal3));
                    inobj.Details[i].FuturevalueA = Math.Round(futureVal1);
                    inobj.Details[i].FuturevalueB = Math.Round(futureVal2);
                    inobj.Details[i].FuturevalueC = Math.Round(futureVal3);
                }
                taxReturnRate = ((taxRate)
                           - (((taxRate)
                           * ((fTaxRate1) / 100))
                           + (((taxRate)
                           * ((sTaxRate1) / 100))
                           + ((taxRate)
                           * ((olTaxRate1) / 100)))));

                defTaxReturnRate = ((defTaxRate)
                            - (((defTaxRate)
                            * ((fTaxRate2) / 100))
                            + (((defTaxRate)
                            * ((sTaxRate2) / 100))
                            + ((defTaxRate)
                            * ((olTaxRate2) / 100)))));
                decimal TotalTaxable = (futureVal1);
                decimal TotalTaxDeferred = (futureVal2);
                decimal TotalTaxFree = (futureVal3);

                decimal TotTXAfter = (futureVal1);
                decimal TotDFAfter = ((futureVal2
                                - ((futureVal2
                                - ((amount)
                                + ((annualInvest) * (years))))
                                * (((fTaxRate2)
                                + ((sTaxRate2) + (olTaxRate2)))
                                / 100))));
                decimal TotTFAfter = (futureVal3);


                inobj.TotalTaxDeferred = Math.Round(TotalTaxDeferred);
                inobj.TotalTaxable = Math.Round(TotalTaxable);
                inobj.TotalTaxFree = Math.Round(TotalTaxFree);
                inobj.TotTxAfter = Math.Round(TotTXAfter);
                inobj.TotTfTxAfter = Math.Round(TotTFAfter);
                inobj.TotDfTxAfter = Math.Round(TotDFAfter);
                inobj.taxReturnRate = Math.Round(taxReturnRate, 2);
                inobj.defTaxReturnRate = Math.Round(defTaxReturnRate, 2);

            }
            catch (Exception ex)
            {
                throw;
            }
            return inobj;
        }

        public DIMEModel DIME(DIMEModel inobj)
        {
            try
            {
                inobj.Children = inobj.Children ?? 0;
                inobj.deathExpenses = inobj.deathExpenses ?? 0;
                inobj.debtAmount = inobj.debtAmount ?? 0;
                inobj.educationNeeds = inobj.educationNeeds ?? 0;
                inobj.mortgageBalance = inobj.mortgageBalance ?? 0;


                inobj.isPrimaryIncluded = inobj.isPrimaryIncluded ?? true;
                inobj.isSpouseIncluded = inobj.isSpouseIncluded ?? false;


                inobj.isDeathExpensesIncluded = inobj.isDeathExpensesIncluded ?? true;
                inobj.isDebtAmountIncluded = inobj.isDebtAmountIncluded ?? true;
                inobj.isEducationNeedsIncluded = inobj.isEducationNeedsIncluded ?? true;
                inobj.ismortgageBalanceIncluded = inobj.ismortgageBalanceIncluded ?? true;


                inobj.primaryProtectionNeeds = new DIMEDetailsModel();
                inobj.spouseProtectionNeeds = new DIMEDetailsModel();

                inobj.primaryProtectionNeeds.IncomeNeed = 0;
                inobj.spouseProtectionNeeds.IncomeNeed = 0;

                inobj.pTotalNeed = 0;
                inobj.sTotalNeed = 0;

                decimal? inflation = (inobj.inflationRate.Value / 100);
                decimal interestRate = (inobj.returnRate.Value / 100);

                if ((inobj.pName == "" || inobj.pName == null))
                {
                    inobj.primaryProtectionNeeds.name = "Primary";
                }
                else
                {
                    inobj.primaryProtectionNeeds.name = inobj.pName;
                }
                inobj.primaryProtectionNeeds.IncomeNeed = getIncomeNeed(inflation, interestRate, inobj.pMonthlyNeed, (inobj.pYears - 1));
                if (inobj.isPrimaryIncluded == false || inobj.isPrimaryIncluded == null)
                {
                    inobj.pTotalNeed = 0;
                }
                else
                {
                    inobj.pTotalNeed = inobj.primaryProtectionNeeds.IncomeNeed;
                }
                if ((inobj.isDebtAmountIncluded == true && inobj.isDeathExpensesIncluded == true))
                {
                    inobj.primaryProtectionNeeds.DebtDeath = inobj.deathExpenses + inobj.debtAmount;
                    inobj.pTotalNeed = inobj.pTotalNeed + inobj.primaryProtectionNeeds.DebtDeath;
                    inobj.sTotalNeed = inobj.sTotalNeed + inobj.primaryProtectionNeeds.DebtDeath;
                }
                else if (inobj.isDebtAmountIncluded == true)
                {
                    inobj.primaryProtectionNeeds.DebtDeath = inobj.debtAmount;
                    inobj.pTotalNeed = inobj.pTotalNeed + inobj.primaryProtectionNeeds.DebtDeath;
                    inobj.sTotalNeed = inobj.sTotalNeed + inobj.primaryProtectionNeeds.DebtDeath;
                }
                else if (inobj.isDeathExpensesIncluded == true)
                {
                    inobj.primaryProtectionNeeds.DebtDeath = inobj.deathExpenses;
                    inobj.pTotalNeed = inobj.pTotalNeed + inobj.primaryProtectionNeeds.DebtDeath;
                    inobj.sTotalNeed = inobj.sTotalNeed + inobj.primaryProtectionNeeds.DebtDeath;
                }
                else
                {
                    inobj.primaryProtectionNeeds.DebtDeath = inobj.deathExpenses + inobj.debtAmount;
                }

                inobj.spouseProtectionNeeds.DebtDeath = inobj.primaryProtectionNeeds.DebtDeath;
                if ((inobj.sName == "" || inobj.sName == null))
                {
                    inobj.spouseProtectionNeeds.name = "Spouse";
                }
                else
                {
                    inobj.spouseProtectionNeeds.name = inobj.sName;
                }
                inobj.spouseProtectionNeeds.IncomeNeed = getIncomeNeed(inflation, interestRate, inobj.sMonthlyNeed ?? 0, (inobj.sYears ?? 0 - 1));
                if (inobj.isSpouseIncluded == false || inobj.isSpouseIncluded == null)
                {

                }
                else
                {
                    inobj.sTotalNeed = inobj.sTotalNeed + inobj.spouseProtectionNeeds.IncomeNeed;
                }

                inobj.primaryProtectionNeeds.Mortgage = inobj.mortgageBalance;
                inobj.spouseProtectionNeeds.Mortgage = inobj.mortgageBalance;
                if (inobj.ismortgageBalanceIncluded == false)
                {

                }
                else
                {
                    inobj.pTotalNeed = inobj.pTotalNeed + inobj.primaryProtectionNeeds.Mortgage;
                    inobj.sTotalNeed = inobj.sTotalNeed + inobj.spouseProtectionNeeds.Mortgage;
                }

                inobj.primaryProtectionNeeds.Education = inobj.educationNeeds * inobj.Children;
                inobj.spouseProtectionNeeds.Education = inobj.educationNeeds * inobj.Children;
                if (inobj.isEducationNeedsIncluded == false)
                {

                }
                else
                {
                    inobj.pTotalNeed = inobj.pTotalNeed + inobj.primaryProtectionNeeds.Education;
                    inobj.sTotalNeed = inobj.sTotalNeed + inobj.spouseProtectionNeeds.Education;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            inobj.pTotalNeed = Math.Round(inobj.pTotalNeed.Value, 2);
            inobj.sTotalNeed = Math.Round(inobj.sTotalNeed.Value, 2);
            inobj.primaryProtectionNeeds.Education = Math.Round(inobj.primaryProtectionNeeds.Education.Value, 2);
            inobj.spouseProtectionNeeds.Education = Math.Round(inobj.spouseProtectionNeeds.Education.Value, 2);


            return inobj;
        }
        public decimal? getIncomeNeed(decimal? inflation, decimal interestRate, decimal? monthlyNeed, int? years)
        {
            decimal? incomeNeed = 0;
            decimal? currentNeed = 0;
            decimal? currentInterest = 0;
            decimal? currentPrincipal = 0;
            if ((years == -1))
            {
                // 'unlimited years
                if ((interestRate <= inflation))
                {
                    incomeNeed = 0;
                }
                else
                {
                    incomeNeed = ((monthlyNeed * 12)
                                / (interestRate - inflation));
                }

            }
            else
            {
                for (int i = years.Value; i >= 0; i--)
                {
                    currentNeed = (monthlyNeed * 12) * Convert.ToDecimal(Math.Pow((1 + Convert.ToDouble(inflation)), i));
                    currentInterest = ((incomeNeed / (1 - interestRate)) - incomeNeed);
                    currentPrincipal = (currentNeed - currentInterest);
                    incomeNeed = (incomeNeed + currentPrincipal);
                    if ((incomeNeed < 0))
                    {
                        incomeNeed = 0;
                        break;
                    }
                }

            }

            return incomeNeed;
        }
        public CompoundInterestModel compoundInterestAtGo(CompoundInterestModel modelObj)
        {
            try
            {
                int? currentAge = modelObj.age;
                decimal? startInvestment = modelObj.initialStartInvestment;
                decimal? monthlyInvestment = modelObj.monthlyInvestment;
                decimal? interestRate1 = modelObj.interestRate1;
                decimal? interestRate2 = modelObj.interestRate2;
                decimal? interestRate3 = modelObj.interestRate3;
                int? years = modelObj.no_of_years;

                decimal? anualInvestment = 0;
                decimal? totalInvestment = 0;
                decimal? futurevalueA = 0;
                decimal? futurevalueB = 0;
                decimal? futurevalueC = 0;
                decimal? totalAnualInvestment = 0;
                decimal? total_TotalInvestment = 0;
                decimal? TotalfuturevalueA = 0;
                decimal? TotalfuturevalueB = 0;
                decimal? TotalfuturevalueC = 0;
                modelObj.cInterestDetails = new List<CompoundInterestDetailsModel>();
                for (int i = 0; i <= years; i++)
                {
                    if (currentAge > 0)
                    {
                        modelObj.cInterestDetails.Add(new CompoundInterestDetailsModel { years = i, age = currentAge + i });
                    }
                    else
                    {
                        modelObj.cInterestDetails.Add(new CompoundInterestDetailsModel { years = i });
                    }
                    if (i == 0)
                    {
                        futurevalueA = startInvestment;
                        futurevalueB = startInvestment;
                        futurevalueC = startInvestment;
                        anualInvestment = startInvestment;
                        totalInvestment = startInvestment;
                        totalAnualInvestment = totalAnualInvestment + anualInvestment;
                        // total_TotalInvestment = total_TotalInvestment + totalInvestment;
                    }
                    else
                    {
                        for (int j = 1; j <= 12; j++)
                        {
                            futurevalueA = Math.Round(Convert.ToDecimal((futurevalueA * (1 + (interestRate1 / 1200)))), 2) + (monthlyInvestment);
                            futurevalueB = Math.Round(Convert.ToDecimal((futurevalueB * (1 + (interestRate2 / 1200)))), 2) + (monthlyInvestment);
                            futurevalueC = Math.Round(Convert.ToDecimal((futurevalueC * (1 + (interestRate3 / 1200)))), 2) + (monthlyInvestment);
                        }
                        anualInvestment = monthlyInvestment * 12;
                        totalInvestment = totalInvestment + anualInvestment;
                        totalAnualInvestment = totalAnualInvestment + anualInvestment;
                        //total_TotalInvestment = total_TotalInvestment + totalInvestment;
                    }
                    modelObj.cInterestDetails[i].anualInvestment = (anualInvestment) ?? 0;
                    modelObj.cInterestDetails[i].totalInvestment = (totalInvestment) ?? 0;
                    modelObj.cInterestDetails[i].futurevalueA = (futurevalueA) ?? 0;
                    modelObj.cInterestDetails[i].futurevalueB = (futurevalueB) ?? 0;
                    modelObj.cInterestDetails[i].futurevalueC = (futurevalueC) ?? 0;
                }
                modelObj.total_anualInvestment = (totalAnualInvestment) ?? 0;
                modelObj.total_totalInvestment = (totalInvestment) ?? 0;
                modelObj.total_futurevalueA = (futurevalueA) ?? 0;
                modelObj.total_futurevalueB = (futurevalueB) ?? 0;
                modelObj.total_futurevalueC = (futurevalueC) ?? 0;

            }
            catch (Exception ex)
            {
                throw;
            }
            return modelObj;
        }
        public GoalsAndDreamsModel GoalsAndDreamsAtGo(GoalsAndDreamsModel modelObj)
        {
            try
            {
                decimal? inflationRate = modelObj.inflationRate;
                decimal? returnRate1 = modelObj.returnRate1;
                decimal? returnRate2 = modelObj.returnRate2;
                decimal? returnRate3 = modelObj.returnRate3;
                decimal? returnRate = 0;
                decimal? CurrentCostToday = 0;
                decimal? currentSavingsToday = 0;

                decimal? GoalCostFuture = 0;
                decimal? CurrentSavingsFuture = 0;
                decimal? SavingsGap = 0;
                int? MonthsToSave = 0;
                decimal? MonthlySavingsNeeded = 0;

                decimal? totalCurrentCostToday = 0;
                decimal? totalcurrentSavingsToday = 0;
                DateTime TGoalDate, dt, dt1;
                decimal? totalGoalCostFuture = 0;
                decimal? totalCurrentSavingsFuture = 0;
                decimal? totalSavingsGap = 0;
                decimal? totalMonthsToSave = 0;
                decimal? totalMonthlySavingsNeeded = 0;
                int years = 0, timeFrame = 0, maxyear = 0, j = 0;
                modelObj.goalsOutput = new List<GoalsOutputModel>();

                for (int i = 0; i < modelObj.goalsInput.Count; i++)
                {
                    modelObj.goalsOutput.Add(new GoalsOutputModel() { goalName = modelObj.goalsInput[i].savingName });
                    years = (modelObj.goalsInput[i].years) ?? 0;
                    timeFrame = years * 12;
                    CurrentCostToday = modelObj.goalsInput[i].currentCost;
                    currentSavingsToday = modelObj.goalsInput[i].currentSavings;
                    if (years > maxyear)
                    {
                        maxyear = years;
                        j = i;
                    }
                    if (timeFrame <= 60)
                    {
                        returnRate = returnRate1 / 1200;
                    }

                    else if (timeFrame >= 60 && timeFrame <= 120)
                    {
                        returnRate = returnRate2 / 1200;
                    }
                    else if (timeFrame > 120)
                    {
                        returnRate = returnRate3 / 1200;
                    }
                    dt = System.DateTime.Now.Date;
                    dt1 = dt.AddYears(years);
                    GoalCostFuture = CurrentCostToday * Convert.ToDecimal(Math.Pow(Convert.ToDouble(1 + (inflationRate / 1200)), Convert.ToDouble(timeFrame)));
                    CurrentSavingsFuture = currentSavingsToday * Convert.ToDecimal(Math.Pow(Convert.ToDouble(1 + returnRate), Convert.ToDouble(timeFrame)));
                    SavingsGap = GoalCostFuture - CurrentSavingsFuture;
                    MonthsToSave = timeFrame;
                    MonthlySavingsNeeded = (SavingsGap * returnRate) / Convert.ToDecimal((Math.Pow(Convert.ToDouble(1 + returnRate), Convert.ToDouble(timeFrame)) - 1));

                    totalCurrentCostToday = totalCurrentCostToday + CurrentCostToday;
                    totalcurrentSavingsToday = totalcurrentSavingsToday + currentSavingsToday;
                    totalGoalCostFuture = totalGoalCostFuture + GoalCostFuture;
                    totalCurrentSavingsFuture = totalCurrentSavingsFuture + CurrentSavingsFuture;
                    totalSavingsGap = totalSavingsGap + SavingsGap;
                    totalMonthlySavingsNeeded = totalMonthlySavingsNeeded + MonthlySavingsNeeded;

                    //modelObj.goalsOutput[i].goalName = modelObj.goalsInput[i].savingName;
                    modelObj.goalsOutput[i].currentCost = Math.Round(Convert.ToDecimal(CurrentCostToday), 2);
                    modelObj.goalsOutput[i].currentSavings = Math.Round(Convert.ToDecimal(currentSavingsToday), 2);
                    modelObj.goalsOutput[i].goalDate = (dt1.Month + "/" + dt1.Day + "/" + dt1.Year);
                    modelObj.goalsOutput[i].goalCostFuture = Math.Round(Convert.ToDecimal(GoalCostFuture), 2);
                    modelObj.goalsOutput[i].currentSavingsFuture = Math.Round(Convert.ToDecimal(CurrentSavingsFuture), 2);
                    modelObj.goalsOutput[i].savingsGap = Math.Round(Convert.ToDecimal(SavingsGap), 2);
                    modelObj.goalsOutput[i].monthsToSave = timeFrame;
                    modelObj.goalsOutput[i].monthlySavingsNeeded = Math.Round(Convert.ToDecimal(MonthlySavingsNeeded), 2);

                }
                modelObj.totalCurrentCostToday = Math.Round(Convert.ToDecimal(totalCurrentCostToday), 2);
                modelObj.totalcurrentSavingsToday = Math.Round(Convert.ToDecimal(totalcurrentSavingsToday), 2);
                modelObj.totalGoalCostFuture = Math.Round(Convert.ToDecimal(totalGoalCostFuture), 2);
                modelObj.totalCurrentSavingsFuture = Math.Round(Convert.ToDecimal(totalCurrentSavingsFuture), 2);
                modelObj.totalSavingsGap = Math.Round(Convert.ToDecimal(totalSavingsGap), 2);
                modelObj.totalMonthlySavingsNeeded = Math.Round(Convert.ToDecimal(totalMonthlySavingsNeeded), 2);
                modelObj.maxGoalDate = modelObj.goalsOutput[j].goalDate;
                modelObj.totalMonthsToSave = ((modelObj.goalsInput[j].years) * 12) ?? 0;
            }
            catch (Exception ex)
            {
                throw;
            }
            return modelObj;
        }

        public CreditCardModel creditCardCalculators(CreditCardModel inobj)
        {
            try
            {
                double loanAmount = inobj.loanAmount.Value;
                double intRate = inobj.intRate.Value;
                double cardMinPay = inobj.cardMinPay.Value;
                double percentPay = inobj.percentPay.Value;
                int monName = inobj.monName.Value;
                int years = inobj.years.Value;
                string cName = inobj.cName;
                DateTime paidDate;
                double rate = 0;
                double interest = 0;
                double tempPayment = 0;
                int totPayments = 1;
                double newBalance = 0;
                double minPay = 0;
                double totalPayment = 0;
                double interestPaid = 0;
                double currentBalance = 0;
                int noOfPayments = 0;
                string payoff;
                string payOffYears;

                newBalance = loanAmount;
                minPay = (newBalance * (percentPay) / 100);
                if (((monName == 2) && (System.DateTime.Now.Day > 29)))
                {
                    paidDate = DateTime.Parse((monName <= 9 ? "0" + monName.ToString() : monName.ToString() + ("/" + ((System.DateTime.Now.Day - 4) + ("/" + years)))));
                }
                else
                {
                    paidDate = DateTime.Parse((monName <= 9 ? "0" + monName.ToString() : monName.ToString() + ("/" + (System.DateTime.Now.Day + ("/" + years)))));
                }
                while (newBalance > 0)
                {
                    rate = intRate / 100;

                    if (paidDate.Month == System.DateTime.Now.Month && paidDate.Year == System.DateTime.Now.Year)
                    {
                        currentBalance = newBalance;
                    }

                    interest = (newBalance * (rate / 12));
                    interestPaid = interestPaid + interest;

                    tempPayment = newBalance * (percentPay / 100);
                    if (tempPayment <= cardMinPay)
                    {
                        tempPayment = cardMinPay;
                    }


                    newBalance = (newBalance + interest);

                    newBalance = (newBalance - tempPayment);

                    if ((tempPayment >= newBalance))
                    {
                        tempPayment = newBalance;
                    }

                    if ((newBalance <= 0))
                    {
                        break;
                    }
                    paidDate = paidDate.AddMonths(1);
                    totPayments++;
                }
                noOfPayments = totPayments;
                totalPayment = interestPaid + loanAmount;
                payoff = paidDate.ToString("MMMM,yyyy");
                if (((totPayments % 12) == 0))
                {
                    payOffYears = (((totPayments / 12)) + " years");

                }
                else
                {
                    payOffYears = (totPayments / 12) + (" years, " + ((totPayments % 12) + " months"));

                }
                inobj.totPayments = Convert.ToString(totPayments);
                inobj.payOffYears = payOffYears;
                inobj.payoff = payoff;
                inobj.interestPaid = Convert.ToString(Math.Round(interestPaid, 2));
                inobj.currentBalance = Convert.ToString(Math.Round(currentBalance, 2));
            }
            catch (Exception ex)
            {
                throw;
            }

            return inobj;
        }

        public CreditCardModel creditCardAmmortCal(CreditCardModel inobj)
        {
            try
            {
                double loanAmount = inobj.loanAmount ?? 0;
                double rate = 0;
                int monName = inobj.monName ?? 1;
                int years = inobj.years ?? 2018;
                DateTime paidDate;
                double totPrincipal = 0;
                double balance = 0;
                double newBalance = 0;
                double tempPayment = 0;
                double interest;
                double totalInterest = 0;
                double totPayment = 0;
                double totAdditional = 0;
                double currentBalance = 0;
                double totalAnnualPrincipal = 0;
                double totalAnnualPayment = 0;
                double totalAnnualInterest = 0;
                double totAddPayment = 0;

                int i = 0;
                int j = 0;
                double adnlPay = 0;
                DateTime prevDate;
                newBalance = loanAmount;
                rate = (inobj.intRate ?? 0) / 100;
                if (((monName == 2) && (System.DateTime.Now.Day > 29)))
                {
                    paidDate = DateTime.ParseExact(((monName <= 9 ? "0" + monName.ToString() : monName.ToString()) + ("/" + ((System.DateTime.Now.Day - 4) + ("/" + years)))), "d", CultureInfo.InvariantCulture);
                }
                else
                {
                    paidDate = DateTime.ParseExact(((monName <= 9 ? "0" + monName.ToString() : monName.ToString()) + ("/" + (System.DateTime.Now.Day + ("/" + years)))), "d", CultureInfo.InvariantCulture);
                }
                inobj.Amortization = new CreditRevolveAmortization();
                inobj.Amortization.PaymentSummary = new List<CreditRevolveModel>();

                if (inobj.IsYearly == false)
                {
                    while (newBalance > 0)
                    {
                        inobj.Amortization.PaymentSummary.Add(new CreditRevolveModel { PMT = i + 1 });
                        inobj.Amortization.PaymentSummary[i].date = paidDate.ToString("MMM, yyyy");
                        interest = (newBalance * rate) / 12;
                        inobj.Amortization.PaymentSummary[i].interest = Math.Round(interest, 2);
                        totalInterest += interest;
                        inobj.Amortization.PaymentSummary[i].interestPaid = Math.Round(totalInterest, 2);
                        if ((paidDate.Month == System.DateTime.Now.Month) && (paidDate.Year == System.DateTime.Now.Year))
                        {
                            currentBalance = balance;
                        }
                        if (!inobj.fixedPayment == true)
                        {
                            tempPayment = newBalance * (inobj.percentPay ?? 0) / 100;
                            if (tempPayment < inobj.cardMinPay)
                            {
                                tempPayment = inobj.cardMinPay ?? 0;
                            }
                        }
                        else
                        {
                            tempPayment = (double)loanAmount * (double)inobj.percentPay / 100;
                        }

                        if (inobj.IsAllPayment == true && inobj.IsOneTime == true)
                        {
                            if ((i == 0))
                            {
                                adnlPay = Convert.ToDouble(inobj.OneTimeValue + inobj.AllPaymentValue);
                            }
                            else
                            {
                                adnlPay = Convert.ToDouble(inobj.AllPaymentValue);
                            }
                        }
                        else if (inobj.IsOneTime == true)
                        {
                            if ((i == 0))
                            {
                                adnlPay = Convert.ToDouble(inobj.OneTimeValue);
                            }
                            else
                            {
                                adnlPay = 0;
                            }
                        }
                        else if (inobj.IsAllPayment == true)
                        {
                            adnlPay = Convert.ToDouble(inobj.AllPaymentValue);
                        }
                        else
                        {
                            adnlPay = 0;
                        }

                        totAdditional += adnlPay;
                        inobj.Amortization.PaymentSummary[i].prePayment = adnlPay;

                        if ((i == 0))
                        {
                            inobj.Amortization.minPay = tempPayment + adnlPay;
                        }

                        tempPayment = tempPayment + adnlPay;
                        newBalance = newBalance + interest;
                        newBalance = newBalance - tempPayment;
                        if ((newBalance > 0))
                        {

                            inobj.Amortization.PaymentSummary[i].Principal = Math.Round(tempPayment - interest, 2);
                            totPrincipal += inobj.Amortization.PaymentSummary[i].Principal ?? 0;
                            inobj.Amortization.PaymentSummary[i].totalPrincipal = Math.Round(totPrincipal, 2);
                            inobj.Amortization.PaymentSummary[i].newBalance = Math.Round(newBalance, 2);
                            inobj.Amortization.PaymentSummary[i].totalPayment = Math.Round(tempPayment, 2);
                            totPayment += inobj.Amortization.PaymentSummary[i].totalPayment;
                        }
                        else
                        {
                            inobj.Amortization.PaymentSummary[i].newBalance = 0;
                            inobj.Amortization.PaymentSummary[i].Principal = Math.Round(tempPayment + newBalance - interest, 2);
                            totPrincipal = totPrincipal + tempPayment + newBalance - interest;
                            inobj.Amortization.PaymentSummary[i].totalPrincipal = Math.Round(totPrincipal);
                            inobj.Amortization.PaymentSummary[i].totalPayment = Math.Round((tempPayment + newBalance), 2);
                            totPayment = totPayment + inobj.Amortization.PaymentSummary[i].totalPayment;
                            inobj.Amortization.PaymentSummary[i].prePayment = 0;

                        }
                        i++;
                        if ((newBalance <= 0))
                        {
                            break;
                        }
                        paidDate = paidDate.AddMonths(1);
                    }
                }
                if (inobj.IsYearly == true)
                {
                    prevDate = paidDate;
                    while (newBalance > 0)
                    {

                        interest = (newBalance * rate) / 12;

                        if (!inobj.fixedPayment == true)
                        {
                            tempPayment = newBalance * (inobj.percentPay ?? 0) / 100;
                            if (tempPayment < inobj.cardMinPay)
                            {
                                tempPayment = inobj.cardMinPay ?? 0;
                            }
                        }
                        else
                        {
                            tempPayment = (double)loanAmount * (double)inobj.percentPay / 100;
                        }

                        if (inobj.IsAllPayment == true && inobj.IsOneTime == true)
                        {
                            if ((i == 0))
                            {
                                adnlPay = Convert.ToDouble(inobj.OneTimeValue + inobj.AllPaymentValue);
                            }
                            else
                            {
                                adnlPay = Convert.ToDouble(inobj.AllPaymentValue);
                            }
                        }
                        else if (inobj.IsOneTime == true)
                        {
                            if ((i == 0))
                            {
                                adnlPay = Convert.ToDouble(inobj.OneTimeValue);
                            }
                            else
                            {
                                adnlPay = 0;
                            }
                        }
                        else if (inobj.IsAllPayment == true)
                        {
                            adnlPay = Convert.ToDouble(inobj.AllPaymentValue);
                        }
                        else
                        {
                            adnlPay = 0;
                        }

                        if (i == 0)
                        {
                            inobj.Amortization.minPay = (tempPayment + adnlPay);
                        }

                        tempPayment += adnlPay;
                        newBalance += interest;
                        newBalance -= tempPayment;
                        if (newBalance > 0)
                        {
                            totalAnnualPrincipal += tempPayment - interest;
                            totalAnnualPayment = totalAnnualPayment + tempPayment;
                        }
                        else
                        {
                            totalAnnualPrincipal += tempPayment + newBalance - interest;
                            totalAnnualPayment += tempPayment + newBalance;
                        }
                        totalAnnualInterest = (totalAnnualInterest + interest);
                        totAddPayment = (totAddPayment + adnlPay);
                        if (newBalance <= 0)
                        {
                            inobj.Amortization.PaymentSummary.Add(new CreditRevolveModel { PMT = j + 1 });
                            inobj.Amortization.PaymentSummary[j].date = prevDate.ToString("MMM") + "-" + paidDate.ToString("MMM") + "," + paidDate.ToString("yyyy");
                            inobj.Amortization.PaymentSummary[j].totalPayment = Math.Round((totalAnnualInterest + totalAnnualPrincipal), 2);
                            inobj.Amortization.PaymentSummary[j].totalPrincipal = Math.Round(totalAnnualPrincipal, 2);
                            inobj.Amortization.PaymentSummary[j].prePayment = Math.Round((totAddPayment - adnlPay), 2);
                            inobj.Amortization.PaymentSummary[j].interestPaid = Math.Round(totalAnnualInterest, 2);
                            inobj.Amortization.PaymentSummary[j].newBalance = 0;
                            i++;
                            break;
                        }

                        else if (newBalance >= 0 && paidDate.Month == 12)
                        {
                            inobj.Amortization.PaymentSummary.Add(new CreditRevolveModel { PMT = j + 1 });
                            inobj.Amortization.PaymentSummary[j].date = prevDate.ToString("MMM") + "-" + paidDate.ToString("MMM") + "," + paidDate.ToString("yyyy");
                            inobj.Amortization.PaymentSummary[j].interestPaid = Math.Round(totalAnnualInterest, 2);
                            inobj.Amortization.PaymentSummary[j].totalPrincipal = Math.Round(totalAnnualPrincipal, 2);
                            inobj.Amortization.PaymentSummary[j].totalPayment = Math.Round(totalAnnualPayment, 2);
                            inobj.Amortization.PaymentSummary[j].prePayment = Math.Round(totAddPayment, 2);
                            inobj.Amortization.PaymentSummary[j].newBalance = Math.Round(newBalance, 2);
                            inobj.Amortization.PaymentSummary[j].Principal = null;
                            inobj.Amortization.PaymentSummary[j].interest = null;
                            totalAnnualInterest = 0;
                            totalAnnualPrincipal = 0;
                            totalAnnualPayment = 0;
                            totAddPayment = 0;

                            j = j + 1;
                            prevDate = paidDate.AddMonths(1);
                        }

                        i++;
                        paidDate = paidDate.AddMonths(1);
                    }
                }

                inobj.Amortization.payments = i;
                inobj.Amortization.payoff = paidDate.ToString("MMM, yyyy");
                inobj.Amortization.startingBalance = inobj.loanAmount ?? 0;
                if (inobj.IsYearly == false)
                {
                    inobj.Amortization.TotalInterest = Math.Round(totalInterest, 2);
                    inobj.Amortization.TotalPayments = Math.Round(totPayment, 2);
                    inobj.Amortization.TotalAdditional = Math.Round(totAdditional, 2);
                }
                if (inobj.IsYearly == true)
                {
                    inobj.Amortization.TotalInterest = Math.Round((inobj.Amortization.PaymentSummary.Sum(s => s.interestPaid)), 2);
                    inobj.Amortization.TotalPayments = Math.Round((inobj.Amortization.PaymentSummary.Sum(s => s.totalPayment)), 2);
                    inobj.Amortization.TotalAdditional = Math.Round((inobj.Amortization.PaymentSummary.Sum(s => s.prePayment)), 2);
                }
            }

            catch (Exception ex)
            {
                throw;
            }

            return inobj;
        }

        public VehicleLoanModel vehicleloancalculator(VehicleLoanModel inobj, int status = 0)
        {
            try
            {
                inobj.LoanAmortization = new LoanAmortization();
                inobj.LoanAmortization.PaymentSummary = new List<PaymentSummaryModel>();

                double purchasePrice = inobj.purchasePrice ?? 0;
                double cashDown = inobj.cashDown ?? 0;
                double drpCashDown = inobj.drpCashDown ?? 0;
                double tradeAllowance = inobj.tradeAllowance ?? 0;
                double salesTaxRate = inobj.salesTaxRate ?? 0;
                double intRate = inobj.intRate ?? 0;
                int noOfInstl = inobj.noOfInstl ?? 0;
                int monName = inobj.monName ?? 1;
                int years = inobj.years ?? 2018;


                DateTime paidDate;
                double rate = 0;
                double totSalesTax = 0;
                double totBalanceFinanced = 0;
                double monthlyPayment = 0;
                int i = 0;
                double Interest = 0;
                double newBalance = 0;
                double balance = 0;
                double currentBalance = 0; double Principal = 0; double totInterest = 0;
                double totPrincipal = 0;
                double totalAnnualPrincipal = 0; double totalAnnualInterest = 0; double totalAnnualPayment = 0; double totalAdditionalPayment = 0;
                double adnlPay = 0;

                rate = intRate / 1200;
                totSalesTax = (purchasePrice * salesTaxRate) / 100;
                totBalanceFinanced = (purchasePrice - tradeAllowance) + ((purchasePrice * (salesTaxRate) / 100) - cashDown);
                monthlyPayment = (totBalanceFinanced * (rate * (Math.Pow((1 + rate), noOfInstl) / (Math.Pow((1 + rate), (noOfInstl)) - 1))));

                if (((monName == 2) && (System.DateTime.Now.Day > 29)))
                {
                    paidDate = DateTime.ParseExact(((monName <= 9 ? "0" + monName.ToString() : monName.ToString()) + ("/" + ((System.DateTime.Now.Day - 4) + ("/" + years)))), "d", CultureInfo.InvariantCulture);
                }
                else
                {
                    paidDate = DateTime.ParseExact(((monName <= 9 ? "0" + monName.ToString() : monName.ToString()) + ("/" + (System.DateTime.Now.Day + ("/" + years)))), "d", CultureInfo.InvariantCulture);
                }

                int TLoop = paidDate.Month;
                int j = 0;
                DateTime prevDate = new DateTime();
                prevDate = paidDate;
                for (i = 0; i < noOfInstl; i++)
                {
                    if (newBalance != 0)
                    {
                        balance = newBalance;
                    }
                    else
                    {
                        balance = totBalanceFinanced;
                    }

                    if ((paidDate.Month == System.DateTime.Now.Month) && (paidDate.Year == System.DateTime.Now.Year))
                    {
                        currentBalance = balance;
                    }


                    if (status == 1)
                    {
                        if (inobj.IsAllPayment == true && inobj.IsOneTime == true)
                        {
                            if ((i == 0))
                            {
                                adnlPay = Convert.ToDouble(inobj.OneTimeValue + inobj.AllPaymentValue);
                            }
                            else
                            {
                                adnlPay = Convert.ToDouble(inobj.AllPaymentValue);
                            }

                        }
                        else if (inobj.IsOneTime == true)
                        {
                            if ((i == 0))
                            {
                                adnlPay = Convert.ToDouble(inobj.OneTimeValue);
                            }
                            else
                            {
                                adnlPay = 0;
                            }
                        }
                        else if (inobj.IsAllPayment == true)
                        {
                            adnlPay = Convert.ToDouble(inobj.AllPaymentValue);
                        }
                        else
                        {
                            adnlPay = 0;
                        }

                        if (inobj.isYearly == false)
                        {
                            Interest = balance * rate;
                            if (((newBalance + Interest) < monthlyPayment) && i != 0)
                            {
                                Principal = newBalance;
                            }
                            else
                            {
                                Principal = monthlyPayment + adnlPay - Interest;
                                totalAdditionalPayment += adnlPay;
                            }
                            totInterest += Interest;
                            totPrincipal += Principal;
                            newBalance = balance + Interest - monthlyPayment - adnlPay;
                            paidDate = paidDate.AddMonths(1);
                            totalAnnualInterest += Interest;

                            inobj.totSalesTax = Math.Round(totSalesTax, 2);
                            inobj.totBalanceFinanced = Math.Round(totBalanceFinanced, 2);
                            inobj.monthlyPayment = Math.Round(monthlyPayment, 2);
                            inobj.currentBalance = Math.Round(currentBalance, 2);
                            inobj.currentDate = System.DateTime.Now.ToString("MMM, yyyy");
                            inobj.paidDate = paidDate;

                            inobj.LoanAmortization.PaymentSummary.Add(new PaymentSummaryModel { PMT = i + 1 });
                            inobj.LoanAmortization.PaymentSummary[i].Interest = Math.Round(Interest, 2);
                            inobj.LoanAmortization.PaymentSummary[i].Date = inobj.paidDate.AddMonths(-1).ToString("MMM, yyyy");
                            inobj.LoanAmortization.PaymentSummary[i].Principal = Math.Round(Principal, 2);
                            inobj.LoanAmortization.PaymentSummary[i].Interest = Math.Round(Interest, 2);
                            inobj.LoanAmortization.PaymentSummary[i].totalPrincipal = Math.Round(totPrincipal, 2);
                            inobj.LoanAmortization.PaymentSummary[i].totalInterest = Math.Round(totInterest, 2);
                            inobj.LoanAmortization.PaymentSummary[i].Balance = null;
                            inobj.LoanAmortization.PaymentSummary[i].TotalPayment = null;

                            if (newBalance > 0)
                            {
                                inobj.LoanAmortization.PaymentSummary[i].prePayment = Math.Round(adnlPay, 2);
                                inobj.LoanAmortization.PaymentSummary[i].Balance = Math.Round(newBalance, 2);
                            }
                            else
                            {
                                inobj.LoanAmortization.PaymentSummary[i].prePayment = 0;
                                inobj.LoanAmortization.PaymentSummary[i].Balance = 0;
                            }

                        }
                        else if (inobj.isYearly == true)
                        {
                            Interest = (balance * rate);
                            totalAnnualInterest += Interest;
                            //paidDate = paidDate.AddMonths(1);
                            if (((newBalance + Interest) < monthlyPayment) && i != 0)
                            {
                                totalAnnualPrincipal += newBalance;
                                totalAnnualPayment += newBalance + Interest;
                            }
                            else
                            {
                                totalAnnualPrincipal += (monthlyPayment - Interest) + adnlPay;
                                totalAnnualPayment += monthlyPayment + adnlPay;
                                totalAdditionalPayment += adnlPay;
                            }
                            newBalance = Math.Round(balance + Interest - monthlyPayment - adnlPay, 2);

                            if (newBalance >= 0 && paidDate.Month == 12)
                            {
                                inobj.LoanAmortization.PaymentSummary.Add(new PaymentSummaryModel { PMT = j + 1 });
                                inobj.LoanAmortization.PaymentSummary[j].Date = prevDate.ToString("MMM") + "-" + paidDate.ToString("MMM") + "," + paidDate.ToString("yyyy");
                                inobj.LoanAmortization.PaymentSummary[j].totalInterest = Math.Round(totalAnnualInterest, 2);
                                inobj.LoanAmortization.PaymentSummary[j].totalPrincipal = Math.Round(totalAnnualPrincipal, 2);
                                inobj.LoanAmortization.PaymentSummary[j].TotalPayment = Math.Round(totalAnnualPayment, 2);
                                inobj.LoanAmortization.PaymentSummary[j].prePayment = Math.Round(totalAdditionalPayment, 2);
                                inobj.LoanAmortization.PaymentSummary[j].Balance = Math.Round(newBalance, 2);
                                inobj.LoanAmortization.PaymentSummary[j].Principal = null;
                                inobj.LoanAmortization.PaymentSummary[j].Interest = null;

                                totalAnnualInterest = 0;
                                totalAnnualPrincipal = 0;
                                totalAnnualPayment = 0;
                                totalAdditionalPayment = 0;

                                j = j + 1;
                                prevDate = paidDate.AddMonths(1);
                            }
                            else if (newBalance <= 1)
                            {
                                inobj.LoanAmortization.PaymentSummary.Add(new PaymentSummaryModel { PMT = j + 1 });
                                inobj.LoanAmortization.PaymentSummary[j].Date = prevDate.ToString("MMM") + "-" + paidDate.ToString("MMM") + "," + paidDate.ToString("yyyy");
                                inobj.LoanAmortization.PaymentSummary[j].totalInterest = Math.Round(totalAnnualInterest, 2);
                                inobj.LoanAmortization.PaymentSummary[j].totalPrincipal = Math.Round(totalAnnualPrincipal, 2);
                                inobj.LoanAmortization.PaymentSummary[j].TotalPayment = Math.Round(totalAnnualPayment, 2);
                                inobj.LoanAmortization.PaymentSummary[j].prePayment = Math.Round(totalAdditionalPayment, 2);
                                inobj.LoanAmortization.PaymentSummary[j].Balance = 0;
                                inobj.LoanAmortization.PaymentSummary[j].Principal = null;
                                inobj.LoanAmortization.PaymentSummary[j].Interest = null;
                                break;
                            }

                            paidDate = paidDate.AddMonths(1);

                        }
                    }

                }
                if (status == 1)
                {

                    inobj.LoanAmortization.payoff = paidDate.AddMonths(-1).ToString("MMM, yyyy");
                    inobj.LoanAmortization.CashDown = cashDown;
                    inobj.LoanAmortization.TradeAllowance = tradeAllowance;
                    inobj.LoanAmortization.InitialLoanAmount = purchasePrice;
                    inobj.LoanAmortization.TotalBalanceFinanced = totBalanceFinanced;
                    inobj.LoanAmortization.MonthlyPayment = Math.Round(monthlyPayment, 2);
                    inobj.LoanAmortization.TotalAdditional = totalAdditionalPayment;

                    if (inobj.isYearly == false)
                    {
                        inobj.LoanAmortization.payments = i;
                        inobj.LoanAmortization.totPrincipal = Math.Round(totInterest + totPrincipal, 2);
                        inobj.LoanAmortization.totInterest = Math.Round(totInterest, 2);
                    }
                    else
                    {
                        inobj.LoanAmortization.payments = i + 1;
                        inobj.LoanAmortization.totPrincipal = Math.Round(inobj.LoanAmortization.PaymentSummary.Sum(s => s.TotalPayment ?? 0), 2);
                        inobj.LoanAmortization.totInterest = Math.Round(inobj.LoanAmortization.PaymentSummary.Sum(s => s.totalInterest), 2);
                    }
                }



            }
            catch (Exception ex)
            {
                throw;
            }

            return inobj;
        }

        public FixedvsMinimumCalcModel FixedvsMinimumCalculator(FixedvsMinimumCalcModel inobj)
        {
            try
            {

                double loanAmount = inobj.loanAmount.Value;
                double intRate = inobj.intRate.Value;
                double percentPay = inobj.percentPay.Value;
                double cardMinPay = inobj.cardMinPay.Value;
                int monName = inobj.monName ?? 1;
                inobj.monName = monName;
                int years = inobj.years.Value;
                double minPay = inobj.minPay.Value;
                double fixedMinPay = 0;
                double balance = 0;
                double totalPayment = 0;
                string fixedPayOff;
                string payOffYears;
                string yearSavings;
                string fixedPayOffYears = "";
                string payoff;
                double interestPaid = 0;
                double fixedInterestPaid = 0;
                double fixedTotalPayment = 0;

                double newBalance = 0;
                double currentBalance = 0;
                double fixedNewBalance = 0;
                DateTime paidDate;
                DateTime fixedPaidDate;
                double interestSavings = 0;
                newBalance = loanAmount;
                fixedNewBalance = newBalance;

                double rate = 0;
                double interest = 0;
                double tempPayment = 0;
                double fixedInterest = 0;
                double totPayments = 1;
                double fixedTempPayment = 0;
                double totFixedPayments = 1;
                double paymentSavings = 0;

                if ((minPay == 0))
                {
                    minPay = newBalance * (percentPay / 100);
                }

                if (((monName == 2) && (System.DateTime.Now.Day > 29)))
                {
                    paidDate = DateTime.Parse(((monName <= 9 ? "0" + monName.ToString() : monName.ToString()) + ("/" + ((System.DateTime.Now.Day - 4) + ("/" + years)))));
                }
                else
                {
                    paidDate = DateTime.Parse(((monName <= 9 ? "0" + monName.ToString() : monName.ToString()) + ("/" + (System.DateTime.Now.Day + ("/" + years)))));
                }

                fixedPaidDate = paidDate;

                while ((newBalance > 0))
                {

                    rate = intRate / 100;

                    if ((paidDate.Month == System.DateTime.Now.Month) && (paidDate.Year == System.DateTime.Now.Year))
                    {
                        currentBalance = newBalance;

                    }



                    interest = (newBalance * (rate / 12));
                    interestPaid = interestPaid + interest;

                    tempPayment = newBalance * (percentPay) / 100;
                    if (tempPayment <= cardMinPay)
                    {
                        tempPayment = cardMinPay;
                    }
                    newBalance = newBalance + interest;
                    newBalance = newBalance - tempPayment;

                    if (tempPayment >= newBalance)
                    {
                        tempPayment = newBalance;
                    }

                    if (newBalance <= 0)
                    {
                        break;
                    }
                    paidDate = paidDate.AddMonths(1);

                    if (fixedNewBalance > 0)
                    {
                        fixedMinPay = minPay;

                        fixedInterest = fixedNewBalance * rate / 12;
                        if (totPayments == 1)
                        {
                            fixedTempPayment = minPay;
                            fixedMinPay = minPay;
                        }

                        fixedNewBalance = (fixedNewBalance + fixedInterest);
                        fixedNewBalance = (fixedNewBalance - fixedTempPayment);
                        fixedInterestPaid = fixedInterestPaid + fixedInterest;
                        if (fixedNewBalance > 0)
                        {
                            fixedPaidDate = fixedPaidDate.AddMonths(1);
                        }

                        totFixedPayments = totPayments;
                        fixedPayOffYears = Math.Truncate(totPayments / 12) + " years, " + (totPayments % 12) + " months";

                    }

                    totPayments++;
                }

                totalPayment = interestPaid + loanAmount;
                fixedTotalPayment = fixedInterestPaid + loanAmount;
                payoff = paidDate.Month + ", " + paidDate.Year;

                fixedPayOff = fixedPaidDate.Month + ", " + fixedPaidDate.Year;
                payOffYears = Math.Truncate(totPayments / 12) + " years, " + (totPayments % 12) + " months";
                interestSavings = interestPaid - fixedInterestPaid;

                balance = loanAmount;
                if (((totPayments - totFixedPayments) % 12) > 0)
                {
                    yearSavings = Math.Truncate((totPayments - totFixedPayments) / 12) + " years, " + ((totPayments - totFixedPayments) % 12) + " months early";
                }
                else
                {
                    yearSavings = ((totPayments - totFixedPayments) / 12) + " years early";
                }
                paymentSavings = totPayments - totFixedPayments;

                inobj.output = new FixedvsMinimumCalcDetailsModel();
                inobj.output.paymentSavings = paymentSavings;
                inobj.output.interestSavings = Math.Abs(Math.Round(interestSavings, 2));
                inobj.output.currentBalance = Math.Round(currentBalance, 2);
                inobj.output.yearSavings = yearSavings;
                inobj.output.totalPayment = Math.Round(totalPayment, 2);
                inobj.output.fixedTotalPayment = Math.Round(fixedTotalPayment, 2);
                inobj.output.payOffYears = payOffYears;
                inobj.output.fixedPayOffYears = fixedPayOffYears;
                inobj.output.interestPaid = Math.Round(interestPaid, 2);
                inobj.output.fixedInterestPaid = Math.Round(fixedInterestPaid, 2);
                inobj.output.paidDate = paidDate.ToString("MMM, yyyy");
                inobj.output.fixedPaidDate = fixedPaidDate.ToString("MMM, yyyy");


            }
            catch (Exception ex)
            {
                throw;
            }

            return inobj;
        }

        public Mortgage_PI_Model mortgageInterestCalculator(Mortgage_PI_Model modelObj)
        {
            try
            {
                double loanAmount = modelObj.loanAmount.Value;
                double rate = (modelObj.intRate.Value) / 1200;
                int terms = modelObj.no_of_terms.Value;
                int monName = modelObj.monName.Value;
                int years = modelObj.years.Value;
                string cName = modelObj.cName;
                DateTime paidDate;
                int i;

                double Principal = 0;
                double Interest = 0;
                double balance = 0;
                double newBalance = 0;
                double currentBalance = 0;
                double InstlAmount = 0;
                double totPrincipal = 0;
                double totInterest = 0;

                InstlAmount = (loanAmount) * (rate * Math.Pow((1 + rate), terms)) / ((Math.Pow((1 + rate), terms)) - 1);
                if (((monName == 2) && (System.DateTime.Now.Day > 29)))
                {
                    paidDate = DateTime.ParseExact(((monName <= 9 ? "0" + monName.ToString() : monName.ToString()) + ("/" + ((System.DateTime.Now.Day - 4) + ("/" + years)))), "d", CultureInfo.InvariantCulture);
                }
                else
                {
                    paidDate = DateTime.ParseExact(((monName <= 9 ? "0" + monName.ToString() : monName.ToString()) + ("/" + (System.DateTime.Now.Day + ("/" + years)))), "d", CultureInfo.InvariantCulture);
                }
                for (i = 0; i < terms; i++)
                {
                    if (newBalance != 0)
                    {
                        balance = newBalance;
                    }
                    else
                    {
                        balance = loanAmount;
                    }
                    if (paidDate.Month == System.DateTime.Now.Month && paidDate.Year == System.DateTime.Now.Year)
                    {
                        currentBalance = balance;

                    }
                    Interest = balance * rate;
                    Principal = InstlAmount - Interest;
                    totInterest = totInterest + Interest;
                    totPrincipal = totPrincipal + Principal;
                    newBalance = balance + Interest - InstlAmount;
                    paidDate = paidDate.AddMonths(1);
                }
                modelObj.prinicipal_Interest_Pay = Math.Round(InstlAmount, 2);
                modelObj.currentBalance = Math.Round(currentBalance, 2);
                modelObj.payoff = System.DateTime.Now.ToString("MMM,yyyy");


            }
            catch (Exception ex)
            {
                throw;
            }

            return modelObj;
        }

        public MortgagePaymentModel mortgagePaymentCalculator(MortgagePaymentModel modelObj)
        {
            try
            {
                double loanAmount = modelObj.loanAmount.Value;
                double rate = (modelObj.intRate.Value) / 1200;
                double propertyTax = (modelObj.propertyTax) ?? 0;
                double homeInsurance = (modelObj.homeInsurance) ?? 0;
                double monthlyPMI = (modelObj.monthlyPMI) ?? 0;
                double MonthlyTaxInsPMI = 0;
                double TotMortPayment = 0;

                int terms = modelObj.no_of_terms.Value;
                int monName = modelObj.monName.Value;
                int years = modelObj.years.Value;
                string cName = modelObj.cName;
                DateTime paidDate;
                int i;

                double Principal = 0;
                double Interest = 0;
                double balance = 0;
                double newBalance = 0;
                double currentBalance = 0;
                double InstlAmount = 0;
                double totPrincipal = 0;
                double totInterest = 0;

                InstlAmount = (loanAmount) * (rate * Math.Pow((1 + rate), terms)) / ((Math.Pow((1 + rate), terms)) - 1);
                MonthlyTaxInsPMI = (propertyTax / 12) + (homeInsurance / 12) + monthlyPMI;
                TotMortPayment = MonthlyTaxInsPMI + InstlAmount;
                if (((monName == 2) && (System.DateTime.Now.Day > 29)))
                {
                    paidDate = DateTime.ParseExact(((monName <= 9 ? "0" + monName.ToString() : monName.ToString()) + ("/" + ((System.DateTime.Now.Day - 4) + ("/" + years)))), "d", CultureInfo.InvariantCulture);
                }
                else
                {
                    paidDate = DateTime.ParseExact(((monName <= 9 ? "0" + monName.ToString() : monName.ToString()) + ("/" + (System.DateTime.Now.Day + ("/" + years)))), "d", CultureInfo.InvariantCulture);
                }
                for (i = 0; i < terms; i++)
                {
                    if (newBalance != 0)
                    {
                        balance = newBalance;
                    }
                    else
                    {
                        balance = loanAmount;
                    }
                    if ((paidDate.Month == System.DateTime.Now.Month) && (paidDate.Year == System.DateTime.Now.Year))
                    {
                        currentBalance = balance;

                    }
                    Interest = balance * rate;
                    Principal = InstlAmount - Interest;
                    totInterest = totInterest + Interest;
                    totPrincipal = totPrincipal + Principal;
                    newBalance = balance + Interest - InstlAmount;
                    paidDate = paidDate.AddMonths(1);
                }
                modelObj.prinicipal_Interest_Pay = Math.Round(InstlAmount, 2);
                modelObj.monthly_Tax_Insurance_PMIPay = Math.Round(MonthlyTaxInsPMI, 2);
                modelObj.total_Monthly_MortagePay = Math.Round(TotMortPayment, 2);

            }
            catch (Exception ex)
            {
                throw;
            }

            return modelObj;
        }

        public MortgageMissingValueModel mortgageMissingValue(MortgageMissingValueModel modelObj)
        {
            try
            {
                double loanAmount = (modelObj.loanAmount) ?? 0;
                double rate = ((modelObj.intRate) / 1200) ?? 0;
                double InstlAmount = (modelObj.monthlyPayment) ?? 0;

                int terms = (modelObj.no_of_terms) ?? 0;
                int monName = modelObj.monName.Value;
                int years = modelObj.years.Value;
                string cName = modelObj.cName;
                DateTime paidDate;
                int i;

                double Principal = 0;
                double Interest = 0;
                double balance = 0;
                double newBalance = 0;
                double currentBalance = 0;
                double totPrincipal = 0;
                double totInterest = 0;

                double min_rate = 0;
                double max_rate = 100;
                double guessed_pmt = 0;
                double mid_rate = 0;

                if (modelObj.monthlyPaymentB == false)
                {
                    InstlAmount = (loanAmount) * (rate * Math.Pow((1 + rate), terms)) / ((Math.Pow((1 + rate), terms)) - 1);
                    modelObj.monthlyPayment = Math.Round(InstlAmount, 2);
                }
                else if (modelObj.loanAmountB == false)
                {
                    loanAmount = Math.Round((InstlAmount) * ((Math.Pow((1 + rate), terms) - 1)) / (rate * Math.Pow((1 + rate), terms)), 0);
                    modelObj.loanAmount = Math.Round(loanAmount, 0);
                }
                else if (modelObj.no_of_termsB == false)
                {

                    if (InstlAmount <= (loanAmount * rate))
                    {
                        try
                        {
                            decimal a = 1;
                            decimal b = 0;
                            decimal c = a / b;
                        }
                        catch (Exception ex)
                        {
                            double val = loanAmount * rate;
                            throw new Exception("Monthly payment should be greater than " + val.ToString() + " for " + rate * 1200 + " % of anual interest rate");

                        }
                    }
                    double temp = (Math.Round(-(Math.Log(1 - (loanAmount / InstlAmount) * rate)) / Math.Log(1 + (rate))));
                    modelObj.no_of_terms = Convert.ToInt32(temp);
                    terms = Convert.ToInt32(temp);
                }
                else if (modelObj.intRateB == false)
                {
                    while (min_rate < (max_rate - 0.0001))
                    {
                        mid_rate = (min_rate + max_rate) / 2;
                        rate = mid_rate / 1200;
                        guessed_pmt = (loanAmount) * rate * (Math.Pow(1 + rate, terms)) / (Math.Pow(1 + rate, terms) - 1);
                        if (guessed_pmt > InstlAmount)
                        {
                            max_rate = mid_rate;
                        }
                        else
                        {
                            min_rate = mid_rate;
                        }
                    }
                    rate = rate * 1200;
                    modelObj.intRate = Math.Round(rate, 2);
                }
                else
                {
                    InstlAmount = (loanAmount) * (rate * Math.Pow((1 + rate), terms)) / ((Math.Pow((1 + rate), terms)) - 1);
                    modelObj.monthlyPayment = Math.Round(InstlAmount, 2);
                }
                if (((monName == 2) && (System.DateTime.Now.Day > 29)))
                {
                    paidDate = DateTime.ParseExact(((monName <= 9 ? "0" + monName.ToString() : monName.ToString()) + ("/" + ((System.DateTime.Now.Day - 4) + ("/" + years)))), "d", CultureInfo.InvariantCulture);
                }
                else
                {
                    paidDate = DateTime.ParseExact(((monName <= 9 ? "0" + monName.ToString() : monName.ToString()) + ("/" + (System.DateTime.Now.Day + ("/" + years)))), "d", CultureInfo.InvariantCulture);
                }
                for (i = 0; i < terms; i++)
                {
                    if (newBalance != 0)
                    {
                        balance = newBalance;
                    }
                    else
                    {
                        balance = loanAmount;
                    }
                    if ((paidDate.Month == System.DateTime.Now.Month) && (paidDate.Year == System.DateTime.Now.Year))
                    {
                        currentBalance = balance;

                    }
                    Interest = balance * rate;
                    Principal = InstlAmount - Interest;
                    totInterest = totInterest + Interest;
                    totPrincipal = totPrincipal + Principal;
                    newBalance = balance + Interest - InstlAmount;
                    paidDate = paidDate.AddMonths(1);
                }
                modelObj.currentBalance = Math.Round(currentBalance, 0);
                modelObj.currentDate = System.DateTime.Now.ToString("MMM, yyyy");

            }
            catch (Exception ex)
            {
                throw;
            }

            return modelObj;
        }


        public CollegecostListArg collegecostList(CollegecostListArg modelObj)
        {
            int searchType = 1;
            string searcha = "";
            string searchb = "";
            if (modelObj.name != null && modelObj.state != null)
            {
                searchType = 3;
                searcha = modelObj.state;
                searchb = modelObj.name;
            }
            else if (modelObj.name != null && modelObj.name != "")
            {
                searchType = 1;
                searcha = modelObj.name;
                searchb = null;
            }
            else if (modelObj.state != null && modelObj.state != "")
            {
                searchType = 2;
                searcha = modelObj.state;
                searchb = null;
            }
            else
            {

                searchType = 4;
                searcha = null;
                searchb = null;
            }


            CollegecostListArg outObj = new CollegecostListArg();
            outObj.otherColleges = new List<CollegeCostResultModel>();
            outObj.nationalColleges = new List<CollegeCostResultModel>();
            using (var db = Connection.getConnect())
            {
                try
                {
                    int a = searchType;
                    var query = "EXEC dbo.CollegeCostCalculator  @searchValue,@searchValueb,@Type , @PageNo ,@PageSize,@SortColumn ,@SortOrder";
                    var parameters = new[]
                    {
                        new SqlParameter("searchValue", searcha??(object)DBNull.Value),
                        new SqlParameter("searchValueb", searchb??(object)DBNull.Value),
                        new SqlParameter("Type",searchType),
                        new SqlParameter("PageNo", modelObj.PageNo>0 ?modelObj.PageNo : 1),
                        new SqlParameter("PageSize",modelObj.PageSize>0 ?modelObj.PageSize : 20),
                        new SqlParameter("SortColumn",modelObj.SortColumn>0 ? modelObj.SortColumn :0),
                        new SqlParameter("SortOrder", modelObj.SortOrder>0 ?modelObj.SortOrder : 0)
                    };
                    using (var multiResultSet = DbContextExtensions.MultiResultSetSqlQuery(db, query, parameters))
                    {
                        outObj.otherColleges = multiResultSet.ResultSetFor<CollegeCostResultModel>().ToList();
                    }

                    outObj.nationalColleges = (from i in db.colleges
                                               where i.collegeState == null && i.finYear == "2013-2014"
                                               select new CollegeCostResultModel
                                               {
                                                   collegeID = i.collegeID,
                                                   collegeName = "2013-2014 " + i.collegeName,
                                                   inState = i.inState ?? 0,
                                                   outState = i.outState ?? 0,
                                                   fees = i.fees,
                                                   inStateTutionFees = i.inStateTutionFees,
                                                   outStateTutionFees = i.outStateTutionFees,
                                                   roomBoardFees = i.roomBoardFees
                                               }).ToList();

                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return outObj;
        }
        public List<CollegeCostResultModel> getcollegeCost()
        {
            CollegecostListArg outObj = new CollegecostListArg();
            outObj.nationalColleges = new List<CollegeCostResultModel>();
            using (var db = Connection.getConnect())
            {
                try
                {

                    outObj.nationalColleges = (from i in db.colleges
                                               where i.collegeState == null && i.finYear == "2013-2014"
                                               select new CollegeCostResultModel
                                               {
                                                   collegeID = i.collegeID,
                                                   collegeName = "2013-2014 " + i.collegeName,
                                                   inState = i.inState ?? 0,
                                                   outState = i.outState ?? 0,
                                                   fees = i.fees,
                                                   inStateTutionFees = i.inStateTutionFees,
                                                   outStateTutionFees = i.outStateTutionFees,
                                                   roomBoardFees = i.roomBoardFees
                                               }).ToList();

                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return outObj.nationalColleges;

        }


        public mortgageAmortizationCalculator mortgageAmortization(Mortgage_PI_Model modelObj)
        {
            mortgageAmortizationCalculator mg = new mortgageAmortizationCalculator();
            mg.amortList = new List<mortgageAmortizationChart>();
            modelObj = mortgageInterestCalculator(modelObj);
            if (modelObj != null)
            {
                try
                {
                    mg.cName = modelObj.cName;
                    double loanAmount = modelObj.loanAmount ?? 0;
                    double rate = (modelObj.intRate ?? 0) / 1200;
                    mg.intRate = (modelObj.intRate ?? 6);
                    int terms = modelObj.no_of_terms ?? 0;
                    mg.no_of_terms = terms;
                    mg.currentDate = modelObj.payoff;
                    mg.currentBalance = modelObj.currentBalance ?? 0;
                    mg.oneTime = modelObj.prePayment??false;
                    mg.allPayments = modelObj.prePayment1 ?? false;
                    mg.oneTimeValue = modelObj.singlePayment??0;
                    mg.allPaymentValue = modelObj.adnlPayment??0;
                    mg.totalPayments = 0;
                    mg.totalAdditional = 0;
                    mg.totalInterest = 0;
                    int monName = modelObj.monName.Value;
                    int years = modelObj.years.Value;
                    string cName = modelObj.cName;
                    DateTime paidDate;
                    DateTime prevDate;
                    int pmt = 0;
                    int j = 0;
                    int k = 0;
                    int yearNumber = 1;

                    double Principal = 0;
                    double interest = 0;
                    double balance = 0;
                    double currentBalance = balance;
                    double InstlAmount = 0;
                    double totPrincipal = 0;
                    double totInterest = 0;
                    double totAdditional = 0;
                    double TotalPrincipal = 0;
                    double TotalInterest = 0;
                    double TotalPayAmount = 0;
                    double totalAnnualInterest = 0;
                    double totalAnnualPayment = 0;
                    double totalAnnualPrincipal = 0;
                    double totalAdditionalPayment = 0;
                    double Total_Interest = 0;
                    double TotalPayment = 0;
                    double TotalAdditional = 0;

                    double singlePayment = modelObj.singlePayment ?? 0;
                    double adnlPayment = modelObj.adnlPayment ?? 0;
                    double monthlyInstl = 0;
                    double newBalance = 0;
                    double Pre_Payment = 0;

                    bool prePayment = modelObj.prePayment ?? false;
                    bool prePayment1 = modelObj.prePayment1 ?? false;


                    InstlAmount = (loanAmount) * (rate * Math.Pow((1 + rate), terms)) / ((Math.Pow((1 + rate), terms)) - 1);
                    monthlyInstl = InstlAmount;
                    if (((monName == 2) && (System.DateTime.Now.Day > 29)))
                    {
                        paidDate = DateTime.ParseExact(((monName <= 9 ? "0" + monName.ToString() : monName.ToString()) + ("/" + ((System.DateTime.Now.Day - 4) + ("/" + years)))), "d", CultureInfo.InvariantCulture);
                    }
                    else
                    {
                        paidDate = DateTime.ParseExact(((monName <= 9 ? "0" + monName.ToString() : monName.ToString()) + ("/" + (System.DateTime.Now.Day + ("/" + years)))), "d", CultureInfo.InvariantCulture);
                    }
                    mg.firstPayment = paidDate.ToString("MMM,yyyy");
                    prevDate = paidDate;
                    for (int i = 0; i < terms; i++)
                    {
                        pmt = i + 1;
                        if (newBalance != 0)
                        {
                            balance = newBalance;
                        }
                        else
                        {
                            balance = loanAmount;
                        }
                        interest = Math.Round(balance * rate, 2);
                        totalAnnualInterest += Math.Round(interest, 2);
                        if (prePayment == true && prePayment1 == true)
                        {
                            if (i == 0)
                                Pre_Payment = singlePayment + adnlPayment;
                            else
                                Pre_Payment = adnlPayment;
                        }
                        else if (prePayment == true)
                        {
                            if (i == 0)
                                Pre_Payment = singlePayment;
                            else
                                Pre_Payment = 0;
                        }
                        else if (prePayment1 == true)
                            Pre_Payment = adnlPayment;
                        else
                            Pre_Payment = 0;

                        if (((newBalance + interest) < monthlyInstl) && i != 0)
                        {
                            Principal = newBalance;
                            totalAnnualPrincipal = totalAnnualPrincipal + newBalance;
                            totalAnnualPayment = totalAnnualPayment + newBalance + interest;
                        }
                        else
                        {
                            Principal = monthlyInstl + Pre_Payment - interest;
                            totalAnnualPrincipal = totalAnnualPrincipal + (monthlyInstl - interest) + Pre_Payment;
                            totalAnnualPayment = totalAnnualPayment + monthlyInstl + Pre_Payment;
                            totalAdditionalPayment = totalAdditionalPayment + Pre_Payment;

                        }
                        //Monthly Amortization Table
                        if (modelObj.mortgageType == 0)
                        {

                            totAdditional = totAdditional + Pre_Payment;
                            totInterest = totInterest + interest;
                            totPrincipal = totPrincipal + Principal;
                            TotalPrincipal = Math.Round(totPrincipal, 2);
                            TotalInterest = Math.Round(totInterest, 2);
                            TotalInterest = Math.Round(totInterest, 2);
                            TotalPayAmount = Math.Round((TotalPrincipal + TotalInterest), 2);
                            newBalance = Math.Round((((balance + interest) - monthlyInstl) - Pre_Payment), 2);

                            mg.amortList.Add(new mortgageAmortizationChart { pmt = i + 1 });
                            mg.amortList[i].date = paidDate.ToString("MMM,yyyy");
                            mg.amortList[i].principal = Math.Round(Principal, 2);
                            mg.amortList[i].interest = Math.Round(interest, 2);
                            mg.amortList[i].totalPrincipal = TotalPrincipal;
                            mg.amortList[i].totalInterest = TotalInterest;
                            mg.amortList[i].prePayment = Pre_Payment;
                            mg.amortList[i].balance = Math.Round(newBalance, 2);

                            if (newBalance <= 0)
                            {
                                mg.amortList[i].prePayment = 0;
                                mg.amortList[i].balance = 0;
                                break;
                            }
                            paidDate = paidDate.AddMonths(1);
                        }
                        //Annual Amortization Table
                        else if (modelObj.mortgageType == 1)
                        {
                            newBalance = Math.Round((((balance + interest) - monthlyInstl) - Pre_Payment), 2);

                            if (newBalance >= 0 && paidDate.Month == 12)
                            {
                                mg.amortList.Add(new mortgageAmortizationChart { year = j + 1 });
                                mg.amortList[j].period = prevDate.ToString("MMM") + "-" + paidDate.ToString("MMM") + "," + paidDate.ToString("yyyy");
                                mg.amortList[j].totalInterest = Math.Round(totalAnnualInterest, 2);
                                //mg.totalInterest = mg.totalInterest + mg.amortList[j].totalInterest;
                                Total_Interest = Total_Interest + (mg.amortList[j].totalInterest ?? 0);
                                mg.amortList[j].totalPrincipal = Math.Round(totalAnnualPrincipal, 2);
                                mg.amortList[j].totalPayment = Math.Round(totalAnnualPayment, 2);
                                TotalPayment = TotalPayment + (mg.amortList[j].totalPayment ?? 0);
                                // mg.totalPayments =mg.totalPayments + mg.amortList[j].totalPayment;
                                mg.amortList[j].prePayment = Math.Round(totalAdditionalPayment, 2);
                                TotalAdditional = TotalAdditional + (mg.amortList[j].prePayment ?? 0);
                                // mg.totalAdditional =mg.totalAdditional + mg.amortList[j].prePayment;
                                mg.amortList[j].balance = Math.Round(newBalance, 2);

                                totalAnnualInterest = 0;
                                totalAnnualPrincipal = 0;
                                totalAnnualPayment = 0;
                                totalAdditionalPayment = 0;

                                j = j + 1;
                                prevDate = paidDate.AddMonths(1);
                            }
                            else if (newBalance <= 1)
                            {
                                mg.amortList.Add(new mortgageAmortizationChart { year = j + 1 });
                                mg.amortList[j].period = prevDate.ToString("MMM") + "-" + paidDate.ToString("MMM") + "," + paidDate.ToString("yyyy");
                                mg.amortList[j].totalInterest = Math.Round(totalAnnualInterest, 2);
                                Total_Interest = Total_Interest + (mg.amortList[j].totalInterest ?? 0);
                                mg.amortList[j].totalPrincipal = Math.Round(totalAnnualPrincipal, 2);
                                mg.amortList[j].totalPayment = Math.Round(totalAnnualPayment, 2);
                                TotalPayment = TotalPayment + (mg.amortList[j].totalPayment ?? 0);
                                mg.amortList[j].prePayment = Math.Round(totalAdditionalPayment, 2);
                                TotalAdditional = TotalAdditional + (mg.amortList[j].prePayment ?? 0);
                                mg.amortList[j].balance = 0;
                                break;
                            }
                            paidDate = paidDate.AddMonths(1);
                        }
                    }
                    mg.loanAmount = loanAmount;
                    mg.no_of_payments = pmt;
                    mg.payOff = paidDate.ToString("MMM,yyyy");
                    mg.principal_Interest_Pay = modelObj.prinicipal_Interest_Pay;
                    if (modelObj.mortgageType == 0)
                    {
                        mg.totalPayments = Math.Round(TotalPayAmount, 2);
                        mg.totalAdditional = Math.Round((totAdditional - adnlPayment), 2);
                        mg.totalInterest = Math.Round(TotalInterest, 2);
                    }
                    else
                    {
                        mg.totalPayments = Math.Round(TotalPayment, 2);
                        mg.totalAdditional = Math.Round(TotalAdditional, 2);
                        mg.totalInterest = Math.Round(Total_Interest, 2);
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return mg;
        }

        public FixedvsMinimumCalcModel FixedvsMinimumAmort(FixedvsMinimumCalcModel inobj)
        {
            try
            {
                double loanAmount = inobj.loanAmount ?? 0;
                double rate = 0;
                int monName = inobj.monName ?? 1;
                int years = inobj.years ?? System.DateTime.Now.Year;
                DateTime paidDate;
                double totPrincipal = 0;
                double balance = 0;
                double newBalance = 0;
                double tempPayment = 0;
                double interest;
                double totalInterest = 0;
                double totPayment = 0;
                double totAdditional = 0;
                double currentBalance = 0;
                double totalAnnualPrincipal = 0;
                double totalAnnualPayment = 0;
                double totalAnnualInterest = 0;
                double totAddPayment = 0;
                int i = 0;
                int j = 0;
                double adnlPay = 0;
                DateTime prevDate;
                newBalance = loanAmount;
                rate = (inobj.intRate ?? 0) / 100;
                if (((monName == 2) && (System.DateTime.Now.Day > 29)))
                {
                    paidDate = DateTime.ParseExact(((monName <= 9 ? "0" + monName.ToString() : monName.ToString()) + ("/" + ((System.DateTime.Now.Day - 4) + ("/" + years)))), "d", CultureInfo.InvariantCulture);
                }
                else
                {
                    paidDate = DateTime.ParseExact(((monName <= 9 ? "0" + monName.ToString() : monName.ToString()) + ("/" + (System.DateTime.Now.Day + ("/" + years)))), "d", CultureInfo.InvariantCulture);
                }
                inobj.Amortization = new CreditRevolveAmortization();
                inobj.Amortization.PaymentSummary = new List<CreditRevolveModel>();

                if (inobj.IsYearly == false)
                {
                    while (newBalance > 0)
                    {
                        inobj.Amortization.PaymentSummary.Add(new CreditRevolveModel { PMT = i + 1 });
                        inobj.Amortization.PaymentSummary[i].date = paidDate.ToString("MMM, yyyy");
                        interest = (newBalance * rate) / 12;
                        inobj.Amortization.PaymentSummary[i].interest = Math.Round(interest, 2);
                        totalInterest += interest;
                        inobj.Amortization.PaymentSummary[i].interestPaid = Math.Round(totalInterest, 2);
                        if ((paidDate.Month == System.DateTime.Now.Month) && (paidDate.Year == System.DateTime.Now.Year))
                        {
                            currentBalance = balance;
                        }
                        if (!inobj.fixedPayment == true)
                        {
                            tempPayment = newBalance * (inobj.percentPay ?? 0) / 100;
                            if (tempPayment < inobj.cardMinPay)
                            {
                                tempPayment = inobj.cardMinPay ?? 0;
                            }
                        }
                        else
                        {
                            tempPayment = (double)loanAmount * (double)inobj.percentPay / 100;
                        }

                        if (inobj.IsAllPayment == true && inobj.IsOneTime == true)
                        {
                            if ((i == 0))
                            {
                                adnlPay = Convert.ToDouble(inobj.OneTimeValue + inobj.AllPaymentValue);
                            }
                            else
                            {
                                adnlPay = Convert.ToDouble(inobj.AllPaymentValue);
                            }
                        }
                        else if (inobj.IsOneTime == true)
                        {
                            if ((i == 0))
                            {
                                adnlPay = Convert.ToDouble(inobj.OneTimeValue);
                            }
                            else
                            {
                                adnlPay = 0;
                            }
                        }
                        else if (inobj.IsAllPayment == true)
                        {
                            adnlPay = Convert.ToDouble(inobj.AllPaymentValue);
                        }
                        else
                        {
                            adnlPay = 0;
                        }

                        totAdditional += adnlPay;
                        inobj.Amortization.PaymentSummary[i].prePayment = adnlPay;

                        if ((i == 0))
                        {
                            inobj.Amortization.minPay = tempPayment + adnlPay;
                        }

                        tempPayment = tempPayment + adnlPay;
                        newBalance = newBalance + interest;
                        newBalance = newBalance - tempPayment;
                        if ((newBalance > 0))
                        {

                            inobj.Amortization.PaymentSummary[i].Principal = Math.Round(tempPayment - interest, 2);
                            totPrincipal += inobj.Amortization.PaymentSummary[i].Principal ?? 0;
                            inobj.Amortization.PaymentSummary[i].totalPrincipal = Math.Round(totPrincipal, 2);
                            inobj.Amortization.PaymentSummary[i].newBalance = Math.Round(newBalance, 2);
                            inobj.Amortization.PaymentSummary[i].totalPayment = Math.Round(tempPayment, 2);
                            totPayment += inobj.Amortization.PaymentSummary[i].totalPayment;
                        }
                        else
                        {
                            inobj.Amortization.PaymentSummary[i].newBalance = 0;
                            inobj.Amortization.PaymentSummary[i].Principal = Math.Round(tempPayment + newBalance - interest, 2);
                            totPrincipal = totPrincipal + tempPayment + newBalance - interest;
                            inobj.Amortization.PaymentSummary[i].totalPrincipal = Math.Round(totPrincipal);
                            inobj.Amortization.PaymentSummary[i].totalPayment = Math.Round((tempPayment + newBalance), 2);
                            totPayment = totPayment + inobj.Amortization.PaymentSummary[i].totalPayment;
                            inobj.Amortization.PaymentSummary[i].prePayment = 0;

                        }
                        i++;
                        if ((newBalance <= 0))
                        {
                            break;
                        }
                        paidDate = paidDate.AddMonths(1);
                    }
                }
                if (inobj.IsYearly == true)
                {
                    prevDate = paidDate;
                    while (newBalance > 0)
                    {

                        interest = (newBalance * rate) / 12;

                        if (!inobj.fixedPayment == true)
                        {
                            tempPayment = newBalance * (inobj.percentPay ?? 0) / 100;
                            if (tempPayment < inobj.cardMinPay)
                            {
                                tempPayment = inobj.cardMinPay ?? 0;
                            }
                        }
                        else
                        {
                            tempPayment = (double)loanAmount * (double)inobj.percentPay / 100;
                        }

                        if (inobj.IsAllPayment == true && inobj.IsOneTime == true)
                        {
                            if ((i == 0))
                            {
                                adnlPay = Convert.ToDouble(inobj.OneTimeValue + inobj.AllPaymentValue);
                            }
                            else
                            {
                                adnlPay = Convert.ToDouble(inobj.AllPaymentValue);
                            }
                        }
                        else if (inobj.IsOneTime == true)
                        {
                            if ((i == 0))
                            {
                                adnlPay = Convert.ToDouble(inobj.OneTimeValue);
                            }
                            else
                            {
                                adnlPay = 0;
                            }
                        }
                        else if (inobj.IsAllPayment == true)
                        {
                            adnlPay = Convert.ToDouble(inobj.AllPaymentValue);
                        }
                        else
                        {
                            adnlPay = 0;
                        }

                        if (i == 0)
                        {
                            inobj.Amortization.minPay = (tempPayment + adnlPay);
                        }

                        tempPayment += adnlPay;
                        newBalance += interest;
                        newBalance -= tempPayment;
                        if (newBalance > 0)
                        {
                            totalAnnualPrincipal += tempPayment - interest;
                            totalAnnualPayment = totalAnnualPayment + tempPayment;
                        }
                        else
                        {
                            totalAnnualPrincipal += tempPayment + newBalance - interest;
                            totalAnnualPayment += tempPayment + newBalance;
                        }
                        totalAnnualInterest = (totalAnnualInterest + interest);
                        totAddPayment = (totAddPayment + adnlPay);
                        if (newBalance <= 0)
                        {
                            inobj.Amortization.PaymentSummary.Add(new CreditRevolveModel { PMT = j + 1 });
                            inobj.Amortization.PaymentSummary[j].date = prevDate.ToString("MMM") + "-" + paidDate.ToString("MMM") + "," + paidDate.ToString("yyyy");
                            inobj.Amortization.PaymentSummary[j].totalPayment = Math.Round((totalAnnualInterest + totalAnnualPrincipal), 2);
                            inobj.Amortization.PaymentSummary[j].totalPrincipal = Math.Round(totalAnnualPrincipal, 2);
                            inobj.Amortization.PaymentSummary[j].prePayment = Math.Round((totAddPayment - adnlPay), 2);
                            inobj.Amortization.PaymentSummary[j].interestPaid = Math.Round(totalAnnualInterest, 2);
                            inobj.Amortization.PaymentSummary[j].newBalance = 0;
                            i++;
                            break;
                        }

                        else if (newBalance >= 0 && paidDate.Month == 12)
                        {
                            inobj.Amortization.PaymentSummary.Add(new CreditRevolveModel { PMT = j + 1 });
                            inobj.Amortization.PaymentSummary[j].date = prevDate.ToString("MMM") + "-" + paidDate.ToString("MMM") + "," + paidDate.ToString("yyyy");
                            inobj.Amortization.PaymentSummary[j].interestPaid = Math.Round(totalAnnualInterest, 2);
                            inobj.Amortization.PaymentSummary[j].totalPrincipal = Math.Round(totalAnnualPrincipal, 2);
                            inobj.Amortization.PaymentSummary[j].totalPayment = Math.Round(totalAnnualPayment, 2);
                            inobj.Amortization.PaymentSummary[j].prePayment = Math.Round(totAddPayment, 2);
                            inobj.Amortization.PaymentSummary[j].newBalance = Math.Round(newBalance, 2);
                            inobj.Amortization.PaymentSummary[j].Principal = null;
                            inobj.Amortization.PaymentSummary[j].interest = null;
                            totalAnnualInterest = 0;
                            totalAnnualPrincipal = 0;
                            totalAnnualPayment = 0;
                            totAddPayment = 0;

                            j = j + 1;
                            prevDate = paidDate.AddMonths(1);
                        }

                        i++;
                        paidDate = paidDate.AddMonths(1);
                    }
                }

                inobj.Amortization.payments = i;
                inobj.Amortization.payoff = paidDate.ToString("MMM, yyyy");
                inobj.Amortization.startingBalance = inobj.loanAmount ?? 0;
                if (inobj.IsYearly == false)
                {
                    inobj.Amortization.TotalInterest = Math.Round(totalInterest, 2);
                    inobj.Amortization.TotalPayments = Math.Round(totPayment, 2);
                    inobj.Amortization.TotalAdditional = Math.Round(totAdditional, 2);
                }
                if (inobj.IsYearly == true)
                {
                    inobj.Amortization.TotalInterest = Math.Round((inobj.Amortization.PaymentSummary.Sum(s => s.interestPaid)), 2);
                    inobj.Amortization.TotalPayments = Math.Round((inobj.Amortization.PaymentSummary.Sum(s => s.totalPayment)), 2);
                    inobj.Amortization.TotalAdditional = Math.Round((inobj.Amortization.PaymentSummary.Sum(s => s.prePayment)), 2);
                }
            }

            catch (Exception ex)
            {
                throw;
            }

            return inobj;
        }

    }

}