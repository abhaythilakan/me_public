﻿using CLayer;
using System;
using CLayer.common;
using CLayer.DBModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Globalization;

namespace DLayer
{
    public class ChangePassword
    {
        public bool changepassword(ChangePasswordModel obj,long userID)
        {
            bool status = false;
            Common.pwdEncryptDecrypt decrypt = new Common.pwdEncryptDecrypt();
            string pwd;
            using (var db = Connection.getConnect())
            {

                User u = new User();
                string newPassword;
                try
                {
                    pwd= decrypt.Encrypt(obj.oldPwd);
                    newPassword = decrypt.Encrypt(obj.oldPwd);
                    var itm = db.Users.Where(b => b.userId == userID && b.password == pwd).FirstOrDefault();
                    if (itm != null)
                    {
                        u = db.Users.SingleOrDefault(b => b.userId == userID && b.password == pwd);
                        if (u != null && userID > 0)
                        {
                            u.password = obj.newPwd;
                            db.Entry(u).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            return true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }

        
            }
            return status;
        }
    }
}
