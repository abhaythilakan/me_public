﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using DLayer.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLayer
{
    public class PermissionSett
    {
        public List<PermissionModel> getPermissionList(long userID, CustomEnum.userCatEnum usrCat)
        {
            List<PermissionModel> mdl = new List<PermissionModel>();
            try
            {
                using (var db = Connection.getConnect())
                {
                    var query = "EXEC dbo.getPagePermission @ID,@TYPE";
                    var parameters = new[]
                    {
                        new SqlParameter("ID", userID),
                        new SqlParameter("TYPE", (int)usrCat)
                    };
                    using (var multiResultSet = DbContextExtensions.MultiResultSetSqlQuery(db, query, parameters))
                    {
                        mdl = multiResultSet.ResultSetFor<PermissionModel>().ToList();
                    }
                    //if (mdl != null)
                    //{
                    //    //Need to fix
                    //    //for (int i = 0; i < mdl.Count(); i++)
                    //    //{
                    //    //    if (usrCat == CustomEnum.userCatEnum.Client)
                    //    //        mdl[i].reportTypeID = db.Clients.Where(b => b.clientId == userID && b.isActive == true).FirstOrDefault().reportType;
                    //    //    else if (usrCat == CustomEnum.userCatEnum.Profile)
                    //    //        mdl[i].reportTypeID = db.profiles.Where(b => b.profileID == userID && b.isActive == true).FirstOrDefault().reportType;
                    //    //}
                    //}
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return mdl;
        }

        public List<PermissionModel> savePermission(List<PermissionModel> data, long id, CustomEnum.userCatEnum userType, ME_v2Entities db)
        {
            try
            {
                //using (var db = Connection.getConnect())
                //{
                int i = -1;
                if (data != null)
                {
                    foreach (var itm in data)
                    {
                        i++;
                        Permission dd = new Permission();
                        if (itm.perId > 0)
                        {
                            dd = db.Permissions.SingleOrDefault(b => b.perID == itm.perId);
                            if (dd != null && dd.perID > 0)
                            {
                                dd.isRead = itm.isRead;
                                dd.isEdit = itm.isEdit;
                                dd.reportTypeID = itm.reportTypeID;
                                db.Entry(dd).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            if (userType == CustomEnum.userCatEnum.Advisor)
                            {
                                dd.advisorId = id;

                            }
                            if (userType == CustomEnum.userCatEnum.Client)
                            {
                                dd.clientId = id;
                            }
                            if (userType == CustomEnum.userCatEnum.Profile)
                            {
                                dd.profileId = id;
                            }
                            dd.pageId = itm.pageId;
                            dd.isRead = itm.isRead;
                            dd.isEdit = itm.isEdit;
                            dd.reportTypeID = itm.reportTypeID;
                            db.Permissions.Add(dd);
                            db.SaveChanges();
                            long perID = dd.perID;
                            data[i].perId = perID;
                        }
                    }
                }
                //}
            }
            catch (Exception ex)
            {
                throw;
            }
            return data;
        }


        public AccesSettingsModel saveAdvisorPermission(AccesSettingsModel inputDT, long advisorID, ME_v2Entities db)
        {
            try
            {
                //using (var db = Connection.getConnect())
                //{
                int i = -1;
                if (inputDT.AdvancedReportDefaults != null && inputDT.AdvancedReportDefaults != null)
                {
                    foreach (var itm in inputDT.AdvancedReportDefaults)
                    {
                        i++;
                        Permission dd = new Permission();
                        if (itm.perID > 0)
                        {
                            dd = db.Permissions.SingleOrDefault(b => b.perID == itm.perID);
                            if (dd != null && dd.perID > 0)
                            {
                                dd.isRead = itm.isRead;
                                dd.isEdit = itm.isEdit;
                                db.Entry(dd).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            dd = new Permission();
                            dd = db.Permissions.SingleOrDefault(b => b.advisorId == advisorID && b.reportTypeID == (int)CustomEnum.reportTypeEnum.advancedReport && b.isRead == itm.isRead && b.isEdit == itm.isEdit && b.pageId == itm.pageId);
                            if (dd == null)
                            {
                                dd = new Permission();
                                dd.advisorId = advisorID;
                                dd.clientId = 0;
                                dd.profileId = 0;
                                dd.reportTypeID = itm.reportTypeID ?? 1;
                                dd.pageId = itm.pageId;
                                dd.isRead = itm.isRead;
                                dd.isEdit = itm.isEdit;
                                db.Permissions.Add(dd);
                                db.SaveChanges();
                                long perID = dd.perID;
                                inputDT.AdvancedReportDefaults[i].perID = perID;
                            }
                        }
                    }
                    int j = -1;
                    foreach (var itm in inputDT.ExpressReportDefaults)
                    {
                        j++;
                        Permission dd = new Permission();
                        if (itm.perID > 0)
                        {
                            dd = db.Permissions.SingleOrDefault(b => b.perID == itm.perID);
                            if (dd != null && dd.perID > 0)
                            {
                                dd.isRead = itm.isRead;
                                dd.isEdit = itm.isEdit;
                                db.Entry(dd).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            dd = new Permission();
                            dd = db.Permissions.SingleOrDefault(b => b.advisorId == advisorID && b.reportTypeID == (int)CustomEnum.reportTypeEnum.expressReport && b.isRead == itm.isRead && b.isEdit == itm.isEdit && b.pageId == itm.pageId);
                            if (dd == null)
                            {
                                dd = new Permission();
                                dd.advisorId = advisorID;
                                dd.clientId = 0;
                                dd.profileId = 0;
                                dd.reportTypeID = itm.reportTypeID ?? 2;
                                dd.pageId = itm.pageId;
                                dd.isRead = itm.isRead;
                                dd.isEdit = itm.isEdit;
                                db.Permissions.Add(dd);
                                db.SaveChanges();
                                long perID = dd.perID;
                                inputDT.ExpressReportDefaults[j].perID = perID;
                            }
                        }
                    }
                }
                //}
            }
            catch (Exception ex)
            {
                throw;
            }
            return inputDT;
        }


    }
}
