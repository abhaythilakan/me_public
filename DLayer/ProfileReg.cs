﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using DLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLayer
{
    public class ProfileReg
    {
        public profile fillProfileToDB(ProfileModel data, profile dd)
        {
            try
            {
                if (data.profileID > 0)
                {
                    dd.createdDate = data.createdDate;
                }
                else
                {
                    dd.createdDate = DateTime.Now;
                }
                dd.profileID = data.profileID;
                dd.profileDate = data.profileDate;
                dd.notes = data.notes;
                dd.profileName = data.profileName;
                dd.clientID = data.clientID;
                dd.isClientAccess = data.isClientAccess;
                dd.isActive = true;
                dd.designMode = data.designMode;
                dd.formattedActionPlan = data.formattedActionPlan;
                dd.reportType = data.reportType;
                dd.isReportDateChanged = data.isReportDateChanged;
                dd.finYear = data.finYear;
            }
            catch (Exception ex)
            {
                throw;
            }
            return dd;
        }
        public ProfileModel fillDBToProfile(profile data)
        {
            ProfileModel dd = new ProfileModel();
            try
            {
                dd.profileID = data.profileID;
                dd.profileDate = data.profileDate;
                dd.notes = data.notes;
                dd.profileName = data.profileName;
                dd.clientID = data.clientID;
                dd.isClientAccess = data.isClientAccess;
                dd.isActive = true;
                dd.designMode = data.designMode;
                dd.formattedActionPlan = data.formattedActionPlan;
                dd.reportType = data.reportType;
                dd.isReportDateChanged = data.isReportDateChanged;
                dd.finYear = data.finYear;
                dd.createdDate = data.createdDate;
            }
            catch (Exception ex)
            {
                throw;
            }
            return dd;
        }

        public List<ProfileModel> getProfileList(ProfileListArgModel data)
        {
            List<ProfileModel> mdl = new List<ProfileModel>();
            try
            {
                using (var db = Connection.getConnect())
                {
                    List<profile> dd = db.profiles.Where(b => b.clientID == data.clientId).ToList();
                    if (dd != null && dd.Count() > 0)
                    {
                        foreach (profile itm in dd)
                        {
                            ProfileModel md = new ProfileModel();
                            md = fillDBToProfile(itm);
                            mdl.Add(md);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return mdl;
        }

        public ProfileModel saveProfile(ProfileModel data, ME_v2Entities db = null)
        {
            profile dd = new profile();
            try
            {
                db = db ?? Connection.getConnect();
                //db.Database.Connection.Close();
                if (data.profileID > 0)
                {
                    dd = db.profiles.SingleOrDefault(b => b.profileID == data.profileID);
                    if (dd != null && dd.profileID > 0)
                    {
                        dd = fillProfileToDB(data, dd);
                        db.Entry(dd).State = System.Data.Entity.EntityState.Modified;
                    }
                    db.SaveChanges();
                }
                else
                {
                    dd = fillProfileToDB(data, dd);
                    db.profiles.Add(dd);
                    db.SaveChanges();
                    data.profileID = dd.profileID;
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                throw;
            }
            return data;
        }

        public ProfileCollection saveProfileFull(ProfileCollection data)
        {
            using (var db = Connection.getConnect())
            {
                //using (var transaction = db.Database.BeginTransaction(IsolationLevel.Serializable))
                //{
                try
                {
                    //using (var db = Connection.getConnect())
                    //{
                    var cd = data.profile;
                    if (cd.profileID > 0)
                    {
                        profile dd = db.profiles.Single(b => b.profileID == cd.profileID);
                        if (dd != null && dd.profileID > 0)
                        {
                            dd = fillProfileToDB(data.profile, dd);
                            db.Entry(dd).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        profile dd = new profile();
                        dd = fillProfileToDB(data.profile, dd);
                        db.profiles.Add(dd);
                        db.SaveChanges();
                        data.profile.profileID = dd.profileID;
                    }
                    if (data.profile != null && data.profile.profileID > 0)
                    {
                        ProfileCollection dd = new ProfileCollection();
                        dd = saveProfileSettings(data, db);
                        data.global = dd.global;
                        data.ret = dd.ret;
                        data.Income = dd.Income;
                        data.debt = dd.debt;
                        data.emerg = dd.emerg;
                        data.insu = dd.insu;
                        data.edu = dd.edu;
                        if (cd.reportType > 0)
                        {
                            PermissionSett ps = new PermissionSett();
                            if (data.permissionList != null)
                            {
                                for (int i = 0; i < data.permissionList.Count(); i++)
                                {
                                    data.permissionList[i].reportTypeID = cd.reportType;
                                }
                                data.permissionList = ps.savePermission(data.permissionList, data.profile.profileID, CustomEnum.userCatEnum.Profile, db);
                                //transaction.Commit();
                            }
                            else
                            {
                                //transaction.Commit();
                                List<PermissionModel> ddl = new List<PermissionModel>();
                                ddl = ps.getPermissionList(data.profile.profileID, CustomEnum.userCatEnum.Profile);
                                data.permissionList = ddl;
                                data.permissionList = ps.savePermission(data.permissionList, data.profile.profileID, CustomEnum.userCatEnum.Profile, db);
                            }
                        }
                    }
                }
                //}
                catch (Exception ex)
                {
                    //transaction.Rollback();
                    throw;
                }
                //}
            }
            return data;
        }

        public ProfileCollection saveProfileSettings(ProfileCollection data, ME_v2Entities db)
        {
            try
            {
                //using (var db = Connection.getConnect())
                //{
                DefaultsReg dg = new DefaultsReg();
                var cd = data.profile;
                if (cd.profileID > 0)
                {
                    if (data.global != null)
                    {
                        long globalID = dg.saveGlobalSettings(data.global, cd.profileID, data.usrCat);
                        data.global.defultID = globalID;
                    }
                    if (data.ret != null)
                    {
                        long retID = dg.saveRetirementDefault(data.ret, cd.profileID, data.usrCat);
                        data.ret.RetirementDefaultID = retID;
                    }
                    if (data.Income != null)
                    {
                        long inProjID = dg.saveIncomeProjectionDefault(data.Income, cd.profileID, data.usrCat);
                        data.Income.defaultID = inProjID;
                    }
                    if (data.debt != null)
                    {
                        long debtID = dg.saveDebtDefault(data.debt, cd.profileID, data.usrCat);
                        data.debt.defaultID = debtID;
                    }
                    if (data.emerg != null)
                    {
                        long emerID = dg.saveEmergencyFundDefault(data.emerg, cd.profileID, data.usrCat);
                        data.emerg.defaultID = emerID;
                    }
                    if (data.insu != null)
                    {
                        long insuID = dg.saveInsuranceDefault(data.insu, cd.profileID, data.usrCat);
                        data.insu.defaultID = insuID;
                    }
                    if (data.edu != null)
                    {
                        long eduID = dg.saveEducationDefault(data.edu, cd.profileID, data.usrCat);
                        data.edu.defaultID = eduID;
                    }
                }
                //}
            }
            catch (Exception ex)
            {
                throw;
            }
            return data;
        }

        public ProfileCollection getProfileDetails(long profileID)
        {
            ProfileCollection md = new ProfileCollection();
            try
            {
                using (var db = Connection.getConnect())
                {
                    DefaultsReg dg = new DefaultsReg();
                    profile data = db.profiles.Single(b => b.profileID == profileID);
                    md.profile = fillDBToProfile(data);

                    md.global = dg.getGlobalDefault(profileID, (int)CustomEnum.userCatEnum.Profile);
                    md.ret = dg.getRetirementDefault(profileID, (int)CustomEnum.userCatEnum.Profile);
                    md.Income = dg.getIncomeProjectionDefault(profileID, (int)CustomEnum.userCatEnum.Profile);
                    md.debt = dg.getDebtDefault(profileID, (int)CustomEnum.userCatEnum.Profile);
                    md.emerg = dg.getEmergencyFundDefault(profileID, (int)CustomEnum.userCatEnum.Profile);
                    md.insu = dg.getInsuranceDefault(profileID, (int)CustomEnum.userCatEnum.Profile);
                    md.edu = dg.getEducationDefault(profileID, (int)CustomEnum.userCatEnum.Profile);
                    if (data.reportType > 0)
                    {
                        PermissionSett ps = new PermissionSett();
                        md.permissionList = ps.getPermissionList(profileID, CustomEnum.userCatEnum.Profile);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return md;
        }

        public bool deleteProfile(long profileID)
        {
            bool isDeleted = false;
            try
            {
                using (var db = Connection.getConnect())
                {
                    profile cl = db.profiles.SingleOrDefault(b => b.profileID == profileID);
                    cl.isActive = false;
                    db.Entry(cl).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    isDeleted = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isDeleted;
        }


    }
}
