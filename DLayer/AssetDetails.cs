﻿using CLayer;
using System;
using CLayer.common;
using CLayer.DBModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Globalization;

namespace DLayer
{
    public class AssetDetails
    {
        public AssetModel getNonRetAssetDetails(long profileID, int assetType)
        {
            AssetModel outObj = new AssetModel();
            outObj.AssetList = new List<NonRetirementAssetsModel>();
            int type = 0;
            using (var db = Connection.getConnect())
            {
                try
                {
                    if (profileID > 0)
                    {
                        outObj.AssetList = (from d in db.Asset_NonRetirement
                                            where d.profileID == profileID && d.isActive == true && d.assetCatID == assetType
                                            select new NonRetirementAssetsModel
                                            {
                                                amount = d.amount,
                                                assetCatID = d.assetCatID,
                                                profileID = d.profileID,
                                                assetID = d.assetID,
                                                assetTypeID = d.assetTypeID,
                                                company_location = d.company_location,
                                                description = d.description,
                                                isIncludeEmergencyFund = d.isIncludeEmergencyFund,
                                                monthlyContribution = d.monthlyContribution,
                                                valueAsOf = d.valueAsOf,
                                                isActive = d.isActive,
                                                taxVsDefferedID= d.taxVsDefferedID

                                            }).ToList();
                        if (outObj.AssetList.Count == 0)
                        {
                            outObj.AssetList.Add(new NonRetirementAssetsModel { profileID = profileID, amount = 0, monthlyContribution = 0, isIncludeEmergencyFund = false, valueAsOf = DateTime.Now });
                        }
                        else
                        {
                            outObj.totalSum = outObj.AssetList.Sum(b => b.amount).Value;
                        }
                    }
                    else
                    {
                        return null;
                    }

                }
                catch (Exception ex)
                {
                    throw;
                }

            }
            return outObj;
        }

        public List<Asset_RetirementModel> getRetirementAsset(long profileID)
        {
            List<Asset_RetirementModel> outObj = new List<Asset_RetirementModel>();
            int type = 0;
            using (var db = Connection.getConnect())
            {
                try
                {
                    if (profileID > 0)
                    {
                        outObj = (from d in db.Asset_Retirement
                                  where d.profileID == profileID && d.isActive == true
                                  select new Asset_RetirementModel
                                  {
                                      name = d.name,
                                      totalAmount = db.Asset_RetirementDetails.Where(s => s.assetID == d.assetID && s.isActive == true).Sum(b => b.amount.Value),
                                      assetID = d.assetID,
                                      lifeEcpectancy = d.lifeEcpectancy,
                                      retAge = d.retAge,
                                      profileID = d.profileID

                                  }).ToList();
                        if (outObj.Count == 0)
                        {
                            outObj.Add(new Asset_RetirementModel { profileID = profileID, totalAmount = 0, retAge = 60, lifeEcpectancy = 90 });
                        }
                        else
                        {
                            int i = 0;
                            foreach (var a in outObj)
                            {
                                if (a.totalAmount == null)
                                {
                                    outObj[i].totalAmount = 0;
                                }
                                i++;
                            }
                        }
                    }
                    else
                    {
                        return null;
                    }

                }
                catch (Exception ex)
                {
                    throw;
                }

            }
            return outObj;
        }

        public List<Asset_RetirementDetailsModel> getRetirementAssetDetails(long ID, int type)
        {
            List<Asset_RetirementDetailsModel> outObj = new List<Asset_RetirementDetailsModel>();
            bool isOther = false;
            using (var db = Connection.getConnect())
            {
                try
                {
                    switch (type)
                    {
                        case (int)CustomEnum.AssetTypesEnum.RetirementAsset:
                            isOther = false;
                            break;
                        case (int)CustomEnum.AssetTypesEnum.OtherRetirement:
                            isOther = true;
                            break;
                    }

                    if (ID > 0)
                    {
                        outObj = (from d in db.Asset_RetirementDetails
                                  where (d.profileID == ID && d.isActive == true && d.isOtherRetirement == true && isOther == true) || (d.assetID == ID && d.isActive == true && d.isOtherRetirement == false && isOther != true)
                                  select new Asset_RetirementDetailsModel
                                  {
                                      amount = d.amount,
                                      company = d.company,
                                      assetTypeID = d.assetTypeID,
                                      assetID_Retirement = d.assetID,
                                      monthlyContribution = d.monthlyContribution,
                                      notes = d.notes,
                                      profileID = d.profileID,
                                      retID = d.retID,
                                      empContribution = d.empContribution,
                                      valueAsOf = d.valueAsOf,
                                      isActive = d.isActive,
                                      isOtherRetirement = d.isOtherRetirement,
                                      taxVsDefferedID = d.taxVsDefferedID
                                  }).ToList();

                        //if (outObj.Count == 0)
                        //{
                        //    if (type == (int)CustomEnum.AssetTypesEnum.OtherRetirement)
                        //        outObj.Add(new Asset_RetirementDetailsModel { amount = 0, assetID_Retirement = 0, profileID = ID });
                        //    else if (type == (int)CustomEnum.AssetTypesEnum.RetirementAsset)
                        //        outObj.Add(new Asset_RetirementDetailsModel { amount = 0 });
                        //}
                    }
                    else
                    {
                        return null;
                    }

                }
                catch (Exception ex)
                {
                    throw;
                }

            }
            return outObj;
        }

        public Asset_EmergencyFundModel getEmergencyFundandhouseholdIncome(long profileID)
        {
            Asset_EmergencyFundModel outObj = new Asset_EmergencyFundModel();
            int type = 0;
            using (var db = Connection.getConnect())
            {
                try
                {
                    if (profileID > 0)
                    {
                        outObj = (from d in db.Asset_EmergencyFund
                                  where d.profileID == profileID
                                  select new Asset_EmergencyFundModel
                                  {
                                      fundID = d.fundID,
                                      householdIncome = d.householdIncome,
                                      profileID = d.profileID,
                                      emergencyFund = d.emergencyFund
                                  }).SingleOrDefault();
                        if (outObj == null)
                        {
                            DefaultsEmergencyFund obj = new DefaultsEmergencyFund();
                            obj = db.DefaultsEmergencyFunds.Where(b => b.profileID == profileID).SingleOrDefault();
                            if (obj == null)
                            {
                                throw new Exception("Default Profile Values not found");
                                // = "Default Profile Values not found";
                            }
                            outObj = new Asset_EmergencyFundModel();
                            outObj.profileID = profileID;
                            outObj.emergencyFund = obj.emergencyFundValue ?? 20000;
                            outObj.householdIncome = 0;
                        }

                    }
                    else
                    {
                        return null;
                    }

                }
                catch (Exception ex)
                {
                    throw;
                }

            }
            return outObj;
        }

        public NonRetirementAssetsModel saveNonRetirementAsset(NonRetirementAssetsModel inObj, int type, long usrID)
        {
            Asset_NonRetirement outobj = new Asset_NonRetirement();
            using (var db = Connection.getConnect())
            {
                try
                {

                    if (inObj.assetID > 0)
                    {
                        outobj = db.Asset_NonRetirement.FirstOrDefault(b => b.assetID == inObj.assetID && b.profileID == inObj.profileID && b.isActive == true);
                        if (outobj != null && outobj.assetID > 0)
                        {
                            outobj.assetCatID = type;
                            outobj.assetTypeID = inObj.assetTypeID;
                            outobj.taxVsDefferedID = inObj.taxVsDefferedID;
                            outobj.amount = inObj.amount;
                            outobj.company_location = inObj.company_location ?? "";
                            outobj.description = inObj.description;
                            outobj.valueAsOf = inObj.valueAsOf;
                            outobj.isIncludeEmergencyFund = inObj.isIncludeEmergencyFund ?? false;
                            outobj.monthlyContribution = inObj.monthlyContribution ?? 0;
                            outobj.assetUpdatedBy = usrID;
                            outobj.assetUpdatedDate = System.DateTime.Now;
                            db.Entry(outobj).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                    }
                    else if (inObj.profileID > 0)
                    {
                        outobj = new Asset_NonRetirement();
                        outobj.assetCatID = type;
                        outobj.assetTypeID = inObj.assetTypeID;
                        outobj.taxVsDefferedID = inObj.taxVsDefferedID;
                        outobj.amount = inObj.amount;
                        outobj.company_location = inObj.company_location ?? "";
                        outobj.description = inObj.description;
                        outobj.valueAsOf = inObj.valueAsOf;
                        outobj.isIncludeEmergencyFund = inObj.isIncludeEmergencyFund ?? false;
                        outobj.monthlyContribution = inObj.monthlyContribution ?? 0;
                        outobj.assetUpdatedBy = usrID;
                        outobj.assetCreatedDate = System.DateTime.Now;
                        outobj.isActive = true;
                        outobj.profileID = inObj.profileID;
                        db.Asset_NonRetirement.Add(outobj);
                        db.SaveChanges();
                        inObj.assetID = outobj.assetID;
                    }

                }
                catch (Exception ex)
                {
                    throw;
                }

            }
            return inObj;
        }
        public Asset_RetirementDetailsModel saveAsset_RetirementDetails(Asset_RetirementDetailsModel inObj, int Type, long usrID)
        {
            bool isOther = false;
            Asset_RetirementDetails outobj = new Asset_RetirementDetails();
            switch (Type)
            {
                case (int)CustomEnum.AssetTypesEnum.RetirementAsset:
                    isOther = false;
                    break;
                case (int)CustomEnum.AssetTypesEnum.OtherRetirement:
                    isOther = true;
                    break;
            }
            using (var db = Connection.getConnect())
            {
                DateTime? date;
                try
                {
                    if (isOther == false)
                    {
                        if (inObj.retID > 0)
                        {
                            outobj = db.Asset_RetirementDetails.FirstOrDefault(b => b.assetID == inObj.assetID_Retirement && b.retID == inObj.retID && b.profileID == inObj.profileID && b.isActive == true && b.isOtherRetirement == isOther);
                            if (outobj != null && outobj.retID > 0)
                            {
                                if (inObj.valueAsOf == null)
                                {
                                    date = outobj.valueAsOf;
                                }
                                else
                                {
                                    date = inObj.valueAsOf;
                                }
                                outobj.assetTypeID = inObj.assetTypeID;
                                outobj.taxVsDefferedID = inObj.taxVsDefferedID;
                                outobj.amount = inObj.amount ?? 0;
                                outobj.company = inObj.company ?? "";
                                outobj.valueAsOf = date;
                                outobj.monthlyContribution = inObj.monthlyContribution ?? 0;
                                outobj.empContribution = inObj.empContribution ?? 0;
                                outobj.updatedBy = usrID;
                                outobj.updatedDate = System.DateTime.Now;
                                db.Entry(outobj).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        else if (inObj.profileID > 0 && inObj.profileID != null)
                        {
                            outobj = new Asset_RetirementDetails();
                            outobj.profileID = inObj.profileID;
                            outobj.assetID = inObj.assetID_Retirement;
                            outobj.assetTypeID = inObj.assetTypeID;
                            outobj.taxVsDefferedID = inObj.taxVsDefferedID;
                            outobj.isOtherRetirement = false;
                            outobj.notes = inObj.notes;
                            outobj.amount = inObj.amount ?? 0;
                            outobj.company = inObj.company ?? "";
                            outobj.valueAsOf = inObj.valueAsOf ?? System.DateTime.Now;
                            outobj.monthlyContribution = inObj.monthlyContribution ?? 0;
                            outobj.empContribution = inObj.empContribution ?? 0;
                            outobj.updatedBy = usrID;
                            outobj.createdDate = System.DateTime.Now;
                            outobj.isActive = true;
                            db.Asset_RetirementDetails.Add(outobj);
                            db.SaveChanges();
                            inObj.retID = outobj.retID;
                        }
                    }
                    else if (isOther == true)
                    {
                        if (inObj.retID > 0)
                        {
                            outobj = db.Asset_RetirementDetails.FirstOrDefault(b => b.retID == inObj.retID && b.profileID == inObj.profileID && b.isActive == true && b.isOtherRetirement == isOther);
                            if (outobj != null && outobj.retID > 0)
                            {
                                outobj.assetTypeID = inObj.assetTypeID;
                                outobj.taxVsDefferedID = inObj.taxVsDefferedID;
                                outobj.amount = inObj.amount ?? 0;
                                outobj.company = inObj.company ?? "";
                                outobj.valueAsOf = inObj.valueAsOf;
                                outobj.monthlyContribution = inObj.monthlyContribution ?? 0;
                                outobj.empContribution = inObj.empContribution ?? 0;
                                outobj.updatedBy = usrID;
                                outobj.updatedDate = System.DateTime.Now;
                                db.Entry(outobj).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        else if (inObj.profileID != null && inObj.profileID > 0)
                        {
                            outobj = new Asset_RetirementDetails();
                            outobj.profileID = inObj.profileID;
                            outobj.assetID = 0;
                            outobj.assetTypeID = inObj.assetTypeID;
                            outobj.taxVsDefferedID = inObj.taxVsDefferedID;
                            outobj.isOtherRetirement = true;
                            outobj.notes = inObj.notes;
                            outobj.amount = inObj.amount ?? 0;
                            outobj.company = inObj.company ?? "";
                            outobj.valueAsOf = inObj.valueAsOf;
                            outobj.monthlyContribution = inObj.monthlyContribution ?? 0;
                            outobj.empContribution = inObj.empContribution ?? 0;
                            outobj.updatedBy = usrID;
                            outobj.createdDate = System.DateTime.Now;
                            outobj.isActive = true;
                            db.Asset_RetirementDetails.Add(outobj);
                            db.SaveChanges();
                            inObj.retID = outobj.retID;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }

            }

            return inObj;

        }

        public Asset_EmergencyFundModel saveAsset_EmergencyFundDeatails(Asset_EmergencyFundModel inObj, long usrID)
        {
            Asset_EmergencyFund outObj = new Asset_EmergencyFund();

            using (var db = Connection.getConnect())
            {
                try
                {
                    if (inObj.profileID > 0)
                    {
                        outObj = db.Asset_EmergencyFund.FirstOrDefault(b => b.profileID == inObj.profileID);

                        if (outObj != null && outObj.fundID > 0)
                        {
                            outObj.householdIncome = (inObj.householdIncome > 0) ? inObj.householdIncome : outObj.householdIncome;
                            outObj.emergencyFund = (inObj.emergencyFund > 0) ? inObj.emergencyFund : outObj.emergencyFund;
                            outObj.updatedDate = System.DateTime.Now;
                            outObj.updatedBy = usrID;
                            db.Entry(outObj).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            inObj.householdIncome = outObj.emergencyFund;
                            inObj.emergencyFund = outObj.emergencyFund;
                        }
                        else
                        {
                            outObj = new Asset_EmergencyFund();
                            outObj.householdIncome = inObj.householdIncome ?? 0;
                            outObj.emergencyFund = inObj.emergencyFund ?? 0;
                            outObj.profileID = inObj.profileID;
                            outObj.createdDate = System.DateTime.Now;
                            outObj.updatedBy = usrID;
                            db.Asset_EmergencyFund.Add(outObj);
                            db.SaveChanges();
                            inObj.fundID = outObj.fundID;

                        }
                    }
                }

                catch (Exception ex)
                {
                    throw;
                }
            }
            return inObj;
        }

        public bool deleteNonRetDetails(DeleteAssetDetailsModel inObj, long userID)
        {
            bool status = false;
            Asset_NonRetirement outObj = new Asset_NonRetirement();
            using (var db = Connection.getConnect())
            {
                try
                {
                    if (inObj.profileID > 0)
                    {
                        outObj = db.Asset_NonRetirement.FirstOrDefault(b => b.profileID == inObj.profileID && b.assetID == inObj.deleteID && b.isActive == true);

                        if (outObj != null && outObj.assetID > 0)
                        {
                            outObj.isActive = false;
                            outObj.assetUpdatedDate = System.DateTime.Now;
                            outObj.assetUpdatedBy = userID;
                            db.Entry(outObj).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            status = true;
                        }
                        status = true;
                    }
                }

                catch (Exception ex)
                {
                    throw;
                }
            }
            return status;

        }

        public bool deleteRetDetails(DeleteAssetDetailsModel inObj, long userID)
        {
            bool status = false;
            Asset_RetirementDetails outObj = new Asset_RetirementDetails();
            using (var db = Connection.getConnect())
            {
                try
                {
                    if (inObj.profileID > 0)
                    {
                        outObj = db.Asset_RetirementDetails.FirstOrDefault(b => b.profileID == inObj.profileID && b.retID == inObj.deleteID && b.isActive == true);

                        if (outObj != null)
                        {
                            outObj.isActive = false;
                            outObj.updatedDate = System.DateTime.Now;
                            outObj.updatedBy = userID;
                            db.Entry(outObj).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            status = true;
                        }
                        status = true;
                    }
                }

                catch (Exception ex)
                {
                    throw;
                }
            }
            return status;

        }

    }
}
