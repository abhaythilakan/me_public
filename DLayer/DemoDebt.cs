﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLayer
{
    public class DemoDebt
    {
        public List<DebtDisappearList> getDisappearList(long profileID, decimal extraPay)
        {
            List<DebtDisappearList> mdl = new List<DebtDisappearList>();
            try
            {
                using (var db = Connection.getConnect())
                {
                    mdl = db.Debts.Where(b => b.profileID == profileID && b.inclElmPlan == true && b.isActive == true && b.projectedPayOffDate > DateTime.Now).OrderBy(b => b.projectedPayOffDate)
                        .Select(b => new DebtDisappearList()
                        {
                            debtID = b.debtID,
                            debtCreditor = b.creditor,
                            debtCatId = b.debtCatId ?? 0,
                            initialMinimumPayment = b.minPay ?? 0,
                            debtAmt = b.amount ?? 0
                        }).ToList();

                    var yearlyDebtRec = getDebtEliminationSystemAutoEstimation(profileID, extraPay);
                    if (yearlyDebtRec != null && mdl != null && mdl.Count() > 0)
                    {
                        for (int i = 0; i < mdl.Count(); i++)
                        {
                            mdl[i].debtCatName = getdebtCatName(mdl[i].debtCatId);
                            mdl[i].debtAmt = mdl.Single(b => b.debtID == mdl[i].debtID).debtAmt;
                            //var iin = yearlyDebtRec.Where(b => b.debtID == mdl[i].debtID).FirstOrDefault().accelDate;
                            if (yearlyDebtRec.Where(b => b.debtID == mdl[i].debtID && b.accelDate != null && b.accelDate > DateTime.MinValue).Any())
                            {
                                mdl[i].accelDate = yearlyDebtRec.Where(b => b.debtID == mdl[i].debtID && b.accelDate != null && b.accelDate > DateTime.MinValue).FirstOrDefault().accelDate;
                                mdl[i].accelDateString = Convert.ToDateTime(mdl[i].accelDate) == DateTime.MinValue ? "" : Convert.ToDateTime(mdl[i].accelDate).ToString("MMM yyyy");
                                mdl[i].accelPayment = yearlyDebtRec.Where(b => b.debtID == mdl[i].debtID && b.accelDate != null && b.accelDate > DateTime.MinValue).FirstOrDefault().accelPayment;
                            }
                            else
                            {
                                mdl[i].accelDate = null;
                                mdl[i].accelDateString = "";
                                mdl[i].accelPayment = 0;
                            }
                            mdl[i].newPayment = mdl[i].initialMinimumPayment + mdl[i].accelPayment;
                            if (yearlyDebtRec.Where(b => b.debtID == mdl[i].debtID && b.projPayOff != null && b.projPayOff > DateTime.MinValue).Any())
                            {
                                mdl[i].projPayOff = yearlyDebtRec.Where(b => b.debtID == mdl[i].debtID && b.projPayOff != null && b.projPayOff > DateTime.MinValue).FirstOrDefault().projPayOff;
                                mdl[i].projPayOffString = Convert.ToDateTime(mdl[i].projPayOff) == DateTime.MinValue ? "" : Convert.ToDateTime(mdl[i].projPayOff).ToString("MMM yyyy");
                            }
                            mdl[i].projInterestPaid = yearlyDebtRec.Where(b => b.debtID == mdl[i].debtID).Sum(b => b.interestPaid);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return mdl;
        }

        public string getdebtCatName(int catID)
        {
            string name = "";
            try
            {
                using (var db = Connection.getConnect())
                {
                    name = db.DebtCats.Where(b => b.debtCatId == catID).FirstOrDefault().catName;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return name;
        }






        public List<DebtDisappearYearlyD2> getDebtEliminationSystemAutoEstimation(long profileID, decimal? totalExtraPayArg = 0)
        {
            List<DebtDisappearYearlyD2> model = new List<DebtDisappearYearlyD2>();
            try
            {
                long ProfileID = profileID;
                using (var db = Connection.getConnect())
                {
                    List<Debt> dt = new List<Debt>();

                    dt = db.Debts.Where(b => b.profileID == ProfileID && b.inclElmPlan == true && b.isActive == true && b.projectedPayOffDate > DateTime.Now).OrderBy(b => b.projectedPayOffDate).ToList();
                    decimal totalExtraPay = 0;
                    if (totalExtraPayArg > 0)
                    {
                        totalExtraPay = (decimal)totalExtraPayArg;
                    }
                    else
                    {
                        totalExtraPay = Convert.ToDecimal(dt.Sum(b => b.extraPay));
                    }

                    decimal additionalAccelAmt = 0;
                    additionalAccelAmt = totalExtraPay;
                    if (dt != null)
                    {
                        if (dt.Count() > 0)
                        {
                            DateTime? additionalAccelDate = null;
                            decimal yearlyPendingBalanceAmt = 0;
                            decimal acceleratedPayAmt_ForNxtDebt = 0;
                            DateTime? acceleratedPayStartDate_ForNxtDebt = null;
                            decimal balanceAmt = 0;
                            int ii = 0;
                            DateTime startDatetime = dt.OrderBy(b => b.Balanceasof).Take(1).FirstOrDefault().Balanceasof ?? DateTime.Now;

                            foreach (var itm in dt)
                            {
                                ii++;
                                balanceAmt = Convert.ToDecimal(itm.amount);

                                if (startDatetime.Year <= Convert.ToDateTime(itm.Balanceasof).Year)
                                {
                                    DateTime startDate = Convert.ToDateTime(itm.Balanceasof);
                                    DateTime startDate1 = Convert.ToDateTime(itm.Balanceasof);

                                    if (ii == 1 && additionalAccelAmt > 0)
                                    {
                                        acceleratedPayStartDate_ForNxtDebt = itm.Balanceasof;
                                        additionalAccelDate = acceleratedPayStartDate_ForNxtDebt;
                                    }
                                    yearlyPendingBalanceAmt = Convert.ToDecimal(itm.amount);
                                    //additionalAccelAmt += totalExtraPay;

                                    decimal acceleratedPayAmt_ForNxtDebt1 = 0;
                                    DateTime? acceleratedPayStartDate_ForNxtDebt1 = null;
                                    if (acceleratedPayAmt_ForNxtDebt > 0)
                                    {
                                        acceleratedPayAmt_ForNxtDebt1 = acceleratedPayAmt_ForNxtDebt;
                                        acceleratedPayStartDate_ForNxtDebt1 = acceleratedPayStartDate_ForNxtDebt;
                                    }

                                    for (int year = startDate1.Year; yearlyPendingBalanceAmt > 0; year++)
                                    {
                                        if (yearlyPendingBalanceAmt > 0)
                                        {
                                            balanceAmt = yearlyPendingBalanceAmt;
                                            yearlyPendingBalanceAmt = 0;
                                            if (acceleratedPayAmt_ForNxtDebt1 > 0)
                                            {
                                                if (Convert.ToDateTime(acceleratedPayStartDate_ForNxtDebt1).Year == year)
                                                {
                                                    additionalAccelAmt = acceleratedPayAmt_ForNxtDebt1;
                                                    additionalAccelDate = acceleratedPayStartDate_ForNxtDebt1;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (acceleratedPayAmt_ForNxtDebt1 > 0)
                                            {
                                                if (Convert.ToDateTime(acceleratedPayStartDate_ForNxtDebt1).Year == year)
                                                {
                                                    additionalAccelAmt = acceleratedPayAmt_ForNxtDebt1;
                                                    additionalAccelDate = acceleratedPayStartDate_ForNxtDebt1;
                                                }
                                            }
                                        }
                                        //if (acceleratedPayAmt_ForNxtDebt > 0)
                                        //{
                                        //    if (Convert.ToDateTime(acceleratedPayStartDate_ForNxtDebt).Year == year)
                                        //    {
                                        //        additionalAccelAmt = acceleratedPayAmt_ForNxtDebt;
                                        //        additionalAccelDate = acceleratedPayStartDate_ForNxtDebt;
                                        //    }
                                        //}
                                        if (itm.debtCatId == (int)CustomEnum.debtCatEnum.Mortage)
                                        {
                                            getMortageAmmo1(balanceAmt, ref startDate, additionalAccelAmt, Convert.ToDateTime(additionalAccelDate), out yearlyPendingBalanceAmt
                                                , out acceleratedPayAmt_ForNxtDebt, out acceleratedPayStartDate_ForNxtDebt, itm, model);
                                        }
                                        else if (itm.debtCatId == (int)CustomEnum.debtCatEnum.Fixed)
                                        {
                                            getFixedAmmo1(balanceAmt, ref startDate, additionalAccelAmt, Convert.ToDateTime(additionalAccelDate), out yearlyPendingBalanceAmt
                                            , out acceleratedPayAmt_ForNxtDebt, out acceleratedPayStartDate_ForNxtDebt, itm, model);
                                        }
                                        else if (itm.debtCatId == (int)CustomEnum.debtCatEnum.Revolving)
                                        {
                                            getRevolvingAmmo1(balanceAmt, ref startDate, additionalAccelAmt, Convert.ToDateTime(additionalAccelDate), out yearlyPendingBalanceAmt
                                            , out acceleratedPayAmt_ForNxtDebt, out acceleratedPayStartDate_ForNxtDebt, itm, model);
                                        }
                                        //additionalAccelAmt = 0;
                                        additionalAccelDate = null;
                                    }
                                    additionalAccelAmt = 0;
                                }
                            }
                        }
                    }
                }
                model = model.OrderBy(b => b.yearName).ToList();
            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }

        public List<DebtDisappearYearlyD2> getMortageAmmo1(decimal balanceAmt, ref DateTime startDate, decimal additionalAccelAmt, DateTime additionalAccelDate
            , out decimal yearlyPendingBalanceAmt, out decimal acceleratedPayAmt_ForNxtDebt, out DateTime? acceleratedPayStartDate_ForNxtDebt
            , Debt deb, List<DebtDisappearYearlyD2> model)
        {
            //List<DebtDisappearYearlyD2> model = new List<DebtDisappearYearlyD2>();
            try
            {
                decimal mnthEndPayBalanceAmt = balanceAmt;
                acceleratedPayAmt_ForNxtDebt = 0; //return
                acceleratedPayStartDate_ForNxtDebt = null; //return
                yearlyPendingBalanceAmt = 0; //return
                decimal interestAmt = 0;
                int currentYear = startDate.Year;
                DateTime incStartDate = startDate;
                decimal monthlyEMI = 0;

                using (var db = Connection.getConnect())
                {
                    DebtDisappearYearlyD2 data = new DebtDisappearYearlyD2();
                    data.beginingBalance = mnthEndPayBalanceAmt;
                    data.initialMinimumPayment = Convert.ToDecimal(deb.minPay);
                    data.accelPayment = additionalAccelAmt;
                    data.accelDate = additionalAccelDate == DateTime.MinValue ? (DateTime?)null : additionalAccelDate;
                    data.accelDateString = additionalAccelDate == DateTime.MinValue ? "" : additionalAccelDate.ToString("MMM yyyy");
                    data.debtCreditor = deb.creditor;
                    data.yearName = currentYear;
                    data.debtID = deb.debtID;

                    decimal iRateMonthly = 0;
                    if (deb.rate > 0)
                    {
                        iRateMonthly = Convert.ToDecimal(deb.rate) / 1200;
                    }

                    if (deb.debtCatId > 0)
                    {
                        data.debtCatId = Convert.ToInt64(deb.debtCatId);
                        data.debtCatName = db.DebtCats.Where(b => b.debtCatId == deb.debtCatId).FirstOrDefault().catName;
                    }

                    if (data.mnthList == null)
                    {
                        data.mnthList = new List<DebtDisappearMnthsD2>();
                    }
                    decimal interestPaid = 0;
                    for (int i = 1; i <= 12; i++)
                    {
                        if (mnthEndPayBalanceAmt > 0)
                        {
                            if ((incStartDate.Month <= i && incStartDate.Year == startDate.Year) || (i > 0 && incStartDate.Year < startDate.Year))
                            {
                                interestAmt = Math.Round(mnthEndPayBalanceAmt * iRateMonthly, 2);
                                interestPaid = interestPaid + interestAmt;
                                if ((additionalAccelDate.Month <= i && additionalAccelDate.Year == currentYear) || (additionalAccelDate == DateTime.MinValue))   //*****************************
                                    monthlyEMI = Convert.ToDecimal(deb.minPay) + additionalAccelAmt;// + interestAmt
                                else
                                    monthlyEMI = Convert.ToDecimal(deb.minPay);
                                if ((monthlyEMI - interestAmt) <= mnthEndPayBalanceAmt)
                                {
                                    mnthEndPayBalanceAmt = mnthEndPayBalanceAmt - (monthlyEMI - interestAmt);
                                    if (i == 12)
                                    {
                                        yearlyPendingBalanceAmt = mnthEndPayBalanceAmt;
                                        acceleratedPayAmt_ForNxtDebt = additionalAccelAmt;
                                        acceleratedPayStartDate_ForNxtDebt = null;
                                    }
                                }
                                else
                                {
                                    data.projPayOff = startDate;
                                    acceleratedPayAmt_ForNxtDebt = monthlyEMI;
                                    acceleratedPayStartDate_ForNxtDebt = startDate.AddMonths(1);
                                    decimal lastEMIBalance = (monthlyEMI - interestAmt) - mnthEndPayBalanceAmt;
                                    monthlyEMI = (monthlyEMI - lastEMIBalance) + interestAmt;
                                    mnthEndPayBalanceAmt = 0;
                                    //monthlyEMI = mnthEndPayBalanceAmt + interestAmt;
                                }
                                if (startDate.Year >= DateTime.Now.Year)
                                {
                                    DebtDisappearMnthsD2 md = new DebtDisappearMnthsD2();
                                    md.mnthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i);
                                    md.mnthNo = i;
                                    md.amtEMI = monthlyEMI;
                                    md.Comment = "Payed";
                                    data.mnthList.Add(md);
                                }
                                startDate = startDate.AddMonths(1);
                            }
                            else
                            {
                                if (startDate.Year >= DateTime.Now.Year)
                                {
                                    DebtDisappearMnthsD2 md = new DebtDisappearMnthsD2();
                                    md.mnthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i);
                                    md.mnthNo = i;
                                    md.amtEMI = 0;
                                    md.Comment = "Not Started";
                                    data.mnthList.Add(md);
                                }
                                //startDate = startDate.AddMonths(1);
                            }
                        }
                        else
                        {
                            if (startDate.Year >= DateTime.Now.Year)
                            {
                                DebtDisappearMnthsD2 md = new DebtDisappearMnthsD2();
                                md.mnthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i);
                                md.mnthNo = i;
                                md.amtEMI = 0;
                                md.Comment = "Pay Off";
                                data.mnthList.Add(md);
                            }
                            startDate = startDate.AddMonths(1);
                        }
                    }
                    data.interestPaid = interestPaid;
                    if (yearlyPendingBalanceAmt > 0)
                    {
                        data.endingBalance = yearlyPendingBalanceAmt;
                    }
                    model.Add(data);
                }

            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }

        public List<DebtDisappearYearlyD2> getFixedAmmo1(decimal balanceAmt, ref DateTime startDate, decimal additionalAccelAmt, DateTime additionalAccelDate
            , out decimal yearlyPendingBalanceAmt, out decimal acceleratedPayAmt_ForNxtDebt, out DateTime? acceleratedPayStartDate_ForNxtDebt
            , Debt deb, List<DebtDisappearYearlyD2> model)
        {
            //List<DebtDisappearYearlyD2> model = new List<DebtDisappearYearlyD2>();
            try
            {
                decimal mnthEndPayBalanceAmt = balanceAmt;
                acceleratedPayAmt_ForNxtDebt = 0; //return
                acceleratedPayStartDate_ForNxtDebt = null; //return
                yearlyPendingBalanceAmt = 0; //return
                decimal interestAmt = 0;
                int currentYear = startDate.Year;
                DateTime incStartDate = startDate;
                decimal monthlyEMI = 0;

                using (var db = Connection.getConnect())
                {
                    DebtDisappearYearlyD2 data = new DebtDisappearYearlyD2();
                    data.beginingBalance = mnthEndPayBalanceAmt;
                    data.initialMinimumPayment = Convert.ToDecimal(deb.minPay);
                    data.accelPayment = additionalAccelAmt;
                    data.accelDate = additionalAccelDate == DateTime.MinValue ? (DateTime?)null : additionalAccelDate;
                    data.accelDateString = additionalAccelDate == DateTime.MinValue ? "" : additionalAccelDate.ToString("MMM yyyy");
                    data.debtCreditor = deb.creditor;
                    data.yearName = currentYear;
                    data.debtID = deb.debtID;

                    bool isFromIntodate = false;

                    decimal iRateMonthly = 0;
                    decimal iIntroRateMonthly = 0;
                    if (deb.debtCatId > 0)
                    {
                        data.debtCatId = Convert.ToInt64(deb.debtCatId);
                        data.debtCatName = db.DebtCats.Where(b => b.debtCatId == deb.debtCatId).FirstOrDefault().catName;
                    }
                    if (deb.rate > 0)
                    {
                        iRateMonthly = Convert.ToDecimal(deb.rate) / 1200;
                    }
                    if (deb.introRate > 0)
                    {
                        iIntroRateMonthly = Convert.ToDecimal(deb.introRate) / 1200;
                    }
                    decimal interestActual = 0;
                    if (data.mnthList == null)
                    {
                        data.mnthList = new List<DebtDisappearMnthsD2>();
                    }
                    decimal interestPaid = 0;
                    for (int i = 1; i <= 12; i++)
                    {
                        if (mnthEndPayBalanceAmt > 0)
                        {
                            DateTime introEndDate = Convert.ToDateTime(deb.introDate);
                            if (startDate != DateTime.MinValue && introEndDate != DateTime.MinValue && String.Format("{0:MMM-yyyy}", startDate) == String.Format("{0:MMM-yyyy}", introEndDate.AddMonths(-1)))
                            {
                                interestActual = iRateMonthly;
                                isFromIntodate = true;
                            }
                            else
                            {
                                if (!(isFromIntodate) && iIntroRateMonthly > 0)
                                {
                                    interestActual = iIntroRateMonthly;   //Console.WriteLine("rate 2 selected :"+irate3); 
                                }
                                else
                                {
                                    interestActual = iRateMonthly;   //  Console.WriteLine("rate 1 selected.second :"+irate2);                                  }  
                                }
                            }
                            //if (mnthEndPayBalanceAmt > 0)
                            //{
                            if ((incStartDate.Month <= i && incStartDate.Year == startDate.Year) || (i > 0 && incStartDate.Year < startDate.Year))
                            {
                                interestAmt = Math.Round(mnthEndPayBalanceAmt * interestActual, 2);
                                interestPaid = interestPaid + interestAmt;
                                if ((additionalAccelDate.Month <= i && additionalAccelDate.Year == currentYear) || (additionalAccelDate == DateTime.MinValue))
                                    monthlyEMI = Convert.ToDecimal(deb.minPay) + additionalAccelAmt;//+ interestAmt
                                else
                                    monthlyEMI = Convert.ToDecimal(deb.minPay);
                                if ((monthlyEMI - interestAmt) <= mnthEndPayBalanceAmt)
                                {
                                    mnthEndPayBalanceAmt = mnthEndPayBalanceAmt - (monthlyEMI - interestAmt);
                                    if (i == 12)
                                    {
                                        yearlyPendingBalanceAmt = mnthEndPayBalanceAmt;
                                        acceleratedPayAmt_ForNxtDebt = additionalAccelAmt;
                                        acceleratedPayStartDate_ForNxtDebt = null;
                                    }
                                }
                                else
                                {
                                    data.projPayOff = startDate;
                                    acceleratedPayAmt_ForNxtDebt = monthlyEMI;
                                    acceleratedPayStartDate_ForNxtDebt = startDate.AddMonths(1);
                                    decimal lastEMIBalance = (monthlyEMI - interestAmt) - mnthEndPayBalanceAmt;
                                    monthlyEMI = (monthlyEMI - lastEMIBalance) + interestAmt;
                                    mnthEndPayBalanceAmt = 0;
                                    //monthlyEMI = mnthEndPayBalanceAmt + interestAmt;
                                }
                                if (startDate.Year >= DateTime.Now.Year)
                                {
                                    DebtDisappearMnthsD2 md = new DebtDisappearMnthsD2();
                                    md.mnthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i);
                                    md.mnthNo = i;
                                    md.amtEMI = monthlyEMI;
                                    md.Comment = "Payed";
                                    data.mnthList.Add(md);
                                }
                                startDate = startDate.AddMonths(1);
                            }
                            else
                            {
                                if (startDate.Year >= DateTime.Now.Year)
                                {
                                    DebtDisappearMnthsD2 md = new DebtDisappearMnthsD2();
                                    md.mnthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i);
                                    md.mnthNo = i;
                                    md.amtEMI = 0;
                                    md.Comment = "Not Started";
                                    data.mnthList.Add(md);
                                }
                                //startDate = startDate.AddMonths(1);
                            }
                        }
                        else
                        {
                            if (startDate.Year >= DateTime.Now.Year)
                            {
                                DebtDisappearMnthsD2 md = new DebtDisappearMnthsD2();
                                md.mnthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i);
                                md.mnthNo = i;
                                md.amtEMI = 0;
                                md.Comment = "Pay Off";
                                data.mnthList.Add(md);
                            }
                            startDate = startDate.AddMonths(1);
                        }
                    }
                    data.interestPaid = interestPaid;
                    if (yearlyPendingBalanceAmt > 0)
                    {
                        data.endingBalance = yearlyPendingBalanceAmt;
                    }
                    model.Add(data);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }

        public List<DebtDisappearYearlyD2> getRevolvingAmmo1(decimal balanceAmt, ref DateTime startDate, decimal additionalAccelAmt, DateTime additionalAccelDate
            , out decimal yearlyPendingBalanceAmt, out decimal acceleratedPayAmt_ForNxtDebt, out DateTime? acceleratedPayStartDate_ForNxtDebt
            , Debt deb, List<DebtDisappearYearlyD2> model)
        {
            //List<DebtDisappearYearlyD2> model = new List<DebtDisappearYearlyD2>();
            try
            {
                decimal mnthEndPayBalanceAmt = balanceAmt;
                acceleratedPayAmt_ForNxtDebt = 0; //return
                acceleratedPayStartDate_ForNxtDebt = null; //return
                yearlyPendingBalanceAmt = 0; //return
                decimal interestAmt = 0;
                int currentYear = startDate.Year;
                DateTime incStartDate = startDate;

                decimal monthlyEMI = 0;

                using (var db = Connection.getConnect())
                {
                    DebtDisappearYearlyD2 data = new DebtDisappearYearlyD2();
                    data.beginingBalance = mnthEndPayBalanceAmt;

                    var defaults = db.DefaultsDebts.Where(b => b.profileID == deb.profileID).FirstOrDefault();
                    if (defaults != null)
                        data.initialMinimumPayment = Convert.ToDecimal(deb.minPay) > 0 ? Convert.ToDecimal(deb.minPay) : (Convert.ToDecimal(defaults.minPay));
                    else
                        data.initialMinimumPayment = Convert.ToDecimal(deb.minPay);

                    data.accelPayment = additionalAccelAmt;
                    data.accelDate = additionalAccelDate == DateTime.MinValue ? (DateTime?)null : additionalAccelDate;
                    data.accelDateString = additionalAccelDate == DateTime.MinValue ? "" : additionalAccelDate.ToString("MMM yyyy");
                    data.debtCreditor = deb.creditor;
                    data.yearName = currentYear;
                    data.debtID = deb.debtID;

                    bool isFromIntodate = false;

                    decimal iRateMonthly = 0;
                    decimal iIntroRateMonthly = 0;
                    if (deb.debtCatId > 0)
                    {
                        data.debtCatId = Convert.ToInt64(deb.debtCatId);
                        data.debtCatName = db.DebtCats.Where(b => b.debtCatId == deb.debtCatId).FirstOrDefault().catName;
                    }
                    if (deb.rate > 0)
                    {
                        iRateMonthly = Convert.ToDecimal(deb.rate) / 1200;
                    }
                    if (deb.introRate > 0)
                    {
                        iIntroRateMonthly = Convert.ToDecimal(deb.introRate) / 1200;
                    }
                    decimal interestActual = 0;
                    if (data.mnthList == null)
                    {
                        data.mnthList = new List<DebtDisappearMnthsD2>();
                    }
                    decimal interestPaid = 0;
                    for (int i = 1; i <= 12; i++)
                    {

                        if (mnthEndPayBalanceAmt > 0)
                        {
                            DateTime introEndDate = Convert.ToDateTime(deb.introDate);
                            if (startDate != DateTime.MinValue && introEndDate != DateTime.MinValue && String.Format("{0:MMM-yyyy}", startDate) == String.Format("{0:MMM-yyyy}", introEndDate.AddMonths(-1)))
                            {
                                interestActual = iRateMonthly;
                                isFromIntodate = true;
                            }
                            else
                            {
                                if (!(isFromIntodate) && iIntroRateMonthly > 0)
                                {
                                    interestActual = iIntroRateMonthly;   //Console.WriteLine("rate 2 selected :"+irate3); 
                                }
                                else
                                {
                                    interestActual = iRateMonthly;   //  Console.WriteLine("rate 1 selected.second :"+irate2);                                  }  
                                }
                            }
                            decimal minMonthPay = 0;
                            if (defaults != null)
                                minMonthPay = Convert.ToDecimal(deb.minPay) > 0 ? Convert.ToDecimal(deb.minPay) : (Convert.ToDecimal(defaults.minPay));
                            else
                                minMonthPay = Convert.ToDecimal(deb.minPay);

                            decimal cardPayPercent = 0;
                            if (defaults != null)
                                cardPayPercent = Convert.ToDecimal(deb.unPaidBalance) > 0 ? Convert.ToDecimal(deb.unPaidBalance) : (Convert.ToDecimal(defaults.unpaidPercentage));
                            else
                                cardPayPercent = Convert.ToDecimal(deb.unPaidBalance);
                            decimal cardPaymonthly = (Convert.ToDecimal(mnthEndPayBalanceAmt) * (cardPayPercent)) / 100;

                            if (cardPaymonthly <= minMonthPay)
                            {
                                //minMonthPay = minMonthPay;
                            }
                            else
                            {
                                minMonthPay = cardPaymonthly;
                            }
                            //if (mnthEndPayBalanceAmt > 0)
                            //{
                            if ((incStartDate.Month <= i && incStartDate.Year == startDate.Year) || (i > 0 && incStartDate.Year < startDate.Year))
                            {
                                interestAmt = Math.Round(mnthEndPayBalanceAmt * interestActual, 2);
                                interestPaid = interestPaid + interestAmt;
                                if ((additionalAccelDate.Month <= i && additionalAccelDate.Year == currentYear) || (additionalAccelDate == DateTime.MinValue))
                                    monthlyEMI = minMonthPay + additionalAccelAmt;// + interestAmt
                                else
                                    monthlyEMI = minMonthPay;
                                if ((monthlyEMI - interestAmt) <= mnthEndPayBalanceAmt)
                                {
                                    mnthEndPayBalanceAmt = mnthEndPayBalanceAmt - (monthlyEMI - interestAmt);
                                    if (i == 12)
                                    {
                                        yearlyPendingBalanceAmt = mnthEndPayBalanceAmt;
                                        acceleratedPayAmt_ForNxtDebt = additionalAccelAmt;
                                        acceleratedPayStartDate_ForNxtDebt = null;
                                    }
                                }
                                else
                                {
                                    data.projPayOff = startDate;
                                    acceleratedPayAmt_ForNxtDebt = monthlyEMI;
                                    acceleratedPayStartDate_ForNxtDebt = startDate.AddMonths(1);
                                    decimal lastEMIBalance = (monthlyEMI - interestAmt) - mnthEndPayBalanceAmt;
                                    monthlyEMI = (monthlyEMI - lastEMIBalance) + interestAmt;
                                    mnthEndPayBalanceAmt = 0;
                                    //monthlyEMI = mnthEndPayBalanceAmt + interestAmt;
                                }
                                if (startDate.Year >= DateTime.Now.Year)
                                {
                                    DebtDisappearMnthsD2 md = new DebtDisappearMnthsD2();
                                    md.mnthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i);
                                    md.mnthNo = i;
                                    md.amtEMI = monthlyEMI;
                                    md.Comment = "Payed";
                                    data.mnthList.Add(md);
                                }
                                startDate = startDate.AddMonths(1);
                            }
                            else
                            {
                                if (startDate.Year >= DateTime.Now.Year)
                                {
                                    DebtDisappearMnthsD2 md = new DebtDisappearMnthsD2();
                                    md.mnthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i);
                                    md.mnthNo = i;
                                    md.amtEMI = 0;
                                    md.Comment = "Not Started";
                                    data.mnthList.Add(md);
                                }
                                //startDate = startDate.AddMonths(1);
                            }
                        }
                        else
                        {
                            if (startDate.Year >= DateTime.Now.Year)
                            {
                                DebtDisappearMnthsD2 md = new DebtDisappearMnthsD2();
                                md.mnthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i);
                                md.mnthNo = i;
                                md.amtEMI = 0;
                                md.Comment = "Pay Off";
                                data.mnthList.Add(md);
                            }
                            startDate = startDate.AddMonths(1);
                        }
                    }
                    data.interestPaid = interestPaid;
                    if (yearlyPendingBalanceAmt > 0)
                    {
                        data.endingBalance = yearlyPendingBalanceAmt;
                    }
                    model.Add(data);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }

        public DebtAmortizationModel getMortageAmmo(DebtProjectionInput model, Debt deb, DebtAmortizationModel data)
        {
            try
            {
                using (var db = Connection.getConnect())
                {

                    DateTime DateStart = Convert.ToDateTime(deb.Balanceasof);
                    List<DebtProjectionModel> md = new List<DebtProjectionModel>();
                    decimal irateMonthly = 0;
                    if (model.interestRate > 0)
                    {
                        irateMonthly = model.interestRate / 1200;
                    }
                    decimal totalInterestI = 0, totalPrincipalI = 0, interestI, principalI, balanceI = model.startBalance, iI = 0;
                    DateTime? payOffDate = null;

                    while (totalPrincipalI != model.startBalance)
                    {
                        DebtProjectionModel dp = new DebtProjectionModel();
                        interestI = Math.Round(balanceI * irateMonthly, 2);
                        if (model.isOneTimePay)
                        {
                            if (iI == 0)
                            {
                                principalI = Math.Round(((Convert.ToDecimal(deb.minPay) + Convert.ToDecimal(model.additionalPayOneTime)) - interestI), 2);
                                model.payPrincInterstPerMnth = Convert.ToDecimal(deb.minPay) + Convert.ToDecimal(model.additionalPayOneTime);
                                if (model.isAllTimePay)
                                {
                                    principalI = principalI + model.additionalPayAllTime;
                                    model.payPrincInterstPerMnth = model.payPrincInterstPerMnth + Convert.ToDecimal(model.additionalPayAllTime);
                                }
                            }
                            else
                            {
                                principalI = Math.Round((Convert.ToDecimal(deb.minPay) - interestI), 2);
                                model.payPrincInterstPerMnth = Convert.ToDecimal(deb.minPay);
                            }
                        }
                        else
                        {
                            principalI = Math.Round(((Convert.ToDecimal(deb.minPay) + Convert.ToDecimal(model.additionalPayAllTime)) - interestI), 2);
                            model.payPrincInterstPerMnth = Convert.ToDecimal(deb.minPay) + Convert.ToDecimal(model.additionalPayAllTime);
                        }
                        totalPrincipalI = Math.Round(totalPrincipalI + principalI, 2);
                        totalInterestI = Math.Round(totalInterestI + interestI, 2);
                        balanceI = Math.Round((model.startBalance - totalPrincipalI), 2);

                        if (balanceI < model.payPrincInterstPerMnth)
                        {
                            DateStart = DateStart.AddMonths(1);
                            iI++;
                            dp.Date = DateStart;
                            dp.dateString = DateStart.ToString("MMM, yyyy");
                            dp.principal = principalI;
                            dp.interest = interestI;
                            dp.totalPrincipal = totalPrincipalI;
                            dp.totalInterest = totalInterestI;
                            dp.balance = balanceI;
                            dp.totalPayment = dp.totalPrincipal + dp.totalInterest;
                            if (model.isOneTimePay)
                            {
                                dp.additionalPay = iI == 1 ? Convert.ToDecimal(model.additionalPayOneTime) : 0;
                                model.totalAdditional = iI == 1 ? (model.totalAdditional + Convert.ToDecimal(model.additionalPayOneTime)) : Convert.ToDecimal(model.additionalPayOneTime);
                                if (model.isAllTimePay)
                                {
                                    dp.additionalPay = (dp.additionalPay + Convert.ToDecimal(model.additionalPayAllTime));
                                    model.totalAdditional = (model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime));
                                }
                            }
                            else
                            {
                                dp.additionalPay = Convert.ToDecimal(model.additionalPayAllTime);
                                model.totalAdditional = model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime);
                            }
                            md.Add(dp);

                            principalI = balanceI;
                            totalPrincipalI = totalPrincipalI + principalI;
                            interestI = Math.Round(balanceI * irateMonthly, 2);
                            totalInterestI = totalInterestI + interestI;
                            balanceI = Math.Round((model.startBalance - totalPrincipalI), 2);

                            dp = new DebtProjectionModel();
                            DateStart = DateStart.AddMonths(1);
                            dp.Date = DateStart;
                            dp.dateString = DateStart.ToString("MMM, yyyy");
                            dp.principal = principalI;
                            dp.interest = interestI;
                            dp.totalPrincipal = totalPrincipalI;
                            dp.totalInterest = totalInterestI;
                            dp.balance = balanceI;
                            dp.totalPayment = dp.totalPrincipal + dp.totalInterest;
                            if (model.isOneTimePay)
                            {
                                dp.additionalPay = iI == 1 ? Convert.ToDecimal(model.additionalPayOneTime) : 0;
                                model.totalAdditional = iI == 1 ? (model.totalAdditional + Convert.ToDecimal(model.additionalPayOneTime)) : Convert.ToDecimal(model.additionalPayOneTime);
                                if (model.isAllTimePay)
                                {
                                    dp.additionalPay = (dp.additionalPay + Convert.ToDecimal(model.additionalPayAllTime));
                                    model.totalAdditional = (model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime));
                                }
                            }
                            else
                            {
                                dp.additionalPay = Convert.ToDecimal(model.additionalPayAllTime);
                                model.totalAdditional = model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime);
                            }
                            md.Add(dp);
                            payOffDate = DateStart;
                        }
                        else
                        {
                            iI++;
                            DateStart = DateStart.AddMonths(1);
                            dp.Date = DateStart;
                            dp.dateString = DateStart.ToString("MMM, yyyy");
                            dp.principal = principalI;
                            dp.interest = interestI;
                            dp.totalPrincipal = totalPrincipalI;
                            dp.totalInterest = totalInterestI;
                            dp.balance = balanceI;
                            dp.totalPayment = dp.totalPrincipal + dp.totalInterest;
                            if (model.isOneTimePay)
                            {
                                dp.additionalPay = iI == 1 ? Convert.ToDecimal(model.additionalPayOneTime) : 0;
                                model.totalAdditional = iI == 1 ? (model.totalAdditional + Convert.ToDecimal(model.additionalPayOneTime)) : Convert.ToDecimal(model.additionalPayOneTime);
                                if (model.isAllTimePay)
                                {
                                    dp.additionalPay = (dp.additionalPay + Convert.ToDecimal(model.additionalPayAllTime));
                                    model.totalAdditional = (model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime));
                                }
                            }
                            else
                            {
                                dp.additionalPay = Convert.ToDecimal(model.additionalPayAllTime);
                                model.totalAdditional = model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime);
                            }
                            md.Add(dp);
                        }

                    }

                    model.noOfPay = md.Count();
                    model.payOff = Convert.ToDateTime(payOffDate).ToString("MMM, yyyy");
                    model.payOffDate = Convert.ToDateTime(payOffDate);
                    model.totalPay = md.LastOrDefault().totalPrincipal + md.LastOrDefault().totalInterest;
                    model.totalInterest = md.LastOrDefault().totalInterest;
                    data.DebtProjectionInput = model;
                    if (model.isYearlychart)
                    {
                        data.DebtProjectionList = (from m in md
                                                   group m by m.Date.Year into g
                                                   select new DebtProjectionModel
                                                   {
                                                       principal = g.Sum(b => b.principal),
                                                       interest = g.Sum(b => b.interest),
                                                       totalPrincipal = g.LastOrDefault().totalPrincipal,
                                                       totalInterest = g.LastOrDefault().totalInterest,
                                                       totalPayment = g.LastOrDefault().totalPayment,
                                                       additionalPay = g.Sum(b => b.additionalPay),
                                                       balance = g.LastOrDefault().balance,
                                                       Period = g.Count() > 1 ? g.FirstOrDefault().Date.ToString("MMM") + " - " + g.LastOrDefault().Date.ToString("MMM, yyyy") : g.LastOrDefault().Date.ToString("MMM, yyyy")
                                                   }).ToList();
                        model.noOfPay = data.DebtProjectionList.Count();
                    }
                    else
                    {
                        data.DebtProjectionList = md;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return data;
        }
    }
}
