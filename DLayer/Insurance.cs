﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLayer
{
    public class Insurance
    {
        public FullInsurance GetFullInsurance(long id)
        {
            FullInsurance model = new FullInsurance();
            model.OtherInsurance = GetOtherInsurance(id);
            model.PrimaryInsurance = GetLifeInsurance(id, "primary");
            model.SpouseInsurance = GetLifeInsurance(id, "spouse");
            model.ChildInsurance = GetChildInsurance(id, 0);
            return model;
        }



        public FullLifeInsurance GetLifeInsurance(long id, string type)
        {
            FullLifeInsurance model = new FullLifeInsurance();
            try
            {

                using (var db = Connection.getConnect())
                {
                    bool isPrimary = (type == "primary") ? true : false;
                    var Insurance = db.lifeInsurances.Where(x => x.profileID == id && x.isActive == true).ToList();
                    var personInfo = db.Persons.Where(x => x.profileID == id && x.isActive==true).ToList();
                    model.personInfo = (from y in personInfo
                                        where y.isPrimary == isPrimary
                                        group y by new
                                        {
                                            insuranceList = Insurance.Where(x => x.personID == y.personID)
                                                                                 .Select(ins => new InsuranceModel
                                                                                 {
                                                                                     lifeID = ins.lifeID,
                                                                                     personID = y.personID,
                                                                                     amount = ins.amount,
                                                                                     company = ins.company,
                                                                                     lifeTypeID = ins.lifeTypeID,
                                                                                     lifeTypeName = ins.lifeTypeName,
                                                                                     notes = ins.notes,
                                                                                     NotesShow = ins.NotesShow,
                                                                                     profileID = ins.profileID,
                                                                                     valueAsOf = ins.valueAsOf,
                                                                                     premiumType = ins.premiumType.Value,
                                                                                     premiumAmount = ins.premiumAmount,
                                                                                     monthly=ins.monthly,
                                                                                     faceValue = ins.faceValue,
                                                                                     ModifiedDate = ins.ModifiedDate,
                                                                                     ModifiedBy = ins.ModifiedBy,
                                                                                     CreatedDate = ins.CreatedDate,
                                                                                     showInMouseOver = ins.showInMouseOver
                                                                                 }).ToList(),
                                            profileID = y.profileID,
                                            personID = y.personID,
                                            fName = y.firstName,
                                            midName = y.midName,
                                            Lname = y.lastName,
                                            isMale = y.isMale,
                                            isPrimary = y.isPrimary,
                                        } into g
                                        select new PersonInfoModel
                                        {
                                            profileID = g.Key.profileID,
                                            personID = g.Key.personID,
                                            firstName = g.Key.fName,
                                            midName = g.Key.midName,
                                            lastName = g.Key.Lname,
                                            isMale = g.Key.isMale,
                                            isPrimary = g.Key.isPrimary,
                                            InsuranceDetails = (g.Key.insuranceList.Count == 0) ? new List<InsuranceModel>() { new InsuranceModel() { profileID = id, valueAsOf = DateTime.Now, personID = g.Key.personID, lifeTypeID = 0 } } : g.Key.insuranceList,
                                            faceValue = g.Key.insuranceList.Sum(s => s.faceValue).Value
                                        }).SingleOrDefault();

                    model.details = (from d in db.InsuranceDetails
                                     join p in db.Persons on d.profileId equals p.profileID
                                     where p.isPrimary == isPrimary && p.profileID == id && d.isActive == true && p.personID == d.personId && p.isActive == true
                                     select new InsuranceDetailsModel
                                     {
                                         payDept = d.payDept.Value,
                                         payMortage = d.payMortage.Value,
                                         personId = p.personID,
                                         profileId = p.profileID,
                                         payCollege = d.payCollege.Value,
                                         financialExpence = d.financialExpence,
                                         expenceafterdeath = d.expenceafterdeath,
                                         insurancePeriod = d.insurancePeriod,
                                         insuranceType = d.insuranceType,
                                         retAsset = d.retAsset.Value,
                                         otherRetAsset = d.otherRetAsset.Value,
                                         selectAll = d.otherRetAsset.Value,
                                         cashAccounts = d.cashAccounts.Value,
                                         spouseRetAsset = d.spouseRetAsset.Value,
                                         nonRetAsset = d.nonRetAsset.Value,
                                         modifiedDate = DateTime.Now,
                                         modifiedBy = d.modifiedBy
                                     }).SingleOrDefault();
                    if (model.details == null && model.personInfo != null)
                        model.details = new InsuranceDetailsModel() { profileId = id, personId = model.personInfo.personID, insuranceType = 0 };
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }
        private ChildInsurance GetChildInsurance(long id, long? childId)
        {
            ChildInsurance model = new ChildInsurance();
            try
            {
                using (var db = Connection.getConnect())
                {
                    var Insurance = db.lifeInsurances.Where(x => x.profileID == id && x.isActive == true).ToList();
                    var childInfo = (childId > 0) ? db.ChildrenInfoes.Where(x => x.profileID == id && x.childID == childId && x.isActive == true).ToList() :
                                                    db.ChildrenInfoes.Where(x => x.profileID == id && x.isActive == true).ToList();
                    model.childInfo = (from y in childInfo
                                       group y by
                                       new
                                       {
                                           insuranceList = Insurance.Where(x => x.childID == y.childID)
                                                                       .Select(ins => new InsuranceModel
                                                                       {
                                                                           lifeID = ins.lifeID,
                                                                           childID = y.childID,
                                                                           amount = ins.amount,
                                                                           company = ins.company,
                                                                           lifeTypeID = ins.lifeTypeID,
                                                                           lifeTypeName = ins.lifeTypeName,
                                                                           notes = ins.notes,
                                                                           NotesShow = ins.NotesShow,
                                                                           profileID = ins.profileID,
                                                                           valueAsOf = ins.valueAsOf,
                                                                           premiumType = ins.premiumType.Value,
                                                                           premiumAmount = ins.premiumAmount,
                                                                           monthly = ins.monthly,
                                                                           faceValue = ins.faceValue,
                                                                           ModifiedDate = ins.ModifiedDate,
                                                                           ModifiedBy = ins.ModifiedBy,
                                                                           CreatedDate = ins.CreatedDate,
                                                                           showInMouseOver = ins.showInMouseOver
                                                                       }).ToList(),
                                           profileID = y.profileID,
                                           childID = y.childID,
                                           fName = y.fName,
                                           midName = y.midName,
                                           Lname = y.Lname,
                                           isMale = y.isMale
                                       } into g
                                       select new ChildInfoModel
                                       {
                                           profileID = g.Key.profileID,
                                           childID = g.Key.childID,
                                           fName = g.Key.fName,
                                           midName = g.Key.midName,
                                           Lname = g.Key.Lname,
                                           isMale = g.Key.isMale,
                                           InsuranceDetails = (g.Key.insuranceList.Count == 0) ? new List<InsuranceModel>() { new InsuranceModel() { profileID = id, valueAsOf = DateTime.Now, childID = g.Key.childID, lifeTypeID = 0 } } : g.Key.insuranceList,
                                           faceValue = g.Key.insuranceList.Sum(s => s.faceValue).Value
                                       }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }

        public FullLifeInsurance DeleteInsurance(InsuranceInputModel input, long userId)
        {
            FullLifeInsurance model = new FullLifeInsurance();
            try
            {
                lifeInsurance insurance = new lifeInsurance();
                using (var db = Connection.getConnect())
                {
                    insurance = db.lifeInsurances.FirstOrDefault(b => b.lifeID == input.lifeId && b.profileID == input.profileId);
                    if (insurance != null)
                    {
                        insurance.isActive = false;
                        insurance.ModifiedDate = DateTime.Now;
                        insurance.ModifiedBy = userId;
                        db.Entry(insurance).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        model = GetLifeInsurance(input.profileId, input.type);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }
        public ChildInsurance DeleteChildInsurance(InsuranceChildInput input, long userId)
        {
            ChildInsurance model = new ChildInsurance();
            try
            {
                lifeInsurance insurance = new lifeInsurance();
                using (var db = Connection.getConnect())
                {
                    insurance = db.lifeInsurances.FirstOrDefault(b => b.lifeID == input.lifeId && b.profileID == input.profileId);
                    if (insurance != null)
                    {
                        insurance.isActive = false;
                        insurance.ModifiedDate = DateTime.Now;
                        insurance.ModifiedBy = userId;
                        db.Entry(insurance).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        model = GetChildInsurance(input.profileId, input.childId);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }
        public OtherLifeInsurance DeleteOtherInsurance(OtherInsuranceInputModel input, long userId)
        {
            OtherLifeInsurance model = new OtherLifeInsurance();
            try
            {
                OtherInsurance insurance = new OtherInsurance();
                using (var db = Connection.getConnect())
                {
                    insurance = db.OtherInsurances.FirstOrDefault(x => x.lifeID == input.lifeId && x.profileID == input.profileId);
                    if (insurance != null)
                    {
                        insurance.isActive = false;
                        insurance.ModifiedDate = DateTime.Now;
                        insurance.ModifiedBy = userId;
                        db.Entry(insurance).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        model = GetOtherInsurance(input.profileId);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }

        public InsuranceDetailsModel SaveInsuranceDetails(InsuranceDetailsModel input, long userId)
        {
            try
            {

                InsuranceDetail insurance = new InsuranceDetail();
                using (var db = Connection.getConnect())
                {
                    insurance = db.InsuranceDetails.FirstOrDefault(b => b.personId == input.personId && b.profileId == input.profileId && b.isActive == true);
                    if (insurance != null)
                    {
                        insurance.personId = input.personId;
                        insurance.payDept = input.payDept;
                        insurance.payMortage = input.payMortage;
                        insurance.payCollege = input.payCollege;
                        insurance.financialExpence = input.financialExpence;
                        insurance.expenceafterdeath = input.expenceafterdeath;
                        insurance.insurancePeriod = input.insurancePeriod;
                        insurance.insuranceType = input.insuranceType;
                        insurance.retAsset = input.retAsset;
                        insurance.otherRetAsset = input.otherRetAsset;
                        insurance.cashAccounts = input.cashAccounts;
                        insurance.spouseRetAsset = input.spouseRetAsset;
                        insurance.selectAllAccounts = input.selectAll;
                        insurance.nonRetAsset = input.nonRetAsset;
                        insurance.modifiedDate = DateTime.Now;
                        insurance.modifiedBy = userId;
                        db.Entry(insurance).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        input.InsuranceId = insurance.InsuranceId;
                        return input;
                    }
                    else
                    {
                        insurance = new InsuranceDetail()
                        {
                            personId = input.personId,
                            payDept = input.payDept,
                            payMortage = input.payMortage,
                            profileId = input.profileId,
                            payCollege = input.payCollege,
                            financialExpence = input.financialExpence,
                            expenceafterdeath = input.expenceafterdeath,
                            insurancePeriod = input.insurancePeriod,
                            insuranceType = input.insuranceType,
                            retAsset = input.retAsset,
                            otherRetAsset = input.otherRetAsset,
                            cashAccounts = input.cashAccounts,
                            spouseRetAsset = input.spouseRetAsset,
                            selectAllAccounts = input.selectAll,
                            nonRetAsset = input.nonRetAsset,
                            modifiedDate = DateTime.Now,
                            createdDate = DateTime.Now,
                            modifiedBy = userId,
                            isActive = true
                        };
                        db.InsuranceDetails.Add(insurance);
                        db.SaveChanges();
                        input.InsuranceId = insurance.InsuranceId;
                        return input;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public FullLifeInsurance SaveInsurance(List<InsuranceModel> inputList, long userId)
        {
            try
            {

                lifeInsurance insurance = new lifeInsurance();
                using (var db = Connection.getConnect())
                {
                    foreach (var input in inputList)
                    {

                        insurance = db.lifeInsurances.FirstOrDefault(b => b.lifeID == input.lifeID && b.profileID == input.profileID && b.isActive == true);
                        if (insurance != null && insurance.lifeID > 0)
                        {
                            insurance.premiumAmount = input.premiumAmount;
                            insurance.premiumType = input.premiumType;
                            insurance.monthly = GetMonthly(input.premiumAmount, input.premiumType);
                            insurance.company = input.company;
                            insurance.lifeTypeID = input.lifeTypeID;
                            insurance.lifeTypeName = db.lifeTypes.SingleOrDefault(x => x.lifeTypeID == input.lifeTypeID).lifeTypeName;
                            insurance.notes = input.notes;
                            insurance.valueAsOf = input.valueAsOf;
                            insurance.NotesShow = input.NotesShow;
                            insurance.amount = input.amount;
                            insurance.profileID = input.profileID;
                            insurance.faceValue = input.faceValue;
                            insurance.childID = input.childID;
                            insurance.personID = input.personID;
                            insurance.showInMouseOver = input.showInMouseOver;
                            insurance.ModifiedDate = DateTime.Now;
                            insurance.ModifiedBy = userId;
                            db.Entry(insurance).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            insurance = new lifeInsurance()
                            {
                                premiumAmount = input.premiumAmount,
                                premiumType = input.premiumType,
                                monthly = GetMonthly(input.premiumAmount, input.premiumType),
                                company = input.company,
                                lifeTypeID = input.lifeTypeID,
                                lifeTypeName = db.lifeTypes.SingleOrDefault(x => x.lifeTypeID == input.lifeTypeID).lifeTypeName,
                                notes = input.notes,
                                valueAsOf = input.valueAsOf,
                                NotesShow = input.NotesShow,
                                profileID = input.profileID,
                                amount = input.amount,
                                faceValue = input.faceValue,
                                showInMouseOver = input.showInMouseOver,
                                childID = input.childID,
                                personID = input.personID,
                                ModifiedDate = DateTime.Now,
                                ModifiedBy = userId,
                                CreatedDate = DateTime.Now,
                                isActive = true
                            };
                            db.lifeInsurances.Add(insurance);
                            db.SaveChanges();
                        }
                    }
                    return GetLifeInsurance(inputList.FirstOrDefault().profileID, inputList.FirstOrDefault().Type);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }



        public ChildInsurance SaveChildInsurance(List<InsuranceModel> inputList, long userId)
        {
            try
            {
                lifeInsurance insurance = new lifeInsurance();
                using (var db = Connection.getConnect())
                {
                    foreach (var input in inputList)
                    {

                        insurance = db.lifeInsurances.FirstOrDefault(b => b.lifeID == input.lifeID && b.profileID == input.profileID && b.isActive == true);
                        if (insurance != null && insurance.lifeID > 0)
                        {
                            insurance.premiumAmount = input.premiumAmount;
                            insurance.premiumType = input.premiumType;
                            insurance.monthly = GetMonthly(input.premiumAmount, input.premiumType);
                            insurance.company = input.company;
                            insurance.lifeTypeID = input.lifeTypeID;
                            insurance.lifeTypeName = db.lifeTypes.SingleOrDefault(x => x.lifeTypeID == input.lifeTypeID).lifeTypeName;
                            insurance.notes = input.notes;
                            insurance.valueAsOf = input.valueAsOf;
                            insurance.NotesShow = input.NotesShow;
                            insurance.profileID = input.profileID;
                            insurance.amount = input.amount;
                            insurance.childID = input.childID;
                            insurance.faceValue = input.faceValue;
                            insurance.showInMouseOver = input.showInMouseOver;
                            insurance.personID = input.personID;
                            insurance.ModifiedDate = DateTime.Now;
                            insurance.ModifiedBy = userId;
                            db.Entry(insurance).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            insurance = new lifeInsurance()
                            {
                                premiumAmount = input.premiumAmount,
                                premiumType = input.premiumType,
                                monthly = GetMonthly(input.premiumAmount, input.premiumType),
                                company = input.company,
                                lifeTypeID = input.lifeTypeID,
                                lifeTypeName = db.lifeTypes.SingleOrDefault(x => x.lifeTypeID == input.lifeTypeID).lifeTypeName,
                                notes = input.notes,
                                valueAsOf = input.valueAsOf,
                                NotesShow = input.NotesShow,
                                faceValue = input.faceValue,
                                amount = input.amount,
                                profileID = input.profileID,
                                childID = input.childID,
                                showInMouseOver = input.showInMouseOver,
                                personID = input.personID,
                                ModifiedDate = DateTime.Now,
                                ModifiedBy = userId,
                                CreatedDate = DateTime.Now,
                                isActive = true
                            };
                            db.lifeInsurances.Add(insurance);
                            db.SaveChanges();
                        }
                    }
                    return GetChildInsurance(inputList.FirstOrDefault().profileID, inputList.FirstOrDefault().childID);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public OtherLifeInsurance SaveOtherInsurance(List<OtherInsuranceModel> inputList, long userId)
        {
            try
            {
                OtherLifeInsurance model = new OtherLifeInsurance();
                OtherInsurance insurance = new OtherInsurance();

                using (var db = Connection.getConnect())
                {
                    foreach (var input in inputList)
                    {
                        insurance = db.OtherInsurances.FirstOrDefault(b => b.lifeID == input.lifeID && b.profileID == input.profileID && b.isActive == true);
                        if (insurance != null && insurance.lifeID > 0)
                        {
                            insurance.premiumAmount = input.premiumAmount;
                            insurance.premiumType = input.premiumType;
                            insurance.monthly = GetMonthly(input.premiumAmount, input.premiumType);
                            insurance.company = input.company;
                            insurance.lifeTypeID = input.lifeTypeID;
                            insurance.lifeTypeName = db.insuranceTypes.SingleOrDefault(x => x.insuranceTypeID == input.lifeTypeID).insuranceTypeName;
                            insurance.notes = input.notes;
                            insurance.premiumasof = input.valueAsOf;
                            insurance.NotesShow = input.NotesShow;
                            insurance.profileID = input.profileID;
                            insurance.showInMouseOver = input.showInMouseOver;
                            insurance.ModifiedDate = DateTime.Now;
                            insurance.ModifiedBy = userId;
                            db.Entry(insurance).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            insurance = new OtherInsurance()
                            {
                                premiumAmount = input.premiumAmount,
                                premiumType = input.premiumType,
                                monthly = GetMonthly(input.premiumAmount, input.premiumType),
                                company = input.company,
                                lifeTypeID = input.lifeTypeID,
                                lifeTypeName = db.insuranceTypes.SingleOrDefault(x => x.insuranceTypeID == input.lifeTypeID).insuranceTypeName,
                                notes = input.notes,
                                premiumasof = input.valueAsOf,
                                NotesShow = input.NotesShow,
                                profileID = input.profileID,
                                showInMouseOver = input.showInMouseOver,
                                ModifiedDate = DateTime.Now,
                                ModifiedBy = userId,
                                isActive = true,
                                CreatedDate = DateTime.Now,
                            };
                            db.OtherInsurances.Add(insurance);
                            db.SaveChanges();
                        }
                    }
                    return GetOtherInsurance(inputList.FirstOrDefault().profileID);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public OtherLifeInsurance GetOtherInsurance(long id)
        {
            OtherLifeInsurance model = new OtherLifeInsurance();
            try
            {
                using (var db = Connection.getConnect())
                {
                    var otherInsurance = db.OtherInsurances.Where(x => x.profileID == id && x.isActive == true).ToList();
                    model.InsuranceDetails = otherInsurance.Where(x => x.isActive == true).Select(X =>
                                                                new OtherInsuranceModel
                                                                {
                                                                    lifeID = X.lifeID,
                                                                    company = X.company,
                                                                    lifeTypeID = X.lifeTypeID,
                                                                    lifeTypeName = X.lifeTypeName,
                                                                    notes = X.notes,
                                                                    NotesShow = X.NotesShow,
                                                                    profileID = X.profileID,
                                                                    valueAsOf = X.premiumasof,
                                                                    premiumType = X.premiumType.Value,
                                                                    premiumAmount = X.premiumAmount,
                                                                    monthly=X.monthly,
                                                                    ModifiedDate = X.ModifiedDate,
                                                                    ModifiedBy = X.ModifiedBy,
                                                                    CreatedDate = X.CreatedDate,
                                                                    showInMouseOver = X.showInMouseOver
                                                                }).ToList();
                    if (model.InsuranceDetails == null || model.InsuranceDetails.Count == 0)
                        model.InsuranceDetails.Add(new OtherInsuranceModel() { profileID = id, valueAsOf = DateTime.Now, lifeTypeID = 0 });
                    model.monthlyPremium = model.InsuranceDetails.Sum(x => x.monthly).Value;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }
        private decimal? GetMonthly(decimal? premiumAmount, long premiumType)
        {
            switch (premiumType)
            {
                case (int)CustomEnum.PayEnum.Annually:
                    return premiumAmount / 12;
                    break;
                case (int)CustomEnum.PayEnum.SemiAnnually:
                    return premiumAmount / 6;
                    break;
                case (int)CustomEnum.PayEnum.Quarterly:
                    return premiumAmount / 3;
                    break;
                case (int)CustomEnum.PayEnum.Monthly:
                    return premiumAmount;
                    break;
                case (int)CustomEnum.PayEnum.Semimonthly:
                    return premiumAmount * 2;
                    break;
                case (int)CustomEnum.PayEnum.Weekly:
                    return premiumAmount * 52 / 12;
                    break;
                case (int)CustomEnum.PayEnum.Biweekly:
                    return premiumAmount * 26 / 12;
                    break;
            }
            return premiumAmount;
        }
    }
}
