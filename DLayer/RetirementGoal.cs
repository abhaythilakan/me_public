﻿using CLayer;
using System;
using CLayer.common;
using CLayer.DBModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Globalization;

namespace DLayer
{
    public class RetirementGoal
    {
        public RetirementModel GetRetirementDetails(long profileID)
        {
            RetirementModel outobj = new RetirementModel();
            outobj.profileID = profileID;
            DefaultsRetirement dbretobj = new DefaultsRetirement();
            // outobj.otherValuesModel = new otherValuesModel();
            using (var db = Connection.getConnect())
            {
                try
                {
                   // dbretobj = db.DefaultsRetirements.Where(b => b.profileID == profileID).FirstOrDefault();
                    outobj.reportDate = db.profiles.Where(v => v.profileID == profileID).Select(v => v.profileDate).FirstOrDefault();
                    var fund = db.Asset_EmergencyFund.Where(b => b.profileID == profileID).Select(v => v.emergencyFund).FirstOrDefault();


                    dbretobj = db.DefaultsRetirements.Join(db.profiles.Where(x => x.profileID == profileID), d => d.clientID, p => p.clientID, (d, p) => d).ToList()
                        .Select(y => new DefaultsRetirement
                        {
                        inflation=y.inflation,
                        lifeExpectency=y.lifeExpectency,
                        monthIncome=y.monthIncome,
                        retireAge=y.retireAge,
                        retTax=y.retTax,
                        retReturnTax=y.retReturnTax,
                        preRetReturnTax=y.preRetReturnTax,
                        
                        }).FirstOrDefault();
                


                    if (dbretobj == null)
                    {

                        throw new Exception("Retirement defaults not found.");
                    }
                    else
                    {
                        outobj.retTaxRate = dbretobj.retTax;
                        outobj.preRetTax = dbretobj.preRetReturnTax;
                        outobj.inflation = dbretobj.inflation;
                        outobj.retReturnTax = dbretobj.retReturnTax;
                        outobj.currentNeed = fund ?? 10000;
                        outobj.rateA = 3;
                        outobj.rateB = 6;
                        outobj.rateC = 10;
                        //outobj.rateC = outobj.rateC >0 && outobj.rateC!=null ? outobj.rateC  : 10;
                    }
                    outobj.PersonInfoDetails = new List<PersonalInfo>();    
                    outobj.PersonInfoDetails = (from i in db.Asset_Retirement
                                                join p in db.Persons on i.personID equals p.personID
                                                where i.isActive == true && i.profileID == profileID && p.isActive == true
                                                select new PersonalInfo
                                                {
                                                    personID = i.personID.Value,
                                                    dob = p.birthDate.Value,
                                                    retAge = p.retirementAge.Value,
                                                    lifeExpectancy = p.lifeExpectancy.Value,
                                                    name = i.name,
                                                    isprimary = p.isPrimary.Value
                                                }).ToList();
                    if (outobj.PersonInfoDetails.Count > 0)
                    {
                        int i = 0;
                        foreach (var obj in outobj.PersonInfoDetails)
                        {
                            // DateTime zeroTime = new DateTime(1, 1, 1);
                            // DateTime a;
                            // DateTime b;
                            int a;
                            int b;
                            a = obj.dob.Year;
                            b = outobj.reportDate.Value.Year;
                            // TimeSpan span = b - a;
                            // decimal years = Convert.ToDecimal((zeroTime + span).Year - 1);
                            int years = b - a;
                            outobj.PersonInfoDetails[i].age = years;//as of report date
                            outobj.PersonInfoDetails[i].untilRetirement = outobj.PersonInfoDetails[i].retAge - outobj.PersonInfoDetails[i].age;
                            outobj.PersonInfoDetails[i].inRetirement = outobj.PersonInfoDetails[i].lifeExpectancy - outobj.PersonInfoDetails[i].retAge;
                            i++;
                            //if (obj.isprimary == true)
                            //{
                            //    outobj.otherValuesModel.pName = obj.name;
                            //}
                            //else if (obj.isprimary == false)
                            //{
                            //    outobj.otherValuesModel.sName = obj.name;
                            //}
                        }
                    }
                    outobj.currentretSummary = new List<currentRetSummary>();
                    var retAssetPS = db.Asset_Retirement.Where(b => b.profileID == profileID && b.isActive == true).ToList();
                    if (retAssetPS.Count > 0)
                    {
                        int i = 0;
                        foreach (var r in retAssetPS)
                        {
                            outobj.currentretSummary.Add(new currentRetSummary { name = r.name, personID = r.personID, totalplanbalance = 0, totalempContribution = 0, totalpersonalContribution = 0, currentRetSummaryDetails = new List<currentRetSummaryDetails>() });
                            outobj.currentretSummary[i].currentRetSummaryDetails = (from rd in db.Asset_RetirementDetails
                                                                                    join at in db.AssetTypes on rd.assetTypeID equals at.assetTypeID
                                                                                    where rd.isActive == true && rd.isOtherRetirement == false && rd.assetID == r.assetID
                                                                                    select new currentRetSummaryDetails
                                                                                    {
                                                                                        planbalance = rd.amount,
                                                                                        personalContribution = rd.monthlyContribution ?? 0,
                                                                                        empContribution = rd.empContribution ?? 0,
                                                                                        name = at.typeName + " " + rd.company,
                                                                                        istaxed = at.isTaxed
                                                                                    }).ToList();
                            if (outobj.currentretSummary[i].currentRetSummaryDetails.Count > 0)
                            {
                                outobj.currentretSummary[i].totalpersonalContribution = outobj.currentretSummary[i].currentRetSummaryDetails.Sum(b => b.personalContribution);
                                outobj.currentretSummary[i].totalempContribution = outobj.currentretSummary[i].currentRetSummaryDetails.Sum(b => b.empContribution);
                                outobj.currentretSummary[i].totalplanbalance = outobj.currentretSummary[i].currentRetSummaryDetails.Sum(b => b.planbalance);
                            }
                            i++;
                        }
                        outobj.currentretSummary.Add(new currentRetSummary { name = "Other", personID = 0, totalplanbalance = 0, totalempContribution = 0, totalpersonalContribution = 0, currentRetSummaryDetails = new List<currentRetSummaryDetails>() });
                        outobj.currentretSummary[i].currentRetSummaryDetails = (from rd in db.Asset_RetirementDetails
                                                                                join at in db.AssetTypes on rd.assetTypeID equals at.assetTypeID

                                                                                where rd.isActive == true && rd.isOtherRetirement == true && rd.profileID == profileID
                                                                                select new currentRetSummaryDetails
                                                                                {
                                                                                    planbalance = rd.amount,
                                                                                    personalContribution = rd.monthlyContribution ?? 0,
                                                                                    empContribution = rd.empContribution ?? 0,
                                                                                    name = at.typeName + " " + rd.company,
                                                                                    istaxed = at.isTaxed
                                                                                }).ToList();
                        if (outobj.currentretSummary[i].currentRetSummaryDetails.Count > 0)
                        {
                            outobj.currentretSummary[i].totalpersonalContribution = outobj.currentretSummary[i].currentRetSummaryDetails.Sum(b => b.personalContribution);
                            outobj.currentretSummary[i].totalempContribution = outobj.currentretSummary[i].currentRetSummaryDetails.Sum(b => b.empContribution);
                            outobj.currentretSummary[i].totalplanbalance = outobj.currentretSummary[i].currentRetSummaryDetails.Sum(b => b.planbalance);
                        }
                        i++;
                    }

                    if (outobj.whatif == null)
                    {
                        outobj.whatif = new List<whatif>();
                        outobj.whatif.Add(new whatif
                        {
                            whatifplanbalance = 0,
                            whatifpersonalContribution = 0,
                            whatifmonthlyContribution = 0,
                            isTaxed = true
                        });
                    }

                    outobj = FINCalculation(outobj);

                }
                catch (Exception ex)
                {
                    throw;
                }
                return outobj;
            }
        }

        private RetirementModel retirementLedegernew(RetirementModel outobj)
        {
            int startdate = outobj.otherValuesModel.startdate.Value;

            List<IncomeProjectionModel> incomeList = new List<IncomeProjectionModel>();
            DLayer.IncomeDetails IDL = new IncomeDetails();
            incomeList = IDL.incomeProjection(outobj.profileID.Value);
            outobj.ledger = new List<RetirementledgerModel>();
            try
            {
                int i = 0;
                int pAge = 0;
                int sAge = 0;
                int plife = 0;
                int slife = 0;
                int pret = 0;
                int sret = 0;
                double tax = 0;
                int repdateyear = outobj.reportDate.Value.Year;
                double retTaxRate = Convert.ToDouble(outobj.retTaxRate.Value / 100);
                double inflation = Convert.ToDouble(outobj.inflation.Value / 100);
                double retReturnTax = Convert.ToDouble(outobj.retReturnTax.Value / 100);
                double preRetTax = Convert.ToDouble(outobj.preRetTax.Value / 100);
                int months = 0;
                int wmonths = 0;

                double balanceAmount = 0; //amount which is from anual need -  incomes
                double balanceAmountGrowth = 0;

                double pgrowth = 0;
                double pgrowthNotTaxable = 0;
                double sgrowth = 0;
                double sgrowthNotTaxable = 0;
                double ogrowth = 0;
                double ogrowthnotTaxable = 0;
                double wgrowth = 0;
                double wgrowthNotaxable = 0;

                double retireIncome = 0;

                double otherCont = 0;
                double otherContNotInterested = 0;

                double annualContribution = 0;
                double annualRetNeed = 0;
                double otherIncomeafterRet = 0;
                double taxesonOtherIncomeafterRet = 0;
                double withdrawal = 0;
                double taxesOnWithdrawal = 0;
                double endingBalance = 0;


                double pCurrentBalance = outobj.otherValuesModel.pCurrentBalance;
                double pCurrentContribution = outobj.otherValuesModel.pCurrentContribution;
                double pCurrentBalanceNotInterest = outobj.otherValuesModel.pCurrentBalnceNotInterest;
                double pCurrentContributionNotInterest = outobj.otherValuesModel.pCurrentContributionNotInterest;
                // double pCurrentBalanceTotal = 0;

                double sCurrentBalance = outobj.otherValuesModel.sCurrentBalance; ;
                double sCurrentContribution = outobj.otherValuesModel.sCurrentContribution;
                double sCurrentBalanceNotInterest = outobj.otherValuesModel.sCurrentBalnceNotInterest;
                double sCurrentContributionNotInterest = outobj.otherValuesModel.sCurrentContributionNotInterest;
                //double sCurrentBalanceTotal = 0;

                double oCurrentBalance = outobj.otherValuesModel.oCurrentBalance; ;
                double oCurrentContribution = outobj.otherValuesModel.oCurrentContribution;
                double oCurrentBalanceNotInterest = outobj.otherValuesModel.oCurrentBalnceNotInterest;
                double oCurrentContributionNotInterest = outobj.otherValuesModel.oCurrentContributionNotInterest;
                //double oCurrentBalanceTotal = 0;

                double wCurrentBalance = outobj.otherValuesModel.wCurrentBalance;
                double wCurrentContribution = outobj.otherValuesModel.wCurrentContribution;
                double wCurrentBalanceNotInterest = outobj.otherValuesModel.wCurrentBalnceNotInterest;
                double wCurrentContributionNotInterest = outobj.otherValuesModel.wCurrentContributionNotInterest;
                //double wCurrentBalanceTotal = 0;
                int w = 0;

                if (outobj.otherValuesModel.count == 2)
                {
                    pAge = outobj.PersonInfoDetails.Where(p => p.isprimary == true).Select(s => s.age).FirstOrDefault();
                    plife = outobj.PersonInfoDetails.Where(p => p.isprimary == true).Select(s => s.lifeExpectancy).FirstOrDefault();
                    pret = outobj.PersonInfoDetails.Where(p => p.isprimary == true).Select(s => s.retAge).FirstOrDefault();
                    sAge = outobj.PersonInfoDetails.Where(p => p.isprimary == false).Select(s => s.age).FirstOrDefault();
                    slife = outobj.PersonInfoDetails.Where(p => p.isprimary == false).Select(s => s.lifeExpectancy).FirstOrDefault();
                    sret = outobj.PersonInfoDetails.Where(p => p.isprimary == false).Select(s => s.retAge).FirstOrDefault();

                }
                else if (outobj.otherValuesModel.count == 1)
                {
                    if (outobj.PersonInfoDetails[0].isprimary == true)
                    {
                        pAge = outobj.PersonInfoDetails.Where(p => p.isprimary == true).Select(s => s.age).FirstOrDefault();
                        plife = outobj.PersonInfoDetails.Where(p => p.isprimary == true).Select(s => s.lifeExpectancy).FirstOrDefault();
                        pret = outobj.PersonInfoDetails.Where(p => p.isprimary == true).Select(s => s.retAge).FirstOrDefault();
                    }
                    else if (outobj.PersonInfoDetails[0].isprimary == false)
                    {
                        sAge = outobj.PersonInfoDetails.Where(p => p.isprimary == false).Select(s => s.age).FirstOrDefault();
                        slife = outobj.PersonInfoDetails.Where(p => p.isprimary == false).Select(s => s.lifeExpectancy).FirstOrDefault();
                        sret = outobj.PersonInfoDetails.Where(p => p.isprimary == false).Select(s => s.retAge).FirstOrDefault();
                    }
                }
                else
                {
                    outobj.ledger = null;
                    return outobj;
                }

                while (startdate <= outobj.otherValuesModel.enddate.Value)
                {

                    if (startdate == 2069)
                    {
                        string s = "dadsa";
                    }

                    outobj.ledger.Add(new RetirementledgerModel
                    {
                        year = startdate,
                        otherIncomeafterRet = 0,
                        taxesonOtherIncomeafterRet = 0,
                        annualContribution = 0,
                        endingBalance = 0,
                        growth = 0,
                        annualRetNeed = 0,
                        taxesOnWithdrawal = 0,
                        withdrawal = 0,

                    });
                    #region age increment primary and spouse
                    if (pAge > 0 && plife >= pAge)
                    {
                        outobj.ledger[i].pAge = pAge;
                        pAge++;
                    }
                    else
                    {
                        outobj.ledger[i].pAge = null;
                    }

                    if (sAge > 0 && slife >= sAge)
                    {
                        outobj.ledger[i].sAge = sAge;
                        sAge++;
                    }
                    else
                    {
                        outobj.ledger[i].sAge = null;
                    }
                    #endregion
                    if (i == 0)//first year
                    {
                        months = 12 - (outobj.reportDate.Value.Month - 1);
                    }
                    else //from second year
                    {
                        months = 12;
                    }

                    outobj.ledger[i].balance = Math.Round(wCurrentBalance + oCurrentBalance + sCurrentBalance + pCurrentBalance + pCurrentBalanceNotInterest + sCurrentBalanceNotInterest + oCurrentBalanceNotInterest + wCurrentBalanceNotInterest + balanceAmount);
                    // outobj.ledger[i].balance+=
                    if (pCurrentBalance + pCurrentBalanceNotInterest > 0)
                    {
                        //if (outobj.ledger[i].pAge != null && outobj.ledger[i].pAge != 0 && outobj.ledger[i].pAge < plife)
                        //{
                        tax = (outobj.ledger[i].pAge <= pret) ? preRetTax : retReturnTax;
                        pgrowth = /*pgrowth +*/ (pCurrentBalance) * Math.Pow((1 + tax), (Convert.ToDouble(months) / 12)) - pCurrentBalance;        //   1000 * ((1 + .1) ^ (0.166667)) - 1000
                        if (pgrowth < 0)
                        { pgrowth = 0; }
                        pCurrentBalance += pgrowth;
                        pgrowthNotTaxable = /*pgrowth +*/ (pCurrentBalanceNotInterest) * Math.Pow((1 + tax), (Convert.ToDouble(months) / 12)) - pCurrentBalanceNotInterest;        //   1000 * ((1 + .1) ^ (0.166667)) - 1000
                        if (pgrowthNotTaxable < 0)
                        {
                            pgrowthNotTaxable = 0;
                        }
                        pCurrentBalanceNotInterest += pgrowthNotTaxable;
                        // pCurrentBalanceTotal = pCurrentBalance + pCurrentBalanceNotInterest;
                    }
                    else { pgrowth = 0; pgrowthNotTaxable = 0; }

                    // pCurrentBalanceTotal = pCurrentBalance + pCurrentBalnceNotInterest;

                    if (pCurrentContributionNotInterest + pCurrentContribution > 0)
                    {
                        if (outobj.ledger[i].pAge != null && outobj.ledger[i].pAge < pret)
                        {
                            outobj.ledger[i].annualContribution += (pCurrentContributionNotInterest + pCurrentContribution) * months;
                            pCurrentBalance += (pCurrentContribution) * months;
                            pCurrentBalanceNotInterest += (pCurrentContributionNotInterest) * months;
                        }
                    }

                    if (balanceAmount > 0)
                    {
                        tax = retReturnTax;
                        balanceAmountGrowth = (balanceAmount) * Math.Pow((1 + tax), (Convert.ToDouble(months) / 12)) - balanceAmount;
                        if (balanceAmountGrowth < 0)
                        {
                            balanceAmountGrowth = 0;
                        }

                        balanceAmount += balanceAmountGrowth;
                    }

                    //  pCurrentBalanceTotal = pCurrentBalance + pCurrentBalanceNotInterest;

                    if (sCurrentBalance + sCurrentBalanceNotInterest > 0)
                    {
                        //if (outobj.ledger[i].pAge != null && outobj.ledger[i].pAge != 0 && outobj.ledger[i].pAge < plife)
                        //{
                        tax = (outobj.ledger[i].sAge <= sret) ? preRetTax : retReturnTax;
                        sgrowth = /*pgrowth +*/ (sCurrentBalance) * Math.Pow((1 + tax), (Convert.ToDouble(months) / 12)) - sCurrentBalance;        //   1000 * ((1 + .1) ^ (0.166667)) - 1000
                        if (sgrowth < 0)
                        { sgrowth = 0; }
                        sCurrentBalance += sgrowth;
                        sgrowthNotTaxable = /*pgrowth +*/ (sCurrentBalanceNotInterest) * Math.Pow((1 + tax), (Convert.ToDouble(months) / 12)) - sCurrentBalanceNotInterest;        //   1000 * ((1 + .1) ^ (0.166667)) - 1000
                        if (sgrowthNotTaxable < 0)
                        { sgrowthNotTaxable = 0; }
                        sCurrentBalanceNotInterest += sgrowthNotTaxable;

                    }
                    else
                    {
                        sgrowth = 0; sgrowthNotTaxable = 0;
                    }
                    if (sCurrentContributionNotInterest + sCurrentContribution > 0)
                    {
                        if (outobj.ledger[i].sAge != null && outobj.ledger[i].sAge < sret)
                        {
                            outobj.ledger[i].annualContribution += (sCurrentContributionNotInterest + sCurrentContribution) * months;
                            sCurrentBalance += (sCurrentContribution) * months;
                            sCurrentBalanceNotInterest += (sCurrentContributionNotInterest) * months;
                        }
                    }

                    /// sCurrentBalanceTotal = sCurrentBalance + sCurrentBalanceNotInterest;

                    if (oCurrentBalance + oCurrentBalanceNotInterest != 0)
                    {
                        if (startdate >= 0)
                        {
                            tax = (outobj.ledger[i].pAge != null && outobj.ledger[i].pAge < pret) || (outobj.ledger[i].sAge != null && outobj.ledger[i].sAge < sret) ? preRetTax : retReturnTax;
                            ogrowth = /*ogrowth +*/ oCurrentBalance * Math.Pow((1 + tax), (Convert.ToDouble(months) / 12)) - oCurrentBalance;        //   1000 * ((1 + .1) ^ (0.166667)) - 1000
                            if (ogrowth < 0)
                            { ogrowth = 0; }
                            oCurrentBalance += ogrowth;
                            ogrowthnotTaxable = oCurrentBalanceNotInterest * Math.Pow((1 + tax), (Convert.ToDouble(months) / 12)) - oCurrentBalanceNotInterest;
                            if (ogrowthnotTaxable < 0)
                            { ogrowthnotTaxable = 0; }
                            oCurrentBalanceNotInterest += ogrowthnotTaxable;
                        }
                    }
                    else { ogrowth = 0; ogrowthnotTaxable = 0; }
                    if (oCurrentContributionNotInterest + oCurrentContribution > 0)
                    {
                        if ((outobj.ledger[i].sAge != null && outobj.ledger[i].sAge < sret) || (outobj.ledger[i].pAge != null && outobj.ledger[i].pAge < pret))
                        {
                            outobj.ledger[i].annualContribution += (oCurrentContributionNotInterest + oCurrentContribution) * months;
                            double tempContribution = oCurrentContribution * months;
                            double tempContributionNotTaxed = oCurrentContributionNotInterest * months;

                            if (pCurrentBalance > 0)
                            {
                                pCurrentBalance += oCurrentContribution * months;
                                pCurrentBalanceNotInterest += oCurrentContributionNotInterest * months;
                            }
                            else if (sCurrentBalance > 0)
                            {
                                sCurrentBalance += oCurrentContribution * months;
                                sCurrentBalanceNotInterest += oCurrentContributionNotInterest * months;
                            }
                        }
                        else
                        {
                            oCurrentBalance += oCurrentContribution * months;
                            oCurrentBalanceNotInterest += oCurrentContributionNotInterest * months;
                        }

                    }

                    //oCurrentBalanceTotal = oCurrentBalance + oCurrentBalanceNotInterest;

                    if (w == 0)
                    {
                        wmonths = 12 - (System.DateTime.Now.Month - 1);
                        w = 1;
                    }
                    else //from second year
                    {
                        wmonths = 12;
                    }
                    if (startdate >= System.DateTime.Now.Year)
                    {
                        if (wCurrentBalance + wCurrentBalanceNotInterest > 0)
                        {
                            tax = (outobj.ledger[i].pAge != null && outobj.ledger[i].pAge < pret) || (outobj.ledger[i].sAge != null && outobj.ledger[i].sAge < sret) ? preRetTax : retReturnTax;
                            wgrowth = /*wgrowth +*/ wCurrentBalance * Math.Pow((1 + tax), (Convert.ToDouble(wmonths) / 12)) - wCurrentBalance;        //   1000 * ((1 + .1) ^ (0.166667)) - 1000
                            if (wgrowth < 0)
                            { wgrowth = 0; }
                            wCurrentBalance += wgrowth;

                            wgrowthNotaxable = /*wgrowth +*/ wCurrentBalanceNotInterest * Math.Pow((1 + tax), (Convert.ToDouble(wmonths) / 12)) - wCurrentBalanceNotInterest;        //   1000 * ((1 + .1) ^ (0.166667)) - 1000
                            if (wgrowthNotaxable < 0)
                            { wgrowthNotaxable = 0; }
                            wCurrentBalanceNotInterest += wgrowthNotaxable;
                        }
                    }
                    if (wCurrentContributionNotInterest + wCurrentContribution > 0)
                    {
                        if ((outobj.ledger[i].pAge != null && outobj.ledger[i].sAge < sret) || (outobj.ledger[i].pAge != null && outobj.ledger[i].pAge < pret))
                        {
                            outobj.ledger[i].annualContribution += (wCurrentContributionNotInterest + wCurrentContribution) * months;
                            if (pCurrentBalance > 0)
                            {
                                pCurrentBalance += oCurrentContribution * months;
                                pCurrentBalanceNotInterest += oCurrentContributionNotInterest * months;
                            }
                            else if (sCurrentBalance > 0)
                            {
                                sCurrentBalance += oCurrentContribution * months;
                                sCurrentBalanceNotInterest += oCurrentContributionNotInterest * months;
                            }
                        }
                        else
                        {
                            wCurrentBalance += oCurrentContribution * months;
                            wCurrentBalanceNotInterest += oCurrentContribution * months;
                        }


                    }
                    outobj.ledger[i].growth = Math.Round(sgrowth + sgrowthNotTaxable + pgrowth + pgrowthNotTaxable + ogrowth + ogrowthnotTaxable + wgrowth + wgrowthNotaxable + balanceAmountGrowth);
                    // if any person retire
                    double otherincome = 0;
                    if ((outobj.ledger[i].sAge >= 0 && outobj.ledger[i].sAge >= sret && outobj.ledger[i].sAge <= slife) || (outobj.ledger[i].pAge >= 0 && outobj.ledger[i].pAge >= pret && outobj.ledger[i].pAge <= plife))
                    {

                        //var a = incomeList.Where(x => x.IncomeProjectionDetails.Where(o => o.year == startdate).Select(s=>s.amount).Se;
                        foreach (var a in incomeList)
                        {
                            outobj.ledger[i].otherIncomeafterRet += Convert.ToDouble(a.IncomeProjectionDetails.Where(o => o.year == startdate).Select(s => s.amount).FirstOrDefault());
                            outobj.ledger[i].taxesonOtherIncomeafterRet += Convert.ToDouble(a.IncomeProjectionDetails.Where(o => o.year == startdate && o.Tax != null).Select(s => s.Tax).FirstOrDefault());
                            if (a.role == "Other")
                            {
                                otherincome = Convert.ToDouble(a.IncomeProjectionDetails.Where(o => o.year == startdate).Select(s => s.amount).FirstOrDefault());
                            }

                        }
                        double futureneed = Convert.ToDouble(outobj.currentNeed) * (Math.Pow(Convert.ToDouble(1 + outobj.inflation / 100), i));
                        outobj.ledger[i].annualRetNeed = Math.Round(futureneed * 12);

                    }

                    if (pCurrentBalance + sCurrentBalance + balanceAmount + wCurrentBalance + wCurrentBalanceNotInterest + pCurrentBalanceNotInterest + sCurrentBalanceNotInterest + oCurrentBalance + oCurrentBalanceNotInterest == 0)
                    {
                        retireIncome = 0.0000000001;
                    }
                    else
                    {
                        retireIncome = outobj.ledger[0].balance.Value;
                    }
                    if (outobj.ledger[i].otherIncomeafterRet >= outobj.ledger[i].annualRetNeed)
                    {

                        if (outobj.ledger[i].otherIncomeafterRet - outobj.ledger[i].annualRetNeed > otherincome)
                        {
                            balanceAmount += otherincome;
                        }
                        else
                        {
                            balanceAmount += outobj.ledger[i].otherIncomeafterRet.Value - outobj.ledger[i].annualRetNeed.Value;
                        }
                    }
                    else
                    {
                        balanceAmount = 0;
                        outobj.ledger[i].withdrawal = outobj.ledger[i].annualRetNeed.Value - outobj.ledger[i].otherIncomeafterRet.Value;
                    }

                    if (outobj.ledger[i].withdrawal > 0) // withdrawal
                    {
                        double retTax = 0;


                        if (pCurrentBalance > 0)//taxable p amount
                        {
                            retTax = Math.Round((pCurrentBalance / outobj.ledger[i].balance.Value) * outobj.ledger[i].withdrawal.Value * retTaxRate);

                            pCurrentBalance -= outobj.ledger[i].withdrawal.Value * (pCurrentBalance) / (outobj.ledger[i].balance.Value) * (1 + retTaxRate);
                            outobj.ledger[i].taxesOnWithdrawal += retTax;
                        }
                        pCurrentBalanceNotInterest -= outobj.ledger[i].withdrawal.Value * (pCurrentBalanceNotInterest) / (outobj.ledger[i].balance.Value);

                        if (sCurrentBalance > 0)//taxable s amount
                        {
                            retTax = Math.Round((sCurrentBalance / outobj.ledger[i].balance.Value) * outobj.ledger[i].withdrawal.Value * retTaxRate);
                            sCurrentBalance -= outobj.ledger[i].withdrawal.Value * (sCurrentBalance) / (outobj.ledger[i].balance.Value) * (1 + retTaxRate);
                            outobj.ledger[i].taxesOnWithdrawal += retTax;
                        }
                        sCurrentBalanceNotInterest -= outobj.ledger[i].withdrawal.Value * (sCurrentBalanceNotInterest) / (outobj.ledger[i].balance.Value);

                        if (oCurrentBalance > 0)//taxable other amount
                        {
                            retTax = Math.Round((oCurrentBalance / outobj.ledger[i].balance.Value) * outobj.ledger[i].withdrawal.Value * retTaxRate);
                            //oCurrentBalance -= retTax;
                            oCurrentBalance -= outobj.ledger[i].withdrawal.Value * (oCurrentBalance) / (outobj.ledger[i].balance.Value) * (1 + retTaxRate);
                            outobj.ledger[i].taxesOnWithdrawal += retTax;
                        }
                        oCurrentBalanceNotInterest -= outobj.ledger[i].withdrawal.Value * (oCurrentBalanceNotInterest) / (outobj.ledger[i].balance.Value);

                        if (balanceAmount > 0)
                        {
                            balanceAmount -= outobj.ledger[i].withdrawal.Value * (balanceAmount) / (outobj.ledger[i].balance.Value) * (1 + retTaxRate);
                        }



                    }


                    outobj.ledger[i].endingBalance = Math.Round(wCurrentBalance + oCurrentBalance + sCurrentBalance + pCurrentBalance + pCurrentBalanceNotInterest + sCurrentBalanceNotInterest + oCurrentBalanceNotInterest + wCurrentBalanceNotInterest + balanceAmount);
                    //outobj.ledger[i].endingBalance += outobj.ledger[i].growth;
                    outobj.ledger[i].endingBalance -= outobj.ledger[i].withdrawal;
                    outobj.ledger[i].endingBalance -= outobj.ledger[i].taxesOnWithdrawal;
                    //outobj.ledger[i].endingBalance += outobj.ledger[i].taxesonOtherIncomeafterRet;

                    startdate++;
                    i++;
                }

            }
            catch (Exception ex)
            {
                throw;
            }

            return outobj;

        }

        public RetirementModel FINCalculation(RetirementModel outobj)
        {

            try
            {
                int i = 0;
                double currentNeed = Convert.ToDouble(outobj.currentNeed.Value);
                double retireinteret = Convert.ToDouble(outobj.retTaxRate.Value / 100);
                double inflation = Convert.ToDouble(outobj.inflation.Value / 100);
                double retReturnTax = Convert.ToDouble(outobj.retReturnTax.Value / 100);
                double currentInterest = 0;
                double currentPrincipal = 0;
                outobj.RetirementGoal = new CLayer.RetirementGoal();
                outobj.RetirementGoal.AdditionalNeedDetails = new List<AdditionalNeed>();
                outobj.RetirementGoal.RetirementGrowDetails = new List<RetirementGrow>();
                outobj.otherValuesModel = new otherValuesModel();
                outobj.otherValuesModel.startdate = outobj.reportDate.Value.Year;
                int a = 0; int a1 = 0; int b = 0; int b1 = 0;
                //  Primary & Spouse  AGE
                int age1 = 0; int age2 = 0;
                //  RETIREMENT AGE Primary & Spouse
                int ret1 = 0; int ret2 = 0;
                //  LIFE EXPECTATION AGE Primary & Spous
                int life1 = 0;   //Primary expectancy
                int life2 = 0;   //econdary expectancy
                int r1 = 0; int r2 = 0; int r3 = 0;

                if (outobj.PersonInfoDetails.Count == 2)
                {
                    outobj.otherValuesModel.count = 2;
                    age1 = outobj.PersonInfoDetails[0].age;
                    ret1 = outobj.PersonInfoDetails[0].retAge;
                    life1 = outobj.PersonInfoDetails[0].lifeExpectancy;
                    age2 = outobj.PersonInfoDetails[1].age;
                    ret2 = outobj.PersonInfoDetails[1].retAge;
                    life2 = outobj.PersonInfoDetails[1].lifeExpectancy;

                    a = life1 - age1 + 1;      // Total life duration of primary from current date
                    a1 = life2 - age2 + 1;       // Total life duration of secondary from current date
                    b = ret1 - age1 + 1;           // total year to retire Primary
                    b1 = ret2 - age2 + 1;          // Total Year to ritire Secondary
                    int end = outobj.reportDate.Value.Year + life1 - age1;
                    int end2 = outobj.reportDate.Value.Year + life2 - age2;
                    if (end > end2)
                    {
                        outobj.otherValuesModel.enddate = end;
                    }
                    else
                    {
                        outobj.otherValuesModel.enddate = end2;
                    }
                    if (b < b1)  //checking whose have lower retirement duration 
                    {
                        r1 = b - 1;
                        //Console.WriteLine("future need loop value Ent r1 :" + r1);
                    }
                    else
                    {
                        r1 = b1 - 1;
                        // Console.WriteLine("future need loop value r1 :" + r1);
                    }

                    if (a > a1)
                    {    // for selecting whose have more life expectancy
                        r2 = age1 + r1;    // msg = "Primary live most  - First";
                        i = life1 - r2;
                        r3 = r2 - age1;
                    }
                    else
                    {
                        r2 = age2 + r1;
                        //msg = "Second Live Most - Second";
                        i = life2 - r2;
                        r3 = r2 - age2;
                    }
                }
                else if (outobj.PersonInfoDetails.Count == 1)
                {
                    outobj.otherValuesModel.count = 1;
                    age1 = outobj.PersonInfoDetails[0].age;
                    ret1 = outobj.PersonInfoDetails[0].retAge;
                    life1 = outobj.PersonInfoDetails[0].lifeExpectancy;
                    a = life1 - age1 + 1;      // Total life duration of primary from current date
                    b = ret1 - age1 + 1;           // total year to retire Primary
                    r1 = b - 1;
                    r2 = age1 + r1;      //
                    i = life1 - r2;
                    r3 = r1 - age1;
                    outobj.otherValuesModel.enddate = outobj.reportDate.Value.Year + life1 - age1;
                }
                else
                {
                    return outobj;
                }
                double f = currentNeed * (Math.Pow((1 + inflation), r3));
                outobj.RetirementGoal.monthlyNeedFuture = Math.Round(Convert.ToDecimal(f), 0);
                outobj.RetirementGoal.annualNeedFuture = Math.Round(Convert.ToDecimal(f * 12), 0);

                double independenceNumber = 0;

                int increment = 0;
                double iNumber = 0;
                for (increment = i; increment >= 0; increment += -1)
                {
                    //need for this year'
                    currentNeed = (f * 12) * (Math.Pow((1 + inflation), increment));


                    currentNeed += currentNeed * retireinteret;


                    currentInterest = (iNumber / (1 - retReturnTax)) - iNumber;


                    currentPrincipal = currentNeed - currentInterest;

                    iNumber = iNumber + currentPrincipal;
                    // Console.WriteLine( "value : " + increment +" - " + iNumber);
                }
                independenceNumber = iNumber;
                outobj.RetirementGoal.FIN = Math.Round(Convert.ToDecimal(independenceNumber), 0);
                double pCurrentBalance = 0;
                double pCurrentContribution = 0;  //sum of  personal + employer contribution
                double pCurrentBalnceNotInterest = 0;
                double pCurrentContributionNotInterest = 0;


                outobj.otherValuesModel.pCurrentBalance = 0;
                outobj.otherValuesModel.pCurrentContribution = 0;
                outobj.otherValuesModel.pCurrentBalnceNotInterest = 0;
                outobj.otherValuesModel.pCurrentContributionNotInterest = 0;

                outobj.otherValuesModel.sCurrentBalance = 0;
                outobj.otherValuesModel.sCurrentContribution = 0;
                outobj.otherValuesModel.sCurrentBalnceNotInterest = 0;
                outobj.otherValuesModel.sCurrentContributionNotInterest = 0;

                outobj.otherValuesModel.oCurrentBalance = 0;
                outobj.otherValuesModel.oCurrentContribution = 0;
                outobj.otherValuesModel.oCurrentBalnceNotInterest = 0;
                outobj.otherValuesModel.oCurrentContributionNotInterest = 0;

                outobj.otherValuesModel.wCurrentBalance = 0;
                outobj.otherValuesModel.wCurrentContribution = 0;
                outobj.otherValuesModel.wCurrentBalnceNotInterest = 0;
                outobj.otherValuesModel.wCurrentContributionNotInterest = 0;

                //double OtherBalance = 0;
                foreach (var v in outobj.currentretSummary)
                {
                    if (outobj.PersonInfoDetails.Where(w => w.personID == v.personID).Select(s => s.isprimary).FirstOrDefault() == true && v.name != "Other")
                    {
                        outobj.otherValuesModel.pName = v.name;
                    }
                    else if (outobj.PersonInfoDetails.Where(w => w.personID == v.personID).Select(s => s.isprimary).FirstOrDefault() == false && v.name != "Other")
                    {
                        outobj.otherValuesModel.sName = v.name;
                    }
                    if (v.name != "Other")
                    {

                        foreach (var vlist in v.currentRetSummaryDetails)
                        {
                            if (vlist != null)
                            {
                                if (outobj.PersonInfoDetails.Where(w => w.personID == v.personID).Select(s => s.isprimary).FirstOrDefault() == true)
                                {
                                    if (vlist.istaxed == true)
                                    {
                                        pCurrentBalance += Convert.ToDouble(vlist.planbalance.Value);
                                        outobj.otherValuesModel.pCurrentBalance += Convert.ToDouble(vlist.planbalance.Value);
                                        pCurrentContribution += Convert.ToDouble(vlist.personalContribution.Value) + Convert.ToDouble(vlist.empContribution.Value);
                                        outobj.otherValuesModel.pCurrentContribution += Convert.ToDouble(vlist.personalContribution.Value) + Convert.ToDouble(vlist.empContribution.Value);
                                    }
                                    else
                                    {
                                        pCurrentBalnceNotInterest += Convert.ToDouble(vlist.planbalance.Value);
                                        outobj.otherValuesModel.pCurrentBalnceNotInterest += Convert.ToDouble(vlist.planbalance.Value);
                                        pCurrentContributionNotInterest += Convert.ToDouble(vlist.personalContribution.Value) + Convert.ToDouble(vlist.empContribution.Value);
                                        outobj.otherValuesModel.pCurrentContributionNotInterest += Convert.ToDouble(vlist.personalContribution.Value) + Convert.ToDouble(vlist.empContribution.Value);
                                    }
                                }
                                else if (outobj.PersonInfoDetails.Where(w => w.personID == v.personID).Select(s => s.isprimary).FirstOrDefault() == false)
                                {
                                    outobj.otherValuesModel.sName = v.name;
                                    if (vlist.istaxed == true)
                                    {
                                        pCurrentBalance += Convert.ToDouble(vlist.planbalance.Value);
                                        outobj.otherValuesModel.sCurrentBalance += Convert.ToDouble(vlist.planbalance.Value);
                                        pCurrentContribution += Convert.ToDouble(vlist.personalContribution.Value) + Convert.ToDouble(vlist.empContribution.Value);
                                        outobj.otherValuesModel.sCurrentContribution += Convert.ToDouble(vlist.personalContribution.Value) + Convert.ToDouble(vlist.empContribution.Value);
                                    }
                                    else
                                    {
                                        pCurrentBalnceNotInterest += Convert.ToDouble(vlist.planbalance.Value);
                                        outobj.otherValuesModel.sCurrentBalnceNotInterest += Convert.ToDouble(vlist.planbalance.Value);
                                        pCurrentContributionNotInterest += Convert.ToDouble(vlist.personalContribution.Value) + Convert.ToDouble(vlist.empContribution.Value);
                                        outobj.otherValuesModel.sCurrentContributionNotInterest += Convert.ToDouble(vlist.personalContribution.Value) + Convert.ToDouble(vlist.empContribution.Value);
                                    }
                                }

                            }
                        }
                    }
                    else
                    {
                        foreach (var vlist in v.currentRetSummaryDetails)
                        {
                            if (vlist != null)
                            {
                                if (vlist.istaxed == true)
                                {
                                    pCurrentBalance += Convert.ToDouble(vlist.planbalance.Value);
                                    outobj.otherValuesModel.oCurrentBalance += Convert.ToDouble(vlist.planbalance.Value);
                                    pCurrentContribution += Convert.ToDouble(vlist.personalContribution.Value) + Convert.ToDouble(vlist.empContribution.Value);
                                    outobj.otherValuesModel.oCurrentContribution += Convert.ToDouble(vlist.personalContribution.Value) + Convert.ToDouble(vlist.empContribution.Value);
                                }
                                else
                                {
                                    pCurrentBalnceNotInterest += Convert.ToDouble(vlist.planbalance.Value);
                                    outobj.otherValuesModel.oCurrentBalnceNotInterest += Convert.ToDouble(vlist.planbalance.Value);
                                    pCurrentContributionNotInterest += Convert.ToDouble(vlist.personalContribution.Value) + Convert.ToDouble(vlist.empContribution.Value);
                                    outobj.otherValuesModel.oCurrentContributionNotInterest += Convert.ToDouble(vlist.personalContribution.Value) + Convert.ToDouble(vlist.empContribution.Value);
                                }
                            }
                        }
                    }

                }
                double wC = 0;
                double wCC = 0;
                double wCNotInterested = 0;
                double wCCNotInterested = 0;

                foreach (var whatList in outobj.whatif)
                {
                    if (whatList != null)
                    {
                        if (whatList.isTaxed == true)
                        {
                            wC += Convert.ToDouble(whatList.whatifplanbalance.Value);
                            outobj.otherValuesModel.wCurrentBalance += Convert.ToDouble(whatList.whatifplanbalance.Value);
                            wCC += Convert.ToDouble(whatList.whatifpersonalContribution.Value) + Convert.ToDouble(whatList.whatifmonthlyContribution.Value);
                            outobj.otherValuesModel.wCurrentContribution += Convert.ToDouble(whatList.whatifpersonalContribution.Value) + Convert.ToDouble(whatList.whatifmonthlyContribution.Value);
                        }
                        else
                        {
                            wCNotInterested += Convert.ToDouble(whatList.whatifplanbalance.Value);
                            outobj.otherValuesModel.wCurrentBalnceNotInterest += Convert.ToDouble(whatList.whatifplanbalance.Value);
                            wCCNotInterested += Convert.ToDouble(whatList.whatifpersonalContribution.Value) + Convert.ToDouble(whatList.whatifmonthlyContribution.Value);
                            outobj.otherValuesModel.wCurrentContributionNotInterest += Convert.ToDouble(whatList.whatifpersonalContribution.Value) + Convert.ToDouble(whatList.whatifmonthlyContribution.Value);
                        }
                    }
                }

                int retirementYears = r3;
                double currentValue = 0;

                int ii, jj, kk;
                double retirementProjection = 0;

                double pC = 0;
                double pCC = 0;
                double pCNotinterest = 0;
                double pCCNotinterest = 0;
                double wCa = 0;
                double wCCa = 0;
                double wCNotInteresteda = 0;
                double wCCNotInteresteda = 0;

                double TR = 0;
                int period = 0;
                double lumpsum = 0;
                double monthlySum = 0;
                double goal = 0;
                // for 3%   ie this rate = 3/12/100   

                for (kk = 0; kk <= 2; kk++)
                {
                    pC = pCurrentBalance;
                    pCC = pCurrentContribution;
                    pCNotinterest = pCurrentBalnceNotInterest;
                    pCCNotinterest = pCurrentContributionNotInterest;

                    wCa = wC;
                    wCCa = wCC;
                    wCNotInteresteda = wCNotInterested;
                    wCCNotInteresteda = wCCNotInterested;

                    if (kk == 0)
                    {
                        TR = Convert.ToDouble(outobj.rateA.Value) / 12 / 100;  //3%  3/12/100

                    }
                    else if (kk == 1)
                    {
                        TR = Convert.ToDouble(outobj.rateB.Value) / 12 / 100;   // 6%  6/12/100

                    }
                    else
                    {
                        TR = Convert.ToDouble(outobj.rateC.Value) / 12 / 100;
                    }
                    double whatiftotal = 0;
                    for (ii = 0; ii <= retirementYears; ii++)
                    {
                        currentValue = (pC + pCNotinterest+ whatiftotal); // current value for each year till retirement

                        for (jj = 1; jj <= 12; jj++)
                        {
                            pC = pC * (1 + TR) + pCC;
                            pCNotinterest = pCNotinterest * (1 + TR) + pCCNotinterest;             //pCNotinterest + pCCNotinterest;

                            if (wC + wCC + wCNotInterested + wCCNotInterested > 0)
                            {
                                if (retirementYears >= System.DateTime.Now.Year)
                                {
                                    wCa = wCa * (1 + TR) + wCCa;
                                    wCNotInterested= wCNotInterested * (1 + TR) + wCCNotInteresteda;
                                    whatiftotal = wCa + wCNotInterested;
                                }
                                else
                                {
                                    whatiftotal = 0;
                                }
                            }
                        }

                    }

                    retirementProjection = Math.Round(currentValue, 0);
                    goal = Math.Round(retirementProjection - independenceNumber, 0);
                    decimal ourate = Convert.ToDecimal(Math.Round((TR * 12 * 100), 0));

                    outobj.RetirementGoal.RetirementGrowDetails.Add(new RetirementGrow
                    {
                        rate = ourate,
                        amount = Convert.ToDecimal(Math.Round(retirementProjection, 0)),
                        shortfallsurplus = Convert.ToDecimal(Math.Round(goal, 0))
                    });
                    //Console.WriteLine(Math.Round((TR * 12 * 100), 0) + "%-At retirement, your accounts will grow to : " + retirementProjection);
                    //Console.WriteLine(" A Shortfall of  : " + goal);

                    goal = goal * -1;
                    lumpsum = Math.Round((goal) / (Math.Pow((1 + TR), period)), 0);
                    string lump;
                    if (lumpsum < 0)
                    {
                        lump = "On Target";
                    }
                    else
                    {
                        lump = lumpsum.ToString();
                    }
                    period = r1 * 12;
                    //Console.WriteLine(Math.Round((TR * 12 * 100), 0) + "% Lumpsum :" + Math.Round(lumpsum, 0));
                    monthlySum = Math.Round((goal * TR) / ((Math.Pow((1 + TR), period)) - 1), 0);
                    string month;
                    if (monthlySum < 0)
                    {
                        month = "On Target";
                    }
                    else
                    {
                        month = monthlySum.ToString()+ " Per month";
                    }
                    //Console.WriteLine(Math.Round((TR * 12 * 100), 0) + "% Monthly sum :" + Math.Round(monthlySum, 0));

                    outobj.RetirementGoal.AdditionalNeedDetails.Add(new AdditionalNeed
                    {
                        rate = ourate,
                        lumpsum = Convert.ToDecimal(lumpsum),
                        lumpsumstring = lump,
                        monthly = Convert.ToDecimal(monthlySum),
                        monthlyString = month
                    });
                }
                outobj.currentretSummaryTotal = new currentRetSummaryTotal();
                outobj.currentretSummaryTotal.totalplanbalance = outobj.currentretSummary.Sum(sum => sum.totalplanbalance) + outobj.whatif.Sum(w => w.whatifplanbalance);
                outobj.currentretSummaryTotal.totalempContribution = outobj.currentretSummary.Sum(sum => sum.totalempContribution) + outobj.whatif.Sum(w => w.whatifmonthlyContribution);
                outobj.currentretSummaryTotal.totalpersonalContribution = outobj.currentretSummary.Sum(sum => sum.totalpersonalContribution) + outobj.whatif.Sum(w => w.whatifpersonalContribution);

            }
            catch (Exception ex)
            {
                throw;
            }
            outobj = retirementLedeger(outobj);
            return outobj;
        }

        private RetirementModel retirementLedeger(RetirementModel outobj)
        {
            int startdate = outobj.otherValuesModel.startdate.Value;

            List<IncomeProjectionModel> incomeList = new List<IncomeProjectionModel>();
            DLayer.IncomeDetails IDL = new IncomeDetails();
            incomeList = IDL.incomeProjection(outobj.profileID.Value);
            outobj.ledger = new List<RetirementledgerModel>();
            try
            {
                int i = 0;
                int pAge = 0;
                int sAge = 0;
                int plife = 0;
                int slife = 0;
                int pret = 0;
                int sret = 0;
                double tax = 0;
                int repdateyear = outobj.reportDate.Value.Year;
                double retTaxRate = Convert.ToDouble(outobj.retTaxRate.Value / 100);
                double inflation = Convert.ToDouble(outobj.inflation.Value / 100);
                double retReturnTax = Convert.ToDouble(outobj.retReturnTax.Value / 100);
                double preRetTax = Convert.ToDouble(outobj.preRetTax.Value / 100);
                int months = 0;
                int wmonths = 0;
                int w = 0;
                //taxable amount
                double pPlanAmtTaxable = 0;
                double sPlanAmtTaxable = 0;
                double oPlanAmtTaxable = 0;
                double wPlanAmtTaxable = 0;
                //tax free amount
                double pPlanAmtTaxfree = 0;
                double sPlanAmtTaxfree = 0;
                double oPlanAmtTaxfree = 0;
                double wPlanAmtTaxfree = 0;
                //propotional percentage with total retire balance
                double pPlanTotalTax = 0;
                double sPlanTotalTax = 0;
                double oPlanTotalTax = 0;
                double wPlanTotalTax = 0;
                double pPlanTotalTaxRoth = 0;
                double sPlanTotalTaxRoth = 0;
                double oPlanTotalTaxRoth = 0;
                double wPlanTotalTaxRoth = 0;
                double totalRetirement = 1;

                //extra values

                double otherBalance = 0;
                double currentInterest = 0;
                double currentIncome = 0;
                double currentNeed = 0;
                double otherincome = 0; // aavo
                double retireIncome = 0;
                double totalBalance = 0;

                if (outobj.currentretSummaryTotal.totalplanbalance != 0)
                    totalRetirement = Convert.ToDouble(outobj.currentretSummaryTotal.totalplanbalance);

                #region plan balance and taxes
                double pCurrentBalance = outobj.otherValuesModel.pCurrentBalance;
                pPlanTotalTax = Math.Round(pCurrentBalance / totalRetirement, 2);
                double pCurrentBalanceRoth = outobj.otherValuesModel.pCurrentBalnceNotInterest;
                pPlanTotalTaxRoth = Math.Round(pCurrentBalanceRoth / totalRetirement, 2);
                double pCurrentContribution = outobj.otherValuesModel.pCurrentContribution;
                double pCurrentContributionRoth = outobj.otherValuesModel.pCurrentContributionNotInterest;

                double sCurrentBalance = outobj.otherValuesModel.sCurrentBalance;
                sPlanTotalTax = Math.Round(sCurrentBalance / totalRetirement, 2);
                double sCurrentBalanceRoth = outobj.otherValuesModel.sCurrentBalnceNotInterest;
                sPlanTotalTaxRoth = Math.Round(sCurrentBalanceRoth / totalRetirement, 2);
                double sCurrentContribution = outobj.otherValuesModel.sCurrentContribution;
                double sCurrentContributionRoth = outobj.otherValuesModel.sCurrentContributionNotInterest;

                double oCurrentBalance = outobj.otherValuesModel.oCurrentBalance;
                oPlanTotalTax = Math.Round(oCurrentBalance / totalRetirement, 2);
                double oCurrentBalanceRoth = outobj.otherValuesModel.oCurrentBalnceNotInterest;
                oPlanTotalTaxRoth = Math.Round(oCurrentBalanceRoth / totalRetirement, 2);
                double oCurrentContribution = outobj.otherValuesModel.oCurrentContribution;
                double oCurrentContributionRoth = outobj.otherValuesModel.oCurrentContributionNotInterest;

                double CurrentWhatifBalance = outobj.otherValuesModel.wCurrentBalance;
                wPlanTotalTax = Math.Round(CurrentWhatifBalance / totalRetirement, 2);
                double CurrentWhatifBalanceRoth = outobj.otherValuesModel.wCurrentBalnceNotInterest;
                wPlanTotalTaxRoth = Math.Round(CurrentWhatifBalanceRoth / totalRetirement, 2);
                double CurrentWhatifContribution = outobj.otherValuesModel.wCurrentContribution;
                double CurrentWhatifContributionRoth = outobj.otherValuesModel.wCurrentContributionNotInterest;

                pPlanAmtTaxable = pCurrentBalance;
                pPlanAmtTaxfree = pCurrentBalanceRoth;
                sPlanAmtTaxable = sCurrentBalance;
                sPlanAmtTaxfree = sCurrentBalanceRoth;
                oPlanAmtTaxable = oCurrentBalance;
                oPlanAmtTaxfree = oCurrentBalance;
                wPlanAmtTaxable = CurrentWhatifBalance;
                wPlanAmtTaxfree = CurrentWhatifBalanceRoth;

                Double planTaxableRate = 0;
                planTaxableRate = ((pPlanAmtTaxable + sPlanAmtTaxable + oPlanAmtTaxable + wPlanAmtTaxable) / totalRetirement);
                #endregion plan balance 

                if (outobj.otherValuesModel.count == 2)
                {
                    pAge = outobj.PersonInfoDetails.Where(p => p.isprimary == true).Select(s => s.age).FirstOrDefault();
                    plife = outobj.PersonInfoDetails.Where(p => p.isprimary == true).Select(s => s.lifeExpectancy).FirstOrDefault();
                    pret = outobj.PersonInfoDetails.Where(p => p.isprimary == true).Select(s => s.retAge).FirstOrDefault();
                    sAge = outobj.PersonInfoDetails.Where(p => p.isprimary == false).Select(s => s.age).FirstOrDefault();
                    slife = outobj.PersonInfoDetails.Where(p => p.isprimary == false).Select(s => s.lifeExpectancy).FirstOrDefault();
                    sret = outobj.PersonInfoDetails.Where(p => p.isprimary == false).Select(s => s.retAge).FirstOrDefault();

                }
                else if (outobj.otherValuesModel.count == 1)
                {
                    if (outobj.PersonInfoDetails[0].isprimary == true)
                    {
                        pAge = outobj.PersonInfoDetails.Where(p => p.isprimary == true).Select(s => s.age).FirstOrDefault();
                        plife = outobj.PersonInfoDetails.Where(p => p.isprimary == true).Select(s => s.lifeExpectancy).FirstOrDefault();
                        pret = outobj.PersonInfoDetails.Where(p => p.isprimary == true).Select(s => s.retAge).FirstOrDefault();
                    }
                    else if (outobj.PersonInfoDetails[0].isprimary == false)
                    {
                        sAge = outobj.PersonInfoDetails.Where(p => p.isprimary == false).Select(s => s.age).FirstOrDefault();
                        slife = outobj.PersonInfoDetails.Where(p => p.isprimary == false).Select(s => s.lifeExpectancy).FirstOrDefault();
                        sret = outobj.PersonInfoDetails.Where(p => p.isprimary == false).Select(s => s.retAge).FirstOrDefault();
                    }
                }
                else
                {
                    outobj.ledger = null;
                    return outobj;
                }

                while (startdate <= outobj.otherValuesModel.enddate.Value)
                {
                    outobj.ledger.Add(new RetirementledgerModel
                    {
                        year = startdate,
                        otherIncomeafterRet = 0,
                        taxesonOtherIncomeafterRet = 0,
                        annualContribution = 0,
                        endingBalance = 0,
                        growth = 0,
                        annualRetNeed = 0,
                        taxesOnWithdrawal = 0,
                        withdrawal = 0,

                    });
                    #region age increment primary and spouse
                    if (pAge > 0 && plife >= pAge)
                    {
                        outobj.ledger[i].pAge = pAge;
                        pAge++;
                    }
                    else
                    {
                        outobj.ledger[i].pAge = null;
                    }

                    if (sAge > 0 && slife >= sAge)
                    {
                        outobj.ledger[i].sAge = sAge;
                        sAge++;
                    }
                    else
                    {
                        outobj.ledger[i].sAge = null;
                    }
                    #endregion
                    outobj.ledger[i].balance = Math.Round(pCurrentBalance + pCurrentBalanceRoth + sCurrentBalance + sCurrentBalanceRoth + oCurrentBalance + oCurrentBalanceRoth + otherBalance);
                    if (startdate >= System.DateTime.Now.Year)
                    {

                        outobj.ledger[i].balance += Math.Round(CurrentWhatifBalance + CurrentWhatifBalanceRoth);
                    }                     
                    currentInterest = 0;
                    if (i == 0)//first year
                    {
                        months = 12 - (outobj.reportDate.Value.Month - 1);
                    }
                    else //from second year
                    {
                        months = 12;
                    }
                    if (pCurrentBalance + pCurrentBalanceRoth + sCurrentBalance + sCurrentBalanceRoth + otherBalance + oCurrentBalance + oCurrentBalanceRoth > 0)
                    {
                        tax = (outobj.ledger[i].pAge <= pret) ? preRetTax : retReturnTax;
                        if (pCurrentBalance > 0)
                        {
                            currentInterest += (pCurrentBalance) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - pCurrentBalance;
                            pCurrentBalance += (pCurrentBalance) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - pCurrentBalance;
                        }
                        if (pCurrentBalanceRoth > 0)
                        {
                            currentInterest += (pCurrentBalanceRoth) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - pCurrentBalanceRoth;
                            pCurrentBalanceRoth += (pCurrentBalanceRoth) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - pCurrentBalanceRoth;
                        }
                        tax = (outobj.ledger[i].sAge <= sret) ? preRetTax : retReturnTax;
                        if (sCurrentBalance > 0)
                        {
                            currentInterest += (sCurrentBalance) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - sCurrentBalance;
                            sCurrentBalance += (sCurrentBalance) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - sCurrentBalance;
                        }
                        if (sCurrentBalanceRoth > 0)
                        {
                            currentInterest += (sCurrentBalanceRoth) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - sCurrentBalanceRoth;
                            sCurrentBalanceRoth += (sCurrentBalanceRoth) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - sCurrentBalanceRoth;
                        }
                        //tax = (outobj.ledger[i].sAge <= sret) ? preRetTax : retReturnTax;
                        //if (sCurrentBalance > 0)
                        //{
                        //    currentInterest += (sCurrentBalance) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - sCurrentBalance;
                        //    sCurrentBalance += (sCurrentBalance) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - sCurrentBalance;
                        //}
                        //if (pCurrentBalanceRoth > 0)
                        //{
                        //    currentInterest += (sCurrentBalanceRoth) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - sCurrentBalanceRoth;
                        //    sCurrentBalanceRoth += (sCurrentBalanceRoth) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - sCurrentBalanceRoth;
                        //}
                        tax = (outobj.ledger[i].pAge != null && outobj.ledger[i].pAge < pret) || (outobj.ledger[i].sAge != null && outobj.ledger[i].sAge < sret) ? preRetTax : retReturnTax;
                        if (oCurrentBalance > 0)
                        {
                            currentInterest += (oCurrentBalance) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - oCurrentBalance;
                            oCurrentBalance += (oCurrentBalance) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - oCurrentBalance;
                        }
                        if (oCurrentBalanceRoth > 0)
                        {
                            currentInterest += (oCurrentBalanceRoth) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - oCurrentBalanceRoth;
                            oCurrentBalanceRoth += (oCurrentBalanceRoth) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - oCurrentBalanceRoth;
                        }
                    }
                    if (startdate >= System.DateTime.Now.Year)
                    {
                        if (CurrentWhatifBalance + CurrentWhatifBalanceRoth > 0)
                        {
                            if (w == 0)
                            {
                                wmonths = 12 - (System.DateTime.Now.Month - 1);
                                w = 1;
                            }
                            else //from second year
                            {
                                wmonths = 12;
                            }
                            tax = (outobj.ledger[i].pAge != null && outobj.ledger[i].pAge < pret) || (outobj.ledger[i].sAge != null && outobj.ledger[i].sAge < sret) ? preRetTax : retReturnTax;
                            if (CurrentWhatifBalance > 0)
                            {
                                currentInterest += (CurrentWhatifBalance) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - CurrentWhatifBalance;
                                CurrentWhatifBalance += (CurrentWhatifBalance) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - CurrentWhatifBalance;
                            }
                            if (CurrentWhatifBalanceRoth > 0)
                            {
                                currentInterest += (CurrentWhatifBalanceRoth) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - CurrentWhatifBalanceRoth;
                                CurrentWhatifBalanceRoth += (CurrentWhatifBalanceRoth) * Math.Pow((1 + tax), Math.Round(Convert.ToDouble(months) / 12, 2)) - CurrentWhatifBalanceRoth;
                            }
                        }
                        if (CurrentWhatifContributionRoth + CurrentWhatifContribution > 0)
                        {
                            if ((outobj.ledger[i].pAge != null && outobj.ledger[i].pAge < pret) || (outobj.ledger[i].sAge != null && outobj.ledger[i].sAge < sret))
                            {
                                outobj.ledger[i].annualContribution += (CurrentWhatifContribution + CurrentWhatifContributionRoth) * wmonths;
                                CurrentWhatifBalance += (CurrentWhatifContribution) * wmonths;
                                CurrentWhatifBalanceRoth += (CurrentWhatifContributionRoth) * wmonths;
                            }
                        }
                    }

                    if (otherBalance > 0)
                    {
                        currentInterest += (otherBalance) * Math.Pow((1 + retReturnTax), Math.Round(Convert.ToDouble(months) / 12, 2)) - otherBalance;
                        otherBalance += (otherBalance) * Math.Pow((1 + retReturnTax), Math.Round(Convert.ToDouble(months) / 12, 2)) - otherBalance;
                    }

                    if (pCurrentContribution + pCurrentContributionRoth > 0)
                    {
                        if (outobj.ledger[i].pAge != null && outobj.ledger[i].pAge < pret)
                        {
                            outobj.ledger[i].annualContribution += (pCurrentContributionRoth + pCurrentContribution) * months;
                            pCurrentBalance += pCurrentContribution * months;
                            pCurrentBalanceRoth += pCurrentContributionRoth * months;
                        }
                    }
                    if (sCurrentContributionRoth + sCurrentContribution > 0)
                    {
                        if (outobj.ledger[i].sAge != null && outobj.ledger[i].sAge < sret)
                        {
                            outobj.ledger[i].annualContribution += (sCurrentContributionRoth + sCurrentContribution) * months;
                            sCurrentBalance += (sCurrentContribution) * months;
                            sCurrentBalanceRoth += (sCurrentContributionRoth) * months;
                        }
                    }
                    if (oCurrentContributionRoth + oCurrentContribution > 0)
                    {
                        if ((outobj.ledger[i].pAge != null && outobj.ledger[i].pAge < pret) || (outobj.ledger[i].sAge != null && outobj.ledger[i].sAge < sret))
                        {
                            outobj.ledger[i].annualContribution += (oCurrentContributionRoth + oCurrentContribution) * months;
                            oCurrentBalance += (oCurrentContribution) * months;
                            oCurrentBalanceRoth += (oCurrentContributionRoth) * months;
                        }
                    }

                    double currentOtherTax = 0;
                    outobj.ledger[i].growth = Math.Round(currentInterest);
                    if ((outobj.ledger[i].sAge >= 0 && outobj.ledger[i].sAge >= sret && outobj.ledger[i].sAge <= slife) || (outobj.ledger[i].pAge >= 0 && outobj.ledger[i].pAge >= pret && outobj.ledger[i].pAge <= plife))
                    {

                        double futureneed = Convert.ToDouble(outobj.currentNeed) * (Math.Pow(Convert.ToDouble(1 + outobj.inflation / 100), i));
                        currentNeed = Math.Round(futureneed * 12);
                        outobj.ledger[i].annualRetNeed = currentNeed;

                        //var a = incomeList.Where(x => x.IncomeProjectionDetails.Where(o => o.year == startdate).Select(s=>s.amount).Se;
                        currentIncome = 0;
                        if (currentNeed > 0)
                        {
                            foreach (var a in incomeList)
                            {
                                currentIncome += Convert.ToDouble(a.IncomeProjectionDetails.Where(o => o.year == startdate).Select(s => s.amount).FirstOrDefault());
                                currentOtherTax += Convert.ToDouble(a.IncomeProjectionDetails.Where(o => o.year == startdate && o.Tax != null).Select(s => s.Tax).FirstOrDefault());
                                if (a.role == "Other")
                                {
                                    otherincome = Convert.ToDouble(a.IncomeProjectionDetails.Where(o => o.year == startdate).Select(s => s.amount).FirstOrDefault());
                                }

                            }
                        }
                    }
                    if (currentIncome > 0)
                    {
                        outobj.ledger[i].otherIncomeafterRet = currentIncome;
                        outobj.ledger[i].taxesonOtherIncomeafterRet = currentOtherTax;
                    }
                    if (currentIncome+ outobj.ledger[i].taxesonOtherIncomeafterRet >= currentNeed)
                    {
                        retireIncome = 0;
                        if (currentIncome + outobj.ledger[i].taxesonOtherIncomeafterRet - currentNeed > otherincome)
                        {
                            otherBalance += otherincome;
                        }
                        else
                        {
                            otherBalance += outobj.ledger[i].taxesonOtherIncomeafterRet.Value +currentIncome - currentNeed;
                        }
                    }
                    else
                    {
                        retireIncome = currentNeed - currentIncome- outobj.ledger[i].taxesonOtherIncomeafterRet.Value;
                    }
                    outobj.ledger[i].withdrawal = retireIncome;
                    totalBalance = Math.Abs(pCurrentBalanceRoth) + Math.Abs(pCurrentBalance) + Math.Abs(sCurrentBalanceRoth) + Math.Abs(sCurrentBalance) + Math.Abs(otherBalance);
                    totalBalance += Math.Abs(CurrentWhatifBalance) + Math.Abs(CurrentWhatifBalanceRoth);
                    totalBalance += Math.Abs(oCurrentBalance) + Math.Abs(oCurrentBalanceRoth);

                    if (totalBalance == 0)
                    { totalBalance = .0000000001; }
                    double pTaxableAmount = 0; double pTaxFreeAmount = 0;
                    double sTaxableAmount = 0; double sTaxFreeAmount = 0;
                    double oTaxableAmount = 0; double oTaxFreeAmount = 0;
                    double temTotalTax = 0;
                    if (retireIncome > 0)
                    {
                        pCurrentBalanceRoth -= retireIncome * (Math.Abs(pCurrentBalanceRoth) / (totalBalance));
                        sCurrentBalanceRoth -= retireIncome * (Math.Abs(sCurrentBalanceRoth) / (totalBalance));
                        oCurrentBalanceRoth -= retireIncome * (Math.Abs(oCurrentBalanceRoth) / (totalBalance));
                        CurrentWhatifBalanceRoth -= retireIncome * (Math.Abs(CurrentWhatifBalanceRoth) / (totalBalance));

                        currentNeed += retireIncome * (Math.Abs(pCurrentBalance) / (totalBalance));
                        pCurrentBalance -= retireIncome * (Math.Abs(pCurrentBalance) / (totalBalance)) * (1 + retTaxRate);

                        currentNeed += retireIncome * (Math.Abs(sCurrentBalance) / (totalBalance));
                        sCurrentBalance -= retireIncome * (Math.Abs(sCurrentBalance) / (totalBalance)) * (1 + retTaxRate);

                        currentNeed += retireIncome * (Math.Abs(oCurrentBalance) / (totalBalance));
                        oCurrentBalance -= retireIncome * (Math.Abs(oCurrentBalance) / (totalBalance)) * (1 + retTaxRate);

                        currentNeed += retireIncome * (Math.Abs(otherBalance) / (totalBalance));
                        otherBalance -= retireIncome * (Math.Abs(otherBalance) / (totalBalance)) * (1 + retTaxRate);

                        currentNeed += retireIncome * (Math.Abs(CurrentWhatifBalance) / (totalBalance));
                        CurrentWhatifBalance -= retireIncome * (Math.Abs(CurrentWhatifBalance) / (totalBalance)) * (1 + retTaxRate);
                        temTotalTax = retireIncome;
                        if (pCurrentBalance + sCurrentBalance + oCurrentBalance + otherBalance + CurrentWhatifBalance > 0)
                        {
                            //pTaxableAmount = (temTotalTax * pPlanTotalTax) * retTaxRate ;
                            //sTaxableAmount = (temTotalTax * sPlanTotalTax) * retTaxRate ;
                            //oTaxableAmount = (temTotalTax * oPlanTotalTax) * retTaxRate ;

                            //pTaxFreeAmount = (temTotalTax * pPlanTotalTaxRoth) * retTaxRate ;
                            //sTaxFreeAmount = (temTotalTax * sPlanTotalTaxRoth) * retTaxRate ;
                            //oTaxFreeAmount = (temTotalTax * oPlanTotalTaxRoth) * retTaxRate ;

                            pTaxableAmount = (temTotalTax * (Math.Abs(pCurrentBalance) / (totalBalance))) * retTaxRate;
                            sTaxableAmount = (temTotalTax * (Math.Abs(sCurrentBalance) / (totalBalance))) * retTaxRate;
                            oTaxableAmount = (temTotalTax * (Math.Abs(oCurrentBalance) / (totalBalance))) * retTaxRate;
                            pTaxFreeAmount = (temTotalTax * (Math.Abs(pCurrentBalanceRoth) / (totalBalance))) * retTaxRate;
                            sTaxFreeAmount = (temTotalTax * (Math.Abs(sCurrentBalanceRoth) / (totalBalance))) * retTaxRate;
                            oTaxFreeAmount = (temTotalTax * (Math.Abs(oCurrentBalanceRoth) / (totalBalance))) * retTaxRate;

                            if (oTaxableAmount < 0) oTaxableAmount = 0;
                            if (oTaxFreeAmount < 0) oTaxFreeAmount = 0;
                        }
                        outobj.ledger[i].taxesOnWithdrawal = Math.Round(pTaxableAmount + sTaxableAmount + oTaxableAmount);

                    }
                  
                     outobj.ledger[i].endingBalance = Math.Round(pCurrentBalance + pCurrentBalanceRoth + sCurrentBalance + sCurrentBalanceRoth + oCurrentBalance + oCurrentBalanceRoth + otherBalance) ;
                    if (startdate >= System.DateTime.Now.Year)
                    {
                        outobj.ledger[i].endingBalance += Math.Round(CurrentWhatifBalance + CurrentWhatifBalanceRoth);
                    }

                    startdate++;
                    i++;
                }


            }
            catch (Exception ex)
            {
                throw;
            }

            return outobj;

        }
    }
}
