﻿using CLayer;
using System;
using CLayer.common;
using CLayer.DBModel;
using DLayer;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CLayer.AdvisorModel;
using static System.Net.Mime.MediaTypeNames;
using DLayer.Common;
using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;

namespace DLayer
{
    public class EducationInformation
    {
        public EducationModel GetEducationInformation(long profileID)
        {

            EducationModel em = new EducationModel();
            List<Education> edu = new List<Education>();
            DefaultsEducation eduDefault = new DefaultsEducation();
            //  DefaultsGlobal globalDefaults = new DefaultsGlobal();
            DefaultsReg dr = new DefaultsReg();
            int i = -1;
            using (var db = Connection.getConnect())
            {
                try
                {
                    if (profileID > 0)
                    {
                        eduDefault = db.DefaultsEducations.SingleOrDefault(b => b.profileID == profileID);
                        if (eduDefault == null)
                        {
                            eduDefault = new DefaultsEducation();
                            eduDefault = db.DefaultsEducations.Join(db.profiles.Where(x => x.profileID == profileID), d => d.clientID, p => p.clientID, (d, p) => d).ToList()
                                .Select(y => new DefaultsEducation
                                {
                                    collageYears = y.collageYears,
                                    collegeAge = y.collegeAge,
                                    collegeStartMonth = y.collegeStartMonth,
                                    rate1 = y.rate1,
                                    rate2 = y.rate2,
                                    rate3 = y.rate3,
                                    tutionFeeInflation = y.tutionFeeInflation,
                                    stateTypeId = y.stateTypeId,
                                    parentPay = y.parentPay,
                                }).FirstOrDefault();
                        }
                        if (eduDefault == null)
                        { throw new Exception("Education Default not found."); }
                        //globalDefaults = db.DefaultsGlobals.SingleOrDefault(b => b.profileID == profileID);
                      
                        edu = db.Educations.Where(b => b.profileID == profileID && b.isActive == true).ToList();

                        if (edu != null && eduDefault != null )
                        {
                            em.EducationInfoModel = new List<EducationInfoModel>();

                            foreach (var item in edu)//fetching saved Details with Deafult values
                            {
                                i++;
                                if (item.educationID > 0)
                                {
                                    if (item.IsParent == true)
                                    {
                                        var a = em.EducationInfoModel.Where(w => w.personID == item.personID).Count();
                                        if (a == 0)
                                        {
                                            em.EducationInfoModel.Add(fillEducationdbtomodel(item, eduDefault));
                                        }
                                    }
                                    if (item.IsParent == false)
                                    {
                                        var a = em.EducationInfoModel.Where(w => w.childID == item.childID).Count();
                                        if (a == 0)
                                        {
                                            em.EducationInfoModel.Add(fillEducationdbtomodel(item, eduDefault));
                                        }
                                    }

                                   // em.EducationInfoModel.Add(fillEducationdbtomodel(item, eduDefault));
                                }
                            }
                            //Education Output calculation based on education input values-with saved details
                            em = educationCalculation(em, eduDefault);
                            em.profileID = profileID;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            return em;
        }

        public decimal? totalCurrentsavings(List<Education_DetailsModel> CurrentSavingsDetails)
        {
            decimal d = 0;
            if (CurrentSavingsDetails != null)
            {
                foreach (var item in CurrentSavingsDetails)
                {
                    if (item.detailID > 0)
                    {
                        d = d + (item.amount ?? 0);
                    }
                }
            }
            return d;
        }

        public decimal? totalMonthlyContribution(List<Education_DetailsModel> CurrentSavingsDetails)
        {
            decimal d = 0;
            if (CurrentSavingsDetails != null)
            {
                foreach (var item in CurrentSavingsDetails)
                {
                    if (item.detailID > 0)
                    {
                        d = d + (item.monthly ?? 0);
                    }
                }
            }
            return d;
        }

        public List<Education_DetailsModel> getCurrentSavingDetails(long educationID)
        {
            List<Education_Details> eduDB = new List<Education_Details>();
            List<Education_DetailsModel> eduModel = new List<Education_DetailsModel>();
            int i = -1;
            using (var db = Connection.getConnect())
            {
                try
                {
                    if (educationID > 0)
                    {

                        eduDB = db.Education_Details.Where(b => b.educationID == educationID && b.isActive == true).ToList();
                        if (eduDB != null && eduDB.Count > 0)
                        {
                            foreach (var item in eduDB)
                            {
                                if (item.detailID > 0)
                                {
                                    eduModel.Add(fillCurrentSavingsdbtomodel(item, educationID));
                                }
                            }

                        }
                        else
                        {
                            eduModel = new List<Education_DetailsModel>();
                            eduModel.Add(new Education_DetailsModel { educationID = educationID });
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            return eduModel;
        }
        public List<Education_DetailsModel> getCurrentSavingDetails2(long educationID)
        {
            List<Education_Details> eduDB = new List<Education_Details>();
            List<Education_DetailsModel> eduModel = new List<Education_DetailsModel>();
            int i = -1;
            using (var db = Connection.getConnect())
            {
                try
                {
                    if (educationID > 0)
                    {

                        eduDB = db.Education_Details.Where(b => b.educationID == educationID && b.isActive == true).ToList();
                        if (eduDB != null && eduDB.Count > 0)
                        {
                            foreach (var item in eduDB)
                            {
                                if (item.detailID > 0)
                                {
                                    eduModel.Add(fillCurrentSavingsdbtomodel(item, educationID));
                                }
                            }

                        }
                        else
                        {
                            return null;
                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            return eduModel;
        }

        public Education_DetailsModel fillCurrentSavingsdbtomodel(Education_Details item, long educationID)
        {
            Education_DetailsModel obj = new Education_DetailsModel()
            {
                detailID = item.detailID,
                amount = item.amount,
                cID = item.cID,
                amountValueAsOf = item.amountValueAsOf,
                company = item.company,
                educationTypeID = item.educationTypeID,
                educationID = educationID,
                monthly = item.monthly,
                notes = item.notes
            };
            return obj;
        }

        public EducationModel saveEducationInfo(EducationModel em, long userID)
        {
            DefaultsEducation eduDefault = new DefaultsEducation();


            using (var db = Connection.getConnect())
            {

                List<EducationInfoModel> ed = new List<EducationInfoModel>();
                try
                {
                    if (em.EducationInfoModel.Count>0)
                    {
                        eduDefault = db.DefaultsEducations.SingleOrDefault(b => b.profileID == em.profileID);
                        if (eduDefault == null)
                        {
                            eduDefault = new DefaultsEducation();
                            eduDefault = db.DefaultsEducations.Join(db.profiles.Where(x => x.profileID == em.profileID), d => d.clientID, p => p.clientID, (d, p) => d).ToList()
                                .Select(y => new DefaultsEducation
                                {
                                    collageYears = y.collageYears,
                                    collegeAge = y.collegeAge,
                                    collegeStartMonth = y.collegeStartMonth,
                                    rate1 = y.rate1,
                                    rate2 = y.rate2,
                                    rate3 = y.rate3,
                                    tutionFeeInflation = y.tutionFeeInflation,
                                    stateTypeId = y.stateTypeId,
                                    parentPay = y.parentPay

                                }).FirstOrDefault();
                            if (eduDefault == null)
                            {
                                throw new Exception("Education Default not found.");
                            }
                        }
                   
                      


                        foreach (var item in em.EducationInfoModel)//fetching saved Details with Deafult values
                            {

                            item.profileID = em.profileID.Value;
                                if (item.profileID > 0)
                                {
                                    ed.Add(saveEduInfo(item, eduDefault, userID));
                                }
                            }
                           //Education Output calculation based on education input values-with saved details
                          //em.EducationInfoModel = ed;
                         // em = educationCalculation(em, eduDefault);

                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return em;
        }

        public List<Education_DetailsModel> saveCurrentSavings(List<Education_DetailsModel> currentSavingsDetails, long userID)
        {
            Education_Details ed = new Education_Details();
            if (currentSavingsDetails != null)
            {
                using (var db = Connection.getConnect())
                {
                    try
                    {
                        if (currentSavingsDetails != null)
                        {
                            int i = 0;
                            foreach (var item in currentSavingsDetails)
                            {
                                if (item != null)
                                {
                                    if (item.detailID > 0)
                                    {
                                        ed = db.Education_Details.SingleOrDefault(b => b.educationID == item.educationID && b.detailID == item.detailID);
                                        ed = fillEduDetailModelToObj(ed, item);
                                        ed.detailsUpdatedDate = System.DateTime.Now;
                                        ed.detailsModifiedBy = userID;
                                        db.Entry(ed).State = System.Data.Entity.EntityState.Modified;
                                        db.SaveChanges();
                                    }
                                    else
                                    {
                                        ed = fillEduDetailModelToObj(ed, item);
                                        ed.detailsCreatedDate = System.DateTime.Now;
                                        ed.isActive = true;
                                        ed.detailsModifiedBy = userID;
                                        db.Education_Details.Add(ed);
                                        db.SaveChanges();
                                        currentSavingsDetails[i].detailID = ed.detailID;
                                    }
                                }
                                i++;
                            }
                        }
                        else
                        {
                            return null;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }

                }
            }
            return currentSavingsDetails;
        }

        public Education_Details fillEduDetailModelToObj(Education_Details ed, Education_DetailsModel obj)
        {

            ed.educationID = obj.educationID;
            ed.educationTypeID = obj.educationTypeID;
            ed.company = obj.company;
            ed.amount = obj.amount;
            ed.amountValueAsOf = obj.amountValueAsOf;
            ed.monthly = obj.monthly ?? 0;
            ed.notes = obj.notes;
            return ed;
        }

        public bool deleteCurrentSavings(long deleteID, long userID)
        {
            if (deleteID > 0 && userID > 0)
            {
                using (var db = Connection.getConnect())
                    try
                    {
                        var ed = db.Education_Details.FirstOrDefault(s => s.detailID == deleteID);
                        if (ed != null && ed.detailID > 0)
                        {
                            ed.isActive = false;
                            ed.detailsModifiedBy = userID;
                            ed.detailsUpdatedDate = System.DateTime.Now;
                            db.Entry(ed).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                return true;
            }
            return false;

        }

        public EducationModel educationCalculation(EducationModel em, DefaultsEducation eduDefault)
        {

            if (eduDefault == null)
            {
                using (var db = Connection.getConnect())
                {

                    eduDefault = new DefaultsEducation();

                    eduDefault = db.DefaultsEducations.SingleOrDefault(b => b.profileID == em.profileID);
                    em.reportDate = db.profiles.Where(v => v.profileID == em.profileID).Select(v => v.profileDate).FirstOrDefault();
                    if (eduDefault == null)
                    {
                        eduDefault = new DefaultsEducation();
                        eduDefault = db.DefaultsEducations.Join(db.profiles.Where(x => x.profileID == em.profileID), d => d.clientID, p => p.clientID, (d, p) => d).ToList()
                            .Select(y => new DefaultsEducation
                            {
                                collageYears = y.collageYears,
                                collegeAge = y.collegeAge,
                                collegeStartMonth = y.collegeStartMonth,
                                rate1 = y.rate1,
                                rate2 = y.rate2,
                                rate3 = y.rate3,
                                tutionFeeInflation = y.tutionFeeInflation,
                                stateTypeId = y.stateTypeId,
                                parentPay = y.parentPay

                            }).FirstOrDefault();
                    }
                }

            }

            if (eduDefault == null)
            {
                throw new Exception("Education Default not found.");
                // return null;
            }

            decimal rate1, rate2, rate3, inflation; DateTime SSDate= new DateTime();
            decimal? Todaysanualcost, percentPay, currentsavings, monthlysavings;
            int? yearsIn;
            decimal? costToday, costFuture = 0;
            rate1 = (eduDefault.rate1 ?? 5);
            rate2 = (eduDefault.rate2 ?? 6);
            rate3 = (eduDefault.rate3 ?? 9);
            if (em.inflation == 0)
            {
                inflation = (eduDefault.tutionFeeInflation ?? 7) / 100;
            }
            else
            { inflation = em.inflation / 100; }

            int grandTemp = 0;
     
            em.grandTotalCostToday = 0;
            em.grandTotalCostFuture = 0;
            em.savingsRequired = new List<savingsRequired>();
            em.savingsRequiredGrandTotal = new List<savingsRequiredGrantTotal>();
            em.savingsRequiredGrandTotal.Add(new savingsRequiredGrantTotal() { lumpSum = 0, returnRate = 0, additionalMonthlySavings = 0 });
            //em.savingsRequiredGrandTotal.Add(new savingsRequiredGrantTotal() { lumpSum = 0, returnRate = 0, additionalMonthlySavings = 0 });
            //em.savingsRequiredGrandTotal.Add(new savingsRequiredGrantTotal() { lumpSum = 0, returnRate = 0, additionalMonthlySavings = 0 }); 
            decimal? GrandtotalCostToday = 0;
            decimal? GrandTotalCostFuture = 0;
            int loop1 = 0;
            try
            {
                if (em.EducationInfoModel != null)
                {
                    foreach (var item in em.EducationInfoModel)//loop for person and child included in education page
                    {
                        em.savingsRequired.Add(new savingsRequired()
                        {
                            name = item.fName
                        });

                        int temp = 0;

                        em.savingsRequired[loop1].savingsRequiredDetails = new List<savingsRequiredDetails>();

                        foreach (var item2 in item.EducationDetails.Where(w=>w.isSavingsRequired==true))
                        {
                            em.savingsRequired[loop1].savingsRequiredDetails.Add(new savingsRequiredDetails() { collegename = item2.collegename });

                            #region section A
                            costToday = 0;
                            Todaysanualcost = item2.totalAnualCost;
                            percentPay = item2.parentPayRate;
                            yearsIn = item2.yearAtSchool;
                            currentsavings = item2.totalCurrentSavings;
                            monthlysavings = item2.monthlySavings;
                            DateTime? PDate = em.reportDate ?? item2.educationCreatedDate;
                            SSDate = System.DateTime.Now;
                            switch (item2.startDateType)
                            {
                                case (int)CustomEnum.eduStartTypeEnum.age:
                                    int a = Convert.ToInt32(item2.collegeStartAge);
                                    int year = item.dob.Value.Year;
                                    SSDate = new DateTime(year + a, eduDefault.collegeStartMonth.Value, 1);
                                    break;
                                case (int)CustomEnum.eduStartTypeEnum.date:
                                    DateTime.TryParse(item2.startDate, out SSDate);
                                    break;
                            }
                            if (SSDate.Year + yearsIn < System.DateTime.Now.Year || SSDate.Year + yearsIn < PDate.Value.Year)
                            {
                                em.savingsRequired[loop1].savingsRequiredDetails[temp] = null;
                            }
                            else
                            {
                                if (SSDate.Year < PDate.Value.Year && SSDate.Year + yearsIn > PDate.Value.Year)
                                {
                                    int tampyears = PDate.Value.Year - SSDate.Year;
                                    yearsIn -= tampyears;
                                    SSDate = new DateTime(System.DateTime.Now.Year, SSDate.Month, SSDate.Day);
                                }

                                decimal years = ((SSDate.Year - PDate.Value.Year)) * 12;
                                decimal years1 = ((SSDate.Month - PDate.Value.Month));
                                decimal yearvalue = ((years + years1) / 12);
                                decimal no_of_years = Math.Truncate((yearvalue * 12) / 12);
                                decimal no_of_Months = (yearvalue * 12) % 12;
                                if (no_of_years < 0)
                                {
                                    no_of_years = 0;
                                }
                                if (no_of_Months < 0)
                                {
                                    no_of_Months = 0;
                                }
                                //em.savingsRequired[loop1].savingsRequiredDetails.Add(new savingsRequiredDetails() { timeToStartYear = Convert.ToInt32(no_of_years), timeToStartMonth = Convert.ToInt32(no_of_Months) });
                                em.savingsRequired[loop1].savingsRequiredDetails[temp].timeToStartYear = Convert.ToInt32(no_of_years);
                                em.savingsRequired[loop1].savingsRequiredDetails[temp].timeToStartMonth = Convert.ToInt32(no_of_Months);
                                while (yearsIn > 0)
                                {
                                    costToday = costToday + (Todaysanualcost * (percentPay / 100));
                                    if (yearsIn > 1)
                                    {
                                        costToday = costToday * (1 + inflation);
                                    }
                                    yearsIn = yearsIn - 1;
                                }
                                em.savingsRequired[loop1].savingsRequiredDetails[temp].totalCostToday = Math.Round(Convert.ToDecimal(costToday), 0);
                                costFuture = costToday * Convert.ToDecimal(Math.Pow(Convert.ToDouble(1 + inflation), Convert.ToDouble(yearvalue)));
                                if (costFuture > 0)
                                {
                                    GrandTotalCostFuture = GrandTotalCostFuture + costFuture;
                                }
                                if (costToday > 0)
                                {
                                    GrandtotalCostToday = GrandtotalCostToday + costToday;
                                }
                                em.savingsRequired[loop1].savingsRequiredDetails[temp].totalCostFuture = Math.Round(Convert.ToDecimal(costFuture), 0);

                                #endregion section A

                                #region Section B
                                decimal? rateP = 0;
                                if (em.goRate == 0)
                                {
                                    if (em.savingsRequired[loop1].savingsRequiredDetails[temp].timeToStartYear < 4)
                                    {
                                        rateP = (em.savingsRequired[loop1].savingsRequiredDetails[temp].returnRate ?? rate1) / 1200;
                                    }
                                    else if (em.savingsRequired[loop1].savingsRequiredDetails[temp].timeToStartYear < 6)
                                    {
                                        rateP = (em.savingsRequired[loop1].savingsRequiredDetails[temp].returnRate ?? rate2) / 1200;
                                    }
                                    else if (em.savingsRequired[loop1].savingsRequiredDetails[temp].timeToStartYear > 6)
                                    {
                                        rateP = (em.savingsRequired[loop1].savingsRequiredDetails[temp].returnRate ?? rate3) / 1200;
                                    }
                                }
                                else
                                {
                                    rateP = em.goRate/1200;
                                }

                                em.savingsRequired[loop1].savingsRequiredDetails[temp].returnRate = Convert.ToDecimal(Math.Round(Convert.ToDouble(rateP) * 1200));

                                   // em.savingsRequired[temp].savingsRequiredDetails.Add(new savingsRequiredDetails { returnRate = interestrate });
                                    decimal? current1;
                                    current1 = 0;
                                    decimal? current2;
                                    current2 = 0;
                                    current1 = currentsavings * Convert.ToDecimal(Math.Pow(Convert.ToDouble(1 + rateP), Convert.ToDouble(yearvalue * 12)));

                                    for (decimal d = yearvalue * 12; d > 0; d--)
                                    {

                                        current2 = current2 + monthlysavings;
                                        if (d == 1)
                                        {
                                            break;
                                        }
                                        current2 = current2 + (current2 * rateP);
                                    }

                                    em.savingsRequired[loop1].savingsRequiredDetails[temp].currentSavings = Math.Round(Convert.ToDecimal(current2 + current1));
                                    decimal lumpsum = Convert.ToDecimal(costFuture - (current1 + current2)) / Convert.ToDecimal(Math.Pow(Convert.ToDouble(1 + rateP), Convert.ToDouble(yearvalue * 12)));

                                    if (lumpsum > 0)
                                    {
                                        em.savingsRequired[loop1].savingsRequiredDetails[temp].lumpSum = Convert.ToString(Math.Round(lumpsum, 0));

                                        decimal monthlySum;
                                        decimal? Tcurrent, TPeriod;
                                        TPeriod = yearvalue * 12;
                                        Tcurrent = current1 + current2;
                                        if (rateP == 0)
                                        {
                                            if (TPeriod > 0)
                                                monthlySum = (Convert.ToDecimal(costFuture) - Convert.ToDecimal(Tcurrent)) / (TPeriod ?? 1);
                                            else
                                                monthlySum = ((costFuture ?? 0) - (Tcurrent ?? 0));
                                        }
                                        else
                                        {
                                            if (TPeriod > 0)
                                                monthlySum = ((costFuture ?? 0) - (Tcurrent ?? 0)) / ((1 + (rateP ?? 0)) * Convert.ToDecimal((((Math.Pow((1 + (Convert.ToDouble(rateP ?? 0))), Convert.ToDouble(TPeriod ?? 0)))) - 1) / Convert.ToDouble(rateP ?? 1)));
                                            else
                                                monthlySum = ((costFuture ?? 0) - (Tcurrent ?? 0));
                                        }

                                        em.savingsRequired[loop1].savingsRequiredDetails[temp].additionalMonthlySavings = Convert.ToString(Math.Round(monthlySum, 0));

                                        //em.savingsRequiredGrandTotal[grandTemp].returnRate = interestrate;
                                        em.savingsRequiredGrandTotal[grandTemp].lumpSum += Math.Round(Convert.ToDecimal(lumpsum ));
                                        em.savingsRequiredGrandTotal[grandTemp].additionalMonthlySavings += Math.Round(Convert.ToDecimal(Math.Round(monthlySum)));

                                        //  em.savingsRequiredGrandTotal[loop - 1].lumpSum = em.savingsRequiredGrandTotal[loop - 1].lumpSum + lumpsum;
                                        // em.savingsRequiredGrandTotal[loop - 1].additionalMonthlySavings = em.savingsRequiredGrandTotal[loop - 1].additionalMonthlySavings ?? 0 + monthlySum;

                                    }
                                    else
                                    {
                                       // em.savingsRequiredGrandTotal[grandTemp].returnRate = interestrate;
                                        em.savingsRequired[loop1].savingsRequiredDetails[temp].lumpSum = "On Target";
                                        em.savingsRequired[loop1].savingsRequiredDetails[temp].additionalMonthlySavings = "On Target";
                                    }                          


                                #endregion Section B

                                temp++;
                            }

                        }
                        loop1++;
                    }
                    em.grandTotalCostFuture = Convert.ToDecimal(Math.Round(Convert.ToDouble( GrandTotalCostFuture)));
                    em.grandTotalCostToday = Convert.ToDecimal(Math.Round(Convert.ToDouble(GrandtotalCostToday)));
                }

                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
             em.savingsRequired.RemoveAll(b => b.savingsRequiredDetails.All(w => w == null));
             em.savingsRequired.All(b => b.savingsRequiredDetails.RemoveAll(w => w == null) > 0);

            return em;

        }

        public EducationInfoModel fillEducationdbtomodel(Education obj, DefaultsEducation objD)
        {
            EducationInfoModel edu = new EducationInfoModel();
            edu = fillEducationDbtoObj(obj, objD);
            // edu.totalCurrentSavings = totalCurrentsavings(edu.CurrentSavingsDetails);
            //edu.totalMonthlyContribution = totalMonthlyContribution(edu.CurrentSavingsDetails);
            return edu;
        }

        public EducationInfoModel fillEducationDbtoObj(Education obj, DefaultsEducation objD)
        {
            string age = Convert.ToString(objD.collegeAge);
            using (var db = Connection.getConnect())
                try
                {
                    if (obj != null && objD != null)
                    {
                        DateTime zeroTime = new DateTime(1, 1, 1);
                        DateTime a;
                        DateTime b;
                        a = (DateTime)obj.dob;
                        b = System.DateTime.Now;
                        TimeSpan span = b - a;
                        decimal years = Convert.ToDecimal((zeroTime + span).Year - 1);


                        EducationInfoModel edu = new EducationInfoModel()
                        {
                            age = Convert.ToInt32(years),
                            childID = obj.childID ?? 0,
                            fName = obj.fName,
                            dob = obj.dob,
                            personID = obj.personID ?? 0,
                            profileID = obj.profileID,
                            IsParent = obj.IsParent,
                           // educationID = obj.educationID,

                            //CurrentSavingsDetails = getCurrentSavingDetails(obj.educationID)
                        };

                        edu.EducationDetails = new List<EducationDetailsModel>();
                        if (edu.IsParent == true)
                        {
                            

                            edu.EducationDetails = db.Educations.Where(w => w.personID == edu.personID.Value&& w.isActive==true).Select(s => new EducationDetailsModel
                            {
                                educationID = s.educationID,
                                startDateType = s.startDateType ?? (int)CustomEnum.eduStartTypeEnum.age,
                                startDate = s.startDate ?? objD.collegeAge.ToString(),
                                yearAtSchool = s.yearAtSchool ?? objD.collageYears,
                                schoolID = s.schoolID ?? null,
                                totalAnualCost = s.totalAnualCost ?? 0,
                                monthlySavings = s.monthlySavings ?? 0,
                                parentPayRate = s.parentPayRate ?? objD.parentPay,
                                isActive = s.isActive,
                                isSavingsRequired = obj.isSavingsRequired,
                                educationCreatedDate = obj.educationCreatedDate.Value,
                                //CurrentSavingsDetails = getCurrentSavingDetails(s.educationID)
                            }).ToList();
                        }
                        else if (edu.IsParent == false)
                        {
                            edu.EducationDetails = db.Educations.Where(w => w.childID == edu.childID.Value && w.isActive == true).Select(s => new EducationDetailsModel
                            {
                                educationID = obj.educationID,
                                startDateType = s.startDateType ?? (int)CustomEnum.eduStartTypeEnum.age,
                                startDate = s.startDate ?? objD.collegeAge.ToString(),
                                yearAtSchool = s.yearAtSchool ?? objD.collageYears,
                                schoolID = s.schoolID ?? null,
                                totalAnualCost = s.totalAnualCost ?? 0,
                                monthlySavings = s.monthlySavings ?? 0,
                                parentPayRate = s.parentPayRate ?? objD.parentPay,
                                isActive = s.isActive,
                                isSavingsRequired = obj.isSavingsRequired,
                                educationCreatedDate = obj.educationCreatedDate.Value,
                                // CurrentSavingsDetails = getCurrentSavingDetails(s.educationID)
                            }).ToList();
                        }
                        int i = 0;

                        foreach (var edua in edu.EducationDetails)
                        {
                            if (edua.startDateType == 1)
                            {
                                edu.EducationDetails[i].collegeStartAge = Convert.ToInt32(edua.startDate);
                                edu.EducationDetails[i].startDate = System.DateTime.Now.Year + "-" + System.DateTime.Now.Month + "-" + System.DateTime.Now.Day;
                                // edu.startDate = DateTime.ParseExact(System.DateTime.Now.ToString(), "yyyy/MM/dd", CultureInfo.InvariantCulture).ToString(); 
                            }
                            edu.EducationDetails[i].CurrentSavingsDetails = getCurrentSavingDetails(edua.educationID);

                            edu.EducationDetails[i].CurrentSavingsDetails.RemoveAll(w => w.detailID == 0 || w.amount == null);

                            edu.EducationDetails[i].totalCurrentSavings = edua.CurrentSavingsDetails.Sum(bb => bb.amount) ?? 0;
                            if (edua.schoolID != null && edua.schoolID > 0)
                            {
                                edu.EducationDetails[i].collegename = db.colleges.Where(bb => bb.collegeID == edua.schoolID).Select(ss => ss.collegeName).FirstOrDefault();
                            }
                            i++;
                        }
                        return edu;


                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            return null;
        }

        public EducationInfoModel saveEduInfo(EducationInfoModel item, DefaultsEducation eduDefault, long userID)
        {
            DateTime SSDate = new DateTime();
            Education obj = new Education();
            using (var db = Connection.getConnect())
            {
                try
                {
                    if (eduDefault == null)
                    {
                        eduDefault = db.DefaultsEducations.SingleOrDefault(b => b.profileID == item.profileID);
                        if (eduDefault == null)
                        {
                            eduDefault = new DefaultsEducation();
                            eduDefault = db.DefaultsEducations.Join(db.profiles.Where(x => x.profileID == item.profileID), d => d.clientID, p => p.clientID, (d, p) => d).ToList()
                                .Select(y => new DefaultsEducation
                                {
                                    collageYears = y.collageYears,
                                    collegeAge = y.collegeAge,
                                    collegeStartMonth = y.collegeStartMonth,
                                    rate1 = y.rate1,
                                    rate2 = y.rate2,
                                    rate3 = y.rate3,
                                    tutionFeeInflation = y.tutionFeeInflation,
                                    stateTypeId = y.stateTypeId,
                                    parentPay = y.parentPay
                                }).FirstOrDefault();

                        }
                    }            
                  foreach (var eobj in item.EducationDetails)
                  {
                        if (eobj.educationID > 0)
                        {
                            obj = db.Educations.SingleOrDefault(b => b.educationID == eobj.educationID && b.profileID == item.profileID);
                            if (obj != null && userID > 0)
                            {
                                switch (eobj.startDateType)
                                {
                                    case (int)CustomEnum.eduStartTypeEnum.age:
                                        int a = Convert.ToInt32(eobj.collegeStartAge);
                                        int year = obj.dob.Value.Year;
                                        SSDate = new DateTime(year + a, eduDefault.collegeStartMonth ?? 9, 1);
                                        eobj.startDate = eobj.collegeStartAge.ToString();
                                        break;
                                    case (int)CustomEnum.eduStartTypeEnum.date:
                                        DateTime.TryParse(eobj.startDate, out SSDate);
                                        break;
                                }
                                if ((SSDate.Year + eobj.yearAtSchool) > System.DateTime.Now.Year)
                                {
                                    obj.isSavingsRequired = true;
                                }
                                else
                                {
                                    obj.isSavingsRequired = false;
                                }
                                obj.startDateType = eobj.startDateType;
                                obj.startDate = eobj.startDate;
                                obj.yearAtSchool = eobj.yearAtSchool ?? eduDefault.collageYears;
                                obj.schoolID = eobj.schoolID;
                                obj.totalAnualCost = eobj.totalAnualCost;
                                obj.monthlySavings = eobj.monthlySavings ?? 0;
                                obj.parentPayRate = eobj.parentPayRate ?? eduDefault.parentPay;
                                obj.educationUpdatedDate = System.DateTime.Now;
                                obj.educationModifiedBy = userID;
                                db.Entry(obj).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }

                        }
                        else if(item.profileID >0)
                        {
                            obj = new Education();
                            switch (eobj.startDateType)
                            {
                                case (int)CustomEnum.eduStartTypeEnum.age:
                                    int a = Convert.ToInt32(eobj.collegeStartAge);
                                    int year = obj.dob.Value.Year;
                                    SSDate = new DateTime(year + a, eduDefault.collegeStartMonth ?? 9, 1);
                                    eobj.startDate = eobj.collegeStartAge.ToString();
                                    break;
                                case (int)CustomEnum.eduStartTypeEnum.date:
                                    DateTime.TryParse(eobj.startDate, out SSDate);
                                    break;
                            }
                            if ((SSDate.Year + eobj.yearAtSchool) > System.DateTime.Now.Year)
                            {
                                obj.isSavingsRequired = true;
                            }
                            else
                            {
                                obj.isSavingsRequired = false;
                            }
                            obj.profileID = item.profileID;
                            obj.childID = item.childID;
                            obj.personID = item.personID;
                            obj.IsParent = item.IsParent;
                            obj.isActive = true;
                            obj.dob = item.dob;
                            obj.startDateType = eobj.startDateType;
                            obj.startDate = eobj.startDate;
                            obj.yearAtSchool = eobj.yearAtSchool ?? eduDefault.collageYears;
                            obj.schoolID = eobj.schoolID;
                            obj.totalAnualCost = eobj.totalAnualCost;
                            obj.monthlySavings = eobj.monthlySavings ?? 0;
                            obj.parentPayRate = eobj.parentPayRate ?? eduDefault.parentPay;
                            obj.educationCreatedDate = System.DateTime.Now;
                            obj.educationModifiedBy = userID;
                            db.Educations.Add(obj);
                            db.SaveChanges();
                        }
                   
                  }

                }
                catch (Exception ex)
                {
                    throw;
                }
            }


            return item;
        }

        public bool deleteEducation(long deleteID, long userID)
        {
            bool status = false;
            using (var db = Connection.getConnect())
            {
                try
                {
                    var edu = db.Educations.Where(w => w.educationID == deleteID & w.isActive == true).FirstOrDefault();

                    if (edu != null && edu.educationID > 0)
                    {
                        edu.isActive = false;
                        edu.educationUpdatedDate = System.DateTime.Now;
                        edu.educationModifiedBy = userID;
                        db.Entry(edu).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        status = true;
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return status;
        }


    }
}