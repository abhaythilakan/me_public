﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static CLayer.common.CustomEnum;

namespace DLayer
{
    public class DebtReg
    {
        public Debt fillModelToDB(DebtModel model, Debt data)
        {
            try
            {
                data.debtID = model.debtID;
                data.profileID = model.profileID;
                data.debtCatId = model.debtCatId;
                data.creditor = model.creditor;
                data.amount = model.amount;
                if (model.minPay > 0)
                    data.minPay = model.minPay;
                else
                {
                    using (var db = Connection.getConnect())
                    {
                        var defaults = db.DefaultsDebts.Where(b => b.profileID == data.profileID).FirstOrDefault();
                        if (defaults != null)
                            data.minPay = defaults.minPay;
                    }
                }
                if (model.unPaidBalance > 0)
                    data.unPaidBalance = model.unPaidBalance;
                else
                {
                    using (var db = Connection.getConnect())
                    {
                        var defaults = db.DefaultsDebts.Where(b => b.profileID == data.profileID).FirstOrDefault();
                        if (defaults != null)
                            data.unPaidBalance = defaults.unpaidPercentage;
                    }
                }
                data.extraPay = model.extraPay;
                data.actualPay = model.actualPay;
                data.rate = model.rate;
                data.isFixed = model.isFixed;
                data.notes = model.notes;
                data.percentPay = model.percentPay;
                data.introRate = model.introRate;
                data.introDate = model.introDate;
                data.isPlan = model.isPlan;
                data.debtTypeID = model.debtTypeID;
                data.inclElmPlan = model.inclElmPlan;
                data.cardMinPay = model.cardMinPay;
                if (model.Balanceasof > DateTime.MinValue)
                    data.Balanceasof = model.Balanceasof == null ? DateTime.Now.AddMonths(1) : model.Balanceasof.Value.AddMonths(1);
                else
                    data.Balanceasof = DateTime.Now.AddMonths(1);
                data.isPrimaryResidence = model.isPrimaryResidence;
                data.NotesShow = model.NotesShow;
                if (model.debtID > 0)
                    data.debtCreatedDate = model.debtCreatedDate;
                else
                    data.debtCreatedDate = DateTime.Now;
                data.debtModifiedDate = DateTime.Now;
                data.debtModifiedBy = model.debtModifiedBy;
                data.isActive = true;
            }
            catch (Exception ex)
            {
                throw;
            }
            return data;
        }

        public decimal getTotalExtraPayment(long profileID)
        {
            decimal totalExtraPay = 0;
            try
            {
                using (var db = Connection.getConnect())
                {
                    totalExtraPay = db.Debts.Where(b => b.isActive == true && b.inclElmPlan == true && b.profileID == profileID).Sum(b => b.extraPay) ?? 0;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return totalExtraPay;
        }

        public void setProjectedRate(ref List<DebtModel> data)
        {
            try
            {
                for (int i = 0; i < data.Count(); i++)
                {
                    data[i].projectedPayOff = getDebtInputPrediction_Individual(new DebtProjectionInput { debtId = data[i].debtID }).DebtProjectionInput.payOff ?? "";
                    data[i].projectedPayOffDate = getDebtInputPrediction_Individual(new DebtProjectionInput { debtId = data[i].debtID }).DebtProjectionInput.payOffDate;
                    data[i].projectedInterest = getDebtInputPrediction_Individual(new DebtProjectionInput { debtId = data[i].debtID }).DebtProjectionInput.totalInterest;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //public void getClientDebts()
        //{
        //    long ProfileID = 0;
        //    List<DebtModel> model = new List<DebtModel>();
        //    try
        //    {
        //        using (var db = Connection.getConnect())
        //        {
        //            var debts = db.Debts.Where(b => b.profileID == ProfileID && b.isActive == true).ToList();
        //            if (debts != null)
        //            {
        //                if (debts.Count() > 0)
        //                {

        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}


        #region Mortage
        public List<DebtModel> getMortageList(long profileID)
        {
            List<DebtModel> data = new List<DebtModel>();
            try
            {
                using (var db = Connection.getConnect())
                {

                    //projectedPayOff = getDebtInputPrediction(new DebtProjectionInput { debtId = d.debtID }).DebtProjectionInput.payOff ?? "",
                    //            projectedPayOffDate = getDebtInputPrediction(new DebtProjectionInput { debtId = d.debtID }).DebtProjectionInput.payOffDate,
                    //            projectedInterest = getDebtInputPrediction(new DebtProjectionInput { debtId = d.debtID }).DebtProjectionInput.totalInterest

                    data = (from d in db.Debts
                            join dt in db.DebtTypes on d.debtTypeID equals dt.debtTypeId
                            join dc in db.DebtCats on dt.debtCatId equals dc.debtCatId
                            where dc.catName == debtCatEnum.Mortage.ToString() && d.isActive == true && d.profileID == profileID
                            select new DebtModel
                            {
                                debtID = d.debtID,
                                profileID = d.profileID,
                                debtCatId = d.debtCatId,
                                creditor = d.creditor,
                                amount = d.amount,
                                minPay = d.minPay,
                                unPaidBalance = d.unPaidBalance,
                                actualPay = d.actualPay,
                                extraPay = d.extraPay,
                                rate = d.rate,
                                isFixed = d.isFixed,
                                notes = d.notes,
                                percentPay = d.percentPay,
                                introRate = d.introRate,
                                introDate = d.introDate,
                                isPlan = d.isPlan,
                                debtTypeID = d.debtTypeID,
                                inclElmPlan = d.inclElmPlan,
                                cardMinPay = d.cardMinPay,
                                //Balanceasof = d.Balanceasof.Value.AddMonths(-1),
                                Balanceasof = d.Balanceasof,
                                isPrimaryResidence = d.isPrimaryResidence,
                                NotesShow = d.NotesShow,
                                debtCreatedDate = d.debtCreatedDate,
                                debtModifiedDate = d.debtModifiedDate,
                                debtModifiedBy = d.debtModifiedBy
                            }).ToList();
                    setProjectedRate(ref data);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return data;
        }
        #endregion
        #region Fixed
        public List<DebtModel> getFixedList(long profileID)
        {
            List<DebtModel> data = new List<DebtModel>();
            try
            {
                using (var db = Connection.getConnect())
                {
                    data = (from d in db.Debts
                            join dt in db.DebtTypes on d.debtTypeID equals dt.debtTypeId
                            join dc in db.DebtCats on dt.debtCatId equals dc.debtCatId
                            where dc.catName == debtCatEnum.Fixed.ToString() && d.isActive == true && d.profileID == profileID
                            select new DebtModel
                            {
                                debtID = d.debtID,
                                profileID = d.profileID,
                                debtCatId = d.debtCatId,
                                creditor = d.creditor,
                                amount = d.amount,
                                minPay = d.minPay,
                                unPaidBalance = d.unPaidBalance,
                                actualPay = d.actualPay,
                                extraPay = d.extraPay,
                                rate = d.rate,
                                isFixed = d.isFixed,
                                notes = d.notes,
                                percentPay = d.percentPay,
                                introRate = d.introRate,
                                introDate = d.introDate,
                                isPlan = d.isPlan,
                                debtTypeID = d.debtTypeID,
                                inclElmPlan = d.inclElmPlan,
                                cardMinPay = d.cardMinPay,
                                //Balanceasof = d.Balanceasof.Value.AddMonths(-1),
                                Balanceasof = d.Balanceasof,
                                isPrimaryResidence = d.isPrimaryResidence,
                                NotesShow = d.NotesShow,
                                debtCreatedDate = d.debtCreatedDate,
                                debtModifiedDate = d.debtModifiedDate,
                                debtModifiedBy = d.debtModifiedBy
                            }).ToList();
                    setProjectedRate(ref data);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return data;
        }
        #endregion
        #region Revolving
        public List<DebtModel> getRevolvingList(long profileID)
        {
            List<DebtModel> data = new List<DebtModel>();
            try
            {
                using (var db = Connection.getConnect())
                {
                    data = (from d in db.Debts
                            join dt in db.DebtTypes on d.debtTypeID equals dt.debtTypeId
                            join dc in db.DebtCats on dt.debtCatId equals dc.debtCatId
                            where dc.catName == debtCatEnum.Revolving.ToString() && d.isActive == true && d.profileID == profileID
                            select new DebtModel
                            {
                                debtID = d.debtID,
                                profileID = d.profileID,
                                debtCatId = d.debtCatId,
                                creditor = d.creditor,
                                amount = d.amount,
                                minPay = d.minPay,
                                unPaidBalance = d.unPaidBalance,
                                actualPay = d.actualPay,
                                extraPay = d.extraPay,
                                rate = d.rate,
                                isFixed = d.isFixed,
                                notes = d.notes,
                                percentPay = d.percentPay,
                                introRate = d.introRate,
                                introDate = d.introDate,
                                isPlan = d.isPlan,
                                debtTypeID = d.debtTypeID,
                                inclElmPlan = d.inclElmPlan,
                                cardMinPay = d.cardMinPay,
                                //Balanceasof = d.Balanceasof.Value.AddMonths(-1),
                                Balanceasof = d.Balanceasof,
                                isPrimaryResidence = d.isPrimaryResidence,
                                NotesShow = d.NotesShow,
                                debtCreatedDate = d.debtCreatedDate,
                                debtModifiedDate = d.debtModifiedDate,
                                debtModifiedBy = d.debtModifiedBy
                            }).ToList();
                    setProjectedRate(ref data);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return data;
        }
        #endregion


        public DebtModel getDebtDetails(long debtId)
        {
            DebtModel data = new DebtModel();
            try
            {
                using (var db = Connection.getConnect())
                {
                    data = db.Debts.Where(b => b.debtID == debtId && b.isActive == true).Select(d => new DebtModel
                    {
                        debtID = d.debtID,
                        profileID = d.profileID,
                        debtCatId = d.debtCatId,
                        creditor = d.creditor,
                        amount = d.amount,
                        minPay = d.minPay,
                        actualPay = d.actualPay,
                        extraPay = d.extraPay,
                        rate = d.rate,
                        isFixed = d.isFixed,
                        notes = d.notes,
                        percentPay = d.percentPay,
                        introRate = d.introRate,
                        introDate = d.introDate,
                        isPlan = d.isPlan,
                        debtTypeID = d.debtTypeID,
                        inclElmPlan = d.inclElmPlan,
                        cardMinPay = d.cardMinPay,
                        Balanceasof = d.Balanceasof,
                        isPrimaryResidence = d.isPrimaryResidence,
                        NotesShow = d.NotesShow,
                        debtCreatedDate = d.debtCreatedDate,
                        debtModifiedDate = d.debtModifiedDate,
                        debtModifiedBy = d.debtModifiedBy,
                        projectedPayOffDate = d.projectedPayOffDate
                    }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return data;
        }
        public DebtModel saveDebtDetails(DebtModel model)
        {
            try
            {
                using (var db = Connection.getConnect())
                {
                    Debt data = new Debt();
                    if (model.debtID > 0)
                    {
                        data = db.Debts.SingleOrDefault(b => b.debtID == model.debtID);
                    }
                    data = fillModelToDB(model, data ?? new Debt());
                    if (model.debtID > 0)
                        db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                    else
                        db.Debts.Add(data);
                    db.SaveChanges();
                    model.debtID = data.debtID;
                    //model.actualPay = data.minPay + data.extraPay;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }
        public List<DebtModel> deleteDebt(DebtModel model)
        {
            bool isDelete = false;
            List<DebtModel> mdl = new List<DebtModel>();
            try
            {
                using (var db = Connection.getConnect())
                {
                    Debt data = new Debt();
                    if (model.debtID > 0)
                    {
                        data = db.Debts.SingleOrDefault(b => b.debtID == model.debtID);
                        if (data != null)
                        {
                            data.isActive = false;
                            data.debtModifiedBy = model.debtModifiedBy;
                            data.debtModifiedDate = DateTime.Now;
                            db.SaveChanges();
                            isDelete = true;
                            if (data.debtCatId > (int)CustomEnum.debtCatEnum.Mortage)
                            {
                                mdl = getMortageList(model.profileID);
                            }
                            else if (data.debtCatId > (int)CustomEnum.debtCatEnum.Fixed)
                            {
                                mdl = getFixedList(model.profileID);
                            }
                            else if (data.debtCatId > (int)CustomEnum.debtCatEnum.Revolving)
                            {
                                mdl = getRevolvingList(model.profileID);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return mdl;
        }

        public DebtAmortizationModel getMortageAmmo(DebtProjectionInput model, Debt deb, DebtAmortizationModel data)
        {
            try
            {
                using (var db = Connection.getConnect())
                {

                    DateTime DateStart = Convert.ToDateTime(deb.Balanceasof);

                    List<DebtProjectionModel> md = new List<DebtProjectionModel>();
                    decimal irateMonthly = model.interestRate / 1200;
                    decimal totalInterestI = 0, totalPrincipalI = 0, interestI, principalI, balanceI = model.startBalance, iI = 0;
                    DateTime? payOffDate = null;

                    while (totalPrincipalI != model.startBalance)
                    {
                        DebtProjectionModel dp = new DebtProjectionModel();
                        interestI = Math.Round(balanceI * irateMonthly, 2);
                        if (model.isOneTimePay)
                        {
                            if (iI == 0)
                            {
                                principalI = Math.Round(((Convert.ToDecimal(deb.minPay) + Convert.ToDecimal(model.additionalPayOneTime)) - interestI), 2);
                                model.payPrincInterstPerMnth = Convert.ToDecimal(deb.minPay) + Convert.ToDecimal(model.additionalPayOneTime);
                                if (model.isAllTimePay)
                                {
                                    principalI = principalI + model.additionalPayAllTime;
                                    model.payPrincInterstPerMnth = model.payPrincInterstPerMnth + Convert.ToDecimal(model.additionalPayAllTime);
                                }
                            }
                            else
                            {
                                principalI = Math.Round((Convert.ToDecimal(deb.minPay) - interestI), 2);
                                model.payPrincInterstPerMnth = Convert.ToDecimal(deb.minPay);
                            }
                        }
                        else
                        {
                            principalI = Math.Round(((Convert.ToDecimal(deb.minPay) + Convert.ToDecimal(model.additionalPayAllTime)) - interestI), 2);
                            model.payPrincInterstPerMnth = Convert.ToDecimal(deb.minPay) + Convert.ToDecimal(model.additionalPayAllTime);
                        }
                        totalPrincipalI = Math.Round(totalPrincipalI + principalI, 2);
                        totalInterestI = Math.Round(totalInterestI + interestI, 2);
                        balanceI = Math.Round((model.startBalance - totalPrincipalI), 2);
                        //if (iI > 72)
                        //{
                        //    var ttd = 0;
                        //}
                        if (balanceI < model.payPrincInterstPerMnth)
                        {
                            DateStart = DateStart.AddMonths(1);
                            iI++;
                            dp.Date = DateStart;
                            dp.dateString = DateStart.ToString("MMM, yyyy");
                            dp.principal = principalI;
                            dp.interest = interestI;
                            dp.totalPrincipal = totalPrincipalI;
                            dp.totalInterest = totalInterestI;
                            dp.balance = balanceI;
                            dp.totalPayment = dp.totalPrincipal + dp.totalInterest;
                            if (model.isOneTimePay)
                            {
                                dp.additionalPay = iI == 1 ? Convert.ToDecimal(model.additionalPayOneTime) : 0;
                                model.totalAdditional = iI == 1 ? (model.totalAdditional + Convert.ToDecimal(model.additionalPayOneTime)) : Convert.ToDecimal(model.additionalPayOneTime);
                                if (model.isAllTimePay)
                                {
                                    dp.additionalPay = (dp.additionalPay + Convert.ToDecimal(model.additionalPayAllTime));
                                    model.totalAdditional = (model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime));
                                }
                            }
                            else
                            {
                                dp.additionalPay = Convert.ToDecimal(model.additionalPayAllTime);
                                model.totalAdditional = model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime);
                            }
                            md.Add(dp);

                            principalI = balanceI;
                            totalPrincipalI = totalPrincipalI + principalI;
                            interestI = Math.Round(balanceI * irateMonthly, 2);
                            totalInterestI = totalInterestI + interestI;
                            balanceI = Math.Round((model.startBalance - totalPrincipalI), 2);

                            dp = new DebtProjectionModel();
                            DateStart = DateStart.AddMonths(1);
                            dp.Date = DateStart;
                            dp.dateString = DateStart.ToString("MMM, yyyy");
                            dp.principal = principalI;
                            dp.interest = interestI;
                            dp.totalPrincipal = totalPrincipalI;
                            dp.totalInterest = totalInterestI;
                            dp.balance = balanceI;
                            dp.totalPayment = dp.totalPrincipal + dp.totalInterest;
                            if (model.isOneTimePay)
                            {
                                dp.additionalPay = iI == 1 ? Convert.ToDecimal(model.additionalPayOneTime) : 0;
                                model.totalAdditional = iI == 1 ? (model.totalAdditional + Convert.ToDecimal(model.additionalPayOneTime)) : Convert.ToDecimal(model.additionalPayOneTime);
                                if (model.isAllTimePay)
                                {
                                    dp.additionalPay = (dp.additionalPay + Convert.ToDecimal(model.additionalPayAllTime));
                                    model.totalAdditional = (model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime));
                                }
                            }
                            else
                            {
                                dp.additionalPay = Convert.ToDecimal(model.additionalPayAllTime);
                                model.totalAdditional = model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime);
                            }
                            md.Add(dp);
                            payOffDate = DateStart;
                        }
                        else
                        {
                            iI++;
                            DateStart = DateStart.AddMonths(1);
                            dp.Date = DateStart;
                            dp.dateString = DateStart.ToString("MMM, yyyy");
                            dp.principal = principalI;
                            dp.interest = interestI;
                            dp.totalPrincipal = totalPrincipalI;
                            dp.totalInterest = totalInterestI;
                            dp.balance = balanceI;
                            dp.totalPayment = dp.totalPrincipal + dp.totalInterest;
                            if (model.isOneTimePay)
                            {
                                dp.additionalPay = iI == 1 ? Convert.ToDecimal(model.additionalPayOneTime) : 0;
                                model.totalAdditional = iI == 1 ? (model.totalAdditional + Convert.ToDecimal(model.additionalPayOneTime)) : Convert.ToDecimal(model.additionalPayOneTime);
                                if (model.isAllTimePay)
                                {
                                    dp.additionalPay = (dp.additionalPay + Convert.ToDecimal(model.additionalPayAllTime));
                                    model.totalAdditional = (model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime));
                                }
                            }
                            else
                            {
                                if (model.isAllTimePay)
                                {
                                    dp.additionalPay = Convert.ToDecimal(model.additionalPayAllTime);
                                    model.totalAdditional = model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime);
                                }
                            }
                            md.Add(dp);
                        }

                    }

                    model.noOfPay = md.Count();
                    model.payOff = Convert.ToDateTime(payOffDate).ToString("MMM, yyyy");
                    model.payOffDate = Convert.ToDateTime(payOffDate);
                    model.totalPay = md.LastOrDefault().totalPrincipal + md.LastOrDefault().totalInterest;
                    model.totalInterest = md.LastOrDefault().totalInterest;
                    data.DebtProjectionInput = model;
                    //model.isYearlychart = true;
                    if (model.isYearlychart)
                    {
                        data.DebtProjectionList = (from m in md
                                                   group m by m.Date.Year into g
                                                   select new DebtProjectionModel
                                                   {
                                                       principal = g.Sum(b => b.principal),
                                                       interest = g.Sum(b => b.interest),
                                                       totalPrincipal = g.LastOrDefault().totalPrincipal,
                                                       totalInterest = g.LastOrDefault().totalInterest,
                                                       totalPayment = g.LastOrDefault().totalPayment,
                                                       additionalPay = g.Sum(b => b.additionalPay),
                                                       balance = g.LastOrDefault().balance,
                                                       Period = g.Count() > 1 ? g.FirstOrDefault().Date.ToString("MMM") + " - " + g.LastOrDefault().Date.ToString("MMM, yyyy") : g.LastOrDefault().Date.ToString("MMM, yyyy")
                                                   }).ToList();
                        model.noOfPay = data.DebtProjectionList.Count();
                    }
                    else
                    {
                        data.DebtProjectionList = md;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return data;
        }

        public DebtAmortizationModel getFixedAmmo(DebtProjectionInput model, Debt deb, DebtAmortizationModel data)
        {
            try
            {
                using (var db = Connection.getConnect())
                {
                    DateTime DateStart = Convert.ToDateTime(deb.Balanceasof);
                    List<DebtProjectionModel> md = new List<DebtProjectionModel>();
                    decimal amount = model.startBalance;
                    decimal irate = model.interestRate;               // main interest user input
                    decimal minMonthPay = Convert.ToDecimal(deb.minPay);
                    decimal introRate = Convert.ToDecimal(deb.introRate);   // user input for initial interest calculation "Intro Rate". and End Date.
                    DateTime introEndDate = Convert.ToDateTime(deb.introDate);

                    decimal actMonthPay = 0;//minMonthPay + extramMonthPay;

                    decimal totalInterestI = 0, totalPrincipalI = 0, interestI = 0, principalI = 0, balanceI = amount, iI = 0, totalPaymentI;
                    DateTime? payOffDate = null;

                    bool isFromIntodate = false;
                    decimal iRateMonthly = 0;
                    decimal iIntroRateMonthly = 0;
                    if (irate > 0)
                        iRateMonthly = irate / 1200;
                    if (introRate > 0)
                        iIntroRateMonthly = introRate / 1200;
                    decimal interestActual = 0;

                    while (totalPrincipalI != model.startBalance)
                    {
                        DebtProjectionModel dp = new DebtProjectionModel();

                        if ((DateStart != DateTime.MinValue && introEndDate != DateTime.MinValue) && String.Format("{0:MMM-yyyy}", DateStart) == String.Format("{0:MMM-yyyy}", introEndDate.AddMonths(-1)))
                        {
                            //if (String.Format("{0:MMM-yyyy}", DateStart) == String.Format("{0:MMM-yyyy}", introEndDate.AddMonths(-1)))
                            //{
                            interestActual = iRateMonthly;
                            isFromIntodate = true;
                        }
                        else
                        {
                            if (!(isFromIntodate) && iIntroRateMonthly > 0)
                            {
                                interestActual = iIntroRateMonthly;   //Console.WriteLine("rate 2 selected :"+irate3); 
                            }
                            else
                            {
                                interestActual = iRateMonthly;   //  Console.WriteLine("rate 1 selected.second :"+irate2); 
                            }
                        }
                        interestI = 0;
                        totalPaymentI = 0;

                        interestI = Math.Round(balanceI * interestActual, 2); //Interest Rate from Last Balance Amount

                        if (model.isOneTimePay)
                        {
                            dp.additionalPay = iI == 0 ? Convert.ToDecimal(model.additionalPayOneTime) : 0;
                            model.totalAdditional = iI == 0 ? (model.totalAdditional + Convert.ToDecimal(model.additionalPayOneTime)) : Convert.ToDecimal(model.additionalPayOneTime);
                            if (model.isAllTimePay)
                            {
                                dp.additionalPay = (dp.additionalPay + Convert.ToDecimal(model.additionalPayAllTime));
                                model.totalAdditional = (model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime));
                            }
                        }
                        else
                        {
                            if (model.isAllTimePay)
                            {
                                dp.additionalPay = Convert.ToDecimal(model.additionalPayAllTime);
                                model.totalAdditional = model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime);
                            }
                        }
                        actMonthPay = Convert.ToDecimal(deb.minPay) + Convert.ToDecimal(dp.additionalPay);


                        //actMonthPay = model.totalAdditional;
                        totalPaymentI = totalPaymentI + actMonthPay;
                        principalI = Math.Round(totalPaymentI - interestI, 2);
                        totalPrincipalI = Math.Round(totalPrincipalI + principalI, 2);
                        totalInterestI = Math.Round(totalInterestI + interestI, 2);
                        balanceI = Math.Round((amount - totalPrincipalI), 2);

                        if ((balanceI + balanceI * interestActual) < totalPaymentI)
                        {
                            DateStart = DateStart.AddMonths(1);
                            iI++;
                            dp.Date = DateStart;
                            dp.dateString = DateStart.ToString("MMM, yyyy");
                            dp.Period = DateStart.ToString("MMM, yyyy");
                            dp.principal = principalI;
                            dp.interest = interestI;
                            dp.totalPrincipal = totalPrincipalI;
                            dp.totalInterest = totalInterestI;
                            dp.balance = balanceI;
                            dp.totalPayment = dp.totalPrincipal + dp.totalInterest;
                            md.Add(dp);

                            principalI = balanceI;
                            totalPrincipalI = totalPrincipalI + principalI;
                            interestI = Math.Round(balanceI * iRateMonthly, 2);
                            totalInterestI = totalInterestI + interestI;
                            balanceI = Math.Round((model.startBalance - totalPrincipalI), 2);

                            dp = new DebtProjectionModel();
                            DateStart = DateStart.AddMonths(1);
                            dp.Date = DateStart;
                            dp.dateString = DateStart.ToString("MMM, yyyy");
                            dp.Period = DateStart.ToString("MMM, yyyy");
                            dp.principal = principalI;
                            dp.interest = interestI;
                            dp.totalPrincipal = totalPrincipalI;
                            dp.totalInterest = totalInterestI;
                            dp.balance = balanceI;
                            dp.totalPayment = dp.totalPrincipal + dp.totalInterest;
                            md.Add(dp);
                            payOffDate = DateStart;
                            break;
                        }
                        else
                        {
                            iI++;
                            DateStart = DateStart.AddMonths(1);
                            dp.Date = DateStart;
                            dp.dateString = DateStart.ToString("MMM, yyyy");
                            dp.Period = DateStart.ToString("MMM, yyyy");
                            dp.principal = principalI;
                            dp.interest = interestI;
                            dp.totalPrincipal = totalPrincipalI;
                            dp.totalInterest = totalInterestI;
                            dp.balance = balanceI;
                            dp.totalPayment = dp.totalPrincipal + dp.totalInterest;
                        }
                        md.Add(dp);
                    }
                    model.noOfPay = md.Count();
                    model.payOff = Convert.ToDateTime(payOffDate).ToString("MMM, yyyy");
                    model.payOffDate = Convert.ToDateTime(payOffDate);
                    model.totalPay = md.LastOrDefault().totalPrincipal + md.LastOrDefault().totalInterest;
                    model.totalInterest = md.LastOrDefault().totalInterest;
                    data.DebtProjectionInput = model;
                    //model.isYearlychart = true;
                    if (model.isYearlychart)
                    {
                        data.DebtProjectionList = (from m in md
                                                   group m by m.Date.Year into g
                                                   select new DebtProjectionModel
                                                   {
                                                       principal = g.Sum(b => b.principal),
                                                       interest = g.Sum(b => b.interest),
                                                       totalPrincipal = g.LastOrDefault().totalPrincipal,
                                                       totalInterest = g.LastOrDefault().totalInterest,
                                                       totalPayment = g.LastOrDefault().totalPayment,
                                                       additionalPay = g.Sum(b => b.additionalPay),
                                                       balance = g.LastOrDefault().balance,
                                                       Period = g.Count() > 1 ? g.FirstOrDefault().Date.ToString("MMM") + " - " + g.LastOrDefault().Date.ToString("MMM, yyyy") : g.LastOrDefault().Date.ToString("MMM, yyyy")
                                                   }).ToList();
                        model.noOfPay = data.DebtProjectionList.Count();
                    }
                    else
                    {
                        data.DebtProjectionList = md;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return data;
        }

        public DebtAmortizationModel getRevolvingAmmo(DebtProjectionInput model, Debt deb, DebtAmortizationModel data)
        {
            try
            {
                using (var db = Connection.getConnect())
                {
                    DateTime DateStart = Convert.ToDateTime(deb.Balanceasof);
                    List<DebtProjectionModel> md = new List<DebtProjectionModel>();
                    var defaults = db.DefaultsDebts.Where(b => b.profileID == deb.profileID).FirstOrDefault();
                    decimal amount = model.startBalance;
                    decimal irate = model.interestRate;               // main interest user input
                    decimal minMonthPay = 0;
                    if (defaults != null)
                        minMonthPay = Convert.ToDecimal(deb.minPay) > 0 ? Convert.ToDecimal(deb.minPay) : (Convert.ToDecimal(defaults.minPay));
                    else
                        minMonthPay = Convert.ToDecimal(deb.minPay);
                    decimal introRate = Convert.ToDecimal(deb.introRate);   // user input for initial interest calculation "Intro Rate". and End Date.
                    DateTime introEndDate = Convert.ToDateTime(deb.introDate);

                    decimal actMonthPay = 0;//minMonthPay + extramMonthPay;

                    decimal totalInterestI = 0, totalPrincipalI = 0, interestI = 0, principalI = 0, balanceI = amount, iI = 0, totalPaymentI = 0;
                    DateTime? payOffDate = null;

                    bool isFromIntodate = false;
                    decimal iRateMonthly = 0;
                    decimal iIntroRateMonthly = 0;
                    if (irate > 0)
                        iRateMonthly = irate / 1200;
                    if (introRate > 0)
                        iIntroRateMonthly = introRate / 1200;
                    //decimal iRateMonthly = irate / 1200;
                    //decimal iIntroRateMonthly = introRate / 1200;
                    decimal interestActual = 0;

                    while (totalPrincipalI != model.startBalance)
                    {
                        DebtProjectionModel dp = new DebtProjectionModel();
                        if ((DateStart != DateTime.MinValue && introEndDate != DateTime.MinValue) && String.Format("{0:MMM-yyyy}", DateStart) == String.Format("{0:MMM-yyyy}", introEndDate.AddMonths(-1)))
                        {
                            //if (String.Format("{0:MMM-yyyy}", DateStart) == String.Format("{0:MMM-yyyy}", introEndDate.AddMonths(-1)))
                            //{
                            interestActual = iRateMonthly;
                            isFromIntodate = true;
                        }
                        else
                        {
                            if (!(isFromIntodate) && iIntroRateMonthly > 0)
                            {
                                interestActual = iIntroRateMonthly;   //Console.WriteLine("rate 2 selected :"+irate3); 
                            }
                            else
                            {
                                interestActual = iRateMonthly;   //  Console.WriteLine("rate 1 selected.second :"+irate2);                                  }  
                            }
                        }


                        interestI = 0;
                        actMonthPay = 0;
                        //totalPaymentI = 0;

                        interestI = Math.Round(balanceI * interestActual, 2);

                        decimal cardPayPercent = 0;
                        if (defaults != null)
                            cardPayPercent = Convert.ToDecimal(deb.unPaidBalance) > 0 ? Convert.ToDecimal(deb.unPaidBalance) : (Convert.ToDecimal(defaults.unpaidPercentage));
                        else
                            cardPayPercent = Convert.ToDecimal(deb.unPaidBalance);
                        decimal cardPaymonthly = (balanceI * (cardPayPercent)) / 100;

                        if (cardPaymonthly < minMonthPay)
                        {
                            //minMonthPay = minMonthPay;
                        }
                        else
                        {
                            minMonthPay = cardPaymonthly;
                        }

                        if (model.isOneTimePay)
                        {
                            dp.additionalPay = iI == 0 ? Convert.ToDecimal(model.additionalPayOneTime) : 0;
                            model.totalAdditional = iI == 0 ? (model.totalAdditional + Convert.ToDecimal(model.additionalPayOneTime)) : Convert.ToDecimal(model.additionalPayOneTime);
                            if (model.isAllTimePay)
                            {
                                dp.additionalPay = (dp.additionalPay + Convert.ToDecimal(model.additionalPayAllTime));
                                model.totalAdditional = (model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime));
                            }
                        }
                        else
                        {
                            if (model.isAllTimePay)
                            {
                                dp.additionalPay = Convert.ToDecimal(model.additionalPayAllTime);
                                model.totalAdditional = model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime);
                            }
                        }
                        /////////////////
                        actMonthPay = Convert.ToDecimal(minMonthPay) + Convert.ToDecimal(dp.additionalPay);
                        /////////////////

                        totalPaymentI = totalPaymentI + actMonthPay;
                        principalI = Math.Round(actMonthPay - interestI, 2);
                        totalPrincipalI = Math.Round(totalPrincipalI + principalI, 2);
                        totalInterestI = Math.Round(totalInterestI + interestI, 2);
                        // Console.WriteLine("totalPayment :"+ totalPayment);
                        balanceI = Math.Round((amount - totalPrincipalI), 2);

                        var valAmt = (balanceI + (balanceI * interestActual));

                        if (valAmt < actMonthPay)
                        {

                            iI++;
                            DateStart = DateStart.AddMonths(1);
                            dp.Date = DateStart;
                            dp.dateString = DateStart.ToString("MMM, yyyy");
                            dp.Period = DateStart.ToString("MMM, yyyy");
                            dp.principal = principalI;
                            dp.interest = interestI;
                            dp.totalPrincipal = totalPrincipalI;
                            dp.totalInterest = totalInterestI;
                            dp.balance = balanceI;
                            dp.totalPayment = dp.totalPrincipal + dp.totalInterest;
                            md.Add(dp);

                            dp = new DebtProjectionModel();
                            principalI = balanceI;
                            actMonthPay = 0;
                            totalPrincipalI = totalPrincipalI + principalI;
                            interestI = Math.Round(balanceI * interestActual, 2);
                            totalInterestI = totalInterestI + interestI;
                            balanceI = Math.Round((amount - totalPrincipalI), 2);
                            totalPaymentI = principalI + interestI;// Console.WriteLine("totalPayment "+totalPayment);  
                            DateStart = DateStart.AddMonths(1);

                            dp.Date = DateStart;
                            dp.dateString = DateStart.ToString("MMM, yyyy");
                            dp.Period = DateStart.ToString("MMM, yyyy");
                            dp.principal = principalI;
                            dp.interest = interestI;
                            dp.totalPrincipal = totalPrincipalI;
                            dp.totalInterest = totalInterestI;
                            dp.balance = balanceI;
                            dp.totalPayment = dp.totalPrincipal + dp.totalInterest;
                            md.Add(dp);
                            payOffDate = DateStart;
                            break;
                        }
                        iI++;
                        DateStart = DateStart.AddMonths(1);
                        dp.Date = DateStart;
                        dp.dateString = DateStart.ToString("MMM, yyyy");
                        dp.Period = DateStart.ToString("MMM, yyyy");
                        dp.principal = principalI;
                        dp.interest = interestI;
                        dp.totalPrincipal = totalPrincipalI;
                        dp.totalInterest = totalInterestI;
                        dp.balance = balanceI;
                        dp.totalPayment = dp.totalPrincipal + dp.totalInterest;
                        md.Add(dp);
                    }
                    model.noOfPay = md.Count();
                    model.payOff = Convert.ToDateTime(payOffDate).ToString("MMM, yyyy");
                    model.payOffDate = Convert.ToDateTime(payOffDate);
                    model.totalPay = md.LastOrDefault().totalPrincipal + md.LastOrDefault().totalInterest;
                    model.totalInterest = md.LastOrDefault().totalInterest;
                    data.DebtProjectionInput = model;
                    //model.isYearlychart = true;
                    if (model.isYearlychart)
                    {
                        data.DebtProjectionList = (from m in md
                                                   group m by m.Date.Year into g
                                                   select new DebtProjectionModel
                                                   {
                                                       principal = g.Sum(b => b.principal),
                                                       interest = g.Sum(b => b.interest),
                                                       totalPrincipal = g.LastOrDefault().totalPrincipal,
                                                       totalInterest = g.LastOrDefault().totalInterest,
                                                       totalPayment = g.LastOrDefault().totalPayment,
                                                       additionalPay = g.Sum(b => b.additionalPay),
                                                       balance = g.LastOrDefault().balance,
                                                       Period = g.Count() > 1 ? g.FirstOrDefault().Date.ToString("MMM") + " - " + g.LastOrDefault().Date.ToString("MMM, yyyy") : g.LastOrDefault().Date.ToString("MMM, yyyy")
                                                   }).ToList();
                        model.noOfPay = data.DebtProjectionList.Count();
                    }
                    else
                    {
                        data.DebtProjectionList = md;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return data;
        }

        //public void getClientDebts()
        //{
        //    try
        //    {

        //    }
        //    catch(Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //public DebtAmortizationModel getDebtInputPrediction_All(long profileID)
        //{
        //    DebtProjectionInput model = new DebtProjectionInput();
        //    DebtAmortizationModel data = new DebtAmortizationModel();
        //    try
        //    {
        //        if (profileID > 0)
        //        {
        //            using (var db = Connection.getConnect())
        //            {
        //                var debts = db.profiles.Where(b => b.profileID == profileID && b.isActive == true).FirstOrDefault() == null ? null : db.Debts.Where(b => b.profileID == profileID && b.isActive == true).ToList();
        //                if (debts != null)
        //                {
        //                    var deb = new Debt();
        //                    var cnt = db.Debts.Where(b => b.debtID == model.debtId && b.isActive == true).Count();
        //                    if (cnt == 1)
        //                    {
        //                        deb = db.Debts.Single(b => b.debtID == model.debtId);
        //                        model.debtName = deb.creditor;
        //                        model.debtCatId = Convert.ToInt64(deb.debtCatId);
        //                        model.debtCatName = db.DebtCats.Where(b => b.debtCatId == deb.debtCatId).FirstOrDefault().catName;
        //                        model.isOneTimePay = model.isOneTimePay != true ? false : true;
        //                        model.isAllTimePay = (model.isOneTimePay != true) ? true : model.isAllTimePay;
        //                        if (model.isAllTimePay)
        //                            model.additionalPayAllTime = model.additionalPayAllTime > 0 ? model.additionalPayAllTime : Convert.ToDecimal(deb.extraPay);
        //                        if (model.isOneTimePay)
        //                            model.additionalPayOneTime = model.additionalPayOneTime > 0 ? model.additionalPayOneTime : Convert.ToDecimal(deb.extraPay);
        //                        model.startBalance = Convert.ToDecimal(deb.amount);
        //                        model.interestRate = Convert.ToDecimal(deb.rate);

        //                        if (deb.debtCatId == (int)CustomEnum.debtCatEnum.Mortage)
        //                            data = getMortageAmmo(model, deb, data);
        //                        else if (deb.debtCatId == (int)CustomEnum.debtCatEnum.Fixed)
        //                            data = getFixedAmmo(model, deb, data);
        //                        else if (deb.debtCatId == (int)CustomEnum.debtCatEnum.Revolving)
        //                            data = getRevolvingAmmo(model, deb, data);

        //                        if (data != null)
        //                        {
        //                            if (deb.debtID > 0)
        //                            {
        //                                deb.projectedPayOffDate = data.DebtProjectionInput.payOffDate;
        //                                db.Entry(deb).State = System.Data.Entity.EntityState.Modified;
        //                                db.SaveChanges();
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //    return data;
        //}

        public DebtAmortizationModel getDebtInputPrediction_Individual(DebtProjectionInput model)
        {
            DebtAmortizationModel data = new DebtAmortizationModel();
            try
            {
                using (var db = Connection.getConnect())
                {
                    var deb = new Debt();
                    var cnt = db.Debts.Where(b => b.debtID == model.debtId && b.isActive == true).Count();
                    if (cnt == 1)
                    {
                        deb = db.Debts.Single(b => b.debtID == model.debtId && b.isActive == true);
                        model.debtName = deb.creditor;
                        model.debtCatId = Convert.ToInt64(deb.debtCatId);
                        model.debtCatName = db.DebtCats.Where(b => b.debtCatId == deb.debtCatId).FirstOrDefault().catName;
                        //model.isOneTimePay = model.isOneTimePay != true ? false : true;
                        //model.isAllTimePay = (model.isOneTimePay != true) ? true : model.isAllTimePay;
                        if (model.isAllTimePay)
                            model.additionalPayAllTime = model.additionalPayAllTime > 0 ? model.additionalPayAllTime : Convert.ToDecimal(deb.extraPay);
                        if (model.isOneTimePay)
                            model.additionalPayOneTime = model.additionalPayOneTime > 0 ? model.additionalPayOneTime : Convert.ToDecimal(deb.extraPay);
                        model.startBalance = Convert.ToDecimal(deb.amount);
                        model.interestRate = Convert.ToDecimal(deb.rate);

                        if (deb.debtCatId == (int)CustomEnum.debtCatEnum.Mortage)
                            data = getMortageAmmo(model, deb, data);
                        else if (deb.debtCatId == (int)CustomEnum.debtCatEnum.Fixed)
                            data = getFixedAmmo(model, deb, data);
                        else if (deb.debtCatId == (int)CustomEnum.debtCatEnum.Revolving)
                            data = getRevolvingAmmo(model, deb, data);

                        if (data != null)
                        {
                            if (deb.debtID > 0)
                            {
                                deb.projectedPayOffDate = data.DebtProjectionInput.payOffDate;
                                db.Entry(deb).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }

                        #region Calculation
                        //List<DebtProjection> md = new List<DebtProjection>();
                        ////Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
                        //decimal irateMonthly = model.interestRate / 1200;
                        ////double actMonthPay = minMonthPay + extramMonthPay;
                        //decimal totalInterestI = 0, totalPrincipalI = 0, interestI, principalI, balanceI = model.startBalance, iI = 0;
                        //DateTime? payOffDate = null;

                        //while (totalPrincipalI != model.startBalance)
                        //{

                        //    //iI++;
                        //    //DateStart = DateStart.AddMonths(1);
                        //    DebtProjection dp = new DebtProjection();
                        //    interestI = Math.Round(balanceI * irateMonthly, 2);
                        //    if (model.isOneTimePay)
                        //    {
                        //        if (iI == 0)
                        //        {
                        //            principalI = Math.Round(((Convert.ToDecimal(deb.minPay) + Convert.ToDecimal(model.additionalPayOneTime)) - interestI), 2);
                        //            model.payPrincInterstPerMnth = Convert.ToDecimal(deb.minPay) + Convert.ToDecimal(model.additionalPayOneTime);
                        //            if (model.isAllTimePay)
                        //            {
                        //                principalI = principalI + model.additionalPayAllTime;
                        //                model.payPrincInterstPerMnth = model.payPrincInterstPerMnth + Convert.ToDecimal(model.additionalPayAllTime);
                        //            }
                        //        }
                        //        else
                        //        {
                        //            principalI = Math.Round((Convert.ToDecimal(deb.minPay) - interestI), 2);
                        //            model.payPrincInterstPerMnth = Convert.ToDecimal(deb.minPay);
                        //        }
                        //    }
                        //    else
                        //    {
                        //        principalI = Math.Round(((Convert.ToDecimal(deb.minPay) + Convert.ToDecimal(model.additionalPayAllTime)) - interestI), 2);
                        //        model.payPrincInterstPerMnth = Convert.ToDecimal(deb.minPay) + Convert.ToDecimal(model.additionalPayAllTime);
                        //    }
                        //    totalPrincipalI = Math.Round(totalPrincipalI + principalI, 2);
                        //    totalInterestI = Math.Round(totalInterestI + interestI, 2);
                        //    balanceI = Math.Round((model.startBalance - totalPrincipalI), 2);
                        //    //if (iI > 72)
                        //    //{
                        //    //    var ttd = 0;
                        //    //}
                        //    if (balanceI < model.payPrincInterstPerMnth)
                        //    {
                        //        DateStart = DateStart.AddMonths(1);
                        //        iI++;
                        //        dp.Date = DateStart;
                        //        dp.dateString = DateStart.ToString("MMM, yyyy");
                        //        dp.principal = principalI;
                        //        dp.interest = interestI;
                        //        dp.totalPrincipal = totalPrincipalI;
                        //        dp.totalInterest = totalInterestI;
                        //        dp.balance = balanceI;
                        //        dp.totalPayment = dp.totalPrincipal + dp.totalInterest;
                        //        if (model.isOneTimePay)
                        //        {
                        //            dp.additionalPay = iI == 1 ? Convert.ToDecimal(model.additionalPayOneTime) : 0;
                        //            model.totalAdditional = iI == 1 ? (model.totalAdditional + Convert.ToDecimal(model.additionalPayOneTime)) : Convert.ToDecimal(model.additionalPayOneTime);
                        //            if (model.isAllTimePay)
                        //            {
                        //                dp.additionalPay = (dp.additionalPay + Convert.ToDecimal(model.additionalPayAllTime));
                        //                model.totalAdditional = (model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime));
                        //            }
                        //        }
                        //        else
                        //        {
                        //            dp.additionalPay = Convert.ToDecimal(model.additionalPayAllTime);
                        //            model.totalAdditional = model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime);
                        //        }
                        //        md.Add(dp);

                        //        principalI = balanceI;
                        //        totalPrincipalI = totalPrincipalI + principalI;
                        //        interestI = Math.Round(balanceI * irateMonthly, 2);
                        //        totalInterestI = totalInterestI + interestI;
                        //        balanceI = Math.Round((model.startBalance - totalPrincipalI), 2);

                        //        dp = new DebtProjection();
                        //        DateStart = DateStart.AddMonths(1);
                        //        dp.Date = DateStart;
                        //        dp.dateString = DateStart.ToString("MMM, yyyy");
                        //        dp.principal = principalI;
                        //        dp.interest = interestI;
                        //        dp.totalPrincipal = totalPrincipalI;
                        //        dp.totalInterest = totalInterestI;
                        //        dp.balance = balanceI;
                        //        dp.totalPayment = dp.totalPrincipal + dp.totalInterest;
                        //        if (model.isOneTimePay)
                        //        {
                        //            dp.additionalPay = iI == 1 ? Convert.ToDecimal(model.additionalPayOneTime) : 0;
                        //            model.totalAdditional = iI == 1 ? (model.totalAdditional + Convert.ToDecimal(model.additionalPayOneTime)) : Convert.ToDecimal(model.additionalPayOneTime);
                        //            if (model.isAllTimePay)
                        //            {
                        //                dp.additionalPay = (dp.additionalPay + Convert.ToDecimal(model.additionalPayAllTime));
                        //                model.totalAdditional = (model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime));
                        //            }
                        //        }
                        //        else
                        //        {
                        //            dp.additionalPay = Convert.ToDecimal(model.additionalPayAllTime);
                        //            model.totalAdditional = model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime);
                        //        }
                        //        md.Add(dp);
                        //        payOffDate = DateStart;
                        //    }
                        //    else
                        //    {
                        //        iI++;
                        //        DateStart = DateStart.AddMonths(1);
                        //        dp.Date = DateStart;
                        //        dp.dateString = DateStart.ToString("MMM, yyyy");
                        //        dp.principal = principalI;
                        //        dp.interest = interestI;
                        //        dp.totalPrincipal = totalPrincipalI;
                        //        dp.totalInterest = totalInterestI;
                        //        dp.balance = balanceI;
                        //        dp.totalPayment = dp.totalPrincipal + dp.totalInterest;
                        //        if (model.isOneTimePay)
                        //        {
                        //            dp.additionalPay = iI == 1 ? Convert.ToDecimal(model.additionalPayOneTime) : 0;
                        //            model.totalAdditional = iI == 1 ? (model.totalAdditional + Convert.ToDecimal(model.additionalPayOneTime)) : Convert.ToDecimal(model.additionalPayOneTime);
                        //            if (model.isAllTimePay)
                        //            {
                        //                dp.additionalPay = (dp.additionalPay + Convert.ToDecimal(model.additionalPayAllTime));
                        //                model.totalAdditional = (model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime));
                        //            }
                        //        }
                        //        else
                        //        {
                        //            dp.additionalPay = Convert.ToDecimal(model.additionalPayAllTime);
                        //            model.totalAdditional = model.totalAdditional + Convert.ToDecimal(model.additionalPayAllTime);
                        //        }
                        //        md.Add(dp);
                        //    }

                        //}

                        //model.noOfPay = md.Count();
                        //model.payOffDate = Convert.ToDateTime(payOffDate).ToString("MMM, yyyy");
                        //model.totalPay = md.LastOrDefault().totalPrincipal + md.LastOrDefault().totalInterest;
                        //model.totalInterest = md.LastOrDefault().totalInterest;
                        //data.DebtProjectionInput = model;
                        ////model.isYearlychart = true;
                        //if (model.isYearlychart)
                        //{
                        //    data.DebtProjectionList = (from m in md
                        //                               group m by m.Date.Year into g
                        //                               select new DebtProjection
                        //                               {
                        //                                   principal = g.Sum(b => b.principal),
                        //                                   interest = g.Sum(b => b.interest),
                        //                                   totalPrincipal = g.LastOrDefault().totalPrincipal,
                        //                                   totalInterest = g.LastOrDefault().totalInterest,
                        //                                   totalPayment = g.LastOrDefault().totalPayment,
                        //                                   additionalPay = g.Sum(b => b.additionalPay),
                        //                                   balance = g.LastOrDefault().balance,
                        //                                   Period = g.Count() > 1 ? g.FirstOrDefault().Date.ToString("MMM") + " - " + g.LastOrDefault().Date.ToString("MMM, yyyy") : g.LastOrDefault().Date.ToString("MMM, yyyy")
                        //                               }).ToList();
                        //}
                        //else
                        //{
                        //    data.DebtProjectionList = md;
                        //}
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return data;
        }

        //public void getDebtEliminationSystemAutoEstimation()
        //{
        //    try
        //    {
        //        long ProfileID = 0;
        //        decimal acceleratedPay = 20;
        //        using (var db = Connection.getConnect())
        //        {
        //            List<Debt> dt = new List<Debt>();
        //            dt = db.Debts.Where(b => b.profileID == ProfileID && b.isActive == true).OrderBy(b => b.Balanceasof).ThenBy(b => b.projectedPayOffDate).ToList();
        //            if (dt != null)
        //            {
        //                if (dt.Count() > 0)
        //                {
        //                    DebtDisappearFullModel data = new DebtDisappearFullModel();
        //                    foreach(var itm in dt)
        //                    {

        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}
        //public void getDebtDissapearingList()
        //{
        //    try
        //    {
        //        using (var db = Connection.getConnect())
        //        {

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //public void getDebtDissapearingDetailedList()
        //{
        //    try
        //    {
        //        using (var db = Connection.getConnect())
        //        {

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}
    }
}
