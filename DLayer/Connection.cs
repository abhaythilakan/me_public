﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLayer
{
    public class Connection
    {
        private static CLayer.DBModel.ME_v2Entities db = null;

        public static CLayer.DBModel.ME_v2Entities getConnect()
        {
            db = new CLayer.DBModel.ME_v2Entities();
            return db;
        }
    }
}
