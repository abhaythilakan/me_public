//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DLayer.EntityFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class AdvisorBranch
    {
        public long bId { get; set; }
        public Nullable<long> advisorId { get; set; }
        public string bFirstName { get; set; }
        public string bLastName { get; set; }
        public string bAddress { get; set; }
        public string bCity { get; set; }
        public string bState { get; set; }
        public string bZip { get; set; }
        public string bPhone { get; set; }
        public string bCompany { get; set; }
    }
}
