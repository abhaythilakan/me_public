﻿using CLayer;
using System;
using CLayer.common;
using CLayer.DBModel;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using DLayer.Common;

namespace DLayer
{
    public class CollegeCRUD
    {
        public List<AllCollegeModel> getColleges(CollegeListArgs col, CustomEnum.ColgSearchType stype)
        {
            List<AllCollegeModel> college = new List<AllCollegeModel>();
            using (var db = Connection.getConnect())
            {
                try
                {
                    int a = (int)stype;
                    var query = "EXEC dbo.CollegePopUp  @searchValue,@Type , @PageNo ,@PageSize,@SortColumn ,@SortOrder";
                    var parameters = new[]
                    {
                        new SqlParameter("searchValue", col.searchValue??(object)DBNull.Value),
                        new SqlParameter("Type",(int)stype) ,
                        new SqlParameter("PageNo", col.PageNo>0 ?col.PageNo : 1),
                        new SqlParameter("PageSize",col.PageSize>0 ?col.PageSize : 20),
                        new SqlParameter("SortColumn",col.SortColumn>0 ? col.SortColumn :0),
                        new SqlParameter("SortOrder", col.SortOrder>0 ?col.SortOrder : 0)
                    };
                    using (var multiResultSet = DbContextExtensions.MultiResultSetSqlQuery(db, query, parameters))
                    {
                        college = multiResultSet.ResultSetFor<AllCollegeModel>().ToList();
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return college;
        }

        public bool RemoveFavCollege(AdvisorFavList favList, long userID)
        {
            bool stutus = false;
            List<AllCollegeModel> college = new List<AllCollegeModel>();
            DefaultCollege favobj = new DefaultCollege();
            using (var db = Connection.getConnect())
            {
                try
                {
                    foreach (var item in favList.CollegeID)
                    {
                        if (item > 0)
                        {
                            favobj = db.DefaultColleges.SingleOrDefault(b => b.collegeID == item && b.advisorID== favList.advisorId && b.isActive ==true);

                            if (favobj != null && favobj.id > 0)
                            {
                                favobj.isActive = false;
                                favobj.favcollegeUpdatedDate = System.DateTime.Now;
                                favobj.favcollegeModifiedBy = userID;
                                db.Entry(favobj).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }

                        }

                    }
                    return true;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return stutus;
        }

        public bool saveCollegesToFav(AdvisorFavList obj, long userID)
        {
            bool stutus = false;
            List<AllCollegeModel> college = new List<AllCollegeModel>();
            DefaultCollege favobj = new DefaultCollege();
            using (var db = Connection.getConnect())
            {
                try
                {
                    foreach (var item in obj.CollegeID)
                    {

                        if (obj.advisorId > 0)
                        {
                            favobj = db.DefaultColleges.SingleOrDefault(b => b.collegeID == item && b.advisorID == userID);
                            if (favobj != null)
                            {
                                if (favobj.isActive == false)
                                {
                                    favobj.isActive = true;
                                    favobj.advisorID = obj.advisorId;
                                    favobj.collegeID = item;
                                    favobj.favcollegeUpdatedDate = System.DateTime.Now;
                                    favobj.favcollegeModifiedBy = userID;
                                    db.Entry(favobj).State = System.Data.Entity.EntityState.Modified;
                                }

                            }
                            else
                            {

                                favobj = new DefaultCollege();
                                favobj.isActive = true;
                                favobj.advisorID = obj.advisorId;
                                favobj.collegeID = item;
                                favobj.favcollegeCreatedDate = System.DateTime.Now;
                                favobj.favcollegeModifiedBy = userID;
                                db.DefaultColleges.Add(favobj);
                            }
                            db.SaveChanges();

                            //}
                            //return true;
                        }
                        else if (obj.clientID > 0)
                        {

                        }
                        else if (obj.profileID > 0)
                        {

                        }
                    }
                    return true;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return stutus;
        }
        public List<DefaultCollege> getFavCollegeList(long id, int userCat)
        {
            List<DefaultCollege> objList = new List<DefaultCollege>();
            // var objList;
            try
            {

                using (var db = Connection.getConnect())
                {
                    if (id > 0)
                        switch (userCat)
                        {
                            case (int)CustomEnum.userCatEnum.Advisor:

                                objList = db.DefaultColleges.Where(b => b.advisorID == id && b.isActive == true).ToList();
                                if (objList != null)
                                {
                                    int i = 0;
                                    foreach (var a in objList)
                                    {
                                        objList[i].college = db.colleges.Where(b => b.collegeID == a.collegeID).SingleOrDefault();

                                        i++;
                                    }
                                }
                                else { return null; }
                                break;
                            case (int)CustomEnum.userCatEnum.Client:

                                objList = db.DefaultColleges.Where(b => b.clientID == id && b.isActive == true).ToList();
                                break;
                            case (int)CustomEnum.userCatEnum.Profile:
                                objList = db.DefaultColleges.Where(b => b.profileID == id && b.isActive == true).ToList();
                                if (objList != null)
                                {
                                    int i = 0;
                                    foreach (var a in objList)
                                    {
                                        objList[i].college = db.colleges.Where(b => b.collegeID == a.collegeID).SingleOrDefault();

                                        i++;
                                    }
                                }
                                break;
                        }
                    else
                    {
                        objList = null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return objList;

        }

        private List<CLayer.adminPagePermissionModel> getPagePermissionList(long userID, CustomEnum.userCatEnum usrCat, int reportType)
        {
            List<CLayer.adminPagePermissionModel> report = new List<CLayer.adminPagePermissionModel>();

            try
            {

                using (var db = Connection.getConnect())
                {
                    var query = "EXEC getPagePermissionList @reptype";
                    var parameters = new[]
                    {
                         new SqlParameter("@ID", userID),
                        new SqlParameter("@REPORTYPE", (int)usrCat),
                          new SqlParameter("@TYPE", (int)reportType)
                    };
                    using (var multiResultSet = DbContextExtensions.MultiResultSetSqlQuery(db, query, parameters))
                    {
                        report = multiResultSet.ResultSetFor<adminPagePermissionModel>().ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return report;



        }

        public List<AllCollegeModel> FavDefaultCollegeList()
        {
            List<AllCollegeModel> college = new List<AllCollegeModel>();
            using (var db = Connection.getConnect())
            {
                try
                {
                    college = db.colleges.Where(w => w.collegeState == null && w.city==null) .ToList().Select(s=> new AllCollegeModel
                    {
                     collegeID=s.collegeID,
                     collegeName=s.collegeName,
                     inState=s.inState,
                     outState=s.outState,
                     inStateTutionFees=s.inStateTutionFees,
                     outStateTutionFees=s.outStateTutionFees,
                     fees=s.fees,
                     roomBoardFees=s.roomBoardFees,
                     roomFees=s.roomFees,                     

                    }).ToList();
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return college;
        }

    }
}