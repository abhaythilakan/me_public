﻿using CLayer;
using System;
using CLayer.common;
using CLayer.DBModel;
using DLayer;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CLayer.AdvisorModel;
using static System.Net.Mime.MediaTypeNames;
using DLayer.Common;
using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;
using System.Text.RegularExpressions;
using System.Globalization;


namespace DLayer
{
    public class EmergencyFund
    {
        //To get the details of emergency fund types
        public EmergencyFundTypes getEmergencyTypes()
        {
            EmergencyFundTypes et = new EmergencyFundTypes();
            try
            {
                using (var db = Connection.getConnect())
                {
                    et.eTypes = db.EmergencyFund_Types.Where(b => b.eFundTypeID > 0).ToList();
                    if (et.eTypes != null)
                    {
                        int i = 0;
                        foreach (var a in et.eTypes)
                        {
                            et.eTypes[i].EFund = db.EmergencyFunds.Where(b => b.eTypeID == a.eFundTypeID).ToList();
                            i++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return et;

        }

        public void getDefaultEmergencyFund(long profileID, ref EmergencyFundModel efModel)
        {
            try
            {
                efModel.details = new List<CurrentFundDetails>();
                using (var db = Connection.getConnect())
                {
                    //To fetch the details of non-retirement investments
                    List<Asset_NonRetirement> valueList = new List<Asset_NonRetirement>();
                    valueList = db.Asset_NonRetirement.Where(b => b.profileID == profileID && b.isIncludeEmergencyFund == true && (b.assetCatID == 1 || b.assetCatID == 2) && b.isActive == true).OrderBy(b => b.assetCatID).ToList();
                    if (valueList != null && valueList.Count > 0)
                    {
                        //var catname = db.AssetCategories.Where(b => b.catID == 1).Select(b => b.catName).SingleOrDefault();
                        //efModel.details.Add(new CurrentFundDetails { assetCatg = catname });
                        //efModel.details[i].assetType = new List<assetType>();

                        //if (valueList != null)
                        for (int ii = 0, ass1Cnt = 0, ass2Cnt = 0; ii < valueList.Count(); ii++)
                        {
                            if (valueList[ii].assetCatID == 1)
                            {
                                if (ass1Cnt == 0)
                                {
                                    ass1Cnt++;
                                    var catname = db.AssetCategories.Where(b => b.catID == 1).Select(b => b.catName).SingleOrDefault();
                                    efModel.details.Add(new CurrentFundDetails { assetCatg = catname });
                                    efModel.details[0].assetType = new List<assetType>();
                                }
                                if (valueList[ii].assetID > 0)
                                {
                                    var tm = valueList[ii].assetTypeID;
                                    var accountname = db.AssetTypes.Where(b => b.assetTypeID == tm).Select(k => k.typeName).FirstOrDefault();
                                    efModel.details[0].assetType.Add(new assetType { name = accountname, value = valueList[ii].amount });
                                    efModel.currentFund = (efModel.currentFund ?? 0) + Math.Round(Convert.ToDecimal(valueList[ii].amount), 0);
                                }
                            }
                            else if (valueList[ii].assetCatID == 2)
                            {
                                if (ass2Cnt == 0)
                                {
                                    ass2Cnt++;
                                    var catname = db.AssetCategories.Where(b => b.catID == 2).Select(b => b.catName).SingleOrDefault();
                                    efModel.details.Add(new CurrentFundDetails { assetCatg = catname });
                                    efModel.details[1].assetType = new List<assetType>();
                                }
                                if (valueList[ii].assetID > 0)
                                {
                                    var tm = valueList[ii].assetTypeID;
                                    var accountname = db.AssetTypes.Where(b => b.assetTypeID == tm).Select(k => k.typeName).FirstOrDefault();
                                    //var accountname = db.AssetTypes.Where(b => b.assetTypeID == valueList[ii].assetTypeID).Select(k => k.typeName).SingleOrDefault();
                                    efModel.details[1].assetType.Add(new assetType { name = accountname, value = valueList[ii].amount });
                                    efModel.currentFund = (efModel.currentFund ?? 0) + Math.Round(Convert.ToDecimal(valueList[ii].amount), 0);
                                }
                            }
                        }

                        //foreach (var a in valueList)
                        //{
                        //    if (a != null && a.assetID > 0)
                        //    {
                        //        var accountname = db.AssetTypes.Where(b => b.assetTypeID == a.assetTypeID).Select(k => k.typeName).SingleOrDefault();
                        //        efModel.details[i].assetType.Add(new assetType { name = accountname, value = a.amount });
                        //        efModel.currentFund = efModel.currentFund + Math.Round(Convert.ToDecimal(a.amount), 0);
                        //    }

                        //}
                        //i++;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        //To get the emergency fund details of the profile
        public EmergencyFundModel getEmergencyFund(long profileID)
        {
            EmergencyFundModel efModel = new EmergencyFundModel();
            decimal s = 0;
            decimal rate;
            int no_of_years;
            decimal amount;
            decimal goalValue;
            efModel.currentFund = 0;
            efModel.details = new List<CurrentFundDetails>();
            int i = 0;

            try
            {
                using (var db = Connection.getConnect())
                {
                    getDefaultEmergencyFund(profileID, ref efModel);

                    #region 
                    ////To fetch the details of non-retirement investments
                    //var valueList = db.Asset_NonRetirement.Where(b => b.profileID == profileID && b.isIncludeEmergencyFund == true && (b.assetCatID == 1 || b.assetCatID == 2) && b.isActive == true).OrderBy(b => b.assetCatID).ToList();
                    //if (valueList != null && valueList.Count > 0)
                    //{
                    //    //var catname = db.AssetCategories.Where(b => b.catID == 1).Select(b => b.catName).SingleOrDefault();
                    //    //efModel.details.Add(new CurrentFundDetails { assetCatg = catname });
                    //    //efModel.details[i].assetType = new List<assetType>();

                    //    //if (valueList != null)
                    //    for (int ii = 0, ass1Cnt = 0, ass2Cnt = 0; ii < valueList.Count(); ii++)
                    //    {
                    //        if (valueList[ii].assetCatID == 1)
                    //        {
                    //            if (ass1Cnt == 0)
                    //            {
                    //                ass1Cnt++;
                    //                var catname = db.AssetCategories.Where(b => b.catID == 1).Select(b => b.catName).SingleOrDefault();
                    //                efModel.details.Add(new CurrentFundDetails { assetCatg = catname });
                    //                efModel.details[i].assetType = new List<assetType>();
                    //            }
                    //            if (valueList[ii].assetID > 0)
                    //            {
                    //                var accountname = db.AssetTypes.Where(b => b.assetTypeID == valueList[ii].assetTypeID).Select(k => k.typeName).SingleOrDefault();
                    //                efModel.details[i].assetType.Add(new assetType { name = accountname, value = valueList[ii].amount });
                    //                efModel.currentFund = efModel.currentFund + Math.Round(Convert.ToDecimal(valueList[ii].amount), 0);
                    //            }
                    //        }
                    //        else if (valueList[ii].assetCatID == 2)
                    //        {
                    //            if (ass2Cnt == 0)
                    //            {
                    //                ass2Cnt++;
                    //                var catname = db.AssetCategories.Where(b => b.catID == 2).Select(b => b.catName).SingleOrDefault();
                    //                efModel.details.Add(new CurrentFundDetails { assetCatg = catname });
                    //                efModel.details[i].assetType = new List<assetType>();
                    //            }
                    //            if (valueList[ii].assetID > 0)
                    //            {
                    //                var accountname = db.AssetTypes.Where(b => b.assetTypeID == valueList[ii].assetTypeID).Select(k => k.typeName).SingleOrDefault();
                    //                efModel.details[i].assetType.Add(new assetType { name = accountname, value = valueList[ii].amount });
                    //                efModel.currentFund = efModel.currentFund + Math.Round(Convert.ToDecimal(valueList[ii].amount), 0);
                    //            }
                    //        }
                    //    }

                    //    //foreach (var a in valueList)
                    //    //{
                    //    //    if (a != null && a.assetID > 0)
                    //    //    {
                    //    //        var accountname = db.AssetTypes.Where(b => b.assetTypeID == a.assetTypeID).Select(k => k.typeName).SingleOrDefault();
                    //    //        efModel.details[i].assetType.Add(new assetType { name = accountname, value = a.amount });
                    //    //        efModel.currentFund = efModel.currentFund + Math.Round(Convert.ToDecimal(a.amount), 0);
                    //    //    }

                    //    //}
                    //    //i++;
                    //}
                    //////To fetch the details of cash accounts
                    ////var valueList1 = db.Asset_NonRetirement.Where(b => b.profileID == profileID && b.isIncludeEmergencyFund == true && b.assetCatID == 2 && b.isActive == true).ToList();
                    ////if (valueList1 != null && valueList1.Count > 0)
                    ////{
                    ////    var catname = db.AssetCategories.Where(b => b.catID == 2).Select(b => b.catName).SingleOrDefault();
                    ////    efModel.details.Add(new CurrentFundDetails { assetCatg = catname });
                    ////    efModel.details[i].assetType = new List<assetType>();
                    ////    foreach (var a in valueList1)
                    ////    {
                    ////        if (a != null && a.assetID > 0)
                    ////        {
                    ////            var accountname = db.AssetTypes.Where(b => b.assetTypeID == a.assetTypeID).Select(k => k.typeName).SingleOrDefault();
                    ////            efModel.details[i].assetType.Add(new assetType { name = accountname, value = a.amount });
                    ////            efModel.currentFund = efModel.currentFund + Math.Round(Convert.ToDecimal(a.amount), 0);
                    ////        }

                    ////    }
                    ////}
                    #endregion 
                    efModel.targetAmount = db.Asset_EmergencyFund.Where(b => b.profileID == profileID).Select(b => b.emergencyFund).FirstOrDefault();
                    if (efModel.targetAmount == null)
                    {
                        efModel.targetAmount = 0;
                    }

                    if (efModel.targetAmount > efModel.currentFund)
                    {
                        efModel.status = "Shortfall";
                        efModel.statusValue = Convert.ToString(Convert.ToDecimal(efModel.targetAmount - efModel.currentFund));
                        efModel.returnRate = db.DefaultsEmergencyFunds.Where(b => b.profileID == profileID).Select(b => b.inflation).FirstOrDefault();
                        efModel.years = 5;
                        rate = ((efModel.returnRate / 1200) ?? 3 / 1200);
                        no_of_years = ((efModel.years * 12) ?? 5 * 12);
                        goalValue = Convert.ToDecimal(efModel.statusValue);
                        decimal value = Convert.ToDecimal((((Math.Pow(Convert.ToDouble(1 + rate), Convert.ToDouble(no_of_years))) - 1) / Convert.ToDouble(rate)) * Convert.ToDouble(1 + rate));
                        amount = Math.Round(goalValue / value);
                        efModel.monthlySavings = Convert.ToString(amount);
                    }
                    else
                    {
                        efModel.returnRate = db.DefaultsEmergencyFunds.Where(b => b.profileID == profileID).Select(b => b.inflation).FirstOrDefault();
                        if (efModel.returnRate == null)
                        {
                            efModel.returnRate = 3;
                        }
                        efModel.years = 5;
                        efModel.status = "Goal Met";
                        efModel.statusValue = "Goal Met";
                        efModel.monthlySavings = "Goal Met";
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            efModel.profileID = profileID;
            return efModel;
        }

        //public List<EmergencyFund_Types> GetFundTypes(ME_v2Entities db)
        //{
        //    EmergencyFundModel efModel = new EmergencyFundModel();
        //    efModel.eTypes = db.EmergencyFund_Types.Where(b => b.eFundTypeID > 0).ToList();
        //    if (efModel.eTypes != null)
        //    {
        //        int i = 0;
        //        foreach (var a in efModel.eTypes)
        //        {
        //            efModel.eTypes[i].EFund = db.EmergencyFunds.Where(b => b.eTypeID == a.eFundTypeID).ToList();
        //            i++;
        //        }
        //    }
        //    return efModel.eTypes;
        //}

        public EmergencyFundModel getMonthlySavings(EmergencyFundModel ef, long profileID)
        {
            decimal s = 0;
            decimal? rate;
            int no_of_months;
            decimal amount;
            decimal goalValue;
            decimal? currentFund;
            decimal? targetAmount;

            try
            {
                EmergencyFundModel efModel = new EmergencyFundModel();
                getDefaultEmergencyFund(profileID, ref efModel);
                if (ef.currentFund == efModel.currentFund)
                {
                    ef.details = efModel.details;
                }

                using (var db = Connection.getConnect())
                {
                    if (ef != null)
                    {
                        rate = (ef.returnRate / 1200);
                        no_of_months = ((ef.years * 12) ?? 5 * 12);
                        targetAmount = (ef.targetAmount) ?? 0;
                        currentFund = (ef.currentFund) ?? 0;
                        if (targetAmount > currentFund)
                        {
                            ef.status = "Shortfall";
                            ef.statusValue = Convert.ToString(ef.targetAmount - (ef.currentFund ?? 0));
                            goalValue = Convert.ToDecimal(ef.statusValue == "" ? "0" : ef.statusValue);
                            decimal value = Math.Round(Convert.ToDecimal((((Math.Pow(Convert.ToDouble(1 + rate), Convert.ToDouble(no_of_months))) - 1) / Convert.ToDouble(rate)) * Convert.ToDouble(1 + rate)), 2);
                            amount = Math.Round(goalValue / value);
                            ef.monthlySavings = Convert.ToString(amount);
                        }
                        else
                        {
                            ef.status = "Goal Met";
                            ef.statusValue = Convert.ToString(ef.currentFund - ef.targetAmount);
                            ef.monthlySavings = "Goal Met";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            ef.profileID = profileID;
            return ef;
        }
    }
}
