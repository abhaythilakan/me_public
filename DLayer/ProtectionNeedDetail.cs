﻿using CLayer;
using System;
using CLayer.common;
using CLayer.DBModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Globalization;

namespace DLayer
{
    public class ProtectionNeedDetail
    {
        EducationInformation edObj = new EducationInformation();
        //To get protection need info
        public ProtectionNeeds getProtectioninfo(long profileID)
        {
            AssetDetails astObj = new AssetDetails();
            ProtectionNeeds pm = new ProtectionNeeds();
            pm.protectionNeedModel = new List<protectionNeedModel>();
            pm.protectionPlan = new List<protectionPlan>();
            List<Person> persons = new List<Person>();
            List<Debt> dt = new List<Debt>();
            InsuranceDetail ins = new InsuranceDetail();
            List<Education> edu = new List<Education>();
            pm.Statement = new List<Statement>();
            pm.profileID = profileID;
            pm.inflationRate = 3;
            pm.returnRate = 6;

            using (var db = Connection.getConnect())
            {
                try
                {
                    if (profileID > 0)
                    {
                        pm.em = edObj.GetEducationInformation(profileID);
                        pm.ar = astObj.getRetirementAsset(profileID); // To calculate retirement asset for primary and spouse
                        persons = db.Persons.Where(b => b.profileID == profileID).ToList();
                        if (persons != null && persons.Count > 0)
                        {
                            int i = 0;
                            int p = 0;
                            foreach (var item in persons)
                            {
                                pm.astModel = astObj.getNonRetAssetDetails(profileID, 1); // To get non retirement asset values
                                pm.protectionNeedModel.Add(new protectionNeedModel { personID = item.personID });
                                pm.protectionNeedModel[i].personName = item.firstName;
                                if (item.isPrimary == true)
                                {
                                    pm.protectionNeedModel[i].isPrimary = true;
                                    p = i;
                                }
                                else
                                {
                                    pm.protectionNeedModel[i].isPrimary = false;
                                }
                                pm.protectionNeedModel[i].totalDebtAndDeath = 0;
                                pm.protectionNeedModel[i].debtIsInclude = false;
                                pm.protectionNeedModel[i].totalMortgage = 0;
                                pm.protectionNeedModel[i].mortgageIsInclude = false;
                                pm.protectionNeedModel[i].incomeReplacement = 0;
                                pm.protectionNeedModel[i].totalEducationCost = 0;
                                pm.protectionNeedModel[i].eduIsInclude = false;
                                pm.protectionNeedModel[i].DebtAndDeath = new DebtAndDeath();
                                //To get debt details of each person
                                dt = db.Debts.Where(b => b.profileID == profileID && b.isActive == true).ToList();
                                if (dt != null && dt.Count > 0)
                                { 
                                    List<DebtDetailsModel> outobj = new List<DebtDetailsModel>();
                                    
                                    //To fetch the details of fixed and revolving debts
                                    outobj = (from dt2 in db.Debts
                                              join dc in db.DebtCats on dt2.debtCatId equals dc.debtCatId
                                              where dt2.profileID == profileID && dc.debtCatId != 1
                                              select new DebtDetailsModel
                                              {
                                                  debtName = dc.catName,
                                                  debtValue = dt2.amount.Value
                                              }).ToList();
                                    if (outobj != null && outobj.Count > 0)
                                    {
                                        pm.protectionNeedModel[i].DebtAndDeath.DebtDetails = new List<DebtDetailsModel>();
                                        pm.protectionNeedModel[i].DebtAndDeath.DebtDetails = outobj;
                                        foreach (var itm in outobj)
                                        {
                                            pm.protectionNeedModel[i].totalDebtAndDeath = pm.protectionNeedModel[i].totalDebtAndDeath + itm.debtValue.Value;
                                        }
                                        pm.protectionNeedModel[i].debtIsInclude = true;
                                    }
                                    
                                    List<Mortgage> outobj1 = new List<Mortgage>();

                                    //To fetch the details of mortgage
                                    outobj1 = (from dt2 in db.Debts
                                               join dc in db.DebtCats on dt2.debtCatId equals dc.debtCatId
                                               where dt2.profileID == profileID && dc.debtCatId == 1
                                               select new Mortgage
                                               {
                                                   mortgageName = dt2.creditor,
                                                   mortgageValue = dt2.amount.Value
                                               }).ToList();
                                    if (outobj1 != null && outobj1.Count > 0)
                                    {
                                        pm.protectionNeedModel[i].mortgageIsInclude = true;
                                        pm.protectionNeedModel[i].Mortgage = new List<Mortgage>();
                                        pm.protectionNeedModel[i].Mortgage = outobj1;
                                        foreach (var itm in outobj1)
                                        {
                                            pm.protectionNeedModel[i].totalMortgage = pm.protectionNeedModel[i].totalMortgage + itm.mortgageValue.Value;
                                        }
                                        pm.protectionNeedModel[i].mortgageIsInclude = true;
                                    }
                                }

                                //To get insurance details of each person
                                ins = db.InsuranceDetails.Where(b => b.profileId == profileID && b.personId == item.personID && b.isActive == true).SingleOrDefault();
                                if (ins != null && ins.InsuranceId > 0 && ins.financialExpence != null)
                                {
                                    pm.protectionNeedModel[i].DebtAndDeath.finalExpenseName = "Final Expenses";
                                    pm.protectionNeedModel[i].DebtAndDeath.finalExpenseValue = ins.financialExpence;
                                    pm.protectionNeedModel[i].totalDebtAndDeath = pm.protectionNeedModel[i].totalDebtAndDeath + ins.financialExpence;
                                    pm.protectionNeedModel[i].debtIsInclude = true;
                                }

                                //To get income replacement value
                                pm.protectionNeedModel[i].incomeReplacement = getIncomeReplacementValue(pm, i);

                                //To fetch the education details of the profile
                                pm.protectionNeedModel[i].EducationNeed = new List<EducationNeed>();
                                if (pm.em.savingsRequired != null)
                                {
                                    int j = 0;
                                    foreach (var a in pm.em.savingsRequired)
                                    {
                                        if (a.savingsRequiredDetails != null)
                                        {
                                            pm.protectionNeedModel[i].EducationNeed.Add(new EducationNeed
                                            {
                                                name = a.name
                                            });
                                            pm.protectionNeedModel[i].EducationNeed[j].EducationNeedDetailsModel = new List<EducationNeedDetailsModel>();
                                            int loop = 0;
                                            foreach (var ed in a.savingsRequiredDetails)
                                            {
                                                pm.protectionNeedModel[i].EducationNeed[j].EducationNeedDetailsModel.Add(new EducationNeedDetailsModel
                                                {
                                                    collegeName=ed.collegename,
                                                    eduValue=ed.lumpSum
                                                });                                                
                                                 
                                                loop++;
                                            }

                                            //pm.protectionNeedModel[i].EducationNeed[j].eduValue = a.savingsRequiredDetails.Where(b => b.returnRate == 6).Select(s => s.lumpSum).FirstOrDefault();
                                        }
                                        j++;
                                    }
                                    pm.protectionNeedModel[i].totalEducationCost = pm.em.savingsRequiredGrandTotal.Select(s => s.lumpSum).FirstOrDefault();

                                    pm.protectionNeedModel[i].eduIsInclude = true;
                                    if (pm.protectionNeedModel[i].totalEducationCost == null)
                                    {
                                        pm.protectionNeedModel[i].totalEducationCost = 0;
                                        pm.protectionNeedModel[i].eduIsInclude = false;
                                    }
                                }
                                pm.protectionNeedModel[i].totalProtectionNeed = 0;
                                pm.protectionNeedModel[i].totalProtectionNeed = getTotalProtectionNeed(pm, i);//To calculate the total protection need

                                List<Group> gp = new List<Group>();
                                List<Individual> ind = new List<Individual>();
                                List<Assets> ast = new List<Assets>();
                                pm.protectionPlan.Add(new protectionPlan { personName = item.firstName });
                                //To fetch the details of group type life insurance
                                gp = (from dt1 in db.lifeInsurances
                                      where dt1.profileID == profileID && dt1.personID == item.personID && dt1.isActive == true && (dt1.lifeTypeID == 3 || dt1.lifeTypeID == 4)
                                      select new Group
                                      {
                                          groupName = dt1.lifeTypeName,
                                          groupValue = dt1.faceValue
                                      }).ToList();
                                pm.protectionPlan[i].Group = new List<Group>();
                                pm.protectionPlan[i].totalGroup = 0;
                                if (gp != null && gp.Count > 0)
                                {
                                    pm.protectionPlan[i].Group = gp;

                                    foreach (var itm in gp)
                                    {
                                        pm.protectionPlan[i].totalGroup = pm.protectionPlan[i].totalGroup + itm.groupValue;
                                    }
                                }
                                pm.protectionPlan[i].totalProtectionPlan = 0;
                                pm.protectionPlan[i].totalProtectionPlan = pm.protectionPlan[i].totalProtectionPlan + pm.protectionPlan[i].totalGroup;
                                //To fetch the details of individual type life insurance
                                ind = (from dt2 in db.lifeInsurances
                                       where dt2.profileID == profileID && dt2.personID == item.personID && dt2.isActive == true && (dt2.lifeTypeID != 3 || dt2.lifeTypeID != 4)
                                       select new Individual
                                       {
                                           individualName = dt2.lifeTypeName,
                                           individualValue = dt2.faceValue
                                       }).ToList();
                                pm.protectionPlan[i].Individual = new List<Individual>();
                                pm.protectionPlan[i].totalIndividual = 0;
                                if (ind != null && ind.Count > 0)
                                {
                                    pm.protectionPlan[i].Individual = ind;
                                    foreach (var itm in ind)
                                    {
                                        pm.protectionPlan[i].totalIndividual = pm.protectionPlan[i].totalIndividual + itm.individualValue;
                                    }
                                }
                                pm.protectionPlan[i].totalProtectionPlan = pm.protectionPlan[i].totalProtectionPlan + pm.protectionPlan[i].totalIndividual;


                                pm.protectionPlan[i].Assets = new List<Assets>();
                                pm.protectionPlan[i].totalAssets = 0;
                                int k = 0;

                                if (ins != null && ins.InsuranceId > 0)
                                {
                                    // To fetch retirement asset value if mentioned to include in insurance details
                                    if (ins.retAsset == true)
                                    {

                                        if (pm.ar != null)
                                        {
                                            foreach (var a in pm.ar)
                                            {
                                                if (a.totalAmount != null)
                                                {
                                                    pm.protectionPlan[i].Assets.Add(new Assets
                                                    {
                                                        assetName = a.name + " " + "Retirement Asset"
                                                    });
                                                    pm.protectionPlan[i].Assets[k].assetValue = a.totalAmount;
                                                    pm.protectionPlan[i].totalAssets = pm.protectionPlan[i].totalAssets + pm.protectionPlan[i].Assets[k].assetValue;
                                                }
                                                k++;
                                            }

                                        }
                                    }
                                    // To fetch other retirement asset value if mentioned to include in insurance details
                                    if (ins.otherRetAsset == true)
                                    {

                                        var totalOther = db.Asset_RetirementDetails.Where(b => b.profileID == profileID && b.isOtherRetirement == true && b.isActive == true).Sum(x => x.amount);
                                        if (totalOther != null)
                                        {
                                            pm.protectionPlan[i].Assets.Add(new Assets
                                            {
                                                assetName = "Other Retirement Assets"
                                            });
                                            pm.protectionPlan[i].Assets[k].assetValue = totalOther;
                                            pm.protectionPlan[i].totalAssets = pm.protectionPlan[i].totalAssets + pm.protectionPlan[i].Assets[k].assetValue;
                                            k++;
                                        }
                                    }
                                    // To fetch non-retirement asset value if mentioned to include in insurance details
                                    if (ins.nonRetAsset == true)
                                    {
                                        if (pm.astModel != null)
                                        {
                                            if (pm.astModel.totalSum > 0)
                                            {
                                                pm.protectionPlan[i].Assets.Add(new Assets
                                                {
                                                    assetName = "Non-Retirement Assets"
                                                });
                                                pm.protectionPlan[i].Assets[k].assetValue = pm.astModel.totalSum;
                                                pm.protectionPlan[i].totalAssets = pm.protectionPlan[i].totalAssets + pm.protectionPlan[i].Assets[k].assetValue;
                                                k++;
                                            }
                                        }
                                    }
                                    // To fetch total cash account if mentioned to include in insurance details
                                    if (ins.cashAccounts == true)
                                    {
                                        pm.astModel = astObj.getNonRetAssetDetails(profileID, 2);
                                        if (pm.astModel != null)
                                        {
                                            pm.protectionPlan[i].Assets.Add(new Assets
                                            {
                                                assetName = "Cash Accounts"
                                            });
                                            pm.protectionPlan[i].Assets[k].assetValue = pm.astModel.totalSum;
                                            pm.protectionPlan[i].totalAssets = pm.protectionPlan[i].totalAssets + pm.protectionPlan[i].Assets[k].assetValue;
                                            k++;
                                        }
                                    }
                                }
                                //To get total protection plan
                                pm.protectionPlan[i].totalProtectionPlan = pm.protectionPlan[i].totalProtectionPlan + pm.protectionPlan[i].totalAssets;
                                //To get protection need shortfall/surplus value
                                pm.protectionPlan[i].Shortfall_Surplus = getShortfall_Surplus(pm, i);

                                i++;
                            }

                            //To get life insurance statement in protection needs
                            pm = getStatements(pm, i);
                            pm.em = null;
                            pm.ar = null;
                            pm.astModel = null;
                            }
                        }
                    }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return pm;
        }
        //Function to get the life insurance statement list 
        public ProtectionNeeds getStatements(ProtectionNeeds pm, int i)
        {
            using (var db = Connection.getConnect())
            {
                try
                {
                    pm.Statement = new List<Statement>();
                    //If either primary or spouse exists
                    if (i == 1)
                    {
                        string name = "";
                        decimal expenseAfterDeath = 0;
                        long personID = 0;
                        long years = 0;
                        InsuranceDetail ins1 = new InsuranceDetail();
                        personID = pm.protectionNeedModel[0].personID.Value;
                        name = pm.protectionNeedModel[0].personName;
                        ins1 = db.InsuranceDetails.Where(b => b.profileId == pm.profileID && b.personId == personID && b.isActive == true).SingleOrDefault();
                        if (ins1 != null)
                        {
                            if (ins1.expenceafterdeath == null)
                            {
                                expenseAfterDeath = 0;
                            }
                            else
                            {
                                expenseAfterDeath = ins1.expenceafterdeath.Value;
                            }
                            if (ins1.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.LifeExpectancy))
                            {
                                years = 90;
                            }
                            else if (ins1.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.SpecificNoOfYear))
                            {
                                years = ins1.insurancePeriod.Value;
                            }

                        }

                        if (expenseAfterDeath > 0)
                        {
                            if (ins1.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.LifeExpectancy) || ins1.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.SpecificNoOfYear))
                            {
                                pm.Statement.Add(new Statement
                                {
                                    statement = name + "'s" + " " + " income replacement total is the amount of money needed" + "  " + "in the event of" + "  " + name + "'s" + "  " + "untimely death to provide $" + expenseAfterDeath + "  " + "a month for" + "  " + years + "year's" + "  " + "inflated at" + "  " + pm.inflationRate + "%."
                                });
                            }
                            else if (ins1.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.Unlimited))
                            {
                                pm.Statement.Add(new Statement
                                {
                                    statement = name + "'s" + " " + " income replacement total is the amount of money needed" + "  " + "in the event of" + "  " + name + "'s" + "  " + "untimely death to provide $" + expenseAfterDeath + "  " + "a month forever inflated at" + "  " + pm.inflationRate + "%."
                                });
                            }

                        }

                    }
                    //If both primary and spouse exists
                    else if (i == 2)
                    {
                        string pName = "";
                        string sName = "";
                        decimal ExpenseAfterPDeath = 0;
                        decimal ExpenseAfterSDeath = 0;
                        long pID = 0;
                        long sID = 0;
                        int j = 0;
                        long years = 0;
                        InsuranceDetail ins1 = new InsuranceDetail();
                        
                        pID = pm.protectionNeedModel[0].personID.Value;
                        pName = pm.protectionNeedModel[0].personName;
                        sID = pm.protectionNeedModel[1].personID.Value;
                        sName = pm.protectionNeedModel[1].personName;
                        ins1 = db.InsuranceDetails.Where(b => b.profileId == pm.profileID && b.personId == pID && b.isActive == true).SingleOrDefault();
                        if (ins1 != null)
                        {
                            if (ins1.expenceafterdeath == null)
                            {
                                ExpenseAfterPDeath = 0;
                            }
                            else
                            {
                                ExpenseAfterPDeath = ins1.expenceafterdeath.Value;
                            }
                            if (ins1.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.LifeExpectancy))
                            {
                                years = 90;
                            }
                            else if (ins1.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.SpecificNoOfYear))
                            {
                                years = ins1.insurancePeriod.Value;
                            }

                        }
                        InsuranceDetail ins2 = new InsuranceDetail();
                        ins2 = db.InsuranceDetails.Where(b => b.profileId == pm.profileID && b.personId == sID && b.isActive == true).SingleOrDefault();
                        if (ins2 != null)
                        {
                            if (ins2.expenceafterdeath == null)
                            {
                                ExpenseAfterSDeath = 0;
                            }
                            else
                            {
                                ExpenseAfterSDeath = ins2.expenceafterdeath.Value;
                            }
                            if (ins2.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.LifeExpectancy))
                            {
                                years = 90;
                            }
                            else if (ins2.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.SpecificNoOfYear))
                            {
                                years = ins2.insurancePeriod.Value;
                            }

                        }
                        if (ExpenseAfterPDeath > 0)
                        {
                            if (ins1.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.LifeExpectancy))
                            {
                                pm.Statement.Add(new Statement
                                {
                                    statement = pName + "'s" + " " + " income replacement total is the amount of money needed for" + "  " + sName + "  " + "in the event of" + "  " + pName + "'s" + "  " + "untimely death to provide $" + ExpenseAfterPDeath + "  " + "a month until" + "  " + sName + "'s" + "  " + "age 90(life expectancy) inflated at" + "  " + pm.inflationRate + "%."
                                });
                            }
                            else if (ins1.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.SpecificNoOfYear))
                            {
                                pm.Statement.Add(new Statement
                                {
                                    statement = pName + "'s" + " " + " income replacement total is the amount of money needed for" + "  " + sName + "  " + "in the event of" + "  " + pName + "'s" + "  " + "untimely death to provide $" + ExpenseAfterPDeath + "  " + "a month for" + "  " + years + " " + "year's inflated at" + "  " + pm.inflationRate + "%."
                                });
                            }
                            else if (ins1.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.Unlimited))
                            {
                                pm.Statement.Add(new Statement
                                {
                                    statement = pName + "'s" + " " + " income replacement total is the amount of money needed for" + "  " + sName + "  " + "in the event of" + "  " + pName + "'s" + "  " + "untimely death to provide $" + ExpenseAfterPDeath + "  " + "a month forever inflated at" + "  " + pm.inflationRate + "%."
                                });
                            }
                        }
                        if (ExpenseAfterSDeath > 0)
                        {
                            if (ins2.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.LifeExpectancy))
                            {
                                pm.Statement.Add(new Statement
                                {
                                    statement = sName + "'s" + " " + "income replacement total is the amount of money needed for" + " " + pName + " " + "in the event of" + " " + sName + "'s " + " " + "untimely death to provide $" + ExpenseAfterSDeath + " " + "a month until" + " " + pName + "'s age 90(life expectancy) inflated at" + " " + pm.inflationRate + "%."
                                });
                            }
                            else if (ins2.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.SpecificNoOfYear))
                            {
                                pm.Statement.Add(new Statement
                                {
                                    statement = sName + "'s" + " " + "income replacement total is the amount of money needed for" + " " + pName + " " + "in the event of" + " " + sName + "'s " + " " + "untimely death to provide $" + ExpenseAfterSDeath + " " + "a month for" + " " + years + "year's inflated at" + " " + pm.inflationRate + "%."
                                });
                            }
                            else if (ins2.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.Unlimited))
                            {
                                pm.Statement.Add(new Statement
                                {
                                    statement = sName + "'s" + " " + "income replacement total is the amount of money needed for" + " " + pName + " " + "in the event of" + " " + sName + "'s " + " " + "untimely death to provide $" + ExpenseAfterSDeath + " " + "a month forever inflated at" + " " + pm.inflationRate + "%."
                                });
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                return pm;
            }
        }
        //To calculate shortfall and surplus value
        public decimal? getShortfall_Surplus(ProtectionNeeds pm, int i)
        {

            decimal shortfall_surplus = 0;
            try
            {
                if (pm.protectionNeedModel[i].totalProtectionNeed >= 0 && pm.protectionPlan[i].totalProtectionPlan >= 0)
                {
                    shortfall_surplus = pm.protectionPlan[i].totalProtectionPlan??0 - pm.protectionNeedModel[i].totalProtectionNeed??0;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return shortfall_surplus;

        }
        //To calculate total protection need
        public decimal getTotalProtectionNeed(ProtectionNeeds pm, int i)
        {
            decimal totalProtectionNeed = 0;
            totalProtectionNeed = pm.protectionNeedModel[i].incomeReplacement.Value;

            if (pm.protectionNeedModel[i].debtIsInclude == true)
            {
                totalProtectionNeed = totalProtectionNeed + (pm.protectionNeedModel[i].totalDebtAndDeath??0);
            }
            if (pm.protectionNeedModel[i].mortgageIsInclude == true)
            {
                totalProtectionNeed = totalProtectionNeed + (pm.protectionNeedModel[i].totalMortgage??0);
            }
            if (pm.protectionNeedModel[i].eduIsInclude == true)
            {
                totalProtectionNeed = totalProtectionNeed + (pm.protectionNeedModel[i].totalEducationCost??0);
            }
            return totalProtectionNeed;
        }
        //To calculate income replacement value
        public decimal? getIncomeReplacementValue(ProtectionNeeds pm,int i)
        {
            InsuranceDetail ins = new InsuranceDetail();
            Person p = new Person();
            long pID = pm.protectionNeedModel[i].personID.Value;
            DateTime birthDate = System.DateTime.Now;
            DateTime dt2;
            int lifeExp = 0;
            TimeSpan TS;
            int years = 0;
            decimal inflationRate = ((pm.inflationRate) ?? 3) / 100;
            decimal returnRate = ((pm.returnRate) ?? 6) / 100;
            decimal incomeNeed = 0, currentNeed = 0, currentPrincipal = 0, currentInterest = 0;
            decimal monthlyNeed = 0;
            using (var db = Connection.getConnect())
            {
                try
                {
                    ins = db.InsuranceDetails.Where(b => b.profileId == pm.profileID && b.personId ==pID && b.isActive == true).FirstOrDefault();
                    if (ins != null && ins.InsuranceId > 0 && ins.expenceafterdeath!=null)
                    {
                        monthlyNeed = ins.expenceafterdeath.Value;
                        if (ins.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.LifeExpectancy))
                        {
                            if (pm.protectionNeedModel[i].isPrimary == true)
                            {
                                p = db.Persons.Where(b => b.profileID == pm.profileID && b.isPrimary == false).FirstOrDefault();
                                if (p != null && p.personID > 0)
                                {
                                    birthDate = p.birthDate.Value;
                                    lifeExp = p.lifeExpectancy.Value;
                                }
                            }
                            else
                            {
                                p = db.Persons.Where(b => b.profileID == pm.profileID && b.isPrimary == true).FirstOrDefault();
                                if (p != null && p.personID > 0)
                                {
                                    birthDate = p.birthDate.Value;
                                    lifeExp = p.lifeExpectancy.Value;

                                }
                            }
                            dt2 = birthDate.AddYears(lifeExp);
                            TS = dt2 - DateTime.Now;
                            years = Convert.ToInt32(Math.Truncate(TS.TotalDays / 365.25));
                            years = years + 1;
                            for (int j = years - 1; j >= 0; j += -1)
                            {
                                currentNeed = (monthlyNeed * 12) * Convert.ToDecimal((Math.Pow(Convert.ToDouble((1 + inflationRate)), j)));
                                currentInterest = (incomeNeed / (1 - returnRate)) - incomeNeed;
                                currentPrincipal = currentNeed - currentInterest;
                                incomeNeed = incomeNeed + currentPrincipal;

                                if (incomeNeed < 0)
                                {
                                    incomeNeed = 0;
                                    break;
                                }
                            }
                        }
                        else if (ins.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.SpecificNoOfYear))
                        {
                            years = Convert.ToInt32(ins.insurancePeriod.Value);
                            for (int j = years - 1; j >= 0; j += -1)
                            {
                                currentNeed = (monthlyNeed * 12) * Convert.ToDecimal((Math.Pow(Convert.ToDouble((1 + inflationRate)), j)));
                                currentInterest = (incomeNeed / (1 - returnRate)) - incomeNeed;
                                currentPrincipal = currentNeed - currentInterest;
                                incomeNeed = incomeNeed + currentPrincipal;
                                if (incomeNeed < 0)
                                {
                                    incomeNeed = 0;
                                    break;
                                }
                            }
                        }
                        else if (ins.insuranceType == Convert.ToInt32(CustomEnum.InsuranceTypesEnum.Unlimited))
                        {
                            incomeNeed = (monthlyNeed * 12) / (returnRate - inflationRate);

                        }
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            incomeNeed = Math.Round(incomeNeed, 0);
            if(incomeNeed<=0)
            {
                return 0;
            }
            else
            { 
            return incomeNeed;
            }
        }
        //To get protection Need values at Go rate
        public ProtectionNeeds goProtectioninfo(ProtectionNeeds pm)
        {
            //decimal inflationRate = (pm.inflationRate)??3;
            //decimal returnRate = (pm.returnRate) ?? 6;
            try
            {
                int i = 0;
                foreach(var item in pm.protectionNeedModel)
                {
                    if(item.incomeReplacement!=null&& item.incomeReplacement>0)
                    {
                        pm.protectionNeedModel[i].incomeReplacement = getIncomeReplacementValue(pm,i);
                    }
                    if (item.EducationNeed != null&&item.EducationNeed.Count>0)
                    {
                        pm.em = edObj.GetEducationInformation(pm.profileID.Value);

                        pm.em.goRate = (pm.returnRate) ?? 6;
                        pm.em.inflation = pm.inflationRate.Value;
                        pm.em = edObj.educationCalculation(pm.em, null);
                        if (pm.em.savingsRequired != null)
                        {
                            pm.protectionNeedModel[i].EducationNeed = new List<EducationNeed>();
                            int j = 0;
                            foreach (var a in pm.em.savingsRequired)
                            {
                              
                                if (a.savingsRequiredDetails != null)
                                {
                                    pm.protectionNeedModel[i].EducationNeed.Add(new EducationNeed
                                    {
                                        name = a.name
                                    });
                                    pm.protectionNeedModel[i].EducationNeed[j].EducationNeedDetailsModel = new List<EducationNeedDetailsModel>();
                                    int loop = 0;
                                    foreach (var ed in a.savingsRequiredDetails)
                                    {
                                        pm.protectionNeedModel[i].EducationNeed[j].EducationNeedDetailsModel.Add(new EducationNeedDetailsModel
                                        {
                                            collegeName = ed.collegename,
                                            eduValue = ed.lumpSum
                                        });

                                        loop++;
                                    }

                                    //pm.protectionNeedModel[i].EducationNeed[j].eduValue = a.savingsRequiredDetails.Where(b => b.returnRate == 6).Select(s => s.lumpSum).FirstOrDefault();
                                }
                                j++;
                            }
                            pm.protectionNeedModel[i].totalEducationCost = pm.protectionNeedModel[i].totalEducationCost = pm.em.savingsRequiredGrandTotal.Select(s => s.lumpSum).FirstOrDefault();
                        }
                    }

                    pm.protectionNeedModel[i].totalProtectionNeed = getTotalProtectionNeed(pm, i);
                    pm.protectionPlan[i].Shortfall_Surplus = getShortfall_Surplus(pm, i);
                i++;

                }
                pm = getStatements(pm, i);

            }
            catch (Exception ex)
            {
                throw;
            }
            pm.em = null;
            return pm;
        }

    }
}
