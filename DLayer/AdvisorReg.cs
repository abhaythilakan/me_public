﻿using CLayer;
using System;
using CLayer.common;
using CLayer.DBModel;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CLayer.AdvisorModel;
using static System.Net.Mime.MediaTypeNames;
using DLayer.Common;
using System.Drawing;
using System.Runtime.InteropServices;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;

namespace DLayer
{
    public class AdvisorReg
    {


        public List<AdvisorEmailListModel> getAdvListBasedOnCat(CustomEnum.EmailAdvisorCategory enm, string couponcode)
        {
            List<AdvisorEmailListModel> mdl = new List<AdvisorEmailListModel>();
            using (var db = Connection.getConnect())
            {
                if ((int)enm == (int)CustomEnum.EmailAdvisorCategory.CurrentAdvisers)
                {
                    mdl = db.Users.Where(b => b.advisorId > 0 && b.isActive == true).Select(b => new AdvisorEmailListModel() { advisorId = b.advisorId ?? 0, advEmail = b.emailPrimary }).ToList();
                }
                else if ((int)enm == (int)CustomEnum.EmailAdvisorCategory.AdviserHasExpiredTheirTrialWithoutValidPayment)
                {
                    mdl = db.Users.Where(b => b.advisorId > 0 && b.isActive == true && b.trialEndDate != null  && b.trialEndDate < DateTime.Now && b.isTrial == true && b.isLicensed != true).Select(b => new AdvisorEmailListModel() { advisorId = b.advisorId ?? 0, advEmail = b.emailPrimary }).ToList();
                }
                else if ((int)enm == (int)CustomEnum.EmailAdvisorCategory.PaidAdviserHasMissedABillingPeriod_LessThan30days)
                {
                    mdl = db.Users.Where(b => b.advisorId > 0 && b.isActive == true && b.trialEndDate != null && b.trialEndDate > DateTime.Now.AddDays(-1) && b.trialEndDate < DateTime.Now && b.isLicensed == true).Select(b => new AdvisorEmailListModel() { advisorId = b.advisorId ?? 0, advEmail = b.emailPrimary }).ToList();
                }
                else if ((int)enm == (int)CustomEnum.EmailAdvisorCategory.PaidAdviserHasExpired)
                {
                    //mdl = db.Users.Where(b => b.advisorId > 0 && b.isActive == true && b.trialEndDate != null && b.trialEndDate > DateTime.Now.AddDays(-1) && b.trialEndDate < DateTime.Now && b.isLicensed == true).Select(b => new AdvisorEmailListModel() { advisorId = b.advisorId ?? 0, advEmail = b.emailPrimary }).ToList();
                }
                else if ((int)enm == (int)CustomEnum.EmailAdvisorCategory.AdviserHasCanceledTheirBillingButCanStillLoginAndRenew)
                {
                    //mdl = db.Users.Where(b => b.advisorId > 0 && b.isActive == true && b.trialEndDate != null && b.trialEndDate > DateTime.Now.AddDays(-1) && b.trialEndDate < DateTime.Now && b.isLicensed == true).Select(b => new AdvisorEmailListModel() { advisorId = b.advisorId ?? 0, advEmail = b.emailPrimary }).ToList();
                }
                else if ((int)enm == (int)CustomEnum.EmailAdvisorCategory.AdviserIsSetToInactiveAndCannotLogin)
                {
                    mdl = db.Users.Where(b => b.advisorId > 0 && b.isActive == false).Select(b => new AdvisorEmailListModel() { advisorId = b.advisorId ?? 0, advEmail = b.emailPrimary }).ToList();
                }
                else if ((int)enm == (int)CustomEnum.EmailAdvisorCategory.CurrentAdvisersOfAParticularCouponCode)
                {
                    //mdl = db.Users.Where(b => b.advisorId > 0 && b.isActive == true && b.trialEndDate != null && b.trialEndDate > DateTime.Now.AddDays(-1) && b.trialEndDate < DateTime.Now && b.isLicensed == true).Select(b => new AdvisorEmailListModel() { advisorId = b.advisorId ?? 0, advEmail = b.emailPrimary }).ToList();
                }

            }
            return mdl;
        }

        public advisorDetails advisorReg(advisorDetails AdvisorDetails)
        {
            AdvisorDetailsDBModel ad = new AdvisorDetailsDBModel();
            //advisorDetails  = new advisorDetails();          
            Common.pwdEncryptDecrypt cipher = new Common.pwdEncryptDecrypt();
            using (var db = Connection.getConnect())
            {
                try
                {
                    //using (var db = Connection.getConnect())
                    //{
                    //var adv = AdvisorDetails.advisorInfo;
                    //var user = AdvisorDetails.UserInfo;
                    if (AdvisorDetails.advisorInfo.advisorId > 0)   //update advisor & user Details
                    {
                        DefaultsReg dr = new DefaultsReg();
                        ad.AdvisorInfoDB = saveAdvisor(AdvisorDetails.advisorInfo, db);
                        if (AdvisorDetails.advBranch != null)
                        { AdvisorDetails.advBranch.bId = savebrach(AdvisorDetails.advBranch, AdvisorDetails.advisorInfo.advisorId); }
                        if (AdvisorDetails.advContact != null)
                        { AdvisorDetails.advContact.contactId = savecontact(AdvisorDetails.advContact, AdvisorDetails.advisorInfo.advisorId); }


                        AdvisorDetails.advDebt.defaultID = dr.saveDebtDefault(AdvisorDetails.advDebt, AdvisorDetails.advisorInfo.advisorId, (int)CustomEnum.userCatEnum.Advisor);
                        AdvisorDetails.advDebt.advisorID = AdvisorDetails.advisorInfo.advisorId;
                        AdvisorDetails.advEducation.defaultID = dr.saveEducationDefault(AdvisorDetails.advEducation, AdvisorDetails.advisorInfo.advisorId, (int)CustomEnum.userCatEnum.Advisor);
                        AdvisorDetails.advEducation.advisorID = AdvisorDetails.advisorInfo.advisorId;
                        AdvisorDetails.advEmergency.defaultID = dr.saveEmergencyFundDefault(AdvisorDetails.advEmergency, AdvisorDetails.advisorInfo.advisorId, (int)CustomEnum.userCatEnum.Advisor);
                        AdvisorDetails.advEmergency.advisorID = AdvisorDetails.advisorInfo.advisorId;
                        AdvisorDetails.advglobal.defultID = dr.saveGlobalSettings(AdvisorDetails.advglobal, AdvisorDetails.advisorInfo.advisorId, (int)CustomEnum.userCatEnum.Advisor);
                        AdvisorDetails.advglobal.advisorID = AdvisorDetails.advisorInfo.advisorId;
                        AdvisorDetails.advIncome.defaultID = dr.saveIncomeProjectionDefault(AdvisorDetails.advIncome, AdvisorDetails.advisorInfo.advisorId, (int)CustomEnum.userCatEnum.Advisor);
                        AdvisorDetails.advIncome.advisorID = AdvisorDetails.advisorInfo.advisorId;
                        AdvisorDetails.advInsurance.defaultID = dr.saveInsuranceDefault(AdvisorDetails.advInsurance, AdvisorDetails.advisorInfo.advisorId, (int)CustomEnum.userCatEnum.Advisor);
                        AdvisorDetails.advInsurance.advisorID = AdvisorDetails.advisorInfo.advisorId;
                        AdvisorDetails.advRetirement.RetirementDefaultID = dr.saveRetirementDefault(AdvisorDetails.advRetirement, AdvisorDetails.advisorInfo.advisorId, (int)CustomEnum.userCatEnum.Advisor);
                        AdvisorDetails.advRetirement.RetirementDefaultID = AdvisorDetails.advisorInfo.advisorId;
                        if (AdvisorDetails.UserInfo.userId > 0)
                        {
                            ad.UserInfoDB = saveUser(AdvisorDetails.UserInfo, db);
                        }

                        PermissionSett ps = new PermissionSett();
                        if (AdvisorDetails.AccesSettings != null)
                        {
                            AdvisorDetails.AccesSettings = ps.saveAdvisorPermission(AdvisorDetails.AccesSettings, AdvisorDetails.advisorInfo.advisorId, db);

                        }
                        return AdvisorDetails;
                    }
                    else
                    {
                        ad.AdvisorInfoDB = new Advisor();
                        DefaultsReg dr = new DefaultsReg();
                        ad.AdvisorInfoDB.firstName = AdvisorDetails.advisorInfo.firstName;
                        ad.AdvisorInfoDB.lastName = AdvisorDetails.advisorInfo.lastName;
                        ad.AdvisorInfoDB.finraReg = false;
                        ad.AdvisorInfoDB.secReg = false;
                        ad.AdvisorInfoDB.designation = AdvisorDetails.advisorInfo.designation;
                        ad.AdvisorInfoDB.isActive = true;
                        DateTime dt = System.DateTime.Now;
                        ad.AdvisorInfoDB.createdDate = dt;
                        db.Advisors.Add(ad.AdvisorInfoDB);
                        db.SaveChanges();
                        AdvisorDetails.advisorInfo.advisorId = ad.AdvisorInfoDB.advisorId;
                        AdvisorDetails.advglobal = dr.getGlobalDefault(0, (int)CustomEnum.userCatEnum.Advisor);
                        AdvisorDetails.advRetirement = dr.getRetirementDefault(0, (int)CustomEnum.userCatEnum.Advisor);
                        AdvisorDetails.advIncome = dr.getIncomeProjectionDefault(0, (int)CustomEnum.userCatEnum.Advisor);
                        AdvisorDetails.advDebt = dr.getDebtDefault(0, (int)CustomEnum.userCatEnum.Advisor);
                        AdvisorDetails.advEmergency = dr.getEmergencyFundDefault(0, (int)CustomEnum.userCatEnum.Advisor);
                        AdvisorDetails.advInsurance = dr.getInsuranceDefault(0, (int)CustomEnum.userCatEnum.Advisor);
                        AdvisorDetails.advEducation = dr.getEducationDefault(0, (int)CustomEnum.userCatEnum.Advisor);
                        ad.UserInfoDB.userName = AdvisorDetails.UserInfo.userName;
                        ad.UserInfoDB.password = cipher.Encrypt(AdvisorDetails.UserInfo.password);
                        ad.UserInfoDB.isActive = true;
                        ad.UserInfoDB.createdDate = dt;
                        ad.UserInfoDB.trialEndDate = dt.AddDays(30);
                        ad.UserInfoDB.emailPrimary = AdvisorDetails.UserInfo.emailPrimary;
                        ad.UserInfoDB.advisorId = ad.AdvisorInfoDB.advisorId;
                        ad.UserInfoDB.clientId = 0;
                        ad.UserInfoDB.isAdmin = false;
                        ad.UserInfoDB.isManager = false;
                        ad.UserInfoDB.isManager = false;
                        ad.UserInfoDB.isSuperUser = false;
                        ad.UserInfoDB.emailPrimaryValid = false;
                        ad.UserInfoDB.emailSecondaryValid = false;
                        ad.UserInfoDB.requestedForgot = false;
                        ad.UserInfoDB.isTrial = true;
                        db.Users.Add(ad.UserInfoDB);
                        db.SaveChanges();
                        AdvisorDetails.UserInfo.userId = ad.UserInfoDB.userId;
                        AdvisorDetails.UserInfo.advisorId = ad.AdvisorInfoDB.advisorId;
                        //transaction.Commit();
                        AdvisorDetails.AccesSettings = new AccesSettingsModel();
                        AdvisorDetails.AccesSettings.AdvancedReportDefaults = new List<adminPagePermissionModel>();
                        AdvisorDetails.AccesSettings.ExpressReportDefaults = new List<adminPagePermissionModel>();
                        //AdvisorDetails.AccesSettings.AdvancedReportDefaults = getReportTypeDefaults((int)CustomEnum.reportTypeEnum.advancedReport, db);
                        // AdvisorDetails.AccesSettings.ExpressReportDefaults = getReportTypeDefaults((int)CustomEnum.reportTypeEnum.expressReport, db);
                        AdvisorDetails.advBranch = new AdvisorBranchModel();
                        AdvisorDetails.advContact = new AdvisorContactModel();
                        AdvisorDetails.advBranch.advisorId = ad.AdvisorInfoDB.advisorId;
                        AdvisorDetails.advContact.advisorId = ad.AdvisorInfoDB.advisorId;


                    }  //save
                    return AdvisorDetails;
                    //}
                }
                catch (Exception ex)
                {
                    //transaction.Rollback();
                    throw;
                }
                //}
            }
        }

        public AdvisorListModel getAdvisorList(AdvisorListModel inObj)
        {
            //ClientListPaginated md = new ClientListPaginated();
            inObj.advisors = new List<advisorList>();
            try
            {
                using (var db = Connection.getConnect())
                {
                    var query = "EXEC dbo.ListAdvisor @advisorID,@PageNo,@SearchText,@PageSize,@SortColumn,@SortOrder";
                    var parameters = new[]
                    {
                        new SqlParameter("advisorID", inObj.Collection.MangerID),
                        new SqlParameter("PageNo", inObj.Collection.PageNo),
                        new SqlParameter("SearchText", inObj.Collection.SearchText??""),
                        new SqlParameter("PageSize", inObj.Collection.PageSize),
                        new SqlParameter("SortColumn", inObj.Collection.SortColumn),
                        new SqlParameter("SortOrder", inObj.Collection.SortOrder)
                    };
                    using (var multiResultSet = DbContextExtensions.MultiResultSetSqlQuery(db, query, parameters))
                    {
                        inObj.advisors = multiResultSet.ResultSetFor<advisorList>().ToList();
                        //md.clients = mdl;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            if (inObj.advisors.Count > 0)
            {
                inObj.Collection.TotalCount = inObj.advisors[0].TotalCount;
            }
            else
            {

                inObj.Collection.TotalCount = 0;
            }

            return inObj;
        }

        public advisorDetails addnewadvisor(advisorDetails AdvisorDetails, long managerID)
        {
            AdvisorDetailsDBModel ad = new AdvisorDetailsDBModel();
            //advisorDetails  = new advisorDetails();          
            Common.pwdEncryptDecrypt cipher = new Common.pwdEncryptDecrypt();
            using (var db = Connection.getConnect())
            {
                try
                {
                    //using (var db = Connection.getConnect())
                    //{
                    //var adv = AdvisorDetails.advisorInfo;
                    //var user = AdvisorDetails.UserInfo;
                    if (managerID > 0)   //update advisor & user Details
                    {
                        if (AdvisorDetails.advisorInfo.advisorId == 0)
                        {
                            // AdvisorMailModel mailObj = new AdvisorMailModel();
                            DateTime dt = System.DateTime.Now;
                            ad.AdvisorInfoDB = new Advisor();
                            DefaultsReg dr = new DefaultsReg();


                            ad.AdvisorInfoDB.firstName = AdvisorDetails.advisorInfo.firstName;
                            ad.AdvisorInfoDB.lastName = AdvisorDetails.advisorInfo.lastName;
                            ad.AdvisorInfoDB.title1 = AdvisorDetails.advisorInfo.titleA;
                            ad.AdvisorInfoDB.title2 = AdvisorDetails.advisorInfo.titleB;
                            ad.AdvisorInfoDB.finraReg = AdvisorDetails.advisorInfo.finraReg;
                            ad.AdvisorInfoDB.secReg = AdvisorDetails.advisorInfo.secReg;
                            ad.AdvisorInfoDB.managerId = AdvisorDetails.advisorInfo.managerId;
                            ad.AdvisorInfoDB.numPlanner = AdvisorDetails.advisorInfo.numPlanner;
                            ad.AdvisorInfoDB.disclaimer = AdvisorDetails.advisorInfo.disclaimer;
                            ad.AdvisorInfoDB.couponCode = AdvisorDetails.advisorInfo.couponCode;
                            ad.AdvisorInfoDB.capitalChoiceCode = AdvisorDetails.advisorInfo.capitalChoiceCode;
                            ad.AdvisorInfoDB.setUpFee = AdvisorDetails.advisorInfo.setUpFee;
                            ad.AdvisorInfoDB.initMonthlyFee = AdvisorDetails.advisorInfo.initMonthlyFee;
                            ad.AdvisorInfoDB.monthlyFee = AdvisorDetails.advisorInfo.monthlyFee;
                            ad.AdvisorInfoDB.riaName = AdvisorDetails.advisorInfo.riaName;
                            ad.AdvisorInfoDB.brokerName = AdvisorDetails.advisorInfo.brokerName;
                            ad.AdvisorInfoDB.designation = AdvisorDetails.advisorInfo.designation;
                            ad.AdvisorInfoDB.imgPath = AdvisorDetails.advisorInfo.imgPath;
                            ad.AdvisorInfoDB.managerId = managerID;
                            ad.AdvisorInfoDB.isActive = true;
                            ad.AdvisorInfoDB.createdDate = System.DateTime.Now;

                            db.Advisors.Add(ad.AdvisorInfoDB);

                            db.SaveChanges();

                            ad.UserInfoDB.userName = AdvisorDetails.UserInfo.userName;
                            ad.UserInfoDB.password = cipher.Encrypt(AdvisorDetails.UserInfo.password);
                            ad.UserInfoDB.isActive = true;
                            ad.UserInfoDB.createdDate = dt;
                            ad.UserInfoDB.trialEndDate = dt.AddDays(30);
                            ad.UserInfoDB.emailPrimary = AdvisorDetails.UserInfo.emailPrimary;
                            ad.UserInfoDB.advisorId = ad.AdvisorInfoDB.advisorId;
                            ad.UserInfoDB.clientId = 0;
                            ad.UserInfoDB.isAdmin = false;
                            ad.UserInfoDB.isManager = false;
                            ad.UserInfoDB.isSuperUser = false;
                            ad.UserInfoDB.emailPrimaryValid = false;
                            ad.UserInfoDB.emailSecondaryValid = false;
                            ad.UserInfoDB.requestedForgot = false;
                            ad.UserInfoDB.isTrial = true;
                            db.Users.Add(ad.UserInfoDB);
                            db.SaveChanges();

                            AdvisorDetails.mailObj = new AdvisorMailModel();
                            AdvisorDetails.mailObj.name = AdvisorDetails.advisorInfo.firstName + " " + AdvisorDetails.advisorInfo.lastName;
                            AdvisorDetails.mailObj.toEmail = AdvisorDetails.UserInfo.emailPrimary;
                            AdvisorDetails.mailObj.advisoremail = db.Users.Where(w => w.advisorId == managerID).Select(s => s.emailPrimary).FirstOrDefault();
                            AdvisorDetails.mailObj.fromDisplayName = db.Advisors.Where(w => w.advisorId == managerID).Select(s => s.firstName).FirstOrDefault();
                            AdvisorDetails.mailObj.password = AdvisorDetails.UserInfo.password;
                            AdvisorDetails.mailObj.username = AdvisorDetails.UserInfo.userName;


                            AdvisorDetails.advDebt.defaultID = dr.saveDebtDefault(AdvisorDetails.advDebt, ad.AdvisorInfoDB.advisorId, (int)CustomEnum.userCatEnum.Advisor);
                            // AdvisorDetails.advDebt.advisorID = AdvisorDetails.advisorInfo.advisorId;
                            AdvisorDetails.advEducation.defaultID = dr.saveEducationDefault(AdvisorDetails.advEducation, ad.AdvisorInfoDB.advisorId, (int)CustomEnum.userCatEnum.Advisor);
                            //AdvisorDetails.advEducation.advisorID = AdvisorDetails.advisorInfo.advisorId;
                            AdvisorDetails.advEmergency.defaultID = dr.saveEmergencyFundDefault(AdvisorDetails.advEmergency, ad.AdvisorInfoDB.advisorId, (int)CustomEnum.userCatEnum.Advisor);
                            //AdvisorDetails.advEmergency.advisorID = AdvisorDetails.advisorInfo.advisorId;
                            AdvisorDetails.advglobal.defultID = dr.saveGlobalSettings(AdvisorDetails.advglobal, ad.AdvisorInfoDB.advisorId, (int)CustomEnum.userCatEnum.Advisor);
                            // AdvisorDetails.advglobal.advisorID = AdvisorDetails.advisorInfo.advisorId;
                            AdvisorDetails.advIncome.defaultID = dr.saveIncomeProjectionDefault(AdvisorDetails.advIncome, ad.AdvisorInfoDB.advisorId, (int)CustomEnum.userCatEnum.Advisor);
                            //AdvisorDetails.advIncome.advisorID = AdvisorDetails.advisorInfo.advisorId;
                            AdvisorDetails.advInsurance.defaultID = dr.saveInsuranceDefault(AdvisorDetails.advInsurance, ad.AdvisorInfoDB.advisorId, (int)CustomEnum.userCatEnum.Advisor);
                            // AdvisorDetails.advInsurance.advisorID = AdvisorDetails.advisorInfo.advisorId;
                            AdvisorDetails.advRetirement.RetirementDefaultID = dr.saveRetirementDefault(AdvisorDetails.advRetirement, ad.AdvisorInfoDB.advisorId, (int)CustomEnum.userCatEnum.Advisor);



                            PermissionSett ps = new PermissionSett();
                            if (AdvisorDetails.AccesSettings != null)
                            {
                                AdvisorDetails.AccesSettings = ps.saveAdvisorPermission(AdvisorDetails.AccesSettings, ad.AdvisorInfoDB.advisorId, db);

                            }
                            return AdvisorDetails;
                        }
                    }
                }

                catch (Exception ex)
                {
                    throw;
                }
                return null;
            }
        }


        public advisorDetails GetAdvisorDetails(long advisorID, long userID)
        {

            advisorDetails outObj = new advisorDetails();
            DefaultsReg dr = new DefaultsReg();
            //advisorDetails  = new advisorDetails();          
            Common.pwdEncryptDecrypt cipher = new Common.pwdEncryptDecrypt();
            using (var db = Connection.getConnect())
            {
                //using (var transaction = db.Database.BeginTransaction(IsolationLevel.Serializable))
                //{
                try
                {
                    if (advisorID > 0 && userID > 0)
                    {
                        outObj.advBranch = GetAdvisorBranch(advisorID);
                        outObj.advContact = GetAdvisorContact(advisorID);
                        outObj.UserInfo = GetUserInfo(userID);
                        outObj.advisorInfo = GetAdvisorInfo(advisorID);
                        outObj.AccesSettings = GetAccessSettings(advisorID);
                        outObj.advglobal = dr.getGlobalDefault(advisorID, (int)CustomEnum.userCatEnum.Advisor);
                        outObj.advRetirement = dr.getRetirementDefault(advisorID, (int)CustomEnum.userCatEnum.Advisor);
                        outObj.advIncome = dr.getIncomeProjectionDefault(advisorID, (int)CustomEnum.userCatEnum.Advisor);
                        outObj.advDebt = dr.getDebtDefault(advisorID, (int)CustomEnum.userCatEnum.Advisor);
                        outObj.advEmergency = dr.getEmergencyFundDefault(advisorID, (int)CustomEnum.userCatEnum.Advisor);
                        outObj.advInsurance = dr.getInsuranceDefault(advisorID, (int)CustomEnum.userCatEnum.Advisor);
                        outObj.advEducation = dr.getEducationDefault(advisorID, (int)CustomEnum.userCatEnum.Advisor);
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return outObj;
        }

        public advisorDetails GetAddnewAdvisorDetails(long advisorID)
        {
            advisorDetails outObj = new advisorDetails();
            DefaultsReg dr = new DefaultsReg();
            //advisorDetails  = new advisorDetails();          
            Common.pwdEncryptDecrypt cipher = new Common.pwdEncryptDecrypt();
            using (var db = Connection.getConnect())
            {
                try
                {
                    if (advisorID > 0)
                    {
                        outObj.advBranch = new AdvisorBranchModel();
                        outObj.advBranch.bId = 0;// = GetAdvisorBranch(advisorID);
                        outObj.advContact = new AdvisorContactModel();
                        outObj.advContact.contactId = 0;// = GetAdvisorContact(advisorID);
                        outObj.UserInfo = new UserModels();
                        outObj.UserInfo.userId = 0;// = GetAddAdvisorUserInfo(advisorID);
                        outObj.advisorInfo = new AdvisorModel();
                        outObj.advisorInfo.advisorId = 0;// = GetAdvisorInfo(advisorID);
                        outObj.AccesSettings = GetAccessSettings(advisorID);
                        outObj.advglobal = dr.getGlobalDefault(advisorID, (int)CustomEnum.userCatEnum.Advisor);
                        outObj.advRetirement = dr.getRetirementDefault(advisorID, (int)CustomEnum.userCatEnum.Advisor);
                        outObj.advIncome = dr.getIncomeProjectionDefault(advisorID, (int)CustomEnum.userCatEnum.Advisor);
                        outObj.advDebt = dr.getDebtDefault(advisorID, (int)CustomEnum.userCatEnum.Advisor);
                        outObj.advEmergency = dr.getEmergencyFundDefault(advisorID, (int)CustomEnum.userCatEnum.Advisor);
                        outObj.advInsurance = dr.getInsuranceDefault(advisorID, (int)CustomEnum.userCatEnum.Advisor);
                        outObj.advEducation = dr.getEducationDefault(advisorID, (int)CustomEnum.userCatEnum.Advisor);
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return outObj;
        }

        private AccesSettingsModel GetAccessSettings(long advisorID)
        {
            AccesSettingsModel outObj = new AccesSettingsModel();
            outObj.AdvancedReportDefaults = new List<adminPagePermissionModel>();
            outObj.ExpressReportDefaults = new List<adminPagePermissionModel>();
            List<Permission> dbobj;


            using (var db = Connection.getConnect())
            {
                try
                {
                    // dbobj  = new List<Permission>();
                    // dbobj = db.Permissions.Where(b => b.advisorId == advisorID).ToList();

                    outObj.AdvancedReportDefaults = (from ins in db.Permissions
                                                     join p in db.Pages on ins.pageId equals p.pageId
                                                     where ins.advisorId == advisorID && ins.reportTypeID == 1
                                                     select new adminPagePermissionModel
                                                     {
                                                         perID = ins.perID,
                                                         pageName = p.pageName,
                                                         isEdit = ins.isEdit,
                                                         isRead = ins.isRead
                                                     }).ToList();

                    outObj.ExpressReportDefaults = (from ins in db.Permissions
                                                    join p in db.Pages on ins.pageId equals p.pageId
                                                    where ins.advisorId == advisorID && ins.reportTypeID == 2
                                                    select new adminPagePermissionModel
                                                    {
                                                        perID = ins.perID,
                                                        pageName = p.pageName,
                                                        isEdit = ins.isEdit,
                                                        isRead = ins.isRead
                                                    }).ToList();

                    if (outObj.AdvancedReportDefaults.Count == 0 || outObj.AdvancedReportDefaults == null)
                    {
                        outObj.AdvancedReportDefaults = getReportTypeDefaults((int)CustomEnum.reportTypeEnum.advancedReport, db);
                        int i = 0;
                        foreach (var a in outObj.AdvancedReportDefaults)
                        {
                            Permission dbobjP = new Permission()
                            {
                                advisorId = advisorID,
                                isEdit = a.isEdit,
                                isRead = a.isRead,
                                pageId = a.pageId,
                                clientId = 0,
                                profileId = 0,
                                reportTypeID = (int)CustomEnum.reportTypeEnum.advancedReport,

                            };
                            db.Permissions.Add(dbobjP);
                            db.SaveChanges();
                            outObj.AdvancedReportDefaults[i].perID = dbobjP.perID;
                            i++;
                        }
                    }
                    if (outObj.ExpressReportDefaults.Count == 0 || outObj.ExpressReportDefaults == null)
                    {
                        outObj.ExpressReportDefaults = getReportTypeDefaults((int)CustomEnum.reportTypeEnum.expressReport, db);
                        int i = 0;
                        foreach (var a in outObj.ExpressReportDefaults)
                        {
                            Permission dbobjP = new Permission()
                            {
                                advisorId = advisorID,
                                isEdit = a.isEdit,
                                isRead = a.isRead,
                                pageId = a.pageId,
                                clientId = 0,
                                profileId = 0,
                                reportTypeID = (int)CustomEnum.reportTypeEnum.expressReport,
                            };
                            db.Permissions.Add(dbobjP);
                            db.SaveChanges();
                            outObj.AdvancedReportDefaults[i].perID = dbobjP.perID;
                            i++;
                        }
                    }


                }
                catch (Exception ex)
                {
                    throw;
                }

            }
            return outObj;
        }

        private AdvisorModel GetAdvisorInfo(long advisorID)
        {
            AdvisorModel outObj = new AdvisorModel();
            Advisor dbobj = new Advisor();
            using (var db = Connection.getConnect())
            {
                try
                {
                    dbobj = db.Advisors.SingleOrDefault(b => b.advisorId == advisorID);
                    if (dbobj != null)
                    {
                        outObj = new AdvisorModel();
                        outObj.firstName = dbobj.firstName;
                        outObj.lastName = dbobj.lastName;
                        outObj.titleA = dbobj.title1;
                        outObj.titleB = dbobj.title2;
                        outObj.finraReg = dbobj.finraReg ?? true;
                        outObj.secReg = dbobj.secReg ?? true;
                        outObj.managerId = dbobj.managerId;
                        outObj.numPlanner = dbobj.numPlanner;
                        outObj.disclaimer = dbobj.disclaimer;
                        outObj.couponCode = dbobj.couponCode;
                        outObj.capitalChoiceCode = dbobj.capitalChoiceCode;
                        outObj.setUpFee = dbobj.setUpFee;
                        outObj.initMonthlyFee = dbobj.initMonthlyFee;
                        outObj.monthlyFee = dbobj.monthlyFee;
                        outObj.riaName = dbobj.riaName;
                        outObj.brokerName = dbobj.brokerName;
                        outObj.designation = dbobj.designation;
                        outObj.imgPath = dbobj.imgPath;
                        outObj.advisorId = dbobj.advisorId;
                    }
                    return outObj;
                }
                catch (Exception ex)
                {
                    throw;
                }

            }
            return null;
        }

        private UserModels GetUserInfo(long userID)
        {
            UserModels outObj = new UserModels();
            User dbobj = new User();
            using (var db = Connection.getConnect())
            {
                try
                {
                    dbobj = db.Users.SingleOrDefault(b => b.userId == userID);
                    if (dbobj != null && dbobj.userId > 0)
                    {
                        outObj = new UserModels();
                        outObj.emailPrimary = dbobj.emailPrimary;
                        outObj.emailSecondary = dbobj.emailSecondary;
                        outObj.userName = dbobj.userName;
                        outObj.userId = dbobj.userId;
                        outObj.advisorId = dbobj.advisorId;
                    }
                    else { return null; }
                }
                catch (Exception ex)
                {
                    throw;
                }

            }
            return outObj;
        }
        private UserModels GetAddAdvisorUserInfo(long advisorID)
        {
            UserModels outObj = new UserModels();
            User dbobj = new User();
            using (var db = Connection.getConnect())
            {
                try
                {
                    dbobj = db.Users.SingleOrDefault(b => b.advisorId == advisorID);
                    if (dbobj != null && dbobj.userId > 0)
                    {
                        outObj = new UserModels();
                        outObj.emailPrimary = dbobj.emailPrimary;
                        outObj.emailSecondary = dbobj.emailSecondary;
                        outObj.userName = dbobj.userName;
                        outObj.userId = dbobj.userId;
                        outObj.advisorId = dbobj.advisorId;
                    }
                    else { return null; }
                }
                catch (Exception ex)
                {
                    throw;
                }

            }
            return outObj;
        }


        private AdvisorContactModel GetAdvisorContact(long advisorID)
        {
            AdvisorContactModel outObj = new AdvisorContactModel();
            AdvisorContact dbobj = new AdvisorContact();
            using (var db = Connection.getConnect())
            {
                try
                {
                    if (dbobj != null)
                    {
                        dbobj = db.AdvisorContacts.SingleOrDefault(b => b.advisorId == advisorID);
                        if (dbobj != null && dbobj.contactId > 0)
                        {
                            outObj = new AdvisorContactModel();
                            outObj.address = dbobj.address;
                            outObj.city = dbobj.city;
                            outObj.state = dbobj.state;
                            outObj.zip = dbobj.zip;
                            outObj.telephone = dbobj.telephone;
                            outObj.cellphone = dbobj.cellphone;
                            outObj.advisorId = dbobj.advisorId;
                            outObj.contactId = dbobj.contactId;
                        }
                        outObj.advisorId = advisorID;
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }

            }
            return outObj;
        }

        private AdvisorBranchModel GetAdvisorBranch(long advisorID)
        {
            AdvisorBranchModel outObj = new AdvisorBranchModel(); ;
            AdvisorBranch dbobj = new AdvisorBranch();
            using (var db = Connection.getConnect())
            {
                try
                {

                    if (dbobj != null)
                    {
                        dbobj = db.AdvisorBranches.SingleOrDefault(b => b.advisorId == advisorID);
                        if (dbobj != null && dbobj.bId > 0)
                        {
                            // outObj = new AdvisorBranchModel();
                            outObj.advisorId = dbobj.advisorId;
                            outObj.bAddress = dbobj.bAddress;
                            outObj.bCity = dbobj.bCity;
                            outObj.bCompany = dbobj.bCompany;
                            outObj.bFirstName = dbobj.bFirstName;
                            outObj.bId = dbobj.bId;
                            outObj.bLastName = dbobj.bLastName;
                            outObj.bPhone = dbobj.bPhone;
                            outObj.bState = dbobj.bState;
                            outObj.bZip = dbobj.bZip;
                            outObj.bId = dbobj.bId;
                        }
                        else
                        {
                            outObj.advisorId = advisorID;

                        }
                    }

                }
                catch (Exception ex)
                {
                    throw;
                }

            }
            return outObj;
        }

        //public advisorDetails GetAdvisorDetails(long advisorID, long userID)
        //{

        //}

        public string getDisclaimer(long id)
        {
            string s;
            try
            {
                using (var db = Connection.getConnect())
                {
                    s = db.Desclaimers.Where(b => (b.desclaimerID == id)).Select(b => b.desclaimerBody).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return s;


        }

        public bool advImgUpload(string path, long id)
        {
            bool status = false;
            try
            {
                using (var db = Connection.getConnect())
                {
                    Advisor ad = db.Advisors.SingleOrDefault(b => b.advisorId == id);
                    ad.imgPath = path;
                    db.Entry(ad).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    status = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return status;
        }

        public bool advisordelete(long id)
        {
            bool isDeleted = false;
            try
            {
                using (var db = Connection.getConnect())
                {
                    Advisor ad = db.Advisors.SingleOrDefault(b => b.advisorId == id);

                    if (ad != null && ad.advisorId > 0)
                    {
                        ad.isActive = false;
                        db.Entry(ad).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        isDeleted = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isDeleted;
        }


        private long savecontact(AdvisorContactModel advisorContact, long id)
        {
            long globalID = 0;
            try
            {
                using (var db2 = Connection.getConnect())
                {
                    AdvisorContact ac = db2.AdvisorContacts.Where(b => (b.advisorId == id)).FirstOrDefault();

                    if (ac != null && advisorContact.contactId > 0)
                    {
                        ac.address = advisorContact.address;
                        ac.city = advisorContact.city;
                        ac.state = advisorContact.state;
                        ac.zip = advisorContact.zip;
                        ac.telephone = advisorContact.telephone;
                        ac.cellphone = advisorContact.cellphone;
                        db2.Entry(ac).State = System.Data.Entity.EntityState.Modified;
                        db2.SaveChanges();
                    }
                    else if (advisorContact != null && id > 0)
                    {
                        ac = new AdvisorContact();
                        ac.address = advisorContact.address;
                        ac.city = advisorContact.city;
                        ac.state = advisorContact.state;
                        ac.zip = advisorContact.zip;
                        ac.telephone = advisorContact.telephone;
                        ac.cellphone = advisorContact.cellphone;
                        ac.advisorId = id;
                        db2.AdvisorContacts.Add(ac);
                        db2.SaveChanges();
                        globalID = ac.contactId;
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return globalID;
        }
        private long savebrach(AdvisorBranchModel advisorBranch, long id)
        {
            long globalID = 0;
            try
            {
                using (var db2 = Connection.getConnect())
                {
                    AdvisorBranch ab = db2.AdvisorBranches.Where(b => (b.advisorId == id)).FirstOrDefault();

                    if (ab != null && advisorBranch.bId > 0)
                    {
                        ab.bFirstName = advisorBranch.bFirstName;
                        ab.bLastName = advisorBranch.bLastName;
                        ab.bAddress = advisorBranch.bAddress;
                        ab.bCity = advisorBranch.bCity;
                        ab.bState = advisorBranch.bState;
                        ab.bZip = advisorBranch.bZip;
                        ab.bPhone = advisorBranch.bPhone;
                        ab.bCompany = advisorBranch.bCompany;
                        db2.Entry(ab).State = System.Data.Entity.EntityState.Modified;
                        db2.SaveChanges();
                    }
                    else if (advisorBranch != null && id > 0)
                    {

                        ab = new AdvisorBranch();
                        ab.advisorId = id;
                        ab.bFirstName = advisorBranch.bFirstName;
                        ab.bLastName = advisorBranch.bLastName;
                        ab.bAddress = advisorBranch.bAddress;
                        ab.bCity = advisorBranch.bCity;
                        ab.bState = advisorBranch.bState;
                        ab.bZip = advisorBranch.bZip;
                        ab.bPhone = advisorBranch.bPhone;
                        ab.bCompany = advisorBranch.bCompany;

                        db2.AdvisorBranches.Add(ab);
                        db2.SaveChanges();
                        globalID = ab.bId;
                    }


                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return globalID;
        }
        private User saveUser(UserModels user, ME_v2Entities db)
        {
            try
            {
                //using (var db = Connection.getConnect())
                //{
                User usr = db.Users.Single(b => b.advisorId == user.advisorId);
                usr.emailPrimary = user.emailPrimary;
                usr.emailSecondary = user.emailSecondary;
                // usr.password = user.password;
                usr.userName = user.userName;
                db.Entry(usr).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return usr;
                //}
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public Advisor saveAdvisor(AdvisorModel advisorInfo, ME_v2Entities db)
        {

            try
            {
                //using (var db = Connection.getConnect())
                //{
                Advisor advs = db.Advisors.Single(b => b.advisorId == advisorInfo.advisorId);


                advs.firstName = advisorInfo.firstName;
                advs.lastName = advisorInfo.lastName;
                advs.title1 = advisorInfo.titleA;
                advs.title2 = advisorInfo.titleB;
                advs.finraReg = advisorInfo.finraReg;
                advs.secReg = advisorInfo.secReg;
                advs.managerId = advisorInfo.managerId;
                advs.numPlanner = advisorInfo.numPlanner;
                advs.disclaimer = advisorInfo.disclaimer;
                advs.couponCode = advisorInfo.couponCode;
                advs.capitalChoiceCode = advisorInfo.capitalChoiceCode;
                advs.setUpFee = advisorInfo.setUpFee;
                advs.initMonthlyFee = advisorInfo.initMonthlyFee;
                advs.monthlyFee = advisorInfo.monthlyFee;
                advs.riaName = advisorInfo.riaName;
                advs.brokerName = advisorInfo.brokerName;
                advs.designation = advisorInfo.designation;
                advs.imgPath = advisorInfo.imgPath;
                db.Entry(advs).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();


                return advs;
                //}
            }
            catch (Exception ex)
            {
                throw;
            }

        }


        #region fill db class to model class
        public UserModels filluser(User userInfoDB)
        {
            UserModels obj = new UserModels();
            obj.userName = userInfoDB.userName;
            obj.createdDate = userInfoDB.createdDate;
            obj.emailPrimary = userInfoDB.emailPrimary;
            obj.advisorId = userInfoDB.advisorId ?? 0;
            obj.userId = userInfoDB.userId;
            // obj.password=userInfoDB.password;
            // obj.isActive = userInfoDB.isActive;
            // obj.trialEndDate = userInfoDB.trialEndDate;
            return obj;
        }
        public AdvisorModel filladvisor(Advisor advisorInfoDB)
        {
            AdvisorModel obj = new AdvisorModel();
            obj.firstName = advisorInfoDB.firstName;
            obj.lastName = advisorInfoDB.lastName;
            obj.finraReg = advisorInfoDB.finraReg ?? false;
            obj.secReg = advisorInfoDB.secReg ?? false;
            obj.createdDate = advisorInfoDB.createdDate;
            obj.advisorId = advisorInfoDB.advisorId;
            obj.brokerName = advisorInfoDB.brokerName;
            obj.capitalChoiceCode = advisorInfoDB.capitalChoiceCode;
            obj.couponCode = advisorInfoDB.couponCode;
            obj.disclaimer = advisorInfoDB.disclaimer;
            obj.initMonthlyFee = advisorInfoDB.initMonthlyFee;
            obj.monthlyFee = advisorInfoDB.monthlyFee;
            obj.managerId = advisorInfoDB.managerId;
            obj.numPlanner = advisorInfoDB.numPlanner;
            obj.riaName = advisorInfoDB.riaName;
            obj.setUpFee = advisorInfoDB.setUpFee;
            obj.titleA = advisorInfoDB.title1;
            obj.titleB = advisorInfoDB.title2;
            return obj;
        }
        #endregion
        private List<CLayer.adminPagePermissionModel> getReportTypeDefaults(int advancedReport, ME_v2Entities dbs)
        {
            List<CLayer.adminPagePermissionModel> report = new List<CLayer.adminPagePermissionModel>();

            try
            {

                using (var db = Connection.getConnect())
                {
                    var query = "EXEC getAdminPagePermission @reptype";
                    var parameters = new[]
                    {

                        new SqlParameter("@reptype", (int)advancedReport)
                    };
                    using (var multiResultSet = DbContextExtensions.MultiResultSetSqlQuery(db, query, parameters))
                    {
                        report = multiResultSet.ResultSetFor<adminPagePermissionModel>().ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return report;



        }

    }
}
