﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using DLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLayer
{
    public class ActionPlanDetails
    {
        //To get details of predefined actions
        public List<preDefinedActionModel> getPredefinedActions()
        {
            using (var db = Connection.getConnect())
            {

                List<preDefinedActionModel> dd = new List<preDefinedActionModel>();
                try
                {
                    List<PredefinedAction> aa = db.PredefinedActions.Where(b => b.actionNameID > 0).ToList();
                    if (aa != null && aa.Count() > 0)
                    {
                        foreach (PredefinedAction itm in aa)
                        {
                            preDefinedActionModel dd1 = new preDefinedActionModel();
                            dd1.actionID = itm.actionNameID;
                            dd1.actionName = itm.actionName;
                            dd.Add(dd1);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                return dd;
            }
        }
        //To fill the Assigned To dropdownlist
        public List<DropDownList_Int> getDDL_AssignedTo(long profileID)
        {
            using (var db = Connection.getConnect())
            {
                List<DropDownList_Int> DDL = new List<DropDownList_Int>();
                try
                {
                    var advisorName = (from dt in db.Advisors
                                       join dc in db.Clients on dt.advisorId equals dc.advisorId
                                       join dd in db.profiles on dc.clientId equals dd.clientID
                                       where dd.profileID == profileID
                                       select dt).FirstOrDefault().firstName;

                    string name = (advisorName == null) ? " " : advisorName.ToString();
                    List<Person> p = new List<Person>();
                    p = db.Persons.Where(b => b.profileID == profileID).ToList();
                    if (p != null && p.Count != 0)
                    {

                        if (p.Count == 2)
                        {
                            DDL.Add(new DropDownList_Int { VALUE = 1, TEXT = name });
                            DDL.Add(db.Persons.Where(b => b.profileID == profileID && b.isPrimary == true).Select(b => new CLayer.common.DropDownList_Int() { VALUE = 2, TEXT = b.firstName }).FirstOrDefault());
                            DDL.Add(db.Persons.Where(b => b.profileID == profileID && b.isPrimary == false).Select(b => new CLayer.common.DropDownList_Int() { VALUE = 3, TEXT = b.firstName }).FirstOrDefault());
                            DDL.Add(new DropDownList_Int() { VALUE = 4, TEXT = "Both " + DDL[1].TEXT + " and " + DDL[2].TEXT });
                            DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "Other" });
                        }
                        else
                        {
                            if (p[0].isPrimary == true)
                            {
                                DDL.Add(new DropDownList_Int { VALUE = 1, TEXT = advisorName.ToString() });
                                DDL.Add(new DropDownList_Int { VALUE = 2, TEXT = p[0].firstName });
                                DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "Other" });
                            }
                            else
                            {
                                DDL.Add(new DropDownList_Int { VALUE = 1, TEXT = advisorName.ToString() });
                                DDL.Add(new DropDownList_Int { VALUE = 3, TEXT = p[0].firstName });
                                DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "Other" });
                            }
                        }
                    }
                    else
                    {
                        DDL.Add(new DropDownList_Int { VALUE = 1, TEXT = name });
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "Other" });
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                return DDL;
            }
        }
        //To save action
        public actionPointModel saveActionPlan(actionPointModel ap, long userID)
        {
            using (var db = Connection.getConnect())
            {
                ActionPlan actionDB = new ActionPlan();
                try
                {
                    if (ap != null && (ap.actionID == null|| ap.actionID == 0))
                    {
                        actionDB = fillActionPlan(ap, actionDB);
                        actionDB.addedBy = userID;
                        actionDB.actionCreatedDate = System.DateTime.Now;
                        actionDB.actionModifiedBy = userID;
                        actionDB.isResheduled = false;
                        db.ActionPlans.Add(actionDB);
                        db.SaveChanges();
                        ap.actionID = actionDB.actionID;
                    }
                    else if (ap != null && ap.actionID > 0)
                    {
                        actionDB = db.ActionPlans.SingleOrDefault(b => b.actionID == ap.actionID);
                        if (actionDB != null && actionDB.actionID >0)
                        {
                            actionDB = fillActionPlan(ap, actionDB);
                            actionDB.actionModifiedBy = userID;
                            actionDB.actionUpdatedDate = System.DateTime.Now;
                            actionDB.isResheduled = true;
                            db.Entry(actionDB).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            ap.isResheduled = actionDB.isResheduled;
                        }

                    }

                    if (ap.assignedTo == Convert.ToInt32(CustomEnum.ActionAssignedTO.Advisor))
                    {
                        ap.advisorEmail = (from dt in db.Users
                                           join dc in db.Clients on dt.advisorId equals dc.advisorId
                                           join dd in db.profiles on dc.clientId equals dd.clientID
                                           where dd.profileID == ap.profileID
                                           select dt).FirstOrDefault().emailPrimary;
                        // long advisor_id = db.Users.Where(b => b.userId == userID).Select(b => b.advisorId.Value).SingleOrDefault();
                        //ap.fromUser = db.Advisors.Where(b => b.advisorId == advisor_id).Select(b => b.firstName).FirstOrDefault();
                    }
                    else if (ap.assignedTo == Convert.ToInt32(CustomEnum.ActionAssignedTO.Primary))
                    {
                        ap.primaryEmail = db.Persons.Where(b => b.profileID == ap.profileID && b.isPrimary == true).Select(b => b.emailID).FirstOrDefault();
                    }
                    else if (ap.assignedTo == Convert.ToInt32(CustomEnum.ActionAssignedTO.Spouse))
                    {
                        ap.spouseEmail = db.Persons.Where(b => b.profileID == ap.profileID && b.isPrimary == false).Select(b => b.emailID).FirstOrDefault();
                    }
                    else if (ap.assignedTo == Convert.ToInt32(CustomEnum.ActionAssignedTO.Both))
                    {
                        ap.primaryEmail = db.Persons.Where(b => b.profileID == ap.profileID && b.isPrimary == true).Select(b => b.emailID).FirstOrDefault();
                        ap.spouseEmail = db.Persons.Where(b => b.profileID == ap.profileID && b.isPrimary == false).Select(b => b.emailID).FirstOrDefault();
                    }


                }
                catch (Exception ex)
                {
                    throw;
                }

                long clientID = 0;
                long advisorID = 0;
                var cID = (db.Users.Where(b => b.userId == userID).Select(b => b.clientId).FirstOrDefault()) ?? 0;
                if (cID != 0)
                {
                    clientID = Convert.ToInt64(cID);
                }
                var advID = (db.Users.Where(b => b.userId == userID).Select(b => b.advisorId).FirstOrDefault()) ?? 0;
                if (advID != 0)
                {
                    advisorID = Convert.ToInt64(advID);
                }
                if (clientID > 0)
                {
                    ap.fromUser = db.Clients.Where(b => b.clientId == clientID).Select(b => b.firstName).FirstOrDefault();
                }
                else if (advID > 0)
                {
                    ap.fromUser = db.Advisors.Where(b => b.advisorId == advID).Select(b => b.firstName).FirstOrDefault();
                }
                return ap;
            }
        }
        //Function To save action from model class to DB
        public CLayer.DBModel.ActionPlan fillActionPlan(actionPointModel ap, ActionPlan actionDB)
        {
            actionDB.profileID = ap.profileID.Value;
            actionDB.actionNameID = ap.actionNameID;
            actionDB.actionName = ap.actionName;

            //To check due date less than current date
            DateTime zeroTime = new DateTime(1, 1, 1);
            DateTime a;
            DateTime b;
            a = (DateTime)ap.dueDate;
            b = System.DateTime.Now;
            TimeSpan span = a - b;
            decimal years = 0;
            try
            {
                years = Convert.ToDecimal((zeroTime + span).Year - 1);
            }
            catch (Exception ex)
            {
                throw new Exception("Due Date must be greater than current date");

            }

            actionDB.dueDate = ap.dueDate;
            actionDB.assignedToType = ap.assignedTo;
            if (ap.assignedTo == 0)
            {
                actionDB.assignedToType = ap.otherActions.otherID;
                actionDB.otherName = ap.otherActions.otherPersonName;
                actionDB.otherEmailID = ap.otherActions.otherEmailID;
            }
            actionDB.notificationTypeID = ap.notificationTypeID;
            actionDB.comment = ap.comment;
            actionDB.actionStatus = (ap.actionStatus) ?? 2;
            return actionDB;
        }
        //To list actions
        public List<actionList> GetActions(long profileID, int searchType)
        {
            List<ActionPlan> DBobj = new List<ActionPlan>();
            List<actionList> actionList = new List<actionList>();
            actionList actionObj = new actionList();
            try
            {
                using (var db = Connection.getConnect())
                {
                    //To get All actions
                    if (searchType == 1)
                    {
                        DBobj = db.ActionPlans.Where(b => b.profileID == profileID && b.actionStatus!=4).ToList();
                        if (DBobj == null)
                        {
                            return null;
                        }
                        else
                        {
                            foreach (ActionPlan itm in DBobj)
                            {
                                if (itm != null && itm.actionID > 0)
                                {
                                    actionList.Add(fillActionDbToModel(itm, actionObj, searchType));
                                }
                            }
                        }

                        if (actionList != null)
                        {
                            return actionList;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    //To get all actions today
                    else if (searchType == 2)
                    {
                        DateTime dt = System.DateTime.Now.Date;
                        DBobj = db.ActionPlans.Where(b => b.profileID == profileID && b.dueDate == dt).ToList();
                        if (DBobj == null)
                        {
                            return null;
                        }
                        else
                        {
                            foreach (ActionPlan itm in DBobj)
                            {
                                if (itm != null && itm.actionID > 0)
                                {
                                    actionList.Add(fillActionDbToModel(itm, actionObj, searchType));
                                }
                            }
                        }

                        if (actionList != null)
                        {
                            return actionList;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    //To get all not completed actions this Month
                    else if (searchType == 3)
                    {
                        DateTime Todaysdate = System.DateTime.Now.Date;
                        DateTime firstDayOfMonth = new DateTime(Todaysdate.Year, Todaysdate.Month, 1);
                        DateTime lastDayOfMonth = firstDayOfMonth.AddMonths(1).AddDays(-1);
                        DBobj = db.ActionPlans.Where(b => b.profileID == profileID && b.dueDate <= lastDayOfMonth && b.dueDate >= firstDayOfMonth).ToList();
                        if (DBobj == null)
                        {
                            return null;
                        }
                        else
                        {
                            foreach (ActionPlan itm in DBobj)
                            {
                                if (itm != null && itm.actionID > 0)
                                {
                                    actionList.Add(fillActionDbToModel(itm, actionObj, searchType));
                                }
                            }
                        }

                        if (actionList != null)
                        {
                            return actionList;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    //To get past due/pending actions
                    else if (searchType == 4)
                    {
                        DateTime dt1 = System.DateTime.Now.Date;

                        DBobj = db.ActionPlans.Where(b =>
                                                b.profileID == profileID &&
                                                     (b.actionStatus == 2 || b.actionStatus == 3) &&
                                                    b.dueDate <= dt1).ToList();

                        foreach (var item in DBobj)
                        {
                            if (item != null && item.actionID > 0)
                            {
                                actionList.Add(fillActionDbToModel(item, actionObj, searchType));
                            }
                        }

                        if (actionList != null)
                        {
                            return actionList;
                        }
                        else
                        {
                            return null;
                        }

                    }
                    //To get upcoming actions
                    else if (searchType == 5)
                    {
                        DateTime Todaysdate = System.DateTime.Now.Date;
                        DBobj = db.ActionPlans.Where(b =>
                                                          b.profileID == profileID &&
                                                          (b.actionStatus == 2 || b.actionStatus == 3) &&
                                                          b.dueDate > Todaysdate).ToList();
                        foreach (var item in DBobj)
                        {
                            if (item != null && item.actionID > 0)

                            {
                                actionList.Add(fillActionDbToModel(item, actionObj, searchType));
                            }
                        }

                        if (actionList != null)
                        {
                            return actionList;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    //To get progressing actions
                    else if (searchType == 6)
                    {
                        DateTime Todaysdate = System.DateTime.Now.Date;
                        DBobj = db.ActionPlans.Where(b =>
                                                          b.profileID == profileID &&
                                                          b.actionStatus == 3).ToList();
                        foreach (var item in DBobj)
                        {
                            if (item != null && item.actionID > 0)

                            {
                                actionList.Add(fillActionDbToModel(item, actionObj, searchType));
                            }
                        }

                        if (actionList != null)
                        {
                            return actionList;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    //To get completed actions
                    else if (searchType == 7)
                    {
                        DateTime Todaysdate = System.DateTime.Now.Date;
                        DBobj = db.ActionPlans.Where(b =>
                                                          b.profileID == profileID &&
                                                          b.actionStatus == 1).ToList();
                        foreach (var item in DBobj)
                        {
                            if (item != null && item.actionID > 0)

                            {
                                actionList.Add(fillActionDbToModel(item, actionObj, searchType));
                            }
                        }

                        if (actionList != null)
                        {
                            return actionList;
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return actionList;
        }
        //Function to fetch action from db to model
        public actionList fillActionDbToModel(ActionPlan item, actionList actionObj, int searchType)
        {
            actionObj = new actionList()
            {
                actionID = item.actionID,
                actionPoint = item.actionName,
                dueDate = item.dueDate.Value.ToString("dd MMM yyyy"),
                comment = item.comment,
                actionStatus = (item.actionStatus) ?? 2
            };
            if (searchType == 1 || searchType == 2 || searchType == 3)
            {
                if (actionObj.actionStatus == Convert.ToInt32(CustomEnum.actionStatus.Open))
                    actionObj.actionPoint = actionObj.actionPoint + "(New)";
                if (actionObj.actionStatus == Convert.ToInt32(CustomEnum.actionStatus.Completed))
                    actionObj.actionPoint = actionObj.actionPoint + "(Completed)";
                if (actionObj.actionStatus == Convert.ToInt32(CustomEnum.actionStatus.Progressing))
                    actionObj.actionPoint = actionObj.actionPoint + "(In Progress)";
            }
            return actionObj;
        }
        public bool changeStatus(long actionID, long userID, int statusType)
        {
            ActionPlan ap = new ActionPlan();
            using (var db = Connection.getConnect())
            {

                try
                {
                    if (actionID > 0)
                    {
                        ap = db.ActionPlans.Where(b => b.actionID == actionID).SingleOrDefault();
                        if (ap != null && ap.actionID > 0)
                        {
                            if (statusType > 0)
                            {
                                ap.actionStatus = statusType;
                                ap.actionModifiedBy = userID;
                                ap.actionUpdatedDate = System.DateTime.Now;
                                db.Entry(ap).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }

                        }

                    }
                }

                catch (Exception ex)
                {
                    throw;
                }
                return true;
            }
        }
        public actionPointModel getOnReshedule(long actionID)
        {
            actionPointModel ap = new actionPointModel();
            ap.otherActions = new otherActions();
            ActionPlan dbobj = new ActionPlan();
            string dt;
            using (var db = Connection.getConnect())
            {
                try
                {
                    if (actionID > 0)
                    {
                        dbobj = db.ActionPlans.SingleOrDefault(b => b.actionID == actionID);
                        if (dbobj != null && dbobj.actionID > 0)
                        {
                            DateTime duedate = dbobj.dueDate.Value.Date;
                            ap.actionID = dbobj.actionID;
                            ap.profileID = dbobj.profileID;
                            ap.actionNameID = dbobj.actionNameID;
                            ap.actionName = dbobj.actionName;
                            // var due = duedate.ToString("MM-dd-yyyy");
                            //var due = (DateTime.ParseExact(dt, "MM-dd-yyyy", CultureInfo.InvariantCulture)).Date;
                            ap.dueDate = duedate;
                            ap.assignedTo = dbobj.assignedToType;
                            ap.otherActions.otherPersonName = dbobj.otherName;
                            ap.otherActions.otherEmailID = dbobj.otherEmailID;
                            ap.notificationTypeID = dbobj.notificationTypeID;
                            ap.comment = dbobj.comment;

                        }
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
                return ap;
            }
        }
    }
}
