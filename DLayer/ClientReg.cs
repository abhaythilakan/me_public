﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using DLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace DLayer
{
    public class ClientReg
    {
        public void saveClientImage(long clientID, string imagePath)
        {
            try
            {
                using (var db = Connection.getConnect())
                {
                    Client data = db.Clients.Single(b => b.clientId == clientID);
                    data.imagePath = imagePath;
                    db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public ClientListPaginated getClientList(ClientListArgModel data)
        {
            ClientListPaginated md = new ClientListPaginated();
            List<ClientModel> mdl = new List<ClientModel>();
            try
            {
                using (var db = Connection.getConnect())
                {
                    var query = "EXEC dbo.ListClients @advisorID,@PageNo,@SearchText,@PageSize,@SortColumn,@SortOrder";
                    var parameters = new[]
                    {
                        new SqlParameter("advisorID", data.advisorId),
                        new SqlParameter("PageNo", data.PageNo),
                        new SqlParameter("SearchText", data.SearchText),
                        new SqlParameter("PageSize", data.PageSize),
                        new SqlParameter("SortColumn", data.SortColumn),
                        new SqlParameter("SortOrder", data.SortOrder)
                    };
                    using (var multiResultSet = DbContextExtensions.MultiResultSetSqlQuery(db, query, parameters))
                    {
                        mdl = multiResultSet.ResultSetFor<ClientModel>().ToList();
                        md.clients = mdl;
                    }
                    if (md.clients != null && md.clients.Count() > 0)
                    {
                        md.Collection = new ClientListArgModel();
                        md.Collection.advisorId = data.advisorId;
                        md.Collection.TotalCount = Convert.ToInt32(md.clients[0].TotalCount);
                        md.Collection.PageNo = data.PageNo;
                        md.Collection.PageSize = data.PageSize;
                        md.Collection.SortColumn = data.SortColumn;
                        md.Collection.SortOrder = data.SortOrder;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return md;
        }

        public ClientModel saveClient(ClientModel data, long UserID)
        {
            using (var db = Connection.getConnect())
            {
                //using (TransactionScope transaction = new TransactionScope())
                //{
                try
                {
                    //using (var db = Connection.getConnect())
                    //{
                    data.clientDOB = Convert.ToDateTime(data.clientDOBString);
                    var cd = data;
                    if (cd.clientId > 0)
                    {
                        Client cli = db.Clients.Single(b => b.clientId == cd.clientId);
                        cli.firstName = cd.firstName;
                        cli.lastName = cd.lastName;
                        cli.advisorId = cd.advisorId;
                        cli.DOBdate = Convert.ToDateTime(cd.clientDOBString);
                        cli.reportType = cd.reportType;
                        cli.modifiedDate = DateTime.Now;
                        cli.modifiedBy = UserID;
                        int indx = cd.imagePath == null ? -1 : cd.imagePath.IndexOf("Files");
                        cli.imagePath = indx >= 0 ? cd.imagePath.Substring(indx) : cd.imagePath;
                        //int indx = cd.imagePath.IndexOf("Files");
                        //cli.imagePath = cd.imagePath.Substring(indx);
                        db.Entry(cli).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        data.clientId = cli.clientId;
                        data.imagePath = indx >= 0 ? cd.imagePath.Substring(indx) : cd.imagePath;
                        //data.imagePath = cd.imagePath.Substring(indx);
                    }
                    else
                    {
                        Client cli = new Client();
                        cli.firstName = cd.firstName;
                        cli.lastName = cd.lastName;
                        cli.advisorId = cd.advisorId;
                        cli.createdDate = DateTime.Now;
                        cli.modifiedDate = DateTime.Now;
                        cli.modifiedBy = UserID;
                        cli.DOBdate = Convert.ToDateTime(cd.clientDOBString);
                        cli.reportType = cd.reportType;
                        int indx = cd.imagePath == null ? -1 : cd.imagePath.IndexOf("Files");
                        cli.imagePath = indx >= 0 ? cd.imagePath.Substring(indx) : cd.imagePath;
                        cli.isActive = true;
                        db.Clients.Add(cli);
                        db.SaveChanges();
                        data.clientId = cli.clientId;
                        data.imagePath = indx >= 0 ? cd.imagePath.Substring(indx) : cd.imagePath;
                        if (data.clientId > 0)
                        {
                            ClientModel md = saveUser(data, db, UserID);
                            DLayer.DefaultsReg dl = new DefaultsReg();
                            dl.saveAdvisorDefaultsToClient(Convert.ToInt64(data.advisorId), data.clientId);
                            ProfileReg pg = new ProfileReg();
                            ProfileModel pm = new ProfileModel();
                            pm.profileDate = DateTime.Now;
                            pm.profileName = data.firstName + " " + data.lastName;
                            pm.clientID = data.clientId;
                            pm.isActive = true;
                            pm = pg.saveProfile(pm, db);
                            if (pm.profileID > 0 && data.clientId > 0)
                            {
                                dl.saveClientDefaultsToProfile(data.clientId, pm.profileID);

                                #region save information to person table for primary
                                DefaultsRetirement ret = db.DefaultsRetirements.Where(b => b.clientID == data.clientId).FirstOrDefault();
                                DLayer.PersonalInformation p = new PersonalInformation();
                                PersonModel person = new PersonModel()
                                {
                                    birthDate = data.clientDOB,
                                    firstName = data.firstName,
                                    lastName = data.lastName,
                                    isMale = data.clientGender == "Male" ? true : false,
                                    isPrimary = true,
                                    profileID = pm.profileID,
                                    lifeExpectancy = ret.lifeExpectency,
                                    retirementAge = ret.retireAge,
                                    inclEducPage = false

                                };
                                person.emailID = db.Users.Where(w => w.clientId == data.clientId).Select(s => s.emailPrimary).FirstOrDefault();
                                var a = db.Persons.Where(w => w.emailID == person.emailID).FirstOrDefault();
                                if (a == null)
                                {
                                    p.savePersonalInfo(person, (int)CustomEnum.userTypeEnum.primary, UserID);
                                }
                                #endregion 

                            }
                        }
                    }
                    //transaction.Complete();
                    //}
                }
                catch (Exception ex)
                {
                    //transaction.Dispose();
                    throw;
                }
                //}
            }
            return data;
        }

        public ClientModel saveUser(ClientModel data, ME_v2Entities db, long createdBy)
        {
            try
            {
                //using (var db = Connection.getConnect())
                //{
                pwdEncryptDecrypt en = new pwdEncryptDecrypt();
                User us = new User();
                us = db.Users.FirstOrDefault(b => b.clientId == data.clientId);
                if (us != null && us.userId > 0)
                {
                    //us.userName = data.userName;
                    us.emailPrimary = data.emailPrimary;
                    us.clientId = data.clientId;
                    //us.password = en.Encrypt(data.password);
                    us.isActive = true;
                    //us.createdDate = DateTime.Now;
                    //us.createdBy = createdBy;
                    db.Entry(us).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    data.userId = us.userId;
                }
                else
                {
                    us = new User();
                    us.userName = data.userName;
                    us.emailPrimary = data.emailPrimary;
                    us.clientId = data.clientId;
                    us.password = en.Encrypt(data.password);
                    us.isActive = true;
                    us.createdDate = DateTime.Now;
                    us.createdBy = createdBy;
                    db.Users.Add(us);
                    db.SaveChanges();
                    data.userId = us.userId;
                    //}
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return data;
        }

        public ClientCollection1 saveClientFull(ClientCollection1 data)
        {
            User us;
            PermissionSett ps = new PermissionSett();
            ClientCollection1 dd = new ClientCollection1();
            List<PermissionModel> ddl = new List<PermissionModel>();
            Client cli = new Client();
            ProfileReg pg = new ProfileReg();
            ProfileModel pm = new ProfileModel();
            data.usrCat = new UserCat();
            data.usrCat.usrCat = (int)CustomEnum.userCatEnum.Client;
            using (var db = Connection.getConnect())
            {
                //using (TransactionScope transaction = new TransactionScope())
                //{
                try
                {
                    //using (var db = Connection.getConnect())
                    //{
                    var cd = data.client;

                    #region Client Save -- Commented  
                    //if (cd.clientId > 0)
                    //{
                    //    cli = db.Clients.Single(b => b.clientId == cd.clientId);
                    //    cli.firstName = cd.firstName;
                    //    cli.lastName = cd.lastName;
                    //    cli.advisorId = cd.advisorId;
                    //    cli.DOBdate = Convert.ToDateTime(cd.clientDOBString);
                    //    cli.reportType = cd.reportType;
                    //    //cli.createdDate = DateTime.Now;
                    //    //cli.imagePath = cd.imagePath;
                    //    db.Entry(cli).State = System.Data.Entity.EntityState.Modified;
                    //    db.SaveChanges();
                    //    data.client.clientId = cli.clientId;
                    //    data.client.imagePath = cd.imagePath;
                    //}
                    //else
                    //{
                    //    cli = new Client();
                    //    cli.firstName = cd.firstName;
                    //    cli.lastName = cd.lastName;
                    //    cli.advisorId = cd.advisorId;
                    //    cli.createdDate = DateTime.Now;
                    //    cli.DOBdate = Convert.ToDateTime(cd.clientDOBString);
                    //    cli.reportType = cd.reportType;
                    //    //cli.imagePath = cd.imagePath;
                    //    cli.isActive = true;
                    //    db.Clients.Add(cli);
                    //    db.SaveChanges();
                    //    data.client.clientId = cli.clientId;
                    //    data.client.imagePath = cd.imagePath;
                    //    if (data.client.clientId > 0)
                    //    {
                    //        pm.profileDate = DateTime.Now;
                    //        pm.profileName = cd.firstName + " " + cd.lastName;
                    //        pm.clientID = data.client.clientId;
                    //        pm.isActive = true;
                    //        pg.saveProfile(pm, db);
                    //    }
                    //}
                    #endregion
                    #region Client user Save - Commented
                    //if (data.client.clientId > 0)
                    //{
                    //    if (data.client.userId > 0)
                    //    {
                    //        us = db.Users.FirstOrDefault(b => b.userId == data.client.userId);
                    //        if (us != null)
                    //        {
                    //            us.userName = data.client.userName;
                    //            us.emailPrimary = data.client.emailPrimary;
                    //            //us.clientId = data.client.clientId;
                    //            us.isActive = true;
                    //            db.Entry(us).State = System.Data.Entity.EntityState.Modified;
                    //            db.SaveChanges();
                    //            data.client.userId = us.userId;
                    //        }
                    //    }
                    //    else
                    //    {
                    //        Common.pwdEncryptDecrypt cipher = new Common.pwdEncryptDecrypt();
                    //        us = new User();
                    //        us.userName = data.client.userName;
                    //        us.emailPrimary = data.client.emailPrimary;
                    //        us.clientId = data.client.clientId;
                    //        us.password = cipher.Encrypt(data.client.password);
                    //        us.isActive = true;
                    //        db.Users.Add(us);
                    //        db.SaveChanges();
                    //        data.client.userId = us.userId;
                    //    }
                    //}
                    #endregion

                    if (cd.clientId > 0)
                    {
                        cli = db.Clients.Single(b => b.clientId == cd.clientId);
                        if (data.client != null && data.client.clientId > 0)
                        {
                            dd = saveClientSettings(data, db);
                            User uss = new User();
                            uss = db.Users.Where(b => b.clientId == cd.clientId).FirstOrDefault();
                            data.global = dd.global;
                            data.ret = dd.ret;
                            data.Income = dd.Income;
                            data.debt = dd.debt;
                            data.emerg = dd.emerg;
                            data.insu = dd.insu;
                            data.edu = dd.edu;
                            if (cli.reportType > 0)
                            {
                                if (data.permissionList != null)
                                {
                                    for (int i = 0; i < data.permissionList.Count(); i++)
                                    {
                                        data.permissionList[i].reportTypeID = cli.reportType;
                                    }
                                    data.permissionList = ps.savePermission(data.permissionList, data.client.clientId, CustomEnum.userCatEnum.Client, db);
                                    data.permissionList = new List<PermissionModel>();
                                    data.permissionList = ps.getPermissionList(uss.userId, CustomEnum.userCatEnum.Client);
                                }
                                else
                                {
                                    if (uss != null && uss.userId > 0)
                                    {
                                        ddl = ps.getPermissionList(uss.userId, CustomEnum.userCatEnum.Client);
                                        data.permissionList = ddl;
                                        data.permissionList = ps.savePermission(data.permissionList, data.client.clientId, CustomEnum.userCatEnum.Client, db);
                                        data.permissionList = new List<PermissionModel>();
                                        data.permissionList = ps.getPermissionList(uss.userId, CustomEnum.userCatEnum.Client);
                                    }
                                }
                            }
                        }
                    }
                    //}
                }
                catch (Exception ex)
                {
                    //transaction.Dispose();
                    throw;
                }
                //}
            }
            return data;
        }

        public ClientCollection1 saveClientSettings(ClientCollection1 data, ME_v2Entities db)
        {
            try
            {
                //using (var db = Connection.getConnect())
                //{
                DefaultsReg dg = new DefaultsReg();
                var cd = data.client;
                if (cd.clientId > 0)
                {
                    if (data.global != null)
                    {
                        long globalID = dg.saveGlobalSettings(data.global, cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        data.global.defultID = globalID;
                    }
                    else
                    {
                        data.global = dg.getGlobalDefault(cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        long globalID = dg.saveGlobalSettings(data.global, cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        data.global.defultID = globalID;
                    }
                    if (data.ret != null)
                    {
                        long retID = dg.saveRetirementDefault(data.ret, cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        data.ret.RetirementDefaultID = retID;
                    }
                    else
                    {
                        data.ret = dg.getRetirementDefault(cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        long retID = dg.saveRetirementDefault(data.ret, cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        data.ret.RetirementDefaultID = retID;
                    }
                    if (data.Income != null)
                    {
                        long inProjID = dg.saveIncomeProjectionDefault(data.Income, cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        data.Income.defaultID = inProjID;
                    }
                    else
                    {
                        data.Income = dg.getIncomeProjectionDefault(cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        long inProjID = dg.saveIncomeProjectionDefault(data.Income, cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        data.Income.defaultID = inProjID;
                    }
                    if (data.debt != null)
                    {
                        long debtID = dg.saveDebtDefault(data.debt, cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        data.debt.defaultID = debtID;
                    }
                    else
                    {
                        data.debt = dg.getDebtDefault(cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        long debtID = dg.saveDebtDefault(data.debt, cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        data.debt.defaultID = debtID;
                    }
                    if (data.emerg != null)
                    {
                        long emerID = dg.saveEmergencyFundDefault(data.emerg, cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        data.emerg.defaultID = emerID;
                    }
                    else
                    {
                        data.emerg = dg.getEmergencyFundDefault(cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        long emerID = dg.saveEmergencyFundDefault(data.emerg, cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        data.emerg.defaultID = emerID;
                    }
                    if (data.insu != null)
                    {
                        long insuID = dg.saveInsuranceDefault(data.insu, cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        data.insu.defaultID = insuID;
                    }
                    else
                    {
                        data.insu = dg.getInsuranceDefault(cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        long insuID = dg.saveInsuranceDefault(data.insu, cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        data.insu.defaultID = insuID;
                    }
                    if (data.edu != null)
                    {
                        long eduID = dg.saveEducationDefault(data.edu, cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        data.edu.defaultID = eduID;
                    }
                    else
                    {
                        data.edu = dg.getEducationDefault(cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        long eduID = dg.saveEducationDefault(data.edu, cd.clientId, (int)CustomEnum.userCatEnum.Client);
                        data.edu.defaultID = eduID;
                    }
                }
                //}
            }
            catch (Exception ex)
            {
                throw;
            }
            return data;
        }

        public ClientCollection getClientDetails(long clientID, bool isClientOnly)
        {
            ClientCollection md = new ClientCollection();
            try
            {
                using (var db = Connection.getConnect())
                {
                    if (clientID > 0)
                    {
                        DefaultsReg dg = new DefaultsReg();
                        User us = db.Users.Where(b => b.clientId == clientID).FirstOrDefault();
                        Client data = db.Clients.Single(b => b.clientId == clientID);
                        md.client = new ClientModel();
                        md.client.clientId = data.clientId;
                        md.client.firstName = data.firstName;
                        md.client.lastName = data.lastName;
                        md.client.advisorId = data.advisorId;
                        md.client.createdDate = data.createdDate;
                        md.client.imagePath = data.imagePath;
                        md.client.clientDOB = Convert.ToDateTime(data.DOBdate);
                        md.client.reportType = data.reportType;
                        if (us != null)
                        {
                            md.client.userId = us.userId;
                            md.client.userName = us.userName;
                            md.client.emailPrimary = us.emailPrimary;
                            md.client.advisorFirstName = db.Advisors.Where(b => b.advisorId == data.advisorId).FirstOrDefault().firstName ?? "";
                            md.client.advisorLastName = db.Advisors.Where(b => b.advisorId == data.advisorId).FirstOrDefault().lastName ?? "";
                        }

                        ProfileModel pl = new ProfileModel();
                        long proID = db.profiles.Where(b => b.clientID == clientID && b.isActive == true).OrderBy(b => b.createdDate).Take(1).FirstOrDefault().profileID;
                        if (proID > 0)
                        {
                            ProfileReg pg = new ProfileReg();
                            pl = pg.getProfileDetails(proID).profile;
                            if (pl != null)
                            {
                                md.profile = pl;
                            }
                        }
                        if (!isClientOnly)
                        {
                            md.global = dg.getGlobalDefault(clientID, (int)CustomEnum.userCatEnum.Client);
                            md.ret = dg.getRetirementDefault(clientID, (int)CustomEnum.userCatEnum.Client);
                            md.Income = dg.getIncomeProjectionDefault(clientID, (int)CustomEnum.userCatEnum.Client);
                            md.debt = dg.getDebtDefault(clientID, (int)CustomEnum.userCatEnum.Client);
                            md.emerg = dg.getEmergencyFundDefault(clientID, (int)CustomEnum.userCatEnum.Client);
                            md.insu = dg.getInsuranceDefault(clientID, (int)CustomEnum.userCatEnum.Client);
                            md.edu = dg.getEducationDefault(clientID, (int)CustomEnum.userCatEnum.Client);

                            if (data.reportType > 0)
                            {
                                PermissionSett ps = new PermissionSett();
                                md.permissionList = ps.getPermissionList(clientID, CustomEnum.userCatEnum.Client);
                                var perLS = new List<PermissionModel>();
                                perLS = ps.savePermission(md.permissionList, clientID, CustomEnum.userCatEnum.Client, db);
                                md.permissionList = ps.getPermissionList(clientID, CustomEnum.userCatEnum.Client);
                            }
                        }
                    }
                    else
                    {
                        md.client = new ClientModel();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return md;
        }

        public bool deleteClient(long clientID, long UserID)
        {
            bool isDeleted = false;
            try
            {
                using (var db = Connection.getConnect())
                {
                    Client cl = db.Clients.SingleOrDefault(b => b.clientId == clientID);
                    cl.isActive = false;
                    cl.modifiedDate = DateTime.Now;
                    cl.modifiedBy = UserID;
                    db.Entry(cl).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    isDeleted = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isDeleted;
        }


    }
}
