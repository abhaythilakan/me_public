﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using DLayer.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLayer
{
    public class DefaultsReg
    {
        #region Permission
        public void getPageList()
        {
            List<Page> pg = new List<Page>();
            try
            {
                using (var db = Connection.getConnect())
                {
                    db.Pages.ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        public void saveAdvisorDefaultsToClient(long advisorID, long clientID)
        {
            try
            {
                using (var db = Connection.getConnect())
                {
                    DefaultsGlobal dg = db.DefaultsGlobals.Where(b => b.advisorID == advisorID).FirstOrDefault();
                    DefaultsGlobal dgC = db.DefaultsGlobals.Where(b => b.clientID == clientID).FirstOrDefault();
                    if (dg != null && dg.defultID > 0 && dgC == null)
                    {
                        DefaultsGlobal dgNew = new DefaultsGlobal();
                        dgNew.clientID = clientID;
                        dgNew.timeZone = dg.timeZone;
                        dgNew.currency = dg.currency;
                        dgNew.defaultstate = dg.defaultstate;
                        db.DefaultsGlobals.Add(dgNew);
                        //db.SaveChanges();
                    }
                    DefaultsRetirement ret = db.DefaultsRetirements.Where(b => b.advisorID == advisorID).FirstOrDefault();
                    DefaultsRetirement retC = db.DefaultsRetirements.Where(b => b.clientID == clientID).FirstOrDefault();
                    if (ret != null && ret.RetirementDefaultID > 0 && retC == null)
                    {
                        DefaultsRetirement retNew = new DefaultsRetirement();
                        retNew.clientID = clientID;
                        retNew.retireAge = ret.retireAge;
                        retNew.lifeExpectency = ret.lifeExpectency;
                        retNew.preRetReturnTax = ret.preRetReturnTax;
                        retNew.retTax = ret.retTax;
                        retNew.retReturnTax = ret.retReturnTax;
                        retNew.monthIncome = ret.monthIncome;
                        retNew.inflation = ret.inflation;
                        db.DefaultsRetirements.Add(retNew);
                        //db.SaveChanges();
                    }
                    DefaultIncomeProjection inc = db.DefaultIncomeProjections.Where(b => b.advisorID == advisorID).FirstOrDefault();
                    DefaultIncomeProjection incC = db.DefaultIncomeProjections.Where(b => b.clientID == clientID).FirstOrDefault();
                    if (inc != null && inc.defaultID > 0 && incC == null)
                    {
                        DefaultIncomeProjection incNew = new DefaultIncomeProjection();
                        incNew.clientID = clientID;
                        incNew.primaryInflation = inc.primaryInflation;
                        incNew.spouseInflation = inc.spouseInflation;
                        incNew.inflation = inc.inflation;
                        db.DefaultIncomeProjections.Add(incNew);
                        //db.SaveChanges();
                    }
                    DefaultsDebt deb = db.DefaultsDebts.Where(b => b.advisorID == advisorID).FirstOrDefault();
                    DefaultsDebt debC = db.DefaultsDebts.Where(b => b.clientID == clientID).FirstOrDefault();
                    if (deb != null && deb.defaultID > 0 && debC == null)
                    {
                        DefaultsDebt debNew = new DefaultsDebt();
                        debNew.clientID = clientID;
                        debNew.minPay = deb.minPay;
                        debNew.unpaidPercentage = deb.unpaidPercentage;
                        debNew.inflation = deb.inflation;
                        db.DefaultsDebts.Add(debNew);
                        //db.SaveChanges();
                    }
                    DefaultsEmergencyFund eme = db.DefaultsEmergencyFunds.Where(b => b.advisorID == advisorID).FirstOrDefault();
                    DefaultsEmergencyFund emeC = db.DefaultsEmergencyFunds.Where(b => b.clientID == clientID).FirstOrDefault();
                    if (eme != null && eme.defaultID > 0 && emeC == null)
                    {
                        DefaultsEmergencyFund emeNew = new DefaultsEmergencyFund();
                        emeNew.clientID = clientID;
                        emeNew.emergencyFundValue = eme.emergencyFundValue;
                        emeNew.rate1 = eme.rate1;
                        emeNew.rate2 = eme.rate2;
                        emeNew.rate3 = eme.rate3;
                        emeNew.inflation = eme.inflation;
                        emeNew.isPieChart = eme.isPieChart;
                        emeNew.isThreeMonth = eme.isThreeMonth;
                        db.DefaultsEmergencyFunds.Add(emeNew);
                        //db.SaveChanges();
                    }
                    DefaultsInsurance insu = db.DefaultsInsurances.Where(b => b.advisorID == advisorID).FirstOrDefault();
                    DefaultsInsurance insuC = db.DefaultsInsurances.Where(b => b.clientID == clientID).FirstOrDefault();
                    if (insu != null && insu.defaultID > 0 && insuC == null)
                    {
                        DefaultsInsurance insuNew = new DefaultsInsurance();
                        insuNew.clientID = clientID;
                        insuNew.cost = insu.cost;
                        insuNew.monthlyIncome = insu.monthlyIncome;
                        insuNew.yearAfterDeath = insu.yearAfterDeath;
                        insuNew.retAsset = insu.retAsset;
                        insuNew.otherRetAsset = insu.otherRetAsset;
                        insuNew.cashAccounts = insu.cashAccounts;
                        insuNew.spouseRetAsset = insu.spouseRetAsset;
                        insuNew.nonRetAsset = insu.nonRetAsset;
                        insuNew.inflation = insu.inflation;
                        insuNew.yearAfterDeath = insu.yearAfterDeath;
                        insuNew.specificnoofyears = insu.specificnoofyears ?? 20;
                        db.DefaultsInsurances.Add(insuNew);
                        //db.SaveChanges();
                    }
                    DefaultsEducation edu = db.DefaultsEducations.Where(b => b.advisorID == advisorID).FirstOrDefault();
                    DefaultsEducation eduC = db.DefaultsEducations.Where(b => b.clientID == clientID).FirstOrDefault();
                    if (edu != null && edu.defaultID > 0 && eduC == null)
                    {
                        DefaultsEducation eduNew = new DefaultsEducation();
                        eduNew.clientID = clientID;
                        eduNew.collegeStartMonth = edu.collegeStartMonth;
                        eduNew.tutionFeeInflation = edu.tutionFeeInflation;
                        eduNew.collegeAge = edu.collegeAge;
                        eduNew.collageYears = edu.collageYears;
                        eduNew.rate1 = edu.rate1;
                        eduNew.rate2 = edu.rate2;
                        eduNew.rate3 = edu.rate3;
                        eduNew.parentPay = edu.parentPay;
                        db.DefaultsEducations.Add(eduNew);
                        //db.SaveChanges();
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void saveClientDefaultsToProfile(long clientID, long profileID)
        {
            try
            {
                using (var db = Connection.getConnect())
                {
                    DefaultsGlobal dg = db.DefaultsGlobals.Where(b => b.clientID == clientID).FirstOrDefault();
                    DefaultsGlobal dgP = db.DefaultsGlobals.Where(b => b.profileID == profileID).FirstOrDefault();
                    if (dg != null && dg.defultID > 0 && dgP == null)
                    {
                        DefaultsGlobal dgNew = new DefaultsGlobal();
                        dgNew.profileID = profileID;
                        dgNew.timeZone = dg.timeZone;
                        dgNew.currency = dg.currency;
                        dgNew.defaultstate = dg.defaultstate;
                        db.DefaultsGlobals.Add(dgNew);
                        //db.SaveChanges();
                    }
                    DefaultsRetirement ret = db.DefaultsRetirements.Where(b => b.clientID == clientID).FirstOrDefault();
                    DefaultsRetirement retP = db.DefaultsRetirements.Where(b => b.profileID == profileID).FirstOrDefault();
                    if (ret != null && ret.RetirementDefaultID > 0 && retP == null)
                    {
                        DefaultsRetirement retNew = new DefaultsRetirement();
                        retNew.profileID = profileID;
                        retNew.retireAge = ret.retireAge;
                        retNew.lifeExpectency = ret.lifeExpectency;
                        retNew.preRetReturnTax = ret.preRetReturnTax;
                        retNew.retTax = ret.retTax;
                        retNew.retReturnTax = ret.retReturnTax;
                        retNew.monthIncome = ret.monthIncome;
                        retNew.inflation = ret.inflation;
                        db.DefaultsRetirements.Add(retNew);
                        //db.SaveChanges();
                    }
                    DefaultIncomeProjection inc = db.DefaultIncomeProjections.Where(b => b.clientID == clientID).FirstOrDefault();
                    DefaultIncomeProjection incP = db.DefaultIncomeProjections.Where(b => b.profileID == profileID).FirstOrDefault();
                    if (inc != null && inc.defaultID > 0 && incP == null)
                    {
                        DefaultIncomeProjection incNew = new DefaultIncomeProjection();
                        incNew.profileID = profileID;
                        incNew.primaryInflation = inc.primaryInflation;
                        incNew.spouseInflation = inc.spouseInflation;
                        incNew.inflation = inc.inflation;
                        db.DefaultIncomeProjections.Add(incNew);
                        //db.SaveChanges();
                    }
                    DefaultsDebt deb = db.DefaultsDebts.Where(b => b.clientID == clientID).FirstOrDefault();
                    DefaultsDebt debP = db.DefaultsDebts.Where(b => b.profileID == profileID).FirstOrDefault();
                    if (deb != null && deb.defaultID > 0 && debP == null)
                    {
                        DefaultsDebt debNew = new DefaultsDebt();
                        debNew.profileID = profileID;
                        debNew.minPay = deb.minPay;
                        debNew.unpaidPercentage = deb.unpaidPercentage;
                        debNew.inflation = deb.inflation;
                        db.DefaultsDebts.Add(debNew);
                        //db.SaveChanges();
                    }
                    DefaultsEmergencyFund eme = db.DefaultsEmergencyFunds.Where(b => b.clientID == clientID).FirstOrDefault();
                    DefaultsEmergencyFund emeP = db.DefaultsEmergencyFunds.Where(b => b.profileID == profileID).FirstOrDefault();
                    if (eme != null && eme.defaultID > 0 && emeP == null)
                    {
                        DefaultsEmergencyFund emeNew = new DefaultsEmergencyFund();
                        emeNew.profileID = profileID;
                        emeNew.emergencyFundValue = eme.emergencyFundValue;
                        emeNew.rate1 = eme.rate1;
                        emeNew.rate2 = eme.rate2;
                        emeNew.rate3 = eme.rate3;
                        emeNew.inflation = eme.inflation;
                        emeNew.isPieChart = eme.isPieChart;
                        emeNew.isThreeMonth = eme.isThreeMonth;
                        db.DefaultsEmergencyFunds.Add(emeNew);
                        //db.SaveChanges();
                    }
                    DefaultsInsurance insu = db.DefaultsInsurances.Where(b => b.clientID == clientID).FirstOrDefault();
                    DefaultsInsurance insuP = db.DefaultsInsurances.Where(b => b.profileID == profileID).FirstOrDefault();
                    if (insu != null && insu.defaultID > 0 && insuP == null)
                    {
                        DefaultsInsurance insuNew = new DefaultsInsurance();
                        insuNew.profileID = profileID;
                        insuNew.cost = insu.cost;
                        insuNew.monthlyIncome = insu.monthlyIncome;
                        insuNew.yearAfterDeath = insu.yearAfterDeath;
                        insuNew.retAsset = insu.retAsset;
                        insuNew.otherRetAsset = insu.otherRetAsset;
                        insuNew.cashAccounts = insu.cashAccounts;
                        insuNew.spouseRetAsset = insu.spouseRetAsset;
                        insuNew.nonRetAsset = insu.nonRetAsset;
                        insuNew.inflation = insu.inflation;
                        insuNew.yearAfterDeath = insu.yearAfterDeath;
                        insuNew.specificnoofyears = insu.specificnoofyears ?? 20;
                        db.DefaultsInsurances.Add(insuNew);
                        //db.SaveChanges();
                    }
                    DefaultsEducation edu = db.DefaultsEducations.Where(b => b.clientID == clientID).FirstOrDefault();
                    DefaultsEducation eduP = db.DefaultsEducations.Where(b => b.profileID == profileID).FirstOrDefault();
                    if (edu != null && edu.defaultID > 0 && eduP == null)
                    {
                        DefaultsEducation eduNew = new DefaultsEducation();
                        eduNew.profileID = profileID;
                        eduNew.collegeStartMonth = edu.collegeStartMonth;
                        eduNew.tutionFeeInflation = edu.tutionFeeInflation;
                        eduNew.collegeAge = edu.collegeAge;
                        eduNew.collageYears = edu.collageYears;
                        eduNew.rate1 = edu.rate1;
                        eduNew.rate2 = edu.rate2;
                        eduNew.rate3 = edu.rate3;
                        eduNew.parentPay = edu.parentPay;
                        db.DefaultsEducations.Add(eduNew);
                        //db.SaveChanges();
                    }
                    db.SaveChanges();

                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #region Save/Update Defaults
        public long saveGlobalSettings(DefaultsGlobalModel data, long usrCatID, int usrCat)
        {
            long globalID = 0;
            try
            {
                using (var db = Connection.getConnect())
                {
                    DefaultsGlobal dg = db.DefaultsGlobals.Where(b => (b.advisorID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Advisor) ||
                                                                       (b.clientID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Client) ||
                                                                       (b.profileID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Profile)).FirstOrDefault();
                    if (dg != null && dg.defultID > 0)
                    {
                        dg.timeZone = data.timeZone;
                        dg.currency = data.currency;
                        dg.defaultstate = data.defaultstate;
                        db.Entry(dg).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        dg = new DefaultsGlobal();
                        switch (usrCat)
                        {
                            case (int)CustomEnum.userCatEnum.Advisor:
                                dg.advisorID = usrCatID;
                                break;
                            case (int)CustomEnum.userCatEnum.Client:
                                dg.clientID = usrCatID;
                                break;
                            case (int)CustomEnum.userCatEnum.Profile:
                                dg.profileID = usrCatID;
                                break;
                        }
                        dg.timeZone = data.timeZone;
                        dg.currency = data.currency;
                        dg.defaultstate = data.defaultstate;
                        db.DefaultsGlobals.Add(dg);
                    }
                    db.SaveChanges();
                    globalID = dg.defultID;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return globalID;
        }
        public long saveRetirementDefault(DefaultsRetirementModel data, long usrCatID, int usrCat)
        {
            long retID = 0;
            try
            {
                using (var db = Connection.getConnect())
                {
                    DefaultsRetirement dg = db.DefaultsRetirements.Where(b => (b.advisorID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Advisor) ||
                                                                       (b.clientID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Client) ||
                                                                       (b.profileID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Profile)).FirstOrDefault();
                    if (dg != null && dg.RetirementDefaultID > 0)
                    {
                        dg.retireAge = data.retireAge;
                        dg.lifeExpectency = data.lifeExpectency;
                        dg.preRetReturnTax = data.preRetReturnTax;
                        dg.retTax = data.retTax;
                        dg.retReturnTax = data.retReturnTax;
                        dg.monthIncome = data.monthIncome;
                        dg.inflation = data.inflation;
                        db.Entry(dg).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        dg = new DefaultsRetirement();
                        switch (usrCat)
                        {
                            case (int)CustomEnum.userCatEnum.Advisor:
                                dg.advisorID = usrCatID;
                                break;
                            case (int)CustomEnum.userCatEnum.Client:
                                dg.clientID = usrCatID;
                                break;
                            case (int)CustomEnum.userCatEnum.Profile:
                                dg.profileID = usrCatID;
                                break;
                        }
                        dg.retireAge = data.retireAge;
                        dg.lifeExpectency = data.lifeExpectency;
                        dg.preRetReturnTax = data.preRetReturnTax;
                        dg.retTax = data.retTax;
                        dg.retReturnTax = data.retReturnTax;
                        dg.monthIncome = data.monthIncome;
                        dg.inflation = data.inflation;
                        db.DefaultsRetirements.Add(dg);
                    }
                    db.SaveChanges();
                    retID = dg.RetirementDefaultID;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return retID;
        }
        public long saveIncomeProjectionDefault(DefaultIncomeProjectionModel data, long usrCatID, int usrCat)
        {
            long inProjID = 0;
            try
            {
                using (var db = Connection.getConnect())
                {
                    DefaultIncomeProjection dg = db.DefaultIncomeProjections.Where(b => (b.advisorID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Advisor) ||
                                                                       (b.clientID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Client) ||
                                                                       (b.profileID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Profile)).FirstOrDefault();
                    if (dg != null && dg.defaultID > 0)
                    {
                        dg.primaryInflation = data.primaryInflation;
                        dg.spouseInflation = data.spouseInflation;
                        dg.inflation = data.inflation;
                        db.Entry(dg).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        dg = new DefaultIncomeProjection();
                        switch (usrCat)
                        {
                            case (int)CustomEnum.userCatEnum.Advisor:
                                dg.advisorID = usrCatID;
                                break;
                            case (int)CustomEnum.userCatEnum.Client:
                                dg.clientID = usrCatID;
                                break;
                            case (int)CustomEnum.userCatEnum.Profile:
                                dg.profileID = usrCatID;
                                break;
                        }
                        dg.primaryInflation = data.primaryInflation;
                        dg.spouseInflation = data.spouseInflation;
                        dg.inflation = data.inflation;
                        db.DefaultIncomeProjections.Add(dg);
                    }
                    db.SaveChanges();
                    inProjID = dg.defaultID;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return inProjID;
        }
        public long saveDebtDefault(DefaultsDebtModel data, long usrCatID, int usrCat)
        {
            long debtID = 0;
            try
            {
                using (var db = Connection.getConnect())
                {
                    DefaultsDebt dg = db.DefaultsDebts.Where(b => (b.advisorID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Advisor) ||
                                                                       (b.clientID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Client) ||
                                                                       (b.profileID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Profile)).FirstOrDefault();
                    if (dg != null && dg.defaultID > 0)
                    {
                        dg.minPay = data.minPay;
                        dg.unpaidPercentage = data.unpaidPercentage;
                        dg.inflation = data.inflation;
                        db.Entry(dg).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        dg = new DefaultsDebt();
                        switch (usrCat)
                        {
                            case (int)CustomEnum.userCatEnum.Advisor:
                                dg.advisorID = usrCatID;
                                break;
                            case (int)CustomEnum.userCatEnum.Client:
                                dg.clientID = usrCatID;
                                break;
                            case (int)CustomEnum.userCatEnum.Profile:
                                dg.profileID = usrCatID;
                                break;
                        }
                        dg.minPay = data.minPay;
                        dg.unpaidPercentage = data.unpaidPercentage;
                        dg.inflation = data.inflation;
                        db.DefaultsDebts.Add(dg);
                    }
                    db.SaveChanges();
                    debtID = dg.defaultID;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return debtID;
        }
        public long saveEmergencyFundDefault(DefaultsEmergencyFundModel data, long usrCatID, int usrCat)
        {
            long emerID = 0;
            try
            {
                using (var db = Connection.getConnect())
                {
                    DefaultsEmergencyFund dg = db.DefaultsEmergencyFunds.Where(b => (b.advisorID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Advisor) ||
                                                                       (b.clientID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Client) ||
                                                                       (b.profileID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Profile)).FirstOrDefault();
                    if (dg != null && dg.defaultID > 0)
                    {
                        //dg.graphID = data.graphID;
                        // dg.emergencyFundID = data.emergencyFundID;
                        dg.emergencyFundValue = data.emergencyFundValue;
                        dg.rate1 = data.rateA;
                        dg.rate2 = data.rateB;
                        dg.rate3 = data.rateC;
                        dg.inflation = data.inflation;
                        if (Convert.ToBoolean(data.isPieChart))
                        {
                            dg.isPieChart = data.isPieChart;
                        }
                        else
                        {
                            dg.isPieChart = false;
                        }
                        if (Convert.ToBoolean(data.isThreeMonth))
                        {
                            dg.isThreeMonth = data.isThreeMonth;
                        }
                        else
                        {
                            dg.isThreeMonth = false;
                        }
                        //dg.isPieChart = data.isPieChart;
                        //dg.isThreeMonth = data.isThreeMonth;
                        db.Entry(dg).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        dg = new DefaultsEmergencyFund();
                        switch (usrCat)
                        {
                            case (int)CustomEnum.userCatEnum.Advisor:
                                dg.advisorID = usrCatID;
                                break;
                            case (int)CustomEnum.userCatEnum.Client:
                                dg.clientID = usrCatID;
                                break;
                            case (int)CustomEnum.userCatEnum.Profile:
                                dg.profileID = usrCatID;
                                break;
                        }
                        //dg.graphID = data.graphID;
                        dg.emergencyFundValue = data.emergencyFundValue;
                        // dg.emergencyFundID = data.emergencyFundID;
                        dg.rate1 = data.rateA;
                        dg.rate2 = data.rateB;
                        dg.rate3 = data.rateC;
                        dg.inflation = data.inflation;
                        if (Convert.ToBoolean(data.isPieChart))
                        {
                            dg.isPieChart = data.isPieChart;
                        }
                        else
                        {
                            dg.isPieChart = false;
                        }
                        if (Convert.ToBoolean(data.isThreeMonth))
                        {
                            dg.isThreeMonth = data.isThreeMonth;
                        }
                        else
                        {
                            dg.isThreeMonth = false;
                        }
                        //dg.isPieChart = data.isPieChart;
                        //dg.isThreeMonth = data.isThreeMonth;
                        db.DefaultsEmergencyFunds.Add(dg);
                    }
                    db.SaveChanges();
                    emerID = dg.defaultID;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return emerID;
        }
        public long saveInsuranceDefault(DefaultsInsuranceModel data, long usrCatID, int usrCat)
        {
            long insuID = 0;
            try
            {
                using (var db = Connection.getConnect())
                {
                    DefaultsInsurance dg = db.DefaultsInsurances.Where(b => (b.advisorID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Advisor) ||
                                                                       (b.clientID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Client) ||
                                                                       (b.profileID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Profile)).FirstOrDefault();
                    if (dg != null && dg.defaultID > 0)
                    {
                        dg.cost = data.cost;
                        dg.monthlyIncome = data.monthlyIncome;
                        dg.yearAfterDeath = data.yearAfterDeath;
                        dg.retAsset = data.retAsset;
                        dg.otherRetAsset = data.otherRetAsset;
                        dg.cashAccounts = data.cashAccounts;
                        dg.spouseRetAsset = data.spouseRetAsset;
                        dg.nonRetAsset = data.nonRetAsset;
                        dg.inflation = data.inflation;
                        dg.yearAfterDeath = data.yearAfterDeath;
                        dg.specificnoofyears = data.specificnoofyears ?? 20;
                        db.Entry(dg).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        dg = new DefaultsInsurance();
                        switch (usrCat)
                        {
                            case (int)CustomEnum.userCatEnum.Advisor:
                                dg.advisorID = usrCatID;
                                break;
                            case (int)CustomEnum.userCatEnum.Client:
                                dg.clientID = usrCatID;
                                break;
                            case (int)CustomEnum.userCatEnum.Profile:
                                dg.profileID = usrCatID;
                                break;
                        }
                        dg.cost = data.cost;
                        dg.monthlyIncome = data.monthlyIncome;
                        dg.yearAfterDeath = data.yearAfterDeath;
                        dg.retAsset = data.retAsset;
                        dg.inflation = data.inflation;
                        dg.otherRetAsset = data.otherRetAsset;
                        dg.cashAccounts = data.cashAccounts;
                        dg.spouseRetAsset = data.spouseRetAsset;
                        dg.nonRetAsset = data.nonRetAsset;
                        dg.yearAfterDeath = data.yearAfterDeath;
                        dg.specificnoofyears = data.specificnoofyears ?? 20;
                        db.DefaultsInsurances.Add(dg);
                    }
                    db.SaveChanges();
                    insuID = dg.defaultID;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return insuID;
        }
        public long saveEducationDefault(DefaultsEducationModel data, long usrCatID, int usrCat)
        {
            long eduID = 0;
            try
            {
                using (var db = Connection.getConnect())
                {
                    DefaultsEducation dg = db.DefaultsEducations.Where(b => (b.advisorID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Advisor) ||
                                                                       (b.clientID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Client) ||
                                                                       (b.profileID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Profile)).FirstOrDefault();
                    if (dg != null && dg.defaultID > 0)
                    {
                        dg.collegeStartMonth = data.collegeStartMonth;
                        dg.tutionFeeInflation = data.tutionFeeInflation;
                        dg.collegeAge = data.collegeAge;
                        dg.collageYears = data.collageYears;
                        dg.rate1 = data.rateA;
                        dg.rate2 = data.rateB;
                        dg.rate3 = data.rateC;
                        dg.parentPay = data.parentPay;
                        db.Entry(dg).State = System.Data.Entity.EntityState.Modified;
                    }
                    else
                    {
                        dg = new DefaultsEducation();
                        switch (usrCat)
                        {
                            case (int)CustomEnum.userCatEnum.Advisor:
                                dg.advisorID = usrCatID;
                                break;
                            case (int)CustomEnum.userCatEnum.Client:
                                dg.clientID = usrCatID;
                                break;
                            case (int)CustomEnum.userCatEnum.Profile:
                                dg.profileID = usrCatID;
                                break;
                        }
                        dg.clientID = data.clientID;
                        dg.collegeStartMonth = data.collegeStartMonth;
                        dg.tutionFeeInflation = data.tutionFeeInflation;
                        dg.collegeAge = data.collegeAge;
                        dg.rate1 = data.rateA;
                        dg.rate2 = data.rateB;
                        dg.rate3 = data.rateC;
                        dg.collageYears = data.collageYears;
                        dg.parentPay = data.parentPay;
                        db.DefaultsEducations.Add(dg);
                    }
                    db.SaveChanges();
                    eduID = dg.defaultID;
                    List<FavCollegesModel> lis = new List<FavCollegesModel>();
                    lis = saveDefaultEducationFavCollege(data.FavColleges);
                    data.FavColleges = lis;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return eduID;
        }
        #endregion

        #region Get Defaults
        public DefaultsGlobalModel getGlobalDefault(long usrCatID, int usrCat)
        {
            DefaultsGlobal data = new DefaultsGlobal();
            DefaultsGlobalModel model = new DefaultsGlobalModel();
            try
            {
                long clientID = 0, advisorID = 0;
                using (var db = Connection.getConnect())
                {
                    if (usrCat == (int)CustomEnum.userCatEnum.Profile)
                    {
                        clientID = db.profiles.SingleOrDefault(c => c.profileID == usrCatID).clientID ?? 0;
                        advisorID = db.Clients.SingleOrDefault(c => c.clientId == clientID).advisorId ?? 0;
                    }
                    else if (usrCat == (int)CustomEnum.userCatEnum.Client)
                    {
                        advisorID = db.Clients.SingleOrDefault(c => c.clientId == usrCatID).advisorId ?? 0;
                    }
                    data = db.DefaultsGlobals.Where(b => (b.advisorID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Advisor) ||
                                                                         ((b.clientID == usrCatID || (b.advisorID == advisorID && b.clientID != usrCatID)) && usrCat == (int)CustomEnum.userCatEnum.Client) ||
                                                                         (((b.profileID == usrCatID) || (b.clientID == clientID && b.profileID != usrCatID) || (b.advisorID == advisorID && b.clientID != clientID && b.profileID != usrCatID)) && usrCat == (int)CustomEnum.userCatEnum.Profile)
                                                                         )
                                                                         .FirstOrDefault();
                    if (data != null && data.defultID > 0)
                    {
                        model = fillGlobal(data);
                        return model;
                    }
                    else
                    {
                        if (usrCat == (int)CustomEnum.userCatEnum.Advisor)
                            model.advisorID = usrCatID;
                        else if (usrCat == (int)CustomEnum.userCatEnum.Client)
                            model.clientID = usrCatID;
                        else if (usrCat == (int)CustomEnum.userCatEnum.Profile)
                            model.profileID = usrCatID;
                        model.timeZone = 12;
                        model.currency = 1;
                        model.defultID = 0;
                        model.timeZone = 5;
                        model.currency = 1;
                        model.defaultstate = "CA";
                        return model;
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public DefaultsRetirementModel getRetirementDefault(long usrCatID, int usrCat)
        {
            DefaultsRetirement data = new DefaultsRetirement();
            DefaultsRetirementModel model = new DefaultsRetirementModel();
            try
            {
                long clientID = 0, advisorID = 0;
                using (var db = Connection.getConnect())
                {
                    if (usrCat == (int)CustomEnum.userCatEnum.Profile)
                    {
                        clientID = db.profiles.SingleOrDefault(c => c.profileID == usrCatID).clientID ?? 0;
                        advisorID = db.Clients.SingleOrDefault(c => c.clientId == clientID).advisorId ?? 0;
                    }
                    else if (usrCat == (int)CustomEnum.userCatEnum.Client)
                    {
                        advisorID = db.Clients.SingleOrDefault(c => c.clientId == usrCatID).advisorId ?? 0;
                    }
                    data = db.DefaultsRetirements.Where(b => (b.advisorID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Advisor) ||
                                                                         ((b.clientID == usrCatID || (b.advisorID == advisorID && b.clientID != usrCatID)) && usrCat == (int)CustomEnum.userCatEnum.Client) ||
                                                                         (((b.profileID == usrCatID) || (b.clientID == clientID && b.profileID != usrCatID) || (b.advisorID == advisorID && b.clientID != clientID && b.profileID != usrCatID)) && usrCat == (int)CustomEnum.userCatEnum.Profile)
                                                                         )
                                                                         .FirstOrDefault();


                    if (data != null && data.RetirementDefaultID > 0)
                    {
                        model = fillRetirement(data);
                        return model;
                    }
                    else
                    {
                        if (usrCat == (int)CustomEnum.userCatEnum.Advisor)
                            model.advisorID = usrCatID;
                        else if (usrCat == (int)CustomEnum.userCatEnum.Client)
                            model.clientID = usrCatID;
                        else if (usrCat == (int)CustomEnum.userCatEnum.Profile)
                            model.profileID = usrCatID;
                        model.retireAge = 60;
                        model.lifeExpectency = 90;
                        model.preRetReturnTax = (decimal)9;
                        model.retTax = (decimal)15;
                        model.monthIncome = (decimal)5000;
                        model.retReturnTax = (decimal)6;
                        model.inflation = (decimal)3;
                        model.RetirementDefaultID = 0;
                        return model;
                        //}
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public DefaultIncomeProjectionModel getIncomeProjectionDefault(long usrCatID, int usrCat)
        {
            DefaultIncomeProjection data = new DefaultIncomeProjection();
            DefaultIncomeProjectionModel model = new DefaultIncomeProjectionModel();
            try
            {
                long clientID = 0, advisorID = 0;
                using (var db = Connection.getConnect())
                {
                    if (usrCat == (int)CustomEnum.userCatEnum.Profile)
                    {
                        clientID = db.profiles.SingleOrDefault(c => c.profileID == usrCatID).clientID ?? 0;
                        advisorID = db.Clients.SingleOrDefault(c => c.clientId == clientID).advisorId ?? 0;
                    }
                    else if (usrCat == (int)CustomEnum.userCatEnum.Client)
                    {
                        advisorID = db.Clients.SingleOrDefault(c => c.clientId == usrCatID).advisorId ?? 0;
                    }
                    data = db.DefaultIncomeProjections.Where(b => (b.advisorID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Advisor) ||
                                                                         ((b.clientID == usrCatID || (b.advisorID == advisorID && b.clientID != usrCatID)) && usrCat == (int)CustomEnum.userCatEnum.Client) ||
                                                                         (((b.profileID == usrCatID) || (b.clientID == clientID && b.profileID != usrCatID) || (b.advisorID == advisorID && b.clientID != clientID && b.profileID != usrCatID)) && usrCat == (int)CustomEnum.userCatEnum.Profile)
                                                                         )
                                                                         .FirstOrDefault();
                    if (data != null && data.defaultID > 0)
                    {
                        model = fillIncome(data);
                        return model;
                    }
                    else
                    {
                        if (usrCat == (int)CustomEnum.userCatEnum.Advisor)
                            model.advisorID = usrCatID;
                        else if (usrCat == (int)CustomEnum.userCatEnum.Client)
                            model.clientID = usrCatID;
                        else if (usrCat == (int)CustomEnum.userCatEnum.Profile)
                            model.profileID = usrCatID;
                        model.spouseInflation = (decimal)3;
                        model.primaryInflation = (decimal)3;
                        model.inflation = (decimal)3;
                        model.defaultID = 0;
                        return model;
                        //}

                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public DefaultsDebtModel getDebtDefault(long usrCatID, int usrCat)
        {
            DefaultsDebt data = new DefaultsDebt();
            DefaultsDebtModel model = new DefaultsDebtModel();
            try
            {
                long clientID = 0, advisorID = 0;
                using (var db = Connection.getConnect())
                {
                    if (usrCat == (int)CustomEnum.userCatEnum.Profile)
                    {
                        clientID = db.profiles.SingleOrDefault(c => c.profileID == usrCatID).clientID ?? 0;
                        advisorID = db.Clients.SingleOrDefault(c => c.clientId == clientID).advisorId ?? 0;
                    }
                    else if (usrCat == (int)CustomEnum.userCatEnum.Client)
                    {
                        advisorID = db.Clients.SingleOrDefault(c => c.clientId == usrCatID).advisorId ?? 0;
                    }
                    data = db.DefaultsDebts.Where(b => (b.advisorID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Advisor) ||
                                                                         ((b.clientID == usrCatID || (b.advisorID == advisorID && b.clientID != usrCatID)) && usrCat == (int)CustomEnum.userCatEnum.Client) ||
                                                                         (((b.profileID == usrCatID) || (b.clientID == clientID && b.profileID != usrCatID) || (b.advisorID == advisorID && b.clientID != clientID && b.profileID != usrCatID)) && usrCat == (int)CustomEnum.userCatEnum.Profile)
                                                                         )
                                                                         .FirstOrDefault();
                    if (data != null && data.defaultID > 0)
                    {
                        model = fillDebt(data);
                        return model;
                    }
                    else
                    {
                        if (usrCat == (int)CustomEnum.userCatEnum.Advisor)
                            model.advisorID = usrCatID;
                        else if (usrCat == (int)CustomEnum.userCatEnum.Client)
                            model.clientID = usrCatID;
                        else if (usrCat == (int)CustomEnum.userCatEnum.Profile)
                            model.profileID = usrCatID;
                        model.minPay = (decimal)15;
                        model.inflation = (decimal)3;
                        model.unpaidPercentage = (decimal)2;
                        model.defaultID = 0;
                        return model;
                        //}
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public DefaultsEmergencyFundModel getEmergencyFundDefault(long usrCatID, int usrCat)
        {
            DefaultsEmergencyFund data = new DefaultsEmergencyFund();
            DefaultsEmergencyFundModel model = new DefaultsEmergencyFundModel();
            try
            {
                long clientID = 0, advisorID = 0;
                using (var db = Connection.getConnect())
                {
                    if (usrCat == (int)CustomEnum.userCatEnum.Profile)
                    {
                        clientID = db.profiles.SingleOrDefault(c => c.profileID == usrCatID).clientID ?? 0;
                        advisorID = db.Clients.SingleOrDefault(c => c.clientId == clientID).advisorId ?? 0;
                    }
                    else if (usrCat == (int)CustomEnum.userCatEnum.Client)
                    {
                        advisorID = db.Clients.SingleOrDefault(c => c.clientId == usrCatID).advisorId ?? 0;
                    }
                    data = db.DefaultsEmergencyFunds.Where(b => (b.advisorID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Advisor) ||
                                                                         ((b.clientID == usrCatID || (b.advisorID == advisorID && b.clientID != usrCatID)) && usrCat == (int)CustomEnum.userCatEnum.Client) ||
                                                                         (((b.profileID == usrCatID) || (b.clientID == clientID && b.profileID != usrCatID) || (b.advisorID == advisorID && b.clientID != clientID && b.profileID != usrCatID)) && usrCat == (int)CustomEnum.userCatEnum.Profile)
                                                                         )
                                                                         .FirstOrDefault();

                    if (data != null && data.defaultID > 0)
                    {
                        model = fillEmergency(data);
                        return model;
                    }
                    else
                    {
                        // data = new DefaultsEmergencyFund();

                        if (usrCat == (int)CustomEnum.userCatEnum.Advisor)
                            model.advisorID = usrCatID;
                        else if (usrCat == (int)CustomEnum.userCatEnum.Client)
                            model.clientID = usrCatID;
                        else if (usrCat == (int)CustomEnum.userCatEnum.Profile)
                            model.profileID = usrCatID;
                        model.isPieChart = true;
                        model.isThreeMonth = true;
                        model.isBarChart = false;
                        model.isSixMonth = false;
                        model.emergencyFundValue = 20000;
                        model.rateA = (decimal)3;
                        model.rateB = (decimal)6;
                        model.rateC = (decimal)9;
                        model.inflation = (decimal)3;
                        model.defaultID = 0;
                        //model = fillEmergency(data);
                        return model;
                        //}
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public DefaultsInsuranceModel getInsuranceDefault(long usrCatID, int usrCat)
        {
            DefaultsInsurance data = new DefaultsInsurance();
            DefaultsInsuranceModel model = new DefaultsInsuranceModel();
            try
            {
                long clientID = 0, advisorID = 0;
                using (var db = Connection.getConnect())
                {
                    if (usrCat == (int)CustomEnum.userCatEnum.Profile)
                    {
                        clientID = db.profiles.SingleOrDefault(c => c.profileID == usrCatID).clientID ?? 0;
                        advisorID = db.Clients.SingleOrDefault(c => c.clientId == clientID).advisorId ?? 0;
                    }
                    else if (usrCat == (int)CustomEnum.userCatEnum.Client)
                    {
                        advisorID = db.Clients.SingleOrDefault(c => c.clientId == usrCatID).advisorId ?? 0;
                    }
                    data = db.DefaultsInsurances.Where(b => (b.advisorID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Advisor) ||
                                                                         ((b.clientID == usrCatID || (b.advisorID == advisorID && b.clientID != usrCatID)) && usrCat == (int)CustomEnum.userCatEnum.Client) ||
                                                                         (((b.profileID == usrCatID) || (b.clientID == clientID && b.profileID != usrCatID) || (b.advisorID == advisorID && b.clientID != clientID && b.profileID != usrCatID)) && usrCat == (int)CustomEnum.userCatEnum.Profile)
                                                                         )
                                                                         .FirstOrDefault();

                    if (data != null && data.defaultID > 0)
                    {
                        model = fillInsurance(data);
                        return model;
                    }
                    else
                    {
                        if (usrCat == (int)CustomEnum.userCatEnum.Advisor)
                            model.advisorID = usrCatID;
                        else if (usrCat == (int)CustomEnum.userCatEnum.Client)
                            model.clientID = usrCatID;
                        else if (usrCat == (int)CustomEnum.userCatEnum.Profile)
                            model.profileID = usrCatID;
                        model.cost = (decimal)10000;
                        model.monthlyIncome = (decimal)2000;
                        model.retAsset = true;
                        model.otherRetAsset = true;
                        model.cashAccounts = true;
                        model.spouseRetAsset = true;
                        model.nonRetAsset = true;
                        model.inflation = (decimal)3;
                        model.yearAfterDeath = (int)CustomEnum.incomeAfterDeathEnum.numberOfYears;
                        model.specificnoofyears = 20;
                        model.defaultID = 0;
                        return model;
                        //}

                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public DefaultsEducationModel getEducationDefault(long usrCatID, int usrCat)
        {
            DefaultsEducation data = new DefaultsEducation();
            DefaultsEducationModel model = new DefaultsEducationModel();
            try
            {
                long clientID = 0, advisorID = 0;
                using (var db = Connection.getConnect())
                {
                    if (usrCat == (int)CustomEnum.userCatEnum.Profile)
                    {
                        clientID = db.profiles.SingleOrDefault(c => c.profileID == usrCatID).clientID ?? 0;
                        advisorID = db.Clients.SingleOrDefault(c => c.clientId == clientID).advisorId ?? 0;
                    }
                    else if (usrCat == (int)CustomEnum.userCatEnum.Client)
                    {
                        advisorID = db.Clients.SingleOrDefault(c => c.clientId == usrCatID).advisorId ?? 0;
                    }
                    data = db.DefaultsEducations.Where(b => (b.advisorID == usrCatID && usrCat == (int)CustomEnum.userCatEnum.Advisor) ||
                                                                         ((b.clientID == usrCatID || (b.advisorID == advisorID && b.clientID != usrCatID)) && usrCat == (int)CustomEnum.userCatEnum.Client) ||
                                                                         (((b.profileID == usrCatID) || (b.clientID == clientID && b.profileID != usrCatID) || (b.advisorID == advisorID && b.clientID != clientID && b.profileID != usrCatID)) && usrCat == (int)CustomEnum.userCatEnum.Profile)
                                                                         )
                                                                         .FirstOrDefault();

                    if (data != null && data.defaultID > 0)
                    {
                        model = fillEducation(data);
                       // model.FavColleges = getDefaultEducationFavCollegeList(data.defaultID);
                        return model;
                    }
                    else
                    {
                        if (usrCat == (int)CustomEnum.userCatEnum.Advisor)
                            model.advisorID = usrCatID;
                        else if (usrCat == (int)CustomEnum.userCatEnum.Client)
                            model.clientID = usrCatID;
                        else if (usrCat == (int)CustomEnum.userCatEnum.Profile)
                            model.profileID = usrCatID;
                        model.collegeStartMonth = 9;
                        model.tutionFeeInflation = (decimal)7;
                        model.collegeAge = 18;
                        model.collageYears = 4;
                        model.rateA = (decimal)3;
                        model.rateB = (decimal)6;
                        model.rateC = (decimal)10;
                        model.parentPay = (decimal)100;
                        model.defaultID = 0;
                        return model;
                        //}
                    }
                    
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }
        public List<FavCollegesModel> getDefaultEducationFavCollegeList(long defaultEduID)//Abhay - 13 Nov 2017
        {
            List<FavCollegesModel> data = new List<FavCollegesModel>();
            try
            {
                using (var db = Connection.getConnect())
                {
                    var query = "EXEC dbo.getEducationDefaultFavColleges @defaultEduID";
                    var parameters = new[]
                    {
                        new SqlParameter("defaultEduID", defaultEduID)
                    };
                    using (var multiResultSet = DbContextExtensions.MultiResultSetSqlQuery(db, query, parameters))
                    {
                        data = multiResultSet.ResultSetFor<FavCollegesModel>().ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return data;
        }
        public List<FavCollegesModel> saveDefaultEducationFavCollege(List<FavCollegesModel> model) //Abhay - 13 Nov 2017
        {
            List<FavCollegesModel> data = new List<FavCollegesModel>();
            try
            {
                using (var db = Connection.getConnect())
                {
                    if (model != null && model.Count() > 0)
                    {
                        foreach (var itm in model.Where(b => b.isFav != null).ToList())
                        {
                            if (itm.defaultEduID > 0)
                            {
                                DefaultCollege dd = db.DefaultColleges.Where(b => b.defaultEduID == itm.defaultEduID && b.collegeID == itm.collegeID).FirstOrDefault();
                                if (dd != null && dd.id > 0)
                                {
                                    if (itm.isFav == false)
                                    {
                                        db.DefaultColleges.Remove(dd);
                                        db.SaveChanges();
                                    }
                                }
                                else
                                {
                                    if (itm.isFav == true)
                                    {
                                        dd = new DefaultCollege();
                                        dd.defaultEduID = itm.defaultEduID;
                                        dd.collegeID = itm.collegeID;
                                        db.DefaultColleges.Add(dd);
                                        db.SaveChanges();
                                    }
                                }
                            }
                        }
                       // data = getDefaultEducationFavCollegeList(model[0].defaultEduID);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return data;
        }
        #endregion

        #region Fill Defaults
        public DefaultsGlobalModel fillGlobal(DefaultsGlobal dg)
        {
            DefaultsGlobalModel obj = new DefaultsGlobalModel();

            try
            {

                obj.profileID = dg.profileID;
                obj.clientID = dg.clientID;
                obj.advisorID = dg.advisorID;
                obj.defultID = dg.defultID;
                obj.timeZone = dg.timeZone;
                obj.currency = dg.currency;
                obj.defaultstate = dg.defaultstate;
            }
            catch (Exception ex)
            {
                throw;
            }
            return obj;
        }
        public DefaultsRetirementModel fillRetirement(DefaultsRetirement ob)
        {

            DefaultsRetirementModel obj = new DefaultsRetirementModel();
            try
            {
                obj.profileID = ob.profileID;
                obj.clientID = ob.clientID;
                obj.advisorID = ob.advisorID;
                obj.inflation = ob.inflation;
                obj.RetirementDefaultID = ob.RetirementDefaultID;
                obj.retireAge = ob.retireAge;
                obj.lifeExpectency = ob.lifeExpectency;
                obj.preRetReturnTax = ob.preRetReturnTax;
                obj.retTax = ob.retTax;
                obj.monthIncome = ob.monthIncome;
                obj.retReturnTax = ob.retReturnTax;

            }
            catch (Exception ex)
            {
                throw;
            }
            return obj;
        }
        public DefaultIncomeProjectionModel fillIncome(DefaultIncomeProjection ob)
        {

            DefaultIncomeProjectionModel obj = new DefaultIncomeProjectionModel();
            try
            {
                obj.profileID = ob.profileID;
                obj.clientID = ob.clientID;
                obj.advisorID = ob.advisorID;
                obj.defaultID = ob.defaultID;
                obj.inflation = ob.inflation;
                obj.primaryInflation = ob.primaryInflation;
                obj.spouseInflation = ob.spouseInflation;
            }
            catch (Exception ex)
            {
                throw;
            }
            return obj;
        }
        public DefaultsDebtModel fillDebt(DefaultsDebt ob)
        {

            DefaultsDebtModel obj = new DefaultsDebtModel();
            try
            {
                obj.profileID = ob.profileID;
                obj.clientID = ob.clientID;
                obj.advisorID = ob.advisorID;
                obj.defaultID = ob.defaultID;
                obj.inflation = ob.inflation;
                obj.minPay = ob.minPay;
                obj.unpaidPercentage = ob.unpaidPercentage;
            }
            catch (Exception ex)
            {
                throw;
            }
            return obj;
        }
        public DefaultsEmergencyFundModel fillEmergency(DefaultsEmergencyFund ob)
        {

            DefaultsEmergencyFundModel obj = new DefaultsEmergencyFundModel();
            try
            {
                obj.profileID = ob.profileID;
                obj.clientID = ob.clientID;
                obj.advisorID = ob.advisorID;
                obj.defaultID = ob.defaultID;
                obj.rateA = ob.rate1;
                obj.rateB = ob.rate2;
                obj.rateC = ob.rate3;
                obj.inflation = ob.inflation;
                obj.emergencyFundValue = ob.emergencyFundValue;
                if (Convert.ToBoolean(ob.isPieChart))
                {
                    obj.isPieChart = ob.isPieChart;
                    obj.isBarChart = false;
                }
                else
                {
                    obj.isBarChart = true;
                    obj.isPieChart = false;
                }
                if (Convert.ToBoolean(ob.isThreeMonth))
                {
                    obj.isThreeMonth = ob.isThreeMonth;
                    obj.isSixMonth = false;
                }
                else
                {
                    obj.isSixMonth = true;
                    obj.isThreeMonth = false;
                }
            }

            catch (Exception ex)
            {
                throw;
            }
            return obj;
        }
        public DefaultsInsuranceModel fillInsurance(DefaultsInsurance ob)
        {
            DefaultsInsuranceModel obj = new DefaultsInsuranceModel();
            try
            {
                obj.profileID = ob.profileID;
                obj.clientID = ob.clientID;
                obj.advisorID = ob.advisorID;
                obj.defaultID = ob.defaultID;
                obj.cost = ob.cost;
                obj.monthlyIncome = ob.monthlyIncome;
                obj.nonRetAsset = ob.nonRetAsset;
                obj.otherRetAsset = ob.otherRetAsset;
                obj.retAsset = ob.retAsset;
                obj.spouseRetAsset = ob.spouseRetAsset;
                obj.yearAfterDeath = ob.yearAfterDeath;
                obj.cashAccounts = ob.cashAccounts;
                obj.yearAfterDeath = ob.yearAfterDeath;
                obj.specificnoofyears = ob.specificnoofyears;
                obj.inflation = ob.inflation;
            }
            catch (Exception ex)
            {
                throw;
            }
            return obj;
        }
        public DefaultsEducationModel fillEducation(DefaultsEducation ob)
        {
            DefaultsEducationModel obj = new DefaultsEducationModel();
            try
            {
                obj.profileID = ob.profileID;
                obj.clientID = ob.clientID;
                obj.advisorID = ob.advisorID;
                obj.defaultID = ob.defaultID;
                obj.collageYears = ob.collageYears;
                obj.collegeAge = ob.collegeAge;
                obj.collegeStartMonth = ob.collegeStartMonth;
                obj.parentPay = ob.parentPay;
                obj.tutionFeeInflation = ob.tutionFeeInflation;
                obj.rateA = ob.rate1;
                obj.rateB = ob.rate2;
                obj.rateC = ob.rate3;

            }
            catch (Exception ex)
            {
                throw;
            }
            return obj;
        }
        #endregion

        #region Dropdown Fill
        public List<DesclaimerModel> filldesclaimer(List<Desclaimer> ddlcurrency)
        {
            List<DesclaimerModel> cm = new List<DesclaimerModel>();
            try
            {
                foreach (Desclaimer a in ddlcurrency)
                {
                    DesclaimerModel m = new DesclaimerModel();
                    m.desclaimerID = a.desclaimerID;
                    m.desclaimerName = a.desclaimerName;
                    m.desclaimerBody = a.desclaimerBody;
                    cm.Add(m);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return cm;
        }
        public List<CurrencyModel> fillcurrency(List<Currency> ddlcurrency)
        {
            List<CurrencyModel> cm = new List<CurrencyModel>();
            try
            {
                foreach (Currency a in ddlcurrency)
                {
                    CurrencyModel m = new CurrencyModel();
                    m.cID = a.cID;
                    m.id = a.id;
                    m.name = a.name;
                    cm.Add(m);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return cm;
        }
        public List<GlobalTimeZoneModel> filltimezone(List<GlobalTimeZone> ddlcurrency)
        {
            List<GlobalTimeZoneModel> tz = new List<GlobalTimeZoneModel>();
            try
            {
                foreach (GlobalTimeZone a in ddlcurrency)
                {
                    GlobalTimeZoneModel m = new GlobalTimeZoneModel();
                    m.timeZoneID = a.timeZoneID;
                    m.location = a.location;
                    m.shortlocation = a.shortlocation;
                    m.time = a.time;
                    m.type = a.type;
                    m.text = m.location + " " + m.type + " " + m.time + " " + m.shortlocation;
                    tz.Add(m);

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return tz;
        }
        public List<states> fillstates(List<State> obj)
        {
            List<states> tz = new List<states>();
            try
            {
                foreach (State a in obj)
                {
                    states m = new states();
                    m.stateName = a.stateName;
                    m.stateID = a.stateID;
                    m.stateCode = a.stateCode;
                    tz.Add(m);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return tz;
        }
        #endregion

    }
}

