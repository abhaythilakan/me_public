﻿using CLayer;
using System;
using CLayer.common;
using CLayer.DBModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Globalization;

namespace DLayer
{
    public class ClientDashBoard
    {
        public Income_ExpenseModel getIncomeVsExpense(long profileId)
        {
            Income_ExpenseModel model = new Income_ExpenseModel();
            List<IncomeList> incomelist = new List<IncomeList>();
            List<Income> income = new List<Income>();
            List<Person> personList = new List<Person>();

            try
            {
                model.incomeList = new List<IncomeList>();
                model.expenseList = new List<expenseList>();
                model.totalIncome = 0;
                model.totalExpense = 0;
                using (var db = Connection.getConnect())
                {
                    personList = db.Persons.Where(x => x.profileID == profileId).ToList();
                    income = db.Incomes.Where(x => x.profileID == profileId && x.isActive == true).ToList();
                    var primaryIncome = income.Where(x => x.isFutureIncome == false).GroupBy(g => g.personID, (id, grp) =>
                                           new IncomeList
                                           {
                                               incomeName = personList.Where(x => x.personID == id).FirstOrDefault().firstName + "'s Employment Income",
                                               grossAmount = grp.FirstOrDefault().grossMonthlyIncome
                                           }).ToList();
                    var otherIncome = income.Where(x => x.isFutureIncome == true).GroupBy(g => g.personID, (id, grp) =>
                       new IncomeList
                       {
                           incomeName = "Other Current Income",
                           grossAmount = grp.FirstOrDefault().grossMonthlyIncome
                       }).ToList();

                    if (personList.Count > 0)
                    {
                        model.incomeList = primaryIncome.Concat(otherIncome).ToList();
                    }
                    model.totalIncome = model.incomeList.Sum(x => x.grossAmount ?? 0);


                    var debtDetails = db.Debts.Where(x => x.profileID == profileId && x.isActive == true).ToList();

                    if (debtDetails.Count > 0)
                    {
                        var totalDebt = debtDetails.Sum(x => x.amount ?? 0);
                        model.expenseList.Add(new expenseList { expenseName = "Total Debt Payments", amount = totalDebt });
                        model.totalExpense = model.totalExpense + totalDebt;
                    }
                    decimal totalLifeIns = 0;
                    decimal totalOtherIns = 0;

                    var insurance = db.lifeInsurances.Where(x => x.profileID == profileId && x.isActive == true).ToList();
                    if (insurance.Count > 0)
                    {
                        totalLifeIns = insurance.Sum(b => b.faceValue ?? 0);
                    }
                    var otherIns = db.OtherInsurances.Where(x => x.profileID == profileId && x.isActive == true).ToList();
                    if (otherIns.Count > 0)
                    {
                        totalOtherIns = otherIns.Sum(b => b.monthly ?? 0);
                    }
                    var totalInsurance = totalLifeIns + totalOtherIns;

                    if (totalInsurance > 0)
                    {
                        model.expenseList.Add(new expenseList { expenseName = "Total Insurance Premiums", amount = totalInsurance });
                        model.totalExpense = model.totalExpense + totalInsurance;
                    }
                    var taxDetails = db.IncomeTaxDetails.Where(x => x.profileID == profileId && x.isActive == true).ToList();
                    var totalTax = taxDetails.Sum(b => b.amount ?? 0);
                    if (totalTax > 0)
                    {
                        model.expenseList.Add(new expenseList { expenseName = "Total Taxes", amount = totalTax });
                        model.totalExpense = model.totalExpense + totalTax;
                    }

                    var educationList = db.Educations.Where(x => x.profileID == profileId && x.isActive == true).ToList();
                    if (educationList.Count > 0)
                    {
                        var totalSavings = educationList.Sum(b => b.monthlySavings ?? 0);
                        model.expenseList.Add(new expenseList { expenseName = "Total Savings", amount = totalSavings });
                        model.totalExpense = model.totalExpense + totalSavings;
                    }
                    var livingExp = db.LivingExpenses.Where(x => x.profileId == profileId && x.isActive == true).ToList();
                    if (livingExp.Count > 0)
                    {
                        var totallivingExp = livingExp.Sum(b => b.amount ?? 0);
                        model.expenseList.Add(new expenseList { expenseName = "Total Living Expenses", amount = totallivingExp });
                        model.totalExpense = model.totalExpense + totallivingExp;
                    }

                    if (model.totalIncome > model.totalExpense)
                    {
                        model.status = "Surplus";
                        model.statusValue = model.totalIncome - model.totalExpense;
                    }
                    else
                    {
                        model.status = "Shortfall";
                        model.statusValue = model.totalExpense - model.totalIncome;
                    }
                }

            }

            catch (Exception ex)
            {
                throw;
            }
            return model;
        }

        public InvestmentTaxableModel DashBoardInvestment(long profileID)
        {
            InvestmentTaxableModel outobj = new InvestmentTaxableModel();


            try
            {
                using (var db = Connection.getConnect())
                {
                    TaxableDetailsModel tempObj = new TaxableDetailsModel();
                    outobj.taxableafterTax = new List<TaxableDetailsModel>();
                    outobj.taxdefferedafterTax = new List<TaxableDetailsModel>();
                    outobj.taxdefferedbeforeTax = new List<TaxableDetailsModel>();
                    outobj.taxfreeafterTax = new List<TaxableDetailsModel>();

                    var nonRet = db.Asset_NonRetirement.Where(w => w.isActive == true && w.taxVsDefferedID != null && w.profileID == profileID).ToList();
                    var ret = db.Asset_RetirementDetails.Where(w => w.isActive == true && w.taxVsDefferedID != null && w.profileID == profileID).ToList();
                    var assetTypes = db.AssetTypes.Where(w => w.isActive == true).ToList();

                    if (nonRet.Count > 0)
                    {
                        var taxable = (from n in nonRet
                                       join a in assetTypes on n.assetTypeID equals a.assetTypeID
                                       select new TaxableDetailsModel
                                       {
                                           amount = n.amount ?? 0,
                                           name = a.typeName + (n.company_location != "" ? (" - " + n.company_location) : ""),
                                           taxDefID = n.taxVsDefferedID ?? 1
                                       }).ToList();
                        if (taxable.Count > 0)
                        {
                            outobj.taxdefferedafterTax.AddRange(taxable.Where(w => w.taxDefID == 1));
                            outobj.taxfreeafterTax.AddRange(taxable.Where(w => w.taxDefID == 2));
                            outobj.taxableafterTax.AddRange(taxable.Where(w => w.taxDefID == 3));
                            outobj.taxdefferedbeforeTax.AddRange(taxable.Where(w => w.taxDefID == 4));
                        }
                    }
                    if (ret.Count > 0)
                    {
                        var taxableret = (from n in ret
                                          join a in assetTypes on n.assetTypeID equals a.assetTypeID
                                          select new TaxableDetailsModel
                                          {
                                              amount = n.amount ?? 0,
                                              name = a.typeName + (n.company != "" ? (" - " + n.company) : ""),
                                              taxDefID = n.taxVsDefferedID ?? 1
                                          }).ToList();
                        if (taxableret.Count > 0)
                        {
                            outobj.taxdefferedafterTax.AddRange(taxableret.Where(w => w.taxDefID == 1));
                            outobj.taxfreeafterTax.AddRange(taxableret.Where(w => w.taxDefID == 2));
                            outobj.taxableafterTax.AddRange(taxableret.Where(w => w.taxDefID == 3));
                            outobj.taxdefferedbeforeTax.AddRange(taxableret.Where(w => w.taxDefID == 4));
                        }

                    }

                }

            }
            catch (Exception ex)
            {

            }
            outobj.taxdefferedafterTaxTotal = outobj.taxdefferedafterTax.Sum(s => s.amount);
            outobj.taxfreeafterTaxTotal = outobj.taxfreeafterTax.Sum(s => s.amount);
            outobj.taxableafterTaxTotal = outobj.taxableafterTax.Sum(s => s.amount);
            outobj.taxdefferedbeforeTaxTotal = outobj.taxdefferedbeforeTax.Sum(s => s.amount);

            return outobj;
        }

        public Education_Model educationDashBoard(long profileID)
        {
            EducationInformation edObj = new EducationInformation();
            Education_Model ed = new Education_Model();

            using (var db = Connection.getConnect())
            {
                try
                {
                    if (profileID > 0)
                    {
                        int i = 0;
                        ed.em = edObj.GetEducationInformation(profileID);
                        if (ed.em != null)
                        {
                            if (ed.em.EducationInfoModel != null)
                            {
                                ed.educationInfo = new List<EducationInfo_Model>();
                                foreach (var a in ed.em.EducationInfoModel)
                                {
                                    ed.educationInfo.Add(new EducationInfo_Model
                                    {
                                        fname = a.fName
                                    });
                                    ed.educationInfo[i].totalEdu_Need = 0;
                                    ed.educationInfo[i].totalSavings = 0;
                                    ed.educationInfo[i].educationDetails = new List<EducationDetails_Model>();
                                    int j = 0, c = 0;
                                    foreach (var d in ed.em.EducationInfoModel[i].EducationDetails)
                                    {
                                        ed.educationInfo[i].educationDetails.Add(new EducationDetails_Model
                                        {
                                            collegename = d.collegename,
                                            annualCost = d.totalAnualCost
                                        });
                                        ed.educationInfo[i].totalEdu_Need = (ed.educationInfo[i].totalEdu_Need ?? 0) + (ed.educationInfo[i].educationDetails[j].annualCost ?? 0);

                                        if (d.CurrentSavingsDetails != null)
                                        {
                                            ed.educationInfo[i].educationSavings = new List<CurrentSavingsDetail_Model>();
                                            int k = 0;
                                            foreach (var s in ed.em.EducationInfoModel[i].EducationDetails[j].CurrentSavingsDetails)
                                            {
                                                ed.educationInfo[i].educationSavings.Add(new CurrentSavingsDetail_Model
                                                {
                                                    companyName = s.company,
                                                    savingsAmount = s.amount
                                                });
                                                ed.educationInfo[i].totalSavings = (ed.educationInfo[i].totalSavings ?? 0) + (ed.educationInfo[i].educationSavings[k].savingsAmount ?? 0);
                                                k++;
   
                                                j++;
                                            }
                                        }
                                        
                                    }
                                    i++;
                                }

                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    throw;
                }
                ed.em = null;
                return ed;

            }
        }

        public List<LiabilitiesModel> DashBoardLiabilities(long profileID)
        {
            List<LiabilitiesModel> outobj = new List<LiabilitiesModel>();

            decimal p = 0;
            decimal total = 0;
            try
            {
                using (var db = Connection.getConnect())
                {
                    outobj = new List<LiabilitiesModel>();
                    var debt = db.Debts.Where(w => w.isActive == true && w.profileID == profileID).ToList();
                    var debttype = db.DebtCats.ToList();


                    if (debt.Count > 0)
                    {
                        var taxable = (from n in debt
                                       join a in debttype on n.debtCatId equals a.debtCatId
                                       select new LiabilitiesModel
                                       {
                                           amount = n.amount ?? 0,
                                           name = a.catName + (n.creditor != "" ? (" - " + n.creditor) : ""),
                                       }).ToList();
                        total = taxable.Sum(s => s.amount ?? 0);
                        foreach (var a in taxable)
                        {

                            p = Convert.ToDecimal(Math.Round(Convert.ToDouble(a.amount / total * 100), 2));
                            outobj.Add(new LiabilitiesModel
                            {

                                name = a.name,
                                amount = a.amount,
                                percentage = p
                            });

                        }
                    }


                }

            }
            catch (Exception ex)
            {

            }


            return outobj;
        }

    }

}
