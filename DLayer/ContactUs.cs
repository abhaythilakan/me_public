﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CLayer;


namespace DLayer
{
    public class ContactUs
    {

        public bool contactMoneyEdge(ContactModel contactus)
        {

            using (var db = Connection.getConnect())
            {
                try
                {
                    if (contactus != null)
                    {
                        CLayer.DBModel.contactU contact = new CLayer.DBModel.contactU();
                        contact.name = contactus.name;
                        contact.email = contactus.email;
                        contact.phone = contactus.phone;
                        contact.stateCode = contactus.stateCode ?? "";
                        contact.company = contactus.company;
                        contact.findUs = contactus.findUs ?? 0;
                        contact.subject = contactus.subject;
                        contact.comments = contactus.comments;
                        contact.toEmail = "info@moneyedgepro.com";
                        contact.status = contactus.readStatus ?? 0;
                        contact.createdDate = System.DateTime.Now;
                        db.contactUs.Add(contact);
                        db.SaveChanges();
                        contactus.contactId = contact.contactId;

                    }
                    else
                        return false;

                }

                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return true;
        }

    }
}

