﻿using CLayer;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DLayer
{
    public class Login
    {

        public bool saveActiveToken(string username, string password, string activeToken)
        {
            bool isSave = false;
            try
            {
                using (var db = Connection.getConnect())
                {
                    if (username != null && password != null && activeToken != null)
                    {
                        if (username.Trim() != "" && password.Trim() != "" && activeToken.Trim() != "")
                        {
                            Common.pwdEncryptDecrypt decrypt = new Common.pwdEncryptDecrypt();
                            string encriptPass = decrypt.Encrypt(password);
                            var usr = db.Users.Single(b => b.userName == username.Trim() && b.password == encriptPass.Trim());
                            if (usr != null)
                            {
                                usr.activeToken = activeToken;
                                db.Entry(usr).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                                isSave = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isSave;
        }

        public bool isActiveToken(long userID, string activeToken)
        {
            bool isActive = false;
            try
            {
                using (var db = Connection.getConnect())
                {
                    if (userID > 0)
                    {
                        var usr = db.Users.Where(b => b.userId == userID && b.activeToken == activeToken).FirstOrDefault();
                        if (usr != null)
                        {
                            isActive = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isActive;
        }

        public bool logOut(long userID)
        {
            bool isLogout = false;
            try
            {
                using (var db = Connection.getConnect())
                {
                    if (userID > 0)
                    {
                        var usr = db.Users.Single(b => b.userId == userID);
                        if (usr != null)
                        {
                            usr.activeToken = "";
                            db.Entry(usr).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            isLogout = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isLogout;
        }



        public bool isUserNameExists(string userName, long userId)
        {
            bool isExists = true;
            try
            {
                using (var db = Connection.getConnect())
                {
                    var itm = db.Users.Where(b => b.userId != userId && (b.userName == userName)).ToList();
                    if (itm != null)
                    {
                        if (itm.Count() > 0)
                        {
                            isExists = true;
                        }
                        else
                        {
                            isExists = false;
                        }
                    }
                    else
                    {
                        isExists = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isExists;
        }
        public bool isEmailExists(string email, long userId)
        {
            bool isExists = true;
            try
            {
                using (var db = Connection.getConnect())
                {
                    var itm = db.Users.Where(b => b.userId != userId && (b.emailPrimary == email)).ToList();
                    if (itm != null)
                    {
                        if (itm.Count() > 0)
                        {
                            isExists = true;
                        }
                        else
                        {
                            isExists = false;
                        }
                    }
                    else
                    {
                        isExists = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return isExists;
        }

        public UserModels isValidUser(string username, string password)//string username, string password
        {
            CLayer.UserModels model = new CLayer.UserModels();
            User user = new User();
            Common.pwdEncryptDecrypt decrypt = new Common.pwdEncryptDecrypt();
            try
            {
                using (var db = Connection.getConnect())
                {
                    string pass = decrypt.Encrypt(password);
                    user = db.Users.Where(b => b.userName == username && b.password == pass && b.isActive == true).FirstOrDefault();
                    if (user != null && user.userId > 0)
                    {

                        model.userId = user.userId;
                        model.userName = user.userName;
                        model.emailPrimary = user.emailPrimary;
                        model.advisorId = user.advisorId;
                        model.clientId = user.clientId;
                        model.trialEndDate = user.trialEndDate;
                        model.expDate = user.expDate;
                        model.isAdmin = user.isAdmin;
                        model.isManager = user.isManager;
                        model.isSuperUser = user.isSuperUser;
                        model.isLicensed = user.isLicensed;
                        model.hashCode = user.hashCode;
                        model.requestedForgot = user.requestedForgot;
                        model.isTrial = user.isTrial;
                        model.isNonTrial = user.isNonTrial;
                        model.profileId = 0;

                        var proId = (dynamic)null;
                        if (user.clientId > 0)
                        {
                            proId = db.profiles.Where(b => b.clientID == user.clientId).OrderBy(b => b.createdDate).FirstOrDefault().profileID;
                        }
                        model.profileId = proId == null ? Convert.ToInt64(0) : proId;
                        LoginTrack lt = db.LoginTracks.Where(b => b.userId == user.userId).FirstOrDefault();
                        if (lt != null && lt.loginTrackId > 0)
                        {
                            string inn = lt.logInDate;
                            string item = "";
                            if (inn.Split(',').Count() > 20)
                            {
                                for (int i = 0; i < inn.Split(',').Count(); i++)
                                {
                                    if (inn.Split(',')[i].Trim() != "")
                                    {
                                        if (i > 0)
                                            item = item + inn.Split(',')[i] + ",";
                                    }
                                }
                                lt.logInDate = item + DateTime.Now + ",";
                                db.Entry(lt).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                            else
                            {
                                lt.logInDate = lt.logInDate ?? "";
                                lt.logInDate = lt.logInDate + DateTime.Now + ",";
                                db.Entry(lt).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            lt = new LoginTrack();
                            lt.logInDate = DateTime.Now.ToString() + ",";
                            //lt.logOutDate = "Logged-out: " + DateTime.Now.ToString() + " :-: ";
                            lt.userId = user.userId;
                            db.LoginTracks.Add(lt);
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return model;
        }

        #region Validation with Old Database

        //public LoginAdvisor isValidUser_AdminAdvisor(string username, string password)
        //{
        //using (var db = Connection.getConnect())
        //{
        //    //List<SqlParameter> paras = new List<SqlParameter>();
        //    //paras.Add(new SqlParameter { ParameterName = "username", Value = username });
        //    //paras.Add(new SqlParameter { ParameterName = "password", Value = password });
        //    var userParam = new SqlParameter { ParameterName = "username", Value = username };
        //    var passParam = new SqlParameter { ParameterName = "password", Value = password };
        //    LoginAdvisor advDet = db.Database.SqlQuery<LoginAdvisor>("exec loginAdvisor_v3 @username,@password", userParam, passParam).FirstOrDefault();
        //    //var df = db.loginAdvisor_v3("danharriman", "edge2014").ToList();

        //    if (advDet != null && advDet.advisorID > 0)
        //    {
        //        advDet.managerID = Convert.ToInt64(advDet.managerID);
        //        if (advDet.managerID == 0)
        //        {
        //            return advDet;
        //            //Successfully Advisor Logged IN
        //            //Claim/Session
        //            //insert values into advisorLog
        //        }
        //        else if (advDet.managerID != 0)
        //        {
        //            if (advDet.isActive == 1)
        //            {
        //                if (advDet.endTrialDate == null)
        //                {
        //                    if (advDet.expireDate >= DateTime.Now.AddMonths(-1))
        //                    {
        //                        return advDet;
        //                        //Successfully Advisor Logged IN
        //                        //Claim/Session
        //                        //insert values into advisorLog
        //                    }
        //                    else
        //                    {
        //                        return null;
        //                        //Error
        //                    }
        //                }
        //                else if (advDet.endTrialDate >= DateTime.Now)
        //                {
        //                    return advDet;
        //                    //Successfully Advisor Logged IN
        //                    //Claim/Session
        //                    //insert values into advisorLog
        //                }
        //                else
        //                {
        //                    return null;
        //                }
        //            }
        //            else
        //            {
        //                return null;
        //            }
        //        }
        //        else
        //        {
        //            return null;
        //        }
        //    }
        //    else
        //    {
        //        return null;
        //    }
        //}
        //}


        //public User isValidUser_Client(string username, string password)//string username, string password
        //{
        //using (var db = Connection.getConnect())
        //{
        //    var userParam = new SqlParameter { ParameterName = "username", Value = username };
        //    var passParam = new SqlParameter { ParameterName = "password", Value = password };
        //    LoginClient cliDet = db.Database.SqlQuery<LoginClient>("exec loginClient_v2 @username,@password", userParam, passParam).FirstOrDefault();

        //    if (cliDet != null && cliDet.clientID > 0)
        //    {
        //        cliDet.profileID = Convert.ToInt64(cliDet.profileID);
        //        if (cliDet.profileID != 0)
        //        {
        //            if (cliDet.isActive == 1)
        //            {
        //                if (cliDet.endTrialDate == null)
        //                {
        //                    if (cliDet.expireDate > DateTime.Now.AddMonths(-1) || cliDet.exemptbilling > 0)
        //                    {
        //                        return cliDet;
        //                        //Successfully Client Logged IN
        //                        //Claim/Session
        //                        //insert values into clientLog
        //                    }
        //                    else
        //                    {
        //                        return null;
        //                        //Error
        //                    }
        //                }
        //                else
        //                {
        //                    if (cliDet.endTrialDate >= DateTime.Now || cliDet.exemptbilling > 0)
        //                    {
        //                        return cliDet;
        //                        //Successfully Client Logged IN
        //                        //Claim/Session
        //                        //insert values into clientLog
        //                    }
        //                    else
        //                    {
        //                        return null;
        //                        //Error
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                return null;
        //                //Error
        //            }
        //        }
        //        else
        //        {
        //            return null;
        //            //Error
        //        }
        //    }
        //    else
        //    {
        //        return null;
        //        //Error
        //    }
        //}
        //}

        #endregion

        public User hasValidAccount(string email, string hashCode)
        {
            //bool hasUser = false;
            User user = new User();
            try
            {
                using (var db = Connection.getConnect())
                {
                    if (db.Users.Where(b => b.emailPrimary == email && b.isActive == true).Any())
                    {
                        user = db.Users.Where(b => b.emailPrimary == email && b.isActive == true).FirstOrDefault();
                        if (user != null)
                        {
                            user.hashCode = hashCode;
                            user.requestedForgot = true;
                            db.Entry(user).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            //hasUser = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return user;
        }

        public long validForgorPasswordToken(string email, string hashCode)
        {
            User usr = new User();
            try
            {
                using (var db = Connection.getConnect())
                {
                    usr = db.Users.Where(b => b.emailPrimary == email && b.hashCode == hashCode && b.isActive == true).FirstOrDefault();
                    if (usr != null && usr.userId > 0)
                    {
                        return usr.userId;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return 0;
        }

        public bool resetPassword(ForgotPasswodModel fp)
        {
            try
            {
                Common.pwdEncryptDecrypt cipher = new Common.pwdEncryptDecrypt();
                using (var db = Connection.getConnect())
                {
                    User usr = db.Users.Where(b => b.userId == fp.userID && b.isActive == true && b.hashCode == fp.token).Single();
                    usr.password = cipher.Encrypt(fp.password);
                    usr.hashCode = "";
                    usr.requestedForgot = false;
                    db.Entry(usr).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return false;
        }

        public bool setLoginedUserToken(long ID, CLayer.common.CustomEnum.userRoles roles)
        {
            try
            {
                using (var db = Connection.getConnect())
                {
                    if (roles == CLayer.common.CustomEnum.userRoles.Admin)
                    {
                        var lis = db.Clients.Where(b => b.isActive == true).ToList();
                        if (lis != null && lis.Count() > 0)
                        {
                            return true;
                        }
                    }
                    else if (roles == CLayer.common.CustomEnum.userRoles.Advisor)
                    {
                        var lis = (from c in db.Clients
                                   join u in db.Users on c.clientId equals u.clientId
                                   where ((c.advisorId == ID && u.isSample != true) || u.isSample == true) && u.isActive == true && c.isActive == true
                                   select c).ToList();
                        if (lis != null && lis.Count() > 0)
                        {
                            return true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return false;
        }

    }
}
