﻿using CLayer;
using System;
using CLayer.common;
using CLayer.DBModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Globalization;

namespace DLayer
{
    public class IncomeDetails
    {
        public List<IncomeProjectionModel> incomeProjection(long profileID, decimal rate = 0,decimal srate=0)
        {
            List<IncomePorjectionObjectModel> inobj = new List<IncomePorjectionObjectModel>();
            List<IncomeProjectionModel> outobj = new List<IncomeProjectionModel>();
            DefaultIncomeProjection Default = new DefaultIncomeProjection();
            using (var db = Connection.getConnect())
            {
                try
                {
                    var reportdate = db.profiles.Where(v => v.profileID == profileID).Select(v => v.profileDate).FirstOrDefault();
                    Default = db.DefaultIncomeProjections.Where(b => b.profileID == profileID).SingleOrDefault();
                    if (Default == null)
                    {
                        if (Default == null)
                        {
                            Default = new DefaultIncomeProjection();
                            Default = db.DefaultIncomeProjections.Join(db.profiles.Where(x => x.profileID == profileID), d => d.clientID, p => p.clientID, (d, p) => d) .FirstOrDefault();
                        }
                        if (Default == null)
                        {
                            throw new Exception("Income defaults not found.");
                        }
                    }                 
                        decimal inflation = 0;
                             inobj = (from i in db.Incomes
                             join p in db.Persons on i.personID equals p.personID
                             where i.isActive == true && i.profileID == profileID && i.isFutureIncome == false
                             orderby p.isPrimary descending
                             select new IncomePorjectionObjectModel
                             {
                                 cola = i.cola ?? 0,
                                 dob = p.birthDate,
                                 income = i.grossMonthlyIncome??0,
                                 incomeID = i.incomeID,
                                 isFuture = i.isFutureIncome.Value,
                                 isprimary = p.isPrimary.Value,
                                 lifeExpectancy = p.lifeExpectancy ?? 60,
                                 name = p.firstName,
                                 retAge = p.retirementAge.Value,
                                 startDate = reportdate
                             }).ToList();
                    int loop = 0;
                    //if (rate > 0)
                    //{
                    //    inflation = rate;
                    //}
                    //else
                    //{
                    //    if (Default != null && Default.defaultID > 0)
                    //    {
                    //        inflation = Default.inflation.Value;
                    //    }
                    //    else
                    //    {
                    //        inflation = 3;
                    //    }
                    //}
                    outobj = new List<IncomeProjectionModel>();
                    int tempendyear = 0;
                    int tempstartyear = 10000;
                    if (inobj.Count > 0)
                    {
                        #region income Projection spouse and primary
                        foreach (var v in inobj)
                        {
                            outobj.Add(new IncomeProjectionModel() { name = v.name });
                            outobj[loop].retage = v.retAge;
                            
                            if (v.isprimary == true )
                            {
                                outobj[loop].role = "Primary";
                                if (rate == 0)
                                {
                                    outobj[loop].inflation = Default.primaryInflation.Value;
                                    inflation = outobj[loop].inflation;
                                }
                                else
                                {
                                    outobj[loop].inflation = rate;
                                    inflation = outobj[loop].inflation;
                                }
                            }
                            else
                            {
                                outobj[loop].role = "Spouse";
                                if (srate == 0)
                                {
                                    outobj[loop].inflation = Default.spouseInflation.Value;
                                    inflation = outobj[loop].inflation;
                                }
                                else
                                {
                                    outobj[loop].inflation = srate;
                                    inflation = outobj[loop].inflation;
                                }

                            }
                            int age = 0;
                            int endYear = v.dob.Value.Year + v.retAge;
                            int startYear = v.startDate.Value.Year;
                            if (tempendyear < endYear)
                            {
                                tempendyear = endYear;
                            }
                            if (tempstartyear > startYear)
                            {
                                tempstartyear = startYear;
                            }

                            outobj[loop].IncomeProjectionDetails = new List<IncomeProjectionDetailsModel>();
                            // outobj[loop].IncomeProjectionDetails.Add(new IncomeProjectionDetailsModel() { age = startYear });
                            //DateTime zeroTime = new DateTime(1, 1, 1);
                            //DateTime a;
                            //DateTime b;
                            //a = (DateTime)v.dob.Value; 
                            //b = System.DateTime.Now;
                            //TimeSpan span = b - a;
                            //decimal years = Convert.ToDecimal((zeroTime + span).Year - 1);

                            //DateTime zeroTime = new DateTime(1, 1, 1);
                            //DateTime a = v.dob.Value;
                            //DateTime b = v.startDate.Value;
                           // TimeSpan span = b - a;
                            int years = v.startDate.Value.Year- v.dob.Value.Year;
                            age = years;
                            decimal totalSum = 0;
                            //outobj[0].name = v.name;
                            int j = 0;
                            while (startYear <= endYear-1 )
                            {
                                outobj[loop].IncomeProjectionDetails.Add(new IncomeProjectionDetailsModel() { age = age, year = startYear });//= age;
                                                                                                                                             // outobj[loop].IncomeProjectionDetails[j].year = startYear;
                                if (j == 0)
                                {
                                    outobj[loop].IncomeProjectionDetails[j].amount = v.income * 12;
                                    totalSum = totalSum + outobj[loop].IncomeProjectionDetails[j].amount;
                                }
                                else
                                {
                                    outobj[loop].IncomeProjectionDetails[j].amount = Math.Round(outobj[loop].IncomeProjectionDetails[j - 1].amount + (outobj[loop].IncomeProjectionDetails[j - 1].amount * (inflation / 100)), 0);
                                    totalSum = totalSum + outobj[loop].IncomeProjectionDetails[j].amount;
                                }
                                age++;
                                startYear++;
                                j++;
                            }
                            outobj[loop].totalSum = totalSum;
                            outobj[loop].grossMonthlyIncome = v.income;                           
                           
                            loop++;
                        }
                        #endregion income Projection spouse and primary
                    }
                    inobj = (from i in db.Incomes
                                 //join p in db.Persons on i.personID equals p.personID
                             where i.isActive == true && i.profileID == profileID && i.isFutureIncome == true
                             select new IncomePorjectionObjectModel
                             {
                                 cola = i.cola ?? 0,
                                 // dob = p.birthDate,
                                 income = i.grossMonthlyIncome??0,
                                 incomeID = i.incomeID,
                                 isFuture = i.isFutureIncome.Value,
                                 //isprimary = p.isPrimary.Value,
                                 // lifeExpectancy = p.lifeExpectancy ?? 60,
                                 // name = p.firstName,
                                 // retAge = p.retirementAge.Value,
                                 startDate = i.startDate,

                                 endDate = i.endDate
                             }).ToList();
                    if (inobj.Count > 0)
                    {
                        int startYear = inobj.Min(b => b.startDate.Value.Year);
                        int endYear = inobj.Max(b => b.endDate.Value.Year);
                        outobj.Add(new IncomeProjectionModel() { name = "Other Income" });
                        int count = outobj.Count() - 1;
                        int i = 0;
                        outobj[count].IncomeProjectionDetails = new List<IncomeProjectionDetailsModel>();
                        #region income Projection OtherCurrent and Future
                        foreach (var other in inobj)
                        {
                            int j = 0;
                            int temp = 0;
                            int startYear2 = startYear;
                            int loopYear = other.startDate.Value.Year;
                            decimal sum = 0;
                            decimal tax = 0;
                            other.totalTax= getTotalTax(inobj[i].incomeID);
                            while (startYear2 <= endYear)
                            {
                                if (i == 0)
                                {
                                    outobj[count].IncomeProjectionDetails.Add(new IncomeProjectionDetailsModel() { year = startYear2, amount = 0,Tax=0 });
                                }
                                if (loopYear == startYear2 && loopYear <= other.endDate.Value.Year)
                                {
                                    if (temp == 0)
                                    {                                     
                                        decimal amount = 0;
                                        decimal no_of_Months = 12 - (other.startDate.Value.Month-1);
                                        if (no_of_Months == 0)
                                        {
                                            amount = 1 * other.income;
                                            tax = other.totalTax.Value;
                                           // pgrowth = /*pgrowth +*/ pCurrentBalance * Math.Pow((1 + tax), (Convert.ToDouble(months) / 12)) - pCurrentBalance;        //   1000 * ((1 + .1) ^ (0.166667)) - 1000 
                                        }
                                        else
                                        {
                                            amount = no_of_Months * other.income;
                                            tax= no_of_Months * other.totalTax.Value;
                                        }
                                        sum = amount;
                                      //  outobj[count].IncomeProjectionDetails[j].Tax += Math.Round( tax * Convert.ToDecimal(Math.Pow(Convert.ToDouble(1 + other.cola.Value), i)),0);
                                    }
                                    else
                                    {
                                        if (startYear2 == other.endDate.Value.Year)
                                        {
                                            decimal no_of_Months = 12 - other.endDate.Value.Month;
                                            decimal amount = other.income * no_of_Months;
                                            // decimal f = amount * Convert.ToDecimal(Math.Pow( Convert.ToDouble(1 + other.cola.Value/1200), Convert.ToDouble(no_of_Months)));
                                            sum = amount + (amount * other.cola.Value / 1200) * no_of_Months;
                                            tax= no_of_Months * other.totalTax.Value;
                                        }
                                        else
                                        {
                                            if (temp == 1)
                                            {
                                                sum = other.income * 12;
                                            }
                                            sum = sum + (sum * other.cola.Value / 100);
                                            tax = 12 * other.totalTax.Value;
                                        }

                                    }
                                    temp++;
                                    loopYear++;
                                    outobj[count].IncomeProjectionDetails[j].amount = Math.Round(outobj[loop].IncomeProjectionDetails[j].amount + sum, 0);
                                    outobj[count].IncomeProjectionDetails[j].Tax = outobj[loop].IncomeProjectionDetails[j].Tax + Math.Round(tax * Convert.ToDecimal(Math.Pow(Convert.ToDouble(1 + other.cola.Value/100), temp-1)), 0);

                                }
                                else
                                {
                                    sum = 0;
                                    tax = 0;
                                    outobj[count].IncomeProjectionDetails[j].amount = Math.Round(outobj[loop].IncomeProjectionDetails[j].amount + sum, 0);
                                    outobj[count].IncomeProjectionDetails[j].Tax = Math.Round(outobj[loop].IncomeProjectionDetails[j].Tax.Value + tax , 0);
                                }
                                startYear2++;
                                j++;
                            }
                            i++;
                        }
                        #endregion income Projection OtherCurrent and Future
                        outobj[count].totalSum = outobj[count].IncomeProjectionDetails.Sum(x => x.amount);


                        if (tempstartyear >= startYear)
                        {
                            tempstartyear = startYear;
                        }
                        else if (tempendyear >= startYear)
                        {
                            tempstartyear = startYear;
                        }
                        else
                        {

                            tempstartyear = 1;
                        }
                        if (tempendyear >= endYear)
                        {
                            tempendyear = endYear;
                        }
                        else if (tempendyear <= endYear)
                        {
                            tempendyear = tempendyear;
                        }
                        else
                        {
                            tempendyear = 1;
                        }


                        var cnt = outobj[count].IncomeProjectionDetails.Where(b => b.year <= tempendyear && b.year >= tempstartyear).Sum(x => x.amount);

                        if (cnt != null && cnt > 0)
                        {
                            outobj[count].otherTotalIncomeBeforeRet = cnt;
                        }
                        else
                        {
                            outobj[count].otherTotalIncomeBeforeRet = 0;
                        }
                        outobj[loop].role = "Other";
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return outobj;
        }
        public List<IncomeModel> getIncomes(long profileID, CustomEnum.IncomeTypeEnum type)
        {
            List<IncomeModel> outObj = new List<IncomeModel>();
            List<Income> inObj = new List<Income>();
            using (var db = Connection.getConnect())
            {
                try
                {
                    bool status = true;
                    if (type == CustomEnum.IncomeTypeEnum.MonthlyIncome)
                    { status = false; }
                    else if (type == CustomEnum.IncomeTypeEnum.FutureIncome)
                    { status = true; }
                    inObj = db.Incomes.Where(b => b.profileID == profileID && b.isFutureIncome == status && b.isActive == true && b.isActive != null).ToList();
                    if (inObj != null && inObj.Count > 0)
                    {
                        foreach (var a in inObj)
                        {
                            if (a.incomeID > 0)
                            {
                                outObj.Add(fillincomeDbtoObj(a));
                            }
                        }
                    }
                    if (outObj.Count == 0)
                    {
                        outObj.Add(new IncomeModel() { profileID = profileID });

                        try
                        {
                        }
                        catch (Exception ex)
                        {
                            var a = db.Persons.Where(b => b.profileID == profileID).FirstOrDefault();
                            if (status == true && a != null)
                            {
                                outObj[0].startType = 0;
                                outObj[0].startDate = a.personCreatedDate;
                                outObj[0].endDate = a.birthDate.Value.AddYears(a.lifeExpectancy.Value);
                                outObj[0].endType = 0;
                            }

                        }
                    }

                }


                catch (Exception ex)
                {
                    throw;
                }

                return outObj;

            }
        }

        private IncomeModel fillincomeDbtoObj(Income i)
        {
            IncomeModel outobj = new IncomeModel()
            {
                incomeID = i.incomeID,
                profileID = i.profileID,
                name = i.name,
                personID = i.personID,
                grossMonthlyIncome = i.grossMonthlyIncome ?? 0,
                payType = i.payType ?? (int)CustomEnum.PayperiodEnum.Monthly,
                payIncome = i.payIncome ?? 0,
                payRollTaxes = getTotalTax(i.incomeID),
                //homePay = i.homePay ?? 0,
                //allowances = i.allowances ?? 0,
                notes = i.notes,
                isActive = i.isActive,
                cola = i.cola ?? 0,
                startType = i.startType ?? 0,
                startDate = i.startDate,
                endType = i.endType ?? 0,
                endDate = i.endDate,
                startAge=i.startAge,
                endAge=i.endAge,
                isFutureIncome = i.isFutureIncome,
                incomeCreatedDate = i.incomeCreatedDate,
                incomeUpdateDate = i.incomeUpdateDate,
                incomeModifiedBy = i.incomeModifiedBy
            };

            return outobj;
        }

        public List<IncomeModel> saveMonthlyIncome(List<IncomeModel> inObj, long usrID)
        {
            Income dbobj = new Income();
            using (var db = Connection.getConnect())
            {
                try
                {
                    int i = 0;
                    foreach (var obj in inObj)
                    {
                        if (obj.incomeID > 0)
                        {
                            dbobj = db.Incomes.Where(b => b.incomeID == obj.incomeID && b.isFutureIncome == false).SingleOrDefault();
                            if (dbobj != null && dbobj.incomeID > 0)
                            {
                                //  dbobj.incomeID = obj.incomeID;
                                //dbobj.profileID = obj.profileID;
                                // dbobj.name = obj.name;
                                // dbobj.personID = obj.personID;
                                dbobj.payType = obj.payType ?? 1;
                                dbobj.payIncome = obj.payIncome ?? 0;
                                switch (dbobj.payType)
                                {
                                    case (int)CustomEnum.PayperiodEnum.Monthly:
                                        dbobj.grossMonthlyIncome = obj.payIncome;
                                        break;
                                    case (int)CustomEnum.PayperiodEnum.Semimonthly:
                                        dbobj.grossMonthlyIncome = Math.Round(Convert.ToDecimal(obj.payIncome) * 2, 0);
                                        break;
                                    case (int)CustomEnum.PayperiodEnum.Weekly:
                                        dbobj.grossMonthlyIncome = Math.Round(Convert.ToDecimal(obj.payIncome) * 52 / 12, 0);
                                        break;
                                    case (int)CustomEnum.PayperiodEnum.Biweekly:
                                        dbobj.grossMonthlyIncome = Math.Round(Convert.ToDecimal(obj.payIncome) * 26 / 12, 0);
                                        break;
                                    default:
                                        dbobj.grossMonthlyIncome = obj.payIncome ?? 0;
                                        break;
                                }
                                dbobj.isFutureIncome = false;
                                //dbobj.homePay = obj.homePay ?? 0;
                                //dbobj.allowances = obj.allowances ?? 0;
                                dbobj.notes = obj.notes ?? "";
                                dbobj.totalTaxeAmount = obj.payRollTaxes ?? 0;
                                dbobj.cola = obj.cola ?? 0;
                                dbobj.startType = obj.startType ?? 0;
                                dbobj.startDate = obj.startDate;
                                dbobj.endType = obj.endType ?? 0;
                                dbobj.endDate = obj.endDate;
                                dbobj.incomeModifiedBy = usrID;
                                if (dbobj.incomeCreatedDate == null)
                                {
                                    dbobj.incomeCreatedDate = System.DateTime.Now;
                                }
                                else
                                {
                                    dbobj.incomeUpdateDate = System.DateTime.Now;
                                }
                                dbobj.totalTaxeAmount = getTotalTax(obj.incomeID.Value);
                                db.Entry(dbobj).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                                inObj[i].payRollTaxes = dbobj.totalTaxeAmount ?? 0;
                                inObj[i].grossMonthlyIncome = dbobj.grossMonthlyIncome ?? 0;
                                //inObj[i].profileID = dbobj.profileID ;
                                //inObj[i].name = dbobj.name;
                            }
                        }
                        i++;
                    }

                }
                catch (Exception Ex)
                {
                    throw;
                }
                return inObj;

            }
        }

        public List<IncomeModel> saveFutureIncome(List<IncomeModel> inObj, long usrID)
        {
            Income dbobj = new Income();
            using (var db = Connection.getConnect())
            {
                try
                {
                    int i = 0;
                    foreach (var obj in inObj)
                    {
                        if (obj.incomeID > 0 && obj.incomeID !=null)
                        {
                            dbobj = db.Incomes.Where(b => b.incomeID == obj.incomeID && b.isFutureIncome == true && b.isActive == true).SingleOrDefault();
                            if (dbobj != null && dbobj.incomeID > 0)
                            {
                                dbobj.incomeID = obj.incomeID.Value;
                                // dbobj.profileID = obj.profileID;
                                dbobj.name = obj.name;
                                //dbobj.personID = obj.personID;
                                dbobj.payType = obj.payType ?? 1;
                                dbobj.payIncome = obj.payIncome ?? 0;
                                dbobj.grossMonthlyIncome = obj.grossMonthlyIncome ?? 0;
                                dbobj.isFutureIncome = true;
                                //dbobj.homePay = obj.homePay ?? 0;
                                //dbobj.allowances = obj.allowances ?? 0;
                                dbobj.notes = obj.notes ?? "";
                                //dbobj.isActive = obj.isActive;
                                dbobj.cola = obj.cola ?? 0;
                                dbobj.startType = obj.startType ?? 0;
                                if (obj.startType == 0)
                                {
                                    dbobj.startDate = obj.startDate;
                                }
                                else if (obj.startType > 0)
                                {
                                    if (obj.startAge == null || obj.startAge == 0)
                                    {
                                       // dbobj.startDate = db.Incomes.Where(b => b.personID == obj.startType).Select(s => s.incomeCreatedDate).FirstOrDefault();
                                        dbobj.startDate = new DateTime((db.Incomes.Where(b => b.personID == obj.startType).Select(s => s.incomeCreatedDate.Value.Year).FirstOrDefault()), 1, 1);
                                        obj.startAge = null;
                                    }
                                    else
                                    {
                                        dbobj.startDate = new DateTime((db.Persons.Where(b => b.personID == obj.startType).Select(s => s.birthDate.Value.Year + (obj.startAge.Value)).FirstOrDefault()), 1, 1);
                                        //db.Persons.Where(b => b.personID == obj.startType).Select(s => s.birthDate.Value.AddYears(obj.startAge.Value)).FirstOrDefault();
                                        dbobj.startAge = obj.startAge;
                                    }
                                }
                                else
                                {
                                        //dbobj.startDate = db.Incomes.Where(b => b.personID == obj.startType).Select(s => s.incomeCreatedDate).FirstOrDefault();
                                        //if (dbobj.startDate == null)
                                        //{
                                        //    dbobj.startDate = db.Incomes.Where(b => b.personID == obj.startType).Select(s => s.incomeCreatedDate).FirstOrDefault();
                                        //}
                                }

                                dbobj.endType = obj.endType ?? 0;
                                if (obj.endType == 0)
                                {
                                    dbobj.endDate = obj.endDate;
                                }
                                else if (obj.endType > 0)
                                {
                                    var a = db.Persons.Where(b => b.personID == obj.endType).FirstOrDefault();
                                    if (obj.endAge == null || obj.endAge == 0)
                                    {
                                       // dbobj.startDate = new DateTime((db.Persons.Where(b => b.personID == obj.startType).Select(s => s.birthDate.Value.Year + (obj.startAge.Value)).FirstOrDefault()), 1, 1);
                                        dbobj.endDate = new DateTime(a.birthDate.Value.Year+(a.lifeExpectancy.Value),1,1);
                                        obj.endAge = null;
                                    }
                                    else
                                    {
                                        dbobj.endDate = new DateTime(a.birthDate.Value.Year +(obj.endAge.Value),1,1);
                                        dbobj.endAge = obj.endAge.Value;
                                    }
                                }

                                if (dbobj.startAge > dbobj.endAge)
                                {
                                    throw new Exception("Future income start date must be less than end date");
                                }
                                dbobj.incomeUpdateDate = System.DateTime.Now;
                                dbobj.incomeModifiedBy = usrID;
                                dbobj.totalTaxeAmount = getTotalTax(obj.incomeID.Value);
                                db.Entry(dbobj).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                                inObj[i].payRollTaxes = dbobj.totalTaxeAmount ?? 0;
                                inObj[i].endDate = dbobj.endDate;
                                inObj[i].startDate = dbobj.startDate;
                                //inObj[i].grossMonthlyIncome = dbobj.grossMonthlyIncome ?? 0;
                            }
                        }
                        else if (obj.profileID > 0)
                        {
                            dbobj = new Income()
                            {
                                profileID = obj.profileID,
                                name = obj.name,
                                personID = obj.personID ?? 0,
                                payType = obj.payType ?? 1,
                                grossMonthlyIncome = obj.grossMonthlyIncome ?? 0,
                                payIncome = obj.payIncome ?? 0,
                                isFutureIncome = true,
                                //homePay = obj.homePay ?? 0,
                                //allowances = obj.allowances ?? 0,
                                notes = obj.notes ?? "",
                                isActive = true,
                                cola = obj.cola ?? 0,
                               // startType = obj.startType ?? 0,
                                //startDate = obj.startDate,
                               // endType = obj.endType ?? 0,
                               // endDate = obj.endDate,
                                totalTaxeAmount = obj.payRollTaxes ?? 0,
                                incomeCreatedDate = System.DateTime.Now,
                                incomeModifiedBy = usrID,
                            };
                            if (obj.startType == 0)
                            {
                                dbobj.startDate = obj.startDate;
                            }
                            else if (obj.startType > 0)
                            {
                                if (obj.startAge == null || obj.startAge == 0)
                                {
                                    // dbobj.startDate = db.Incomes.Where(b => b.personID == obj.startType).Select(s => s.incomeCreatedDate).FirstOrDefault();
                                    dbobj.startDate = new DateTime((db.Incomes.Where(b => b.personID == obj.startType).Select(s => s.incomeCreatedDate.Value.Year).FirstOrDefault()), 1, 1);
                                    obj.startAge = null;
                                }
                                else
                                {
                                    dbobj.startDate = new DateTime((db.Persons.Where(b => b.personID == obj.startType).Select(s => s.birthDate.Value.Year + (obj.startAge.Value)).FirstOrDefault()), 1, 1);
                                    //db.Persons.Where(b => b.personID == obj.startType).Select(s => s.birthDate.Value.AddYears(obj.startAge.Value)).FirstOrDefault();
                                    dbobj.startAge = obj.startAge;
                                }
                            }
                            else
                            {
                                dbobj.startDate = db.Persons.Where(b => b.personID == obj.startType).Select(s => s.personCreatedDate.Value).FirstOrDefault();
                            }
                            dbobj.endType = obj.endType ?? 0;
                            if (obj.endType == 0)
                            {
                                dbobj.endDate = obj.endDate;
                            }
                            else if (obj.endType > 0)
                            {
                                var a = db.Persons.Where(b => b.personID == obj.endType).FirstOrDefault();
                                if (obj.endAge == null || obj.endAge == 0)
                                {
                                    // dbobj.startDate = new DateTime((db.Persons.Where(b => b.personID == obj.startType).Select(s => s.birthDate.Value.Year + (obj.startAge.Value)).FirstOrDefault()), 1, 1);
                                    dbobj.endDate = new DateTime(a.birthDate.Value.Year + (a.lifeExpectancy.Value), 1, 1);
                                    obj.endAge = null;
                                }
                                else
                                {
                                    dbobj.endDate = new DateTime(a.birthDate.Value.Year + (obj.endAge.Value), 1, 1);
                                    dbobj.endAge = obj.endAge.Value;
                                }
                            }
                            db.Incomes.Add(dbobj);
                            db.SaveChanges();
                            inObj[i].incomeID = dbobj.incomeID;
                        }
                        i++;
                    }
                }
                catch (Exception Ex)
                {
                    throw;
                }
                return inObj;

            }
        }

        private decimal? getTotalTax(long incomeID)//future and payroll tax(spouse primary)
        {
            decimal? d = 0;
            using (var db = Connection.getConnect())
            {
                try
                {
                    d = db.IncomeTaxDetails.Where(b => b.incomeID == incomeID && b.isOther == false && b.isActive == true).Sum(b => b.amount) ?? Convert.ToDecimal(0.0);
                    if (d == null)
                    { d = 0; }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return d;
        }

        private decimal? getTotalTaxFuture(long id)
        {
            decimal d = 0;
            using (var db = Connection.getConnect())
            {
                try
                {
                    var a = db.IncomeTaxDetails.Where(b => b.profileID == id && b.isOther == true && b.isActive == true).Sum(b => b.amount)?? Convert.ToDecimal(0.0);
                   
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return d;
        }

        public List<IncomeTaxDetailModel> getTaxes(long id)
        {
            List<IncomeTaxDetailModel> outObj = new List<IncomeTaxDetailModel>();
            using (var db = Connection.getConnect())
            {
                try
                {
                    outObj = (from d in db.IncomeTaxDetails
                              where d.incomeID == id && d.isActive == true && d.isOther == false
                              select new IncomeTaxDetailModel
                              {
                                  paytype = d.paytype,
                                  payingTax = d.payingTax,
                                  incomeID = d.incomeID,
                                  isActive = d.isActive,
                                  taxDetailID = d.taxDetailID,
                                  taxTypeID = d.taxID,
                                  note = d.note,
                                  isMouseHover = d.isMouseHover

                              }).ToList();
                    if (outObj.Count == 0)
                    {
                        outObj.Add(new IncomeTaxDetailModel { amount = 0, incomeID = id });

                    }
                }

                catch (Exception ex)
                {
                    throw;
                }
            }
            return outObj;
        }//income ID
        public IncomeOtherTaxModel getOtherTaxes(long id)//profileID
        {
            IncomeOtherTaxModel outObj = new IncomeOtherTaxModel();
            using (var db = Connection.getConnect())
            {
                try
                {
                    //   var type = db.IncomeTaxes.ToList();
                    outObj.otherTaxDetails = (from d in db.IncomeTaxDetails
                                              where d.profileID == id && d.isActive == true && d.isOther == true
                                              select new IncomeTaxDetailModel
                                              {
                                                  payingTax = d.payingTax,
                                                  paytype = d.paytype,
                                                  incomeID = d.incomeID,
                                                  isActive = d.isActive,
                                                  taxDetailID = d.taxDetailID,
                                                  taxTypeID = d.taxID,
                                                  TypeName = db.IncomeTaxes.FirstOrDefault(x => x.taxID == d.taxID.Value).taxName.ToString(),
                                                  note = d.note,
                                                  isMouseHover = d.isMouseHover

                                              }).ToList();

                    if (outObj.otherTaxDetails.Count > 0)
                    {
                        outObj.totalTax = outObj.otherTaxDetails.Sum(b => b.amount).Value;
                    }
                    else
                    {
                        outObj.totalTax = 0;

                        outObj.otherTaxDetails.Add(new IncomeTaxDetailModel { amount = 0, incomeID = id });

                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return outObj;
        }
        public List<IncomeTaxDetailModel> saveTaxeDetails(List<IncomeTaxDetailModel> inObj, long usrID)//annually
        {
            IncomeTaxDetail dbobj = new IncomeTaxDetail();
            using (var db = Connection.getConnect())
            {
                try
                {
                    int i = 0;
                    foreach (var a in inObj)
                    {
                        if (a.taxDetailID > 0 && a != null)
                        {
                            dbobj = db.IncomeTaxDetails.Where(b => b.taxDetailID == a.taxDetailID && b.isActive == true && b.isOther == false).SingleOrDefault();
                            if (dbobj != null && dbobj.taxDetailID > 0)
                            {
                                switch (dbobj.paytype)
                                {
                                    case (int)CustomEnum.PayTaxperiodEnum.Monthly:
                                        dbobj.amount = a.payingTax;
                                        break;
                                    case (int)CustomEnum.PayTaxperiodEnum.Quarterly:
                                        dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) / 3, 0);
                                        break;
                                    case (int)CustomEnum.PayTaxperiodEnum.Semiannually:
                                        dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) / 6, 0);
                                        break;
                                    case (int)CustomEnum.PayTaxperiodEnum.Annually:
                                        dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) / 12, 0);
                                        break;
                                    default:
                                        dbobj.amount = a.payingTax ?? 0;
                                        break;
                                }
                                dbobj.payingTax = a.payingTax;
                                dbobj.paytype = a.paytype;
                                dbobj.taxID = a.taxTypeID;
                                dbobj.note = a.note;
                                // dbobj.amount = a.amount;
                                dbobj.isMouseHover = a.isMouseHover;
                                //  dbobj.isOther = false;
                                dbobj.updatedBy = usrID;
                                dbobj.updatedDate = System.DateTime.Now;
                                //dbobj.profileID = 0;
                                db.Entry(dbobj).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        else if (a.incomeID > 0)
                        {
                            dbobj = new IncomeTaxDetail();
                            dbobj.taxID = a.taxTypeID;
                            dbobj.incomeID = a.incomeID;
                            dbobj.note = a.note ?? "";
                            //dbobj.amount = a.amount;
                            dbobj.isMouseHover = a.isMouseHover;
                            dbobj.isOther = false;
                            dbobj.isActive = true;
                            dbobj.updatedBy = usrID;
                            dbobj.createdDate = System.DateTime.Now;
                            dbobj.profileID = 0;
                            switch (dbobj.paytype)
                            {
                                case (int)CustomEnum.PayTaxperiodEnum.Monthly:
                                    dbobj.amount = a.payingTax;
                                    break;
                                case (int)CustomEnum.PayTaxperiodEnum.Quarterly:
                                    dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) / 3, 0);
                                    break;
                                case (int)CustomEnum.PayTaxperiodEnum.Semiannually:
                                    dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) / 6, 0);
                                    break;
                                case (int)CustomEnum.PayTaxperiodEnum.Annually:
                                    dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) / 12, 0);
                                    break;
                                default:
                                    dbobj.amount = a.payingTax ?? 0;
                                    break;
                            }
                            dbobj.payingTax = a.payingTax ?? 0;
                            dbobj.paytype = a.paytype;
                            db.IncomeTaxDetails.Add(dbobj);
                            db.SaveChanges();
                            inObj[i].taxDetailID = dbobj.taxDetailID;
                        }
                        i++;
                    }

                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return inObj;
        } // future tax
        public List<IncomeTaxDetailModel> savepayrollTaxeDetails(List<IncomeTaxDetailModel> inObj, long usrID) //spouse & primary-- weekly
        {
            IncomeTaxDetail dbobj = new IncomeTaxDetail();
            using (var db = Connection.getConnect())
            {
                try
                {
                    int i = 0;
                    foreach (var a in inObj)
                    {
                        if (a.taxDetailID > 0 && a != null)
                        {
                            dbobj = db.IncomeTaxDetails.Where(b => b.taxDetailID == a.taxDetailID && b.isActive == true && b.isOther == false).SingleOrDefault();
                            if (dbobj != null && dbobj.taxDetailID > 0)
                            {

                                switch (dbobj.paytype)
                                {
                                    case (int)CustomEnum.PayperiodEnum.Monthly:
                                        dbobj.amount = a.payingTax;
                                        break;
                                    case (int)CustomEnum.PayperiodEnum.Semimonthly:
                                        dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) * 2, 0);
                                        break;
                                    case (int)CustomEnum.PayperiodEnum.Weekly:
                                        dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) * 52 / 12, 0);
                                        break;
                                    case (int)CustomEnum.PayperiodEnum.Biweekly:
                                        dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) * 26 / 12, 0);
                                        break;
                                    default:
                                        dbobj.amount = a.payingTax ?? 0;
                                        break;
                                }                               
                                dbobj.payingTax = a.payingTax;
                                dbobj.paytype = a.paytype;
                                dbobj.taxID = a.taxTypeID;
                                dbobj.note = a.note;
                                // dbobj.amount = a.amount;
                                dbobj.isMouseHover = a.isMouseHover;
                                //  dbobj.isOther = false;
                                dbobj.updatedBy = usrID;
                                dbobj.updatedDate = System.DateTime.Now;
                                //dbobj.profileID = 0;
                                db.Entry(dbobj).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        else if (a.incomeID > 0)
                        {
                            dbobj = new IncomeTaxDetail();
                            dbobj.taxID = a.taxTypeID;
                            dbobj.incomeID = a.incomeID;
                            dbobj.note = a.note ?? "";
                            //dbobj.amount = a.amount;
                            dbobj.isMouseHover = a.isMouseHover;
                            dbobj.isOther = false;
                            dbobj.isActive = true;
                            dbobj.updatedBy = usrID;
                            dbobj.createdDate = System.DateTime.Now;
                            dbobj.profileID = 0;
                            switch (dbobj.paytype)
                            {
                                case (int)CustomEnum.PayperiodEnum.Monthly:
                                    dbobj.amount = a.payingTax;
                                    break;
                                case (int)CustomEnum.PayperiodEnum.Semimonthly:
                                    dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) * 2, 0);
                                    break;
                                case (int)CustomEnum.PayperiodEnum.Weekly:
                                    dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) * 52 / 12, 0);
                                    break;
                                case (int)CustomEnum.PayperiodEnum.Biweekly:
                                    dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) * 26 / 12, 0);
                                    break;
                                default:
                                    dbobj.amount = a.payingTax ?? 0;
                                    break;
                            }
                            dbobj.payingTax = a.payingTax ?? 0;
                            dbobj.paytype = a.paytype;
                            db.IncomeTaxDetails.Add(dbobj);
                            db.SaveChanges();
                            inObj[i].taxDetailID = dbobj.taxDetailID;
                        }
                        i++;
                    }

                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return inObj;
        }
        //other tax save
        public List<IncomeTaxDetailModel> saveOtherTaxeDetails(List<IncomeTaxDetailModel> inObj, long usrID)
        {
            IncomeTaxDetail dbobj = new IncomeTaxDetail();
            using (var db = Connection.getConnect())
            {
                try
                {
                    int i = 0;
                    foreach (var a in inObj)
                    {
                        if (a.taxDetailID > 0 && a != null)
                        {
                            dbobj = db.IncomeTaxDetails.Where(b => b.taxDetailID == a.taxDetailID && b.isActive == true && b.isOther == true).SingleOrDefault();
                            if (dbobj != null && dbobj.taxDetailID > 0)
                            {
                                switch (dbobj.paytype)
                                {
                                    case (int)CustomEnum.PayTaxperiodEnum.Monthly:
                                        dbobj.amount = a.payingTax;
                                        break;
                                    case (int)CustomEnum.PayTaxperiodEnum.Quarterly:
                                        dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) / 3, 0);
                                        break;
                                    case (int)CustomEnum.PayTaxperiodEnum.Semiannually:
                                        dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) / 6, 0);
                                        break;
                                    case (int)CustomEnum.PayTaxperiodEnum.Annually:
                                        dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) / 12, 0);
                                        break;
                                    default:
                                        dbobj.amount = a.payingTax ?? 0;
                                        break;
                                }
                                dbobj.taxID = a.taxTypeID;
                                dbobj.payingTax = a.payingTax;
                                dbobj.paytype = a.paytype;
                                dbobj.note = a.note ?? "";
                                //dbobj.amount = a.amount;
                                dbobj.isMouseHover = a.isMouseHover;
                                dbobj.isOther = true;
                                dbobj.updatedBy = usrID;
                                dbobj.updatedDate = System.DateTime.Now;
                                db.Entry(dbobj).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        else if (a.profileID > 0)
                        {
                            dbobj = new IncomeTaxDetail();
                            switch (dbobj.paytype)
                            {
                                case (int)CustomEnum.PayTaxperiodEnum.Monthly:
                                    dbobj.amount = a.payingTax;
                                    break;
                                case (int)CustomEnum.PayTaxperiodEnum.Quarterly:
                                    dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) / 3, 0);
                                    break;
                                case (int)CustomEnum.PayTaxperiodEnum.Semiannually:
                                    dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) / 6, 0);
                                    break;
                                case (int)CustomEnum.PayTaxperiodEnum.Annually:
                                    dbobj.amount = Math.Round(Convert.ToDecimal(a.payingTax) / 12, 0);
                                    break;
                                default:
                                    dbobj.amount = a.payingTax ?? 0;
                                    break;
                            }

                            dbobj.taxID = a.taxTypeID;
                            dbobj.incomeID = 0;
                            dbobj.note = a.note ?? "";
                            dbobj.payingTax = a.payingTax;
                            dbobj.paytype = a.paytype;
                            // dbobj.amount = a.amount;
                            dbobj.isMouseHover = a.isMouseHover;
                            dbobj.isOther = true;
                            dbobj.isActive = true;
                            dbobj.updatedBy = usrID;
                            dbobj.createdDate = System.DateTime.Now;
                            dbobj.profileID = a.profileID;
                            db.IncomeTaxDetails.Add(dbobj);
                            db.SaveChanges();
                            inObj[i].taxDetailID = dbobj.taxDetailID;
                        }
                        i++;
                    }


                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return inObj;
        }



        public bool deleteTax(long deleteID, long usrID)//common
        {
            IncomeTaxDetail dbobj = new IncomeTaxDetail();
            using (var db = Connection.getConnect())
            {
                try
                {
                    dbobj = db.IncomeTaxDetails.Where(b => b.taxDetailID == deleteID && b.isActive == true).SingleOrDefault();
                    if (dbobj != null && dbobj.taxDetailID > 0)
                    {
                        dbobj.isActive = false;
                        dbobj.updatedBy = usrID;
                        dbobj.updatedDate = System.DateTime.Now;
                        db.Entry(dbobj).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return true;
        }

        public bool dedeleteFutureincome(long deleteID, long usrID)//future income
        {
            Income dbobj = new Income();
            using (var db = Connection.getConnect())
            {
                try
                {
                    dbobj = db.Incomes.Where(b => b.incomeID == deleteID && b.isActive == true && b.isFutureIncome == true).SingleOrDefault();
                    if (dbobj != null && dbobj.incomeID > 0)
                    {
                        dbobj.isActive = false;
                        dbobj.incomeModifiedBy = usrID;
                        dbobj.incomeUpdateDate = System.DateTime.Now;
                        db.Entry(dbobj).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return true;
        }




    }
}
