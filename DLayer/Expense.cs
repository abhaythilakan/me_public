﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CLayer;
using CLayer.DBModel;

namespace DLayer
{
    public class Expense
    {
        public LivingExpenseModel GetLivingExpense(long id)
        {
            LivingExpenseModel model = new LivingExpenseModel();
            try
            {
                using (var db = Connection.getConnect())
                {
                    model.expenseList = db.LivingExpenses.Where(x => x.profileId == id && x.isActive == true).ToList()
                        .Select(x => new ExpenseModel
                        {
                            ExpenseId = x.expenseId,
                            profileID = x.profileId.Value,
                            amount = x.amount,
                            company = x.company,
                            CreatedDate = x.CreatedDate,
                            expenseCategoryId = x.expenseCategoryId,
                            expSubCategoryId = x.expSubCategoryId,
                            ModifiedBy = x.ModifiedBy,
                            ModifiedDate = x.ModifiedDate,
                            notes = x.notes,
                            typeId = x.typeId,
                            showInMouseOver = x.showInMouseOver
                        }).ToList();
                    if (model.expenseList == null || model.expenseList.Count == 0)
                    {
                        model.expenseList.Add(new ExpenseModel() { profileID = id, expenseCategoryId = 0, ExpenseId = 0, typeId = 0 });
                    }
                    model.totalExpense = model.expenseList.Sum(x => x.amount);
                    model.homeExpense = model.expenseList.Where(x => x.expenseCategoryId == 1).ToList().Sum(y => y.amount);
                    model.nonHomeExpense = model.expenseList.Where(x => x.expenseCategoryId != 1).ToList().Sum(y => y.amount);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }
        public LivingExpenseModel SaveLivingExpense(List<ExpenseModel> inputList, long userId)
        {
            LivingExpenseModel model = new LivingExpenseModel();
            try
            {
                LivingExpense expense = new LivingExpense();
                using (var db = Connection.getConnect())
                {
                    foreach (var input in inputList)
                    {

                        expense = db.LivingExpenses.FirstOrDefault(b => b.profileId == input.profileID && b.expenseId == input.ExpenseId && b.isActive == true);
                        if (expense != null && expense.expenseId > 0)
                        {
                            expense.company = input.company;
                            expense.amount = input.amount;
                            expense.expenseCategoryId = input.expenseCategoryId;
                            expense.expSubCategoryId = input.expSubCategoryId;
                            expense.notes = input.notes;
                            expense.profileId = input.profileID;
                            expense.typeId = input.typeId;
                            expense.ModifiedDate = DateTime.Now;
                            expense.ModifiedBy = userId;
                            expense.showInMouseOver = input.showInMouseOver;
                            db.Entry(expense).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }
                        else
                        {
                            expense = new LivingExpense
                            {
                                company = input.company,
                                amount = input.amount,
                                expenseCategoryId = input.expenseCategoryId,
                                expSubCategoryId = input.expSubCategoryId,
                                notes = input.notes,
                                profileId = input.profileID,
                                typeId = input.typeId,
                                showInMouseOver = input.showInMouseOver,
                                ModifiedDate = DateTime.Now,
                                ModifiedBy = userId,
                                CreatedDate = DateTime.Now,
                                isActive = true,
                            };
                            db.LivingExpenses.Add(expense);
                            db.SaveChanges();

                        }
                    }
                    return GetLivingExpense(inputList.FirstOrDefault().profileID);
                }
                return model;
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public LivingExpenseModel DeleteLivingExpense(ExpenseInputModel input, long userId)
        {
            LivingExpenseModel model = new LivingExpenseModel();
            try
            {
                LivingExpense expense = new LivingExpense();
                using (var db = Connection.getConnect())
                {
                    expense = db.LivingExpenses.FirstOrDefault(b => b.profileId == input.profileId && b.expenseId == input.expenseId && b.isActive == true);
                    if (expense != null)
                    {
                        expense.isActive = false;
                        expense.ModifiedDate = DateTime.Now;
                        expense.ModifiedBy = userId;
                        db.Entry(expense).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        model = GetLivingExpense(input.profileId);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }

        public ExpenseListModel GetAllExpenseList(long profileId)
        {
            ExpenseListModel list = new ExpenseListModel();
            list.insuranceExpList = GetInsuranceList(profileId);
            list.deptExpList = GetDeptList(profileId);
            list.incomeExpList = GetIncomeList(profileId);
            list.taxExpList = GetTaxList(profileId);
            list.savExpList = GetSavingList(profileId);
            return list;
        }

        public InsurancePremiumModel GetInsuranceList(long profileId)
        {
            InsurancePremiumModel model = new InsurancePremiumModel();
            try
            {
                using (var db = Connection.getConnect())
                {
                    List<IndividualInsurance> insuranceList = new List<IndividualInsurance>();
                    var insurance = db.lifeInsurances.Where(x => x.profileID == profileId && x.isActive == true).ToList();
                    var personInfo = db.Persons.Where(x => x.profileID == profileId && x.isActive==true).ToList();
                    var childInfo = db.ChildrenInfoes.Where(x => x.profileID == profileId && x.isActive == true).ToList();
                    var otherInsurance = db.OtherInsurances.Where(x => x.profileID == profileId && x.isActive == true).ToList();
                    var otherIns = otherInsurance.Select(o => new IndividualInsurance
                    {
                        InsuranceCategory = "Other",
                        company = o.company,
                        insuranceType = o.lifeTypeName
                    }).ToList();
                    insuranceList.AddRange(otherIns);
                    var personIns = (from i in insurance
                                     join p in personInfo on i.personID equals p.personID
                                     select new IndividualInsurance
                                     {
                                         InsuranceCategory = "Individual",
                                         company = i.company,
                                         currentCashValue = i.amount,
                                         faceValue = i.faceValue,
                                         insuranceType = i.lifeTypeName,
                                         name = p.firstName
                                     }).ToList();
                    insuranceList.AddRange(personIns);
                    var childIns = (from i in insurance
                                    join p in childInfo on i.childID equals p.childID
                                    select new IndividualInsurance
                                    {
                                        InsuranceCategory = "Individual",
                                        company = i.company,
                                        currentCashValue = i.amount,
                                        faceValue = i.faceValue,
                                        insuranceType = i.lifeTypeName,
                                        name = p.fName
                                    }).ToList();
                    insuranceList.AddRange(childIns);
                    model.insuranceList = insuranceList;
                    model.faceValueSum = insuranceList.Sum(x => x.faceValue);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }
        public DeptExpenseModel GetDeptList(long profileId)
        {
            DeptExpenseModel model = new DeptExpenseModel();
            try
            {
                List<DeptExpenseList> deptList = new List<DeptExpenseList>();
                using (var db = Connection.getConnect())
                {
                    var deptTypes = db.DebtTypes.ToList();
                    var dept = db.Debts.Where(x => x.profileID == profileId && x.isActive == true).ToList();
                    model.deptList = dept.Select(y => new DeptExpenseList
                    {
                        deptCatType = db.DebtCats.Where(x => x.debtCatId == y.debtCatId).FirstOrDefault().catName,
                        deptType = deptTypes.Where(x => x.debtTypeId == y.debtTypeID).FirstOrDefault().debtTypeName,
                        creditorName = y.creditor,
                        balance = y.amount,
                        interestRate = y.rate,
                        balanceAsOf = y.Balanceasof.Value.ToString("MMM dd yyyy"),
                        principleInterestRate = y.actualPay
                    }).ToList();
                    model.totalPay = model.deptList.Sum(x => x.principleInterestRate).Value;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }

        private IncomeExpenseOutput GetIncomeList(long profileId)
        {
            IncomeExpenseOutput model = new IncomeExpenseOutput();
            try
            {
                List<IncomeExpenseModel> expenseList = new List<IncomeExpenseModel>();
                using (var db = Connection.getConnect())
                {
                    var income = db.Incomes.Where(x => x.profileID == profileId && x.isActive == true).ToList();
                    var personList = db.Persons.Where(x => x.profileID == profileId && x.isActive==true).ToList();
                    var primaryIncome = income.Where(x => x.isFutureIncome == false).GroupBy(g => g.personID, (id, grp) =>
                                           new IncomeExpenseModel
                                           {
                                               name = personList.Where(x => x.personID == id).FirstOrDefault().firstName + "'s Employment Income",
                                               totalGrossAmount = grp.Sum(x => x.grossMonthlyIncome),
                                               incomeList = grp.Select(y => new IncomeExpenseList
                                               {
                                                   grossAmount = y.grossMonthlyIncome,
                                                   incomeName = "Gross Monthly Income"
                                               }).ToList()
                                           });

                    var otherIncome = income.Where(x => x.isFutureIncome == true).GroupBy(g => g.personID, (id, grp) =>
                       new IncomeExpenseModel
                       {
                           name = "Other Current Income",
                           totalGrossAmount = grp.Sum(x => x.grossMonthlyIncome),
                           incomeList = grp.Select(y => new IncomeExpenseList
                           {
                               grossAmount = y.grossMonthlyIncome,
                               incomeName = "Other Income"
                           }).ToList()
                       });
                    if (personList.Count > 0)
                        expenseList = primaryIncome.Concat(otherIncome).ToList();
                    model.incomeOutput = expenseList;
                    model.total = expenseList.Sum(x => x.totalGrossAmount);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }

        private TaxExpenseOutput GetTaxList(long profileId)
        {
            TaxExpenseOutput model = new TaxExpenseOutput();
            try
            {
                using (var db = Connection.getConnect())
                {
                    var income = db.Incomes.Where(x => x.profileID == profileId && x.isActive == true).ToList();
                    var incomeDetails = db.IncomeTaxDetails.Where(x => x.isActive == true).ToList();
                    var incomeTaxes = db.IncomeTaxes.Where(x => x.isActive == true).ToList();
                    //var incomeTaxTypes = db.IncomeTaxTypes.Where(x => x.IsActive == true).ToList();
                    var personList = db.Persons.Where(x => x.profileID == profileId && x.isActive==true).ToList();

                    model.expenseOutput = income.Join(incomeDetails.Where(x => x.isOther == false), i => i.incomeID, d => d.incomeID, (i, d) => new { id = i.personID, name = i.name, isOther = d.isOther, d = d }).Distinct().ToList()
                          .Union(income.Join(incomeDetails.Where(x => x.isOther == true), i => i.profileID, d => d.profileID, (i, d) => new { id = i.profileID, name = "", isOther = d.isOther, d = d }).Distinct().ToList())
                          .GroupBy(g => g.isOther, (id, grp) =>
                           new TaxExpenseModel
                           {
                               name = (id == false) ? "Income" : "Other",
                               totalTax = grp.Sum(x => x.d.amount),
                               incomeList = grp.Select(y => new TaxExpenseList
                               {
                                   taxName = (id == false) ? personList.Where(x => x.personID == y.id).FirstOrDefault().firstName + "'s " + incomeTaxes.Where(x => x.taxID == y.d.taxID).FirstOrDefault().taxName :
                                                     y.name + " " + incomeTaxes.Where(x => x.taxID == y.d.taxID).FirstOrDefault().taxName,
                                   taxAmount = y.d.amount
                               }).Distinct().ToList()
                           }).Distinct().ToList();
                    model.total = model.expenseOutput.Sum(x => x.totalTax);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return model;
        }

        private SavingsExpenseOutput GetSavingList(long profileId)
        {
            SavingsExpenseOutput savingsModel = new SavingsExpenseOutput();
            try
            {
                List<SavingsExpenseModel> savingsList = new List<SavingsExpenseModel>();
                using (var db = Connection.getConnect())
                {
                    List<SavingsExpenseModel> model = new List<SavingsExpenseModel>();
                    var educationList = db.Educations.Where(x => x.profileID == profileId && x.isActive == true).ToList();
                    var eduType = db.Education_Type.ToList();
                    var eduDetails = db.Education_Details.ToList();

                    var education = educationList.GroupBy(g => g.profileID, (id, grp) =>
                         new SavingsExpenseModel
                         {
                             name = "Education",
                             totalSaving = grp.Sum(x => x.monthlySavings),
                             savingList = grp.Select(x => new SavingsExpenseList
                             {
                                 SavingName = eduDetails.Any(y => y.educationID == x.educationID) ? x.fName + "'s " + eduType.Where(y => y.EduTypeID == (eduDetails.Where(e => e.educationID == x.educationID).FirstOrDefault().educationTypeID))?.FirstOrDefault().EduType : x.fName,
                                 SavingAmount = (x.monthlySavings != null) ? x.monthlySavings : 0
                             }).ToList()
                         }).SingleOrDefault();
                    if (education != null)
                        model.Add(education);


                    var retirementAsset = db.Asset_Retirement.Where(x => x.profileID == profileId && x.isActive == true).ToList();
                    var retirementAssetDetails = db.Asset_RetirementDetails.Where(x => x.profileID == profileId && x.isActive == true).ToList();
                    var nonRetirementAsset = db.Asset_NonRetirement.Where(x => x.profileID == profileId && x.isActive == true && x.assetCatID!= 5 && x.assetCatID != 4).ToList();
                    var assetTypes = db.AssetTypes.ToList();


                    var nonRetirement = nonRetirementAsset.GroupBy(g => g.profileID, (id, grp) =>
                        new SavingsExpenseModel
                        {
                            name = "Non Retirement Savings",
                            totalSaving = grp.Sum(x => x.monthlyContribution),
                            savingList = grp.Select(x => new SavingsExpenseList
                            {
                                SavingAmount = (x.monthlyContribution != null) ? x.monthlyContribution : 0,
                                SavingName = assetTypes.Where(y => y.assetTypeID == x.assetTypeID).FirstOrDefault().typeName + " : " + x.company_location
                            }).ToList()
                        }).SingleOrDefault();
                    if (nonRetirement != null)
                        model.Add(nonRetirement);
                    var retirement = retirementAsset.Join(retirementAssetDetails, r => r.assetID, d => d.assetID, (r, d) => new { name = r.name, d=d }).ToList()
                        .Union(retirementAssetDetails.Where(x => x.isOtherRetirement == true).Select(d => new {name= d.company, d=d })).ToList()
                      .GroupBy(g => g.d.profileID, (id, grp) =>
                       new SavingsExpenseModel
                       {
                           name = "Retirement",
                           totalSaving = grp.Sum(x => x.d.monthlyContribution),
                           savingList = grp.Select(y => new SavingsExpenseList
                           {
                               SavingAmount = (y.d.monthlyContribution != null) ? y.d.monthlyContribution : 0,
                               SavingName = y.name + " : " + assetTypes.Where(x => x.assetTypeID == y.d.assetTypeID).FirstOrDefault().typeName
                           }).ToList()
                       }).SingleOrDefault();
                    if (retirement != null)
                        model.Add(retirement);
                    savingsModel.savingOutput = model;
                    savingsModel.total = model.Sum(x => x.totalSaving);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return savingsModel;
        }
    }
}
