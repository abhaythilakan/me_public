﻿using CLayer;
using System;
using CLayer.common;
using CLayer.DBModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Globalization;

namespace DLayer
{
    public class PersonalInformation
    {
        //save person to income table
        private bool savePersonDetailsToIncome(Person objp, int usrType)
        {
            Income inObj = new Income();
            string name = " ";
            using (var db = Connection.getConnect())
            {
                try
                {
                    switch (usrType)
                    {
                        case (int)CustomEnum.userTypeEnum.primary:
                            name = objp.firstName + " (P)";
                            break;
                        case (int)CustomEnum.userTypeEnum.spouse:
                            name = objp.firstName + " (S)";
                            break;
                    }
                    if (objp != null && objp.profileID > 0 && objp.personID > 0)
                    {
                        inObj = db.Incomes.SingleOrDefault(b => b.personID == objp.personID && b.profileID == objp.profileID);
                        if (inObj != null && inObj.incomeID > 0)
                        {
                            if (inObj.name != name)
                            {
                                inObj.name = name;
                                inObj.isActive = true;
                                inObj.incomeUpdateDate = System.DateTime.Now;
                                inObj.incomeModifiedBy = objp.personModifiedBy;
                                inObj.isFutureIncome = false;
                                db.Entry(inObj).State = System.Data.Entity.EntityState.Modified;
                            }
                        }
                        else
                        {
                            inObj = new Income();
                            inObj.name = name;
                            inObj.isActive = true;
                            inObj.isFutureIncome = false;
                            inObj.profileID = objp.profileID;
                            inObj.personID = objp.personID;
                            inObj.totalTaxeAmount = 0;
                            inObj.incomeCreatedDate = System.DateTime.Now;
                            inObj.incomeModifiedBy = objp.personModifiedBy;
                            db.Incomes.Add(inObj);
                        }
                        db.SaveChanges();
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            return true;
        }
        //primary and spouse personal info save & update
        public PersonModel savePersonalInfo(PersonModel primaryInfo, int usrType, long usrId)
        {

            using (var db = Connection.getConnect())
            {
                //using (var transaction = db.Database.BeginTransaction(IsolationLevel.Serializable))
                //{
                Person p = new Person();
                try
                {
                    if (primaryInfo != null)
                    {
                        switch (usrType)
                        {
                            case (int)CustomEnum.userTypeEnum.primary:
                                primaryInfo.isPrimary = true;
                                break;
                            case (int)CustomEnum.userTypeEnum.spouse:
                                primaryInfo.isPrimary = false;
                                break;
                        }

                    }
                    else { return null; }

                    if (primaryInfo.personID > 0)
                    {
                        p = db.Persons.SingleOrDefault(b => b.personID == primaryInfo.personID);
                        if (p != null && p.personID > 0)
                        {
                            p = fillpersonalInfo(p, primaryInfo);
                            p.personUpdatedDate = System.DateTime.Now;
                            p.personModifiedBy = usrId;
                            db.Entry(p).State = System.Data.Entity.EntityState.Modified;
                        }
                    }
                    else if (primaryInfo.profileID > 0)
                    {
                        p = new Person();
                        p = fillpersonalInfo(p, primaryInfo);
                        p.personCreatedDate = System.DateTime.Now;
                        p.personModifiedBy = usrId;
                        db.Persons.Add(p);
                    }
                    db.SaveChanges();

                    if (true == savePersonDetailsToEducation(p, db))
                    {
                        db.SaveChanges();
                    }
                    if (true == savePersonDetailsToIncome(p, usrType))
                    {

                    }
                    if (true == savePersonDetailsToRetirementAsset(p, usrType))
                    {

                    }
                    //db.SaveChanges();
                    //transaction.Commit();
                    primaryInfo.personID = p.personID;
                    primaryInfo.Age = p.Age;
                }
                catch (Exception ex)
                {
                    //  transaction.Rollback();
                    throw;
                }

                //}
                return primaryInfo;

            }
        }

        private bool savePersonDetailsToRetirementAsset(Person objp, int usrType)
        {
            Asset_Retirement inObj = new Asset_Retirement();
            string name = " ";
            using (var db = Connection.getConnect())
            {
                try
                {
                    switch (usrType)
                    {
                        case (int)CustomEnum.userTypeEnum.primary:
                            name = objp.firstName + " -(P)";
                            break;
                        case (int)CustomEnum.userTypeEnum.spouse:
                            name = objp.firstName + " -(S)";
                            break;
                    }
                    if (objp != null && objp.profileID > 0 && objp.personID > 0)
                    {
                        inObj = db.Asset_Retirement.SingleOrDefault(b => b.personID == objp.personID && b.profileID == objp.profileID);
                        if (inObj != null && inObj.personID > 0)
                        {
                            if (inObj.name != name || inObj.retAge != objp.retirementAge || inObj.lifeEcpectancy != objp.lifeExpectancy)
                            {
                                inObj.name = name;
                                inObj.isActive = true;
                                inObj.retAge = objp.retirementAge;
                                inObj.lifeEcpectancy = objp.lifeExpectancy;
                                inObj.updatedDate = System.DateTime.Now;
                                inObj.updatedBy = objp.personModifiedBy;
                                db.Entry(inObj).State = System.Data.Entity.EntityState.Modified;
                            }

                        }
                        else
                        {
                            inObj = new Asset_Retirement();
                            inObj.name = name;
                            inObj.isActive = true;
                            inObj.retAge = objp.retirementAge ?? 60;
                            inObj.lifeEcpectancy = objp.lifeExpectancy ?? 90;
                            inObj.totalAmount = 0;
                            inObj.createdDate = System.DateTime.Now;
                            inObj.profileID = objp.profileID;
                            inObj.personID = objp.personID;
                            db.Asset_Retirement.Add(inObj);
                        }
                        db.SaveChanges();

                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            return true;
        }


        //primary contact save
        public Person_ContactInfoModel savePrimaryContactInfo(Person_ContactInfoModel cont, long usrId)
        {

            using (var db = Connection.getConnect())
            {

                Person_ContactInfo c = new Person_ContactInfo();
                try
                {


                    if (cont.personInfoID > 0)
                    {
                        c = db.Person_ContactInfo.SingleOrDefault(b => b.personInfoID == cont.personInfoID);
                        if (c != null && c.personInfoID > 0)
                        {
                            c = fillpersoncontactInfo(c, cont);
                            c.contactUpdatedDate = System.DateTime.Now;
                            c.contactModifiedBy = usrId;
                            db.Entry(c).State = System.Data.Entity.EntityState.Modified;
                        }
                    }
                    else if (cont.profileID > 0)
                    {
                        c = new Person_ContactInfo();
                        c = fillpersoncontactInfo(c, cont);
                        c.contactCreatedDate = System.DateTime.Now;
                        c.contactModifiedBy = usrId;
                        db.Person_ContactInfo.Add(c);
                    }

                    db.SaveChanges();
                    cont.personInfoID = c.personInfoID;

                }
                catch (Exception ex)
                {

                    throw;
                }


                return cont;
            }
        }
        //Primary and spouse educational Information save & update
        public Person_EmployerInfoModel saveemploymentInfo(Person_EmployerInfoModel emplInfo, int usrType, long usrID)
        {
            using (var db = Connection.getConnect())
            {

                Person_EmployerInfo emp = new Person_EmployerInfo();
                try
                {
                    if (emplInfo != null)
                    {
                        switch (usrType)
                        {
                            case (int)CustomEnum.userTypeEnum.primary:
                                emplInfo.isPrimary = true;
                                break;
                            case (int)CustomEnum.userTypeEnum.spouse:
                                emplInfo.isPrimary = false;
                                break;
                        }

                    }
                    else { return null; }

                    if (emplInfo.empID != null && emplInfo.empID != null && emplInfo.empID > 0)
                    {
                        emp = db.Person_EmployerInfo.SingleOrDefault(b => b.empID == emplInfo.empID);
                        if (emp != null && emp.empID > 0)
                        {
                            emp = fillemployerInfo(emp, emplInfo);
                            emp.employerUpdatedDate = System.DateTime.Now;
                            emp.employerModifiedBy = usrID;
                            db.Entry(emp).State = System.Data.Entity.EntityState.Modified;
                        }
                    }
                    else if (emplInfo.profileID > 0)
                    {
                        emp = new Person_EmployerInfo();
                        emp = fillemployerInfo(emp, emplInfo);

                        emp.employerCreatedDate = System.DateTime.Now;
                        emp.employerModifiedBy = usrID;
                        db.Person_EmployerInfo.Add(emp);

                    }
                    db.SaveChanges();
                    emplInfo.empID = emp.empID;
                }
                catch (Exception ex)
                {
                    throw;
                }
                return emplInfo;
            }
        }
        //save child information
        public List<ChildrenInfoModel> saveChildInfo(List<ChildrenInfoModel> ChildList, long usrID)
        {
            int i = -1;
            ChildrenInfo child = new ChildrenInfo();
            using (var db = Connection.getConnect())
            {

                if (ChildList != null)
                {
                    foreach (var item in ChildList)
                    {
                        i++;
                        if (item.childID > 0)  // save
                        {
                            child = db.ChildrenInfoes.SingleOrDefault(b => b.childID == item.childID && b.isActive == true);

                            if (child != null && child.childID > 0)
                            {
                                child = fillchild(child, item);
                                child.childUpdatedDate = System.DateTime.Now;
                                child.childModifiedBy = usrID;
                                db.Entry(child).State = System.Data.Entity.EntityState.Modified;
                            }

                        }
                        else if (item.profileID > 0) // update
                        {
                            child = new ChildrenInfo();
                            child = fillchild(child, item);
                            child.childCreatedDate = System.DateTime.Now;
                            child.childModifiedBy = usrID;
                            child.isActive = true;
                            db.ChildrenInfoes.Add(child);
                        }
                        db.SaveChanges();
                        if (true == saveChildDetailsToEducation(child, db, usrID))
                        {

                        }

                        ChildList[i].childID = child.childID;


                    }
                    return ChildList;
                }
            }
            return null;
        }
        //get all personal Details based on profile ID
        public PersonalInformationModel GetPersonalInfoall(long id)
        {
            PersonalInformationModel objP = new PersonalInformationModel();
            try
            {
                using (var db = Connection.getConnect())
                {
                    if (id > 0)
                    {
                        objP.profileID = id;
                        objP.primaryInfo = getPersonalInfo(id, (int)CustomEnum.userTypeEnum.primary); //primary info
                        objP.primaryContactInfo = getPrimaryContact(id); //primary Contact
                        objP.primaryEmployementInfo = getEmployerInfo(id, (int)CustomEnum.userTypeEnum.spouse);//primary Emp info
                        objP.spouseInfo = getPersonalInfo(id, (int)CustomEnum.userTypeEnum.spouse);//spouse personal Info
                        objP.spouseEmployementInfo = getEmployerInfo(id, (int)CustomEnum.userTypeEnum.spouse); //spouse Emp Info
                        objP.childrenInfo = getChildinfo(id);// Children information

                    }
                    else
                    {
                        return null;
                    }

                }

            }
            catch (Exception ex)
            {
                throw;
            }

            return objP;
        }
        // delete child 
        public bool deleteChild(childDelete obj, long usrID)
        {
            if (obj.childID > 0 && obj.profileID > 0)
            {
                using (var db = Connection.getConnect())
                    try
                    {
                        var childvalue = db.ChildrenInfoes.FirstOrDefault(s => s.childID == obj.childID && s.profileID == obj.profileID);
                        if (childvalue != null && childvalue.childID > 0)
                        {
                            childvalue.isActive = false;
                            childvalue.includeEduPage = false;
                            childvalue.childUpdatedDate = System.DateTime.Now;
                            childvalue.childModifiedBy = usrID;
                            db.Entry(childvalue).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                            var edu = db.Educations.FirstOrDefault(s => s.childID == obj.childID && s.profileID == obj.profileID);
                            if (edu != null && edu.childID > 0)
                            {
                                edu.isActive = false;
                                edu.educationUpdatedDate = System.DateTime.Now;
                                edu.educationModifiedBy = usrID;
                                db.Entry(childvalue).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                return true;
            }
            return false;
        }
        //get info on page load
        #region  Get Details
        public Person_EmployerInfoModel getEmployerInfo(long id, int usrType)
        {
            bool b = false;
            Person_EmployerInfoModel emp = new Person_EmployerInfoModel();
            Person_EmployerInfo p = new Person_EmployerInfo();
            switch (usrType)
            {
                case (int)CustomEnum.userTypeEnum.primary:
                    b = true;
                    break;
                case (int)CustomEnum.userTypeEnum.spouse:
                    b = false;
                    break;
            }

            using (var db = Connection.getConnect())
            {
                try
                {
                    if (id > 0)
                    {
                        p = db.Person_EmployerInfo.SingleOrDefault(e => e.profileID == id && e.isPrimary == b);
                        if (p != null && p.empID > 0)
                        {
                            emp = fillemployerInfodbtomodel(p);

                        }
                        else
                        {
                            emp = new Person_EmployerInfoModel()
                            {
                                profileID = id
                            };

                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return emp;
        }
        public Person_ContactInfoModel getPrimaryContact(long id)
        {
            Person_ContactInfoModel pc = new Person_ContactInfoModel();
            Person_ContactInfo pcDB = new Person_ContactInfo();
            using (var db = Connection.getConnect())
            {
                try
                {
                    if (id > 0)
                    {
                        pcDB = db.Person_ContactInfo.SingleOrDefault(e => e.profileID == id);
                        if (pcDB != null && pcDB.personInfoID > 0)
                        {
                            pc = fillpersonalInfodbtomodel(pcDB);
                        }
                        else
                        {
                            pc = new Person_ContactInfoModel()
                            {
                                profileID = id
                            };

                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return pc;



        }
        public PersonModel getPersonalInfo(long id, int usrType)
        {
            bool b = false;
            PersonModel pm = new PersonModel();
            Person p = new Person();
            switch (usrType)
            {
                case (int)CustomEnum.userTypeEnum.primary:
                    b = true;
                    break;
                case (int)CustomEnum.userTypeEnum.spouse:
                    b = false;
                    break;
            }

            using (var db = Connection.getConnect())
            {
                try
                {
                    if (id > 0)
                    {
                        p = db.Persons.SingleOrDefault(e => e.profileID == id && e.isPrimary == b);
                        if (p != null && p.personID > 0)
                        {
                            pm = fillpersondbtomodel(p);
                        }
                        else
                        {
                            DefaultsRetirement dr = new DefaultsRetirement();
                            dr = db.DefaultsRetirements.SingleOrDefault(e => e.profileID == id);
                            if (dr != null)
                            {
                                pm = new PersonModel();
                                pm.profileID = id;
                                pm.retirementAge = dr.retireAge ?? 60;
                                pm.lifeExpectancy = dr.lifeExpectency ?? 90;
                            }
                            else
                            {

                                pm = new PersonModel();
                                pm.profileID = id;
                                pm.retirementAge = 60;
                                pm.lifeExpectancy = 90;
                                pm.birthDate = System.DateTime.Now;

                            }

                        }
                    }
                    else
                    {
                        return null;
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return pm;
        }
        public List<ChildrenInfoModel> getChildinfo(long id)
        {
            List<ChildrenInfoModel> child = new List<ChildrenInfoModel>();
            List<ChildrenInfo> cObj = new List<ChildrenInfo>();

            using (var db = Connection.getConnect())
            {
                try
                {
                    if (id > 0)
                    {
                        cObj = db.ChildrenInfoes.Where(e => e.profileID == id && e.isActive == true).ToList();
                        if (cObj != null && cObj.Count != 0)
                        {
                            foreach (var item in cObj)
                            {
                                if (item.childID > 0)  // save
                                {
                                    child.Add(fillchilddbtomodel(item));
                                }
                            }
                        }
                        else
                        {
                            child = null;
                            //child.Add(new ChildrenInfoModel { age=null,childID= null, dob=null,fName=null,inHouseHold=true,isMale=true,Lname=null,midName=null,profileID=id,ssn=null,includeEduPage=true});
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            return child;
        }
        #endregion Get Details
        // class model to db model
        #region Fill model object to db object
        private ChildrenInfo fillchild(ChildrenInfo c, ChildrenInfoModel item)
        {
            DateTime zeroTime = new DateTime(1, 1, 1);
            DateTime a;
            DateTime b;
            a = (DateTime)item.dob;
            b = System.DateTime.Now;
            TimeSpan span = b - a;
            decimal years = 0;
            try
            {
                years = Convert.ToDecimal((zeroTime + span).Year - 1);
            }
            catch (Exception ex)
            {
                throw new Exception("Date of birth must be less than current date");

            }

            c.age = years;
            c.childID = item.childID.Value;
            c.profileID = item.profileID;
            c.dob = item.dob;
            c.fName = item.fName;
            c.Lname = item.Lname;
            c.includeEduPage = item.includeEduPage ?? false;
            c.inHouseHold = item.inHouseHold;
            c.isMale = item.isMale;
            c.midName = item.midName;
            //c.ssn = item.ssn;
            c.age = years;


            return c;
        }
        private Person_EmployerInfo fillemployerInfo(Person_EmployerInfo emp, Person_EmployerInfoModel emplInfo)
        {

            emp.profileID = emplInfo.profileID;
            emp.ocuupation = emplInfo.ocuupation;
            emp.employer = emplInfo.employer;
            emp.yearInocuupation = emplInfo.yearInocuupation;
            emp.yearAtEmployer = emplInfo.yearAtEmployer;
            emp.addressL1 = emplInfo.addressL1;
            emp.addressL2 = emplInfo.addressL2;
            emp.city = emplInfo.city;
            emp.state = emplInfo.state;
            emp.zip = emplInfo.zip;
            emp.isPrimary = emplInfo.isPrimary;

            return emp;
        }
        private Person fillpersonalInfo(Person p, PersonModel primaryInfo)
        {
            DateTime zeroTime = new DateTime(1, 1, 1);
            DateTime a;
            DateTime b;
            a = (DateTime)primaryInfo.birthDate;
            b = System.DateTime.Now;
            TimeSpan span = b - a;
            decimal years = 0;
            try
            {
                years = Convert.ToDecimal((zeroTime + span).Year - 1);
            }
            catch (Exception ex)
            {
                throw new Exception("Date of birth must be less than current date");

            }

            p.profileID = primaryInfo.profileID;
            p.firstName = primaryInfo.firstName;
            p.midName = primaryInfo.midName;
            p.lastName = primaryInfo.lastName;
            p.isPrimary = primaryInfo.isPrimary ?? true;
            p.isMale = primaryInfo.isMale;
            p.birthDate = primaryInfo.birthDate;
            p.Age = years;
            p.retirementAge = primaryInfo.retirementAge;
            p.lifeExpectancy = primaryInfo.lifeExpectancy;
            p.emailID = primaryInfo.emailID;
            p.includeEduPage = primaryInfo.inclEducPage ?? false;
            return p;
        }
        private Person_ContactInfo fillpersoncontactInfo(Person_ContactInfo obj, Person_ContactInfoModel cont)
        {
            obj.profileID = cont.profileID;
            obj.homePhone = cont.homePhone;
            obj.mobilePhone = cont.mobilePhone;
            obj.addressL1 = cont.addressL1;
            obj.addressL2 = cont.addressL2;
            obj.city = cont.city;
            obj.state = cont.state;
            obj.zip = cont.zip;

            return obj;

        }
        #endregion fill
        // db model to class model
        #region fill db object to model
        private Person_EmployerInfoModel fillemployerInfodbtomodel(Person_EmployerInfo p)
        {
            Person_EmployerInfoModel emp = new Person_EmployerInfoModel()
            {
                addressL1 = p.addressL1,
                addressL2 = p.addressL2,
                city = p.city,
                empID = p.empID,
                employer = p.employer,
                isPrimary = p.isPrimary,
                ocuupation = p.ocuupation,
                profileID = p.profileID,
                state = p.state,
                yearAtEmployer = p.yearAtEmployer,
                yearInocuupation = p.yearInocuupation,
                zip = p.zip

            };
            return emp;
        }
        private PersonModel fillpersondbtomodel(Person p)
        {
            DateTime zeroTime = new DateTime(1, 1, 1);
            DateTime a;
            DateTime b;
            a = (DateTime)p.birthDate;
            b = System.DateTime.Now;
            TimeSpan span = b - a;
            decimal years = Convert.ToDecimal((zeroTime + span).Year - 1);
            PersonModel pm = new PersonModel()
            {
                Age = years,
                birthDate = p.birthDate,
                retirementAge = p.retirementAge,
                profileID = p.profileID,
                midName = p.midName,
                isMale = p.isMale,
                emailID = p.emailID,
                firstName = p.firstName,
                lastName = p.lastName,
                inclEducPage = p.includeEduPage,
                isPrimary = p.isPrimary,
                lifeExpectancy = p.lifeExpectancy,
                personID = p.personID
            };
            return pm;
        }
        private Person_ContactInfoModel fillpersonalInfodbtomodel(Person_ContactInfo pcDB)
        {
            Person_ContactInfoModel pc = new Person_ContactInfoModel()
            {
                addressL1 = pcDB.addressL1,
                addressL2 = pcDB.addressL2,
                city = pcDB.city,
                homePhone = pcDB.homePhone,
                mobilePhone = pcDB.mobilePhone,
                state = pcDB.state,
                zip = pcDB.zip,
                profileID = pcDB.profileID,
                personInfoID = pcDB.personInfoID,
            };
            return pc;
        }
        private ChildrenInfoModel fillchilddbtomodel(ChildrenInfo child)
        {
            DateTime zeroTime = new DateTime(1, 1, 1);
            DateTime a;
            DateTime b;
            a = (DateTime)child.dob;
            b = System.DateTime.Now;
            TimeSpan span = b - a;
            decimal years = Convert.ToDecimal((zeroTime + span).Year - 1);

            ChildrenInfoModel obj = new ChildrenInfoModel()
            {
                age = years,
                childID = child.childID,
                dob = child.dob,
                fName = child.fName,
                isMale = child.isMale,
                Lname = child.Lname,
                includeEduPage = child.includeEduPage,
                inHouseHold = child.inHouseHold,
                midName = child.midName,
                // ssn = child.ssn,
                profileID = child.profileID


            };
            return obj;
        }

        #endregion fill db object to model
        //save education details to education table
        #region save education details to education table
        public bool savePersonDetailsToEducation(Person objp, ME_v2Entities db)
        {
            List<Education> edu = new List<Education>();
            Education e = new Education();
            if (objp != null && objp.profileID > 0 && objp.personID > 0)
            {
                edu = db.Educations.Where(b => b.personID == objp.personID && b.profileID == objp.profileID).ToList();

                if (edu.Count > 0)
                {
                    foreach (var ed in edu)
                    {
                        if (ed != null && ed.educationID > 0)
                        {
                            if (ed.fName != (objp.firstName + " " + objp.lastName) || ed.dob != objp.birthDate || ed.isActive != objp.includeEduPage)
                            {
                                ed.fName = objp.firstName + " " + objp.lastName;
                                ed.dob = objp.birthDate;
                                ed.isActive = objp.includeEduPage;
                                ed.educationUpdatedDate = System.DateTime.Now;
                                ed.educationModifiedBy = objp.personModifiedBy;
                                db.Entry(ed).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }
                }
                else if (objp.includeEduPage == true)
                {
                    e = new Education();
                    e.fName = objp.firstName + " " + objp.lastName;
                    e.dob = objp.birthDate;
                    e.isActive = true;
                    e.IsParent = true;
                    e.isSavingsRequired = false;
                    e.personID = objp.personID;
                    e.profileID = objp.profileID;
                    e.educationCreatedDate = System.DateTime.Now;
                    e.educationModifiedBy = objp.personModifiedBy;
                    db.Educations.Add(e);
                    db.SaveChanges();
                }
            }
            else
            {
                return false;
            }
            
            return true;
        }
        public bool saveChildDetailsToEducation(ChildrenInfo objp, ME_v2Entities db, long userID)
        {
            List<Education> edu = new List<Education>();
            Education e = new Education();
            if (objp != null && objp.profileID > 0)
            {
                edu = db.Educations.Where(b => b.childID == objp.childID).ToList();
                if (edu.Count > 0)
                {
                    foreach (var ed in edu)
                    {
                        if (ed != null && ed.educationID > 0)
                        {
                            if (ed.fName != (objp.fName + " " + objp.Lname) || ed.dob != objp.dob || ed.isActive != objp.includeEduPage)
                            {
                                ed.fName = objp.fName + " " + objp.Lname;
                                ed.dob = objp.dob;
                                ed.isActive = objp.includeEduPage;
                                ed.educationUpdatedDate = System.DateTime.Now;
                                ed.educationModifiedBy = userID;
                                db.Entry(ed).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }
                    }
                else if (objp.includeEduPage == true)
                    {
                    e = new Education();
                    e.fName = objp.fName + " " + objp.Lname;
                    e.dob = objp.dob;
                    e.isActive = true;
                    e.IsParent = false;
                    e.isSavingsRequired = false;
                    e.childID = objp.childID;
                    e.profileID = objp.profileID ?? 0;
                    e.educationCreatedDate = System.DateTime.Now;
                    e.educationModifiedBy = userID;
                    db.Educations.Add(e);
                    db.SaveChanges();
                }

            }
            else
            {
                return false;
            }
            return true;
        }
        #endregion save education details to education table

    }

}
