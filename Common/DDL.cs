﻿using DLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class DDL
    {
        public List<DropDownList_String> getDropDownList(enumDropDownList ddlType)
        {
            List<DropDownList_String> ddl = new List<DropDownList_String>();
            try
            {
                using (var db = Connection.getConnect())
                {
                    if (ddlType == enumDropDownList.State)
                    {
                        ddl = db.States.Where(b => b.stateCode != null).Select(b => new CLayer.common.DropDownList_String() { VALUE = b.stateCode, TEXT = b.stateName.ToString() }).ToList();
                    }
                    else if (ddlType == enumDropDownList.Advisor)
                    {
                        ddl = db.Advisors.Where(b => b.isActive == true).Select(b => new CLayer.common.DropDownList_String() { VALUE = b.advisorId.ToString(), TEXT = b.firstName.ToString() + " " + b.lastName.ToString() }).ToList();
                    }
                    else if (ddlType == enumDropDownList.TimeZones)
                    {
                        ddl = db.GlobalTimeZones.Select(b => new CLayer.common.DropDownList_String() { VALUE = b.timeZoneID.ToString(), TEXT = b.location.ToString() + " " + b.type.ToString() + " " + b.time.ToString() + " " + b.shortlocation.ToString() }).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return ddl;
        }
    }

    public class DropDownList_Int
    {
        public long ID { get; set; }
        public string TEXT { get; set; }
    }
    public class DropDownList_String
    {
        public string VALUE { get; set; }
        public string TEXT { get; set; }
    }

    public enum enumDropDownList
    {
        State = 1,
        Advisor = 2,
        Client = 3,
        TimeZones = 4,
        Currencies = 5
    }
}
