﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;

[assembly: OwinStartup(typeof(ME.Startup))]

namespace ME
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //ConfigureAuth(app);
            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
            var customProvider = new Common.CustomAuthorizationProvider();
            OAuthAuthorizationServerOptions options = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true, //?
                TokenEndpointPath = new PathString("/GenerateToken"), // Call - http://localhost:59132/GenerateToken with body(x-www-form-urlencoded) - username,password,grant-type(password)
                AccessTokenExpireTimeSpan = TimeSpan.FromDays(5),
                //AuthenticationType= OAuthDefaults.AuthenticationType.
                Provider = customProvider,
                AuthenticationMode = Microsoft.Owin.Security.AuthenticationMode.Active
                
            };
            app.UseOAuthAuthorizationServer(options); //?
            app.UseCookieAuthentication(new Microsoft.Owin.Security.Cookies.CookieAuthenticationOptions());
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions()); //?


            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config); //?
        }
    }
}
