﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CLayer.DBModel;
using CLayer;
using CLayer.common;

namespace ME.Controllers
{
    public class FAQsController : Controller
    {
        private ME_v2Entities db = new ME_v2Entities();

        public void fillDBtoModel(FAQ dbT, ref FAQModel mdT)
        {
            mdT.faq_Id = dbT.faq_Id;
            mdT.faq_Que = dbT.faq_Que;
            mdT.faq_Ans = dbT.faq_Ans;
            mdT.faq_SectionID = dbT.faq_SectionID;
            mdT.faq_SectionName = getEnumCaption(Convert.ToInt32(dbT.faq_SectionID));
            mdT.createdDT = dbT.createdDT;
            mdT.modifyDT = dbT.modifyDT;
            mdT.modifyBy = dbT.modifyBy;
            mdT.isActive = dbT.isActive;
        }
        public void fillModeltoDB(FAQModel mdT, ref FAQ dbT)
        {
            dbT.faq_Id = mdT.faq_Id;
            dbT.faq_Que = mdT.faq_Que;
            dbT.faq_Ans = mdT.faq_Ans;
            dbT.faq_SectionID = mdT.faq_SectionID;
            dbT.isActive = true;
            if (dbT.faq_Id > 0)
            {
                dbT.modifyDT = DateTime.Now;
                dbT.createdDT = mdT.createdDT;
            }
            else
                dbT.createdDT = DateTime.Now;
            mdT.modifyBy = dbT.modifyBy;
        }

        public string getEnumCaption(int sectionID)
        {
            if (sectionID == (int)CLayer.common.CustomEnum.FAQsection.TopSupportQuestions) return CLayer.common.CustomEnum.FAQsection.TopSupportQuestions.ToCaption();
            else if (sectionID == (int)CLayer.common.CustomEnum.FAQsection.Billing) return CLayer.common.CustomEnum.FAQsection.Billing.ToCaption();
            else if (sectionID == (int)CLayer.common.CustomEnum.FAQsection.TechnicalAssistance) return CLayer.common.CustomEnum.FAQsection.TechnicalAssistance.ToCaption();
            else if (sectionID == (int)CLayer.common.CustomEnum.FAQsection.Security) return CLayer.common.CustomEnum.FAQsection.Security.ToCaption();
            else if (sectionID == (int)CLayer.common.CustomEnum.FAQsection.AdvisorLogIn) return CLayer.common.CustomEnum.FAQsection.AdvisorLogIn.ToCaption();
            else if (sectionID == (int)CLayer.common.CustomEnum.FAQsection.MultipleAdvisorSubscriptions) return CLayer.common.CustomEnum.FAQsection.MultipleAdvisorSubscriptions.ToCaption();
            else if (sectionID == (int)CLayer.common.CustomEnum.FAQsection.ClientReports) return CLayer.common.CustomEnum.FAQsection.ClientReports.ToCaption();
            else return "";
        }

        // GET: FAQs
        public ActionResult Index()
        {
            return View(db.FAQs.Where(b => b.isActive == true).AsEnumerable().Select(b => new FAQModel()
            {
                faq_Id = b.faq_Id,
                faq_Que = b.faq_Que,
                faq_Ans = b.faq_Ans,
                faq_SectionID = b.faq_SectionID,
                faq_SectionName = getEnumCaption(Convert.ToInt32(b.faq_SectionID)).ToString(),
                createdDT = b.createdDT,
                modifyDT = b.modifyDT,
                modifyBy = b.modifyBy,
                isActive = b.isActive
            }).ToList());
        }

        // GET: FAQs/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FAQ fAQ = db.FAQs.Find(id);
            if (fAQ == null)
            {
                return HttpNotFound();
            }
            FAQModel mdl = new FAQModel();
            fillDBtoModel(fAQ, ref mdl);
            return View(mdl);
        }

        // GET: FAQs/Create
        public ActionResult Create()
        {
            FAQModel mdl = new FAQModel();
            return View(mdl);
        }

        // POST: FAQs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FAQModel mdl)
        {
            if (ModelState.IsValid)
            {
                FAQ fAQ = new FAQ();
                fillModeltoDB(mdl, ref fAQ);
                db.FAQs.Add(fAQ);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mdl);
        }

        // GET: FAQs/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FAQ fAQ = db.FAQs.Find(id);
            if (fAQ == null)
            {
                return HttpNotFound();
            }
            FAQModel mdl = new FAQModel();
            fillDBtoModel(fAQ, ref mdl);
            return View(mdl);
        }

        // POST: FAQs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(FAQModel mdl)
        {
            if (ModelState.IsValid)
            {
                FAQ fAQ = new FAQ();
                fillModeltoDB(mdl, ref fAQ);
                db.Entry(fAQ).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mdl);
        }

        // GET: FAQs/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            FAQ fAQ = db.FAQs.Find(id);
            if (fAQ == null)
            {
                return HttpNotFound();
            }
            FAQModel mdl = new FAQModel();
            fillDBtoModel(fAQ, ref mdl);
            return View(mdl);
        }

        // POST: FAQs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            FAQ fAQ = db.FAQs.Find(id);
            //db.FAQs.Remove(fAQ);
            fAQ.isActive = false;
            db.Entry(fAQ).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
