﻿using CLayer;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ME.Controllers
{
    public class NonRetirementTypeController : Controller
    {
        // GET: NonRetirementType
        private ME_v2Entities db = new ME_v2Entities();

        public void fillDBtoModel(AssetType dbT, ref AssetTypesModel mdT)
        {
            mdT.assetTypeID = dbT.assetTypeID;
            mdT.assetCatID = (int)CLayer.common.CustomEnum.AssetTypesEnum.Nonretirementinvestment;
            mdT.assetCatName = getAssetCatName((int)CLayer.common.CustomEnum.AssetTypesEnum.Nonretirementinvestment);
            mdT.typeName = dbT.typeName;
            mdT.sortOrder = dbT.sortOrder;
            mdT.createdDate = dbT.createdDate;
            mdT.isActive = dbT.isActive;
            mdT.isTaxed = dbT.isTaxed ?? false;
            mdT.updatedBy = dbT.updatedBy;
            mdT.updatedDate = dbT.updatedDate;
        }
        public void fillModeltoDB(AssetTypesModel mdT, ref AssetType dbT)
        {
            dbT.assetTypeID = mdT.assetTypeID;
            dbT.assetCatID = (int)CLayer.common.CustomEnum.AssetTypesEnum.Nonretirementinvestment;
            dbT.typeName = mdT.typeName;
            dbT.sortOrder = mdT.sortOrder;
            dbT.isActive = mdT.isActive;
            dbT.isTaxed = mdT.isTaxed;
            if (dbT.assetTypeID > 0)
            {
                dbT.updatedDate = DateTime.Now;
                dbT.createdDate = mdT.createdDate;
            }
            else
                dbT.createdDate = DateTime.Now;
            mdT.updatedBy = dbT.updatedBy;
        }

        public ActionResult Index()
        {
            List<AssetTypesModel> model = new List<AssetTypesModel>();
            try
            {
                model = (from at in db.AssetTypes
                         join ac in db.AssetCategories on at.assetCatID equals ac.catID
                         where at.isActive == true && ac.catID == (int)CLayer.common.CustomEnum.AssetTypesEnum.Nonretirementinvestment
                         select (new AssetTypesModel()
                         {
                             assetTypeID = at.assetTypeID,
                             assetCatID = at.assetCatID,
                             assetCatName = ac.catName,
                             typeName = at.typeName,
                             sortOrder = at.sortOrder,
                             createdDate = at.createdDate,
                             isActive = at.isActive,
                             isTaxed = at.isTaxed ?? false,
                             updatedBy = at.updatedBy,
                             updatedDate = at.updatedDate
                         })).ToList();
                //model = db.AssetTypes.Where(b => b.isActive == true).Select(b => new AssetTypesModel()
                //{
                //    assetTypeID = b.assetTypeID,
                //    assetCatID = b.assetCatID,
                //    assetCatName = db.AssetCategories.Where(c => c.catID == b.assetCatID).FirstOrDefault().catName,
                //    typeName = b.typeName,
                //    createdDate = b.createdDate,
                //    isActive = b.isActive,
                //    isTaxed = b.isTaxed ?? false,
                //    updatedBy = b.updatedBy,
                //    updatedDate = b.updatedDate
                //}).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }

        public string getAssetCatName(int catID)
        {
            string Name = "";
            try
            {
                if (catID > 0)
                    Name = db.AssetCategories.Single(b => b.catID == catID).catName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Name;
        }

        // GET: NonRetirementType/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssetTypesModel model = new AssetTypesModel();
            try
            {
                AssetType assetType = db.AssetTypes.Find(id);
                if (assetType == null)
                {
                    return HttpNotFound();
                }
                fillDBtoModel(assetType, ref model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }

        // GET: NonRetirementType/Create
        public ActionResult Create()
        {
            AssetTypesModel model = new AssetTypesModel();
            model.assetCatID = (int)CLayer.common.CustomEnum.AssetTypesEnum.Nonretirementinvestment;
            return View(model);
        }

        // POST: NonRetirementType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AssetTypesModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.createdDate = DateTime.Now;
                    model.updatedDate = DateTime.Now;
                    model.updatedBy = 0;
                    model.isActive = true;
                    AssetType assetType = new AssetType();
                    fillModeltoDB(model, ref assetType);
                    db.AssetTypes.Add(assetType);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(model);
        }

        // GET: NonRetirementType/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssetTypesModel model = new AssetTypesModel();
            try
            {
                AssetType assetType = db.AssetTypes.Find(id);
                if (assetType == null)
                {
                    return HttpNotFound();
                }
                fillDBtoModel(assetType, ref model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }

        // POST: NonRetirementType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AssetTypesModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.updatedDate = DateTime.Now;
                    model.updatedBy = 0;
                    AssetType assetType = new AssetType();
                    fillModeltoDB(model, ref assetType);
                    db.Entry(assetType).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }

        // GET: NonRetirementType/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssetTypesModel model = new AssetTypesModel();
            try
            {
                AssetType assetType = db.AssetTypes.Find(id);
                if (assetType == null)
                {
                    return HttpNotFound();
                }
                fillDBtoModel(assetType, ref model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }

        // POST: NonRetirementType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                AssetType assetType = db.AssetTypes.Find(id);
                assetType.isActive = false;
                assetType.updatedDate = DateTime.Now;
                assetType.updatedBy = 0;
                db.Entry(assetType).State = EntityState.Modified;
                //db.AssetTypes.Remove(assetType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}