﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CLayer.DBModel;
using CLayer;

namespace ME.Controllers
{
    public class CouponCodesController : Controller
    {
        private ME_v2Entities db = new ME_v2Entities();

        public void fillDBtoModel(CouponCode dbT, ref CouponCodeModel mdT)
        {
            mdT.cc_Id = dbT.cc_Id;
            mdT.cc_Code = dbT.cc_Code;
            mdT.sp_Id = Convert.ToInt32(dbT.sp_Id);
            mdT.cc_Details = dbT.cc_Details;
            mdT.createdDate = dbT.createdDate;
            mdT.modifyDate = dbT.modifyDate;
            mdT.modifyBy = dbT.modifyBy;
            mdT.cc_isActive = true;
            mdT.sp_Name = getSalesPersonName(dbT.sp_Id);
        }
        public void fillModeltoDB(CouponCodeModel mdT, ref CouponCode dbT)
        {
            dbT.cc_Id = mdT.cc_Id;
            dbT.cc_Code = mdT.cc_Code;
            dbT.sp_Id = Convert.ToInt32(mdT.sp_Id);
            dbT.cc_Details = mdT.cc_Details;
            dbT.cc_isActive = true;
            if (mdT.cc_Id > 0)
            {
                dbT.modifyDate = DateTime.Now;
                dbT.createdDate = mdT.createdDate;
            }
            else
                dbT.createdDate = DateTime.Now;
            dbT.modifyBy = mdT.modifyBy;
        }

        public void fillDBtoModel(CouponCodeDetail dbT, ref CouponCodeDetailModel mdT)
        {
            mdT.ccd_Id = dbT.ccd_Id;
            mdT.cc_Id = dbT.cc_Id;
            mdT.ccd_Title = dbT.ccd_Title;
            mdT.ccd_initSetupFee = dbT.ccd_initSetupFee;
            mdT.ccd_initMonthFee = dbT.ccd_initMonthFee;
            mdT.ccd_initPeriodMonth = dbT.ccd_initPeriodMonth;
            mdT.ccd_RenewalMonthlyFee = dbT.ccd_RenewalMonthlyFee;
            mdT.ccd_MultiUMonthDiscount = dbT.ccd_MultiUMonthDiscount;
            mdT.ccd_Commision = dbT.ccd_Commision;
            mdT.createdDate = dbT.createdDate;
            mdT.modifyDate = dbT.modifyDate;
            mdT.modifyBy = dbT.modifyBy;
        }
        public void fillModeltoDB(CouponCodeDetailModel mdT, ref CouponCodeDetail dbT)
        {
            dbT.ccd_Id = mdT.ccd_Id;
            dbT.cc_Id = mdT.cc_Id;
            dbT.ccd_Title = mdT.ccd_Title;
            dbT.ccd_initSetupFee = mdT.ccd_initSetupFee;
            dbT.ccd_initMonthFee = mdT.ccd_initMonthFee;
            dbT.ccd_initPeriodMonth = mdT.ccd_initPeriodMonth;
            dbT.ccd_RenewalMonthlyFee = mdT.ccd_RenewalMonthlyFee;
            dbT.ccd_MultiUMonthDiscount = mdT.ccd_MultiUMonthDiscount;
            dbT.ccd_Commision = mdT.ccd_Commision;
            if (mdT.ccd_Id > 0)
            {
                dbT.modifyDate = DateTime.Now;
                dbT.createdDate = mdT.createdDate;
            }
            else
                dbT.createdDate = DateTime.Now;
            dbT.modifyBy = mdT.modifyBy;
        }

        public string getSalesPersonName(int? sp_Id)
        {
            string name = "";
            try
            {
                if (sp_Id > 0)
                {
                    var tbl = db.SalesPersons.Single(b => b.sp_Id == sp_Id);
                    name = ((tbl.sp_FName ?? "") + " " + (tbl.sp_LName ?? "")).ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return name;
        }


        // GET: CouponCodes
        public ActionResult Index()
        {
            return View((from b in db.CouponCodes
                         join sp in db.SalesPersons on b.sp_Id equals sp.sp_Id
                         where b.cc_isActive == true && sp.sp_isActive == true
                         select new CouponCodeModel()
                         {
                             cc_Id = b.cc_Id,
                             cc_Code = b.cc_Code,
                             sp_Id = b.sp_Id ?? 0,
                             cc_Details = b.cc_Details,
                             createdDate = b.createdDate,
                             modifyDate = b.modifyDate,
                             modifyBy = b.modifyBy,
                             cc_isActive = b.cc_isActive,
                             sp_Name = sp.sp_FName ?? "" + " " + sp.sp_LName ?? ""
                         }));
            //return View(db.CouponCodes.Join() .Where(b=>b.cc_isActive == true).Select(b=> new CouponCodeModel() {
            //    cc_Id = b.cc_Id,
            //    cc_Code = b.cc_Code,
            //    sp_Id = b.sp_Id ?? 0,
            //    cc_Details = b.cc_Details,
            //    createdDate = b.createdDate,
            //    modifyDate = b.modifyDate,
            //    modifyBy = b.modifyBy,
            //    cc_isActive = b.cc_isActive,
            //    sp_Name = ""
            //}));
        }

        // GET: CouponCodes/Details/5
        public ActionResult Details(int? id)
        {
            CouponCodeModel mdl = new CouponCodeModel();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CouponCode couponCode = db.CouponCodes.Find(id);
            if (couponCode == null)
            {
                return HttpNotFound();
            }
            fillDBtoModel(couponCode, ref mdl);
            mdl.CouponCodeDetails = getCouponCodeDetails(id, mdl.CouponCodeDetails);
            return View(mdl);
        }

        // GET: CouponCodes/Create
        public ActionResult Create()
        {
            CouponCodeModel mdl = new CouponCodeModel();
            mdl.CouponCodeDetails = new List<CouponCodeDetailModel>();
            mdl.CouponCodeDetails.Add(new CouponCodeDetailModel());
            return View(mdl);
        }

        public void saveCouponCodeDetails(List<CouponCodeDetailModel> ccd)
        {
            List<CouponCodeDetail> lis = new List<CouponCodeDetail>();
            for (int ii = 0; ii < ccd.Count(); ii++)
            {
                int ccd_Id = ccd[ii].ccd_Id;
                CouponCodeDetail dnT = ccd[ii].ccd_Id > 0 ? db.CouponCodeDetails.Single(b => b.ccd_Id == ccd_Id) : null;
                if (dnT != null)
                {
                    fillModeltoDB(ccd[ii], ref dnT);
                    if (ccd[ii].ccd_Id > 0)
                        db.Entry(dnT).State = EntityState.Modified;
                    else
                        lis.Add(dnT);
                }
            }
            if (lis != null && lis.Count() > 0)
            {
                db.CouponCodeDetails.AddRange(lis);
            }
            db.SaveChanges();
        }
        public List<CouponCodeDetailModel> getCouponCodeDetails(int? cc_Id, List<CouponCodeDetailModel> lis)
        {
            if (cc_Id > 0)
            {
                lis = lis ?? new List<CouponCodeDetailModel>();
                lis = db.CouponCodeDetails.Where(b => b.cc_Id == cc_Id).Select(b => new CouponCodeDetailModel()
                {
                    ccd_Id = b.ccd_Id,
                    cc_Id = b.cc_Id,
                    ccd_initSetupFee = b.ccd_initSetupFee,
                    ccd_initMonthFee = b.ccd_initMonthFee,
                    ccd_initPeriodMonth = b.ccd_initPeriodMonth,
                    ccd_RenewalMonthlyFee = b.ccd_RenewalMonthlyFee,
                    ccd_MultiUMonthDiscount = b.ccd_MultiUMonthDiscount,
                    ccd_Commision = b.ccd_Commision,
                    ccd_Title = b.ccd_Title,
                    createdDate = b.createdDate,
                    modifyBy = b.modifyBy,
                    modifyDate = b.modifyDate
                }).ToList();
            }
            return lis;
        }

        // POST: CouponCodes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CouponCodeModel mdl)
        {
            if (ModelState.IsValid)
            {
                CouponCode couponCode = new CouponCode();
                fillModeltoDB(mdl, ref couponCode);
                db.CouponCodes.Add(couponCode);
                db.SaveChanges();
                if (couponCode.cc_Id > 0)
                {
                    mdl.CouponCodeDetails.Select(b => { b.cc_Id = couponCode.cc_Id; return b; }).ToList();
                    saveCouponCodeDetails(mdl.CouponCodeDetails);
                    //List<CouponCodeDetail> lis = new List<CouponCodeDetail>();
                    //for (int ii = 0; ii < mdl.CouponCodeDetails.Count(); ii++)
                    //{
                    //    CouponCodeDetail dnT = new CouponCodeDetail();
                    //    fillModeltoDB(mdl.CouponCodeDetails[ii], ref dnT);
                    //    lis.Add(dnT);
                    //}
                    //db.CouponCodeDetails.AddRange(lis);
                    //db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            //if(mdl.CouponCodeDetails == null)
            //{
            //    mdl.CouponCodeDetails = new List<CouponCodeDetailModel>();
            //    mdl.CouponCodeDetails.Add(new CouponCodeDetailModel());
            //}
            return View(mdl);
        }

        // GET: CouponCodes/Edit/5
        public ActionResult Edit(int? id)
        {
            CouponCodeModel mdl = new CouponCodeModel();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CouponCode couponCode = db.CouponCodes.Find(id);
            if (couponCode == null)
            {
                return HttpNotFound();
            }
            fillDBtoModel(couponCode, ref mdl);
            mdl.CouponCodeDetails = getCouponCodeDetails(id, mdl.CouponCodeDetails);
            if (mdl.CouponCodeDetails == null)
            {
                mdl.CouponCodeDetails = new List<CouponCodeDetailModel>();
                mdl.CouponCodeDetails.Add(new CouponCodeDetailModel());
            }
            return View(mdl);
        }

        // POST: CouponCodes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(CouponCodeModel mdl)
        {
            if (ModelState.IsValid)
            {
                CouponCode couponCode = new CouponCode();
                fillModeltoDB(mdl, ref couponCode);
                db.Entry(couponCode).State = EntityState.Modified;
                db.SaveChanges();
                if (couponCode.cc_Id > 0)
                {
                    mdl.CouponCodeDetails.Select(b => { b.cc_Id = couponCode.cc_Id; return b; }).ToList();
                    saveCouponCodeDetails(mdl.CouponCodeDetails);
                }
                return RedirectToAction("Index");
            }
            return View(mdl);
        }

        // GET: CouponCodes/Delete/5
        public ActionResult Delete(int? id)
        {
            CouponCodeModel mdl = new CouponCodeModel();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CouponCode couponCode = db.CouponCodes.Find(id);
            if (couponCode == null)
            {
                return HttpNotFound();
            }
            fillDBtoModel(couponCode, ref mdl);
            mdl.CouponCodeDetails = getCouponCodeDetails(id, mdl.CouponCodeDetails);
            return View(mdl);
        }

        // POST: CouponCodes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CouponCode couponCode = db.CouponCodes.Find(id);
            couponCode.modifyDate = DateTime.Now;
            couponCode.cc_isActive = false;
            couponCode.modifyBy = 0;
            db.Entry(couponCode).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
