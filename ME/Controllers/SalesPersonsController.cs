﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CLayer.DBModel;
using CLayer;

namespace ME.Controllers
{
    public class SalesPersonsController : Controller
    {
        private ME_v2Entities db = new ME_v2Entities();

        public void fillDBtoModel(SalesPerson dbT, ref SalesPersonModel mdT)
        {
            mdT.sp_Id = dbT.sp_Id;
            mdT.sp_FName = dbT.sp_FName;
            mdT.sp_LName = dbT.sp_LName;
            mdT.sp_Username = dbT.sp_Username;
            mdT.sp_Password = dbT.sp_Password;
            mdT.sp_isActive = dbT.sp_isActive;
            mdT.sp_Email = dbT.sp_Email;
            mdT.createdDate = dbT.createdDate;
            mdT.modifyDate = dbT.modifyDate;
            mdT.modifyBy = dbT.modifyBy;
        }
        public void fillModeltoDB(SalesPersonModel mdT, ref SalesPerson dbT)
        {
            dbT.sp_Id = mdT.sp_Id;
            dbT.sp_FName = mdT.sp_FName;
            dbT.sp_LName = mdT.sp_LName;
            dbT.sp_Username = mdT.sp_Username;
            dbT.sp_Password = mdT.sp_Password;
            dbT.sp_isActive = mdT.sp_isActive;
            dbT.sp_Email = mdT.sp_Email;
            if (mdT.sp_Id > 0)
                dbT.modifyDate = DateTime.Now;
            else
                dbT.createdDate = DateTime.Now;
            dbT.modifyBy = mdT.modifyBy;
        }

        // GET: SalesPersons
        public ActionResult Index()
        {
            return View(db.SalesPersons.Select(b => new SalesPersonModel()
            {
                sp_Id = b.sp_Id,
                sp_FName = b.sp_FName,
                sp_LName = b.sp_LName,
                sp_Username = b.sp_Username,
                sp_Password = b.sp_Password,
                sp_Email = b.sp_Email,
                sp_isActive = b.sp_isActive,
                createdDate = b.createdDate,
                modifyBy = b.modifyBy,
                modifyDate = b.modifyDate
            }));
        }

        // GET: SalesPersons/Details/5
        public ActionResult Details(int? id)
        {
            SalesPersonModel mdl = new SalesPersonModel();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalesPerson salesPerson = db.SalesPersons.Find(id);
            fillDBtoModel(salesPerson, ref mdl);
            if (salesPerson == null)
            {
                return HttpNotFound();
            }
            return View(mdl);
        }

        // GET: SalesPersons/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: SalesPersons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(SalesPersonModel mdl)
        {
            if (ModelState.IsValid)
            {
                SalesPerson salesPerson = new SalesPerson();
                fillModeltoDB(mdl, ref salesPerson);
                db.SalesPersons.Add(salesPerson);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mdl);
        }

        // GET: SalesPersons/Edit/5
        public ActionResult Edit(int? id)
        {
            SalesPersonModel mdl = new SalesPersonModel();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalesPerson salesPerson = db.SalesPersons.Find(id);
            fillDBtoModel(salesPerson, ref mdl);
            if (salesPerson == null)
            {
                return HttpNotFound();
            }
            return View(mdl);
        }

        // POST: SalesPersons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(SalesPersonModel mdl)
        {
            if (ModelState.IsValid)
            {
                SalesPerson salesPerson = new SalesPerson();
                fillModeltoDB(mdl, ref salesPerson);
                db.Entry(salesPerson).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mdl);
        }

        // GET: SalesPersons/Delete/5
        public ActionResult Delete(int? id)
        {
            SalesPersonModel mdl = new SalesPersonModel();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SalesPerson salesPerson = db.SalesPersons.Find(id);
            fillDBtoModel(salesPerson, ref mdl);
            if (salesPerson == null)
            {
                return HttpNotFound();
            }
            return View(mdl);
        }

        // POST: SalesPersons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SalesPerson salesPerson = db.SalesPersons.Find(id);
            salesPerson.sp_isActive = false;
            salesPerson.modifyDate = DateTime.Now;
            salesPerson.modifyBy = 0;
            db.Entry(salesPerson).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
