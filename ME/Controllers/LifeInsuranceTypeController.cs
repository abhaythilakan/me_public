﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CLayer;
using CLayer.DBModel;

namespace ME.Controllers
{
    public class LifeInsuranceTypeController : Controller
    {
        private ME_v2Entities db = new ME_v2Entities();

        public void fillDBtoModel(lifeType dbT, ref LifeInsuranceTypeModel mdT)
        {
            mdT.lifeTypeID = dbT.lifeTypeID;
            mdT.lifeTypeName = dbT.lifeTypeName;
            mdT.isGroup = dbT.isGroup;
            mdT.orderNum = dbT.orderNum;
            mdT.createdDT = dbT.createdDT;
            mdT.modifyDT = dbT.modifyDT;
            mdT.modifyBy = dbT.modifyBy;
            mdT.isActive = dbT.isActive;
        }
        public void fillModeltoDB(LifeInsuranceTypeModel mdT, ref lifeType dbT)
        {
            dbT.lifeTypeID = mdT.lifeTypeID;
            dbT.lifeTypeName = mdT.lifeTypeName;
            dbT.isGroup = mdT.isGroup;
            dbT.orderNum = mdT.orderNum;
            dbT.isActive = true;
            if (dbT.lifeTypeID > 0)
            {
                dbT.modifyDT = DateTime.Now;
                dbT.createdDT = mdT.createdDT;
            }
            else
                dbT.createdDT = DateTime.Now;
            mdT.modifyBy = dbT.modifyBy;
        }

        // GET: LifeInsuranceType
        public ActionResult Index()
        {
            return View(db.lifeTypes.Where(b => b.isActive == true).Select(b => new LifeInsuranceTypeModel()
            {
                lifeTypeID = b.lifeTypeID,
                lifeTypeName = b.lifeTypeName,
                isGroup = b.isGroup,
                orderNum = b.orderNum,
                createdDT = b.createdDT,
                modifyDT = b.modifyDT,
                modifyBy = b.modifyBy,
                isActive = b.isActive,
            }));
        }

        // GET: LifeInsuranceType/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lifeType lifeType = db.lifeTypes.Find(id);
            if (lifeType == null)
            {
                return HttpNotFound();
            }
            LifeInsuranceTypeModel mdl = new LifeInsuranceTypeModel();
            fillDBtoModel(lifeType, ref mdl);
            return View(mdl);
        }

        // GET: LifeInsuranceType/Create
        public ActionResult Create()
        {
            LifeInsuranceTypeModel mdl = new LifeInsuranceTypeModel();
            return View(mdl);
        }

        // POST: LifeInsuranceType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LifeInsuranceTypeModel mdl)
        {
            if (ModelState.IsValid)
            {
                lifeType lifeType = new lifeType();
                fillModeltoDB(mdl, ref lifeType);
                db.lifeTypes.Add(lifeType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mdl);
        }

        // GET: LifeInsuranceType/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lifeType lifeType = db.lifeTypes.Find(id);
            if (lifeType == null)
            {
                return HttpNotFound();
            }
            LifeInsuranceTypeModel mdl = new LifeInsuranceTypeModel();
            fillDBtoModel(lifeType, ref mdl);
            return View(mdl);
        }

        // POST: LifeInsuranceType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(LifeInsuranceTypeModel mdl)
        {
            if (ModelState.IsValid)
            {
                lifeType lifeType = new lifeType();
                fillModeltoDB(mdl, ref lifeType);
                db.Entry(lifeType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mdl);
        }

        // GET: LifeInsuranceType/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            lifeType lifeType = db.lifeTypes.Find(id);
            if (lifeType == null)
            {
                return HttpNotFound();
            }
            LifeInsuranceTypeModel mdl = new LifeInsuranceTypeModel();
            fillDBtoModel(lifeType, ref mdl);
            return View(mdl);
        }

        // POST: LifeInsuranceType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            lifeType lifeType = db.lifeTypes.Find(id);
            lifeType.isActive = true;
            db.Entry(lifeType).State = EntityState.Modified;
            //db.lifeTypes.Remove(lifeType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
