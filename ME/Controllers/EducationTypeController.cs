﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CLayer.DBModel;
using CLayer;

namespace ME.Controllers
{
    public class EducationTypeController : Controller
    {
        private ME_v2Entities db = new ME_v2Entities();

        // GET: EducationType

        public void fillDBtoModel(Education_Type dbT, ref EducationTypeModel mdT)
        {
            mdT.EduTypeID = dbT.EduTypeID;
            mdT.EduType = dbT.EduType;
            mdT.sortOrder = dbT.sortOrder;
            mdT.createdDT = dbT.createdDT;
            mdT.modifyDT = dbT.modifyDT;
            mdT.modifyBy = dbT.modifyBy;
            mdT.isActive = dbT.isActive;
        }
        public void fillModeltoDB(EducationTypeModel mdT, ref Education_Type dbT)
        {
            dbT.EduTypeID = mdT.EduTypeID;
            dbT.EduType = mdT.EduType;
            dbT.sortOrder = mdT.sortOrder;
            dbT.isActive = true;
            if (dbT.EduTypeID > 0)
            {
                dbT.modifyDT = DateTime.Now;
                dbT.createdDT = mdT.createdDT;
            }
            else
                dbT.createdDT = DateTime.Now;
            mdT.modifyBy = dbT.modifyBy;
        }
        public ActionResult Index()
        {
            return View(db.Education_Type.Where(b=>b.isActive == true).Select(b => new EducationTypeModel()
            {
                EduTypeID = b.EduTypeID,
                EduType = b.EduType,
                sortOrder = b.sortOrder,
                createdDT = b.createdDT,
                modifyDT = b.modifyDT,
                modifyBy = b.modifyBy,
                isActive = b.isActive,
            }));
        }

        // GET: EducationType/Details/5
        public ActionResult Details(long? id)
        {
            EducationTypeModel mdl = new EducationTypeModel();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Education_Type education_Type = db.Education_Type.Find(id);
            if (education_Type == null)
            {
                return HttpNotFound();
            }
            fillDBtoModel(education_Type, ref mdl);
            return View(mdl);
        }

        // GET: EducationType/Create
        public ActionResult Create()
        {
            EducationTypeModel mdl = new EducationTypeModel();
            return View(mdl);
        }

        // POST: EducationType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(EducationTypeModel mdl)
        {
            Education_Type education_Type = new Education_Type();
            if (ModelState.IsValid)
            {
                fillModeltoDB(mdl, ref education_Type);
                db.Education_Type.Add(education_Type);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mdl);
        }

        // GET: EducationType/Edit/5
        public ActionResult Edit(long? id)
        {
            EducationTypeModel mdl = new EducationTypeModel();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Education_Type education_Type = db.Education_Type.Find(id);
            if (education_Type == null)
            {
                return HttpNotFound();
            }
            fillDBtoModel(education_Type, ref mdl);
            return View(mdl);
        }

        // POST: EducationType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EducationTypeModel mdl)
        {
            Education_Type education_Type = new Education_Type();
            if (ModelState.IsValid)
            {
                fillModeltoDB(mdl, ref education_Type);
                db.Entry(education_Type).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mdl);
        }

        // GET: EducationType/Delete/5
        public ActionResult Delete(long? id)
        {
            EducationTypeModel mdl = new EducationTypeModel();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Education_Type education_Type = db.Education_Type.Find(id);
            if (education_Type == null)
            {
                return HttpNotFound();
            }
            fillDBtoModel(education_Type, ref mdl);
            return View(mdl);
        }

        // POST: EducationType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Education_Type education_Type = db.Education_Type.Find(id);
            //db.Education_Type.Remove(education_Type);
            education_Type.isActive = false;
            db.Entry(education_Type).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
