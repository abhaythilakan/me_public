﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CLayer.DBModel;
using CLayer;

namespace ME.Controllers
{
    public class TaxTypeController : Controller
    {
        private ME_v2Entities db = new ME_v2Entities();

        public void fillDBtoModel(IncomeTaxType dbT, ref IncomeTaxTypeModel mdT)
        {
            mdT.taxTypeID = dbT.taxTypeID;
            mdT.tc_Id = Convert.ToInt32(dbT.tc_Id);
            mdT.taxTypeName = dbT.taxTypeName;
            mdT.sortOrder = dbT.sortOrder;
            mdT.createdDate = dbT.createdDate;
            mdT.updatedDate = dbT.updatedDate;
            mdT.updatedBy = dbT.updatedBy;
            mdT.IsActive = dbT.IsActive;
        }
        public void fillModeltoDB(IncomeTaxTypeModel mdT, ref IncomeTaxType dbT)
        {
            dbT.taxTypeID = mdT.taxTypeID;
            dbT.tc_Id = mdT.tc_Id;
            dbT.taxTypeName = mdT.taxTypeName;
            dbT.sortOrder = mdT.sortOrder;
            dbT.createdDate = mdT.createdDate;
            dbT.IsActive = true;
            if (dbT.taxTypeID > 0)
            {
                dbT.updatedDate = DateTime.Now;
                dbT.createdDate = mdT.createdDate;
            }
            else
                dbT.createdDate = DateTime.Now;
            mdT.updatedBy = dbT.updatedBy;
        }

        // GET: TaxType
        public ActionResult Index(int typeCatId)
        {
            var lis = db.IncomeTaxTypes.Where(b => b.IsActive == true && b.tc_Id == typeCatId).Select(b => new IncomeTaxTypeModel()
            {
                taxTypeID = b.taxTypeID,
                tc_Id = b.tc_Id ?? 0,
                taxTypeName = b.taxTypeName,
                sortOrder = b.sortOrder,
                createdDate = b.createdDate,
                updatedDate = b.updatedDate,
                updatedBy = b.updatedBy,
                IsActive = b.IsActive
            }).ToList();
            if (lis == null)
            {
                var lis2 = new List<IncomeTaxTypeModel>();
                lis2.Add(new IncomeTaxTypeModel() { tc_Id = typeCatId });
                return View(lis2);
            }
            return View(lis);
        }

        // GET: TaxType/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IncomeTaxType incomeTaxType = db.IncomeTaxTypes.Find(id);
            if (incomeTaxType == null)
            {
                return HttpNotFound();
            }
            IncomeTaxTypeModel mdl = new IncomeTaxTypeModel();
            fillDBtoModel(incomeTaxType, ref mdl);
            return View(mdl);
        }

        // GET: TaxType/Create
        public ActionResult Create(int typeCatId)
        {
            IncomeTaxTypeModel mdl = new IncomeTaxTypeModel();
            mdl.tc_Id = typeCatId;
            return View(mdl);
        }

        // POST: TaxType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IncomeTaxTypeModel mdl)
        {
            if (ModelState.IsValid)
            {
                IncomeTaxType incomeTaxType = new IncomeTaxType();
                fillModeltoDB(mdl, ref incomeTaxType);
                db.IncomeTaxTypes.Add(incomeTaxType);
                db.SaveChanges();
                return RedirectToAction("Index", new { typeCatId = incomeTaxType.tc_Id });
            }

            return View(mdl);
        }

        // GET: TaxType/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IncomeTaxType incomeTaxType = db.IncomeTaxTypes.Find(id);
            if (incomeTaxType == null)
            {
                return HttpNotFound();
            }
            IncomeTaxTypeModel mdl = new IncomeTaxTypeModel();
            fillDBtoModel(incomeTaxType, ref mdl);
            return View(mdl);
        }

        // POST: TaxType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(IncomeTaxTypeModel mdl)
        {
            if (ModelState.IsValid)
            {
                IncomeTaxType incomeTaxType = new IncomeTaxType();
                fillModeltoDB(mdl, ref incomeTaxType);
                db.Entry(incomeTaxType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { typeCatId = incomeTaxType.tc_Id });
            }
            return View(mdl);
        }

        // GET: TaxType/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            IncomeTaxType incomeTaxType = db.IncomeTaxTypes.Find(id);
            if (incomeTaxType == null)
            {
                return HttpNotFound();
            }
            IncomeTaxTypeModel mdl = new IncomeTaxTypeModel();
            fillDBtoModel(incomeTaxType, ref mdl);
            return View(mdl);
        }

        // POST: TaxType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            IncomeTaxType incomeTaxType = db.IncomeTaxTypes.Find(id);
            //db.IncomeTaxTypes.Remove(incomeTaxType);
            incomeTaxType.IsActive = false;
            db.Entry(incomeTaxType).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index", new { typeCatId = incomeTaxType.tc_Id });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
