﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CLayer.DBModel;
using CLayer;

namespace ME.Controllers
{
    public class EmailAlertController : Controller
    {
        private ME_v2Entities db = new ME_v2Entities();

        public void fillDBtoModel(EmailAlert dbT, ref EmailAlertModel mdT)
        {
            mdT.ea_Id = dbT.ea_Id;
            mdT.ea_Code = dbT.ea_Code;
            mdT.ea_Title = dbT.ea_Title;
            mdT.ea_NoofDays = dbT.ea_NoofDays;
            mdT.ea_BCC = dbT.ea_BCC;
            mdT.ea_Content = dbT.ea_Content;
            mdT.createdDT = dbT.createdDT;
            mdT.modifyDT = dbT.modifyDT;
            mdT.modifyBy = dbT.modifyBy;
            mdT.isActive = dbT.isActive;
        }
        public void fillModeltoDB(EmailAlertModel mdT, ref EmailAlert dbT)
        {
            dbT.ea_Id = mdT.ea_Id;
            dbT.ea_Code = mdT.ea_Code;
            dbT.ea_Title = mdT.ea_Title;
            dbT.ea_NoofDays = mdT.ea_NoofDays;
            dbT.ea_BCC = mdT.ea_BCC;
            dbT.ea_Content = mdT.ea_Content;
            dbT.isActive = true;
            if (dbT.ea_Id > 0)
            {
                dbT.modifyDT = DateTime.Now;
                dbT.createdDT = mdT.createdDT;
            }
            else
                dbT.createdDT = DateTime.Now;
            mdT.modifyBy = mdT.modifyBy;
        }

        // GET: EmailAlert
        public ActionResult Index()
        {
            //EmailAlertModelBase mdl = new EmailAlertModelBase();
            //mdl.EmailAlertModel = new List<EmailAlertModel>();
            var mdl = db.EmailAlerts.Where(b => b.isActive == true).Select(b => new EmailAlertModel()
            {
                ea_Id = b.ea_Id,
                ea_Code = b.ea_Code,
                ea_Title = b.ea_Title,
                ea_NoofDays = b.ea_NoofDays,
                ea_BCC = b.ea_BCC,
                ea_Content = b.ea_Content,
                createdDT = b.createdDT,
                modifyDT = b.modifyDT,
                modifyBy = b.modifyBy,
                isActive = b.isActive
            }).ToList();
            return View(mdl);
        }

        [HttpGet]
        public ActionResult getAdvisorCounts()
        {
            AdvisorCountModel mdl = new AdvisorCountModel();
            mdl.activeAdvisorCnt = db.Users.Where(b=>b.isLicensed == true && b.isActive == true).Count();
            mdl.expAdvisorCnt = db.Users.Where(b => b.isLicensed == true && b.isActive == true).Count();
            mdl.expTrialAdvisorCnt = db.Users.Where(b => b.trialEndDate < DateTime.Now && b.isActive == true).Count();
            mdl.activeClientCnt = db.Users.Where(b => b.clientId > 0 && b.isActive == true).Count();
            return View("AdvisorCount", mdl);
        }

        // GET: EmailAlert/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailAlert emailAlert = db.EmailAlerts.Find(id);
            if (emailAlert == null)
            {
                return HttpNotFound();
            }
            return View(emailAlert);
        }

        // GET: EmailAlert/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EmailAlert/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ea_Id,ea_Code,ea_Title,ea_NoofDays,ea_BCC,ea_Content,createdDT,modifyDT,modifyBy")] EmailAlert emailAlert)
        {
            if (ModelState.IsValid)
            {
                db.EmailAlerts.Add(emailAlert);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(emailAlert);
        }

        // GET: EmailAlert/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailAlert emailAlert = db.EmailAlerts.Find(id);
            if (emailAlert == null)
            {
                return HttpNotFound();
            }
            EmailAlertModel mdl = new EmailAlertModel();
            fillDBtoModel(emailAlert, ref mdl);
            return View(mdl);
        }

        // POST: EmailAlert/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EmailAlertModel mdl)
        {
            if (ModelState.IsValid)
            {
                EmailAlert emailAlert = new EmailAlert();
                fillModeltoDB(mdl, ref emailAlert);
                db.Entry(emailAlert).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mdl);
        }

        // GET: EmailAlert/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailAlert emailAlert = db.EmailAlerts.Find(id);
            if (emailAlert == null)
            {
                return HttpNotFound();
            }
            EmailAlertModel mdl = new EmailAlertModel();
            fillDBtoModel(emailAlert, ref mdl);
            return View(mdl);
        }

        // POST: EmailAlert/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            EmailAlert emailAlert = db.EmailAlerts.Find(id);
            //db.EmailAlerts.Remove(emailAlert);
            emailAlert.isActive = true;
            db.Entry(emailAlert).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
