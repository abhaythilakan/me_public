﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CLayer.DBModel;
using CLayer;

namespace ME.Controllers
{
    public class DisclaimersController : Controller
    {
        private ME_v2Entities db = new ME_v2Entities();

        public void fillDBtoModel(Disclaimer dbT, ref DisclaimerModel mdT)
        {
            mdT.dis_Id = dbT.dis_Id;
            mdT.dis_Name = dbT.dis_Name;
            mdT.dis_Body = dbT.dis_Body;
            mdT.createdDT = dbT.createdDT;
            mdT.modifyDT = dbT.modifyDT;
            mdT.modifyBy = dbT.modifyBy;
            mdT.dis_isActive = true;
        }
        public void fillModeltoDB(DisclaimerModel mdT, ref Disclaimer dbT)
        {
            dbT.dis_Id = mdT.dis_Id;
            dbT.dis_Name = mdT.dis_Name;
            dbT.dis_Body = mdT.dis_Body;
            dbT.createdDT = mdT.createdDT;
            dbT.modifyDT = mdT.modifyDT;
            dbT.modifyBy = mdT.modifyBy;
            dbT.dis_isActive = true;
            if (mdT.dis_Id > 0)
            {
                dbT.modifyDT = DateTime.Now;
                dbT.createdDT = mdT.createdDT;
            }
            else
                dbT.createdDT = DateTime.Now;
            dbT.modifyBy = mdT.modifyBy;
        }

        // GET: Disclaimers
        public ActionResult Index()
        {
            return View(db.Disclaimers.Where(b => b.dis_isActive == true).Select(b => new DisclaimerModel()
            {
                dis_Id = b.dis_Id,
                dis_Name = b.dis_Name,
                dis_Body = b.dis_Body,
                createdDT = b.createdDT,
                modifyDT = b.modifyDT,
                modifyBy = b.modifyBy,
                dis_isActive = b.dis_isActive ?? false
            }));
        }

        // GET: Disclaimers/Details/5
        public ActionResult Details(int? id)
        {
            DisclaimerModel mdl = new DisclaimerModel();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Disclaimer disclaimer = db.Disclaimers.Find(id);
            if (disclaimer == null)
            {
                return HttpNotFound();
            }
            fillDBtoModel(disclaimer, ref mdl);
            return View(mdl);
        }

        // GET: Disclaimers/Create
        public ActionResult Create()
        {
            DisclaimerModel mdl = new DisclaimerModel();
            return View(mdl);
        }

        // POST: Disclaimers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(DisclaimerModel mdl)
        {
            Disclaimer disclaimer = new Disclaimer();
            if (ModelState.IsValid)
            {
                fillModeltoDB(mdl, ref disclaimer);
                db.Disclaimers.Add(disclaimer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mdl);
        }

        // GET: Disclaimers/Edit/5
        public ActionResult Edit(int? id)
        {
            DisclaimerModel mdl = new DisclaimerModel();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Disclaimer disclaimer = db.Disclaimers.Find(id);
            if (disclaimer == null)
            {
                return HttpNotFound();
            }
            fillDBtoModel(disclaimer, ref mdl);
            return View(mdl);
        }

        // POST: Disclaimers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(DisclaimerModel mdl)
        {
            Disclaimer disclaimer = new Disclaimer();
            if (ModelState.IsValid)
            {
                fillModeltoDB(mdl, ref disclaimer);
                db.Entry(disclaimer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mdl);
        }

        // GET: Disclaimers/Delete/5
        public ActionResult Delete(int? id)
        {
            DisclaimerModel mdl = new DisclaimerModel();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Disclaimer disclaimer = db.Disclaimers.Find(id);
            if (disclaimer == null)
            {
                return HttpNotFound();
            }
            fillDBtoModel(disclaimer, ref mdl);
            return View(disclaimer);
        }

        // POST: Disclaimers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Disclaimer disclaimer = db.Disclaimers.Find(id);
            db.Disclaimers.Remove(disclaimer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
