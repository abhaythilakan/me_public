﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CLayer.DBModel;
using CLayer;

namespace ME.Controllers
{
    public class EmailAdvisorsController : Controller
    {
        private ME_v2Entities db = new ME_v2Entities();

        // GET: EmailAdvisors
        public ActionResult Index()
        {
            return View(db.EmailAdvisors.ToList());
        }

        // GET: EmailAdvisors/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailAdvisor emailAdvisor = db.EmailAdvisors.Find(id);
            if (emailAdvisor == null)
            {
                return HttpNotFound();
            }
            return View(emailAdvisor);
        }

        // GET: EmailAdvisors/Create
        public ActionResult Create()
        {
            EmailAdvisorModel mdl = new EmailAdvisorModel();
            mdl.advEmailList = BLayer.AdvisorReg.getAdvListBasedOnCat(CLayer.common.CustomEnum.EmailAdvisorCategory.CurrentAdvisers, null);
            return View();
        }

        // POST: EmailAdvisors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ea_Id,ea_FromEmail,ea_Subject,ea_Content,ea_Advisors,ea_SendDT")] EmailAdvisor emailAdvisor)
        {
            if (ModelState.IsValid)
            {
                db.EmailAdvisors.Add(emailAdvisor);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(emailAdvisor);
        }

        // GET: EmailAdvisors/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailAdvisor emailAdvisor = db.EmailAdvisors.Find(id);
            if (emailAdvisor == null)
            {
                return HttpNotFound();
            }
            return View(emailAdvisor);
        }

        // POST: EmailAdvisors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ea_Id,ea_FromEmail,ea_Subject,ea_Content,ea_Advisors,ea_SendDT")] EmailAdvisor emailAdvisor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(emailAdvisor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(emailAdvisor);
        }

        // GET: EmailAdvisors/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            EmailAdvisor emailAdvisor = db.EmailAdvisors.Find(id);
            if (emailAdvisor == null)
            {
                return HttpNotFound();
            }
            return View(emailAdvisor);
        }

        // POST: EmailAdvisors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            EmailAdvisor emailAdvisor = db.EmailAdvisors.Find(id);
            db.EmailAdvisors.Remove(emailAdvisor);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
