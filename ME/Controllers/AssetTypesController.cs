﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CLayer.DBModel;
using CLayer;

namespace ME.Controllers
{
    public class AssetTypesController : Controller
    {
        private ME_v2Entities db = new ME_v2Entities();

        public void fillDBtoModel(AssetType dbT, ref AssetTypesModel mdT)
        {
            mdT.assetTypeID = dbT.assetTypeID;
            mdT.assetCatID = dbT.assetCatID;
            mdT.assetCatName = getAssetCatName(dbT.assetCatID ?? 0);
            mdT.typeName = dbT.typeName;
            mdT.createdDate = dbT.createdDate;
            mdT.isActive = dbT.isActive;
            mdT.isTaxed = dbT.isTaxed ?? false;
            mdT.updatedBy = dbT.updatedBy;
            mdT.updatedDate = dbT.updatedDate;
        }
        public void fillModeltoDB(AssetTypesModel mdT, ref AssetType dbT)
        {
            dbT.assetTypeID = mdT.assetTypeID;
            dbT.assetCatID = mdT.assetCatID;
            dbT.typeName = mdT.typeName;
            dbT.isActive = mdT.isActive;
            dbT.isTaxed = mdT.isTaxed;
            if (dbT.assetTypeID > 0)
                mdT.updatedDate = DateTime.Now;
            else
                mdT.createdDate = DateTime.Now;
            mdT.updatedBy = dbT.updatedBy;
        }

        // GET: AssetTypes
        public ActionResult Index()
        {
            List<AssetTypesModel> model = new List<AssetTypesModel>();
            try
            {
                model = db.AssetTypes.Where(b => b.isActive == true).Select(b => new AssetTypesModel()
                {
                    assetTypeID = b.assetTypeID,
                    assetCatID = b.assetCatID,
                    assetCatName = db.AssetCategories.Where(c => c.catID == b.assetCatID).FirstOrDefault().catName,
                    typeName = b.typeName,
                    createdDate = b.createdDate,
                    isActive = b.isActive,
                    isTaxed = b.isTaxed ?? false,
                    updatedBy = b.updatedBy,
                    updatedDate = b.updatedDate
                }).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }

        public string getAssetCatName(int catID)
        {
            string Name = "";
            try
            {
                if (catID > 0)
                    Name = db.AssetCategories.Single(b => b.catID == catID).catName;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Name;
        }

        // GET: AssetTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssetTypesModel model = new AssetTypesModel();
            try
            {
                AssetType assetType = db.AssetTypes.Find(id);
                if (assetType == null)
                {
                    return HttpNotFound();
                }
                fillDBtoModel(assetType, ref model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }

        // GET: AssetTypes/Create
        public ActionResult Create()
        {
            AssetTypesModel model = new AssetTypesModel();
            return View(model);
        }

        // POST: AssetTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(AssetTypesModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.createdDate = DateTime.Now;
                    model.updatedDate = DateTime.Now;
                    model.updatedBy = 0;
                    model.isActive = true;
                    AssetType assetType = new AssetType();
                    fillModeltoDB(model, ref assetType);
                    db.AssetTypes.Add(assetType);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return View(model);
        }

        // GET: AssetTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssetTypesModel model = new AssetTypesModel();
            try
            {
                AssetType assetType = db.AssetTypes.Find(id);
                if (assetType == null)
                {
                    return HttpNotFound();
                }
                fillDBtoModel(assetType, ref model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }

        // POST: AssetTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(AssetTypesModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.updatedDate = DateTime.Now;
                    model.updatedBy = 0;
                    AssetType assetType = new AssetType();
                    fillModeltoDB(model, ref assetType);
                    db.Entry(assetType).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }

        // GET: AssetTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AssetTypesModel model = new AssetTypesModel();
            try
            {
                AssetType assetType = db.AssetTypes.Find(id);
                if (assetType == null)
                {
                    return HttpNotFound();
                }
                fillDBtoModel(assetType, ref model);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(model);
        }

        // POST: AssetTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                AssetType assetType = db.AssetTypes.Find(id);
                assetType.isActive = false;
                assetType.updatedDate = DateTime.Now;
                assetType.updatedBy = 0;
                db.Entry(assetType).State = EntityState.Modified;
                //db.AssetTypes.Remove(assetType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
