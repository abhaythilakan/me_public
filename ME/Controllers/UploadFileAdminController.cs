﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CLayer.DBModel;
using CLayer;
using System.IO;

namespace ME.Controllers
{
    public class UploadFileAdminController : Controller
    {
        private ME_v2Entities db = new ME_v2Entities();

        public void fillDBtoModel(UploadAdmin dbT, ref UploadAdminModel mdT)
        {
            string authority = HttpContext.Request.Url.Authority;
            mdT.ul_Id = dbT.ul_Id;
            mdT.ul_PathLabel = dbT.ul_PathLabel;
            mdT.ul_FileName = dbT.ul_FileName;
            mdT.ul_FileSize = dbT.ul_FileSize;
            mdT.ul_FilePath = dbT.ul_FilePath == null ? ("http://" + authority + "//" + dbT.ul_FilePath) : "";
            mdT.ul_Year = dbT.ul_Year;
            mdT.ul_CreatedDT = dbT.ul_CreatedDT;
            mdT.isActive = dbT.isActive;
        }
        public void fillModeltoDB(UploadAdminModel mdT, ref UploadAdmin dbT)
        {
            dbT.ul_Id = mdT.ul_Id;
            dbT.ul_PathLabel = mdT.ul_PathLabel;
            dbT.ul_FileName = mdT.ul_FileName;
            dbT.ul_FileSize = mdT.ul_FileSize;
            dbT.ul_FilePath = mdT.ul_FilePath;
            dbT.ul_Year = mdT.ul_Year;
            dbT.isActive = true;
            if (dbT.ul_Id > 0)
            {
                //dbT.updatedDate = DateTime.Now;
                dbT.ul_CreatedDT = mdT.ul_CreatedDT;
            }
            else
                dbT.ul_CreatedDT = DateTime.Now;
            //mdT.updatedBy = dbT.updatedBy;
        }

        public void updateFilePath(long uploadID, string path)
        {
            UploadAdmin dbT = db.UploadAdmins.Single(b => b.ul_Id == uploadID);
            if (dbT != null)
            {
                dbT.ul_FilePath = path;
                db.Entry(dbT).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        // GET: UploadFileAdmin
        public ActionResult Index()
        {
            return View(db.UploadAdmins.Where(b => b.isActive == true).OrderByDescending(b => b.ul_CreatedDT).Select(b => new UploadAdminModel()
            {
                ul_Id = b.ul_Id,
                ul_PathLabel = b.ul_PathLabel,
                ul_FileName = b.ul_FileName,
                ul_FileSize = b.ul_FileSize,
                ul_FilePath = b.ul_FilePath,
                ul_Year = b.ul_Year,
                ul_CreatedDT = b.ul_CreatedDT,
                isActive = b.isActive
            }).ToList());
        }

        // GET: UploadFileAdmin/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UploadAdmin uploadAdmin = db.UploadAdmins.Find(id);
            if (uploadAdmin == null)
            {
                return HttpNotFound();
            }
            UploadAdminModel mdl = new UploadAdminModel();
            fillDBtoModel(uploadAdmin, ref mdl);
            return View(mdl);
        }

        // GET: UploadFileAdmin/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UploadFileAdmin/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UploadAdminModel mdl)//, HttpPostedFileBase ul_File
        {
            if (ModelState.IsValid)
            {
                if (mdl.ul_File != null && mdl.ul_File.ContentLength > 0)
                {
                    var fileName = Path.GetFileNameWithoutExtension(mdl.ul_File.FileName);
                    string fileSize = "";
                    var ContentLength = mdl.ul_File.ContentLength;
                    var kb = ContentLength / 1024;
                    var mb = kb / 1024;
                    var gb = mb / 1024;
                    if (kb >= 1024 && mb < 1024)
                        fileSize = mb + " MB";
                    else if (mb >= 1024)
                        fileSize = gb + " GB";
                    else
                        fileSize = kb + " KB";

                    mdl.ul_FileName = fileName;
                    mdl.ul_FileSize = fileSize;
                    mdl.ul_CreatedDT = DateTime.Now;
                    mdl.ul_Year = DateTime.Now.Year;
                    var folderName = DateTime.Now.ToString("MMM dd");

                    UploadAdmin uploadAdmin = new UploadAdmin();
                    fillModeltoDB(mdl, ref uploadAdmin);
                    db.UploadAdmins.Add(uploadAdmin);
                    db.SaveChanges();
                    if (uploadAdmin.ul_Id > 0)
                    {
                        string newdirectory = "Files\\Admin\\Uploads\\" + folderName + "\\" + uploadAdmin.ul_Id + "\\";
                        if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~\\" + newdirectory)))
                        {
                            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~\\" + newdirectory));
                        }
                        newdirectory = newdirectory + Path.GetFileName(mdl.ul_File.FileName);
                        mdl.ul_File.SaveAs(System.Web.HttpContext.Current.Server.MapPath("~\\" + newdirectory));
                        updateFilePath(uploadAdmin.ul_Id, newdirectory);
                    }
                    return RedirectToAction("Index");
                }
            }
            return View(mdl);
        }

        //// GET: UploadFileAdmin/Edit/5
        //public ActionResult Edit(long? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    UploadAdmin uploadAdmin = db.UploadAdmins.Find(id);
        //    if (uploadAdmin == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    UploadAdminModel mdl = new UploadAdminModel();
        //    fillDBtoModel(uploadAdmin, ref mdl);
        //    return View(mdl);
        //}

        //// POST: UploadFileAdmin/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit(UploadAdminModel mdl)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        UploadAdmin uploadAdmin = new UploadAdmin();
        //        fillModeltoDB(mdl, ref uploadAdmin);
        //        db.Entry(uploadAdmin).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(mdl);
        //}

        // GET: UploadFileAdmin/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UploadAdmin uploadAdmin = db.UploadAdmins.Find(id);
            if (uploadAdmin == null)
            {
                return HttpNotFound();
            }
            UploadAdminModel mdl = new UploadAdminModel();
            fillDBtoModel(uploadAdmin, ref mdl);
            return View(mdl);
        }

        // POST: UploadFileAdmin/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            UploadAdmin uploadAdmin = db.UploadAdmins.Find(id);
            uploadAdmin.isActive = false;
            db.Entry(uploadAdmin).State = EntityState.Modified;
            //db.UploadAdmins.Remove(uploadAdmin);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
