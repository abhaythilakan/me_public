﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CLayer;
using CLayer.DBModel;

namespace ME.Controllers
{
    public class ExpenseTypeController : Controller
    {
        private ME_v2Entities db = new ME_v2Entities();

        public void fillDBtoModel(ExpenseType dbT, ref ExpenseTypeModel mdT)
        {
            mdT.id = dbT.id;
            mdT.TypeName = dbT.TypeName;
            mdT.expenseSubTypeId = dbT.expenseSubTypeId;
            mdT.sortOrder = dbT.sortOrder;
            mdT.createdDT = dbT.createdDT;
            mdT.modifyDT = dbT.modifyDT;
            mdT.modifyBy = dbT.modifyBy;
            mdT.isActive = dbT.isActive;
        }
        public void fillModeltoDB(ExpenseTypeModel mdT, ref ExpenseType dbT)
        {
            dbT.id = mdT.id;
            dbT.TypeName = mdT.TypeName;
            dbT.expenseSubTypeId = mdT.expenseSubTypeId;
            dbT.sortOrder = mdT.sortOrder;
            dbT.isActive = true;
            if (dbT.id > 0)
            {
                dbT.modifyDT = DateTime.Now;
                dbT.createdDT = mdT.createdDT;
            }
            else
                dbT.createdDT = DateTime.Now;
            mdT.modifyBy = dbT.modifyBy;
        }

        // GET: ExpenseType
        public ActionResult Index(long expenseSubCatIds)
        {
            //List<ExpenseTypeModel> lis = new List<ExpenseTypeModel>();
            var lis = db.ExpenseTypes.Where(b => b.isActive == true && b.expenseSubTypeId == expenseSubCatIds).Select(b => new ExpenseTypeModel()
            {
                id = b.id,
                TypeName = b.TypeName,
                expenseSubTypeId = b.expenseSubTypeId,
                sortOrder = b.sortOrder,
                createdDT = b.createdDT,
                modifyDT = b.modifyDT,
                modifyBy = b.modifyBy,
                isActive = b.isActive
            });
            if (lis == null)
            {
                var lis2 = new List<ExpenseTypeModel>();
                lis2.Add(new ExpenseTypeModel() { expenseSubTypeId = expenseSubCatIds });
                return View(lis2);
            }
            return View(lis);
        }

        // GET: ExpenseType/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExpenseType expenseType = db.ExpenseTypes.Find(id);
            if (expenseType == null)
            {
                return HttpNotFound();
            }
            ExpenseTypeModel mdl = new ExpenseTypeModel();
            fillDBtoModel(expenseType, ref mdl);
            return View(mdl);
        }

        // GET: ExpenseType/Create
        public ActionResult Create(long expenseSubTypeId)
        {
            ExpenseTypeModel mdl = new ExpenseTypeModel() { expenseSubTypeId = expenseSubTypeId };
            return View(mdl);
        }

        // POST: ExpenseType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ExpenseTypeModel mdl)
        {
            ExpenseType expenseType = new ExpenseType();
            if (ModelState.IsValid)
            {
                fillModeltoDB(mdl, ref expenseType);
                db.ExpenseTypes.Add(expenseType);
                db.SaveChanges();
                return RedirectToAction("Index", new { expenseSubCatId = expenseType.expenseSubTypeId });
            }

            return View(mdl);
        }

        // GET: ExpenseType/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExpenseType expenseType = db.ExpenseTypes.Find(id);
            if (expenseType == null)
            {
                return HttpNotFound();
            }
            ExpenseTypeModel mdl = new ExpenseTypeModel();
            fillDBtoModel(expenseType, ref mdl);
            return View(mdl);
        }

        // POST: ExpenseType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ExpenseTypeModel mdl)
        {
            ExpenseType expenseType = new ExpenseType();
            if (ModelState.IsValid)
            {
                fillModeltoDB(mdl, ref expenseType);
                db.Entry(expenseType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", new { expenseSubCatId = expenseType.expenseSubTypeId });
            }
            return View(mdl);
        }

        // GET: ExpenseType/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExpenseType expenseType = db.ExpenseTypes.Find(id);
            if (expenseType == null)
            {
                return HttpNotFound();
            }
            ExpenseTypeModel mdl = new ExpenseTypeModel();
            fillDBtoModel(expenseType, ref mdl);
            return View(mdl);
        }

        // POST: ExpenseType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            ExpenseType expenseType = db.ExpenseTypes.Find(id);
            //db.ExpenseTypes.Remove(expenseType);
            expenseType.isActive = false;
            db.Entry(expenseType).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index", new { expenseSubCatId = expenseType.expenseSubTypeId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
