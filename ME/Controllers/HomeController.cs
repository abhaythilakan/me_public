﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ME.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            //BLayer.Login.isValidUser_AdminAdvisor();
            //BLayer.Login.isValidUser_Client();
            return View();
        }
    }
}
