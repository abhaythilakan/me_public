﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CLayer.DBModel;
using CLayer;

namespace ME.Controllers
{
    public class InsuranceTypeController : Controller
    {
        private ME_v2Entities db = new ME_v2Entities();

        public void fillDBtoModel(insuranceType dbT, ref InsuranceTypeModel mdT)
        {
            mdT.insuranceTypeID = dbT.insuranceTypeID;
            mdT.insuranceTypeName = dbT.insuranceTypeName;
            mdT.orderNum = dbT.orderNum;
            mdT.createdDT = dbT.createdDT;
            mdT.modifyDT = dbT.modifyDT;
            mdT.modifyBy = dbT.modifyBy;
            mdT.isActive = dbT.isActive;
        }
        public void fillModeltoDB(InsuranceTypeModel mdT, ref insuranceType dbT)
        {
            dbT.insuranceTypeID = mdT.insuranceTypeID;
            dbT.insuranceTypeName = mdT.insuranceTypeName;
            dbT.orderNum = mdT.orderNum;
            dbT.isActive = true;
            if (dbT.insuranceTypeID > 0)
            {
                dbT.modifyDT = DateTime.Now;
                dbT.createdDT = mdT.createdDT;
            }
            else
                dbT.createdDT = DateTime.Now;
            mdT.modifyBy = dbT.modifyBy;
        }

        // GET: InsuranceType
        public ActionResult Index()
        {
            return View(db.insuranceTypes.Where(b=>b.isActive == true).Select(b => new InsuranceTypeModel()
            {
                insuranceTypeID = b.insuranceTypeID,
                insuranceTypeName = b.insuranceTypeName,
                orderNum = b.orderNum,
                createdDT = b.createdDT,
                modifyDT = b.modifyDT,
                modifyBy = b.modifyBy,
                isActive = b.isActive,
            }));
        }

        // GET: InsuranceType/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            insuranceType insuranceType = db.insuranceTypes.Find(id);
            if (insuranceType == null)
            {
                return HttpNotFound();
            }
            InsuranceTypeModel mdl = new InsuranceTypeModel();
            fillDBtoModel(insuranceType, ref mdl);
            return View(mdl);
        }

        // GET: InsuranceType/Create
        public ActionResult Create()
        {
            InsuranceTypeModel mdl = new InsuranceTypeModel();
            return View(mdl);
        }

        // POST: InsuranceType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(InsuranceTypeModel mdl)
        {
            insuranceType insuranceType = new insuranceType();
            if (ModelState.IsValid)
            {
                fillModeltoDB(mdl, ref insuranceType);
                db.insuranceTypes.Add(insuranceType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mdl);
        }

        // GET: InsuranceType/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            insuranceType insuranceType = db.insuranceTypes.Find(id);
            if (insuranceType == null)
            {
                return HttpNotFound();
            }
            InsuranceTypeModel mdl = new InsuranceTypeModel();
            fillDBtoModel(insuranceType, ref mdl);
            return View(mdl);
        }

        // POST: InsuranceType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(InsuranceTypeModel mdl)
        {
            insuranceType insuranceType = new insuranceType();
            if (ModelState.IsValid)
            {
                fillModeltoDB(mdl, ref insuranceType);
                db.Entry(insuranceType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mdl);
        }

        // GET: InsuranceType/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            insuranceType insuranceType = db.insuranceTypes.Find(id);
            if (insuranceType == null)
            {
                return HttpNotFound();
            }
            InsuranceTypeModel mdl = new InsuranceTypeModel();
            fillDBtoModel(insuranceType, ref mdl);
            return View(mdl);
        }

        // POST: InsuranceType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            insuranceType insuranceType = db.insuranceTypes.Find(id);
            //db.insuranceTypes.Remove(insuranceType);
            insuranceType.isActive = false;
            db.Entry(insuranceType).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
