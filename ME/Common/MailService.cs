﻿using CLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Net;
//using System.Net.Mail;
using System.Web;

namespace ME.Common
{
    public class MailService
    {
        public static void sendMail(string subject, string body, string toEmail,string fromDisplayName)
        {
            //Properties.Settings.Default["EmailAddress"].ToString();
            //Properties.Settings.Default["EmailPassword"].ToString();
            var fromAddress = new System.Net.Mail.MailAddress(Properties.Settings.Default["EmailAddress"].ToString(), fromDisplayName);
            var toAddress = new System.Net.Mail.MailAddress(toEmail.ToString());
            string fromPassword = Properties.Settings.Default["EmailPassword"].ToString();
            //OutLook- smtp.live.com, Port- 587
            //Yahoo- smtp.mail.yahoo.com, Port- 465
            //Hotmail- smtp.live.com, Port- 465
            //Office365.com- smtp.office365.com, Port- 587
            var smtp = new System.Net.Mail.SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(fromAddress.Address, fromPassword),
            };
            using (var message = new System.Net.Mail.MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
            {
                smtp.Send(message);
            }
        }


        //public delegate System.Net.IPEndPoint BindIPEndPoint(System.Net.ServicePoint servicePoint, System.Net.IPEndPoint remoteEndPoint, int retryCount);

        //private System.Net.IPEndPoint BindIPEndPointCallback(System.Net.ServicePoint servicePoint, System.Net.IPEndPoint remoteEndPoint, int retryCount)
        //{
        //    //if (retryCount < 3 && ddSendFrom.SelectedValue.Length > 0)
        //    //    return new System.Net.IPEndPoint(IPAddress.Parse("192.168.1.100"), 0); //bind to a specific ip address on your server
        //    //else
        //    //    return new IPEndPoint(IPAddress.Any, 0);
        //}

        public static void sendNotificationMail(string emailFrom, string messageFromName, string phone, string subject, string comment)
        {
            try
            {

                var fromAddress = new System.Net.Mail.MailAddress(Properties.Settings.Default["EmailAddress"].ToString(), emailFrom);
                var toAddress = new System.Net.Mail.MailAddress(Properties.Settings.Default["ContactUsMail"].ToString());
                string fromPassword = Properties.Settings.Default["EmailPassword"].ToString();
                var smtp = new System.Net.Mail.SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new System.Net.NetworkCredential(fromAddress.Address, fromPassword),
                };
                using (var message = new System.Net.Mail.MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = "Name : " + messageFromName + "\n" + "Mobile : " + phone + "\n" + "Comment : " + comment,
                    IsBodyHtml = true
                })
                {
                    smtp.Send(message);
                }
                //Success Message
            }
            catch (Exception ex)
            {
                //Failure Message
                throw;
            }
        }
        public static void sendAddadvisorMail(AdvisorMailModel inobj )
        {
            //Properties.Settings.Default["EmailAddress"].ToString();
            //Properties.Settings.Default["EmailPassword"].ToString();
            var fromAddress = new System.Net.Mail.MailAddress(Properties.Settings.Default["EmailAddress"].ToString(), inobj.fromDisplayName);
            var toAddress = new System.Net.Mail.MailAddress(inobj.toEmail.ToString());
            string fromPassword = Properties.Settings.Default["EmailPassword"].ToString();
            //OutLook- smtp.live.com, Port- 587
            //Yahoo- smtp.mail.yahoo.com, Port- 465
            //Hotmail- smtp.live.com, Port- 465
            //Office365.com- smtp.office365.com, Port- 587
            string body="";

            //string userName = "John Doe";
            StringBuilder mailBody = new StringBuilder();  
            mailBody.AppendFormat("<h1>Login Credentials</h1>");
            mailBody.AppendFormat("Dear {0}," , inobj.name);
            mailBody.AppendFormat("<br />");
            mailBody.AppendFormat("<p> {0}  has invited you to login MoneyEdge -'A Tool to Help Your Financial Practice Proposer'  with following login credentials:</p>", inobj.fromDisplayName);
            mailBody.AppendFormat("<br />");
            mailBody.AppendFormat("<br />");
            mailBody.AppendFormat("<p> Username : {0} </p>", inobj.username);
            mailBody.AppendFormat("<br />");
            mailBody.AppendFormat("<p> Password : {0} </p>", inobj.password);
            mailBody.AppendFormat("<br />");
            mailBody.AppendFormat("<br />");
            mailBody.AppendFormat("<p>Please click this link to login http://52.25.167.251/MEapp/app-login </p>");
            

            var smtp = new System.Net.Mail.SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new System.Net.NetworkCredential(fromAddress.Address, fromPassword),
            };
            using (var message = new System.Net.Mail.MailMessage(fromAddress, toAddress)
            {
                Subject = inobj. subject,
                Body = mailBody.ToString(),
                IsBodyHtml = true
            })
            {
                System.Net.Mail.MailAddress advisor = new System.Net.Mail.MailAddress(inobj.advisoremail);
                message.CC.Add(advisor);
                smtp.Send(message);
            }
        }

    }
}