﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Heijden.DNS;

namespace ME.Common
{
    public enum ValidationMode
    {
        Syntax,
        Network
    };

    public class EmailValidator : IDisposable   
    {
        public string MailFrom { get; set; }

        public string GetLastError { private set; get; }

        public bool ValidateEmail(string strInputEmail, ValidationMode enumValidationMode)
        {
            string strAcceptableEmailAddressPattern = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex regPatternEmail = new Regex(strAcceptableEmailAddressPattern);

            switch (enumValidationMode)
            {
                case ValidationMode.Syntax:
                    try
                    {
                        return (regPatternEmail.IsMatch(strInputEmail));
                    }
                    catch (Exception RegExPatternException)
                    {
                        GetLastError = string.Format("Could not match pattern because of the error {0} ",RegExPatternException.Message);
                        return (false);
                    }
                case ValidationMode.Network:
                    if (ValidateEmail(strInputEmail, ValidationMode.Syntax))
                    {
                        string strHostName = string.Empty;
                        try
                        {
                            string[] arrHost = strInputEmail.Split('@');
                            strHostName = arrHost[1];
                        }
                        catch (Exception HostMalformedException)
                        {
                            GetLastError = string.Format("Could not identify the target email server because of the error {0} ", HostMalformedException.Message);
                        }

                        try
                        {
                            Resolver objResolver = new Resolver();
                            Response objResponse = objResolver.Query(strHostName, QType.MX, QClass.IN);

                            bool blnValidAddress = false;

                            if (objResponse.RecordsMX.Length < 1)
                            {
                                return (false);
                            }
                            else
                            {

                                foreach (RecordMX recMx in objResponse.RecordsMX)
                                {
                                    blnValidAddress = VerifySmtpResponse(recMx.EXCHANGE, strInputEmail);
                                    if (blnValidAddress)
                                        break;
                                }
                            }

                            return (blnValidAddress);
                        }
                        catch (Exception NetworkQueryException)
                        {
                            GetLastError = NetworkQueryException.Message;
                            return (false);
                        }
                        
                    }
                    else
                    {
                        return (false);
                    }
                default:
                    return (false);
            }
        }

        public void Dispose()
        {
            
        }

        public bool VerifySmtpResponse(string strSmtpServerURL, string strEmailAddress)
        {
            IPHostEntry hostEntry = Dns.GetHostEntry(strSmtpServerURL);
            IPEndPoint endPoint = new IPEndPoint(hostEntry.AddressList[0], 25);

            using (Socket tcpSocket = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp))
            {
                tcpSocket.Connect(endPoint);

                if (!CheckResponse(tcpSocket, 220))
                {
                    tcpSocket.Close();
                    return false;
                }

                //HELO server
                SendData(tcpSocket, string.Format("HELO {0}\r\n", Dns.GetHostName()));
                if (!CheckResponse(tcpSocket, 250))
                {
                    tcpSocket.Close();
                    return false;
                }

                //Identify yourself
                //Servers may resolve your domain and check whether 
                //you are listed in BlackLists etc.
                SendData(tcpSocket, string.Format("MAIL From: {0}\r\n", MailFrom));
                if (!CheckResponse(tcpSocket, 220))
                {
                    tcpSocket.Close();
                    return false;
                }

                //Attempt Delivery (I can use VRFY, but most 
                //SMTP servers only disable it for security reasons)
                SendData(tcpSocket, strEmailAddress);
                if (!CheckResponse(tcpSocket, 220))
                {
                    tcpSocket.Close();
                    return false;
                }
            }
            return (true);
        }

        private static void SendData(Socket socket, string data)
        {
            byte[] dataArray = Encoding.ASCII.GetBytes(data);
            socket.Send(dataArray, 0, dataArray.Length, SocketFlags.None);
        }

        private static bool CheckResponse(Socket socket, int expectedCode)
        {
            while (socket.Available == 0)
            {
                System.Threading.Thread.Sleep(100);
            }
            byte[] responseArray = new byte[1024];
            socket.Receive(responseArray, 0, socket.Available, SocketFlags.None);
            string responseData = Encoding.ASCII.GetString(responseArray);
            int responseCode = Convert.ToInt32(responseData.Substring(0, 3));
            if (responseCode == expectedCode)
            {
                return true;
            }
            return false;
        }
    }
}