﻿using CLayer.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;

using static CLayer.common.CustomEnum;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ME.Common
{
    public class CustomErrorHandler : HandleErrorAttribute
    {
        public static ResultModel<Messages> getValidationErrors(System.Web.Http.ModelBinding.ModelStateDictionary model)
        {
            Messages msg = new Messages();
            msg.apiStatus = (int)apiStatusEnum.ValidationError;//validation errors
            msg.Message = "Invalid Validation";
            msg.ValidationErrors = new List<ValidationErrors>();
            var mdlState = model.Where(n => n.Value.Errors.Count > 0).ToList();
            foreach (var tt in mdlState)
            {
                ValidationErrors err = new ValidationErrors();
                err.Field = tt.Key.ToString();
                err.FieldErrorMessage = tt.Value.Errors[0].ErrorMessage.ToString();
                msg.ValidationErrors.Add(err);
            }

            var mdl = ResultFunction.SetReturnModel(msg);
            mdl.apiStatus = (int)apiStatusEnum.ValidationError;
            mdl.Message = "Invalid Validation";
            mdl.ValidationErrors = msg.ValidationErrors;
            return mdl;
        }

        public static ResultModel<Messages> getInvalidEntry()
        {
            Messages msg = new Messages();
            msg.apiStatus = (int)apiStatusEnum.ValidationError;//logic errors
            msg.Message = "Invalid Entry";

            var mdl = ResultFunction.SetReturnModel(msg);
            mdl.apiStatus = (int)apiStatusEnum.ValidationError;
            mdl.Message = "Invalid Entry";
            mdl.ValidationErrors = null;
            return mdl;
        }

        public static ResultModel<Messages> getProfileValidationErrors()
        {
            Messages msg = new Messages();
            msg.apiStatus = (int)apiStatusEnum.ValidationError;//logic errors
            msg.Message = "Invalid Profile";

            var mdl = ResultFunction.SetReturnModel(msg);
            mdl.apiStatus = (int)apiStatusEnum.ValidationError;
            mdl.Message = "Invalid Profile";
            mdl.ValidationErrors = null;
            return mdl;
        }

        public static ResultModel<Messages> getLogicErrors(Exception ex)
        {
            Messages msg = new Messages();
            msg.apiStatus = (int)apiStatusEnum.Error;//logic errors
            msg.Message = ex.Message.ToString();

            var mdl = ResultFunction.SetReturnModel(msg);
            mdl.Message = ex.Message.ToString();
            mdl.apiStatus = (int)apiStatusEnum.Error;
            mdl.ValidationErrors = null;
            return mdl;
        }

        public static ResultModel<Messages> forgotPasswordTokenExpired()
        {
            Messages msg = new Messages();
            msg.apiStatus = (int)apiStatusEnum.ValidationError;//logic errors
            msg.Message = "Forgot Password link has been expired";

            var mdl = ResultFunction.SetReturnModel(msg);
            mdl.Message = "Forgot Password link has been expired";
            mdl.apiStatus = (int)apiStatusEnum.ValidationError;
            mdl.ValidationErrors = null;
            return mdl;
        }

        public static ResultModel<Messages> getInvalidUsernamePassword()
        {
            Messages msg = new Messages();
            msg.apiStatus = (int)apiStatusEnum.Error;//logic errors
            msg.Message = "Invalid Username & Password!!!";

            var mdl = ResultFunction.SetReturnModel(msg);
            mdl.Message = "Invalid Username & Password!!!";
            mdl.apiStatus = (int)apiStatusEnum.Error;
            mdl.ValidationErrors = null;
            return mdl;
        }

        public static ResultModel<Messages> getLogoutMessage()
        {
            Messages msg = new Messages();
            msg.apiStatus = (int)apiStatusEnum.Success;
            msg.Message = "Logged Out Successfully";

            var mdl = ResultFunction.SetReturnModel(msg);
            mdl.Message = "Logged Out Successfully";
            mdl.apiStatus = (int)apiStatusEnum.Success;
            mdl.ValidationErrors = null;
            return mdl;
        }

        public static ResultModel<Messages> getActiveTokenMessage()
        {
            Messages msg = new Messages();
            msg.apiStatus = (int)apiStatusEnum.Success;
            msg.Message = "Token is active";

            var mdl = ResultFunction.SetReturnModel(msg);
            mdl.Message = "Token is active";
            mdl.apiStatus = (int)apiStatusEnum.Success;
            mdl.ValidationErrors = null;
            return mdl;
        }

        public static ResultModel<Messages> getInActiveTokenMessage()
        {
            Messages msg = new Messages();
            msg.apiStatus = (int)apiStatusEnum.Error;
            msg.Message = "Token is inactive";

            var mdl = ResultFunction.SetReturnModel(msg);
            mdl.Message = "Token is inactive";
            mdl.apiStatus = (int)apiStatusEnum.Error;
            mdl.ValidationErrors = null;
            return mdl;
        }

        public static ResultModel<Messages> mailSendSuccessfully()
        {
            Messages msg = new Messages();
            msg.Message = "E-Mail send successfully";

            var mdl = ResultFunction.SetReturnModel(msg);
            mdl.Message = "E-Mail send successfully";
            mdl.ValidationErrors = null;
            return mdl;
        }
        public static ResultModel<Messages> mailSendUnSuccessfully()
        {
            Messages msg = new Messages();
            msg.apiStatus = (int)apiStatusEnum.Error;
            msg.Message = "E-Mail is not registered!!!";

            var mdl = ResultFunction.SetReturnModel(msg);
            mdl.apiStatus = (int)apiStatusEnum.Error;
            mdl.Message = "E-Mail is not registered!!!";
            mdl.ValidationErrors = null;
            return mdl;
        }

        public static ResultModel<Messages> passwordChanged()
        {
            Messages msg = new Messages();
            msg.Message = "Password changed successfully";

            var mdl = ResultFunction.SetReturnModel(msg);
            mdl.Message = "Password changed successfully";
            mdl.ValidationErrors = null;
            return mdl;
        }
        public static ResultModel<Messages> passwordNotChanged()
        {
            Messages msg = new Messages();
            msg.apiStatus = (int)apiStatusEnum.Error;
            msg.Message = "Invalid password!!!";

            var mdl = ResultFunction.SetReturnModel(msg);
            mdl.apiStatus = (int)apiStatusEnum.Error;
            mdl.Message = "Invalid password!!!";
            mdl.ValidationErrors = null;
            return mdl;
        }

        public static ResultModel<Messages> invalidCurrentPassword()
        {
            Messages msg = new Messages();
            var mdl = ResultFunction.SetReturnModel(msg);
            mdl.Message = "Invalid Current Password";
            mdl.apiStatus = (int)apiStatusEnum.Error;
            mdl.ValidationErrors = null;
            return mdl;
        }

        public override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
        }

    }
}