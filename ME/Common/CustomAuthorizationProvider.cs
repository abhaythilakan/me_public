﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Security.Claims;
using CLayer.DBModel;
using Microsoft.Owin.Security;

namespace ME.Common
{
    public class CustomAuthorizationProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated(); //?
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                CLayer.UserModels usr = new CLayer.UserModels();
                //User usr = new User();
                usr = BLayer.Login.isValidUser(context.UserName, context.Password);
                if (usr != null)
                {
                    if (usr.advisorId > 0)
                    {
                        var identityAdv = new ClaimsIdentity(context.Options.AuthenticationType);
                        identityAdv.AddClaim(new Claim("UserID", Convert.ToString(usr.userId)));
                        identityAdv.AddClaim(new Claim("advisorID", Convert.ToString(usr.advisorId)));
                        identityAdv.AddClaim(new Claim("ClientID", "0"));
                        identityAdv.AddClaim(new Claim("ProfileID", "0"));
                        identityAdv.AddClaim(new Claim(ClaimTypes.Name, Convert.ToString(usr.userName)));
                        if (usr.isAdmin == true)
                        {
                            identityAdv.AddClaim(new Claim(ClaimTypes.Role, "Admin"));
                        }
                        identityAdv.AddClaim(new Claim(ClaimTypes.Role, "Advisor"));
                        identityAdv.AddClaim(new Claim(ClaimTypes.Email, Convert.ToString(usr.emailPrimary)));
                        //var props = new AuthenticationProperties(new Dictionary<string, string>
                        //{
                        //    {
                        //        "UserID", Convert.ToString(usr.userId).ToString()
                        //    }
                        //});
                        //var ticket = new AuthenticationTicket(identityAdv, props);
                        //context.Response.Add("username", claim.Value);
                        context.Validated(identityAdv);
                    }
                    else if (usr.clientId > 0)
                    {
                        var identityAdv = new ClaimsIdentity(context.Options.AuthenticationType);
                        identityAdv.AddClaim(new Claim("UserID", Convert.ToString(usr.userId)));
                        identityAdv.AddClaim(new Claim("advisorID", "0"));
                        identityAdv.AddClaim(new Claim("ClientID", Convert.ToString(usr.clientId)));
                        identityAdv.AddClaim(new Claim("ProfileID", Convert.ToString(usr.profileId)));
                        identityAdv.AddClaim(new Claim(ClaimTypes.Name, Convert.ToString(usr.userName)));
                        identityAdv.AddClaim(new Claim(ClaimTypes.Role, "Client"));
                        identityAdv.AddClaim(new Claim(ClaimTypes.Email, Convert.ToString(usr.emailPrimary)));
                        //var props = new AuthenticationProperties(new Dictionary<string, string>
                        //{
                        //    {
                        //        "UserID", Convert.ToString(usr.userId).ToString()
                        //    }
                        //});
                        //var ticket = new AuthenticationTicket(identityAdv, props);
                        context.Validated(identityAdv);
                    }
                    else
                    {
                        context.SetError("Invalid Entry", "Username and Password is invalid");
                        return;
                    }
                }
                else
                {
                    context.SetError("Invalid Entry", "Username and Password is invalid");
                    return;
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                context.SetError("Logical Error", msg.Message.ToString());
                return;
            }
        }
    }
}