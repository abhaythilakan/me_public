﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using DLayer;
using ME.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace ME.ControllerApi
{
    public class CollegeController : ApiController
    {
        [HttpPost]
        [AllowAnonymous]
        [Route("api/College/collegeSearchName")]
        public IHttpActionResult collegeSearchName([FromBody]  CollegeListArgs col)
        {
            long userID = 0;
            List<AllCollegeModel> collegList = new List<AllCollegeModel>();
            try
            {

                if (ModelState.IsValid)
                {
                    collegList = BLayer.College.collegeSearchName(col);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(collegList));
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/College/collegeSearchState")]
        public IHttpActionResult collegeSearchState([FromBody]  CollegeListArgs col)
        {
            long userID = 0;
            List<AllCollegeModel> collegList = new List<AllCollegeModel>();
            try
            {

                if (ModelState.IsValid)
                {
                    collegList = BLayer.College.collegeSearchState(col);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(collegList));
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/College/collegeSearchAll")]
        public IHttpActionResult collegeSearchAll([FromBody] CollegeListArgs col)
        {
            col.searchValue = null;
            List<AllCollegeModel> collegList = new List<AllCollegeModel>();
            try
            {

                if (ModelState.IsValid)
                {
                    collegList = BLayer.College.collegeSearchAll(col);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(collegList));
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/College/saveFaveColleges")]
        public IHttpActionResult saveFaveColleges([FromBody]  AdvisorFavList favList)
        {
            long userID = 0;
            bool status = false;
            long id=0;
            List<AllCollegeModel> collegList = new List<AllCollegeModel>();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();

                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                    using (var db = Connection.getConnect())
                    {
                        var a = db.Users.Where(s => s.userId == userID).SingleOrDefault();

                        id = Convert.ToInt64(a.advisorId);
                    }
                }

                if (ModelState.IsValid)
                {
                    status = BLayer.College.saveFaveCollege(favList, id);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(status));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/College/RemoveFavCollege")]
        public IHttpActionResult removeFavCollege([FromBody]  AdvisorFavList favList)
        {
            long userID = 0;
            long advisorID = 0;
            bool status = false;

           // List<AllCollegeModel> collegList = new List<AllCollegeModel>();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                var claim2 = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "advisorID").FirstOrDefault();
                if (claim != null && claim2 !=null)
                {
                    userID = Convert.ToInt64(claim.Value);
                    advisorID = Convert.ToInt64(claim2.Value);
                    favList.advisorId = advisorID;
                    if (ModelState.IsValid)
                    {
                        status = BLayer.College.RemoveFavCollege(favList, userID);
                    }
                    else
                    {
                        var msg = CustomErrorHandler.getValidationErrors(ModelState);
                        return Ok(msg);
                    }
                }

               

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(status));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/College/FavCollegeList")]
        public IHttpActionResult FavCollegeList()
        {
            long userID = 0; long id = 0;
            List<DefaultCollege> collegList = new List<DefaultCollege>();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                    using (var db = Connection.getConnect())
                    {

                        if (ModelState.IsValid)
                        {
                            var a = db.Users.Where(s => s.userId == userID).SingleOrDefault();
                            if (a.advisorId > 0)
                            {
                                id = Convert.ToInt64(a.advisorId);
                                collegList = BLayer.College.advisorFavListList(id);
                            }
                            else if ((a.clientId > 0))
                            {
                                id = Convert.ToInt64(a.clientId);
                                collegList = BLayer.College.ClientFavListList(id);
                            }

                            }
                        else
                        {
                            var msg = CustomErrorHandler.getValidationErrors(ModelState);
                            return Ok(msg);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(collegList));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/College/AvgDefaultCollegeList")]
        public IHttpActionResult AvgDefaultCollegeList()
        {
            long userID = 0; long id = 0;
            List<AllCollegeModel> collegList = new List<AllCollegeModel>();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                    using (var db = Connection.getConnect())
                    {

                        if (ModelState.IsValid)
                        {
                            var a = db.Users.Where(s => s.userId == userID).SingleOrDefault();
                         collegList = BLayer.College.FavDefaultCollegeList();
                           
                        }
                        else
                        {
                            var msg = CustomErrorHandler.getValidationErrors(ModelState);
                            return Ok(msg);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(collegList));
        }

    }
}
