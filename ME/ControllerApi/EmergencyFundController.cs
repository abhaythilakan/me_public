﻿using ME.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CLayer;
using CLayer.common;
using CLayer.DBModel;
using System.Security.Claims;

namespace ME.ControllerApi
{
    public class EmergencyFundController : ApiController
    {
        private long profileID;
        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/EmergencyFund/GetFundTypes")]
        public IHttpActionResult GetEmergencyTypes()
        {
            EmergencyFundTypes model = new EmergencyFundTypes();
            try
            {
                
                    model = BLayer.EmergencyFund.getEmergencyTypes();
                
                
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }


        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/EmergencyFund/GetFund")]
        public IHttpActionResult GetEmergencyFund([FromBody] long profileID)
        {
            long profileID2 = 0;
            EmergencyFundModel model = new EmergencyFundModel();
            var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "ProfileID").FirstOrDefault();
            if (claim != null)
            {
                profileID2 = Convert.ToInt64(claim.Value);
            }

            try
            {
                if (profileID > 0)
                {
                    model = BLayer.EmergencyFund.getEmergencyFund(profileID);
                }
                else
                {
                    var msg = CustomErrorHandler.getProfileValidationErrors();
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }

        //[HttpPost]
        //[Common.Authorize(Roles = "Admin,Advisor,Client")]
        //[Route("api/EmergencyFund/CheckGoal")]
        //public IHttpActionResult GetEFundAtGo([FromBody] decimal targetAmount)
        //{

        //    var Identity = (ClaimsIdentity)User.Identity;
        //    var proID = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
        //    profileID = Convert.ToInt64(proID);
        //    profileID = 12;
        //    EmergencyFundModel model = new EmergencyFundModel();
        //    try
        //    {
        //        if (profileID > 0)
        //        {
        //            model = BLayer.EmergencyFund.getEFundAtGo(profileID, targetAmount);
        //        }
        //        else
        //        {
        //            var msg = CustomErrorHandler.getProfileValidationErrors();
        //            return Ok(msg);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        var msg = CustomErrorHandler.getLogicErrors(ex);
        //        return Ok(msg);
        //    }
        //    return Ok(ResultFunction.SetReturnModel(model));
        //}
        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/EmergencyFund/GoFund")]
        public IHttpActionResult GoEmergencyFund(EmergencyFundModel ef)
        {
            long profileID2 = 0;
            var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "ProfileID").FirstOrDefault();
            if (claim != null)
            {
                profileID2 = Convert.ToInt64(claim.Value);
            }
            // profileID = 30047;
            if (claim != null)
            {
                profileID2 = Convert.ToInt64(claim.Value);
            }
            try
            {
                if (ModelState.IsValid)
                {
                    if (ef.profileID > 0)
                    {
                        profileID = (ef.profileID)?? profileID2;
                        ef = BLayer.EmergencyFund.getMonthlySavings(ef, profileID);
                    }
                    else
                    {
                        var msg = CustomErrorHandler.getProfileValidationErrors();
                        return Ok(msg);
                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(ef));
        }



    }
}
