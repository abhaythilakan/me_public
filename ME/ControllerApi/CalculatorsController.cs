﻿using ME.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CLayer;
using CLayer.common;
using System.Security.Claims;
using System.Web.Http.ModelBinding;

namespace ME.ControllerApi
{
    public class CalculatorsController : ApiController
    {
        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/EarlyInvestor")]
        public IHttpActionResult EarlyInvestor([FromBody] EarlyInvestorModel inObj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    inObj = BLayer.Calculators.getEarlyInvestor(inObj);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/getEarlyInvestor")]
        public IHttpActionResult getEarlyInvestor()
        {
            EarlyInvestorModel inObj = new EarlyInvestorModel()
            {
                anualInvestment = 5000,
                currentAge = 22,
                endYear = 40,
                rateOfreturn = 12,
                isGraphEnable = true

            };
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/TaxableVsTaxDeffered")]
        public IHttpActionResult TaxableVsTaxDeffered([FromBody] TaxableVsTaxDefferedModel inObj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    inObj = BLayer.Calculators.TaxableVsTaxDeffered(inObj);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/getTaxableVsTaxDeffered")]
        public IHttpActionResult GetTaxableVsTaxDeffered()
        {
            TaxableVsTaxDefferedModel inObj = new TaxableVsTaxDefferedModel()
            {
                amount = 100000,
                annualInvest = 0,
                fTaxRateA = 15,
                fTaxRateB = 15,
                taxRate = 10,
                taxFreeRate = 10,
                defTaxRate = 10,
                olTaxRateA = 0,
                olTaxRateB = 0,
                sTaxRateA = 0,
                sTaxRateB = 0,
                years = 20

            };

            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/DIMECalculator")]
        public IHttpActionResult DIME([FromBody] DIMEModel inObj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    inObj = BLayer.Calculators.DIME(inObj);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/getDIME")]
        public IHttpActionResult getDIME()
        {
            DIMEModel inObj = new DIMEModel()
            {
                pName = "",
                prAge = 22,
                sName = "",
                sAge = 22,
                pMonthlyNeed = 2000,
                sMonthlyNeed = 2000,
                deathExpenses = 15000,
                debtAmount = 0,
                mortgageBalance = 100000,
                educationNeeds = 15000,
                inflationRate = 3,
                returnRate = 6,
                isEducationNeedsIncluded = true,
                isDeathExpensesIncluded = true,
                isDebtAmountIncluded = true,
                ismortgageBalanceIncluded = true,
                isPrimaryIncluded = true,
                isSpouseIncluded = true
            };
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/getCompoundInterest")]
        public IHttpActionResult getCompoundInterest()
        {
            CompoundInterestModel model = new CompoundInterestModel()
            {
                initialStartInvestment = 1000,
                monthlyInvestment = 100,
                interestRate1 = 3,
                interestRate2 = 6,
                interestRate3 = 12,
                no_of_years = 25

            };
            return Ok(ResultFunction.SetReturnModel(model));

        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/FutureValue")]
        public IHttpActionResult CompoundInterestAtGo([FromBody]CompoundInterestModel model)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    model = BLayer.Calculators.compoundInterestAtGo(model);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model));

        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/getGoals")]
        public IHttpActionResult getGoalsAndDreams()
        {
            GoalsAndDreamsModel model = new GoalsAndDreamsModel()
            {
                inflationRate = 3,
                returnRate1 = 3,
                returnRate2 = 6,
                returnRate3 = 12
            };
            return Ok(ResultFunction.SetReturnModel(model));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/GoalsAtGo")]
        public IHttpActionResult GoalsAndDreamsAtGo([FromBody]GoalsAndDreamsModel model)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    model = BLayer.Calculators.GoalsAndDreamsAtGo(model);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model));

        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/creditCardCalculator")]
        public IHttpActionResult creditCardCalculator([FromBody] CreditCardModel inObj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    inObj = BLayer.Calculators.creditCardCalculator(inObj);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/creditCardAmmortization")]
        public IHttpActionResult creditCardAmmortization([FromBody] CreditCardModel inObj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    inObj = BLayer.Calculators.creditCardAmmortCal(inObj);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/getcreditCardCalculator")]
        public IHttpActionResult getcreditCardCalculator()
        {
            CreditCardModel outobj;
            try
            {
                outobj = new CreditCardModel()
                {
                    cardMinPay = 15,
                    cName = "",
                    loanAmount = 10000,
                    monName = System.DateTime.Now.Month,
                    years = System.DateTime.Now.Year,
                    percentPay = 2,
                    intRate = 18
                };

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(outobj));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/getmortgageInterest")]
        public IHttpActionResult getMortgageInterestCalculator()
        {
            Mortgage_PI_Model modelObj;
            try
            {
                modelObj = new Mortgage_PI_Model()
                {
                    loanAmount = 100000,
                    intRate = 6,
                    no_of_terms = 360,
                    monName = System.DateTime.Now.Month,
                    years = System.DateTime.Now.Year,
                    prinicipal_Interest_Pay = 0,
                    currentBalance = 0

                };

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(modelObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/mortgageInterest")]
        public IHttpActionResult mortgageInterestCalculator([FromBody] Mortgage_PI_Model modelObj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    modelObj = BLayer.Calculators.mortgageInterestCalculator(modelObj);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(modelObj));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/getmortgagePayment")]
        public IHttpActionResult getmortgagePaymentCalculator()
        {
            MortgagePaymentModel modelObj;
            try
            {
                modelObj = new MortgagePaymentModel()
                {
                    loanAmount = 100000,
                    intRate = 6,
                    no_of_terms = 360,
                    monName = System.DateTime.Now.Month,
                    years = System.DateTime.Now.Year,
                    propertyTax = 2000,
                    homeInsurance = 1000,
                    monthlyPMI = 40
                };

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(modelObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/mortgagePayment")]
        public IHttpActionResult mortgagePaymentCalculator([FromBody] MortgagePaymentModel modelObj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    modelObj = BLayer.Calculators.mortgagePaymentCalculator(modelObj);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(modelObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/VehicleLoanCalculator")]
        public IHttpActionResult VehicleLoanCalculator([FromBody] VehicleLoanModel inObj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    inObj = BLayer.Calculators.vehicleloancalculator(inObj,0);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/VehicleLoanAmortizationCalc")]
        public IHttpActionResult VehicleLoanAmortizationCalc([FromBody] VehicleLoanModel inObj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    inObj = BLayer.Calculators.vehicleloancalculator(inObj,1);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }
   
        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/GetVehicleLoanCalculator")]
        public IHttpActionResult GetVehicleLoanCalculator()
        {
            VehicleLoanModel inObj;
            try
            {
                inObj = new VehicleLoanModel()
                {
                    purchasePrice = 25000,
                    cashDown = 2500,
                    drpCashDown = 10,
                    tradeAllowance = 0,
                    salesTaxRate = 8,
                    intRate = 6,
                    noOfInstl = 48,
                    monName = 1,
                    years = System.DateTime.Now.Year
                };

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/FixedvsMinimumCalculator")]
        public IHttpActionResult FixedvsMinimumCalculator([FromBody] FixedvsMinimumCalcModel inObj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    inObj = BLayer.Calculators.FixedvsMinimumCalculator(inObj);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/FixedvsMinimumCalculator")]
        public IHttpActionResult FixedvsMinimumAmmortisation([FromBody] FixedvsMinimumCalcModel inObj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    inObj = BLayer.Calculators.FixedvsMinimumAmort(inObj);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/GetfixedvsMinimumCalculator")]
        public IHttpActionResult getfixedvsMinimumCalculator()
        {
            FixedvsMinimumCalcModel inObj;
            try
            {
                inObj = new FixedvsMinimumCalcModel()
                {
                    cardMinPay = 15,
                    loanAmount = 10000,
                    intRate = 18,
                    minPay = 200,
                    percentPay = 2,
                    monName = 1,
                    years = System.DateTime.Now.Year
                };
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/GetCollegeCostCalculator")]
        public IHttpActionResult GetCollegeCostCalculator()
        {

            CollegecostListArg inObj;
            try
            {
                inObj = new CollegecostListArg()
                {
                    name = null,
                    state = null,
                    inState = true,
                    outState = true,
                    PageNo = 1,
                    PageSize = 20
                };
                inObj.nationalColleges = new List<CollegeCostResultModel>();
                inObj.nationalColleges = BLayer.Calculators.getcollegeCost();
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/CollegeCostCalculator")]
        public IHttpActionResult CollegeCostCalculator([FromBody] CollegecostListArg inobj)
        {

            var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
            try
            {
                if (ModelState.IsValid)
                {
                    inobj = BLayer.Calculators.collegecostList(inobj);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(inobj));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/getMortgageMissingValue")]
        public IHttpActionResult getMortgageMissingValue()
        {
            MortgageMissingValueModel modelObj;
            try
            {
                modelObj = new MortgageMissingValueModel()
                {
                    loanAmount = 100000,
                    intRate = 6,
                    no_of_terms = 360,
                    monName = System.DateTime.Now.Month,
                    years = System.DateTime.Now.Year,
                    monthlyPayment = 599.55,
                    loanAmountB = true,
                    intRateB = true,
                    no_of_termsB = true,
                    monthlyPaymentB = false
                };

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(modelObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/mortgageMissingValueCalculate")]
        public IHttpActionResult mortgageMissingValueCalculate([FromBody] MortgageMissingValueModel modelObj)
        {
            try
            {
                if (modelObj.loanAmountB == false)
                {
                    this.ModelState.Remove("modelObj.loanAmount");

                }
                else if (modelObj.intRateB == false)
                {

                    this.ModelState.Remove("modelObj.intRate");
                }
                else if (modelObj.no_of_termsB == false)
                {

                    this.ModelState.Remove("modelObj.no_of_terms");
                }
                else if (modelObj.monthlyPaymentB == false)
                {
                    this.ModelState.Remove("modelObj.monthlyPayment");
                }

                if (ModelState.IsValid)
                {
                    modelObj = BLayer.Calculators.missingValueCalculate(modelObj);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(modelObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Calculators/mortgageAmortization")]
        public IHttpActionResult mortgageAmortization([FromBody] Mortgage_PI_Model inobj)
        {
            mortgageAmortizationCalculator mc = new mortgageAmortizationCalculator();
            var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
            try
            {
                if (ModelState.IsValid)
                {
                    mc = BLayer.Calculators.mortChart(inobj);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(mc));
        }

    }
}

