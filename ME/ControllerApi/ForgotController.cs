﻿using Heijden.DNS;
using ME.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Web.Http;

namespace ME.ControllerApi
{
    public class ForgotController : ApiController
    {
        [HttpGet]
        public IHttpActionResult validateEmailNetwork(string strInputEmail)
        {
            EmailValidator ev = new EmailValidator();
            bool valid = false;
            valid = ev.ValidateEmail(strInputEmail, ValidationMode.Network);
            return Ok(valid);
        }

        public IHttpActionResult validEmail(string address)
        {
            //string[] host = (address.Split('@'));
            //string hostname = host[1];

            //IPHostEntry IPhst = Dns.Resolve(hostname);
            //IPEndPoint endPt = new IPEndPoint(IPhst.AddressList[0], 25);
            //Socket s = new Socket(endPt.AddressFamily,
            //             SocketType.Stream, ProtocolType.Tcp);
            //s.Connect(endPt);

            ////Attempting to connect
            //if (!Check_Response(s, SMTPResponse.CONNECT_SUCCESS))
            //{
            //    s.Close();
            //    return false;
            //}

            ////HELO server
            //Senddata(s, string.Format("HELO {0}\r\n", Dns.GetHostName()));
            //if (!Check_Response(s, SMTPResponse.GENERIC_SUCCESS))
            //{
            //    s.Close();
            //    return false;
            //}

            ////Identify yourself
            ////Servers may resolve your domain and check whether 
            ////you are listed in BlackLists etc.
            //Senddata(s, string.Format("MAIL From: {0}\r\n",
            //     "testexample@deepak.portland.co.uk"));
            //if (!Check_Response(s, SMTPResponse.GENERIC_SUCCESS))
            //{
            //    s.Close();
            //    return false;
            //}


            ////Attempt Delivery (I can use VRFY, but most 
            ////SMTP servers only disable it for security reasons)
            //Senddata(s, address);
            //if (!Check_Response(s, SMTPResponse.GENERIC_SUCCESS))
            //{
            //    s.Close();
            //    return false;
            //}
            //return (true);
            return Ok();
        }

        public bool IsValidEmail(string email="abhay@focaloid.com")
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
