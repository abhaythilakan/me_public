﻿using ME.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CLayer;
using CLayer.common;
using System.Security.Claims;

namespace ME.ControllerApi
{
    public class RetirementController : ApiController
    {
        // GET: RetirementGoal
        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/RetirementGoal/GetRetirementDetails")]
        public IHttpActionResult GetRetirementDetails([FromBody] long profileID)
        {
            RetirementModel outobj = new RetirementModel();

            try
            {
                outobj = BLayer.RetirementGoal.GetRetirementDetails(profileID);


            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outobj));
        }
        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/RetirementGoal/GetRetirementDetailsGo")]
        public IHttpActionResult GetRetirementDetailsGo([FromBody] RetirementModel inobj)
        {

            try
            {
                inobj = BLayer.RetirementGoal.GetRetirementDetailsGo(inobj);


            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(inobj));
        }
    }
}