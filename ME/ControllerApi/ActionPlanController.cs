﻿using CLayer;
using CLayer.common;
using ME.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using CLayer.DBModel;


namespace ME.ControllerApi
{
    public class ActionPlanController : ApiController
    {
        private long profileID;
        private long userID;

        [HttpGet]
        [Route("api/actionPlan/predefineActions")]
        public IHttpActionResult getPredefinedActions()
        {
            List<preDefinedActionModel> data = new List<preDefinedActionModel>();
            try
            {
                if (ModelState.IsValid)
                {
                    data = BLayer.ActionPlanInfo.getPredefinedActions();
                }
                else
                {
                    var msg = CustomErrorHandler.getProfileValidationErrors();
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(data));
        }

        [HttpPost]
        [Route("api/actionPlan/assignedToDDL")]
        public IHttpActionResult getAssignedToDDl([FromBody] long profileID)
        {
            List<DropDownList_Int> ddl = new List<DropDownList_Int>();
            try
            {
                ddl = BLayer.ActionPlanInfo.ddlAssignedTO(profileID);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(ddl));
        }

        [HttpPost]
        [Route("api/actionPlan/saveAction")]
        public IHttpActionResult saveActionPlan(actionPointModel ap)
        {
            bool status, status1;
            var Identity = (ClaimsIdentity)User.Identity;
            //  var proID = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
            var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
            // profileID = Convert.ToInt64(proID);
            userID = Convert.ToInt64(usrID);
            try
            {
                try
                {
                    DateTime date = ap.dueDate.Value;
                }
                catch (Exception ex)
                {
                    throw new Exception("Due date is not valid");
                }
                if (ap.assignedTo > 0)
                {
                    this.ModelState.Remove("ap.otherActions.otherID");
                    this.ModelState.Remove("ap.otherActions.otherPersonName");
                    this.ModelState.Remove("ap.otherActions.otherEmailID");

                }
                if (ModelState.IsValid)
                {
                    ap = BLayer.ActionPlanInfo.saveActionPoint(ap, userID);

                    try
                    {
                        string comment = ap.comment +","+"Due Date:"+ ap.dueDate;
                       // comment += "Due Date:" + ap.dueDate;
                        if (ap.assignedTo == Convert.ToInt32(CustomEnum.ActionAssignedTO.Advisor))
                        {
                            status = sendEmail(ap.actionName, ap.comment, ap.advisorEmail, ap.fromUser);
                        }
                        else if (ap.assignedTo == Convert.ToInt32(CustomEnum.ActionAssignedTO.Primary))
                        {
                            status = sendEmail(ap.actionName, comment, ap.primaryEmail, ap.fromUser);
                        }
                        else if (ap.assignedTo == Convert.ToInt32(CustomEnum.ActionAssignedTO.Spouse))
                        {
                            status = sendEmail(ap.actionName, comment, ap.spouseEmail, ap.fromUser);
                        }
                        else if (ap.assignedTo == Convert.ToInt32(CustomEnum.ActionAssignedTO.Other))
                        {
                            status = sendEmail(ap.actionName, comment, ap.otherActions.otherEmailID, ap.fromUser);
                        }
                        else if (ap.assignedTo == Convert.ToInt32(CustomEnum.ActionAssignedTO.Both))
                        {
                            status = sendEmail(ap.actionName, comment, ap.primaryEmail, ap.fromUser);
                            status1 = sendEmail(ap.actionName, comment, ap.spouseEmail, ap.fromUser);
                        }
                    }
                    catch (Exception ex)
                    {
                        var msg = CustomErrorHandler.mailSendUnSuccessfully();
                        return Ok(msg);
                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(ap));
        }
        private bool sendEmail(string actionName, string comment, string toEmail, string fromUser)
        {
            try
            {
                MailService.sendMail(actionName, comment, toEmail, fromUser);
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [HttpPost]
        [Route("api/actionPlan/GetAllActions")]
        public IHttpActionResult SearchAllActions([FromBody]long profileID)
        {
            var Identity = (ClaimsIdentity)User.Identity;
            // var proID = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
            // profileID = Convert.ToInt64(proID);
            List<actionList> dataList = new List<actionList>();
            // profileID = 1;
            try
            {
                if (profileID > 0)
                {
                    if (ModelState.IsValid)
                    {
                        dataList = BLayer.ActionPlanInfo.GetAllActions(profileID);
                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }

            return Ok(ResultFunction.SetReturnModel(dataList));
        }
        [HttpPost]
        [Route("api/actionPlan/all_Today")]
        public IHttpActionResult all_Today([FromBody] long profileID)
        {
            var Identity = (ClaimsIdentity)User.Identity;
            List<actionList> dataList = new List<actionList>();
            try
            {
                if (ModelState.IsValid)
                {
                    dataList = BLayer.ActionPlanInfo.all_Today(profileID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }

            return Ok(ResultFunction.SetReturnModel(dataList));
        }
        [HttpPost]
        [Route("api/actionPlan/all_ThisMonth")]
        public IHttpActionResult all_ThisMonth([FromBody] long profileID)
        {
            var Identity = (ClaimsIdentity)User.Identity;
            List<actionList> dataList = new List<actionList>();
            try
            {
                if (ModelState.IsValid)
                {
                    dataList = BLayer.ActionPlanInfo.all_ThisMonth(profileID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }

            return Ok(ResultFunction.SetReturnModel(dataList));
        }

        [HttpPost]
        [Route("api/actionPlan/pasttDueActions")]
        public IHttpActionResult pastDueActions([FromBody] long profileID)
        {
            var Identity = (ClaimsIdentity)User.Identity;
            List<actionList> dataList = new List<actionList>();
            try
            {
                if (ModelState.IsValid)
                {
                    dataList = BLayer.ActionPlanInfo.pastDueActions(profileID);
                }

                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }

            return Ok(ResultFunction.SetReturnModel(dataList));
        }
        [HttpPost]
        [Route("api/actionPlan/upcomingActions")]
        public IHttpActionResult SearchupcomingActions([FromBody] long profileID)
        {
            var Identity = (ClaimsIdentity)User.Identity;
            List<actionList> dataList = new List<actionList>();
            try
            {
                if (ModelState.IsValid)
                {
                    dataList = BLayer.ActionPlanInfo.upcomingActions(profileID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }

            return Ok(ResultFunction.SetReturnModel(dataList));
        }

        [HttpPost]
        [Route("api/actionPlan/progressingActions")]
        public IHttpActionResult progressingActions([FromBody] long profileID)
        {
            var Identity = (ClaimsIdentity)User.Identity;
            List<actionList> dataList = new List<actionList>();
            try
            {
                if (ModelState.IsValid)
                {
                    dataList = BLayer.ActionPlanInfo.progressingActions(profileID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }

            return Ok(ResultFunction.SetReturnModel(dataList));
        }

        [HttpPost]
        [Route("api/actionPlan/completedActions")]
        public IHttpActionResult completedActions([FromBody] long profileID)
        {
            var Identity = (ClaimsIdentity)User.Identity;
            List<actionList> dataList = new List<actionList>();
            try
            {
                if (ModelState.IsValid)
                {
                    dataList = BLayer.ActionPlanInfo.completedActions(profileID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }

            return Ok(ResultFunction.SetReturnModel(dataList));
        }
        [HttpPost]
        [Route("api/actionPlan/markCompleted")]
        public IHttpActionResult markCompleted([FromBody] long actionID)
        {
            bool status=false;
            var Identity = (ClaimsIdentity)User.Identity;
            var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
            userID = Convert.ToInt64(usrID);
            try
            {
                if (ModelState.IsValid)
                {
                    status = BLayer.ActionPlanInfo.markCompleted(actionID, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }

            return Ok(ResultFunction.SetReturnModel(status));
        }
        [HttpPost]
        [Route("api/actionPlan/markInProgress")]
        public IHttpActionResult markInProgress([FromBody] long actionID)
        {
            bool status = false;
            var Identity = (ClaimsIdentity)User.Identity;
            var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
            userID = Convert.ToInt64(usrID);
            try
            {
                if (ModelState.IsValid)
                {
                    status = BLayer.ActionPlanInfo.markInProgress(actionID, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }

            return Ok(ResultFunction.SetReturnModel(status));
        }
        [HttpPost]
        [Route("api/actionPlan/getOnReshedule")]
        public IHttpActionResult getOnReshedule([FromBody] long actionID)
        {
            var Identity = (ClaimsIdentity)User.Identity;
            var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
            userID = Convert.ToInt64(usrID);
            actionPointModel ap = new actionPointModel();
            try
            {
                if (ModelState.IsValid)
                {
                    ap = BLayer.ActionPlanInfo.getOnReshedule(actionID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }

            return Ok(ResultFunction.SetReturnModel(ap));
        }



    }
}
