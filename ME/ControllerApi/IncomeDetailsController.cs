﻿using ME.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CLayer;
using CLayer.common;
using System.Security.Claims;

namespace ME.ControllerApi
{
    public class IncomeDetailsController : ApiController
    {
        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/IncomeDetails/saveTaxeDetails")]
        public IHttpActionResult saveTaxeDetails([FromBody] List<IncomeTaxDetailModel> inobj) //Future Save
        {
            long userID = 0;
            // IncomeOtherTaxModel outObj = new IncomeOtherTaxModel();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                int i = 0;
                foreach (var a in inobj)
                {
                    if (a.incomeID > 0)
                    {
                        this.ModelState.Remove("inobj[" + i + "].profileID");
                    }
                    i++;
                }
                if (ModelState.IsValid)
                {
                    inobj = BLayer.Income.saveTaxeDetails(inobj, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(inobj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/IncomeDetails/savepayrollTaxeDetails")]
        public IHttpActionResult savepayrollTaxeDetails([FromBody] List<IncomeTaxDetailModel> inobj)
        {
            long userID = 0;
            // IncomeOtherTaxModel outObj = new IncomeOtherTaxModel();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                int i = 0;
                foreach (var a in inobj)
                {
                    if (a.incomeID > 0)
                    {
                        this.ModelState.Remove("inobj[" + i + "].profileID");
                    }
                    i++;
                }
                if (ModelState.IsValid)
                {
                    inobj = BLayer.Income.savepayrollTaxeDetails(inobj, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(inobj));
        }//primary spouse

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/IncomeDetails/saveOtherTaxeDetails")]
        public IHttpActionResult saveOtherTaxeDetails([FromBody] List<IncomeTaxDetailModel> inobj)//other taxes
        {
            long userID = 0;
            // IncomeOtherTaxModel outObj = new IncomeOtherTaxModel();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }

                int i = 0;
                foreach (var a in inobj)
                {
                    if (a.profileID > 0)
                    {
                        this.ModelState.Remove("inobj[" + i + "].incomeID");
                    }
                    i++;
                }
                if (ModelState.IsValid)
                {
                    inobj = BLayer.Income.saveOtherTaxeDetails(inobj, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(inobj));
        }//for other taxes

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/IncomeDetails/GetIncomeDetailsAll")]
        public IHttpActionResult IncomeDetails([FromBody] long DetailsID)
        {
            List<IncomeModel> outObj = new List<IncomeModel>();
            try
            {
                // outObj = BLayer.Income.IncomeDetails(DetailsID);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/IncomeDetails/IncomeProjection")]
        public IHttpActionResult IncomeProjection([FromBody] long ProfileID)
        {
            decimal goRate = 0;
            IncomeProjectionObjectsModel outObj = new IncomeProjectionObjectsModel();
            try
            {
                outObj = BLayer.Income.incomeProjection(ProfileID);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/IncomeDetails/IncomeProjectionGo")]
        public IHttpActionResult IncomeProjectionGo([FromBody]  IncomeProjectionGoModel inobj)
        {
            //decimal goRate = 0;
            IncomeProjectionObjectsModel outObj = new IncomeProjectionObjectsModel();
            if (inobj.primaryRate == null)
            {
                inobj.primaryRate = 0;
            }
            if (inobj.spouseRate == null)
            {
                inobj.spouseRate = 0;
            }
            try
            {
                if (ModelState.IsValid)
                {
                    outObj = BLayer.Income.incomeProjection(inobj.profileID.Value, inobj.primaryRate.Value, inobj.spouseRate.Value);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }


            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/IncomeDetails/DeleteTaxes")]
        public IHttpActionResult DeleteTaxes([FromBody] long deleteID)
        {
            long userID = 0;
            bool status = false;
            // IncomeOtherTaxModel outObj = new IncomeOtherTaxModel();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }

                if (ModelState.IsValid)
                {
                    status = BLayer.Income.deleteTax(deleteID, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(status));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/IncomeDetails/DeleteFutureIncome")]
        public IHttpActionResult DeleteFutureIncome([FromBody] long deleteID)
        {
            long userID = 0;
            bool status = false;
            // IncomeOtherTaxModel outObj = new IncomeOtherTaxModel();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }

                if (ModelState.IsValid)
                {
                    status = BLayer.Income.deleteFutureincome(deleteID, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(status));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/IncomeDetails/GetMonthlyIncome")]
        public IHttpActionResult GetMonthlyIncome([FromBody] long profileID)
        {
            long profileID2 = 0;
            List<IncomeModel> outObj = new List<IncomeModel>();
            var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "ProfileID").FirstOrDefault();
            if (claim != null)
            {
                profileID2 = Convert.ToInt64(claim.Value);
            }
            try
            {
                outObj = BLayer.Income.GetIncomeDetails(profileID, CustomEnum.IncomeTypeEnum.MonthlyIncome);
            }

            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/IncomeDetails/getFutureIncomeDetails")]
        public IHttpActionResult GetFutureIncome([FromBody] long profileID)
        {
            List<IncomeModel> outObj = new List<IncomeModel>();
            try
            {

                outObj = BLayer.Income.GetIncomeDetails(profileID, CustomEnum.IncomeTypeEnum.FutureIncome);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/IncomeDetails/GetTaxes")]
        public IHttpActionResult getTaxes([FromBody] long DetailsID)
        {
            List<IncomeTaxDetailModel> outObj = new List<IncomeTaxDetailModel>();
            try
            {
                outObj = BLayer.Income.getTaxDetails(DetailsID);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/IncomeDetails/getotherTaxDetails")]
        public IHttpActionResult getotherTaxDetails([FromBody] long DetailsID)
        {
            IncomeOtherTaxModel outObj = new IncomeOtherTaxModel();
            try
            {
                outObj = BLayer.Income.getotherTaxDetails(DetailsID);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/IncomeDetails/saveMonthlyIncome")]
        public IHttpActionResult saveMonthlyIncome([FromBody] List<IncomeModel> inobj)
        {
            long userID = 0;
            // IncomeOtherTaxModel outObj = new IncomeOtherTaxModel();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }

                int i = 0;
                foreach (var a in inobj)
                {
                    if (a.incomeID > 0)
                    {
                        this.ModelState.Remove("inobj[" + i + "].startDate");
                        this.ModelState.Remove("inobj[" + i + "].endDate");
                        this.ModelState.Remove("inobj[" + i + "].name");
                        this.ModelState.Remove("inobj[" + i + "].profileID");
                        this.ModelState.Remove("inobj[" + i + "].grossMonthlyIncome");
                        inobj[i].cola = 0;
                    }
                    i++;
                }
                if (ModelState.IsValid)
                {
                    inobj = BLayer.Income.saveMonthlyIncome(inobj, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(inobj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/IncomeDetails/saveFutureIncome")]
        public IHttpActionResult saveFutureIncome([FromBody] List<IncomeModel> inobj)
        {
            long userID = 0;
            
            // IncomeOtherTaxModel outObj = new IncomeOtherTaxModel();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }

                int i = 0;
                foreach (var a in inobj)
                {
                    if (a.profileID > 0)
                    {
                        this.ModelState.Remove("inobj[" + i + "].payType");
                        this.ModelState.Remove("inobj[" + i + "].incomeID");
                        this.ModelState.Remove("inobj[" + i + "].name");
                        this.ModelState.Remove("inobj[" + i + "].payIncome");
                        if (a.startType != 0 && a.startType != null)
                        {
                            this.ModelState.Remove("inobj[" + i + "].startDate");
                  
                        }
                        if (a.endType != 0 && a.endType != null)
                        {
                            this.ModelState.Remove("inobj[" + i + "].endDate");
                        }
                    }
                    i++;
                }
                if (ModelState.IsValid)
                {
                    inobj = BLayer.Income.saveFutureIncome(inobj, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(inobj));
        }

       


    }
}
