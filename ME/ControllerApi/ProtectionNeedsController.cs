﻿using CLayer;
using CLayer.common;
using ME.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using CLayer.DBModel;
namespace ME.ControllerApi
{
    public class ProtectionNeedsController : ApiController
    {
        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/protection/protectionInfo")]
        public IHttpActionResult getProtectionInfo([FromBody]long profileID)
        {
            long userID = 0;
            ProtectionNeeds pm = new ProtectionNeeds();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                if (ModelState.IsValid)
                {

                    pm = BLayer.ProtectionNeedsInfo.getProtectionNeeds(profileID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(pm));
        }
        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/protection/goProtectionInfo")]
        public IHttpActionResult goProtectionInfo(ProtectionNeeds pm)
        {
            long userID = 0;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                int i = 0;


                if (ModelState.IsValid)
                {

                    pm = BLayer.ProtectionNeedsInfo.goProtectionNeeds(pm);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(pm));
        }
    }
}
