﻿using CLayer;
using CLayer.common;
using ME.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace ME.ControllerApi
{
    [Common.Authorize(Roles = "Admin, Advisor, Client")]
    public class ExpenseController : ApiController
    {
        private long userId;
        [HttpPost]
        [Route("api/Expense/getLivingExpense")]
        public IHttpActionResult getLivingExpense([FromBody]long profileId)
        {
            LivingExpenseModel model = new LivingExpenseModel();
            try
            {
                var Identity = (ClaimsIdentity)User.Identity;
               // var proId = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
               // profileId = Convert.ToInt64(proId);
                model = BLayer.Expense.GetLivingExpense(profileId);
            }
            catch (Exception ex)
            {
                return Ok(CustomErrorHandler.getLogicErrors(ex));
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }
        [HttpPost]
        [Route("api/Expense/saveLivingExpense")]
        public IHttpActionResult saveLivingExpense(List<ExpenseModel> inputList)
        {
            LivingExpenseModel model = new LivingExpenseModel();
            try
            {
                var identity = (ClaimsIdentity)User.Identity;
                var usrId = identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
                userId = Convert.ToInt64(usrId);
                if (ModelState.IsValid)
                {
                    model = BLayer.Expense.SaveLivingExpense(inputList, userId);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch(Exception ex)
            {
                return Ok(CustomErrorHandler.getLogicErrors(ex));
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }
        [HttpPost]
        [Route("api/Expense/deleteLivingExpense")]
        public IHttpActionResult deleteLivingExpense(ExpenseInputModel input)
        {
            LivingExpenseModel output = new LivingExpenseModel();
            try
            {
                var Identity = (ClaimsIdentity)User.Identity;
                //var proId = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
                var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
              //  profileId = Convert.ToInt64(proId);
                userId = Convert.ToInt64(usrID);
              //  input.profileId = profileId;
                if (ModelState.IsValid)
                {
                    output = BLayer.Expense.DeleteLivingExpense(input, userId);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(output));
        }
        [HttpGet]
        [Route("api/Expense/getExpenseCategory")]
        public IHttpActionResult getExpenseCategory()
        {
            List<DropDownList_Int> list = new List<DropDownList_Int>();
            try
            {
                list = BLayer.DropDownList.DDL_List_int(enumDropDownList.expenseCategory);
            }
            catch(Exception ex)
            {

                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(list));
        }

        [HttpGet]
        [Route("api/Expense/getExpenseSubCategory")]
        public IHttpActionResult getExpenseSubCategory(long id)
        {
            List<DropDownList_Int> list = new List<DropDownList_Int>();
            try
            {
                list = BLayer.DropDownList.DDL_List_int_withID(enumDropDownList.expenseSubCategory,id);
            }
            catch (Exception ex)
            {

                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(list));
        }
        [HttpGet]
        [Route("api/Expense/getExpenseType")]
        public IHttpActionResult getExpenseType(long id)
        {
            List<DropDownList_Int> list = new List<DropDownList_Int>();
            try
            {
                list = BLayer.DropDownList.DDL_List_int_withID(enumDropDownList.expenseType,id);
            }
            catch (Exception ex)
            {

                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(list));
        }
        [HttpPost]
        [Route("api/Expense/getAllExpense")]
        public IHttpActionResult getAllExpense([FromBody]long profileId)
        {
            ExpenseListModel model = new ExpenseListModel();
            try
            {
                var Identity = (ClaimsIdentity)User.Identity;
              //  var proId = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
               // profileId = Convert.ToInt64(proId);
                model = BLayer.Expense.GetAllExpenseList(profileId);
            }
            catch (Exception ex)
            {
                return Ok(CustomErrorHandler.getLogicErrors(ex));
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }

    }
}
