﻿using ME.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CLayer;
using CLayer.common;
using System.Security.Claims;

namespace ME.ControllerApi
{
    //[Common.Authorize(Roles = "Admin,Advisor,Client")]
    public class EducationController : ApiController
    {
        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Education/SaveEducationCurretSavingsDetails")]
        public IHttpActionResult SaveCurretSavingDetails(List<Education_DetailsModel> em)
        {
            long userID = 0;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                if (ModelState.IsValid)
                {

                    em = BLayer.Education.saveEducationDetails(em, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(em));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Education/deleteEducationCurretSavingsDetails")]
        public IHttpActionResult deleteCurretSavingDetails([FromBody] long id)
        {
            long userID = 0;
            bool status = false;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }

                status = BLayer.Education.deleteEducationDetails(id, userID);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(status));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Education/getEducationCurretSavingsDetails")]
        public IHttpActionResult getCurretSavingsDetails([FromBody] long educationID)
        {
            List<Education_DetailsModel> savingsList = new List<Education_DetailsModel>();
            try
            {

                savingsList = BLayer.Education.GetCurrentSavings(educationID);
            }

            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(savingsList));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Education/Gocalculation")]
        public IHttpActionResult Gocalculation(EducationModel em)
        {
            try
            {
                int i = 0;

                foreach (var a in em.EducationInfoModel)
                {
                    int j = 0 ;
                    foreach (var b in a.EducationDetails)
                    {

                        if (em.profileID > 0 && b.startDateType == 1)
                        {
                            this.ModelState.Remove("em.EducationInfoModel[" + i + "].EducationDetails[" + j + "].startDate");
                        }
                        else if (em.profileID > 0 && b.startDateType == 2)
                        {
                            this.ModelState.Remove("em.EducationInfoModel[" + i + "].EducationDetails[" + j + "].collegeStartAge");
                        }
                        j++;
                    }
                    i++;
                }
                if (ModelState.IsValid)
                {
                    em = BLayer.Education.gocalculation(em);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }          

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(em));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Education/GetEducationInfo")]
        public IHttpActionResult getSavedDetails([FromBody] long profileID)
        {
            EducationModel em = new EducationModel();
            try
            {
                em = BLayer.Education.EducationDetails(profileID);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(em));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Education/SaveEducationInfo")]
        public IHttpActionResult SaveEduInfo(EducationModel em)
        {
            long userID = 0;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                int j = 0;
                foreach (var obj in em.EducationInfoModel)
                {
                    int i = 0;
                    foreach (var a in obj.EducationDetails)
                    {
                        if (obj.profileID > 0 && a.startDateType == 1)
                        {
                            this.ModelState.Remove("em["+j+"].EducationDetails[" + i + "].startDate");
                        }
                        else if (obj.profileID > 0 && a.startDateType == 2)
                        {
                            this.ModelState.Remove("em[" + j + "].EducationDetails[" + i + "].collegeStartAge");
                        }
                        i++;
                    }
                    j++;
                }
                if (ModelState.IsValid)
                {

                    em = BLayer.Education.saveEducation(em, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(em));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Education/DeleteEducation")]
        public IHttpActionResult deleteEducation([FromBody] long id)
        {
            long userID = 0;
            bool status = false;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }

                status = BLayer.Education.deleteEducation(id, userID);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(status));
        }


    }
}
