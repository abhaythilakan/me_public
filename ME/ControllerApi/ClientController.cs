﻿using CLayer;
using CLayer.common;
using ME.Common;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.Security;
using static CLayer.common.CustomEnum;

namespace ME.ControllerApi
{
    [Common.Authorize(Roles = "Admin, Advisor, Client")]
    public class ClientController : ApiController
    {

        private Microsoft.Owin.Security.IAuthenticationManager Authentication
        {
            get { return Request.GetOwinContext().Authentication; }
        }

        [HttpPost]
        [Route("api/Client/showClientProfile")]
        public IHttpActionResult showClientProfile(ClientShow model)
        {
            ClientCollection data = new ClientCollection();
            try
            {
                data.usrCat = new UserCat();
                data.usrCat.usrCat = (int)CustomEnum.userCatEnum.Client;
                if (ModelState.IsValid)
                {
                    data = BLayer.ClientReg.getClientDetails(model.clientId, true);
                    if (data != null)
                    {
                        var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                        var identity = (ClaimsIdentity)User.Identity;

                        identity.RemoveClaim(identity.FindFirst("ClientID"));
                        identity.AddClaim(new Claim("ClientID", Convert.ToString(model.clientId)));
                        identity.RemoveClaim(identity.FindFirst("ProfileID"));
                        identity.AddClaim(new Claim("ProfileID", Convert.ToString(model.profileId)));
                        identity.AddClaim(new Claim("Profile", "Demo123"));

                        authenticationManager.AuthenticationResponseGrant = new AuthenticationResponseGrant(
                            new ClaimsPrincipal(identity),
                            new AuthenticationProperties { IsPersistent = true });
                        authenticationManager.SignIn();
                        //authenticationManager.SignOut();
                        var identity123 = (ClaimsIdentity)User.Identity;
                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(data));
        }

        [HttpPost]
        [Route("api/Client/saveClient")]
        public IHttpActionResult saveClient([FromBody]ClientModel model)
        {
            ClientModel data = new ClientModel();
            try
            {
                var Identity = (ClaimsIdentity)User.Identity;
                var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
                long UserID = Convert.ToInt64(usrID);
                if (model.clientId > 0)
                {
                    ModelState.Remove("password");
                    ModelState.Remove("userName");
                }
                if (ModelState.IsValid)
                {
                    if (UserID > 0)
                    {
                        data = BLayer.ClientReg.saveClient(model, UserID);
                    }
                    else
                    {
                        Messages msg = new Messages();
                        msg.apiStatus = (int)apiStatusEnum.ValidationError;//logic errors
                        msg.Message = "Invalid logged in User";
                        return Ok(msg);
                    }
                    try
                    {
                        if (Convert.ToString(model.imagePath).Trim() != "")
                        {
                            string authority = HttpContext.Current.Request.Url.Authority;
                            string imagePath = model.imagePath.Replace("http://" + authority + "/", "");
                            if (model.imagePath.Contains("\\Temp\\"))
                            {
                                string FN = DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss.fff");
                                FN = Regex.Replace(FN, "[.|-|:|/]", "");
                                string newdirectory = "Files\\Client\\" + data.clientId + "\\Profile\\ProfileImage_" + FN + ".jpg";

                                string sourceLoc = System.Web.HttpContext.Current.Server.MapPath("~\\" + imagePath);
                                string targetLoc = System.Web.HttpContext.Current.Server.MapPath("~\\" + newdirectory);
                                if (!System.IO.Directory.Exists(targetLoc))
                                {
                                    System.IO.Directory.CreateDirectory(targetLoc);
                                }
                                System.IO.File.Copy(sourceLoc, targetLoc, true);
                                //Array.ForEach(Directory.GetFiles(@"" + System.Web.HttpContext.Current.Server.MapPath("~\\" + imagePath)), System.IO.File.Delete);
                                if ((System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath("~\\" + imagePath))))
                                {
                                    System.IO.File.Delete(System.Web.HttpContext.Current.Server.MapPath("~\\" + imagePath));
                                }
                                data.imagePath = newdirectory;//"http://" + authority + "/" + 
                                //model.imagePath = "http://" + authority + "/" + newdirectory;
                                model.imagePath = newdirectory;
                                BLayer.ClientReg.saveClientProfile(data.clientId, data.imagePath);
                                data.imagePath = "http://" + authority + "/" + newdirectory;
                            }
                        }
                        #region Image Save
                        //if (model.imageString != null && model.imageString.Trim() != "" && data.clientId > 0)
                        //{
                        //    byte[] bytes = Convert.FromBase64String(model.imageString);
                        //    Image images;
                        //    using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
                        //    {
                        //        ms.Write(bytes, 0, bytes.Length);
                        //        images = Image.FromStream(ms, true);
                        //    }
                        //    Bitmap bitmap = new Bitmap(images);
                        //    int sourceWidth = bitmap.Width;
                        //    //Get the image current height
                        //    int sourceHeight = bitmap.Height;

                        //    float nPercent = 0;
                        //    float nPercentW = 0;
                        //    float nPercentH = 0;
                        //    nPercentW = ((float)200 / (float)sourceWidth);

                        //    nPercentH = ((float)200 / (float)sourceHeight);

                        //    if (nPercentH < nPercentW)
                        //        nPercent = nPercentH;
                        //    else
                        //        nPercent = nPercentW;

                        //    int destWidth = (int)(sourceWidth * nPercent);

                        //    int destHeight = (int)(sourceHeight * nPercent);
                        //    Bitmap b = new Bitmap(destWidth, destHeight);
                        //    Graphics g = Graphics.FromImage((System.Drawing.Image)b);
                        //    g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                        //    g.DrawImage(bitmap, 0, 0, destWidth, destHeight);
                        //    g.Dispose();
                        //    Image images2;
                        //    images2 = (System.Drawing.Image)b;
                        //    string FILE_PATH = null;
                        //    if (images2 != null)
                        //    {
                        //        if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\" + data.clientId + "\\Profile\\")))
                        //        {
                        //            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\" + data.clientId + "\\Profile\\"));
                        //        }
                        //        Array.ForEach(Directory.GetFiles(@"" + System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\" + data.clientId + "\\Profile\\")), System.IO.File.Delete);
                        //        string FN = DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss.fff");
                        //        FN = Regex.Replace(FN, "[.|-|:|/]", "");
                        //        string newdirectory = "Files\\Client\\" + data.clientId + "\\Profile\\ProfileImage_" + FN + ".jpg";
                        //        if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\" + data.clientId + "\\Profile\\")))
                        //        {
                        //            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\" + data.clientId + "\\Profile\\"));
                        //        }
                        //        FILE_PATH = newdirectory;
                        //        images2.Save(System.Web.HttpContext.Current.Server.MapPath("~") + "\\" + FILE_PATH);
                        //        model.imagePath = FILE_PATH;
                        //        BLayer.ClientReg.saveClientProfile(data.clientId, model.imagePath);
                        //    }
                        //}
                        #endregion
                    }
                    catch (Exception ex)
                    {

                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
                //throw;
            }
            return Ok(ResultFunction.SetReturnModel(data));
        }

        [HttpPost]
        [Route("api/Client/saveClientDetails")]
        public IHttpActionResult saveClientDetails([FromBody]ClientCollection1 model)
        {
            ClientCollection1 data = new ClientCollection1();
            try
            {
                model.usrCat = new UserCat();
                model.usrCat.usrCat = (int)CustomEnum.userCatEnum.Client;
                if (ModelState.IsValid)
                {
                    data = BLayer.ClientReg.saveClientFull(model);
                    #region save Client Image - Commented
                    //try
                    //{
                    //    if (model.client.imageString != null && model.client.imageString.Trim() != "" && data.client.clientId > 0)
                    //    {
                    //        byte[] bytes = Convert.FromBase64String(model.client.imageString);
                    //        Image images;
                    //        using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
                    //        {
                    //            ms.Write(bytes, 0, bytes.Length);
                    //            images = Image.FromStream(ms, true);
                    //        }
                    //        Bitmap bitmap = new Bitmap(images);
                    //        int sourceWidth = bitmap.Width;
                    //        //Get the image current height
                    //        int sourceHeight = bitmap.Height;

                    //        float nPercent = 0;
                    //        float nPercentW = 0;
                    //        float nPercentH = 0;
                    //        nPercentW = ((float)200 / (float)sourceWidth);

                    //        nPercentH = ((float)200 / (float)sourceHeight);

                    //        if (nPercentH < nPercentW)
                    //            nPercent = nPercentH;
                    //        else
                    //            nPercent = nPercentW;

                    //        int destWidth = (int)(sourceWidth * nPercent);

                    //        int destHeight = (int)(sourceHeight * nPercent);
                    //        Bitmap b = new Bitmap(destWidth, destHeight);
                    //        Graphics g = Graphics.FromImage((System.Drawing.Image)b);
                    //        g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    //        g.DrawImage(bitmap, 0, 0, destWidth, destHeight);
                    //        g.Dispose();
                    //        Image images2;
                    //        images2 = (System.Drawing.Image)b;
                    //        string FILE_PATH = null;
                    //        if (images2 != null)
                    //        {
                    //            if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\" + data.client.clientId + "\\Profile\\")))
                    //            {
                    //                System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\" + data.client.clientId + "\\Profile\\"));
                    //            }
                    //            Array.ForEach(Directory.GetFiles(@"" + System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\" + data.client.clientId + "\\Profile\\")), System.IO.File.Delete);
                    //            string FN = DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss.fff");
                    //            FN = Regex.Replace(FN, "[.|-|:|/]", "");
                    //            string newdirectory = "Files\\Client\\" + data.client.clientId + "\\Profile\\ProfileImage_" + FN + ".jpg";
                    //            if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\" + data.client.clientId + "\\Profile\\")))
                    //            {
                    //                System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\" + data.client.clientId + "\\Profile\\"));
                    //            }
                    //            FILE_PATH = newdirectory;
                    //            images2.Save(System.Web.HttpContext.Current.Server.MapPath("~") + "\\" + FILE_PATH);
                    //            model.client.imagePath = FILE_PATH;
                    //            BLayer.ClientReg.saveClientProfile(data.client.clientId, model.client.imagePath);
                    //        }
                    //    }
                    //}
                    //catch (Exception ex)
                    //{

                    //}
                    #endregion
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
                //throw;
            }
            return Ok(ResultFunction.SetReturnModel(data));
        }

        [HttpGet]
        [Route("api/Client/GetClientOnlyDetails")]
        public IHttpActionResult getClientOnlyDetails(long clientID)
        {
            ClientCollection data = new ClientCollection();
            try
            {
                data.usrCat = new UserCat();
                data.usrCat.usrCat = (int)CustomEnum.userCatEnum.Client;
                if (ModelState.IsValid)
                {
                    data = BLayer.ClientReg.getClientDetails(clientID, true);
                    if (data != null && data.client != null)
                    {
                        string authority = HttpContext.Current.Request.Url.Authority;
                        data.client.imagePath = "http://" + authority + "/" + data.client.imagePath;
                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(data.client));
        }

        [HttpPost]
        [Route("api/Client/GetClientDetails")]
        public IHttpActionResult getClientDetails([FromBody]long clientID)
        {
            ClientCollection data = new ClientCollection();
            try
            {
                data.usrCat = new UserCat();
                data.usrCat.usrCat = (int)CustomEnum.userCatEnum.Client;
                if (ModelState.IsValid)
                {
                    data = BLayer.ClientReg.getClientDetails(clientID, false);
                    if (data != null && data.client != null)
                    {
                        string authority = HttpContext.Current.Request.Url.Authority;
                        data.client.imagePath = "http://" + authority + "/" + data.client.imagePath;
                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(data));
        }

        [HttpPost]
        [Route("api/Client/GetClientList")]
        public IHttpActionResult getClientList([FromBody]ClientListArgModel arg)
        {
            ClientListPaginated md = new ClientListPaginated();
            //List<ClientModel> model = new List<ClientModel>();
            try
            {
                if (ModelState.IsValid)
                {
                    md = BLayer.ClientReg.getClientList(arg);
                    //md.clients = model;
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(md));
        }

        [HttpDelete]
        [Route("api/Client/DeleteClient")]
        public IHttpActionResult deleteClient([FromBody]ClientListArgModel data)
        {
            ClientListPaginated model = new ClientListPaginated();
            try
            {
                var Identity = (ClaimsIdentity)User.Identity;
                var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
                long UserID = Convert.ToInt64(usrID);
                if (ModelState.IsValid)
                {
                    if (data.clientID > 0)
                    {
                        if (UserID > 0)
                        {
                            BLayer.ClientReg.deleteClient(data.clientID, UserID);
                        }
                        else
                        {
                            Messages msg = new Messages();
                            msg.apiStatus = (int)apiStatusEnum.ValidationError;//logic errors
                            msg.Message = "Invalid logged in User";
                            return Ok(msg);
                        }
                    }
                    else
                    {
                        Messages msg = new Messages();
                        msg.apiStatus = (int)apiStatusEnum.ValidationError;//logic errors
                        msg.Message = "Client must have a value";
                        return Ok(msg);
                    }
                    model = BLayer.ClientReg.getClientList(data);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/Client/getAdvisorDDL")]
        public IHttpActionResult getAdvisorDDL()
        {
            ClientModel model = new ClientModel();
            try
            {
                List<DropDownList_Int> ddl1 = CLayer.common.DDLReg.getDropDownList_Int(enumDropDownList.Advisor);
                model.advisorDDL = ddl1;
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model.advisorDDL));
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/Client/getReportTypeDDL")]
        public IHttpActionResult getReportTypeDDL()
        {
            ClientModel model = new ClientModel();
            try
            {
                List<DropDownList_Int> ddl1 = CLayer.common.DDLReg.getDropDownList_Int(enumDropDownList.ReportType);
                model.reportTypeDDL = ddl1;
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model.reportTypeDDL));
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/Client/getStateDDL")]
        public IHttpActionResult getStateDDL()
        {
            ClientModel model = new ClientModel();
            try
            {
                List<DropDownList_String> ddl1 = CLayer.common.DDLReg.getDropDownList(enumDropDownListCode.State);
                model.stateDDL = ddl1;
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model.stateDDL));
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("api/Client/getCurrencyDDL")]
        public IHttpActionResult getCurrencyDDL()
        {
            ClientModel model = new ClientModel();
            try
            {
                List<DropDownList_Int> ddl1 = CLayer.common.DDLReg.getDropDownList_Int(enumDropDownList.Currencies);
                model.currencyDDL = ddl1;
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model.currencyDDL));
        }


        [HttpPost]
        [AllowAnonymous]
        [Route("api/Client/imageUpload")]
        public IHttpActionResult imageupload(imageUpload iu)
        {
            string path = null;
            bool Status = false;
            try
            {
                //if (ModelState.IsValid)
                //{
                string authority = HttpContext.Current.Request.Url.Authority;
                int index = iu.imageString.IndexOf("base64");
                iu.imageString = iu.imageString.Remove(0, index + 7);
                path = uploadimageClient(iu.imageString, iu.id);
                path = "http://" + authority + "/" + path;
                //Status = BLayer.AdvisorReg.advImageUpload(path, iu.id);
                //}
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(path));
        }

        private string uploadimageClient(string imageString, long clientId)
        {
            try
            {
                if (imageString != null && imageString.Trim() != "")
                {
                    byte[] bytes = Convert.FromBase64String(imageString);
                    Image images;
                    using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
                    {
                        ms.Write(bytes, 0, bytes.Length);
                        images = Image.FromStream(ms, true);
                    }
                    Bitmap bitmap = new Bitmap(images);
                    int sourceWidth = bitmap.Width;
                    //Get the image current height
                    int sourceHeight = bitmap.Height;

                    float nPercent = 0;
                    float nPercentW = 0;
                    float nPercentH = 0;
                    nPercentW = ((float)200 / (float)sourceWidth);

                    nPercentH = ((float)200 / (float)sourceHeight);

                    if (nPercentH < nPercentW)
                        nPercent = nPercentH;
                    else
                        nPercent = nPercentW;

                    int destWidth = (int)(sourceWidth * nPercent);

                    int destHeight = (int)(sourceHeight * nPercent);
                    Bitmap b = new Bitmap(destWidth, destHeight);
                    Graphics g = Graphics.FromImage((System.Drawing.Image)b);
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    g.DrawImage(bitmap, 0, 0, destWidth, destHeight);
                    g.Dispose();
                    Image images2;
                    images2 = (System.Drawing.Image)b;
                    string FILE_PATH = null;
                    if (images2 != null)
                    {
                        if (clientId > 0)
                        {
                            if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\" + clientId + "\\Profile\\")))
                            {
                                System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\" + clientId + "\\Profile\\"));
                            }
                            Array.ForEach(Directory.GetFiles(@"" + System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\" + clientId + "\\Profile\\")), System.IO.File.Delete);
                            string FN = DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss.fff");
                            FN = Regex.Replace(FN, "[.|-|:|/]", "");
                            string newdirectory = "Files\\Client\\" + clientId + "\\Profile\\ProfileImage_" + FN + ".jpg";
                            if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\" + clientId + "\\Profile\\")))
                            {
                                System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\" + clientId + "\\Profile\\"));
                            }
                            FILE_PATH = newdirectory;
                            images2.Save(System.Web.HttpContext.Current.Server.MapPath("~") + "\\" + FILE_PATH);
                            string imagePath = FILE_PATH;
                            return imagePath;
                        }
                        else
                        {
                            if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\Temp\\Profile\\")))
                            {
                                System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\Temp\\Profile\\"));
                            }
                            Array.ForEach(Directory.GetFiles(@"" + System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\Temp\\Profile\\")), System.IO.File.Delete);
                            string FN = DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss.fff");
                            FN = Regex.Replace(FN, "[.|-|:|/]", "");
                            string newdirectory = "Files\\Client\\Temp\\Profile\\ProfileImage_" + FN + ".jpg";
                            if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\Temp\\Profile\\")))
                            {
                                System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Client\\Temp\\Profile\\"));
                            }
                            FILE_PATH = newdirectory;
                            images2.Save(System.Web.HttpContext.Current.Server.MapPath("~") + "\\" + FILE_PATH);
                            string imagePath = FILE_PATH;
                            return imagePath;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }

    }
}
