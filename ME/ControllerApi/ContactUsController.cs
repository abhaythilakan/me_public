﻿using ME.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CLayer;
using CLayer.common;
using System.Security.Claims;



namespace ME.ControllerApi
{
    public class ContactUsController : ApiController
    {

        [HttpPost]
        [AllowAnonymous]
        [Route("api/Contact/postContactUs")]
        public IHttpActionResult contactUs([FromBody] ContactModel contact)
        {
            bool isSaved= false;
            try
            {
                if (ModelState.IsValid)
                {
                    isSaved= BLayer.ContactUs.ContactME(contact);
                    try
                    {
                        MailService.sendNotificationMail(contact.email, contact.name, contact.phone, contact.subject, contact.comments);
                        var msg = CustomErrorHandler.mailSendSuccessfully();
                        return Ok(msg);
                    }
                    catch(Exception ex)
                    {
                        var msg = CustomErrorHandler.mailSendUnSuccessfully();
                        return Ok(msg);
                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            //return Ok(isSaved);
        }

    }
}
