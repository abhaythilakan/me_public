﻿using CLayer;
using CLayer.common;
using ME.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace ME.ControllerApi
{
    [Common.Authorize(Roles = "Admin, Advisor, Client")]
    public class DebtController : ApiController
    {
        private long profileID;
        private long userID;

        //public DebtController()
        //{
        //    //var Identity = (ClaimsIdentity)User.Identity;
        //    //var roles = Identity.Claims.Where(b => b.Type == ClaimTypes.Role).Select(b => b.Value);
        //    //var profile = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value);
        //    //var user = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value);
        //}

        [HttpPost]
        [Route("api/Debt/getMortages")]
        public IHttpActionResult getMortages([FromBody] long profileID)
        {
            var Identity = (ClaimsIdentity)User.Identity;
            var proID = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
            var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
            //profileID = Convert.ToInt64(proID);
            userID = Convert.ToInt64(usrID);
            List<DebtModel> model = new List<DebtModel>();
            try
            {
                if (profileID > 0)
                {
                    model = BLayer.DebtReg.getMortageList(profileID);
                }
                else
                {
                    var msg = CustomErrorHandler.getProfileValidationErrors();
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }

        [HttpPost]
        [Route("api/Debt/getTotalExtraPayment")]
        public IHttpActionResult getTotalExtraPayment([FromBody] long profileID)
        {
            DebtDisappearModel mdl = new DebtDisappearModel();
            try
            {
                mdl.profileID = profileID;
                mdl.extraPay = BLayer.DebtReg.getTotalExtraPayment(profileID);
            }
            catch(Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(mdl));
        }

        [HttpPost]
        [Route("api/Debt/getFixeds")]
        public IHttpActionResult getFixeds([FromBody] long profileID)
        {
            var Identity = (ClaimsIdentity)User.Identity;
            var proID = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
            var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
            //profileID = Convert.ToInt64(proID);
            userID = Convert.ToInt64(usrID);
            List<DebtModel> model = new List<DebtModel>();
            try
            {
                if (profileID > 0)
                {
                    model = BLayer.DebtReg.getFixedList(profileID);
                }
                else
                {
                    var msg = CustomErrorHandler.getProfileValidationErrors();
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }

        [HttpPost]
        [Route("api/Debt/getRevolvings")]
        public IHttpActionResult getRevolvings([FromBody] long profileID)
        {
            var Identity = (ClaimsIdentity)User.Identity;
            var proID = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
            var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
            //profileID = Convert.ToInt64(proID);
            userID = Convert.ToInt64(usrID);
            List<DebtModel> model = new List<DebtModel>();
            try
            {
                if (profileID > 0)
                {
                    model = BLayer.DebtReg.getRevolvingList(profileID);
                }
                else
                {
                    var msg = CustomErrorHandler.getProfileValidationErrors();
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }

        //[HttpGet]
        //[Route("api/Debt/getClientDebts")]
        //public IHttpActionResult getClientDebts()
        //{
        //    try
        //    {

        //    }
        //    catch(Exception ex)
        //    {
        //        var msg = CustomErrorHandler.getLogicErrors(ex);
        //        return Ok(msg);
        //    }
        //    return Ok();
        //}

        [HttpPost]
        [Route("api/Debt/getDebtDetail")]
        public IHttpActionResult getDebtDetail(DebtDetailModel model)
        {
            DebtModel data = new DebtModel();
            try
            {
                if (ModelState.IsValid)
                {
                    data = BLayer.DebtReg.getDebtDetails(model.debtID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(data));
        }

        [HttpPost]
        [Route("api/Debt/saveDebtDetailsMortage")]
        public IHttpActionResult saveDebtDetailsMortage(List<DebtModel> model)
        {
            var Identity = (ClaimsIdentity)User.Identity;
            var proID = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
            var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
            profileID = Convert.ToInt64(proID);
            userID = Convert.ToInt64(usrID);
            if (model != null && model.Count() > 0)
            {
                for (int i = 0; i < model.Count(); i++)
                {
                    //model[i].profileID = profileID;
                    model[i].debtCatId = (int)CLayer.common.CustomEnum.debtCatEnum.Mortage;
                    if (model[i].profileID > 0)
                    {
                        if (ModelState.ContainsKey("model.profileID"))
                            ModelState["model.profileID"].Errors.Clear();
                    }
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            if (model[i].isActive == false)
                            {
                                DebtModel data = new DebtModel();
                                data.debtModifiedBy = userID;
                                data.debtID = model[i].debtID;
                                BLayer.DebtReg.deleteDebt(data);
                                model.Remove(model[i]);
                            }
                            else
                            {
                                model[i].debtModifiedBy = userID;
                                DebtModel mm = new DebtModel();
                                mm = BLayer.DebtReg.saveDebtDetails(model[i]);
                                model[i].debtID = mm.debtID;
                            }
                        }
                        else
                        {
                            var msg = CustomErrorHandler.getValidationErrors(ModelState);
                            return Ok(msg);
                        }
                    }
                    catch (Exception ex)
                    {
                        var msg = CustomErrorHandler.getLogicErrors(ex);
                        return Ok(msg);
                    }
                }
            }
            else
            {
                var msg = CustomErrorHandler.getInvalidEntry();
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }

        [HttpPost]
        [Route("api/Debt/saveDebtDetailsFixed")]
        public IHttpActionResult saveDebtDetailsFixed(List<DebtModel> model)
        {
            var Identity = (ClaimsIdentity)User.Identity;
            var proID = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
            var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
            profileID = Convert.ToInt64(proID);
            userID = Convert.ToInt64(usrID);
            //model.profileID = profileID;
            if (model != null && model.Count() > 0)
            {
                for (int i = 0; i < model.Count(); i++)
                {
                    model[i].debtCatId = (int)CLayer.common.CustomEnum.debtCatEnum.Fixed;
                    if (model[i].profileID > 0)
                    {
                        if (ModelState.ContainsKey("model.profileID"))
                            ModelState["model.profileID"].Errors.Clear();
                    }
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            if (model[i].isActive == false)
                            {
                                DebtModel data = new DebtModel();
                                data.debtModifiedBy = userID;
                                data.debtID = model[i].debtID;
                                BLayer.DebtReg.deleteDebt(data);
                                model.Remove(model[i]);
                            }
                            else
                            {
                                model[i].debtModifiedBy = userID;
                                DebtModel mm = new DebtModel();
                                mm = BLayer.DebtReg.saveDebtDetails(model[i]);
                                model[i].debtID = mm.debtID;
                            }
                        }
                        else
                        {
                            var msg = CustomErrorHandler.getValidationErrors(ModelState);
                            return Ok(msg);
                        }
                    }
                    catch (Exception ex)
                    {
                        var msg = CustomErrorHandler.getLogicErrors(ex);
                        return Ok(msg);
                    }
                }
            }
            else
            {
                var msg = CustomErrorHandler.getInvalidEntry();
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }

        [HttpPost]
        [Route("api/Debt/saveDebtDetailsRevolving")]
        public IHttpActionResult saveDebtDetailsRevolving(List<DebtModel> model)
        {
            var Identity = (ClaimsIdentity)User.Identity;
            var proID = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
            var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
            profileID = Convert.ToInt64(proID);
            userID = Convert.ToInt64(usrID);
            //model.profileID = profileID;
            if (model != null && model.Count() > 0)
            {
                for (int i = 0; i < model.Count(); i++)
                {
                    model[i].debtCatId = (int)CLayer.common.CustomEnum.debtCatEnum.Revolving;
                    if (model[i].profileID > 0)
                    {
                        if (ModelState.ContainsKey("model.profileID"))
                            ModelState["model.profileID"].Errors.Clear();
                    }
                    try
                    {
                        if (ModelState.IsValid)
                        {
                            if (model[i].isActive == false)
                            {
                                DebtModel data = new DebtModel();
                                data.debtModifiedBy = userID;
                                data.debtID = model[i].debtID;
                                BLayer.DebtReg.deleteDebt(data);
                                model.Remove(model[i]);
                            }
                            else
                            {
                                model[i].debtModifiedBy = userID;
                                DebtModel mm = new DebtModel();
                                mm = BLayer.DebtReg.saveDebtDetails(model[i]);
                                model[i].debtID = mm.debtID;
                            }
                        }
                        else
                        {
                            var msg = CustomErrorHandler.getValidationErrors(ModelState);
                            return Ok(msg);
                        }
                    }
                    catch (Exception ex)
                    {
                        var msg = CustomErrorHandler.getLogicErrors(ex);
                        return Ok(msg);
                    }
                }
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }

        [HttpDelete]
        [Route("api/Debt/deleteDebt")]
        public IHttpActionResult deleteDebt(DebtDetailModel model)
        {
            List<DebtModel> mdl = new List<DebtModel>();
            var Identity = (ClaimsIdentity)User.Identity;
            var proID = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
            var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
            profileID = Convert.ToInt64(proID);
            userID = Convert.ToInt64(usrID);
            if (model.profileID > 0)
            {
                if (ModelState.ContainsKey("model.profileID"))
                    ModelState["model.profileID"].Errors.Clear();
            }
            try
            {
                if (ModelState.IsValid)
                {
                    DebtModel data = new DebtModel();
                    data.debtModifiedBy = userID;
                    data.debtID = model.debtID;
                    mdl = BLayer.DebtReg.deleteDebt(data);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(mdl));
        }

        //[HttpPost]
        //[AllowAnonymous]
        //[Route("api/Debt/getAllDebtAmotization")]
        //public IHttpActionResult getAllDebtAmotizationList(DebtProjectionInput model)
        //{
        //    var Identity = (ClaimsIdentity)User.Identity;
        //    var proID = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
        //    var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
        //    profileID = Convert.ToInt64(proID);
        //    userID = Convert.ToInt64(usrID);
        //    DebtAmortizationModel data = new DebtAmortizationModel();
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            data = BLayer.DebtReg.getDebtInputPrediction(model);
        //        }
        //        else
        //        {
        //            var msg = CustomErrorHandler.getValidationErrors(ModelState);
        //            return Ok(msg);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        var msg = CustomErrorHandler.getLogicErrors(ex);
        //        return Ok(msg);
        //    }
        //    return Ok(ResultFunction.SetReturnModel(data));
        //}

        [HttpPost]
        [AllowAnonymous]
        [Route("api/Debt/getDebtAmotization")]
        public IHttpActionResult getDebtAmotizationDetailed(DebtProjectionInput model)
        {
            //DebtProjectionInput model = new DebtProjectionInput();

            // Arguments   **********************************************
            //model.debtId = 5;
            //model.isOneTimePay = true;
            //model.additionalPayOneTime = 0;
            //model.isAllTimePay = true;
            //model.additionalPayAllTime = 0;
            //model.isYearlychart = true; *******************************
            DebtAmortizationModel data = new DebtAmortizationModel();
            try
            {
                if (ModelState.IsValid)
                {
                    data = BLayer.DebtReg.getDebtInputPrediction(model);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(data));
        }

        [HttpPost]
        [Route("api/Debt/getDebtDisappearList")]
        public IHttpActionResult getDebtDisappearList(DebtDisappearModel model)
        {
            List<DebtDisappearList> data = new List<DebtDisappearList>();
            try
            {
                var Identity = (ClaimsIdentity)User.Identity;
                var proID = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
                var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
                //profileID = Convert.ToInt64(proID);
                profileID = model.profileID;
                userID = Convert.ToInt64(usrID);
                if (ModelState.IsValid)
                {
                    data = BLayer.DebtReg.getDisappearList(model.profileID, model.extraPay);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(data));
        }


        [HttpPost]
        [Route("api/Debt/getDebtDisappearDetailed")]
        public IHttpActionResult getDebtDisappearDetailed(DebtDisappearModel model)
        {
            List<DebtDisappearYearlyD2> data = new List<DebtDisappearYearlyD2>();
            try
            {
                var Identity = (ClaimsIdentity)User.Identity;
                var proID = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
                var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
                //profileID = Convert.ToInt64(proID);
                profileID = model.profileID;
                userID = Convert.ToInt64(usrID);
                if (ModelState.IsValid)
                {
                    data = BLayer.DebtReg.getDebtEliminationSystemAutoEstimation(profileID, model.extraPay);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(data));
        }
    }
}
