﻿using CLayer;
using CLayer.common;
using ME.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static CLayer.common.CustomEnum;

namespace ME.ControllerApi
{
    [Common.Authorize(Roles = "Admin,Advisor")]
    public class ProfileController : ApiController
    {

        [AllowAnonymous]
        [CustomErrorHandler]
        [HttpGet]
        [Route("getTest")]
        public IHttpActionResult getTest()
        {
            long id = 12;
            long cd = 0;
            try
            {
                var gn = id / cd;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Ok();
        }

        [HttpPost]
        [Route("api/Profile/saveProfile")]
        public IHttpActionResult saveProfile([FromBody]ProfileModel model)
        {
            ProfileModel data = new ProfileModel();
            try
            {
                if (ModelState.IsValid)
                {
                    data = BLayer.ProfileReg.saveProfile(model);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
                //throw;
            }
            return Ok(ResultFunction.SetReturnModel(data));
        }

        [HttpPost]
        [Route("api/Profile/saveProfileDetails")]
        public IHttpActionResult saveProfileDetails([FromBody]ProfileCollection model)
        {
            ProfileCollection data = new ProfileCollection();
            try
            {
                if (ModelState.IsValid)
                {
                    data = BLayer.ProfileReg.saveProfileFull(model);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
                //throw;
            }
            return Ok(ResultFunction.SetReturnModel(data));
        }

        [HttpPost]
        [Route("api/Profile/getProfileDetails")]
        public IHttpActionResult getProfileDetails([FromBody]long profileID)
        {
            ProfileCollection data = new ProfileCollection();
            try
            {
                if (ModelState.IsValid)
                {
                    data = BLayer.ProfileReg.getProfileDetails(profileID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(data));
        }

        [HttpPost]
        [Route("api/Profile/getProfileList")]
        public IHttpActionResult getProfileList([FromBody]ProfileListArgModel arg)
        {
            List<ProfileModel> model = new List<ProfileModel>();
            try
            {
                if (ModelState.IsValid)
                {
                    model = BLayer.ProfileReg.getProfileList(arg);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }

        [HttpDelete]
        [Route("api/Profile/DeleteProfile")]
        public IHttpActionResult deleteClient([FromBody]ProfileListArgModel data)
        {
            List<ProfileModel> model = new List<ProfileModel>();
            try
            {
                if (ModelState.IsValid)
                {
                    if (data.profileId > 0)
                        BLayer.ProfileReg.deleteProfile(data.profileId);
                    else
                    {
                        Messages msg = new Messages();
                        msg.apiStatus = (int)apiStatusEnum.ValidationError;//logic errors
                        msg.Message = "Profile must have a value";
                        return Ok(msg);
                    }
                    model = BLayer.ProfileReg.getProfileList(data);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }

    }
}
