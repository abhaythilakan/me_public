﻿using ME.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CLayer;
using CLayer.common;
using System.Security.Claims;

namespace ME.ControllerApi
{
    public class PersonalInformationController : ApiController
    {

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]     
        [Route("api/PersonalInformation/primaryInfo")]
        public IHttpActionResult SavePrimaryInfo([FromBody] PersonModel PrimaryInfo)
        {
            long userID = 0;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                try
                {                
                        DateTime date = PrimaryInfo.birthDate.Value;                   
                }
                catch (Exception ex)
                {
                    throw new Exception("Primary date of birth is not valid.");
                }
                if (ModelState.IsValid)
                {
                    PrimaryInfo = BLayer.PersonalInformation.saveprimaryPerson(PrimaryInfo, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(PrimaryInfo));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/PersonalInformation/primaryContact")]
        public IHttpActionResult SavePrimaryContact(Person_ContactInfoModel PrimaryContact)
        {
            long userID = 0;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                if (ModelState.IsValid)
                {
                    PrimaryContact = BLayer.PersonalInformation.saveprimaryContact(PrimaryContact, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(PrimaryContact));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/PersonalInformation/primaryEmployer")]
        public IHttpActionResult SavePrimaryEmployer(Person_EmployerInfoModel PrimaryEmployer)
        {
            long userID = 0;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                if (ModelState.IsValid)
                {
                    PrimaryEmployer = BLayer.PersonalInformation.saveprimaryEmployer(PrimaryEmployer, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(PrimaryEmployer));
        }


        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/PersonalInformation/SpouseInfo")]
        public IHttpActionResult SaveSpouseInfo(PersonModel SpouseInfo)
        {
            long userID = 0;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                if (ModelState.IsValid)
                {
                    SpouseInfo = BLayer.PersonalInformation.saveSpouseInfo(SpouseInfo, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(SpouseInfo));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/PersonalInformation/SpouseEmployer")]
        public IHttpActionResult SaveSpouseEmployer(Person_EmployerInfoModel SpouseEmployer)
        {
            long userID = 0;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                if (ModelState.IsValid)
                {
                    SpouseEmployer = BLayer.PersonalInformation.saveSpouseEmployer(SpouseEmployer, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(SpouseEmployer));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/PersonalInformation/childSave")]
        public IHttpActionResult SaveChildSave(List<ChildrenInfoModel> ChildInfo)
        {
            long userID = 0;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();

                try
                {
                    foreach (var a in ChildInfo)
                    {
                        DateTime date = a.dob.Value;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Child date of birth not valid.");
                }

                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                if (ModelState.IsValid)
                {
                    ChildInfo = BLayer.PersonalInformation.saveChildInfo(ChildInfo, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(ChildInfo));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/PersonalInformation/GetPersonalInfo")]
        public IHttpActionResult getSavedDetailsALL([FromBody] long profileID)
        {
            PersonalInformationModel personalInfo = new PersonalInformationModel();
            try
            {

                personalInfo = BLayer.PersonalInformation.getallinfo(profileID);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(personalInfo));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/PersonalInformation/GetPrimaryDetails")]
        public IHttpActionResult GetPrimaryDetails([FromBody] long profileID)
        {
            PersonModel outObj = new PersonModel();
            try
            {

                outObj = BLayer.PersonalInformation.getPersonalDetails(profileID);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/PersonalInformation/getPrimaryContact")]
        public IHttpActionResult getPrimaryContact([FromBody] long profileID)
        {
            Person_ContactInfoModel outObj = new Person_ContactInfoModel();
            try
            {
                outObj = BLayer.PersonalInformation.getPrimaryContact(profileID);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/PersonalInformation/getEmployerInfo")]
        public IHttpActionResult getEmployerInfo([FromBody] long profileID)
        {
            Person_EmployerInfoModel outObj = new Person_EmployerInfoModel();
            try
            {

                outObj = BLayer.PersonalInformation.getEmployerInfo(profileID);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/PersonalInformation/getSpouseDetails")]
        public IHttpActionResult getSpouseDetails([FromBody] long profileID)
        {
            PersonModel outObj = new PersonModel();
            try
            {
                outObj = BLayer.PersonalInformation.getSpouseDetails(profileID);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/PersonalInformation/getEmployerInfoSpouse")]
        public IHttpActionResult getEmployerInfoSpouse([FromBody] long profileID)
        {
            Person_EmployerInfoModel outObj = new Person_EmployerInfoModel();
            try
            {

                outObj = BLayer.PersonalInformation.getEmployerInfoSpouse(profileID);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outObj));
        }


        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/PersonalInformation/getChildDetails")]
        public IHttpActionResult getChildDetails([FromBody] long profileID)
        {
            List<ChildrenInfoModel> outObj = new List<ChildrenInfoModel>();
            try
            {

                outObj = BLayer.PersonalInformation.getChildDetails(profileID);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/PersonalInformation/DeleteChild")]
        public IHttpActionResult deleteChild(childDelete childDelete)
        {
            long userID = 0;
            bool status = false;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                if (ModelState.IsValid)
                {
                    status = BLayer.PersonalInformation.deleteChild(childDelete, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(status));
        }


    }
}

