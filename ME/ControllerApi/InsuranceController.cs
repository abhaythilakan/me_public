﻿using CLayer;
using CLayer.common;
using ME.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;

namespace ME.ControllerApi
{
    [Common.Authorize(Roles = "Admin, Advisor, Client")]
    public class InsuranceController : ApiController
    {
        private long userID;       
        [HttpPost]
        [Route("api/Insurance/getAllInsurance")]
        public IHttpActionResult getAllInsurance([FromBody]long profileId)
        {
            FullInsurance model = new FullInsurance();
            try
            {
                var Identity = (ClaimsIdentity)User.Identity;
                model = BLayer.Insurance.GetFullInsurance(profileId);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }
        [HttpPost]
        [Route("api/Insurance/saveOtherInsurance")]
        public IHttpActionResult saveOtherInsurance(List<OtherInsuranceModel> inputList)
        {
            OtherLifeInsurance output = new OtherLifeInsurance();
            try
            {
                var Identity = (ClaimsIdentity)User.Identity;
                var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
                userID = Convert.ToInt64(usrID);
                try
                {
                    foreach (var d in inputList)
                    {
                        DateTime date = d.valueAsOf.Value;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Date is not valid");
                }
                if (ModelState.IsValid)
                {
                    output = BLayer.Insurance.SaveOtherInsurance(inputList, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(output));
        }

        [HttpPost]
        [Route("api/Insurance/saveInsurance")]
        public IHttpActionResult saveInsurance(List<InsuranceModel> inputList)
        {
            FullLifeInsurance output = new FullLifeInsurance();
            try
            {
                var Identity = (ClaimsIdentity)User.Identity;
                var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
                userID = Convert.ToInt64(usrID);
                try
                {
                    foreach (var d in inputList)
                    {
                        DateTime date = d.valueAsOf.Value;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Date is not valid");
                }
                if (ModelState.IsValid)
                {
                    output = BLayer.Insurance.SaveInsurance(inputList, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(output));
        }
        [HttpPost]
        [Route("api/Insurance/saveChildInsurance")]
        public IHttpActionResult saveChildInsurance(List<InsuranceModel> inputList)
        {
            ChildInsurance output = new ChildInsurance();
            try
            {
                var Identity = (ClaimsIdentity)User.Identity;
                var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
                userID = Convert.ToInt64(usrID);
                try
                {
                    foreach (var d in inputList)
                    {
                        DateTime date = d.valueAsOf.Value;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Date is not valid");
                }
                if (ModelState.IsValid)
                {
                    output = BLayer.Insurance.SaveChildInsurance(inputList, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(output));
        }
        [HttpPost]
        [Route("api/Insurance/saveInsuranceDetails")]
        public IHttpActionResult saveInsuranceDetails(InsuranceDetailsModel input)
        {
            InsuranceDetailsModel output = new InsuranceDetailsModel();
            try
            {
                var Identity = (ClaimsIdentity)User.Identity;
                var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
                userID = Convert.ToInt64(usrID);
                if (ModelState.IsValid)
                {
                    output = BLayer.Insurance.SaveInsuranceDetails(input, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(output));
        }
        [HttpPost]
        [Route("api/Insurance/deleteOtherInsurance")]
        public IHttpActionResult deleteOtherInsurance(OtherInsuranceInputModel input)
        {
            OtherLifeInsurance output = new OtherLifeInsurance();
            try
            {
                var Identity = (ClaimsIdentity)User.Identity;
                var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
                userID = Convert.ToInt64(usrID);
                if (ModelState.IsValid)
                {
                    output = BLayer.Insurance.DeleteOtherInsurance(input, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(output));
        }
        [HttpPost]
        [Route("api/Insurance/deleteInsurance")]
        public IHttpActionResult deleteInsurance(InsuranceInputModel input)
        {
            FullLifeInsurance output = new FullLifeInsurance();
            try
            {
                var Identity = (ClaimsIdentity)User.Identity;
                var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
                userID = Convert.ToInt64(usrID);
                if (ModelState.IsValid)
                {
                    output = BLayer.Insurance.DeleteInsurance(input, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(output));
        }
        [HttpPost]
        [Route("api/Insurance/deleteChildInsurance")]
        public IHttpActionResult deleteChildInsurance(InsuranceChildInput input)
        {
            ChildInsurance output = new ChildInsurance();
            try
            {
                var Identity = (ClaimsIdentity)User.Identity;
                var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
                userID = Convert.ToInt64(usrID);
                if (ModelState.IsValid)
                {
                    output = BLayer.Insurance.DeleteChildInsurance(input, userID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(output));
        }
        [HttpGet]
        [Route("api/Insurance/getInsuranceTypes")]
        public IHttpActionResult getInsuranceTypes()
        {
            List<DropDownList_Int> list = new List<DropDownList_Int>();
            try
            {
                list = BLayer.DropDownList.DDL_List_int(enumDropDownList.LifeInsurance);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(list));
        }
        [HttpGet]
        [Route("api/Insurance/getOtherInsuranceTypes")]
        public IHttpActionResult getOtherInsuranceTypes()
        {
            List<DropDownList_Int> list = new List<DropDownList_Int>();
            try
            {
                list = BLayer.DropDownList.DDL_List_int(enumDropDownList.OtherInsurance);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(list));
        }
        [HttpGet]
        [Route("api/Insurance/getIncomeTypes")]
        public IHttpActionResult getIncomeTypes()
        {
            List<DropDownList_Int> list = new List<DropDownList_Int>();
            try
            {
                list = BLayer.DropDownList.DDL_List_int(enumDropDownList.IncomeAfterDeath);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(list));
        }
        [HttpGet]
        [Route("api/Insurance/getLifeInsTypes")]
        public IHttpActionResult getLifeInsTypes()
        {
            List<DropDownList_Int> list = new List<DropDownList_Int>();
            try
            {
                list = BLayer.DropDownList.DDL_List_int(enumDropDownList.IncomeAfterDeath);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(list));
        }
        [HttpGet]
        [Route("api/Insurance/getPremiumTypes")]
        public IHttpActionResult getPremiumTypes()
        {
            List<DropDownList_Int> list = new List<DropDownList_Int>();
            try
            {
                list = BLayer.DropDownList.DDL_List_int(enumDropDownList.PayType);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(list));
        }
    }
}
