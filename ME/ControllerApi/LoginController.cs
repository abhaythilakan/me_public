﻿using CLayer;
using CLayer.common;
using CLayer.DBModel;
using ME.Common;
using ME.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ME.ControllerApi
{
    public class LoginController : ApiController
    {
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [HttpPost]
        [Route("api/Login/GetSessionContext")]
        public IHttpActionResult GetSessionContext()
        {
            SessionContext data = new SessionContext();
            try
            {
                if (ModelState.IsValid)
                {
                    var Identity = (ClaimsIdentity)User.Identity;
                    var advisorID = Identity.Claims.Where(b => b.Type == "advisorID").Select(b => b.Value).FirstOrDefault();
                    var proID = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
                    var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
                    var clientID = Identity.Claims.Where(b => b.Type == "ClientID").Select(b => b.Value).FirstOrDefault();

                    long profileID = Convert.ToInt64(proID);
                    long userID = Convert.ToInt64(usrID);
                    long ClientID = Convert.ToInt64(clientID);
                    long AdvisorID = Convert.ToInt64(advisorID);

                    data.UserID = userID;
                    data.advisorID = AdvisorID;
                    data.ClientID = ClientID;
                    data.ProfileID = profileID;

                    var roles = Identity.Claims.Where(b => b.Type == ClaimTypes.Role).Select(b => b.Value);
                    data.Role = roles.Where(b => b == "Admin").FirstOrDefault();
                    if (data.Role != "Admin")
                    {
                        data.Role = roles.Where(b => b == "Advisor").FirstOrDefault();
                        if (data.Role != "Advisor")
                        {
                            data.Role = roles.Where(b => b == "Client").FirstOrDefault();
                        }
                    }
                    if (data.Role == "Admin")
                    {
                        data.hasClients = BLayer.Login.setLoginedUserToken(AdvisorID, CLayer.common.CustomEnum.userRoles.Admin);
                    }
                    else if (data.Role == "Advisor")
                    {
                        data.hasClients = BLayer.Login.setLoginedUserToken(AdvisorID, CLayer.common.CustomEnum.userRoles.Advisor);
                    }
                    //else if (data.Role == "Client")
                    //{
                    //    data.hasClients = BLayer.Login.setLoginedUserToken(Cli, CLayer.common.CustomEnum.userRoles.Advisor);
                    //}
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(data));
        }


        [HttpPost]
        [AllowAnonymous]
        [Route("api/Login/SignIn")]
        public IHttpActionResult Login(LoginModel model)
        {
            tokenInfo data = new tokenInfo();
            try
            {
                if (ModelState.IsValid)
                {
                    string authority = HttpContext.Current.Request.Url.Authority;
                    CustomWebRequest wr = new CustomWebRequest("http://" + authority + "/GenerateToken", "POST", "username=" + model.username.Trim() + "&password=" + model.password.Trim() + "&grant_type=" + model.grant_Type.Trim());
                    data = JsonConvert.DeserializeObject<tokenInfo>(wr.GetResponse());
                    data.authority = authority;
                    if (data != null)
                    {
                        bool isSave = BLayer.Login.saveActiveToken(model.username, model.password, data.access_token);
                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getInvalidUsernamePassword();
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(data));
        }


        [HttpPost]
        [Route("api/Login/checkForActiveToken")]
        [Common.Authorize]
        public IHttpActionResult checkForActiveToken([FromBody]string activeToken)
        {
            bool isActive = false;
            try
            {
                var Identity = (ClaimsIdentity)User.Identity;
                var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
                long userID = Convert.ToInt64(usrID);
                if (userID > 0)
                {
                    isActive = BLayer.Login.isActiveToken(userID, activeToken);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            if (isActive)
            {
                var msgs = CustomErrorHandler.getActiveTokenMessage();
                return Ok(msgs);
            }
            else
            {
                var msgs = CustomErrorHandler.getInActiveTokenMessage();
                return Ok(msgs);
            }
        }


        [HttpPost]
        [Route("api/Login/SignOut")]
        [Common.Authorize]
        public IHttpActionResult Logout()
        {
            bool isLogOut = false;
            try
            {
                var Identity = (ClaimsIdentity)User.Identity;
                var usrID = Identity.Claims.Where(b => b.Type == "UserID").Select(b => b.Value).FirstOrDefault();
                long userID = Convert.ToInt64(usrID);
                if (userID > 0)
                {
                    isLogOut = BLayer.Login.logOut(userID);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            var msgs = CustomErrorHandler.getLogoutMessage();
            return Ok(msgs);
        }

        //[AllowAnonymous]
        //[HttpGet]
        //[Route("api/Login/SignIn")]
        //public IHttpActionResult getSignUp() // Header Argument - Authorization = bearer [Token]
        //{
        //    return Ok("All User Logged in time : " + DateTime.Now);
        //}

        [AllowAnonymous]
        [HttpGet]
        [Route("api/Login/AuthenticateSignIn")]
        public IHttpActionResult getSignUpAuthenticated()
        {
            var Identity = (ClaimsIdentity)User.Identity;
            return Ok("Hello " + Identity.Name + ", Autherized User Logged in time : " + DateTime.Now);
        }

        //[Common.Authorize]
        //[HttpGet]
        //[Route("api/Login/authorizeAdmin")]
        //public IHttpActionResult getSignUpAdmin()
        //{
        //    var Identity = (ClaimsIdentity)User.Identity;
        //    var roles = Identity.Claims.Where(b => b.Type == ClaimTypes.Role).Select(b => b.Value);
        //    return Ok("Hello " + Identity.Name + ", Roles : " + string.Join(", ", roles.ToList()));
        //}


        [HttpPost]
        [AllowAnonymous]
        [Route("api/ForgotPassword")]
        public async Task<IHttpActionResult> ForgotPassword(ForgotPasswordModel model)//ForgotPasswordModel model
        {

            if (ModelState.IsValid)
            {
                string hashCode = Guid.NewGuid().ToString();
                try
                {
                    User usr = BLayer.Login.hasValidAccount(model.email, hashCode);
                    if (usr != null && usr.userId > 0)
                    {
                        string authority = HttpContext.Current.Request.Url.Authority;
                        const string subject = "Reset your Money Edge Pro password";
                        string body = "";
                        if (authority.Trim() == "192.168.60.13")
                        {
                            body = "Hi, <br/>You've recently asked to reset the password for this Money Edge Pro account: " + model.email.ToString() + "<br/>To update your password, click the link below:<br/>" +
                                "http://localhost:4200/forgotpwd-confirm?email=" + model.email.ToString() + "&token=" + hashCode.ToString() + "<br/><br/>" +
                                "Cheers,<br/>The Money Edge Pro";
                        }
                        else
                        {
                            body = "Hi, <br/>You've recently asked to reset the password for this Money Edge Pro account: " + model.email.ToString() + "<br/>To update your password, click the link below:<br/>" +
                                "http://" + authority + "/MEapp/forgotpwd-confirm?email=" + model.email.ToString() + "&token=" + hashCode.ToString() + "<br/><br/>" +
                                "Cheers,<br/>The Money Edge Pro";
                        }
                        //new Uri(Url.Link("DefaultLogin", new { Action = "resetPasswordConfirm", email = model.email.ToString(), token = hashCode.ToString() }).ToString());
                        MailService.sendMail(subject, body, model.email, usr.userName);
                    }
                    else
                    {
                        var msg = CustomErrorHandler.mailSendUnSuccessfully();
                        return Ok(msg);
                    }
                }
                catch (Exception ex)
                {
                    var msg = CustomErrorHandler.getLogicErrors(ex);
                    return Ok(msg);
                }
            }
            var ms = CustomErrorHandler.mailSendSuccessfully();
            return Ok(ms);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/Login/resetPasswordConfirm")]
        public IHttpActionResult resetPassword(ForgotRequestConfirmModel model)
        {
            ForgotPasswodModel fp = new ForgotPasswodModel();
            try
            {
                fp.userID = BLayer.Login.validForgorPasswordToken(model.email, model.token);
                if (fp.userID > 0)
                    fp.token = model.token;
                else
                {
                    var msg = CustomErrorHandler.forgotPasswordTokenExpired();
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(fp));
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/Login/resetPassword")] //password update with new password
        public IHttpActionResult resetPassword([FromBody] ForgotPasswodModel fp)
        {
            bool isDone = false;
            if (ModelState.IsValid)
            {
                try
                {
                    isDone = BLayer.Login.resetPassword(fp);
                }
                catch (Exception ex)
                {
                    var msg = CustomErrorHandler.getLogicErrors(ex);
                    return Ok(msg);
                }
            }
            if (isDone)
            {
                var ms = CustomErrorHandler.passwordChanged();
                return Ok(ms);
            }
            else
            {
                var ms = CustomErrorHandler.passwordNotChanged();
                return Ok(ms);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/Login/IsUsernameExist")]
        public IHttpActionResult IsUsernameExist(AlreadyExistsModel model)
        {
            model.isExists = true;
            try
            {
                if (ModelState.IsValid)
                {
                    model.isExists = BLayer.Login.isUserNameExists(model.userName, model.userId);
                    if (model.isExists)
                    {
                        model.apiStatus = 1;
                        model.Message = "Username already exist";
                    }
                    else
                    {
                        model.Message = "Username available.";
                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/Login/IsEmailExist")]
        public IHttpActionResult IsEmailExist(AlreadyExistsEmailModel model)
        {
            model.isExists = true;
            try
            {
                if (ModelState.IsValid)
                {
                    model.isExists = BLayer.Login.isEmailExists(model.email, model.userId);
                    if (model.isExists)
                    {
                        model.apiStatus = 1;
                        model.Message = "Email ID already exist";
                    }
                    else
                    {
                        model.Message = "Email ID available.";
                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }
    }
}
