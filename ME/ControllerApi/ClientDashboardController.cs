﻿using CLayer;
using CLayer.common;
using ME.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using CLayer.DBModel;

namespace ME.ControllerApi
{
    public class ClientDashboardController : ApiController
    {
        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/clientDashBoard/income_Expense")]
        public IHttpActionResult getIncomeVsExpense([FromBody]long profileID)
        {
            long userID = 0;
            Income_ExpenseModel cd = new Income_ExpenseModel();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                if (ModelState.IsValid)
                {

                    cd = BLayer.ClientDBInfo.getIncomeVsExpense(profileID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(cd));
        }
        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/clientDashBoard/EmergencyFund")]
        public IHttpActionResult emergencyFundDashBoard([FromBody] long profileID)
        {
            long profileID2 = 0;
            EmergencyFundModel model = new EmergencyFundModel();
            var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "ProfileID").FirstOrDefault();
            if (claim != null)
            {
                profileID2 = Convert.ToInt64(claim.Value);
            }

            try
            {
                if (profileID > 0)
                {
                    model = BLayer.EmergencyFund.getEmergencyFund(profileID);
                }
                else
                {
                    var msg = CustomErrorHandler.getProfileValidationErrors();
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(model));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/clientDashBoard/protectionInfo")]
        public IHttpActionResult protectionInfo([FromBody]long profileID)
        {
            long userID = 0;
            ProtectionNeeds pm = new ProtectionNeeds();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                if (ModelState.IsValid)
                {

                    pm = BLayer.ProtectionNeedsInfo.getProtectionNeeds(profileID);
                    pm.protectionPlan = null;
                    pm.Statement = null;
                    pm.ar = null;
                    
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(pm));
        }
        
        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/clientDashBoard/EducationInfo")]
        public IHttpActionResult educationInfo([FromBody] long profileID)
        {
            Education_Model em = new Education_Model();
            try
            {
                em = BLayer.ClientDBInfo.educationDashBoard(profileID);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(em));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/clientDashBoard/DashBoardRetirement")]
        public IHttpActionResult DBRetirement([FromBody]long profileID)
        {

            RetirementModel cdret = new RetirementModel();
            try
            {
     
                if (ModelState.IsValid)
                {

                    cdret = BLayer.ClientDBInfo.DBRetirement(profileID);
                    cdret.otherValuesModel = null;
                    cdret.PersonInfoDetails = null;
                    cdret.currentretSummary = null;
                    cdret.RetirementGoal.AdditionalNeedDetails = null;
                    cdret.RetirementGoal.RetirementGrowDetails = null;

                }

                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(cdret));
        }


        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/clientDashBoard/DashBoardInvestment")]
        public IHttpActionResult DashBoardInvestment([FromBody]long profileID)
        {

            InvestmentTaxableModel cdret = new InvestmentTaxableModel();
            try
            {
                if (profileID > 0)
                {

                    cdret = BLayer.ClientDBInfo.DashBoardInvestment(profileID);
                }
                
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(cdret));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/clientDashBoard/DashBoardLiabilities")]
        public IHttpActionResult Liabilities([FromBody]long profileID)
        {

            List<LiabilitiesModel> cdret = new List<LiabilitiesModel>();
            try
            {
                if (profileID > 0)
                {

                    cdret = BLayer.ClientDBInfo.DashBoardLiabilities(profileID);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(cdret));
        }


        //rahul
    }
}
