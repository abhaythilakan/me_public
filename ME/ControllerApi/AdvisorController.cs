﻿
using CLayer;
using CLayer.common;
using ME.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using static CLayer.common.CustomEnum;
using Newtonsoft.Json;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.IO;
using System.Text.RegularExpressions;
using System.Drawing.Drawing2D;
using System.Web;
using System.Web.Http.Cors;
using DLayer;

namespace ME.ControllerApi
{
    public class AdvisorController : ApiController
    {
        private string uploadimage(string imageString, long advisorId)
        {
            try
            {
                if (imageString != null && imageString.Trim() != "")
                {
                    byte[] bytes = Convert.FromBase64String(imageString);
                    Image images;
                    using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
                    {
                        ms.Write(bytes, 0, bytes.Length);
                        images = Image.FromStream(ms, true);
                    }
                    Bitmap bitmap = new Bitmap(images);
                    int sourceWidth = bitmap.Width;
                    //Get the image current height
                    int sourceHeight = bitmap.Height;

                    float nPercent = 0;
                    float nPercentW = 0;
                    float nPercentH = 0;
                    nPercentW = ((float)200 / (float)sourceWidth);

                    nPercentH = ((float)200 / (float)sourceHeight);

                    if (nPercentH < nPercentW)
                        nPercent = nPercentH;
                    else
                        nPercent = nPercentW;

                    int destWidth = (int)(sourceWidth * nPercent);

                    int destHeight = (int)(sourceHeight * nPercent);
                    Bitmap b = new Bitmap(destWidth, destHeight);
                    Graphics g = Graphics.FromImage((System.Drawing.Image)b);
                    g.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    g.DrawImage(bitmap, 0, 0, destWidth, destHeight);
                    g.Dispose();
                    Image images2;
                    images2 = (System.Drawing.Image)b;
                    string FILE_PATH = null;
                    if (images2 != null && advisorId>0)
                    {
                        if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Advisor\\" + advisorId + "\\Profile\\")))
                        {
                            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Advisor\\" + advisorId + "\\Profile\\"));
                        }
                        Array.ForEach(Directory.GetFiles(@"" + System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Advisor\\" + advisorId + "\\Profile\\")), System.IO.File.Delete);
                        string FN = DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss.fff");
                        FN = Regex.Replace(FN, "[.|-|:|/]", "");
                        string newdirectory = "Files\\Advisor\\" + advisorId + "\\Profile\\ProfileImage_" + FN + ".jpg";
                        if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Advisor\\" + advisorId + "\\Profile\\")))
                        {
                            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Advisor\\" + advisorId + "\\Profile\\"));
                        }
                        FILE_PATH = newdirectory;
                        images2.Save(System.Web.HttpContext.Current.Server.MapPath("~") + "\\" + FILE_PATH);
                        string imagePath = FILE_PATH;
                        return imagePath;
                    }

                    else if (advisorId == 0)
                    {
                        if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Advisor\\Temp\\Profile\\")))
                        {
                            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Advisor\\Temp\\Profile\\"));
                        }
                        Array.ForEach(Directory.GetFiles(@"" + System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Advisor\\Temp\\Profile\\")), System.IO.File.Delete);
                        string FN = DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss.fff");
                        FN = Regex.Replace(FN, "[.|-|:|/]", "");
                        string newdirectory = "Files\\Client\\Temp\\Profile\\ProfileImage_" + FN + ".jpg";
                        if (!System.IO.Directory.Exists(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Advisor\\Temp\\Profile\\")))
                        {
                            System.IO.Directory.CreateDirectory(System.Web.HttpContext.Current.Server.MapPath("~\\Files\\Advisor\\Temp\\Profile\\"));
                        }
                        FILE_PATH = newdirectory;
                        images2.Save(System.Web.HttpContext.Current.Server.MapPath("~") + "\\" + FILE_PATH);
                        string imagePath = FILE_PATH;
                        return imagePath;

                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return null;
        }


        [HttpPost]
        //[Common.Authorize(Roles = "Admin")]
        [Route("api/Advisor/deleteAdvisor")]
        public IHttpActionResult DeleteAdvisor([FromBody]long AdvisorID)
        {
            bool status = false;
            try
            {
                if (ModelState.IsValid)
                {
                    if (AdvisorID > 0)
                    {
                        status = BLayer.AdvisorReg.advisordelete(AdvisorID);
                    }
                    else
                    {
                        Messages msg = new Messages();
                        msg.apiStatus = 2;//logic errors
                        msg.Message = "Advisor must have a value";
                        return Ok(msg);
                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);

                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(status);
        }


        [HttpPost]
        [Route("api/Advisor/imageUpload")]
        public IHttpActionResult imageupload(imageUpload iu)
        {
            string path = null;
            bool Status = false;
            try
            {
                if (ModelState.IsValid)
                {
                    string authority = HttpContext.Current.Request.Url.Authority;
                    int index = iu.imageString.IndexOf("base64");
                    iu.imageString = iu.imageString.Remove(0, index + 7);
                    path = uploadimage(iu.imageString, iu.id);
                    path = "http://" + authority + "/" + path;
                    Status = BLayer.AdvisorReg.advImageUpload(path, iu.id);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(path));
        }


        [HttpPost]
        [Route("api/Advisor/imageUploadAdvisor")]
        public IHttpActionResult imageUploadAdvisor(imageUpload iu)
        {
            string path = null;
            bool Status = false;
            try
            {

                if (ModelState.IsValid)
                {
                    string authority = HttpContext.Current.Request.Url.Authority;
                    int index = iu.imageString.IndexOf("base64");
                    iu.imageString = iu.imageString.Remove(0, index + 7);
                    path = uploadimage(iu.imageString, iu.id);
                    path = "http://" + authority + "/" + path;
                    //Status = BLayer.AdvisorReg.advImageUpload(path, iu.id);
                }
            }
            catch (Exception ex) 
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(path));
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("api/Advisor/getDisclaimerDetails")]
        public IHttpActionResult GetdisclaimerBody([FromBody] long id)
        {
            string body = "";
            try
            {
                if (id > 0)
                {
                    body = BLayer.AdvisorReg.getdisclaimerbody(id);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(body));
        }

        [HttpGet]
        public IHttpActionResult testUrl()
        {
            string DomainName = HttpContext.Current.Request.Url.Host;
            string authority = HttpContext.Current.Request.Url.Authority;
            return Ok(authority);
        }
        [HttpPost]
        [AllowAnonymous]
        [Route("api/Advisor/AdvisorRegister")]
        public IHttpActionResult register_save_update(advisorDetails AdvisorDetails)
        {
            //advisorDetails ad = new advisorDetails();

            string authority = HttpContext.Current.Request.Url.Authority;
            try
            {
                if (AdvisorDetails.advisorInfo.advisorId > 0)
                {
                    this.ModelState.Remove("AdvisorDetails.UserInfo.userName");
                }
                else
                {
                    this.ModelState.Remove("AdvisorDetails.advisorInfo.finraReg");
                    this.ModelState.Remove("AdvisorDetails.advisorInfo.secReg");

                }
                if (ModelState.IsValid)
                {
                    if (AdvisorDetails.advisorInfo.imgPath == "http://" + authority + "/")
                    {
                        AdvisorDetails.advisorInfo.imgPath = null;
                    }

                    AdvisorDetails = BLayer.AdvisorReg.advisorReg(AdvisorDetails);
                    AdvisorDetails.imageString = "";
                    if (AdvisorDetails != null && AdvisorDetails.advisorInfo.advisorId > 0 && AdvisorDetails.UserInfo.userId > 0 && AdvisorDetails.UserInfo.password != "" && AdvisorDetails.UserInfo.password != null && AdvisorDetails.UserInfo.userName != "")
                    {
                        string DomainName = HttpContext.Current.Request.Url.Host;

                        CustomWebRequest wr = new CustomWebRequest("http://" + authority + "/GenerateToken", "POST", "username=" + AdvisorDetails.UserInfo.userName.Trim() + "&password=" + AdvisorDetails.UserInfo.password.Trim() + "&grant_type=password");
                        AdvisorDetails.tokeninfo = JsonConvert.DeserializeObject<tokenInfo>(wr.GetResponse());

                        #region clearing Credentils
                        AdvisorDetails.UserInfo.password = "";
                        #endregion clearingCredentils
                    }


                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            ResultModel<advisorDetails> rm = new ResultModel<advisorDetails>();
            rm.Result = AdvisorDetails;
            return Ok(rm);
            //return Ok(ResultFunction.SetReturnModel(AdvisorDetails));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Advisor/AdvisorDetails")]
        public IHttpActionResult GetAdvisor()
        {
            long userID = 0;
            long id = 0;
            advisorDetails ad = new advisorDetails();
            ResultModel<advisorDetails> rm = new ResultModel<advisorDetails>();
            try
            {

                if (ModelState.IsValid)
                {
                    var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                    var claim2 = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "advisorID").FirstOrDefault();


                    if (claim != null && claim2 != null)
                    {
                        ad = BLayer.AdvisorReg.getAdvisorDetails(Convert.ToInt64(claim2.Value), Convert.ToInt64(claim.Value));
                        // rm.Result = ad;
                        //string authority = HttpContext.Current.Request.Url.Authority;
                        //if (rm.Result.advisorInfo.imgPath != null || rm.Result.advisorInfo.imgPath != "")
                        //{
                        //    rm.Result.advisorInfo.imgPath = "http://" + authority + "/" + rm.Result.advisorInfo.imgPath;
                        //    rm.Result.imageString = rm.Result.advisorInfo.imgPath;
                        //}
                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            rm.Result = ad;
            return Ok(rm);
            //return Ok(ResultFunction.SetReturnModel(ad));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Advisor/AdvisorDetailsFList")]
        public IHttpActionResult GetAdvisordetailsFromList(long advisorID)
        {
            long userID = 0;
            long id = 0;
            advisorDetails ad = new advisorDetails();
            ResultModel<advisorDetails> rm = new ResultModel<advisorDetails>();
            try
            {

                if (ModelState.IsValid)
                {
                    var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();



                    if (claim != null && advisorID >0)
                    {
                        ad = BLayer.AdvisorReg.getAdvisorDetails(Convert.ToInt64(advisorID), Convert.ToInt64(claim.Value));

                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            rm.Result = ad;
            return Ok(rm);
            //return Ok(ResultFunction.SetReturnModel(ad));
        }

        [HttpPost]
        [Route("api/Advisor/GetAdvisorList")]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        public IHttpActionResult getAdvisorList([FromBody]AdvisorListModel inObj)
        {
            List<AdvisorModel> model = new List<AdvisorModel>();
            try
            {
                if (ModelState.IsValid)
                {
                    inObj = BLayer.AdvisorReg.getAdvisorList(inObj);

                }

                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Advisor/AddNewAdvisor")]
        public IHttpActionResult AddNewAdvisor([FromBody]advisorDetails AdvisorDetails)
        {
            string path = null;
            bool Status = false;
            string authority = HttpContext.Current.Request.Url.Authority;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                var claim2 = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "advisorID").FirstOrDefault();

            

                this.ModelState.Remove("AdvisorDetails.advContact.zip");
                this.ModelState.Remove("AdvisorDetails.advContact.state");
        


                if (Convert.ToInt64(claim2.Value) != 0)
                {
                    if (ModelState.IsValid)
                    {
                        AdvisorDetails = BLayer.AdvisorReg.addnewadvisor(AdvisorDetails, Convert.ToInt64(claim2.Value));

                        MailService.sendAddadvisorMail(AdvisorDetails.mailObj);

                        imageUpload iu = new imageUpload();
                        iu.id = AdvisorDetails.advisorInfo.advisorId;
                        iu.imageString = AdvisorDetails.imageString;
                        try
                        {
                            if (iu.imageString != null && iu.imageString != "")
                            {
                       

                                if (AdvisorDetails.advisorInfo.imgPath.Contains("\\Temp\\"))
                                {
                                    //  string authority = HttpContext.Current.Request.Url.Authority;
                                    path = AdvisorDetails.advisorInfo.imgPath.Replace("http://" + authority + "/", "");
                                    string FN = DateTime.Now.ToString("yyyy/MM/ddTHH:mm:ss.fff");
                                    FN = Regex.Replace(FN, "[.|-|:|/]", "");
                                    string newdirectory = "Files\\Client\\" + iu.id + "\\Profile\\ProfileImage_" + FN + ".jpg";

                                    string sourceLoc = System.Web.HttpContext.Current.Server.MapPath("~\\" + path);
                                    string targetLoc = System.Web.HttpContext.Current.Server.MapPath("~\\" + newdirectory);
                                    if (!System.IO.Directory.Exists(targetLoc))
                                    {
                                        System.IO.Directory.CreateDirectory(targetLoc);
                                    }
                                    System.IO.File.Copy(sourceLoc, targetLoc, true);
                                    if ((System.IO.File.Exists(System.Web.HttpContext.Current.Server.MapPath("~\\" + path))))
                                    {
                                        System.IO.File.Delete(System.Web.HttpContext.Current.Server.MapPath("~\\" + path));
                                    }
                                    AdvisorDetails.advisorInfo.imgPath = newdirectory;

                                    //model.imagePath = newdirectory;
                                    Status = BLayer.AdvisorReg.advImageUpload(AdvisorDetails.advisorInfo.imgPath, iu.id);
                                    //BLayer.ClientReg.saveClientProfile(data.clientId, data.imagePath);
                                    //data.imagePath = "http://" + authority + "/" + newdirectory;
                                }
                                else
                                {
                                    int index = iu.imageString.IndexOf("base64");
                                    iu.imageString = iu.imageString.Remove(0, index + 7);
                                    path = uploadimage(iu.imageString, iu.id);
                                    path = "http://" + authority + "/" + path;
                                    Status = BLayer.AdvisorReg.advImageUpload(path, iu.id);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new Exception("Image upload failed.");
                        }




                    }
                    else
                    {
                        var msg = CustomErrorHandler.getValidationErrors(ModelState);
                        return Ok(msg);
                    }
                }
                else
                {
                    throw new Exception("Advisor invalid");
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }

            return Ok(ResultFunction.SetReturnModel(AdvisorDetails));
        }


        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Advisor/GetAddNewAdvisor")]
        public IHttpActionResult GetAddNewAdvisor()
        {
            advisorDetails ad = new advisorDetails();
            try
            {

                if (ModelState.IsValid)
                {
                    var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                    var claim2 = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "advisorID").FirstOrDefault();


                    if (claim != null && claim2 != null)
                    {
                        ad = BLayer.AdvisorReg.getAddNewAdvisorDetails(Convert.ToInt64(claim2.Value));
                    }
                    else
                    {
                        throw new Exception("Advisor invalid");
                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(ad));
        }

    }

}



