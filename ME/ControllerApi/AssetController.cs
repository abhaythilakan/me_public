﻿using ME.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using CLayer;
using CLayer.common;
using System.Security.Claims;

namespace ME.ControllerApi
{
    public class AssetController : ApiController
    {

        #region get Details
        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/getNonRetirementAssetDetails")]
        public IHttpActionResult getNonRetirementAssetDetails([FromBody] long profileID)
        {
            long userID = 0;
            AssetModel outobj = new AssetModel();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                if (ModelState.IsValid)
                {
                    outobj = BLayer.Asset.getNonRetAssetDetails(profileID, (int)CustomEnum.AssetTypesEnum.Nonretirementinvestment);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outobj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/getRealEstateAssetDetails")]
        public IHttpActionResult getRealEstateAssetDetails([FromBody] long profileID)
        {
            long userID = 0;
            AssetModel outobj = new AssetModel();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                if (ModelState.IsValid)
                {
                    outobj = BLayer.Asset.getNonRetAssetDetails(profileID, (int)CustomEnum.AssetTypesEnum.RealEstate);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outobj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/getPersonalPropertyDetails")]
        public IHttpActionResult getPersonalPropertyDetails([FromBody] long profileID)
        {
            long userID = 0;
            AssetModel outobj = new AssetModel();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }

                if (ModelState.IsValid)
                {
                    outobj = BLayer.Asset.getNonRetAssetDetails(profileID, (int)CustomEnum.AssetTypesEnum.PersonalProperty);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outobj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/getCashaccountAssetDetails")]
        public IHttpActionResult getCashaccountAssetDetails([FromBody] long profileID)
        {
            long userID = 0;
            AssetModel outobj = new AssetModel();
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userID = Convert.ToInt64(claim.Value);
                }
                if (ModelState.IsValid)
                {
                    outobj = BLayer.Asset.getNonRetAssetDetails(profileID, (int)CustomEnum.AssetTypesEnum.Cashaccount);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outobj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/getRetirementAsset")]
        public IHttpActionResult getRetirementAsset([FromBody] long profileID)
        {

            List<Asset_RetirementModel> outobj = new List<Asset_RetirementModel>();
            try
            {

                if (ModelState.IsValid)
                {
                    outobj = BLayer.Asset.getRetirementAsset(profileID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outobj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/getRetirementAssetDetails")]
        public IHttpActionResult getRetirementAssetDetails([FromBody] long AssetID)
        {

            List<Asset_RetirementDetailsModel> outobj = new List<Asset_RetirementDetailsModel>();
            try
            {

                if (ModelState.IsValid)
                {
                    outobj = BLayer.Asset.getRetirementAssetDetails(AssetID, (int)CustomEnum.AssetTypesEnum.RetirementAsset);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outobj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/getOtherRetirementAsset")]
        public IHttpActionResult getOtherRetirementAssetDetails([FromBody] long ProfileID)
        {

            List<Asset_RetirementDetailsModel> outobj = new List<Asset_RetirementDetailsModel>();
            try
            {

                if (ModelState.IsValid)
                {
                    outobj = BLayer.Asset.getRetirementAssetDetails(ProfileID, (int)CustomEnum.AssetTypesEnum.OtherRetirement);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outobj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/getEmergencyFundandHouseholdIncome")]
        public IHttpActionResult getEmergencyFundandHouseholdIncome([FromBody] long ProfileID)
        {

            Asset_EmergencyFundModel outobj = new Asset_EmergencyFundModel();
            try
            {
                if (ModelState.IsValid)
                {
                    outobj = BLayer.Asset.getEmergencyFundandhouseholdIncome(ProfileID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(outobj));
        }

        #endregion get Details

        #region save Details

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/savePersonalProperty")]
        public IHttpActionResult savePersonalProperty(List<NonRetirementAssetsModel> inObj)
        {
            long usrID = 0;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    usrID = Convert.ToInt64(claim.Value);
                }
                if (inObj != null && inObj.Count() > 0)
                {
                    for (int ii = 0; ii < inObj.Count(); ii++)
                    {
                        this.ModelState.Remove("inObj[" + ii + "].isIncludeEmergencyFund");
                    }
                    for (int i = 0; i < inObj.Count(); i++)
                    {
                        //if (inObj[i].profileID > 0)
                        //{
                        //    this.ModelState.Remove("inObj[" + i + "].isIncludeEmergencyFund");
                        //}
                        if (ModelState.IsValid)
                        {
                            if (inObj[i].isActive == false)
                            {
                                DeleteAssetDetailsModel data = new DeleteAssetDetailsModel();
                                data.deleteID = inObj[i].assetID;
                                data.profileID = inObj[i].profileID;
                                BLayer.Asset.deleteNonRetDetails(data, usrID);
                                inObj.Remove(inObj[i]);
                            }
                            else
                            {
                                NonRetirementAssetsModel md = new NonRetirementAssetsModel();
                                md = BLayer.Asset.saveNonRetirementAsset(inObj[i], (int)CustomEnum.AssetTypesEnum.PersonalProperty, usrID);
                                inObj[i] = md;
                            }
                        }
                        else
                        {
                            var msg = CustomErrorHandler.getValidationErrors(ModelState);
                            return Ok(msg);
                        }
                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getInvalidEntry();
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/saveNonRetirementInvestment")]
        public IHttpActionResult saveNonretirementInvestment(List<NonRetirementAssetsModel> inObj)
        {
            long usrID = 0;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    usrID = Convert.ToInt64(claim.Value);
                }
                if (inObj != null && inObj.Count() > 0)
                {
                    for (int i = 0; i < inObj.Count(); i++)
                    {
                        if (ModelState.IsValid)
                        {
                            if (inObj[i].isActive == false)
                            {
                                DeleteAssetDetailsModel data = new DeleteAssetDetailsModel();
                                data.deleteID = inObj[i].assetID;
                                data.profileID = inObj[i].profileID;
                                BLayer.Asset.deleteNonRetDetails(data, usrID);
                                inObj.Remove(inObj[i]);
                            }
                            else
                            {
                                NonRetirementAssetsModel md = new NonRetirementAssetsModel();
                                md = BLayer.Asset.saveNonRetirementAsset(inObj[i], (int)CustomEnum.AssetTypesEnum.Nonretirementinvestment, usrID);
                                inObj[i] = md;
                            }
                        }
                        else
                        {
                            var msg = CustomErrorHandler.getValidationErrors(ModelState);
                            return Ok(msg);
                        }
                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getInvalidEntry();
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/saveCashAccount")]
        public IHttpActionResult saveCashaccount(List<NonRetirementAssetsModel> inObj)
        {
            long usrID = 0;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    usrID = Convert.ToInt64(claim.Value);
                }
                if (inObj != null && inObj.Count() > 0)
                {
                    for (int i = 0; i < inObj.Count(); i++)
                    {
                        if (ModelState.IsValid)
                        {
                            if (inObj[i].isActive == false)
                            {
                                DeleteAssetDetailsModel data = new DeleteAssetDetailsModel();
                                data.deleteID = inObj[i].assetID;
                                data.profileID = inObj[i].profileID;
                                BLayer.Asset.deleteNonRetDetails(data, usrID);
                                inObj.Remove(inObj[i]);
                            }
                            else
                            {
                                NonRetirementAssetsModel md = new NonRetirementAssetsModel();
                                md = BLayer.Asset.saveNonRetirementAsset(inObj[i], (int)CustomEnum.AssetTypesEnum.Cashaccount, usrID);
                                inObj[i] = md;
                            }
                        }
                        else
                        {
                            var msg = CustomErrorHandler.getValidationErrors(ModelState);
                            return Ok(msg);
                        }
                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getInvalidEntry();
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/saveRealEstate")]
        public IHttpActionResult saveRealEstate(List<NonRetirementAssetsModel> inObj)
        {
            long usrID = 0;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    usrID = Convert.ToInt64(claim.Value);
                }
                if (inObj != null && inObj.Count() > 0)
                {
                    for (int ii = 0; ii < inObj.Count(); ii++)
                    {
                        this.ModelState.Remove("inObj[" + ii + "].isIncludeEmergencyFund");
                    }
                    for (int i = 0; i < inObj.Count(); i++)
                    {
                        if (inObj[i].profileID > 0)
                        {
                            this.ModelState.Remove("inObj.isIncludeEmergencyFund");
                        }
                        if (ModelState.IsValid)
                        {
                            if (inObj[i].isActive == false)
                            {
                                DeleteAssetDetailsModel data = new DeleteAssetDetailsModel();
                                data.deleteID = inObj[i].assetID;
                                data.profileID = inObj[i].profileID;
                                BLayer.Asset.deleteNonRetDetails(data, usrID);
                                inObj.Remove(inObj[i]);
                            }
                            else
                            {
                                NonRetirementAssetsModel md = new NonRetirementAssetsModel();
                                md = BLayer.Asset.saveNonRetirementAsset(inObj[i], (int)CustomEnum.AssetTypesEnum.RealEstate, usrID);
                                inObj[i] = md;
                            }
                        }
                        else
                        {
                            var msg = CustomErrorHandler.getValidationErrors(ModelState);
                            return Ok(msg);
                        }
                    }
                }
                else
                {
                    var msg = CustomErrorHandler.getInvalidEntry();
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/saveAsset_EmergencyFundDeatails")]
        public IHttpActionResult saveAsset_EmergencyFund([FromBody]Asset_EmergencyFundModel inObj)
        {
            long usrID = 0;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    usrID = Convert.ToInt64(claim.Value);
                }
                if (inObj.profileID > 0)
                {
                    this.ModelState.Remove("inObj.householdIncome");
                }
                if (ModelState.IsValid)
                {
                    inObj.householdIncome = 0;
                    inObj = BLayer.Asset.saveAsset_EmergencyFundDeatails(inObj, usrID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/saveAssetHouseHoldAmount")]
        public IHttpActionResult saveAssetHouseHoldAmount([FromBody]Asset_EmergencyFundModel inObj)
        {
            long usrID = 0;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    usrID = Convert.ToInt64(claim.Value);
                }
                if (inObj.profileID > 0)
                {
                    this.ModelState.Remove("inObj.emergencyFund");
                }
                if (ModelState.IsValid)
                {
                    inObj.emergencyFund = 0;
                    inObj = BLayer.Asset.saveAsset_EmergencyFundDeatails(inObj, usrID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/saveRetirementDetails")]
        public IHttpActionResult saveRetirementDetails(List<Asset_RetirementDetailsModel> inObj)
        {
            long usrID = 0;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    usrID = Convert.ToInt64(claim.Value);
                }
                for (int i = 0; i < inObj.Count(); i++)
                {
                    if (ModelState.IsValid)
                    {
                        if (inObj[i].isActive == false)
                        {
                            DeleteAssetDetailsModel data = new DeleteAssetDetailsModel();
                            data.deleteID = inObj[i].retID;
                            data.profileID = Convert.ToInt64(inObj[i].profileID);
                            BLayer.Asset.deleteRetDetails(data, usrID);
                            inObj.Remove(inObj[i]);
                        }
                        else
                        {
                            Asset_RetirementDetailsModel md = new Asset_RetirementDetailsModel();
                            md = BLayer.Asset.saveAsset_RetirementDetails(inObj[i], (int)CustomEnum.AssetTypesEnum.RetirementAsset, usrID);
                            inObj[i].retID = md.retID;
                        }
                    }
                    else
                    {
                        var msg = CustomErrorHandler.getValidationErrors(ModelState);
                        return Ok(msg);
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/saveOtherRetirementDetails")]
        public IHttpActionResult saveOtherRetirementDetails(List<Asset_RetirementDetailsModel> inObj)
        {
            long usrID = 0;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    usrID = Convert.ToInt64(claim.Value);
                }
                for (int i = 0; i < inObj.Count(); i++)
                {
                    if (ModelState.IsValid)
                    {
                        if (inObj[i].isActive == false)
                        {
                            DeleteAssetDetailsModel data = new DeleteAssetDetailsModel();
                            data.deleteID = inObj[i].retID;
                            data.profileID = Convert.ToInt64(inObj[i].profileID);
                            BLayer.Asset.deleteRetDetails(data, usrID);
                            inObj.Remove(inObj[i]);
                        }
                        else
                        {
                            Asset_RetirementDetailsModel md = new Asset_RetirementDetailsModel();
                            md = BLayer.Asset.saveAsset_RetirementDetails(inObj[i], (int)CustomEnum.AssetTypesEnum.OtherRetirement, usrID);
                            inObj[i].retID = md.retID;
                        }
                    }
                    else
                    {
                        var msg = CustomErrorHandler.getValidationErrors(ModelState);
                        return Ok(msg);
                    }
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/DeleteNonRetirementDetails")]
        public IHttpActionResult deleteNonRetirementDetails([FromBody]DeleteAssetDetailsModel inObj)
        {
            long usrID = 0;
            bool status = false;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    usrID = Convert.ToInt64(claim.Value);
                }
                if (ModelState.IsValid)
                {
                    status = BLayer.Asset.deleteNonRetDetails(inObj, usrID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Asset/DeleteRetirementDetails")]
        public IHttpActionResult deleteRetDetails([FromBody]DeleteAssetDetailsModel inObj)
        {
            long usrID = 0;
            bool status = false;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    usrID = Convert.ToInt64(claim.Value);
                }
                if (ModelState.IsValid)
                {
                    status = BLayer.Asset.deleteRetDetails(inObj, usrID);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);

            }
            return Ok(ResultFunction.SetReturnModel(inObj));
        }

        #endregion save Details
    }
}
