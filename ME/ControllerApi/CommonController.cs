﻿using CLayer.common;
using ME.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Web.Http;
namespace ME.ControllerApi
{
    public class CommonController : ApiController
    {

        [HttpGet]
        [Route("api/Common/getTaxVsDeffered")]
        public IHttpActionResult getTaxVsDefferedDDL()
        {
            List<DropDownList_Int> ddl = new List<DropDownList_Int>();
            try
            {
                ddl = DDLReg.getDropDownList_Int(enumDropDownList.TaxVsDeffered_EnumOrder);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(ddl));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor")]
        [Route("api/Common/GetStatusDDL")]
        public IHttpActionResult getStatusDDL()
        {
            List<DropDownList_Int> StatusDDL = new List<DropDownList_Int>();
            try
            {
                StatusDDL = BLayer.DropDownList.DDL_List_int(enumDropDownList.Status).OrderBy(b => b.VALUE).ToList();
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(StatusDDL));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin")]
        [Route("api/Common/GetAdvisorDDL")]
        public IHttpActionResult getAdvisorDDL()
        {
            List<DropDownList_Int> AdvisorDDL = new List<DropDownList_Int>();
            try
            {
                AdvisorDDL = BLayer.DropDownList.DDL_List_int(enumDropDownList.Advisor);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(AdvisorDDL));
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("api/Common/GetDisclaimer")]
        public IHttpActionResult GetDisclaimer()
        {
            List<DropDownList_Int> DDL_Desclaimer = new List<DropDownList_Int>();
            try
            {
                DDL_Desclaimer = BLayer.DropDownList.DDL_List_int(enumDropDownList.Desclaimer);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_Desclaimer));
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("api/Common/GetState")]
        public IHttpActionResult GetState()
        {
            List<DropDownList_String> DDL_State = new List<DropDownList_String>();
            try
            {
                DDL_State = BLayer.DropDownList.DDL_List(enumDropDownListCode.State);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_State));

        }

        [HttpGet]
        [AllowAnonymous]
        [Route("api/Common/GetTimeZone")]
        public IHttpActionResult GetTimeZone()
        {
            List<DropDownList_Int> DDL_TimeZones = new List<DropDownList_Int>();
            try
            {
                DDL_TimeZones = BLayer.DropDownList.DDL_List_int(enumDropDownList.TimeZones);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_TimeZones));
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("api/Common/GetReportType")]
        public IHttpActionResult GetReportType()
        {
            List<DropDownList_Int> DDL_ReportType = new List<DropDownList_Int>();
            try
            {
                DDL_ReportType = BLayer.DropDownList.DDL_List_int(enumDropDownList.ReportType);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_ReportType));
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("api/Common/GetCurrencies")]
        public IHttpActionResult GetCurrencies()
        {
            List<DropDownList_Int> DDL_Currencies = new List<DropDownList_Int>();
            try
            {
                DDL_Currencies = BLayer.DropDownList.DDL_List_int(enumDropDownList.Currencies);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_Currencies));
        }


        [HttpGet]
        [AllowAnonymous]
        [Route("api/Common/ActionSuggestedTo")]
        public IHttpActionResult GetActionSuggestedTo()
        {
            var Identity = (ClaimsIdentity)User.Identity;
            var proID = Identity.Claims.Where(b => b.Type == "ProfileID").Select(b => b.Value).FirstOrDefault();
            long profileID = Convert.ToInt64(proID);

            // profileID = 12;
            List<DropDownList_Int> DDL_SuggestedTo = new List<DropDownList_Int>();
            try
            {
                DDL_SuggestedTo = BLayer.DropDownList.DDL_List_int_withID(enumDropDownList.ActionSuggestedTo, profileID);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_SuggestedTo));
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("api/Common/ActionNotifyType")]
        public IHttpActionResult GetActionNotifyType()
        {
            List<DropDownList_Int> DDL_NotifyType = new List<DropDownList_Int>();
            try
            {
                DDL_NotifyType = BLayer.DropDownList.DDL_List_int(enumDropDownList.ActionNotifyType);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_NotifyType));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Common/Changepassword")]
        public IHttpActionResult changepassword([FromBody] ChangePasswordModel changePwd)
        {
            long userId = 0;
            bool Status = false;
            try
            {
                var claim = ((ClaimsIdentity)User.Identity).Claims.Where(b => b.Type == "UserID").FirstOrDefault();
                if (claim != null)
                {
                    userId = Convert.ToInt64(claim.Value);
                }
                if (ModelState.IsValid)
                {
                    Status = BLayer.CommonBLayer.changePwd(changePwd, userId);
                }
                else
                {
                    var msg = CustomErrorHandler.getValidationErrors(ModelState);
                    return Ok(msg);
                }
                if (Status == false)
                {
                    var msg = CustomErrorHandler.invalidCurrentPassword();
                    return Ok(msg);
                }
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(Status));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Common/GetFutureTaxes")]
        public IHttpActionResult FutureTaxes()
        {
            List<DropDownList_Int> DDL_FutureTaxes = new List<DropDownList_Int>();
            try
            {
                DDL_FutureTaxes = BLayer.DropDownList.DDL_List_int_withID(enumDropDownList.futureTaxes, (int)CustomEnum.TaxCategoryEnum.Future);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_FutureTaxes));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Common/GetPayrollTaxes")]
        public IHttpActionResult PayrollTaxes()
        {
            List<DropDownList_Int> DDL_PayrollTaxes = new List<DropDownList_Int>();
            try
            {
                DDL_PayrollTaxes = BLayer.DropDownList.DDL_List_int_withID(enumDropDownList.PayrollTaxes, (int)CustomEnum.TaxCategoryEnum.Payroll);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_PayrollTaxes));
        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Common/GetOtherTaxes")]
        public IHttpActionResult OtherTaxes()
        {
            List<DropDownList_Int> DDL_OtherTaxes = new List<DropDownList_Int>();
            try
            {
                DDL_OtherTaxes = BLayer.DropDownList.DDL_List_int_withID(enumDropDownList.OtherTaxes, (int)CustomEnum.TaxCategoryEnum.Other);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_OtherTaxes));
        }

        [HttpGet]
        [AllowAnonymous]
        [Route("api/Common/GetIncomeAfterDeath")]
        public IHttpActionResult IncomeAfterDeath()
        {
            List<DropDownList_Int> DDL_IncomeAfterDeath = new List<DropDownList_Int>();
            try
            {
                DDL_IncomeAfterDeath = BLayer.DropDownList.DDL_List_int(enumDropDownList.IncomeAfterDeath);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_IncomeAfterDeath));

        }
        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Common/GetPayperiod")]
        public IHttpActionResult Payperiod()
        {
            List<DropDownList_Int> DDL_Payperiod = new List<DropDownList_Int>();
            try
            {
                DDL_Payperiod = BLayer.DropDownList.DDL_List_int_withID(enumDropDownList.Payperiod, (int)CustomEnum.TaxCategoryEnum.Other);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_Payperiod));
        }
        [HttpGet]
        [AllowAnonymous]
        [Route("api/Common/FindUs")]
        public IHttpActionResult GetFindTypes()
        {
            List<DropDownList_Int> DDL_FindTypes = new List<DropDownList_Int>();
            try
            {
                DDL_FindTypes = BLayer.DropDownList.DDL_List_int(enumDropDownList.findUs);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_FindTypes));

        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Common/AssetTypesNonRetirement")]
        public IHttpActionResult AssetTypesNonRetirement()
        {
            List<DropDownList_Int> DDL_FindTypes = new List<DropDownList_Int>();
            try
            {
                DDL_FindTypes = BLayer.DropDownList.DDL_List_int_withID(enumDropDownList.assetType, (int)CustomEnum.AssetTypesEnum.Nonretirementinvestment);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_FindTypes));

        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Common/AssetTypesCashaccount")]
        public IHttpActionResult CashaccountDDl()
        {
            List<DropDownList_Int> DDL_FindTypes = new List<DropDownList_Int>();
            try
            {
                DDL_FindTypes = BLayer.DropDownList.DDL_List_int_withID(enumDropDownList.assetType, (int)CustomEnum.AssetTypesEnum.Cashaccount);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_FindTypes));

        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Common/AssetTypesRetirementAsset")]
        public IHttpActionResult RetirementAsset()
        {
            List<DropDownList_Int> DDL_FindTypes = new List<DropDownList_Int>();
            try
            {
                DDL_FindTypes = BLayer.DropDownList.DDL_List_int_withID(enumDropDownList.assetType, (int)CustomEnum.AssetTypesEnum.RetirementAsset);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_FindTypes));

        }


        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Common/AssetTypesRealEstate")]
        public IHttpActionResult RealEstate()
        {
            List<DropDownList_Int> DDL_FindTypes = new List<DropDownList_Int>();
            try
            {
                DDL_FindTypes = BLayer.DropDownList.DDL_List_int_withID(enumDropDownList.assetType, (int)CustomEnum.AssetTypesEnum.RealEstate);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_FindTypes));

        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Common/AssetTypesPersonalProperty")]
        public IHttpActionResult PersonalProperty()
        {
            List<DropDownList_Int> DDL_FindTypes = new List<DropDownList_Int>();
            try
            {
                DDL_FindTypes = BLayer.DropDownList.DDL_List_int_withID(enumDropDownList.assetType, (int)CustomEnum.AssetTypesEnum.PersonalProperty);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_FindTypes));

        }

        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Common/AssetTypesOtherRetirement")]
        public IHttpActionResult OtherRetirement()
        {
            List<DropDownList_Int> DDL_FindTypes = new List<DropDownList_Int>();
            try
            {
                DDL_FindTypes = BLayer.DropDownList.DDL_List_int_withID(enumDropDownList.assetType, (int)CustomEnum.AssetTypesEnum.RetirementAsset);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_FindTypes));

        }
        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Common/PayTaxTpeDDL")]
        public IHttpActionResult PayTaxTpeDDL()
        {
            List<DropDownList_Int> DDL_FindTypes = new List<DropDownList_Int>();
            try
            {
                DDL_FindTypes = BLayer.DropDownList.DDL_List_int(enumDropDownList.PayTaxType);

            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_FindTypes));
        }
        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/IncomeDetails/getfutureIncomeStartDateType")]
        public IHttpActionResult getfutureIncomestartTypeDDL([FromBody] long profileID)
        {
            List<DropDownList_Int> DDL_FindTypes = new List<DropDownList_Int>();
            try
            {
                DDL_FindTypes = BLayer.DropDownList.DDL_List_int_withID(enumDropDownList.futureStartDateType, profileID);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_FindTypes));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/IncomeDetails/getfutureIncomeendTypeDDL")]
        public IHttpActionResult getfutureIncomeendTypeDDL([FromBody] long profileID)
        {
            List<DropDownList_Int> DDL_FindTypes = new List<DropDownList_Int>();
            try
            {
                DDL_FindTypes = BLayer.DropDownList.DDL_List_int_withID(enumDropDownList.futureEndDateType, profileID);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_FindTypes));
        }




        [HttpGet]
        [Route("api/Common/getMortageDDL")]
        public IHttpActionResult getMortageDDL()
        {
            List<DropDownList_Int> DDL = new List<DropDownList_Int>();
            try
            {
                DDL = BLayer.DropDownList.DDL_List_int(enumDropDownList.DebtMortage).OrderBy(b => b.VALUE).ToList();
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL));
        }
        [HttpGet]
        [Route("api/Common/getFixedDDL")]
        public IHttpActionResult getFixedDDL()
        {
            List<DropDownList_Int> DDL = new List<DropDownList_Int>();
            try
            {
                DDL = BLayer.DropDownList.DDL_List_int(enumDropDownList.DebtFixed).OrderBy(b => b.VALUE).ToList();
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL));
        }
        [HttpGet]
        [Route("api/Common/getRevolveDDL")]
        public IHttpActionResult getRevolveDDL()
        {
            List<DropDownList_Int> DDL = new List<DropDownList_Int>();
            try
            {
                DDL = BLayer.DropDownList.DDL_List_int(enumDropDownList.DebtRevolving).OrderBy(b => b.VALUE).ToList();
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL));
        }
        [HttpGet]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/IncomeDetails/getEducationTypeDDL")]
        public IHttpActionResult getEducationTypeDDL([FromBody] long profileID)
        {
            List<DropDownList_Int> DDL_FindTypes = new List<DropDownList_Int>();
            try
            {
                DDL_FindTypes = BLayer.DropDownList.DDL_List_int(enumDropDownList.educationType);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_FindTypes));
        }

        [HttpPost]
        [Common.Authorize(Roles = "Admin,Advisor,Client")]
        [Route("api/Common/getmanagers")]
        public IHttpActionResult getManagers([FromBody] long advisorID)
        {
            List<DropDownList_Int> DDL_Managers = new List<DropDownList_Int>();
            try
            {
                DDL_Managers = BLayer.DropDownList.DDL_List_int_withID(enumDropDownList.Managers, advisorID);
            }
            catch (Exception ex)
            {
                var msg = CustomErrorHandler.getLogicErrors(ex);
                return Ok(msg);
            }
            return Ok(ResultFunction.SetReturnModel(DDL_Managers));
        }

    }
}