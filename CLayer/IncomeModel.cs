﻿using CLayer.common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class IncomeModel
    {
        [Required(ErrorMessage = "incomeID is required")]
        [Display(Name = "Income")]
        public Nullable<long> incomeID { get; set; }
        [Required(ErrorMessage = "profileID is required")]
        [Display(Name = "Profile")]
        public Nullable<long> profileID { get; set; }
        public Nullable<long> personID { get; set; }
        [Required(ErrorMessage = "Name is required.")]
        [StringLength(25, ErrorMessage = "Name length should be less than 25.")]
        [Display(Name = "Name")]
        public string name { get; set; }
        [Required(ErrorMessage = " Future income is required.")]
        public Nullable<decimal> grossMonthlyIncome { get; set; }
        [Required(ErrorMessage = "PayType is required.")]
        [Display(Name = "Pay Type")]
        [Range(1, Int64.MaxValue, ErrorMessage = "Please select income pay type .")]
        public Nullable<int> payType { get; set; }
        [Required(ErrorMessage = "Please enter income amount.")]
        [Range(0, Int64.MaxValue, ErrorMessage = "Please enter valid income amount.")]
        [Display(Name = "Income")]
        public Nullable<decimal> payIncome { get; set; }
        public Nullable<decimal> payRollTaxes { get; set; }
        public Nullable<decimal> homePay { get; set; }
        public Nullable<int> allowances { get; set; }
        public string notes { get; set; }
        public Nullable<bool> isActive { get; set; }
        //[Required(ErrorMessage = "Income Required")]
        //[Display(Name = "cola")]
        [Range(0, 100, ErrorMessage = "Please select cola % between 0 and 100.")]
        public Nullable<decimal> cola { get; set; }
        [Range(0, long.MaxValue, ErrorMessage = "Please select valid Future income start date type.")]
        [Required(ErrorMessage = "Future income Start Date type is required.")]
        [Display(Name = "Start Type")]
        public Nullable<long> startType { get; set; }
        [Range(1, 150, ErrorMessage = "Please enter start age less than 150.")]
        public Nullable<int> startAge { get; set; }
        [Range(1, 150, ErrorMessage = "Please enter end age less than 150.")]
        public Nullable<int> endAge { get; set; }
        [Required(ErrorMessage = "Future income Start Date  is required.")]
        [Display(Name = "Start Date")]
        public Nullable<System.DateTime> startDate { get; set; }
        [Range(0, long.MaxValue, ErrorMessage = "Please select valid Future income end date type.")]
        [Required(ErrorMessage = "Future income end date type is required.")]
        [Display(Name = "Start Type")]
        public Nullable<long> endType { get; set; }
        [Required(ErrorMessage = "Future income end date is required.")]
        [Display(Name = "End Date")]
        public Nullable<System.DateTime> endDate { get; set; }
        public Nullable<bool> isFutureIncome { get; set; }
        public Nullable<System.DateTime> incomeCreatedDate { get; set; }
        public Nullable<System.DateTime> incomeUpdateDate { get; set; }
        public Nullable<long> incomeModifiedBy { get; set; }
    }
    public class IncomeTaxDetailModel
    {
        public Nullable<long> taxDetailID { get; set; }
        [Required(ErrorMessage = "Income ID missing.")]
        [Display(Name = "Income ID")]
        public Nullable<long> incomeID { get; set; }
        [Required(ErrorMessage = "Please select any tax type.")]
        [Display(Name = "Tax Type ID")]
        [Range(1, Int64.MaxValue, ErrorMessage = "Please select a valid tax type. ")]
        public Nullable<long> taxTypeID { get; set; }
        [Required(ErrorMessage = "Tax Pay Type is required.")]
        [Display(Name = "Tax Pay Type")]
        [Range(1, Int64.MaxValue, ErrorMessage = "Please select tax pay type .")]
        public Nullable<int> paytype { get; set; }
        [Required(ErrorMessage = "Please enter tax amount.")]
        public Nullable<decimal> payingTax { get; set; }
        [Display(Name = "Tax amount")]
        public Nullable<decimal> amount { get; set; }
        public string note { get; set; }
        [Required(ErrorMessage = "Mouse Hover is required.")]
        [Display(Name = "Mouse Hover")]
        public Nullable<bool> isMouseHover { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<bool> isOther { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<System.DateTime> updatedDate { get; set; }
        public Nullable<long> updatedBy { get; set; }
        [Required(ErrorMessage = "Profile ID missing.")]
        [Display(Name = "Profile ID")]
        public Nullable<long> profileID { get; set; }
        public string TypeName { get; set; }
    }
    public class IncomeOtherTaxModel
    {
        public decimal totalTax { get; set; }
        public List<IncomeTaxDetailModel> otherTaxDetails { get; set; }

    }
    public class IncomeProjectionModel
    {
        public string name { get; set; }
        public decimal inflation { get; set; }
        public decimal retage { get; set; }
        public string role { get; set; }
        public List<IncomeProjectionDetailsModel> IncomeProjectionDetails { get; set; }
        public decimal totalSum { get; set; }
        public decimal grossMonthlyIncome { get; set; }
        public decimal otherTotalIncomeBeforeRet { get; set; }
        public decimal totalTax { get; set; }

    }

    public class IncomeProjectionObjectsModel  // showing saperate graph for angular JS

    {
        public IncomeProjectionModel primary { get; set; }
        public IncomeProjectionModel spouse { get; set; }
        public IncomeProjectionModel other { get; set; }
    }

    public class IncomeProjectionGoModel
    {
        [Required(ErrorMessage = "profile missing")]
        [Display(Name = "Profile ID")]
        public Nullable<long> profileID { get; set; }
    
        [Range(0, 15, ErrorMessage = "Select primary percentage of inflation between 0 and 15.")]
        [Display(Name = "primaryRate")]
        public Nullable<long> primaryRate { get; set; }
      
        [Range(0, 15, ErrorMessage = "Select spouse percentage of inflation between 0 and 15.")]
        [Display(Name = "spouseRate")]
        public Nullable<long> spouseRate { get; set; }

    }
    public class IncomeProjectionDetailsModel
    {
        public int year { get; set; }
        public int age { get; set; }
        public decimal amount { get; set; }
        public Nullable<decimal> Tax { get; set; }
    }
    public class IncomePorjectionObjectModel
    {
        public long incomeID { get; set; }
        public Nullable<DateTime> dob { get; set; }
        public bool isprimary { get; set; }
        public string name { get; set; }
        public int retAge { get; set; }
        public int lifeExpectancy { get; set; }
        public decimal income { get; set; }
        public bool isFuture { get; set; }
        public Nullable<DateTime> startDate { get; set; }
        public Nullable<decimal> cola { get; set; }
        public Nullable<DateTime> endDate { get; set; }
        public Nullable<decimal> totalTax { get; set; }

    }


}
