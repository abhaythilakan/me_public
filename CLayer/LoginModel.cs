﻿using CLayer.common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class LoginModel : Success
    {
        [Required(ErrorMessage = "Invalid Username")]
        public string username { get; set; }
        [Required(ErrorMessage = "Invalid Password")]
        public string password { get; set; }
        [Required(ErrorMessage = "Invalid Grant Type")]
        public string grant_Type { get; set; }
    }

    public class AlreadyExistsModel : Success
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Invalid Username")]
        [Display(Name ="Username")]
        public string userName { get; set; }


        public long userId { get; set; }
        public bool isExists { get; set; }
        public string Message { get; set; }
    }
    public class AlreadyExistsEmailModel : Success
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Invalid EmailID")]
        [Display(Name = "Email")]
        public string email { get; set; }


        public long userId { get; set; }
        public bool isExists { get; set; }
        public string Message { get; set; }
    }
}
