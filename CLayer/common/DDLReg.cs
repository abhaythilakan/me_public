﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CLayer;
using System.Security.Claims;
namespace CLayer.common
{
    public class DDLReg
    {
        public static List<DropDownList_String> getDropDownList(enumDropDownListCode ddlType)
        {
            List<DropDownList_String> DDL = new List<DropDownList_String>();
            try
            {
                using (CLayer.DBModel.ME_v2Entities db = new CLayer.DBModel.ME_v2Entities())
                {
                    if (ddlType == enumDropDownListCode.State)
                    {
                        DDL.Add(new DropDownList_String() { VALUE = "0", TEXT = "--Select--" });
                        DDL.AddRange(db.States.Where(b => b.stateCode != null).Select(b => new CLayer.common.DropDownList_String() { VALUE = b.stateCode, TEXT = b.stateName.ToString() }).ToList());
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return DDL;
        }
        public static List<DropDownList_Int> getDropDownList_Int(enumDropDownList ddlType, long id = 0)
        {

            List<DropDownList_Int> DDL = new List<DropDownList_Int>();
            try
            {
                using (CLayer.DBModel.ME_v2Entities db = new CLayer.DBModel.ME_v2Entities())
                {
                    if (ddlType == enumDropDownList.StartDate)
                    {
                        //DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.eduStartTypeEnum.age, TEXT = "Age" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.eduStartTypeEnum.date, TEXT = "Date" });
                    }
                    if (ddlType == enumDropDownList.Currencies)
                    {
                        DDL = db.Currencies.Select(b => new CLayer.common.DropDownList_Int() { VALUE = b.id, TEXT = b.name.ToString() }).ToList();
                    }
                    else if (ddlType == enumDropDownList.Desclaimer)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange(db.Desclaimers.Where(b => b.desclaimerName != null).Select(b => new CLayer.common.DropDownList_Int() { VALUE = b.desclaimerID, TEXT = b.desclaimerName }).ToList());
                    }
                    else if (ddlType == enumDropDownList.TimeZones)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange(db.GlobalTimeZones.Select(b => new CLayer.common.DropDownList_Int() { VALUE = b.timeZoneID, TEXT = b.location.ToString() + " " + b.type.ToString() + " " + b.time.ToString() + " " + b.shortlocation.ToString() }).ToList());
                    }
                    else if (ddlType == enumDropDownList.IncomeAfterDeath)
                    {
                        //  DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.incomeAfterDeathEnum.lifeExpectancyOfSpouse, TEXT = "Life expectancy of spouse" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.incomeAfterDeathEnum.unlimited, TEXT = "Unlimited" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.incomeAfterDeathEnum.numberOfYears, TEXT = "Specific number of years" });
                    }
                    else if (ddlType == enumDropDownList.ActionSuggestedTo)
                    {
                        // DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange(db.Persons.Where(b => b.profileID == id).Select(b => new CLayer.common.DropDownList_Int() { VALUE = b.personID, TEXT = b.firstName }).ToList());
                        if (DDL != null && DDL.Count > 1)
                        {
                            DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "Both " + DDL[0].TEXT + " and " + DDL[1].TEXT });
                        }

                    }
                    else if (ddlType == enumDropDownList.ActionNotifyType)
                    {
                        DDL.AddRange(db.actionNotifications.Where(b => b.notificationName != null).Select(b => new CLayer.common.DropDownList_Int() { VALUE = b.notificationID, TEXT = b.notificationName }).ToList());
                    }
                    else if (ddlType == enumDropDownList.PayrollTaxes)
                    {
                        // DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange(db.IncomeTaxes.Where(b => b.isActive == true && b.taxTypeID == id).Select(b => new CLayer.common.DropDownList_Int() { VALUE = b.taxID, TEXT = b.taxName }).ToList());
                    }
                    else if (ddlType == enumDropDownList.OtherTaxes)
                    {
                        //DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange(db.IncomeTaxes.Where(b => b.isActive == true && b.taxTypeID == id).Select(b => new CLayer.common.DropDownList_Int() { VALUE = b.taxID, TEXT = b.taxName }).ToList());
                    }
                    else if (ddlType == enumDropDownList.futureTaxes)
                    {
                        //  DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange(db.IncomeTaxes.Where(b => b.isActive == true && b.taxTypeID == id).Select(b => new CLayer.common.DropDownList_Int() { VALUE = b.taxID, TEXT = b.taxName }).ToList());
                    }
                    else if (ddlType == enumDropDownList.ReportType)
                    {
                        DDL.Add(new DropDownList_Int()
                        {
                            VALUE = (int)CustomEnum.reportTypeEnum.expressReport,
                            TEXT = "Express Report"
                        });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.reportTypeEnum.advancedReport, TEXT = "Advanced Report" });
                    }
                    else if (ddlType == enumDropDownList.findUs)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange(db.findUsTypes.Where(b => b.findid != 0).Select(b => new CLayer.common.DropDownList_Int() { VALUE = b.findid, TEXT = b.typeName.ToString() }).ToList());
                    }
                    else if (ddlType == enumDropDownList.Advisor)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--All--" });
                        DDL.AddRange(db.Advisors.Where(b => b.isActive == true).Select(b => new CLayer.common.DropDownList_Int() { VALUE = b.advisorId, TEXT = b.firstName.ToString() + " " + b.lastName.ToString() }).ToList());
                    }
                    else if (ddlType == enumDropDownList.Status)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--All--" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.recordStatus.Active, TEXT = "Active" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.recordStatus.FormerClients, TEXT = "Former Clients" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.recordStatus.Inactive, TEXT = "Inactive" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.recordStatus.Prospects, TEXT = "Prospects" });
                    }
                    else if (ddlType == enumDropDownList.DebtMortage)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange((from dt in db.DebtTypes
                                      join dc in db.DebtCats on dt.debtCatId equals dc.debtCatId
                                      where dc.catName == "Mortage"
                                      orderby dt.typeOrder ascending
                                      select new DropDownList_Int { TEXT = dt.debtTypeName.ToString(), VALUE = dt.debtTypeId }).ToList());
                    }
                    else if (ddlType == enumDropDownList.DebtFixed)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange((from dt in db.DebtTypes
                                      join dc in db.DebtCats on dt.debtCatId equals dc.debtCatId
                                      where dc.catName == "Fixed"
                                      orderby dt.typeOrder ascending
                                      select new DropDownList_Int { TEXT = dt.debtTypeName.ToString(), VALUE = dt.debtTypeId }).ToList());
                    }
                    else if (ddlType == enumDropDownList.DebtRevolving)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange((from dt in db.DebtTypes
                                      join dc in db.DebtCats on dt.debtCatId equals dc.debtCatId
                                      where dc.catName == "Revolving"
                                      orderby dt.typeOrder ascending
                                      select new DropDownList_Int { TEXT = dt.debtTypeName.ToString(), VALUE = dt.debtTypeId }).ToList());
                    }

                    else if (ddlType == enumDropDownList.OtherInsurance)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange(db.insuranceTypes.Select(x => new DropDownList_Int { TEXT = x.insuranceTypeName, VALUE = x.insuranceTypeID }).ToList());
                    }
                    else if (ddlType == enumDropDownList.LifeInsurance)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange(db.lifeTypes.Select(x => new DropDownList_Int { TEXT = x.lifeTypeName, VALUE = x.lifeTypeID }).ToList());
                    }
                    else if (ddlType == enumDropDownList.Payperiod)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.PayperiodEnum.Monthly, TEXT = "Monthly" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.PayperiodEnum.Semimonthly, TEXT = "Semi Monthly" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.PayperiodEnum.Weekly, TEXT = "Weekly" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.PayperiodEnum.Biweekly, TEXT = "Biweekly" });
                    }
                    else if (ddlType == enumDropDownList.assetType)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange((from dt in db.AssetTypes
                                      join dc in db.AssetCategories on dt.assetCatID equals dc.catID
                                      where dt.isActive == true && dt.assetCatID == id
                                      select new DropDownList_Int { TEXT = dt.typeName.ToString(), VALUE = dt.assetTypeID }).ToList());
                    }
                    else if (ddlType == enumDropDownList.AssetCategory)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange((from dt in db.AssetCategories
                                      where dt.isActive == true
                                      select new DropDownList_Int { TEXT = dt.catName.ToString(), VALUE = dt.catID }).ToList());
                    }
                    else if (ddlType == enumDropDownList.PayTaxType)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.PayTaxperiodEnum.Annually, TEXT = "Annually" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.PayTaxperiodEnum.Monthly, TEXT = "Monthly" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.PayTaxperiodEnum.Quarterly, TEXT = "Quarterly" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.PayTaxperiodEnum.Semiannually, TEXT = "Semi Annually" });
                    }
                    else if (ddlType == enumDropDownList.PayType)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.PayEnum.Annually, TEXT = "Annually" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.PayEnum.SemiAnnually, TEXT = "Semi Annually" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.PayEnum.Quarterly, TEXT = "Quarterly" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.PayEnum.Monthly, TEXT = "Monthly" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.PayEnum.Semimonthly, TEXT = "Semi Monthly" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.PayEnum.Weekly, TEXT = "Weekly" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.PayEnum.Biweekly, TEXT = "Biweekly" });
                    }
                    else if (ddlType == enumDropDownList.futureStartDateType)
                    {

                        DDL.AddRange(db.Persons.Where(b => b.profileID == id).Select(b => new CLayer.common.DropDownList_Int() { VALUE = b.personID, TEXT = b.firstName + "'s Age" }).ToList());
                        if (DDL != null && DDL.Count > 1)
                        {
                            DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "Other Date" });
                        }
                        else
                        {
                            DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "Other Date" });
                        }
                    }
                    else if (ddlType == enumDropDownList.futureEndDateType)
                    {

                        DDL.AddRange(db.Persons.Where(b => b.profileID == id).Select(b => new CLayer.common.DropDownList_Int() { VALUE = b.personID, TEXT = b.firstName + "'s Age" }).ToList());
                        if (DDL != null && DDL.Count > 1)
                        {
                            DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "Other Date" });
                        }
                        else
                        {
                            DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "Other Date" });
                        }
                    }
                    else if (ddlType == enumDropDownList.educationType)
                    {

                        DDL.AddRange(db.Education_Type.Select(b => new CLayer.common.DropDownList_Int() { VALUE = b.EduTypeID, TEXT = b.EduType }).ToList());

                    }
                    else if (ddlType == enumDropDownList.expenseCategory)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange(db.ExpenseCategories.Select(x => new DropDownList_Int { TEXT = x.categoryName, VALUE = x.id }).ToList());
                    }
                    else if (ddlType == enumDropDownList.expenseSubCategory)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange(db.ExpenseSubCategories.Where(b => b.expenseCategoryId == id).Select(x => new DropDownList_Int { TEXT = x.subCategory, VALUE = x.id }).ToList());
                    }
                    else if (ddlType == enumDropDownList.expenseType)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange(db.ExpenseTypes.Where(b => b.expenseSubTypeId == id).Select(x => new DropDownList_Int { TEXT = x.TypeName, VALUE = x.id }).ToList());
                    }
                    else if (ddlType == enumDropDownList.TaxVsDeffered_EnumOrder)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.TaxVsDeffered_EnumOrder.TaxDefferedAT, TEXT = CustomEnum.TaxVsDeffered_EnumOrder.TaxDefferedAT.ToCaption() });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.TaxVsDeffered_EnumOrder.TaxFreeAT, TEXT = CustomEnum.TaxVsDeffered_EnumOrder.TaxFreeAT.ToCaption() });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.TaxVsDeffered_EnumOrder.TaxableAT, TEXT = CustomEnum.TaxVsDeffered_EnumOrder.TaxableAT.ToCaption() });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.TaxVsDeffered_EnumOrder.TaxDefferedBT, TEXT = CustomEnum.TaxVsDeffered_EnumOrder.TaxDefferedBT.ToCaption() });
                    }
                    else if (ddlType == enumDropDownList.Managers)
                    {
                        DDL.AddRange((from a in db.Advisors
                                      join u in db.Users on a.advisorId equals u.advisorId
                                      where a.isActive == true && u.isManager == true
                                      select new DropDownList_Int { TEXT = a.firstName.ToString() + " " + a.lastName.ToString(), VALUE = a.advisorId }).ToList());

                        DDL.Add(db.Advisors.Where(b => b.advisorId == id).Select(a => new DropDownList_Int { TEXT = a.firstName.ToString() + " " + a.lastName.ToString(), VALUE = a.advisorId }).FirstOrDefault());


                        // DDL.Add(db.Advisors.Where(b => b.advisorId == id).Select(a => new DropDownList_Int { TEXT = "AAA", VALUE = 1 }).FirstOrDefault());

                    }
                    else if (ddlType == enumDropDownList.SalesPersons)
                    {
                        DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.AddRange(db.SalesPersons.Where(b => b.sp_isActive == true).Select(x => new DropDownList_Int { TEXT = x.sp_FName + " " + x.sp_LName, VALUE = x.sp_Id }).ToList());
                    }
                    else if(ddlType == enumDropDownList.FAQs)
                    {
                        //DDL.Add(new DropDownList_Int() { VALUE = 0, TEXT = "--Select--" });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.FAQsection.TopSupportQuestions, TEXT = CustomEnum.FAQsection.TopSupportQuestions.ToCaption() });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.FAQsection.Billing, TEXT = CustomEnum.FAQsection.Billing.ToCaption() });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.FAQsection.TechnicalAssistance, TEXT = CustomEnum.FAQsection.TechnicalAssistance.ToCaption() });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.FAQsection.Security, TEXT = CustomEnum.FAQsection.Security.ToCaption() });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.FAQsection.AdvisorLogIn, TEXT = CustomEnum.FAQsection.AdvisorLogIn.ToCaption() });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.FAQsection.MultipleAdvisorSubscriptions, TEXT = CustomEnum.FAQsection.MultipleAdvisorSubscriptions.ToCaption() });
                        DDL.Add(new DropDownList_Int() { VALUE = (int)CustomEnum.FAQsection.ClientReports, TEXT = CustomEnum.FAQsection.ClientReports.ToCaption() });
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return DDL;
        }

    }
}
