﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer.common
{

    public class Messages
    {
        public int apiStatus { get; set; }
        public string Message { get; set; }
        public List<ValidationErrors> ValidationErrors { get; set; }
    }

    public class ValidationErrors
    {
        public string Field { get; set; }
        public string FieldErrorMessage { get; set; }
    }

    public class Success
    {
        private int id = 0;
        public int apiStatus { get { return id; } set { id = (int)CustomEnum.apiStatusEnum.Success; } }
    }

}
