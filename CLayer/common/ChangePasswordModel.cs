﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer.common
{
    public class ChangePasswordModel:Success
    {
        [Required(ErrorMessage = "Current Password is required")]
        [StringLength(20, ErrorMessage = "Password charactor length should be less that 20")]
        [Display(Name = "Old Password")]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*[0-9])(?=(?:.*?[!@#$%\^&*\(\)\-_+=;:'""\/\[\]{},.<>|`]){2}).{8,20}$", ErrorMessage = "Password must contain at least two special characters, Password must be at least eight characters long, Password must be alpha numeric")]
        public string oldPwd { get; set; }
        [Required(ErrorMessage = "New Password is required")]
        [StringLength(20, ErrorMessage = "Password charactor length should be less that 20")]
        [Display(Name = "New Password")]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*[0-9])(?=(?:.*?[!@#$%\^&*\(\)\-_+=;:'""\/\[\]{},.<>|`]){2}).{8,20}$", ErrorMessage = "Password must contain at least two special characters, Password must be at least eight characters long, Password must be alpha numeric")]
        public string newPwd { get; set; }
    }
}
