﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer.common
{
    public class DropDownList_Int
    {
        public long VALUE { get; set; }
        public string TEXT { get; set; }
    }
    public class DropDownList_String //: IDropDownList_String
    {
        public string VALUE { get; set; }
        public string TEXT { get; set; }
        //public string VALUE { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        //public string TEXT { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    }

    public enum enumDropDownList
    {
        Advisor = 2,
        Client = 3,
        TimeZones = 4,
        Currencies = 5,
        ReportType = 6,
        DebtMortage = 7,
        DebtFixed = 8,
        DebtRevolving = 9,
        StartDate = 10,
        Desclaimer = 11,
        OtherInsurance = 12,
        LifeInsurance = 13,
        IncomeAfterDeath = 14,
        ActionSuggestedTo = 15,
        ActionNotifyType = 16,
        PayrollTaxes = 17,
        OtherTaxes = 18,
        futureTaxes = 19,
        expenseCategory = 20,
        expenseSubCategory = 21,
        expenseType = 22,
        Payperiod = 23,
        findUs = 24,
        Status = 25,
        assetType = 26,
        ActionNames = 27,
        ActionAssignedTo = 28,
        insuranceType = 29,
        educationType = 30,
        InsuranceTypesEnum = 31,
        futureStartDateType = 32,
        futureEndDateType = 33,
        PayTaxType = 34,
        PayType = 35,
        AssetCategory = 36,
        TaxVsDeffered_EnumOrder = 37,
        Managers = 38,
        SalesPersons = 39,
        FAQs = 40
    }

    public enum enumDropDownListCode
    {
        State = 1
    }

}
