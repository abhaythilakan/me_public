﻿using CLayer.common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class ForgotPasswodModel// : Success
    {
        [Required(ErrorMessage = "Password is required")]
        [StringLength(20, ErrorMessage = "Password charactor length should be less that 20")]
        [Display(Name = "Password")]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*[0-9])(?=(?:.*?[!@#$%\^&*\(\)\-_+=;:'""\/\[\]{},.<>|`]){2}).{8,20}$", ErrorMessage = "Password must contain at least two special characters, Password must be at least eight characters long, Password must be alpha numeric")]
        public string password { get; set; }
        [Required(ErrorMessage = "Token is required")]
        public string token { get; set; }
        [Required(ErrorMessage = "User is required")]
        public long userID { get; set; }
    }

    public class ForgotRequestConfirmModel
    {
        public string email { get; set; }
        public string token { get; set; }
    }

}