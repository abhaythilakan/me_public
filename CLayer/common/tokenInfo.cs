﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer.common
{
    public class tokenInfo
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string expires_in { get; set; }
        public string authority { get; set; }
    }

    public class SessionContext
    {
        public Nullable<long> UserID { get; set; }
        public Nullable<long> advisorID { get; set; }
        public Nullable<long> ClientID { get; set; }
        public Nullable<long> ProfileID { get; set; }
        public string Role { get; set; }
        public bool hasClients { get; set; }
    }
}
