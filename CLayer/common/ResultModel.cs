﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer.common
{
    public class ResultModel<T>
    {
        public T Result { get; set; }
        public int apiStatus { get; set; }
        public string Message { get; set; }
        public List<ValidationErrors> ValidationErrors { get; set; }

        public ResultModel()
        {
            apiStatus = (int)CustomEnum.apiStatusEnum.Success;
        }
    }

    public static class ResultFunction
    {
        public static ResultModel<T> SetReturnModel<T>(T model)
        {
            return new ResultModel<T> { Result = model };
        }
    }
}
