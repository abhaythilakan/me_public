﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer.common
{
    public class defaults
    {

        public Nullable<long> otherID { get; set; }
        public Nullable<long> profileID { get; set; }
        public Nullable<long> advisorID { get; set; }
        public Nullable<long> clientID { get; set; }
        public Nullable<decimal> homeValue { get; set; }
        public string homeValuecID { get; set; }
        public Nullable<decimal> liquid { get; set; }
        public string liquidcID { get; set; }
        public Nullable<decimal> retirementNeed { get; set; }
        public string retirementNeedcID { get; set; }
        public Nullable<decimal> inflation { get; set; }
        public String actionPlanNotes { get; set; }
        public Nullable<decimal> defaultDebtMinPay { get; set; }
        public Nullable<decimal> defaultDebtPercentPay { get; set; }
        public string defaultCID { get; set; }
        public Nullable<double> returnRateA { get; set; }
        public Nullable<double> returnRateB { get; set; }
        public Nullable<double> returnRateC { get; set; }
        public string defaultState { get; set; }
        public Nullable<int> defaultRetirementAge { get; set; }
        public Nullable<int> defaultLifeExpectancy { get; set; }
        public Nullable<decimal> defaultFinalExpenses { get; set; }
        public Nullable<int> defaultDeathIncomeYears { get; set; }
        public Nullable<int> defaultCollegeStartMonth { get; set; }
        public Nullable<decimal> defaultTuitionIncrease { get; set; }
        public Nullable<int> defaultCollegeAge { get; set; }
        public Nullable<int> defaultCollegeYears { get; set; }
        public Nullable<double> defaultEducationPercentPay { get; set; }
        public Nullable<decimal> defaultEmergencyFund { get; set; }
        public string defaultEmail { get; set; }
        public Nullable<double> defaultEmergencyRateA { get; set; }
        public Nullable<double> defaultEmergencyRateB { get; set; }
        public Nullable<double> defaultEmergencyRateC { get; set; }
        public Nullable<double> defaultPreRetirementRate { get; set; }
        public Nullable<double> defaultRetirementRate { get; set; }
        public Nullable<double> defaultRetirementTax { get; set; }
        public string defaultPositionGraph { get; set; }
        public Nullable<double> defaultRetirementIncome { get; set; }
        public Nullable<double> defaultDeathIncome { get; set; }
        public Nullable<bool> emailOnClientAccess { get; set; }
        public Nullable<int> defaultSchoolA { get; set; }
        public Nullable<int> defaultSchoolB { get; set; }
        public Nullable<int> defaultSchoolC { get; set; }
        public Nullable<int> defaultSchoolD { get; set; }
        public Nullable<int> defaultSchoolE { get; set; }
        public string defaultAssetOffset { get; set; }
        public string reportTitle { get; set; }
        public string optComment { get; set; }
        public Nullable<DateTime> homeValueAsOf { get; set; }
        public Nullable<int> localTimeZoneId { get; set; }
    }
}
