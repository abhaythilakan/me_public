﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer.common
{
    public static class CustomEnum
    {
        public enum recordStatus
        {
            Active = 1,
            Prospects = 2,
            Inactive = 3,
            FormerClients = 4
        }

        public enum userRoles
        {
            Admin = 1,
            Advisor = 2,
            Client = 3
        }

        public enum userCatEnum
        {
            Advisor = 1,
            Client = 2,
            Profile = 3
        };
        public enum apiStatusEnum
        {
            Success = 0,
            ValidationError = 1,
            Error = 2
        };
        public enum reportTypeEnum
        {
            advancedReport = 1,
            expressReport = 2
        };

        public enum eduStartTypeEnum
        {
            age = 1,
            date = 2
        };
        public enum userTypeEnum
        {
            primary = 1,
            spouse = 2
        };
        public enum debtCatEnum
        {
            Mortage = 1,
            Fixed = 2,
            Revolving = 3
        };
        public enum ColgSearchType
        {
            name = 1,
            state = 2,
            empty = 3
        };
        public enum graphTypeEnum
        {
            pie = 1,
            bar = 2
        };
        public enum emergencyFundMonthEnum
        {
            months3 = 3,
            months6 = 6
        };
        public enum incomeAfterDeathEnum
        {
            lifeExpectancyOfSpouse = 1,
            unlimited = 2,
            numberOfYears = 3
        };
        public enum actionTypeEnum
        {
            All = 1,
            AllToday = 2,
            AllThisMonth = 3,
            pastDue = 4,
            upcoming = 5,
            Progressing = 6,
            Completed = 7
        };
        public enum actionStatus
        {
            Completed = 1,
            Open = 2,
            Progressing = 3,
            NotActive = 4
        };
        public enum TaxCategoryEnum
        {
            Payroll = 1,
            Other = 2,
            Future = 3
        };
        public enum PayperiodEnum
        {
            Monthly = 1,
            Weekly = 2,
            Semimonthly = 3,
            Biweekly = 4
        };
        public enum IncomeTypeEnum
        {
            MonthlyIncome = 1,
            FutureIncome = 2
        };
        public enum AssetTypesEnum
        {
            Nonretirementinvestment = 1,
            Cashaccount = 2,
            RetirementAsset = 3,
            RealEstate = 4,
            PersonalProperty = 5,
            OtherRetirement = 6,

            Education = 7,
            Insurance = 8,
            LifeInsurance = 9,
            Expense = 10,
            Tax = 11
        };
        public enum RealEstateTypesEnum
        {
            PrimaryResidence = 1,
            PersonalProperty = 2,
            OtherRealEstate = 3,
        };
        public enum PayTaxperiodEnum
        {
            Monthly = 1,//Rah:3 months  calc- (amount)
            Quarterly = 2, //Rah:3 months  calc- (amount)/3
            Semiannually = 3,//Rah:6 months  calc- (amount)/6
            Annually = 4//Rah:3 months  calc- (amount)/12
        };
        public enum PayEnum
        {
            Annually = 1,
            SemiAnnually = 2,
            Quarterly = 3,
            Monthly = 4,
            Semimonthly = 5,
            Weekly = 6,
            Biweekly = 7
        };

        public enum InsuranceTypesEnum
        {
            LifeExpectancy = 1,
            Unlimited = 2,
            SpecificNoOfYear = 3
        };
        public enum ActionAssignedTO
        {
            Advisor = 1,
            Primary = 2,
            Spouse = 3,
            Both = 4,
            Other = 0
        };

        public enum TaxVsDeffered_EnumOrder
        {
            TaxDefferedAT = 1,
            TaxFreeAT = 2,
            TaxableAT = 3,
            TaxDefferedBT = 4
        }

        public static string ToCaption(this TaxVsDeffered_EnumOrder enm)
        {
            string returnValue;
            if ((int)enm == (int)TaxVsDeffered_EnumOrder.TaxDefferedAT)
            {
                returnValue = "Tax Deffered - After Tax";
            }
            else if ((int)enm == (int)TaxVsDeffered_EnumOrder.TaxFreeAT)
            {
                returnValue = "Tax Free - After Tax";
            }
            else if ((int)enm == (int)TaxVsDeffered_EnumOrder.TaxableAT)
            {
                returnValue = "Taxable - After Tax";
            }
            else if ((int)enm == (int)TaxVsDeffered_EnumOrder.TaxDefferedBT)
            {
                returnValue = "Tax Deffered - Before Tax";
            }
            else
            {
                returnValue = "";
            }
            return returnValue;
        }

        public enum FAQsection
        {
            TopSupportQuestions = 1,
            Billing,
            TechnicalAssistance,
            Security,
            AdvisorLogIn,
            MultipleAdvisorSubscriptions,
            ClientReports
        }

        public static string ToCaption(this FAQsection enm)
        {
            string returnValue;
            if ((int)enm == (int)FAQsection.TopSupportQuestions)
            {
                returnValue = "Top Support Questions";
            }
            else if ((int)enm == (int)FAQsection.Billing)
            {
                returnValue = "Billing";
            }
            else if ((int)enm == (int)FAQsection.TechnicalAssistance)
            {
                returnValue = "Technical Assistance";
            }
            else if ((int)enm == (int)FAQsection.Security)
            {
                returnValue = "Security";
            }
            else if ((int)enm == (int)FAQsection.AdvisorLogIn)
            {
                returnValue = "Advisor Log In";
            }
            else if ((int)enm == (int)FAQsection.MultipleAdvisorSubscriptions)
            {
                returnValue = "Multiple Advisor Subscriptions";
            }
            else if ((int)enm == (int)FAQsection.ClientReports)
            {
                returnValue = "Client Reports";
            }
            else
            {
                returnValue = "";
            }
            return returnValue;
        }

        public enum EmailAdvisorCategory
        {
            CurrentAdvisers = 0,
            AdviserHasExpiredTheirTrialWithoutValidPayment = 1,
            PaidAdviserHasMissedABillingPeriod_LessThan30days = 2,
            PaidAdviserHasExpired = 3,
            AdviserHasCanceledTheirBillingButCanStillLoginAndRenew = 4,
            AdviserIsSetToInactiveAndCannotLogin = 5,
            CurrentAdvisersOfAParticularCouponCode = 6
        }
        public static string ToCaption(this EmailAdvisorCategory enm)
        {
            string returnValue;
            if ((int)enm == (int)EmailAdvisorCategory.CurrentAdvisers)
            {
                returnValue = "Current Advisers";
            }
            else if ((int)enm == (int)EmailAdvisorCategory.AdviserHasExpiredTheirTrialWithoutValidPayment)
            {
                returnValue = "Adviser has expired their trial without valid payment";
            }
            else if ((int)enm == (int)EmailAdvisorCategory.PaidAdviserHasMissedABillingPeriod_LessThan30days)
            {
                returnValue = "Paid adviser has missed a billing period (less than 30 days)";
            }
            else if ((int)enm == (int)EmailAdvisorCategory.PaidAdviserHasExpired)
            {
                returnValue = "Paid adviser has expired";
            }
            else if ((int)enm == (int)EmailAdvisorCategory.AdviserHasCanceledTheirBillingButCanStillLoginAndRenew)
            {
                returnValue = "Adviser has canceled their billing but can still login and renew";
            }
            else if ((int)enm == (int)EmailAdvisorCategory.AdviserIsSetToInactiveAndCannotLogin)
            {
                returnValue = "Adviser is set to inactive and cannot login";
            }
            else if ((int)enm == (int)EmailAdvisorCategory.CurrentAdvisersOfAParticularCouponCode)
            {
                returnValue = "Current Advisers of a particular coupon code";
            }
            else
            {
                returnValue = "";
            }
            return returnValue;
        }
    }
}
