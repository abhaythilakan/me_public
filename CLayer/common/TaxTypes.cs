﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer.common
{
    public class TaxTypes
    {
        public Nullable<long> taxTypeID { get; set; }
        public string taxTypeName { get; set; }
        public string TaxCategory { get; set; }
        public Nullable<long> orderNum { get; set; }
    }
}
