﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class DefaultModel
    {
    }

    public class FavCollegesModel
    {
        public long id { get; set; }
        public long defaultEduID { get; set; }
        public long collegeID { get; set; }
        public string collegeName { get; set; }
        public string stateCode { get; set; }
        public string stateName { get; set; }
        public decimal inState { get; set; }
        public decimal outState { get; set; }
        public string finYear { get; set; }
        public string city { get; set; }
        public Nullable<bool> isFav { get; set; }
    }

    public class DefaultsEducationModel
    {
        public long defaultID { get; set; }
        public Nullable<long> profileID { get; set; }
        public Nullable<long> clientID { get; set; }
        public Nullable<long> advisorID { get; set; }
        public Nullable<int> collegeStartMonth { get; set; }
        public Nullable<decimal> tutionFeeInflation { get; set; }
        public Nullable<int> collegeAge { get; set; }
        public Nullable<int> collageYears { get; set; }
        [Range(0, 100, ErrorMessage = "Please enter value of education parent pay rate between 0 and 100.")]
        public Nullable<decimal> parentPay { get; set; }
        public List<FavCollegesModel> FavColleges { get; set; }
        [Range(0, 100, ErrorMessage = "Please enter value of education rate of return between 0 and 100.")]
        public Nullable<decimal> rateA { get; set; }
        [Range(0, 100, ErrorMessage = "Please enter value of education rate of return between 0 and 100.")]
        public Nullable<decimal> rateB { get; set; }
        [Range(0, 100, ErrorMessage = "Please enter value of education rate of return between 0 and 100.")]
        public Nullable<decimal> rateC { get; set; }
    }
    public class DefaultsInsuranceModel
    {
        public long defaultID { get; set; }
        public Nullable<long> profileID { get; set; }
        public Nullable<long> clientID { get; set; }
        public Nullable<long> advisorID { get; set; }
        public Nullable<decimal> cost { get; set; }
        public Nullable<decimal> monthlyIncome { get; set; }
        public Nullable<int> yearAfterDeath { get; set; }
        public Nullable<int> specificnoofyears { get; set; }
        public bool retAsset { get; set; }
        public bool otherRetAsset { get; set; }
        public bool cashAccounts { get; set; }
        public bool spouseRetAsset { get; set; }
        public bool nonRetAsset { get; set; }
        [Range(0, 100, ErrorMessage = "Please enter value of insurance % between 0 and 100.")]
        public Nullable<decimal> inflation { get; set; }
    }
    public class DefaultsEmergencyFundModel
    {
        public bool isBarChart;
        public bool isSixMonth;

        public long defaultID { get; set; }
        public Nullable<long> profileID { get; set; }
        public Nullable<long> clientID { get; set; }
        public Nullable<long> advisorID { get; set; }
       // public Nullable<int> graphID { get; set; }
       // public Nullable<int> emergencyFundID { get; set; }

        public Nullable<decimal> emergencyFundValue { get; set; }
        [Range(0, 100, ErrorMessage = "Please enter value of emergency fund rate of return between 0 and 100.")]
        public Nullable<decimal> rateA { get; set; }
        [Range(0, 100, ErrorMessage = "Please enter value of emergency fund rate of return between 0 and 100.")]
        public Nullable<decimal> rateB { get; set; }
        [Range(0, 100, ErrorMessage = "Please enter value of emergency fund rate of return between 0 and 100.")]
        public Nullable<decimal> rateC { get; set; }
        public Nullable<bool> isPieChart { get; set; }
        public Nullable<bool> isThreeMonth { get; set; }
        [Range(0, 100, ErrorMessage = "Please enter value of emergency fund inflation % between 0 and 100.")]
        public Nullable<decimal> inflation { get; set; }
    }
    public class DefaultsDebtModel
    {
        public long defaultID { get; set; }
        public Nullable<long> profileID { get; set; }
        public Nullable<long> clientID { get; set; }
        public Nullable<long> advisorID { get; set; }
        public Nullable<decimal> minPay { get; set; }
        [Range(0, 100, ErrorMessage = "Please enter value of debt unpaid balance % between 0 and 100.")]
        public Nullable<decimal> unpaidPercentage { get; set; }
        [Range(0, 100, ErrorMessage = "Please enter value of debt inflation % between 0 and 100.")]
        public Nullable<decimal> inflation { get; set; }
    }
    public class DefaultIncomeProjectionModel
    {
        public long defaultID { get; set; }
        public Nullable<long> profileID { get; set; }
        public Nullable<long> clientID { get; set; }
        public Nullable<long> advisorID { get; set; }
        public Nullable<decimal> primaryInflation { get; set; }
        public Nullable<decimal> spouseInflation { get; set; }
        [Range(0, 100, ErrorMessage = "Please enter value of income inflation % between 0 and 100.")]
        public Nullable<decimal> inflation { get; set; }
    }
    public class DefaultsRetirementModel
    {
        public long RetirementDefaultID { get; set; }
        public Nullable<long> profileID { get; set; }
        public Nullable<long> clientID { get; set; }
        public Nullable<long> advisorID { get; set; }
        [Range(1, 150, ErrorMessage = "Please enter value of retirement age between 1 and 150.")]
        public Nullable<int> retireAge { get; set; }
        [Range(1, 150, ErrorMessage = "Please enter value of retirement life expectancy between 1 and 150.")]
        public Nullable<int> lifeExpectency { get; set; }
        [Range(0, 100, ErrorMessage = "Please enter value of retirement pre-return rate % between 0 and 100.")]
        public Nullable<decimal> preRetReturnTax { get; set; }
        [Range(0, 100, ErrorMessage = "Please enter value of retirement tax rate % between 0 and 100.")]
        public Nullable<decimal> retTax { get; set; }
        [Range(0, 100, ErrorMessage = "Please enter value of retirement return rate % between 0 and 100.")]
        public Nullable<decimal> retReturnTax { get; set; }
        public Nullable<decimal> monthIncome { get; set; }
        [Range(0, 100, ErrorMessage = "Please enter value of retirement inflation % between 0 and 100.")]
        public Nullable<decimal> inflation { get; set; }
    }
    public class DefaultsGlobalModel
    {
        public long defultID { get; set; }
        public Nullable<long> profileID { get; set; }
        public Nullable<long> clientID { get; set; }
        public Nullable<long> advisorID { get; set; }
        public Nullable<long> timeZone { get; set; }
        public Nullable<long> currency { get; set; }
        public string defaultstate { get; set; }
    
    }

    public class DesclaimerModel
    {
        public long desclaimerID { get; set; }
        public string desclaimerName { get; set; }
        public string desclaimerBody { get; set; }
    }

    public class CurrencyModel
    {
        public int id { get; set; }
        public string cID { get; set; }
        public string name { get; set; }
    }
    public class GlobalTimeZoneModel
    {
        public long timeZoneID { get; set; }
        public string location { get; set; }
        public string type { get; set; }
        public string time { get; set; }
        public string shortlocation { get; set; }
        public string text { get; set; }
    }

}
