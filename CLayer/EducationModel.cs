﻿using CLayer.common;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CLayer
{
    public class EducationModel
    {
        public Nullable<DateTime> reportDate { get; set; }
        public Nullable<long> userID { get; set; }
        public Nullable<long> profileID { get; set; }
        public List<EducationInfoModel> EducationInfoModel { get; set; }
        public Nullable<decimal> grandTotalCostToday { get; set; }
        public Nullable<decimal> grandTotalCostFuture { get; set; }
        public decimal goRate { get; set; }
        public decimal inflation { get; set; }
        public List<savingsRequired> savingsRequired { get; set; }
        public List<savingsRequiredGrantTotal> savingsRequiredGrandTotal { get; set; }
       
        public List<DefaultCollege> favColleges { get; set; }

        public EducationModel()
        {
            List<DefaultCollege> favColleges = new List<DefaultCollege>();
            List<savingsRequiredGrantTotal> savingsRequiredGrandTotal = new List<savingsRequiredGrantTotal>();
            List<savingsRequired> savingsRequired = new List<CLayer.savingsRequired>();
            List<EducationInfoModel> EducationInfoModel = new List<CLayer.EducationInfoModel>();
          
        }

    }

    public class EducationInfoModel 
    {
       // [Required(ErrorMessage = "Invalid Profile ID.")]
        public long profileID { get; set; }
        [Required(ErrorMessage = "Invalid Child ID.")]
        public Nullable<long> childID { get; set; }
        [Required(ErrorMessage = "Invalid Person ID.")]
        public Nullable<long> personID { get; set; }
        public string fName { get; set; }
        [Required(ErrorMessage = "Student type not passed.")]
        public Nullable<bool> IsParent { get; set; }
        [Required(ErrorMessage = " DOB not passed.")]
        public Nullable<System.DateTime> dob { get; set; }
        public Nullable<int> age { get; set; }

        public List<EducationDetailsModel> EducationDetails { get; set; }

        //[Required(ErrorMessage = "Start type is required.")]
        //public Nullable<int> startDateType { get; set; }
        //[Required(ErrorMessage = "College Start Date is required.")]
        //public string startDate { get; set; }
        //[Range(1, 50, ErrorMessage = "Year at school Value must be less than 150 ")]
        //[Required(ErrorMessage = "Year at school is required.")]
        //public Nullable<int> yearAtSchool { get; set; }
        //public string collegename { get; set; }
        //public Nullable<int> schoolID { get; set; }
        //[Required(ErrorMessage = "Total anual cost is required.")]
        //public Nullable<decimal> totalAnualCost { get; set; }
        //public Nullable<decimal> totalCurrentSavings { get; set; }
        //[Required(ErrorMessage = "parent pay rate is required.")]
        //public Nullable<decimal> parentPayRate { get; set; }
        //public Nullable<decimal> monthlySavings { get; set; }
        //public Nullable<bool> isActive { get; set; }
        //[Required(ErrorMessage = "college Start Age is required.")]
        //[Range(1, 100, ErrorMessage = "Please enter College start age between 1 and 100.")]
        //public Nullable<int> collegeStartAge { get; set; }
        //public Nullable<decimal> totalMonthlyContribution { get; set; }
        //public List<Education_DetailsModel> CurrentSavingsDetails { get; set; }
        //public Nullable<bool> isSavingsRequired { get; set; }

        //public EducationInfoModel()
        //{
        //    List<Education_DetailsModel> CurrentSavingsDetails = new List<Education_DetailsModel>();
        //}
    }
    public class EducationDetailsModel
    {
        public long educationID { get; set; }
        [Required(ErrorMessage = "Start type is required.")]
        public Nullable<int> startDateType { get; set; }
        [Required(ErrorMessage = "College Start Date is required.")]
        public string startDate { get; set; }
        [Range(1, 50, ErrorMessage = "Year at school Value must be less than 150 ")]
        [Required(ErrorMessage = "Year at school is required.")]
        public Nullable<int> yearAtSchool { get; set; }
        public string collegename { get; set; }
        public Nullable<int> schoolID { get; set; }
        [Required(ErrorMessage = "Please select student college")]
        public Nullable<decimal> totalAnualCost { get; set; }
        public Nullable<decimal> totalCurrentSavings { get; set; }
        [Required(ErrorMessage = "Parent pay rate is required.")]
        public Nullable<decimal> parentPayRate { get; set; }
        public Nullable<decimal> monthlySavings { get; set; }
        public Nullable<bool> isActive { get; set; }
        [Required(ErrorMessage = "college Start Age is required.")]
        [Range(1, 100, ErrorMessage = "Please enter education start age between 1 and 100.")]
        public Nullable<int> collegeStartAge { get; set; }
        public Nullable<decimal> totalMonthlyContribution { get; set; }
        public List<Education_DetailsModel> CurrentSavingsDetails { get; set; }
        public Nullable<bool> isSavingsRequired { get; set; }
        public DateTime educationCreatedDate { get; set; }
        
        public EducationDetailsModel()
        {
            List<Education_DetailsModel> CurrentSavingsDetails = new List<Education_DetailsModel>();
        }
    }
    public class savingsRequired 
    {

        public string name { get; set; }

       
        public List<savingsRequiredDetails> savingsRequiredDetails { get; set; }
        public savingsRequired()
        {
            List<savingsRequiredDetails> savingsRequiredDetails = new List<CLayer.savingsRequiredDetails>();

        }


    }
    public class savingsRequiredDetails 
    {

        public string collegename { get; set; }
        public Nullable<decimal> totalCostToday { get; set; }
        public Nullable<decimal> totalCostFuture { get; set; }
        public Nullable<int> timeToStartYear { get; set; }
        public Nullable<int> timeToStartMonth { get; set; }
        [Range(0, 12, ErrorMessage = "Return interest rate must be between 0 and 12 ")]
        public Nullable<decimal> returnRate { get; set; } 
        public Nullable<decimal> currentSavings { get; set; }
        public string lumpSum { get; set; }
        public string additionalMonthlySavings { get; set; }


    }
    public class savingsRequiredGrantTotal 
    {
        public Nullable<decimal> returnRate { get; set; }
        public Nullable<decimal> lumpSum { get; set; }
        public Nullable<decimal> additionalMonthlySavings { get; set; }
    }
    public  class Education_DetailsModel
    {
        public long detailID { get; set; }
        public long educationID { get; set; }
        [Required(ErrorMessage = "Please select any savings type.")]
        [Range(1, 100000, ErrorMessage = "Please select any savings type.")]
        public Nullable<long> educationTypeID { get; set; }
        public string company { get; set; }
        [Required(ErrorMessage = "Please enter any amount.")]
        public Nullable<decimal> amount { get; set; }
        public string cID { get; set; }
        public Nullable<System.DateTime> amountValueAsOf { get; set; }
        public Nullable<decimal> monthly { get; set; }
        public string notes { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<System.DateTime> detailsCreatedDate { get; set; }
        public Nullable<System.DateTime> detailsUpdatedDate { get; set; }
        public Nullable<long> detailsModifiedBy { get; set; }
    }
    public class CollegeModel

    {
        public long collegeID { get; set; }
        public string collegeName { get; set; }
        public string collegeState { get; set; }
        public Nullable<decimal> outState { get; set; }
        public Nullable<decimal> inState { get; set; }
        public string Location { get; set; }
        public decimal fees { get; set; }
        public decimal inStateTutionFees { get; set; }
        public decimal outStateTutionFees { get; set; }

        public decimal roomBoardFees { get; set; }
        // public decimal roomFees { get; set; }
        //public string finYear { get; set; }
        //public string city { get; set; }
        //public string zip { get; set; }
    }
    public class CollegeSearch : Success
    {
  public string name { get; set; }
    }




}
