﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer.DBModel
{
    public partial class Advisor
    {
        public DBModel.AdvisorBranch AdvisorBranch { get; set; }
        public DBModel.AdvisorContact AdvisorContact { get; set; }
        public DBModel.DefaultsGlobal DefaultsGlobal { get; set; }
        public DBModel.DefaultsRetirement DefaultsRetirement { get; set; }
        public DBModel.DefaultIncomeProjection DefaultIncomeProjection { get; set; }
        public DBModel.DefaultsDebt DefaultsDebt { get; set; }
        public DBModel.DefaultsEmergencyFund DefaultsEmergencyFund { get; set; }
        public DBModel.DefaultsInsurance DefaultsInsurance { get; set; }
        public DBModel.DefaultsEducation DefaultsEducation { get; set; }


        //public Advisor()
        //{
        //    DefaultsDebt = new DefaultsDebt();

        //}

    }
}
