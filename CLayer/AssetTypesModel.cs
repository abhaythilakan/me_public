﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CLayer
{
    public class AssetTypesModel
    {
        public int assetTypeID { get; set; }
        [Range(1, Int32.MaxValue, ErrorMessage = "Please select Category")]
        [Required(ErrorMessage = "Please select Category")]
        [Display(Name = "Category")]
        public Nullable<int> assetCatID { get; set; }
        [Required(ErrorMessage = "Please enter Type Name")]
        [Display(Name = "Name")]
        public string typeName { get; set; }
        [Display(Name = "Taxable")]
        public bool isTaxed { get; set; }
        [Display(Name = "Group")]
        public Nullable<bool> isGroup { get; set; }
        [Display(Name = "Order")]
        public Nullable<int> sortOrder { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<System.DateTime> updatedDate { get; set; }
        public Nullable<long> updatedBy { get; set; }

        [Display(Name = "Category")]
        public string assetCatName { get; set; }
        //public List<common.DropDownList_Int> AssetCatDDL { get; set; }

        //public AssetTypesModel()
        //{
        //    List<common.DropDownList_Int> ddl1 = new List<common.DropDownList_Int>();
        //    ddl1 = common.DDLReg.getDropDownList_Int(common.enumDropDownList.AssetCategory);
        //    AssetCatDDL = ddl1;
        //}
    }
}
