﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class ContactModel
    {
       
        public Nullable< long> contactId { get; set; }
        [Required(ErrorMessage = "Name Cannot be Empty")]
        [StringLength(25, ErrorMessage = "Name length should be less than 25")]
        public string name { get; set; }
        [Required(ErrorMessage = "Email Cannot be Empty")]
        [EmailAddress(ErrorMessage = "Email address is invalid")]
        [StringLength(30, ErrorMessage = "Email address length should be less than 30")]
        [Display(Name = "Email Address")]
        public string email { get; set; }
        [StringLength(10, ErrorMessage = "Phone number length should be less than 10")]
        public string phone { get; set; }
        public string stateCode { get; set; }
        public string company { get; set; }
        public Nullable<int> findUs { get; set; }
        public string subject { get; set; }
        public string comments { get; set; }
        public string toEmail { get; set; }
        public Nullable<int> readStatus { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
    }
}
