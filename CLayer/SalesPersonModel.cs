﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class SalesPersonModel
    {
        public int sp_Id { get; set; }
        [Display(Name ="First Name")]
        [Required(ErrorMessage ="First Name is required")]
        public string sp_FName { get; set; }
        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last Name is required")]
        public string sp_LName { get; set; }
        [Display(Name = "User Name")]
        [Required(ErrorMessage = "User Name is required")]
        public string sp_Username { get; set; }
        public string sp_Password { get; set; }
        public string sp_Email { get; set; }
        public Nullable<bool> sp_isActive { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<System.DateTime> modifyDate { get; set; }
        public Nullable<long> modifyBy { get; set; }
    }
}
