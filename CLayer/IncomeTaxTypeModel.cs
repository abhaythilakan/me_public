﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class IncomeTaxTypeModel
    {
        public long taxTypeID { get; set; }
        [Required(ErrorMessage ="Invalid Tax Category")]
        [Range(1, int.MaxValue, ErrorMessage = "Invalid Tax Category")]
        public int tc_Id { get; set; }
        [Display(Name ="Name")]
        [Required(ErrorMessage = "Name is required")]
        public string taxTypeName { get; set; }
        [Display(Name = "Order")]
        public Nullable<int> sortOrder { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<System.DateTime> updatedDate { get; set; }
        public Nullable<long> updatedBy { get; set; }
    }
}
