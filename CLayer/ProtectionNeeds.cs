﻿using CLayer.common;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CLayer
{
    public class ProtectionNeeds : Success
    {
        public Nullable<long> profileID { get; set; }
        public Nullable<decimal> inflationRate { get; set; }
        public Nullable<decimal> returnRate { get; set; }
        public List<protectionNeedModel> protectionNeedModel { get; set; }
        public List<protectionPlan> protectionPlan { get; set; }
        public List<Statement> Statement { get; set; }
        public EducationModel em { get; set; }
        public List<Asset_RetirementModel> ar { get; set; }
        public AssetModel astModel { get; set; }
    }
    public class protectionNeedModel
    {
        public Nullable<long> personID { get; set; }
        public string personName { get; set; }
        public Nullable<bool> isPrimary { get; set; }
        public Nullable<decimal> totalDebtAndDeath { get; set; }
        public Nullable<bool> debtIsInclude { get; set; }
        public Nullable<decimal> incomeReplacement { get; set; }
        public Nullable<decimal> totalMortgage { get; set; }
        public Nullable<bool> mortgageIsInclude { get; set; }
        public Nullable<decimal> totalEducationCost { get; set; }
        public Nullable<bool> eduIsInclude { get; set; }
        public DebtAndDeath DebtAndDeath { get; set; }
        public List<Mortgage> Mortgage { get; set; }
        public List<EducationNeed> EducationNeed { get; set; }
        public Nullable<decimal> totalProtectionNeed { get; set; }
    }
    public class protectionPlan
    {
        public string personName { get; set; }
        public Nullable<decimal> totalGroup { get; set; }
        public List<Group> Group { get; set; }
        public Nullable<decimal> totalIndividual { get; set; }
        public List<Individual> Individual { get; set; }
        public Nullable<decimal> totalAssets { get; set; }
        public List<Assets> Assets { get; set; }
        public Nullable<decimal> totalProtectionPlan { get; set; }
        public Nullable<decimal> Shortfall_Surplus { get; set; }
    }

    public class DebtAndDeath
    {
        public string finalExpenseName { get; set; }
        public Nullable<decimal> finalExpenseValue { get; set; }
        public List<DebtDetailsModel> DebtDetails { get; set; }
    }

    public class Mortgage
    {
        public string mortgageName { get; set; }
        public Nullable<decimal> mortgageValue { get; set; }
    }
    public class DebtDetailsModel
    {
        public string debtName { get; set; }
        public Nullable<decimal> debtValue { get; set; }
    }
    public class EducationNeed
    {
        public string name { get; set; }
        public List<EducationNeedDetailsModel> EducationNeedDetailsModel { get; set;}
    }
    public class EducationNeedDetailsModel
    {
        public string collegeName { get; set; }
        public string eduValue { get; set; }
    }

    public class Group
    {
        public string groupName { get; set; }
        public Nullable<decimal> groupValue { get; set; }
    }
    public class Individual
    {
        public string individualName { get; set; }
        public Nullable<decimal> individualValue { get; set; }
    }
    public class Assets
    {
        public string assetName { get; set; }
        public Nullable<decimal> assetValue { get; set; }
    }
    public class Statement
    {
        public string statement { get; set; }
        //public string primaryName { get; set; }
        //public string spouseName { get; set; }
        //public Nullable<decimal> monthlyNeed { get; set; }
        //public Nullable<int> years { get; set; }
        //public Nullable<decimal> inflationRate { get; set; }
    }
}



