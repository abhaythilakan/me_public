﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class RetirementModel
    {

        public Nullable<decimal> currentNeed { get; set; }
        [Range(0, 50, ErrorMessage = "Please enter Retirement Tax Rate % between 0 and 50.")]
        public Nullable<decimal> retTaxRate { get; set; }
        [Range(0, 12, ErrorMessage = "Please enter Retirement Return % between 0 and 12.")]
        public Nullable<decimal> retReturnTax { get; set; }
        [Range(0, 12, ErrorMessage = "Please enter inflation % between 0 and 12.")]
        public Nullable<decimal> inflation { get; set; }
        [Range(0, 12, ErrorMessage = "Please enter Pre-Retirement Return % between 0 and 12.")]
        public Nullable<decimal> preRetTax { get; set; }
        public Nullable<DateTime> reportDate { get; set; }
        public List<PersonalInfo> PersonInfoDetails { get; set; }
        public Nullable<decimal> rateA { get; set; }
        public Nullable<decimal> rateB { get; set; }
        public Nullable<decimal> rateC { get; set; }

        public List<currentRetSummary> currentretSummary { get; set; }
        public RetirementGoal RetirementGoal { get; set; }
        public List<whatif> whatif { get; set; }
        public currentRetSummaryTotal currentretSummaryTotal { get; set; }
        public otherValuesModel otherValuesModel { get; set; }
        public List<RetirementledgerModel> ledger { get; set; }
        public Nullable<long> profileID { get; set; }



    }
    public class otherValuesModel
    {
        public Nullable<int> startdate { get; set; }
        public Nullable<int> enddate { get; set; }
        public Nullable<int> count { get; set; }
        public string pName { get; set; }
        public string sName { get; set; }
        public double pCurrentBalance { get; set; }
        public double pCurrentContribution { get; set; }
        public double pCurrentBalnceNotInterest { get; set; }
        public double pCurrentContributionNotInterest { get; set; }

        public double sCurrentBalance { get; set; }
        public double sCurrentContribution { get; set; }
        public double sCurrentBalnceNotInterest { get; set; }
        public double sCurrentContributionNotInterest { get; set; }

        public double oCurrentBalance { get; set; }
        public double oCurrentContribution { get; set; }
        public double oCurrentBalnceNotInterest { get; set; }
        public double oCurrentContributionNotInterest { get; set; }

        public double wCurrentBalance { get; set; }
        public double wCurrentContribution { get; set; }
        public double wCurrentBalnceNotInterest { get; set; }
        public double wCurrentContributionNotInterest { get; set; }

    }
    public class RetirementGoal
    {
        public Nullable<decimal> monthlyNeedFuture { get; set; }
        public Nullable<decimal> annualNeedFuture { get; set; }
        public Nullable<decimal> FIN { get; set; }
        public List<RetirementGrow> RetirementGrowDetails { get; set; }
        public List<AdditionalNeed> AdditionalNeedDetails { get; set; }
    }
    public class RetirementledgerModel
    {
        public Nullable<int> year { get; set; }
        public Nullable<int> pAge { get; set; }
        public Nullable<int> sAge { get; set; }
        public Nullable<double> balance { get; set; }
        public Nullable<double> growth { get; set; }
        public Nullable<double> annualContribution { get; set; }
        public Nullable<double> annualRetNeed { get; set; }
        public Nullable<double> otherIncomeafterRet { get; set; }
        public Nullable<double> taxesonOtherIncomeafterRet { get; set; }
        public Nullable<double> withdrawal { get; set; }
        public Nullable<double> taxesOnWithdrawal { get; set; }
        public Nullable<double> endingBalance { get; set; }
    }
    public class PersonalInfo
    {
        public int age { get; set; }

        public int untilRetirement { get; set; }
        [Required(ErrorMessage ="Retirement age required.")]
        public int retAge { get; set; }
        public int inRetirement { get; set; }
        public long personID { get; set; }
        public DateTime dob { get; set; }
        public int lifeExpectancy { get; set; }
        public string name { get; set; }
        public bool isprimary { get; set; }

    }
    public class currentRetSummary
    {

        public string name { get; set; }
        public Nullable<long> personID { get; set; }
        public Nullable<decimal> totalplanbalance { get; set; }
        public Nullable<decimal> totalpersonalContribution { get; set; }
        public Nullable<decimal> totalempContribution { get; set; }
        public List<currentRetSummaryDetails> currentRetSummaryDetails { get; set; }

    }
    public class currentRetSummaryTotal
    {
        public Nullable<decimal> totalplanbalance { get; set; }
        public Nullable<decimal> totalpersonalContribution { get; set; }
        public Nullable<decimal> totalempContribution { get; set; }

    }
    public class whatif
    {
        [Range(0, Int64.MaxValue, ErrorMessage = "Please enter valid what if plan balance.")]
        public Nullable<decimal> whatifplanbalance { get; set; }
        [Range(0, Int64.MaxValue, ErrorMessage = "Please enter valid what if personal contribution balance.")]
        public Nullable<decimal> whatifpersonalContribution { get; set; }
        [Range(0, Int64.MaxValue, ErrorMessage = "Please enter valid what if monthly contribution balance.")]
        public Nullable<decimal> whatifmonthlyContribution { get; set; }
        public Nullable<bool> isTaxed { get; set; }

    }
   
    public class currentRetSummaryDetails
    {
        public string name { get; set; }
        public Nullable<decimal> planbalance { get; set; }
        public Nullable<decimal> personalContribution { get; set; }
        public Nullable<decimal> empContribution { get; set; }
        public Nullable<bool> istaxed { get; set; }

    }
    public class RetirementGrow
    {
        [Range(0, 12, ErrorMessage = "Please enter Retirement grow Go Rate % between 0 and 12.")]
        public Nullable<decimal> rate { get; set; }
        public Nullable<decimal> amount { get; set; }
        public Nullable<decimal> shortfallsurplus { get; set; }


    }
    public class AdditionalNeed
    {
        public Nullable<decimal> rate { get; set; }
        public Nullable<decimal> lumpsum { get; set; }
        public string lumpsumstring { get; set; }
        public Nullable<decimal> monthly { get; set; }
        public string monthlyString { get; set; }

    }
}
