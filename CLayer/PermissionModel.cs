﻿using CLayer.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class PermissionModel// : Success
    {
        public Nullable<long> perId { get; set; }
        public Nullable<long> advisorId { get; set; }
        public Nullable<long> clientId { get; set; }
        public Nullable<long> profileId { get; set; }
        public Nullable<bool> isRead { get; set; }
        public Nullable<bool> isEdit { get; set; }
        public Nullable<int> pageId { get; set; }
        public string pageName { get; set; }
        public Nullable<int> reportTypeID { get; set; }
    }
}
