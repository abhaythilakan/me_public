﻿using CLayer.common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CLayer
{

    public class ActionPlanModel
    {
        public actionPointModel actionPointModel { get; set; }
        public List<actionList> actionList { get; set; }
    }
    public class actionPointModel
    {
        public Nullable<long> actionID { get; set; }
        [Required(ErrorMessage = "Profile ID is required")]
        public Nullable<long> profileID { get; set; }
        public Nullable<long> actionNameID { get; set; }
        [Required(ErrorMessage = "Action Name is required")]
        public string actionName { get; set; }
        [Required(ErrorMessage = "Please select due date")]
        public Nullable<System.DateTime> dueDate { get; set; }
        [Required(ErrorMessage = "Please select assigned to field")]
        [Display(Name = "Assigned To")]
        public Nullable<int> assignedTo { get; set; }
        [Required(ErrorMessage = "Please select desired notification type")]
        [Display(Name = "Notification Type")]
        public Nullable<int> notificationTypeID { get; set; }
        public string comment { get; set; }
        public Nullable<System.DateTime> actionCreatedDate { get; set; }
        public Nullable<System.DateTime> actionUpdatedDate { get; set; }
        public Nullable<long> actionModifiedBy { get; set; }
        public Nullable<int> actionStatus { get; set; }
        public Nullable<bool> isResheduled { get; set; }
        public otherActions otherActions { get; set; }
        public string advisorEmail { get; set; }
        public string primaryEmail { get; set; }
        public string spouseEmail { get; set; }
        public string fromUser { get; set; }
    }
    public class otherActions
    {
    [Required(ErrorMessage = "Other Person ID is required")]
    public Nullable<int> otherID { get; set; }
    public string otherPersonName {get; set;}
    [Required(ErrorMessage = "Other Email ID is required")]
    public string otherEmailID { get; set; }
    }
    public class actionList
    {
        public Nullable<long> actionID { get; set; }
        public string actionPoint { get; set; }
        public string dueDate { get; set; }
        public string comment { get; set; }
        public int actionStatus { get; set; }
    }
    public class preDefinedActionModel
    {
        public Nullable<long> actionID { get; set; }
        public string actionName { get; set; }
    }

}
