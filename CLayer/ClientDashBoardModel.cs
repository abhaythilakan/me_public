﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class ClientDashBoardModel
    {
       
    }
    public class Income_ExpenseModel
    {
        public Nullable<decimal> totalIncome { get; set; }
        public Nullable<decimal> totalExpense { get; set; }
        public string status { get; set; }
        public Nullable<decimal> statusValue { get; set; }
        public List<IncomeList> incomeList { get; set; }
        public List<expenseList> expenseList { get; set; }
    }
    public class IncomeList
    {
        public string incomeName { get; set; }
        public Nullable<decimal> grossAmount { get; set; }
    }
    public class expenseList
    {
        public string expenseName { get; set; }
        public Nullable<decimal> amount { get; set; }
    }


    public class InvestmentTaxableModel
    {
       public List<TaxableDetailsModel> taxdefferedafterTax { get; set; }
        public decimal taxdefferedafterTaxTotal { get; set; }
        public List<TaxableDetailsModel> taxfreeafterTax { get; set; }
        public decimal taxfreeafterTaxTotal { get; set; }
        public List<TaxableDetailsModel> taxableafterTax { get; set; }
        public decimal taxableafterTaxTotal { get; set; }
        public List<TaxableDetailsModel> taxdefferedbeforeTax { get; set; }
        public decimal taxdefferedbeforeTaxTotal { get; set; }
    }
    public class TaxableDetailsModel
    {
        public string name { get; set; }
        public decimal amount { get; set; }
        public int taxDefID { get; set; }
    }

    public class Education_Model
    {
        public EducationModel em { get; set; }
        public Nullable<decimal> totalEducationNeed { get; set; }
        public Nullable<decimal> totalCurrentSavings { get; set; }
        public string status { get; set; }
        public Nullable<decimal> statusValue { get; set; }
        public List<EducationInfo_Model> educationInfo { get; set; }
    }
    public class EducationInfo_Model
    {
        public string fname { get; set; }
        public Nullable<decimal> totalEdu_Need { get; set; }
        public List<EducationDetails_Model> educationDetails { get; set; }
        public Nullable<decimal> totalSavings { get; set; }
        public List<CurrentSavingsDetail_Model> educationSavings { get; set; }
    }
    public class EducationDetails_Model
    {
        public string collegename { get; set; }
        public Nullable<decimal> annualCost { get; set; }
    }
    public class CurrentSavingsDetail_Model
    {
        public string companyName { get; set; }
        public Nullable<decimal> savingsAmount { get; set; }
    }
    public class LiabilitiesModel
    {
        public string name { get; set; }
        public Nullable<decimal> amount { get; set; }
        public Nullable<decimal> percentage { get; set; }
    }
    public class TotalNetworthModel
    {
        public string name { get; set; }
        public Nullable<decimal> amount { get; set; }
        public Nullable<decimal> percentage { get; set; }
        public List<TotalNetworthModelDetails> TotalNetworthModelDetails { get; set; }
    }
    public class TotalNetworthModelDetails
    {
        public string name { get; set; }
        public Nullable<decimal> amount { get; set; }
        

    }


}
