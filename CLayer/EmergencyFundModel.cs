﻿using CLayer.common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace CLayer
{
    public class EmergencyFundModel : Success
    {
        [Required(ErrorMessage = "Profile ID required")]
        public Nullable<long> profileID { get; set; }
        [Required(ErrorMessage = "Target Amount required")]
        public Nullable<decimal> targetAmount { get; set; }
        public Nullable<decimal> currentFund { get; set; }
        public List<CurrentFundDetails> details { get; set; }
        public string status { get; set; }
        public string statusValue { get; set; }
        [Required(ErrorMessage = "Rate of Return required")]
        public Nullable<decimal> returnRate { get; set; }
        [Required(ErrorMessage = "No of Years required")]
        //[RegularExpression("[0-9]*", ErrorMessage = "Must be integer.")]
        [RegularExpression(@"[0-9]*", ErrorMessage = "Must be integer.")]
        [Display(Name = "Year")]
        public Nullable<int> years { get; set; }
        public string monthlySavings { get; set; }

        // public List<DBModel.EmergencyFund_Types> eTypes { get; set; }

    }
    public class CurrentFundDetails
    {
        public string assetCatg { get; set; }
        public List<assetType> assetType { get; set; }
    }
    public class assetType
    {
        public string name { get; set; }
        public Nullable<decimal> value { get; set; }
    }

    public class EmergencyFundTypes : Success
    {

        public List<DBModel.EmergencyFund_Types> eTypes { get; set; }

    }
}