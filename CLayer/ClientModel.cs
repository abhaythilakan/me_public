﻿using CLayer.common;
using CLayer.DBModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CLayer
{
    public class ClientModel //: Success
    {
        public long clientId { get; set; }

        [Required(ErrorMessage = "First name is required")]
        [StringLength(25, ErrorMessage = "First name length should be less than 25")]
        [Display(Name = "First Name")]
        public string firstName { get; set; }

        [Required(ErrorMessage = "Last name is required")]
        [StringLength(25, ErrorMessage = "Last name length should be less than 25")]
        [Display(Name = "Last Name")]
        public string lastName { get; set; }

        [Required(ErrorMessage = "Invalid Advisor")]
        [Display(Name = "Advisor")]
        [Range(1, long.MaxValue, ErrorMessage = "Invalid Advisor")]
        public Nullable<long> advisorId { get; set; }

        public Nullable<System.DateTime> createdDate { get; set; }

        public string imageString { get; set; }
        public string imagePath { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [StringLength(20, ErrorMessage = "Password charector length should be less that 20")]
        [Display(Name = "Password")]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*[0-9])(?=(?:.*?[!@#$%\^&*\(\)\-_+=;:'""\/\[\]{},.<>|`]){2}).{8,20}$", ErrorMessage = "Password must contain at least two special characters, Password must be at least eight characters long, Password must be alpha numeric")]
        public string password { get; set; } //Password must contain at least two special characters, Password must be at least eight characters long, Password must be alpha numeric

        //[EmailAddress(ErrorMessage = "Email address is invalid")]
        //[StringLength(30, ErrorMessage = "Email address length should be less than 30")]
        //[Display(Name = "Email Address")]
        //public string email { get; set; }

        public long userId { get; set; }

        [Required(ErrorMessage = "User Name is required")]
        [StringLength(20, ErrorMessage = "Username length should be less than 20")]
        [Display(Name = "User Name")]
        [Remote("IsUsernameExist", "api/Login", AdditionalFields = "userId", ErrorMessage = "User Name is already exist.", HttpMethod = "GET")]
        public string userName { get; set; }

        [Display(Name = "Report Type")]
        // [Required(ErrorMessage = "Report type is required")]
        public Nullable<int> reportType { get; set; }
        public Nullable<bool> isActive { get; set; }


        #region Additional Properties
        public string advisorFirstName { get; set; }
        public string advisorLastName { get; set; }

        [EmailAddress(ErrorMessage = "Email address is invalid")]
        [StringLength(30, ErrorMessage = "Email address length should be less than 30")]
        [Display(Name = "Email Address")]
        public string emailPrimary { get; set; }
        public DateTime clientDOB { get; set; }
        public DateTime clientDOBString { get; set; }
        public string clientGender { get; set; }
        public Nullable<int> TotalCount { get; set; }
        public Nullable<DateTime> modifiedDate { get; set; }
        

        public List<DropDownList_Int> advisorDDL { get; set; }
        public List<DropDownList_Int> reportTypeDDL { get; set; }
        public List<DropDownList_String> stateDDL { get; set; }
        public List<DropDownList_Int> currencyDDL { get; set; }
        #endregion

        //public ClientModel()
        //{
        //    List<DropDownList_String> ddl1 = common.DDLReg.getDropDownList(enumDropDownList.Advisor);
        //    advisorDDL = ddl1;
        //    List<DropDownList_String> ddl2 = common.DDLReg.getDropDownList(enumDropDownList.ReportType);
        //    reportTypeDDL = ddl2;
        //    List<DropDownList_String> ddl3 = common.DDLReg.getDropDownList(enumDropDownList.State);
        //    stateDDL = ddl3;
        //    List<DropDownList_String> ddl4 = common.DDLReg.getDropDownList(enumDropDownList.Currencies);
        //    currencyDDL = ddl4;
        //}
    }

    public class ClientShow //: Success
    {
        [Required(ErrorMessage = "Invalid Client")]
        [Display(Name = "Client")]
        [Range(1, long.MaxValue, ErrorMessage = "Invalid Client")]
        public long clientId { get; set; }
        public long profileId { get; set; }
    }

    public class ClientListArgModel //: Success
    {
        public long clientID { get; set; }
        [Required(ErrorMessage = "Invalid Advisor")]
        [Display(Name = "Advisor")]
        [Range(1, long.MaxValue, ErrorMessage = "Invalid Advisor")]
        public long advisorId { get; set; }
        public int TotalCount { get; set; }
        public string SearchText { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int SortColumn { get; set; }
        public int SortOrder { get; set; }
    }

    public class ClientListPaginated
    {
        public ClientListArgModel Collection { get; set; }
        public List<ClientModel> clients { get; set; }
    }



    public class ClientCollection // : Success
    {
        public UserCat usrCat { get; set; }
        public ClientModel client { get; set; }
        public ProfileModel profile { get; set; }
        public DefaultsGlobalModel global { get; set; }
        public DefaultsRetirementModel ret { get; set; }
        public DefaultIncomeProjectionModel Income { get; set; }
        public DefaultsDebtModel debt { get; set; }
        public DefaultsEmergencyFundModel emerg { get; set; }
        public DefaultsInsuranceModel insu { get; set; }
        public DefaultsEducationModel edu { get; set; }
        public List<PermissionModel> permissionList { get; set; }
    }
    public class ClientCollection1 // : Success
    {
        public UserCat usrCat { get; set; }
        public ClientModel1 client { get; set; }
        //public ProfileModel profile { get; set; }
        public DefaultsGlobalModel global { get; set; }
        public DefaultsRetirementModel ret { get; set; }
        public DefaultIncomeProjectionModel Income { get; set; }
        public DefaultsDebtModel debt { get; set; }
        public DefaultsEmergencyFundModel emerg { get; set; }
        public DefaultsInsuranceModel insu { get; set; }
        public DefaultsEducationModel edu { get; set; }
        public List<PermissionModel> permissionList { get; set; }
    }

    public class ClientModel1 //: Success
    {
        public long clientId { get; set; }
    }

        public class UserCat
    {
        public int usrCat { get; set; }
    }

}
