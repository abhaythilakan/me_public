﻿using CLayer.common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CLayer
{
    public class FAQModel
    {
        public long faq_Id { get; set; }
        [Display(Name = "Question")]
        [Required(ErrorMessage = "Question is required")]
        public string faq_Que { get; set; }
        [Display(Name = "Answer")]
        [Required(ErrorMessage = "Answer is required")]
        [AllowHtml]
        public string faq_Ans { get; set; }
        [Display(Name = "Section")]
        [Required(ErrorMessage = "Section is required")]
        [Range(1, int.MaxValue, ErrorMessage = "Section is required")]
        public Nullable<int> faq_SectionID { get; set; }
        [Display(Name = "Section")]
        public string faq_SectionName { get; set; }
        public Nullable<System.DateTime> createdDT { get; set; }
        public Nullable<System.DateTime> modifyDT { get; set; }
        public Nullable<System.DateTime> modifyBy { get; set; }
        public Nullable<bool> isActive { get; set; }

        public List<DropDownList_Int> FAQsections { get; set; }

        public FAQModel()
        {
            FAQsections = common.DDLReg.getDropDownList_Int(common.enumDropDownList.FAQs).ToList();
        }
    }
}
