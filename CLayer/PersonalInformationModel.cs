﻿using CLayer.common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class PersonalInformationModel 
    {
       // [Required(ErrorMessage = "Profile Missing")]
        public long profileID { get; set; }
        public PersonModel primaryInfo { get; set; }
        public Person_ContactInfoModel primaryContactInfo { get; set; }
        public Person_EmployerInfoModel primaryEmployementInfo { get; set; }
        public PersonModel spouseInfo { get; set; }
        public Person_EmployerInfoModel spouseEmployementInfo { get; set; }
        public List<ChildrenInfoModel> childrenInfo { get; set; }
       
    }

    public class PersonModel 
    {
        public long personID { get; set; }
        public long profileID { get; set; }
        [Required(ErrorMessage = "First name is required")]
        [StringLength(25, ErrorMessage = "First name length should be less than 25")]
        [Display(Name = "First Name")]
        public string firstName { get; set; }
        public string midName { get; set; }
        [Required(ErrorMessage = "Last name is required")]
        [StringLength(25, ErrorMessage = "Last name length should be less than 25")]
        [Display(Name = "last Name")]
        public string lastName { get; set; }
       // [Required(ErrorMessage = "Please choose gender")]
        public Nullable<bool> isMale { get; set; }
        public Nullable<bool> isPrimary { get; set; }
        [Required(ErrorMessage = "Please select date of birth")]
        public Nullable<System.DateTime> birthDate { get; set; }
        [Required(ErrorMessage = "Please Enter Retirement Age")]
        [Range(1, 150,ErrorMessage = "Retirement Age Value must be less than 150 ")]
        public Nullable<int> retirementAge { get; set; }
        [Range(1, 150,ErrorMessage = "life Expectancy Value must be less than 150")]
        [Required(ErrorMessage = "please Enter Life Expectancy")]
        public Nullable<int> lifeExpectancy { get; set; }      
        public Nullable<decimal> Age { get; set; }
        [Required(ErrorMessage = "Email ID required")]
        [EmailAddress(ErrorMessage = "Email address is invalid")]
        public string emailID { get; set; }
        [Required(ErrorMessage = "please Choose Education include")]
        public Nullable<bool> inclEducPage { get; set; }
        public Nullable<System.DateTime> personCreatedDate { get; set; }
        public Nullable<System.DateTime> personUpdatedDate { get; set; }
        public Nullable<long> personModifiedBy { get; set; }
        public Nullable<bool> isActive { get; set; }
    }
    public class Person_ContactInfoModel 
    {
        public long personInfoID { get; set; }
        public long profileID { get; set; }
        [StringLength(25, ErrorMessage = "Phone number length should be less than 10")]
        [Required(ErrorMessage = "")]
        public string homePhone { get; set; }
        [StringLength(25, ErrorMessage = "Phone number length should be less than 10")]

        public string mobilePhone { get; set; }
        [StringLength(50, ErrorMessage = "Address Line length should be less than 50")]
        [Required(ErrorMessage = "please entrer Address")]
        public string addressL1 { get; set; }
        [StringLength(50, ErrorMessage = "Address Line length should be less than 50")]
        public string addressL2 { get; set; }
        [StringLength(20, ErrorMessage = "City name length should be less than 20")]
        [Required(ErrorMessage = "Please entrer city")]
        public string city { get; set; }
        [Required(ErrorMessage = "Please select state")]
        [StringLength(4, ErrorMessage = "Please select a person contact state")]
        public string state { get; set; }
        //[StringLength(6, MinimumLength = 5,ErrorMessage ="Please check zip code")]
        [RegularExpression("(^\\d{5}(-\\d{4})?$)|(^[ABCEGHJKLMNPRSTVXY]{1}\\d{1}[A-Z]{1} *\\d{1}[A-Z]{1}\\d{1}$)", ErrorMessage = "Person contact zip code is invalid.")]
        [Required(ErrorMessage = "please enter Zip Code")]
        public string zip { get; set; }
        public Nullable<System.DateTime> contactCreatedDate { get; set; }
        public Nullable<System.DateTime> contactUpdatedDate { get; set; }
        public Nullable<long> contactModifiedBy { get; set; }
    }
    public class Person_EmployerInfoModel
    {
        public Nullable<long> empID { get; set; }
        public Nullable<long> profileID { get; set; }
        public string ocuupation { get; set; }
        public string employer { get; set; }
        [Range(0, 100,ErrorMessage = "Year in occupation must be less than 100.")]
        public Nullable<int> yearInocuupation { get; set; }
        [Range(0, 100, ErrorMessage = "Year at employer must be less than 100.")]
        public Nullable<int> yearAtEmployer { get; set; }
        //[Required(ErrorMessage = "Please entrer primary employer address.")]
        public string addressL1 { get; set; }
        public string addressL2 { get; set; }
        [StringLength(20, ErrorMessage = "City name length should be less than 20.")]

        //[Required(ErrorMessage = "Please enter primary employer city.")]
        public string city { get; set; }
        [Required(ErrorMessage = "Please select primary employer state")]
        //[StringLength(0, MinimumLength = 4, ErrorMessage = "Please select a person employer state.")]
        public string state { get; set; }
        //[StringLength(10, MinimumLength = 5, ErrorMessage = "Primary employer zipcode not valid.")]
        [RegularExpression("(^\\d{5}(-\\d{4})?$)|(^[ABCEGHJKLMNPRSTVXY]{1}\\d{1}[A-Z]{1} *\\d{1}[A-Z]{1}\\d{1}$)", ErrorMessage = "Primary employer zip code is invalid.")]
        [Required(ErrorMessage = "Please enter primary employer zipode")]
        public string zip { get; set; }
        public Nullable<bool> isPrimary { get; set; }
        public Nullable<System.DateTime> employerCreatedDate { get; set; }
        public Nullable<System.DateTime> employerUpdatedDate { get; set; }
        public Nullable<long> employerModifiedBy { get; set; }
    }
    public class ChildrenInfoModel
    {
        public Nullable<long> childID { get; set; }
        public Nullable<long> profileID { get; set; }
        [Required(ErrorMessage = "Child first name required")]
        public string fName { get; set; }
        public string midName { get; set; }
        [Required(ErrorMessage = "Child last name required")]
        public string Lname { get; set; }
        [Required(ErrorMessage = "Please select child DOB")]
        public Nullable<System.DateTime> dob { get; set; }
        public Nullable<decimal> age { get; set; }
       // public string ssn { get; set; }
        [Required(ErrorMessage = "Please Choose Gender")]
        public Nullable<bool> isMale { get; set; }
        public Nullable<bool> inHouseHold { get; set; }
       // [Required(ErrorMessage = "Please Choose Inculde on Education")]
        public Nullable<bool> includeEduPage { get; set; }
        public Nullable<System.DateTime> childCreatedDate { get; set; }
        public Nullable<System.DateTime> childUpdatedDate { get; set; }
        public Nullable<long> childModifiedBy { get; set; }
    }
    public class childDelete : Success
    {
        [Required(ErrorMessage = "Profile required")]
        public  long profileID { get; set; }
        [Required(ErrorMessage = "Child required")]
        public  long childID { get; set; }
    }

}
