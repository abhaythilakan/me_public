﻿using CLayer.common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class DebtModel : Success
    {
        public long debtID { get; set; }

        
        [Required(ErrorMessage = "Invalid Profile")]
        [Display(Name = "Profile ID")]
        [Range(1, long.MaxValue, ErrorMessage = "Invalid Profile")]
        public long profileID { get; set; }

        public Nullable<int> debtCatId { get; set; }

        [Required(ErrorMessage = "Creditor is required.")]
        [Display(Name = "Creditor")]
        public string creditor { get; set; }

        [Required(ErrorMessage = "Balance is required.")]
        public Nullable<decimal> amount { get; set; }
        [Required(ErrorMessage = "Principal and Interest Payment is required.")]
        public Nullable<decimal> minPay { get; set; }
        public Nullable<decimal> unPaidBalance { get; set; }
        public Nullable<decimal> actualPay { get; set; }
        public Nullable<decimal> extraPay { get; set; }
        [Required(ErrorMessage = "Interest Rate is required.")]
        public Nullable<decimal> rate { get; set; }
        public Nullable<bool> isFixed { get; set; }
        public string notes { get; set; }
        public Nullable<decimal> percentPay { get; set; }
        public Nullable<decimal> introRate { get; set; }
        public Nullable<System.DateTime> introDate { get; set; }
        public string introDate_SUB { get; set; }
        public Nullable<bool> isPlan { get; set; }

        [Required(ErrorMessage = "Debit Type is required.")]
        [Range(1, long.MaxValue, ErrorMessage = "Debt Type is required.")]
        [Display(Name = "Debit Type")]
        public Nullable<int> debtTypeID { get; set; }

        public Nullable<bool> inclElmPlan { get; set; }
        public Nullable<decimal> cardMinPay { get; set; }
        public Nullable<System.DateTime> Balanceasof { get; set; }
        public string Balanceasof_SUB { get; set; }
        public Nullable<bool> isPrimaryResidence { get; set; }
        public Nullable<int> NotesShow { get; set; }
        public Nullable<System.DateTime> debtCreatedDate { get; set; }
        public string debtCreatedDate_SUB { get; set; }
        public Nullable<System.DateTime> debtModifiedDate { get; set; }
        public string debtModifiedDate_SUB { get; set; }
        public Nullable<long> debtModifiedBy { get; set; }
        public Nullable<System.DateTime> projectedPayOffDate { get; set; }
        public string projectedPayOffDate_SUB { get; set; }

        public bool? isActive { get; set; }

        #region Additional Properties
        public string advisorFirstName { get; set; }
        public string advisorLastName { get; set; }
        
        public string projectedPayOff { get; set; }
        public decimal? projectedInterest { get; set; }


        [EmailAddress(ErrorMessage = "Email address is invalid")]
        [StringLength(30, ErrorMessage = "Email address length should be less than 30")]
        [Display(Name = "Email Address")]
        public string emailPrimary { get; set; }

        public List<DropDownList_Int> mortageDDL { get; set; }
        public List<DropDownList_Int> fixedDDL { get; set; }
        public List<DropDownList_Int> revolvingDDL { get; set; }
        #endregion

        public DebtModel()
        {
            List<DropDownList_Int> ddl1 = common.DDLReg.getDropDownList_Int(enumDropDownList.DebtMortage);
            mortageDDL = ddl1;
            List<DropDownList_Int> ddl2 = common.DDLReg.getDropDownList_Int(enumDropDownList.DebtFixed);
            fixedDDL = ddl2;
            List<DropDownList_Int> ddl3 = common.DDLReg.getDropDownList_Int(enumDropDownList.DebtRevolving);
            revolvingDDL = ddl3;
        }
    }

    public class DebtDetailModel : Success
    {
        [Range(1, long.MaxValue, ErrorMessage = "Invalid Profile")]
        public long profileID;

        [Required(ErrorMessage = "Invalid Debt")]
        [Range(1, long.MaxValue, ErrorMessage = "Invalid Debt")]
        [Display(Name = "Debt ID")]
        public long debtID { get; set; }
    }

    public class DebtDisappearPartialModel
    {
        public string Creditor { get; set; }
        public int debtType { get; set; }
        public string debtTypeName { get; set; }

    }

    public class DebtAmortizationModel
    {
        public DebtProjectionInput DebtProjectionInput { get; set; }
        public List<DebtProjectionModel> DebtProjectionList { get; set; }
    }

    public class DebtDisappearModel
    {
        [Required(ErrorMessage = "Invalid Profile")]
        [Range(1, long.MaxValue, ErrorMessage = "Invalid Profile")]
        [Display(Name = "Profile ID")]
        public long profileID { get; set; }
        public decimal extraPay { get; set; }
    }

    public class DebtProjectionInput
    {
        [Required(ErrorMessage ="Invalid Debt")]
        public long debtId { get; set; }
        public bool isYearlychart { get; set; }
        public string debtName { get; set; }
        public long debtCatId { get; set; }
        public string debtCatName { get; set; }
        public bool isOneTimePay { get; set; }
        public bool isAllTimePay { get; set; }
        public decimal additionalPayAllTime { get; set; }
        public decimal additionalPayOneTime { get; set; }
        public int noOfPay { get; set; }
        public string payOff { get; set; }
        public DateTime payOffDate { get; set; }
        public decimal totalAdditional { get; set; }
        public decimal totalInterest { get; set; }
        public decimal totalPay { get; set; }
        public decimal startBalance { get; set; }
        public decimal interestRate { get; set; }
        public decimal payPrincInterstPerMnth { get; set; }
    }

    public class DebtProjectionModel
    {
        public DateTime Date { get; set; }
        public string dateString { get; set; }
        public string Period { get; set; }
        public decimal principal { get; set; }
        public decimal interest { get; set; }
        public decimal totalPrincipal { get; set; }
        public decimal totalInterest { get; set; }
        public decimal totalPayment { get; set; }
        public decimal additionalPay { get; set; }
        public decimal balance { get; set; }
    }
}
