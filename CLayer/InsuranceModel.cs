﻿using CLayer.common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{

    public class FullInsurance
    {
        public OtherLifeInsurance OtherInsurance { get; set; }
        public FullLifeInsurance PrimaryInsurance { get; set; }
        public FullLifeInsurance SpouseInsurance { get; set; }
        public ChildInsurance ChildInsurance { get; set; }
    }
    public class OtherLifeInsurance
    {
        public decimal monthlyPremium { get; set; }
        public List<OtherInsuranceModel> InsuranceDetails { get; set; }
    }
    public class FullLifeInsurance
    {
        public PersonInfoModel personInfo { get; set; }
        public InsuranceDetailsModel details { get; set; }
    }
    public class ChildInsurance
    {
        public decimal faceValue { get; set; }
        public List<ChildInfoModel> childInfo { get; set; }

    }
    public partial class PersonInfoModel
    {
        public long personID { get; set; }
        public long profileID { get; set; }
        public string firstName { get; set; }
        public string midName { get; set; }
        public string lastName { get; set; }
        public Nullable<bool> isMale { get; set; }
        public Nullable<bool> isPrimary { get; set; }
        public decimal faceValue { get; set; }
        public List<InsuranceModel> InsuranceDetails { get; set; }
    }
    public partial class ChildInfoModel
    {
        public long childID { get; set; }
        public Nullable<long> profileID { get; set; }
        public string fName { get; set; }
        public string midName { get; set; }
        public string Lname { get; set; }
        public Nullable<bool> isMale { get; set; }
        public decimal faceValue { get; set; }
        public List<InsuranceModel> InsuranceDetails { get; set; }
    }
    public class InsuranceModel
    {
        [Required(ErrorMessage = "Life Id required")]
        public long lifeID { get; set; }
        [Required(ErrorMessage = "Profile Id required")]
        public long profileID { get; set; }
        [Required(ErrorMessage = "Face value required")]
        public Nullable<decimal> faceValue { get; set; }
        public Nullable<long> personID { get; set; }
        public Nullable<long> childID { get; set; }
        [Range(1, long.MaxValue, ErrorMessage = "Type required")]
        public long lifeTypeID { get; set; }
        public string lifeTypeName { get; set; }
        [Required(ErrorMessage = "Company name required")]
        [StringLength(50, ErrorMessage = "Company name length should be less than 50")]
        public string company { get; set; }
        [Required(ErrorMessage = "Current cash value required")]
        public Nullable<decimal> amount { get; set; }
        [Range(1, long.MaxValue, ErrorMessage = "Premium type required")]
        public long premiumType { get; set; }
        [Required(ErrorMessage = "Premium amount required")]
        public Nullable<decimal> premiumAmount { get; set; }
        public Nullable<decimal> monthly { get; set; }
        public string notes { get; set; }
        [Required(ErrorMessage = "Date field required")]
        public Nullable<System.DateTime> valueAsOf { get; set; }
        public Nullable<int> NotesShow { get; set; }
        public string Type { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<bool> showInMouseOver { get; set; } = false;
    }
    public class OtherInsuranceModel
    {
        [Required(ErrorMessage = "Life Id required")]
        public long lifeID { get; set; }
        [Required(ErrorMessage = "Profile Id required")]
        public long profileID { get; set; }
        [Range(1, long.MaxValue, ErrorMessage = "Type required")]
        public long lifeTypeID { get; set; }
        public string lifeTypeName { get; set; }
        [Required(ErrorMessage = "Company name required")]
        [StringLength(50, ErrorMessage = "Company name length should be less than 50")]
        public string company { get; set; }
        [Range(1, long.MaxValue, ErrorMessage = "Premium type required")]
        public long premiumType { get; set; }
        [Required(ErrorMessage = "Premium amount required")]
        public Nullable<decimal> premiumAmount { get; set; }
        public Nullable<decimal> monthly { get; set; }
        public string notes { get; set; }
        [Required(ErrorMessage = "Date field required")]
        public Nullable<System.DateTime> valueAsOf { get; set; }
        public Nullable<int> NotesShow { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<bool> showInMouseOver { get; set; } = false;
    }
    public class OtherInsuranceInputModel
    {
        public long lifeId { get; set; }
        public long profileId { get; set; }
        public Nullable<long> modifiedBy { get; set; }
    }
    public class InsuranceInputModel
    {
        public long lifeId { get; set; }
        public long profileId { get; set; }
        [Required(ErrorMessage = "Type required")]
        public string type { get; set; }
        public Nullable<long> modifiedBy { get; set; }
    }
    public class InsuranceChildInput
    {
        public long lifeId { get; set; }
        public long profileId { get; set; }
        public long childId { get; set; }
        public Nullable<long> modifiedBy { get; set; }
    }
    public class InsuranceDetailsModel
    {
        public long InsuranceId { get; set; }
        [Required(ErrorMessage = "Profile Id required")]
        public Nullable<long> profileId { get; set; }
        [Required(ErrorMessage = "Person Id required")]
        public Nullable<long> personId { get; set; }
        public bool payDept { get; set; }
        public bool payMortage { get; set; }
        public bool payCollege { get; set; }
        public Nullable<decimal> financialExpence { get; set; }
        public Nullable<decimal> expenceafterdeath { get; set; }
        public Nullable<long> insurancePeriod { get; set; }
        public Nullable<long> insuranceType { get; set; }
        public bool retAsset { get; set; }
        public bool otherRetAsset { get; set; }
        public bool cashAccounts { get; set; }
        public bool spouseRetAsset { get; set; }
        public bool nonRetAsset { get; set; }
        public bool selectAll { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<System.DateTime> modifiedDate { get; set; }
        public Nullable<long> modifiedBy { get; set; }
        public string type { get; set; }
    }
    //public class InsuranceTypeModel
    //{
    //    public long insuranceTypeID { get; set; }
    //    public string insuranceTypeName { get; set; }
    //    public int orderNum { get; set; }
    //}
    public class LifeTypeModel
    {
        public long lifeTypeID { get; set; }
        public string lifeTypeName { get; set; }
        public int orderNum { get; set; }
        public bool isGroup { get; set; }
    }

}
