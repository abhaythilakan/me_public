﻿using CLayer.common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class ProfileModel : Success
    {
        public long profileID { get; set; }
        public Nullable<System.DateTime> profileDate { get; set; }
        public string notes { get; set; }
        [Display(Name = "Profile Name")]
        [Required(ErrorMessage = "Profile name is required")]
        public string profileName { get; set; }
        [Required(ErrorMessage = "Invalid Client")]
        [Display(Name = "Client")]
        [Range(1, long.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public Nullable<long> clientID { get; set; }
        public Nullable<bool> isClientAccess { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<int> designMode { get; set; }
        public Nullable<bool> formattedActionPlan { get; set; }
        public Nullable<int> reportType { get; set; }
        public Nullable<bool> isReportDateChanged { get; set; }
        public string finYear { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
    }

    public class ProfileListArgModel : Success
    {
        public long profileId { get; set; }
        [Required(ErrorMessage = "Invalid Advisor")]
        [Display(Name = "Advisor")]
        [Range(1, long.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public long advisorId { get; set; }
        [Required(ErrorMessage = "Invalid Client")]
        [Display(Name = "Client")]
        [Range(1, long.MaxValue, ErrorMessage = "Please enter a value bigger than 0")]
        public long clientId { get; set; }
        //[Required(ErrorMessage = "Invalid Profile")]
        //[Display(Name = "Profile")]
        //public long profileId { get; set; }
        public int TotalCount { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int SortColumn { get; set; }
        public int SortOrder { get; set; }
    }

    public class ProfileCollection : Success
    {
        public List<PermissionModel> permissionList;

        public int usrCat { get { return (int)CustomEnum.userCatEnum.Profile; } set { } }
        public ProfileModel profile { get; set; }
        public DefaultsGlobalModel global { get; set; }
        public DefaultsRetirementModel ret { get; set; }
        public DefaultIncomeProjectionModel Income { get; set; }
        public DefaultsDebtModel debt { get; set; }
        public DefaultsEmergencyFundModel emerg { get; set; }
        public DefaultsInsuranceModel insu { get; set; }
        public DefaultsEducationModel edu { get; set; }
    }
}
