﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class InsuranceTypeModel
    {
        public long insuranceTypeID { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is required")]
        public string insuranceTypeName { get; set; }
        [Display(Name = "Order")]
        public int orderNum { get; set; }
        public Nullable<System.DateTime> createdDT { get; set; }
        public Nullable<System.DateTime> modifyDT { get; set; }
        public Nullable<long> modifyBy { get; set; }
        public Nullable<bool> isActive { get; set; }
    }
}
