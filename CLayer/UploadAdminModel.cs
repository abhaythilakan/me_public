﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class UploadAdminModel
    {
        public long ul_Id { get; set; }
        [Display(Name ="Path Label")]
        [Required(ErrorMessage ="Path Label is requied")]
        public string ul_PathLabel { get; set; }
        ////[DataType(DataType.Upload)]
        [Display(Name = "Upload")]
        [Required(ErrorMessage = "Please upload a file")]
        public System.Web.HttpPostedFileBase ul_File { get; set; }
        [Display(Name = "Name")]
        public string ul_FileName { get; set; }
        [Display(Name = "Size")]
        public string ul_FileSize { get; set; }
        [Display(Name = "Uploaded On")]
        public Nullable<System.DateTime> ul_CreatedDT { get; set; }
        public string ul_FilePath { get; set; }
        [Display(Name = "Year")]
        public Nullable<int> ul_Year { get; set; }
        public Nullable<bool> isActive { get; set; }
    }
}
