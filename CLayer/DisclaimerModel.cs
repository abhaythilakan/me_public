﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class DisclaimerModel
    {
        public int dis_Id { get; set; }
        [Display(Name ="Disclaimer Name")]
        [Required(ErrorMessage = "Disclaimer Name is required")]
        public string dis_Name { get; set; }
        [Display(Name = "Disclaimer Body")]
        public string dis_Body { get; set; }
        public Nullable<System.DateTime> createdDT { get; set; }
        public Nullable<System.DateTime> modifyDT { get; set; }
        public Nullable<long> modifyBy { get; set; }
        public bool dis_isActive { get; set; }
    }
}
