﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class LoginClient
    {
        public Nullable<long> clientID { get; set; }
        public Nullable<long> profileID { get; set; }
        public string defaultCID { get; set; }
        public Nullable<DateTime> expireDate { get; set; }
        public Nullable<int> isActive { get; set; }
        public Nullable<DateTime> endTrialDate { get; set; }
        public Nullable<int> isMale { get; set; }
        public Nullable<int> isPrimary { get; set; }
        public string Name { get; set; }
        public Nullable<int> exemptbilling { get; set; }
    }
}
