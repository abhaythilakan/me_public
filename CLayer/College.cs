﻿using CLayer.common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
     public class AllCollegeModel//: Success
    {
        //public int ROWNUM { get; set; }
        public int TotalCount { get; set; }
        public long collegeID { get; set; }
        public string collegeName { get; set; }
        public string collegeState { get; set; }
        public Nullable<decimal> outState { get; set; }
        public Nullable<decimal> inState { get; set; }
        public Nullable<decimal> outStateTutionFees { get; set; }
        public Nullable<decimal> inStateTutionFees { get; set; }
        public Nullable<decimal> fees { get; set; }
        public Nullable<decimal> roomBoardFees { get; set; }
        public Nullable<decimal> roomFees { get; set; }
        public string finYear { get; set; }
        public string city { get; set; }
        public string Location { get; set; }
    }

    public class CollegeListArgs //: Success
    {
            public string searchValue { get; set; }
            public int TotalCount { get; set; }
            public int PageNo { get; set; }
            public int PageSize { get; set; }
            public int SortColumn { get; set; }
            public int SortOrder { get; set; }

    }
    public class AdvisorFavList //: Success
    {
        public long clientID { get; set; }
        public long profileID { get; set; }
        public long advisorId { get; set; }
        public List<long> CollegeID { get; set; }
   

    }
    public class CollegeList //: Success
    {
        public long collegeId { get; set; }
        public long favCollegeId { get; set; }
        public bool isActive { get; set; }
    }

}
