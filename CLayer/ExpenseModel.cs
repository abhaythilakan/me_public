﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class LivingExpenseModel
    {
        public Nullable<decimal> totalExpense { get; set; }
        public Nullable<decimal> homeExpense { get; set; }
        public Nullable<decimal> nonHomeExpense { get; set; }

        public List<ExpenseModel> expenseList { get; set; }
    }
    public class ExpenseCategoryModel
    {
        public long id { get; set; }
        public string expenseCategory1 { get; set; }
    }
    public class ExpenseSubCategoryModel
    {
        public long id { get; set; }
        public string subCategory { get; set; }
        public Nullable<long> expenseCategoryId { get; set; }
    }
    //public class ExpenseTypeModel
    //{
    //    public long id { get; set; }
    //    public string expenseType1 { get; set; }
    //    public Nullable<long> expenseSubTypeId { get; set; }
    //}
    public class ExpenseInputModel
    {
        public long expenseId { get; set; }
        public long profileId { get; set; }
        public Nullable<long> modifiedBy { get; set; }
    }
    public class ExpenseModel
    {
        [Required(ErrorMessage = "Expense Id required")]
        public long ExpenseId { get; set; }
        [Required(ErrorMessage = "Profile Id required")]
        public long profileID { get; set; }
        [Required(ErrorMessage = "Expense category required")]
        public Nullable<long> expenseCategoryId { get; set; }
        [Required(ErrorMessage = "Expense sub-category required")]
        [Range(1, long.MaxValue, ErrorMessage = "Expense sub-category required")]
        public Nullable<long> expSubCategoryId { get; set; }
        public Nullable<long> typeId { get; set; }
        [Required(ErrorMessage = "Company name required")]
        [StringLength(50, ErrorMessage = "Company name length should be less than 50")]
        public string company { get; set; }
        [Required(ErrorMessage = "Amount required")]
        public Nullable<decimal> amount { get; set; }
        [StringLength(500, ErrorMessage = "Notes length should be less than 500")]
        public string notes { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<long> ModifiedBy { get; set; }
        public Nullable<bool> showInMouseOver { get; set; } = false;
    }
    public class ExpenseListModel
    {
        public InsurancePremiumModel insuranceExpList { get; set; }
        public DeptExpenseModel deptExpList { get; set; }
        public IncomeExpenseOutput incomeExpList { get; set; }
        public TaxExpenseOutput taxExpList { get; set; }
        public SavingsExpenseOutput savExpList { get; set; }
    }
    public class InsurancePremiumModel
    {
        public Nullable<decimal> faceValueSum { get; set; }
        public List<IndividualInsurance> insuranceList { get; set; }
    }
    public class IndividualInsurance
    {
        public string InsuranceCategory { get; set; }
        public string insuranceType { get; set; }
        public string company { get; set; }
        public string name { get; set; }
        public Nullable<decimal> faceValue { get; set; }
        public Nullable<decimal> currentCashValue { get; set; }
    }
    public class DeptExpenseModel
    {
        public Nullable<decimal> totalPay { get; set; }
        public List<DeptExpenseList> deptList { get; set; }
    }
    public class DeptExpenseList
    {
        public string deptCatType { get; set; }
        public string deptType { get; set; }
        public string creditorName { get; set; }
        public Nullable<decimal> balance { get; set; }
        public string balanceAsOf { get; set; }
        public Nullable<decimal> interestRate { get; set; }
        public Nullable<decimal> principleInterestRate { get; set; }
    }
    public class IncomeExpenseOutput
    {
        public Nullable<decimal> total { get; set; }
        public List<IncomeExpenseModel> incomeOutput { get; set; }
    }
    public class IncomeExpenseModel
    {
        public string name { get; set; }
        public Nullable<decimal> totalGrossAmount { get; set; }
        public List<IncomeExpenseList> incomeList { get; set; }
    }
    public class IncomeExpenseList
    {
        public string incomeName { get; set; }
        public Nullable<decimal> grossAmount { get; set; }
    }
    public class TaxExpenseOutput
    {
        public Nullable<decimal> total { get; set; }
        public List<TaxExpenseModel> expenseOutput { get; set; }
    }
    public class TaxExpenseModel
    {
        public string name { get; set; }
        public Nullable<decimal> totalTax { get; set; }
        public List<TaxExpenseList> incomeList { get; set; }
    }
    public class TaxExpenseList
    {
        public string taxName { get; set; }
        public Nullable<decimal> taxAmount { get; set; }
    }
    public class SavingsExpenseOutput
    {
        public Nullable<decimal> total{ get; set; }
        public List<SavingsExpenseModel> savingOutput { get; set; }
    }
    public class SavingsExpenseModel
    {
        public string name { get; set; }
        public Nullable<decimal> totalSaving { get; set; }
        public List<SavingsExpenseList> savingList { get; set; }
    }
    public class SavingsExpenseList
    {
        public string SavingName { get; set; }
        public Nullable<decimal> SavingAmount { get; set; }
    }
}
