﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class EarlyInvestorModel
    {
        //[Required(ErrorMessage = "Current Age Required")]
        [Range(1, 100, ErrorMessage = "Select an age between 1 and 100.")]
        public Nullable<int> currentAge { get; set; }
        [Required(ErrorMessage = "End Year Required")]
        [Range(20, 45, ErrorMessage = "Select any number of end years between 20 and 45.")]
        public Nullable<int> endYear { get; set; }
        [Required(ErrorMessage = "Anual Investment Amount required")]
        [Range(1000, Int64.MaxValue, ErrorMessage = "Select an Annual Investment amount of $1,000 or more.")]
        public Nullable<decimal> anualInvestment { get; set; }
        [Required(ErrorMessage = "Rate of return Required")]
        [Range(6, 12, ErrorMessage = "% Input an assumed Rate of Return between 6% and 12%.")]
        public Nullable<decimal> rateOfreturn { get; set; }

        public Nullable<bool> isGraphEnable { get; set; }

        public Nullable<int> earlySpender { get; set; }
        public Nullable<decimal> totalAnualInvestmentA { get; set; }
        public Nullable<decimal> totalAnualInvestmentB { get; set; }
        public List<EarlyInvestorModelDetails> Details { get; set; }
    }
    public class EarlyInvestorModelDetails
    {
        public int year { get; set; }
        public Nullable<int> age { get; set; }
        public decimal anualInvestmentA { get; set; }
        public decimal accumalationA { get; set; }
        public decimal anualInvestmentB { get; set; }
        public decimal accumalationB { get; set; }
        public decimal earlyInvestor { get; set; }
    }

    public class TaxableVsTaxDefferedModel
    {


        [Range(1, 100, ErrorMessage = "Select an age between 1 and 100.")]
        public Nullable<int> cAge { get; set; }
        [Required(ErrorMessage = "Fully taxable Investment Rate Required.")]
        [Range(0, 12, ErrorMessage = "Select any number of Fully taxable Investment Rate  between 0 and 12.")]
        public Nullable<decimal> taxRate { get; set; }
        [Required(ErrorMessage = "Tax-deferred Investment Rate Required.")]
        [Range(0, 12, ErrorMessage = "Select any number of 	Tax-deferred Investment Rate between 0 and 12.")]
        public Nullable<decimal> defTaxRate { get; set; }
        [Required(ErrorMessage = "Tax-Free Investment Rate Required.")]
        [Range(0, 12, ErrorMessage = "Select any number of 	Tax-deferred Investment Rate between 0 and 12.")]
        public Nullable<decimal> taxFreeRate { get; set; }

        [Required(ErrorMessage = "Federal Tax Rate  Required.")]
        [Range(0, 35, ErrorMessage = "Select any number of Federal Tax Rate between 0 and 35.")]
        public Nullable<decimal> fTaxRateA { get; set; }
        [Required(ErrorMessage = "Federal Tax Rate  Required.")]
        [Range(0, 35, ErrorMessage = "Select any number of Federal Tax Rate  between 0 and 35.")]
        public Nullable<decimal> fTaxRateB { get; set; }

        [Range(0, 100, ErrorMessage = "Select any number of State Tax Rate between 0 and 100.")]
        public Nullable<decimal> sTaxRateA { get; set; }
        [Range(0, 100, ErrorMessage = "Select any number of State Tax Rate between 0 and 100.")]
        public Nullable<decimal> sTaxRateB { get; set; }

        [Range(0, 100, ErrorMessage = "Select any number of Local/Other Tax Rate between 0 and 100.")]
        public Nullable<decimal> olTaxRateA { get; set; }
        [Range(0, 100, ErrorMessage = "Select any number of Local/Other Tax Rate between 0 and 100.")]
        public Nullable<decimal> olTaxRateB { get; set; }

        [Required(ErrorMessage = "Initial Starting Investment Required.")]
        [Range(0, Int64.MaxValue, ErrorMessage = "Initial Starting Investment not valid")]
        public Nullable<decimal> amount { get; set; }

        [Required(ErrorMessage = "No of years Required.")]
        [Range(0, 100, ErrorMessage = "Select any number of years between 0 and 100.")]
        public Nullable<int> years { get; set; }



        public Nullable<decimal> annualInvest { get; set; }

        public Nullable<decimal> TotalTaxable { get; set; }
        public Nullable<decimal> TotalTaxDeferred { get; set; }
        public Nullable<decimal> TotalTaxFree { get; set; }
        public Nullable<decimal> TotTxAfter { get; set; }
        public Nullable<decimal> TotDfTxAfter { get; set; }
        public Nullable<decimal> TotTfTxAfter { get; set; }

        public Nullable<decimal> taxReturnRate { get; set; }
        public Nullable<decimal> defTaxReturnRate { get; set; }
        public List<TaxableVsTaxDefferedDetailsModel> Details { get; set; }
    }
    public class TaxableVsTaxDefferedDetailsModel
    {
        public int year { get; set; }
        public decimal FuturevalueA { get; set; }
        public decimal FuturevalueB { get; set; }
        public decimal FuturevalueC { get; set; }
    }


    public class CompoundInterestModel
    {
        public string name { get; set; }
        [Range(1, 100, ErrorMessage = "Select an age between 1 and 100.")]
        [RegularExpression(@"[0-9]*", ErrorMessage = "Must be numeric")]
        public Nullable<int> age { get; set; }
        [Required(ErrorMessage = "Initial Starting Investment Required")]
        public Nullable<decimal> initialStartInvestment { get; set; }
        [Required(ErrorMessage = "Monthly Investment Required")]
        public Nullable<decimal> monthlyInvestment { get; set; }
        [Required(ErrorMessage = "Annual Interest Rate madatory")]
        [Range(0, 12, ErrorMessage = "Select any number of Annual Interest Rate between 0 and 12.")]
        public Nullable<decimal> interestRate1 { get; set; }
        [Required(ErrorMessage = "Annual Interest Rate madatory")]
        [Range(0, 12, ErrorMessage = "Select any number of Annual Interest Rate  between 0 and 12.")]
        public Nullable<decimal> interestRate2 { get; set; }
        [Required(ErrorMessage = "Annual Interest Rate madatory")]
        [Range(0, 12, ErrorMessage = "Select any number of Annual Interest Rate  between 0 and 12.")]
        public Nullable<decimal> interestRate3 { get; set; }
        [Required(ErrorMessage = "No of Years madatory")]
        [Range(1, 100, ErrorMessage = "Select no of years between 1 and 50")]
        [RegularExpression(@"[0-9]*", ErrorMessage = "Must be numeric")]
        public Nullable<int> no_of_years { get; set; }
        public Nullable<decimal> total_anualInvestment { get; set; }
        public Nullable<decimal> total_totalInvestment { get; set; }
        public Nullable<decimal> total_futurevalueA { get; set; }
        public Nullable<decimal> total_futurevalueB { get; set; }
        public Nullable<decimal> total_futurevalueC { get; set; }
        public List<CompoundInterestDetailsModel> cInterestDetails { get; set; }
    }
    public class CompoundInterestDetailsModel
    {
        public Nullable<int> age { get; set; }
        public int years { get; set; }
        public decimal anualInvestment { get; set; }
        public decimal totalInvestment { get; set; }
        public decimal futurevalueA { get; set; }
        public decimal futurevalueB { get; set; }
        public decimal futurevalueC { get; set; }
    }

    public class DIMEModel
    {
        //[RegularExpression("[~`!@#$%^&*()-+=|\\{}':;.,<>/?]", ErrorMessage = "Primary name contains letters only")]
        public string pName { get; set; }
        [RegularExpression(@"[0-9]*", ErrorMessage = "Primary age Must be Numeric value.")]
        public Nullable<int> prAge { get; set; }
        //[RegularExpression("[a-zA-Z]", ErrorMessage = "Spouse name contains letters only")]
        public string sName { get; set; }
        [RegularExpression(@"[0-9]*", ErrorMessage = "Spouse age Must be Numeric value.")]
        public Nullable<int> sAge { get; set; }

        [Range(0, Int64.MaxValue, ErrorMessage = "Select any number of Primary Monthly Need value. ")]
        [Required(ErrorMessage = " Primary Monthly need is madatory")]
        public Nullable<decimal> pMonthlyNeed { get; set; }
        [Range(0, 150, ErrorMessage = "Select any number of Primary Years between 0 and 150.")]
        [Required(ErrorMessage = " Primary Years is madatory")]
        public Nullable<int> pYears { get; set; }
        [Range(0, 150, ErrorMessage = "Select any number of Spouse Years between 0 and 150.")]
        // [Required(ErrorMessage = " Spouse Years is madatory")]
        public Nullable<int> sYears { get; set; }
        public Nullable<bool> isPrimaryIncluded { get; set; }

        [Range(0, Int64.MaxValue, ErrorMessage = "Select any number of Spouse Monthly Need value. ")]
        // [Required(ErrorMessage = " Spouse Years is madatory")]
        public Nullable<decimal> sMonthlyNeed { get; set; }
        public Nullable<bool> isSpouseIncluded { get; set; }
        [Range(0, Int64.MaxValue, ErrorMessage = "Select any number of death Expenses value. ")]
        public Nullable<decimal> deathExpenses { get; set; }
        public Nullable<bool> isDeathExpensesIncluded { get; set; }
        [Range(0, Int64.MaxValue, ErrorMessage = "Select any number of debt Amount value. ")]
        public Nullable<decimal> debtAmount { get; set; }
        public Nullable<bool> isDebtAmountIncluded { get; set; }
        [Range(0, Int64.MaxValue, ErrorMessage = "Select any number of mortgage Balance value .")]
        public Nullable<decimal> mortgageBalance { get; set; }
        public Nullable<bool> ismortgageBalanceIncluded { get; set; }
        [Range(0, Int64.MaxValue, ErrorMessage = "Select any number of education Needs value .")]
        public Nullable<decimal> educationNeeds { get; set; }
        public Nullable<bool> isEducationNeedsIncluded { get; set; }
        [Required(ErrorMessage = "Inflation Rate is madatory")]
        [Range(0, 12, ErrorMessage = "Select any number of Annual Inflation Rate between 0 and 12.")]
        public Nullable<decimal> inflationRate { get; set; }
        [Required(ErrorMessage = "Return Rate is madatory")]
        [Range(0, 12, ErrorMessage = "Select any number of Return Rate between 0 and 12.")]
        public Nullable<decimal> returnRate { get; set; }
        public Nullable<decimal> interestRate { get; set; }
        public Nullable<bool> isSpouseShow { get; set; }
        [Range(0, 10, ErrorMessage = "Select any number of Return Rate between 0 and 10.")]
        [Required(ErrorMessage = "Children nos is madatory")]
        public Nullable<int> Children { get; set; }
        public Nullable<decimal> pTotalNeed { get; set; }
        public Nullable<decimal> sTotalNeed { get; set; }
        public DIMEDetailsModel primaryProtectionNeeds { get; set; }
        public DIMEDetailsModel spouseProtectionNeeds { get; set; }

    }

    public class DIMEDetailsModel
    {
        public string name { get; set; }
        public Nullable<decimal> DebtDeath { get; set; }
        public Nullable<decimal> IncomeNeed { get; set; }
        public Nullable<decimal> Mortgage { get; set; }
        public Nullable<decimal> Education { get; set; }
    }
    public class GoalsAndDreamsModel
    {
        public string name { get; set; }
        [Required(ErrorMessage = "Inflation Rate mandatory")]
        [Range(0, 12, ErrorMessage = "Select any number of Inflation Rate between 0 and 12")]
        public Nullable<decimal> inflationRate { get; set; }
        [Required(ErrorMessage = "Return Rate mandatory")]
        [Range(0, 12, ErrorMessage = "Select any number of Return Rate between 0 and 12")]
        public Nullable<decimal> returnRate1 { get; set; }
        [Required(ErrorMessage = "Return Rate mandatory")]
        [Range(0, 12, ErrorMessage = "Select any number of Return Rate between 0 and 12")]
        public Nullable<decimal> returnRate2 { get; set; }
        [Required(ErrorMessage = "Return Rate mandatory")]
        [Range(0, 12, ErrorMessage = "Select any number of Return Rate between 0 and 12")]
        public Nullable<decimal> returnRate3 { get; set; }
        public List<GoalsInputModel> goalsInput { get; set; }
        public List<GoalsOutputModel> goalsOutput { get; set; }
        public decimal totalCurrentCostToday { get; set; }
        public decimal totalcurrentSavingsToday { get; set; }
        public string maxGoalDate { get; set; }
        public decimal totalGoalCostFuture { get; set; }
        public decimal totalCurrentSavingsFuture { get; set; }
        public decimal totalSavingsGap { get; set; }
        public int totalMonthsToSave { get; set; }
        public decimal totalMonthlySavingsNeeded { get; set; }

    }
    public class GoalsInputModel
    {
        public string savingName { get; set; }
        [Required(ErrorMessage = "No of Years mandatory")]
        [Range(1, 100, ErrorMessage = "Select no of years between 1 and 100")]
        [RegularExpression(@"[0-9]*", ErrorMessage = "Must be numeric")]
        public Nullable<int> years { get; set; }
        [Required(ErrorMessage = "Current Cost mandatory")]
        public Nullable<decimal> currentCost { get; set; }
        [Required(ErrorMessage = "Current Savings mandatory")]
        public Nullable<decimal> currentSavings { get; set; }
    }
    public class GoalsOutputModel
    {
        public string goalName { get; set; }
        public decimal currentCost { get; set; }
        public decimal currentSavings { get; set; }
        public string goalDate { get; set; }
        public decimal goalCostFuture { get; set; }
        public decimal currentSavingsFuture { get; set; }
        public decimal savingsGap { get; set; }
        public int monthsToSave { get; set; }
        public decimal monthlySavingsNeeded { get; set; }

    }

    public class CreditCardModel
    {


        public string cName { get; set; }
        [Required(ErrorMessage = "Balance on your card is required.")]
        public Nullable<double> loanAmount { get; set; }
        [Required(ErrorMessage = "Annual Interest rate is required.")]
        [Range(0, 30, ErrorMessage = "Select any value of annual Interest rate between 0 and 30.")]
        public Nullable<double> intRate { get; set; }
        [Required(ErrorMessage = "The minimum dollar payment amount is required.")]
        [Range(10, 30, ErrorMessage = "Select any value of minimum dollar payment amount between 10 and 30.")]
        public Nullable<double> cardMinPay { get; set; }
        [Range(2, 5, ErrorMessage = "Select any value of minimum percent payment rate between 2 and 5.")]
        [Required(ErrorMessage = "The minimum percent payment amount is required.")]
        public Nullable<double> percentPay { get; set; }
        [Required(ErrorMessage = "Month is required")]
        [Range(1, 12, ErrorMessage = "Please select a valid month.")]
        public Nullable<int> monName { get; set; }
        [Range(1500, 5000, ErrorMessage = "Please select a valid year.")]
        [Required(ErrorMessage = "Year is required")]
        public Nullable<int> years { get; set; }


        public string totPayments { get; set; }
        public string payOffYears { get; set; }
        public string payoff { get; set; }
        public string interestPaid { get; set; }
        public string currentBalance { get; set; }

        public CreditRevolveAmortization Amortization { get; set; }
        public bool IsYearly { get; set; }
        public bool fixedPayment { get; set; }
        public bool IsOneTime { get; set; }
        public decimal OneTimeValue { get; set; }
        public bool IsAllPayment { get; set; }
        public decimal AllPaymentValue { get; set; }
   

    }
    public class CreditRevolveAmortization
    {
        public double payments { get; set; }
        public string payoff { get; set; }
        public double TotalPayments { get; set; }
        public double TotalAdditional { get; set; }
        public double TotalInterest  { get; set; }
        public double minPay { get; set; }
        public double startingBalance { get; set; }
        public List<CreditRevolveModel> PaymentSummary { get; set; }

    }

    public class CreditRevolveModel
    {
        public int PMT { get; set; }
        public string date { get; set; }
        public Nullable<double> Principal { get; set; }
        public double totalPrincipal { get; set; }
        public Nullable<double> interest { get; set; }
        public double interestPaid { get; set; }
        public double totalPayment { get; set; }
        public double prePayment { get; set; }
        // public double cardMinPay { get; set; }   
        // public double percentPay { get; set; }    
        public double newBalance { get; set; }
    }


    public class VehicleLoanModel
    {
        public string cName { get; set; }
        [Range(0, Double.MaxValue, ErrorMessage = "Please enter total purchase price.")]
        [Required(ErrorMessage = "Please enter total purchase price.")]
        public Nullable<double> purchasePrice { get; set; }
        [Range(0, Double.MaxValue, ErrorMessage = "Please enter Rebates and Cash Down.")]
        [Required(ErrorMessage = "Please enter Rebates and Cash Down.")]
        public Nullable<double> cashDown { get; set; }
        [Required(ErrorMessage = "Cash Down percentage is required.")]
        [Range(10, 50, ErrorMessage = "Select any value of Cash Down rate between 10 and 50.")]
        public Nullable<double> drpCashDown { get; set; }
        public Nullable<double> tradeAllowance { get; set; }
        [Required(ErrorMessage = "Sales tax rate is required.")]
        [Range(0, 100, ErrorMessage = "Select any value of sales tax rate between 0 and 100.")]
        public Nullable<double> salesTaxRate { get; set; }
        [Required(ErrorMessage = "Interest rate is required.")]
        [Range(0, 100, ErrorMessage = "Select any value of  Interest rate between 0 and 100.")]
        public Nullable<double> intRate { get; set; }
        [Required(ErrorMessage = "Term in months is required.")]
        [Range(24, 72, ErrorMessage = "Select any value of Term in Months.")]
        public Nullable<int> noOfInstl { get; set; }
        [Required(ErrorMessage = "Month is required")]
        [Range(1, 12, ErrorMessage = "Please select a valid month.")]
        public Nullable<int> monName { get; set; }
        [Range(1500, 5000, ErrorMessage = "Please select a valid year.")]
        [Required(ErrorMessage = "Year is required")]
        public Nullable<int> years { get; set; }
        public double totSalesTax { get; set; }
        public double totBalanceFinanced { get; set; }
        public double monthlyPayment { get; set; }
        public double currentBalance { get; set; }
        public string currentDate { get; set; }
        public DateTime paidDate { get; set; }


        public bool isYearly { get; set; }
        public bool IsOneTime { get; set; }
        public decimal OneTimeValue { get; set; }
        public bool IsAllPayment { get; set; }
        public decimal AllPaymentValue { get; set; }
        public LoanAmortization LoanAmortization { get; set; }
    }

    public class LoanAmortization
    {
        public double payments { get; set; }
        public double totInterest { get; set; }
        public double totPrincipal { get; set; }
        public double InitialLoanAmount { get; set; }
        public double CashDown { get; set; }
        public double TradeAllowance { get; set; }
        public double MonthlyPayment { get; set; }
        public double TotalAdditional { get; set; }
        public string payoff { get; set; }
        public double TotalBalanceFinanced { get; set; }
        public List<PaymentSummaryModel> PaymentSummary { get; set; }

    }

    public class PaymentSummaryModel
    {
        public int PMT { get; set; }
        public string Date { get; set; }
        public Nullable<double> Balance { get; set; }
        public Nullable<double> Principal { get; set; }
        public Nullable<double> Interest { get; set; }
        public double totalPrincipal { get; set; }
        public double totalInterest { get; set; }
        public double prePayment { get; set; }
        // public double newBalance { get; set; }
        public Nullable<double> TotalPayment { get; set; }

    }

    public class FixedvsMinimumCalcModel
    {

        public string cName { get; set; }
        [Range(1, Double.MaxValue, ErrorMessage = "Please enter valid balance on your Card amount.")]
        [Required(ErrorMessage = "Please enter Balance on your Card.")]
        public Nullable<double> loanAmount { get; set; }
        [Required(ErrorMessage = "Annual Interest Rate is required.")]
        [Range(0, 100, ErrorMessage = "Select any value of Annual Interest Rate between 0 and 100.")]
        public Nullable<double> intRate { get; set; }
        [Required(ErrorMessage = "The minimum percent payment amount is required.")]
        [Range(2, 5, ErrorMessage = "Select any value of the minimum percent payment amount between 2 and 5.")]
        public Nullable<double> percentPay { get; set; }
        [Required(ErrorMessage = "The minimum dollar payment amount is required.")]
        [Range(15, 30, ErrorMessage = "Select any value of The minimum dollar payment amount between 15 and 30.")]
        public Nullable<double> cardMinPay { get; set; }
        [Required(ErrorMessage = "Month is required")]
        [Range(1, 12, ErrorMessage = "Please select a valid month.")]
        public Nullable<int> monName { get; set; }
        [Range(1500, 5000, ErrorMessage = "Please select a valid year.")]
        [Required(ErrorMessage = "Year is required")]
        public Nullable<int> years { get; set; }
        [Required(ErrorMessage = "A fixed dollar payment is required.")]
        public Nullable<double> minPay { get; set; }
        public FixedvsMinimumCalcDetailsModel output { get; set; }

        public CreditRevolveAmortization Amortization { get; set; }

        public bool IsYearly { get; set; }         
        public bool fixedPayment { get; set; }   
        public bool IsOneTime { get; set; }          
        public decimal OneTimeValue { get; set; }   
        public bool IsAllPayment { get; set; }      
        public decimal AllPaymentValue { get; set; }

    }

    public class FixedvsMinimumCalcDetailsModel
    {
        public double currentBalance { get; set; }

        public string paidDate { get; set; }
        public string fixedPaidDate { get; set; }

        public string payOffYears { get; set; }
        public string fixedPayOffYears { get; set; }

        public double totalPayment { get; set; }
        public double fixedTotalPayment { get; set; }

        public double interestPaid { get; set; }
        public double fixedInterestPaid { get; set; }

        public string yearSavings { get; set; }
        public double interestSavings { get; set; }
        public double paymentSavings { get; set; }

    }

    public class Mortgage_PI_Model
    {
        public Nullable<int> mortgageType { get; set; }
        public string cName { get; set; }
        [Required(ErrorMessage = "Loan Amount is required.")]
        public Nullable<double> loanAmount { get; set; }
        [Required(ErrorMessage = "Annual Interest rate is required.")]
        [Range(0, 30, ErrorMessage = "Select any value of annual Interest rate between 0 and 30.")]
        public Nullable<double> intRate { get; set; }
        [Required(ErrorMessage = "Number of instalments is mandatory")]
        [RegularExpression(@"[0-9]*", ErrorMessage = "Number of Instalments must be numeric")]
        public Nullable<int> no_of_terms { get; set; }
        [Required(ErrorMessage = "Month is required")]
        [Range(1, 12, ErrorMessage = "Please select a valid month.")]
        public Nullable<int> monName { get; set; }
        [Range(1500, 5000, ErrorMessage = "Please select a valid year.")]
        [Required(ErrorMessage = "Year is required")]
        public Nullable<int> years { get; set; }

        public double prinicipal_Interest_Pay { get; set; }
        public string payoff { get; set; }
        public Nullable<double> currentBalance { get; set; }

        public Nullable<double> singlePayment { get; set; }
        public Nullable<double> adnlPayment { get; set; }
        public Nullable<bool> prePayment { get; set; }
        public Nullable<bool> prePayment1 { get; set; }
    }

    public class MortgagePaymentModel
    {
        public string cName { get; set; }
        [Required(ErrorMessage = "Loan Amount is required.")]
        public Nullable<double> loanAmount { get; set; }
        [Required(ErrorMessage = "Annual Interest rate is required.")]
        [Range(0, 30, ErrorMessage = "Select any value of annual Interest rate between 0 and 30.")]
        public Nullable<double> intRate { get; set; }
        [Required(ErrorMessage = "Number of instalments is mandatory")]
        [RegularExpression(@"[0-9]*", ErrorMessage = "Number of Instalments must be numeric")]
        public Nullable<int> no_of_terms { get; set; }
        public Nullable<double> propertyTax { get; set; }
        public Nullable<double> homeInsurance { get; set; }
        public Nullable<double> monthlyPMI { get; set; }
        [Required(ErrorMessage = "Month is required")]
        [Range(1, 12, ErrorMessage = "Please select a valid month.")]
        public Nullable<int> monName { get; set; }
        [Range(1500, 5000, ErrorMessage = "Please select a valid year.")]
        [Required(ErrorMessage = "Year is required")]
        public Nullable<int> years { get; set; }

        public Nullable<double> prinicipal_Interest_Pay { get; set; }
        public Nullable<double> monthly_Tax_Insurance_PMIPay { get; set; }
        public Nullable<double> total_Monthly_MortagePay { get; set; }
    }

    public class CollegeCostResultModel
    {
        public int TotalCount { get; set; }

        public string collegeName { get; set; }
        public decimal inState { get; set; }
        public decimal outState { get; set; }
        public long collegeID { get; set; }
        public string city { get; set; }
        public string collegeState { get; set; }
        public decimal fees { get; set; }
        public decimal inStateTutionFees { get; set; }
        public decimal outStateTutionFees { get; set; }
        public decimal roomBoardFees { get; set; }
    }
    public class CollegecostListArg
    {
        public string name { get; set; }
        public string state { get; set; }
        public Nullable<bool> inState { get; set; }
        public Nullable<bool> outState { get; set; }
        public int TotalCount { get; set; }
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int SortColumn { get; set; }
        public int SortOrder { get; set; }

        public List<CollegeCostResultModel> nationalColleges { get; set; }
        public List<CollegeCostResultModel> otherColleges { get; set; }

    }
    public class MortgageMissingValueModel
    {
        public string cName { get; set; }
        [Required(ErrorMessage = "Loan amount is required.")]
        [Range(1, Double.MaxValue, ErrorMessage = "Please enter a valid loan amount.")]
        public Nullable<double> loanAmount { get; set; }
        [Required(ErrorMessage = "Annual interest rate is required.")]
        [Range(1, 30, ErrorMessage = "Select any value of annual interest rate between 1 and 30.")]
        public Nullable<double> intRate { get; set; }
        [Required(ErrorMessage = "Number of instalments is mandatory")]
        [Range(0, Int32.MaxValue, ErrorMessage = "Please enter a valid number of terms.")]
        [RegularExpression(@"[0-9]*", ErrorMessage = "Number of instalments must be numeric")]
        public Nullable<int> no_of_terms { get; set; }
        [Required(ErrorMessage = "Month is required")]
        [Range(1, 12, ErrorMessage = "Please select a valid month.")]
        public Nullable<int> monName { get; set; }
        [Range(1500, 5000, ErrorMessage = "Please select a valid year.")]
        [Required(ErrorMessage = "Year is required")]
        public Nullable<int> years { get; set; }

        [Required(ErrorMessage = "Monthly payment amount is mandatory")]
        [Range(0, Double.MaxValue, ErrorMessage = "Please enter a valid monthly payment.")]
        public Nullable<double> monthlyPayment { get; set; }
        public string currentDate { get; set; }
        public double currentBalance { get; set; }

        public Nullable<bool> loanAmountB { get; set; }
        public Nullable<bool> intRateB { get; set; }
        public Nullable<bool> no_of_termsB { get; set; }
        public Nullable<bool> monthlyPaymentB { get; set; }
    }
    public class mortgageAmortizationCalculator
    {
        public string cName { get; set; }
        public Nullable<double> intRate { get; set; }
        public Nullable<int> no_of_terms { get; set; }
        public string firstPayment { get; set; }
        public string currentDate { get; set; }
        public Nullable<double> currentBalance { get; set; }
        public Nullable<bool> oneTime { get; set; }
        public Nullable<bool> allPayments { get; set; }
        public Nullable<double> oneTimeValue { get; set; }
        public Nullable<double> allPaymentValue { get; set; }

        public Nullable<double> loanAmount { get; set; }
        public Nullable<int> no_of_payments { get; set; }
        public string payOff { get; set; }
        public Nullable<double> totalPayments { get; set; }
        public Nullable<double> totalAdditional { get; set; }
        public Nullable<double> totalInterest { get; set; }
        public double principal_Interest_Pay { get; set; }
        public List<mortgageAmortizationChart> amortList { get; set; }
    }

    public class mortgageAmortizationChart
    {
        public Nullable<int> pmt { get; set; }
        public Nullable<int> year { get; set; }
        public string date { get; set; }
        public string period { get; set; }
        public Nullable<double> principal { get; set; }
        public Nullable<double> interest { get; set; }
        public Nullable<double> totalPrincipal { get; set; }
        public Nullable<double> totalInterest { get; set; }
        public Nullable<double> totalPayment { get; set; }
        public Nullable<double> prePayment { get; set; }
        public Nullable<double> balance { get; set; }
    }

}
