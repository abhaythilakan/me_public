﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class DebtDisappearFullModel
    {
        public List<DebtDisappearYearlyD2> yearlyDebt { get; set; }
    }

    public class DebtDisappearYearlyD2
    {
        public long debtID { get; set; }
        public string debtCreditor { get; set; }
        public long debtCatId { get; set; }
        public string debtCatName { get; set; }
        public decimal beginingBalance { get; set; }
        public decimal initialMinimumPayment { get; set; }
        public decimal accelPayment { get; set; }
        public Nullable<DateTime> accelDate { get; set; }
        public string accelDateString { get; set; }
        public int yearName { get; set; }
        public Nullable<DateTime> projPayOff { get; set; }
        public decimal endingBalance { get; set; }
        public decimal interestPaid { get; set; }
        public List<DebtDisappearMnthsD2> mnthList { get; set; }
    }
    public class DebtDisappearMnthsD2
    {
        public string mnthName { get; set; }
        public int mnthNo { get; set; }
        public decimal amtEMI { get; set; }
        public string Comment { get; set; }
    }


    public class DebtDisappearYearlyD1
    {
        public string yearNo { get; set; }
        public decimal beginingBalance { get; set; }
        public decimal initMinPayment { get; set; }
        public decimal endingBalance { get; set; }
        public decimal interestPaid { get; set; }
        public List<DebtDisappearMonthlyD1> monthList { get; set; }
    }
    public class DebtDisappearMonthlyD1
    {
        public int monthNo { get; set; }
        public string monthName { get; set; }
        public decimal payOff { get; set; }
        public List<DebtDisappearListD1> debtList { get; set; }
    }
    public class DebtDisappearListD1
    {
        public int debtCatId { get; set; }
        public int debtCatName { get; set; }
        public DateTime acceleratedDate { get; set; }
        public decimal acceleratedPayment { get; set; }
        public decimal payOff { get; set; }
    }

    public class DebtDisappearList
    {
        public long debtID { get; set; }
        public string debtCreditor { get; set; }
        public int debtCatId { get; set; }
        public string debtCatName { get; set; }
        public Nullable<DateTime> accelDate { get; set; }
        public decimal accelPayment { get; set; }
        public string accelDateString { get; set; }
        public decimal initialMinimumPayment { get; set; }
        public decimal newPayment { get; set; }
        public Nullable<DateTime> projPayOff { get; set; }
        public string projPayOffString { get; set; }
        public decimal projInterestPaid { get; set; }
        public decimal debtAmt { get; set; }
        //public decimal beginingBalance { get; set; }
        //public int yearName { get; set; }
        //public decimal endingBalance { get; set; }
        //public List<DebtDisappearMnthsD2> mnthList { get; set; }
    }
}
