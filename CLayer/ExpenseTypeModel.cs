﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class ExpenseTypeModel
    {
        public long id { get; set; }
        [Display(Name = "Name")]
        [Required(ErrorMessage = "Name is required")]
        public string TypeName { get; set; }
        [Display(Name = "Sub Expense")]
        [Required(ErrorMessage = "Sub Expense is required")]
        public Nullable<long> expenseSubTypeId { get; set; }
        [Display(Name = "Order")]
        public Nullable<int> sortOrder { get; set; }
        public Nullable<System.DateTime> createdDT { get; set; }
        public Nullable<System.DateTime> modifyDT { get; set; }
        public Nullable<long> modifyBy { get; set; }
        public Nullable<bool> isActive { get; set; }
    }
}
