﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CLayer
{
   public class UserModels
    {
        public long userId { get; set; }
        [StringLength(20, ErrorMessage = "Username length should be less than 20")]
        [Display(Name = "User Name")]
        [Required(ErrorMessage = "User Name is required")]
        public string userName { get; set; }
        //  [Required(ErrorMessage = "Password is required")]
        [StringLength(20, ErrorMessage = "Password charector length should be less that 25")]
        [Display(Name = "Password")]
        [RegularExpression(@"^(?=.*[A-Za-z])(?=.*[0-9])(?=(?:.*?[!@#$%\^&*\(\)\-_+=;:'""\/\[\]{},.<>|`]){2}).{8,20}$", ErrorMessage = "Password must contain at least two special characters, minimum eight characters length and should be alpha numeric.")]
        public string password { get; set; }
        [EmailAddress(ErrorMessage = "Email address is invalid")]
        [StringLength(30, ErrorMessage = "Email address length should be less than 30")]
        [Display(Name = "Email Address")]
        public string emailPrimary { get; set; }
        public Nullable<bool> emailPrimaryValid { get; set; }
        public string emailSecondary { get; set; }
        public Nullable<bool> emailSecondaryValid { get; set; }
        public Nullable<long> advisorId { get; set; }
        public Nullable<long> clientId { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<System.DateTime> trialEndDate { get; set; }
        public Nullable<System.DateTime> expDate { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<bool> isAdmin { get; set; }
        public Nullable<bool> isManager { get; set; }
        public Nullable<bool> isSuperUser { get; set; }
        public Nullable<bool> isLicensed { get; set; }
        public string hashCode { get; set; }
        public Nullable<bool> requestedForgot { get; set; }
        public Nullable<bool> isTrial { get; set; }
        public Nullable<bool> isNonTrial { get; set; }

        #region Additional Properties 
        public long profileId { get; set; }
        #endregion
    }
}
