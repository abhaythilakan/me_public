﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CLayer
{
    public class EmailAlertModel
    {
        public int ea_Id { get; set; }
        [Display(Name = "Help")]
        public string ea_Code { get; set; }
        [Display(Name = "Title")]
        [Required(ErrorMessage = "Title is required")]
        public string ea_Title { get; set; }
        public Nullable<int> ea_NoofDays { get; set; }
        [Display(Name = "Add Bcc")]
        public string ea_BCC { get; set; }
        [Display(Name = "Content")]
        [Required(ErrorMessage = "Content is required")]
        [UIHint("tinymce_full")]
        [AllowHtml]
        public string ea_Content { get; set; }
        public Nullable<System.DateTime> createdDT { get; set; }
        public Nullable<System.DateTime> modifyDT { get; set; }
        public Nullable<long> modifyBy { get; set; }
        public Nullable<bool> isActive { get; set; }
    }

    public class AdvisorCountModel
    {
        [Display(Name = "Total no. of Active Advisors")]
        public int activeAdvisorCnt { get; set; }
        [Display(Name = "Total no. of Expired Advisors")]
        public int expAdvisorCnt { get; set; }
        [Display(Name = "Total no. of Expired Trial Advisors")]
        public int expTrialAdvisorCnt { get; set; }
        [Display(Name = "Total no. of Clients")]
        public int activeClientCnt { get; set; }
    }
}
