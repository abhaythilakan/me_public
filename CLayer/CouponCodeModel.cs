﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class CouponCodeModel
    {

        public int cc_Id { get; set; }
        [Display(Name = "Coupon Code")]
        [Required(ErrorMessage = "Coupon Code is required")]
        public string cc_Code { get; set; }
        [Display(Name = "Sales Person")]
        [Required(ErrorMessage = "Sales Person is required")]
        [Range(1, int.MaxValue, ErrorMessage = "Sales Person is required")]
        public int sp_Id { get; set; }
        public string sp_Name { get; set; }
        [Display(Name = "Details")]
        public string cc_Details { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<System.DateTime> modifyDate { get; set; }
        public Nullable<long> modifyBy { get; set; }
        public bool? cc_isActive;

        public List<CouponCodeDetailModel> CouponCodeDetails { get; set; }

        public List<common.DropDownList_Int> SalesPersionsList { get; set; }

        public CouponCodeModel()
        {
            SalesPersionsList = common.DDLReg.getDropDownList_Int(common.enumDropDownList.SalesPersons).ToList();
        }
    }

    public class CouponCodeDetailModel
    {
        public int ccd_Id { get; set; }
        //[Display(Name = "Coupon Code")]
        //[Required(ErrorMessage = "Coupon Code is required")]
        //[Range(1, int.MaxValue, ErrorMessage = "Invalid Coupon Code")]
        public int cc_Id { get; set; }
        [Required(ErrorMessage = "Title is required")]
        public string ccd_Title { get; set; }
        [Display(Name = "Initial Setup Fee ($)")]
        public Nullable<decimal> ccd_initSetupFee { get; set; }
        [Display(Name = "Initial Monthly Fee ($)")]
        public Nullable<decimal> ccd_initMonthFee { get; set; }
        [Display(Name = "Initial Period ($ in months)")]
        public Nullable<int> ccd_initPeriodMonth { get; set; }
        [Display(Name = "Renewal Monthly Fee ($)")]
        public Nullable<decimal> ccd_RenewalMonthlyFee { get; set; }
        [Display(Name = "Multiuser Monthly Discount")]
        public Nullable<decimal> ccd_MultiUMonthDiscount { get; set; }
        [Display(Name = "Commission (%)")]
        public Nullable<int> ccd_Commision { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<System.DateTime> modifyDate { get; set; }
        public Nullable<long> modifyBy { get; set; }
    }


}
