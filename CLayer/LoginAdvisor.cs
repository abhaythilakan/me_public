﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class LoginAdvisor
    {
        public Nullable<long> advisorID { get; set; }
        public Nullable<bool> isAdmin { get; set; }
        public Nullable<DateTime> expireDate { get; set; }
        public Nullable<long> managerID { get; set; }
        public Nullable<int> isActive { get; set; }
        public Nullable<DateTime> endTrialDate { get; set; }
        public Nullable<int> appVersion { get; set; }
        public Nullable<bool> isSuperUser { get; set; }
        public string Name { get; set; }
        public string email { get; set; }
        public string phone1 { get; set; }
    }
}
