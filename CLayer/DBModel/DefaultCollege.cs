//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CLayer.DBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class DefaultCollege
    {
        public long id { get; set; }
        public Nullable<long> defaultEduID { get; set; }
        public Nullable<long> profileID { get; set; }
        public Nullable<long> advisorID { get; set; }
        public Nullable<long> clientID { get; set; }
        public Nullable<long> collegeID { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<System.DateTime> favcollegeCreatedDate { get; set; }
        public Nullable<System.DateTime> favcollegeUpdatedDate { get; set; }
        public Nullable<long> favcollegeModifiedBy { get; set; }
    }
}
