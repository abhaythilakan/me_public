//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CLayer.DBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Advisor
    {
        public long advisorId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string title1 { get; set; }
        public string title2 { get; set; }
        public Nullable<bool> finraReg { get; set; }
        public Nullable<bool> secReg { get; set; }
        public Nullable<long> managerId { get; set; }
        public string designation { get; set; }
        public string numPlanner { get; set; }
        public string disclaimer { get; set; }
        public string couponCode { get; set; }
        public string capitalChoiceCode { get; set; }
        public Nullable<decimal> setUpFee { get; set; }
        public Nullable<decimal> initMonthlyFee { get; set; }
        public Nullable<decimal> monthlyFee { get; set; }
        public string brokerName { get; set; }
        public string riaName { get; set; }
        public string imgPath { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<long> reportType { get; set; }
        public Nullable<bool> isActive { get; set; }
    }
}
