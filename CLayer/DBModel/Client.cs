//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CLayer.DBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Client
    {
        public long clientId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public Nullable<long> advisorId { get; set; }
        public Nullable<System.DateTime> DOBdate { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<long> modifiedBy { get; set; }
        public Nullable<System.DateTime> modifiedDate { get; set; }
        public string imagePath { get; set; }
        public Nullable<int> reportType { get; set; }
        public Nullable<bool> isActive { get; set; }
    }
}
