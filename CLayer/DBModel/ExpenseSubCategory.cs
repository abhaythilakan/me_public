//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CLayer.DBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class ExpenseSubCategory
    {
        public long id { get; set; }
        public string subCategory { get; set; }
        public Nullable<long> expenseCategoryId { get; set; }
    }
}
