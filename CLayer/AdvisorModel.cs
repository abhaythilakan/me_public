﻿using CLayer.common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CLayer
{
    public class AdvisorModel
    {
        public long advisorId { get; set; }
        [Required(ErrorMessage = "First name is required.")]
        [StringLength(25, ErrorMessage = "First name length should be less than 25 characters.")]
        [Display(Name = "First Name")]
        public string firstName { get; set; }
        [Required(ErrorMessage = "Last name is required.")]
        [StringLength(25, ErrorMessage = "Last name length should be less than 25 characters.")]
        [Display(Name = "Last Name")]
        public string lastName { get; set; }
        public string titleA { get; set; }
        public string titleB { get; set; }
        public Nullable<long> managerId { get; set; }
        public string numPlanner { get; set; }
        public string disclaimer { get; set; }
        public string couponCode { get; set; }
        public string capitalChoiceCode { get; set; }
        public Nullable<decimal> setUpFee { get; set; }
        public Nullable<decimal> initMonthlyFee { get; set; }
        public Nullable<decimal> monthlyFee { get; set; }


        [Required(ErrorMessage = "FINRA Registration is required.")]
        public Nullable<bool> finraReg { get; set; }


        [Required(ErrorMessage = "SEC/RIA Registration is required.")]
        public Nullable<bool> secReg { get; set; }


        [StringLength(25, ErrorMessage = "Broker dealer name length should be less than 25 characters.")]
        public string brokerName { get; set; }
        [StringLength(25, ErrorMessage = "SEC/RIA  name length should be less than 25 characters.")]
        public string riaName { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public string designation { get; set; }
        public string imgPath { get; set; }


    }  //advisor Details
    public class AdvisorBranchModel
    {
        public long bId { get; set; }
        public long advisorId { get; set; }
        [StringLength(20, ErrorMessage = "Branch first name length should be less than 20 letters.")]
        public string bFirstName { get; set; }
        [StringLength(20, ErrorMessage = "Branch last name length should be less than 20 letters.")]
        public string bLastName { get; set; }
        [StringLength(100, ErrorMessage = "Branch address length should be less than 200 letters.")]
        public string bAddress { get; set; }
        [StringLength(50, ErrorMessage = "Branch city name length should be less than 50 letters.")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Branch city name only contains letters.")]
        public string bCity { get; set; }
        public string bState { get; set; }
        [RegularExpression("(^\\d{5}(-\\d{4})?$)|(^[ABCEGHJKLMNPRSTVXY]{1}\\d{1}[A-Z]{1} *\\d{1}[A-Z]{1}\\d{1}$)", ErrorMessage = "Branch zipcode is invalid.")]
        //[Required(ErrorMessage = "Please enter branch Zipcode.")]
        public string bZip { get; set; }
        [StringLength(10, ErrorMessage = "Branch phone number length should be less than 10 characters.")]
        [RegularExpression(@"[0-9]*", ErrorMessage = "Branch phone number must be a numeric Value.")]
        public string bPhone { get; set; }
        [StringLength(10, ErrorMessage = "Branch phone number length should be less than 10.")]
        [RegularExpression(@"[0-9]*", ErrorMessage = "Branch cellphone number must be a numeric Value.")]
        public string bCompany { get; set; }
    }//advisor branch Details
    public class AdvisorContactModel
    {
        public long contactId { get; set; }
        public Nullable<long> advisorId { get; set; }
        [StringLength(100, ErrorMessage = "Contact address length should be less than 200 letters.")]
        public string address { get; set; }
        [StringLength(50, ErrorMessage = "Contact city name length should be less than 50 letters.")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Please enter a valid contact city name.")]
        public string city { get; set; }
        [Required(ErrorMessage = "Please choose contact address state.")]
        public string state { get; set; }
        [RegularExpression("(^\\d{5}(-\\d{4})?$)|(^[ABCEGHJKLMNPRSTVXY]{1}\\d{1}[A-Z]{1} *\\d{1}[A-Z]{1}\\d{1}$)", ErrorMessage = "Contact zipcode is invalid.")]
        [Required(ErrorMessage = "Please enter contact zipcode.")]
        public string zip { get; set; }
        [StringLength(10, ErrorMessage = "Contact phone number length should be less than 10 characters.")]
        [RegularExpression(@"[0-9]*", ErrorMessage = "Contact telephone number must be a numeric Value.")]
        public string telephone { get; set; }
        [StringLength(10, ErrorMessage = "Contact cellphone number length should be less than 10 characters.")]
        [RegularExpression(@"[0-9]*", ErrorMessage = "Contact cellphone number must be a numeric Value.")]
        public string cellphone { get; set; }
    } //advisor Contact Details

    public class advisorDetails// : Success
    {

        public AdvisorModel advisorInfo { get; set; }
        public DefaultsGlobalModel advglobal { get; set; }
        public DefaultsRetirementModel advRetirement { get; set; }
        public DefaultIncomeProjectionModel advIncome { get; set; }
        public DefaultsDebtModel advDebt { get; set; }
        public DefaultsEmergencyFundModel advEmergency { get; set; }
        public DefaultsInsuranceModel advInsurance { get; set; }
        public DefaultsEducationModel advEducation { get; set; }
        public AdvisorBranchModel advBranch { get; set; }
        public AdvisorContactModel advContact { get; set; }
        public UserModels UserInfo { get; set; }
        public AccesSettingsModel AccesSettings { get; set; }
        public string imageString { get; set; }
        public tokenInfo tokeninfo { get; set; }
        public AdvisorMailModel mailObj { get; set; }

    }
    public class AdvisorMailModel
    {

        public string subject { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string toEmail { get; set; }
        public string fromDisplayName { get; set; }
        public string advisoremail { get; set; }
        public string name { get; set; }



    }
    public class AccesSettingsModel //: Success
    {
        public List<adminPagePermissionModel> AdvancedReportDefaults { get; set; }
        public List<adminPagePermissionModel> ExpressReportDefaults { get; set; }

        public AccesSettingsModel()
        {
            List<adminPagePermissionModel> AdvancedReportDefaults = new List<adminPagePermissionModel>();
            List<adminPagePermissionModel> ExpressReportDefaults = new List<adminPagePermissionModel>();

        }
    }
    public class AdvisorDetailsDBModel //: Success
    {
        public DBModel.Advisor AdvisorInfoDB { get; set; }
        public DBModel.User UserInfoDB { get; set; }
        public AdvisorDetailsDBModel()
        {
            AdvisorInfoDB = new DBModel.Advisor();
            UserInfoDB = new DBModel.User();

        }
    }  //ENTITY db model class

    public class imageUpload : Success
    {

        [Required(ErrorMessage = "User Required")]
        [Display(Name = "User")]
        public long id { get; set; }
        [Display(Name = "Image")]
        [Required(ErrorMessage = "Image Required")]

        public string imageString { get; set; }
    }
    public class adminPagePermissionModel //: Success
    {
        public long perID { get; set; }
      //  public Nullable<long> advisorId { get; set; }
        public string pageName { get; set; }
        public Nullable<int> reportTypeID { get; set; }
        public Nullable<int> pageId { get; set; }
        public Nullable<bool> isRead { get; set; }
        public Nullable<bool> isEdit { get; set; }
        public Nullable<bool> isActive { get; set; }
    }

    public class AdvisorListArgModel //: Success
    {
        [Required(ErrorMessage = "Manager ID required.")]
        public long MangerID { get; set; }
       // [Required(ErrorMessage = "Invalid Advisor")]
       // [Display(Name = "Advisor")]
      //  [Range(1, long.MaxValue, ErrorMessage = "Invalid Advisor")]
       // public long advisorId { get; set; }
        public int TotalCount { get; set; }
        public string SearchText { get; set; }
        [Required(ErrorMessage = "Page Number required.")]
        public int PageNo { get; set; }
        public int PageSize { get; set; }
        public int SortColumn { get; set; }
        public int SortOrder { get; set; }
    }
    public class AdvisorListModel
    {
        public AdvisorListArgModel Collection { get; set; }
        public List<advisorList> advisors { get; set; }
    }

    public class advisorList
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string emailPrimary { get; set; }
        public Nullable<DateTime> createdDate { get; set; }
        public Nullable<DateTime> renewdate { get; set; }
        public int TotalCount { get; set; }
        public int clients { get; set; }
        public long advisorId { get; set; }
        public bool isActive { get; set; }

    }
}
