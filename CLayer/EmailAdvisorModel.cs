﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using static CLayer.common.CustomEnum;

namespace CLayer
{
    public class EmailAdvisorModel
    {
        public long ea_Id { get; set; }
        [Display(Name = "From")]
        [Required(ErrorMessage = "From Email is required")]
        public string ea_FromEmail { get; set; }
        [Display(Name = "Subject")]
        [Required(ErrorMessage = "Subject is required")]
        public string ea_Subject { get; set; }
        [Display(Name = "Body")]
        [Required(ErrorMessage = "Body is required")]
        [AllowHtml]
        [UIHint("tinymce_full")]
        public string ea_Content { get; set; }
        public string ea_Advisors { get; set; }
        public Nullable<System.DateTime> ea_SendDT { get; set; }

        public int advCatID { get; set; }

        public List<AdvisorEmailListModel> advEmailList { get; set; }

        public SelectList advisorCatList { get; set; }

        public EmailAdvisorModel()
        {
            advCatID = 0;
            var values = from EmailAdvisorCategory e in Enum.GetValues(typeof(EmailAdvisorCategory))
                         select new { Id = e, Name = e.ToCaption() };
            advisorCatList = new SelectList(values, "Id", "Name");
            advEmailList = new List<AdvisorEmailListModel>();
        }
    }

    public class AdvisorEmailListModel
    {
        public long advisorId { get; set; }
        public string advEmail { get; set; }
        public bool isSelected { get; set; }
    }
}
