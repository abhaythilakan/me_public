﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CLayer
{
    public class AssetModel
    {
        public decimal totalSum { get; set; }
        public List<NonRetirementAssetsModel> AssetList { get; set; }
    }

    public class NonRetirementAssetsModel {
        public long assetID { get; set; }
        [Required(ErrorMessage = "Profile is required")]
        [Range(1,long.MaxValue, ErrorMessage = "Profile is required")]
        public long profileID { get; set; }
        public int assetCatID { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "Asset Type is required")]
        [Display(Name = "Asset Type")]
        public int assetTypeID { get; set; }
        //[Required(ErrorMessage = "Tax Type is required")]
        //[Range(1, int.MaxValue, ErrorMessage = "Tax Type is required")]
        public Nullable<int> taxVsDefferedID { get; set; }
        public string company_location { get; set; }
        [Display(Name ="Amount")]
        [Required(ErrorMessage = "Please Enter Amount")]
        public Nullable<decimal> amount { get; set; }
        public Nullable<System.DateTime> valueAsOf { get; set; }
        public Nullable<decimal> monthlyContribution { get; set; }
        public string description { get; set; }
        [Required(ErrorMessage = "Please Choose Emergency Fund Included ")]
        [Display(Name = "Emergency Fund")]
        public Nullable<bool> isIncludeEmergencyFund { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<System.DateTime> assetCreatedDate { get; set; }
        public Nullable<System.DateTime> assetUpdatedDate { get; set; }
        public Nullable<long> assetUpdatedBy { get; set; }

    }

    public partial class Asset_RetirementModel
    {
        public long assetID { get; set; }
        [Range(1, long.MaxValue, ErrorMessage = "profile required")]
        [Required(ErrorMessage = "profile required")]
        public Nullable<long> profileID { get; set; }
        public Nullable<long> personID { get; set; }
        public string name { get; set; }
        public Nullable<decimal> totalAmount { get; set; }
        public Nullable<int> lifeEcpectancy { get; set; }
        public Nullable<int> retAge { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<System.DateTime> updatedDate { get; set; }
        public Nullable<long> updatedBy { get; set; }
        public Nullable<bool> isActive { get; set; }
    }
    public partial class Asset_RetirementDetailsModel
    {

        public long retID { get; set; }
        [Required(ErrorMessage = "Retirement asset ID required")]
        public Nullable<long> assetID_Retirement { get; set; }
        [Required(ErrorMessage = "profile  required")]
        public Nullable<long> profileID { get; set; }
        [Required(ErrorMessage = "Please Choose Asset Type")]
        public Nullable<int> assetTypeID { get; set; }
        //[Range(1, long.MaxValue, ErrorMessage = "Tax type is required")]
        //[Required(ErrorMessage = "Tax type is required")]
        public Nullable<int> taxVsDefferedID { get; set; }
        public string company { get; set; }
        [Required(ErrorMessage = "Please enter Amount")]
        public Nullable<decimal> amount { get; set; }
        public Nullable<System.DateTime> valueAsOf { get; set; }
        public Nullable<decimal> monthlyContribution { get; set; }
        public Nullable<decimal> empContribution { get; set; }
        public string notes { get; set; }
        public bool isOtherRetirement { get; set; }
        public bool isActive { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<System.DateTime> updatedDate { get; set; }
        public Nullable<long> updatedBy { get; set; }
    }
    public class OtherRetirementAssetModel
    {
        public decimal totalSum { get; set; }
        public List<Asset_RetirementDetailsModel> AssetList { get; set; }
    }
    public  class Asset_EmergencyFundModel
    {
        public long fundID { get; set; }
        [Range(1, long.MaxValue, ErrorMessage = "Invalid Profile")]
        [Required(ErrorMessage = "Invalid Profile")]
        public long profileID { get; set; }
        [Required(ErrorMessage = "Please enter household income")]
        public Nullable<decimal> householdIncome { get; set; }
        [Required(ErrorMessage = "Please enter emergency fund")]
        public Nullable<decimal> emergencyFund { get; set; }
        public Nullable<System.DateTime> createdDate { get; set; }
        public Nullable<System.DateTime> updatedDate { get; set; }
        public Nullable<long> updatedBy { get; set; }
    }
    public class DeleteAssetDetailsModel
    {
      //  public long fundID { get; set; }
        [Required(ErrorMessage = "Profile ID required")]
        public long profileID { get; set; }
        [Required(ErrorMessage = "Delete ID Requred")]
        public long deleteID { get; set; }
    }

}
